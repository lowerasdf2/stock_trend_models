# -*- coding: utf-8 -*-
"""
Created on Tue Oct  1 18:53:27 2019

@author: Abhilash Dharmavarapu
@title: News Sentiment Analysis
"""

from newsapi import NewsApiClient
from datetime import datetime
import json
import csv
from textblob import TextBlob
import requests

class SA_News:
   def main():
       def writeTo(data):
            with open('news_data.csv', 'a', newline='') as csvFile:
                writer = csv.writer(csvFile)
                writer.writerow(['Date' ,'Headline', 'Description'])
                for line in data:
                    writer.writerow(line)
                    
       company = 'Current Stock Market United States'
       apiKey = '30a8a6c432804090823fd38a57e39e29'
       time = datetime.today()
       date = time.strftime("%m/%d/%Y")
       r = requests.get('https://newsapi.org/v2/everything?q='+ company +'&from='+ date +'&to='+date+'&sortBy=popularity&apiKey=' + apiKey).json()
       headlines = json.loads(json.dumps(r))
       articles = headlines['articles']
       csvData = []
       for a in articles:
           print(a['title'] + ", " + a['description'])
           csvData.append([str(date), a['title'], a['description']])
       writeTo(csvData)
   if __name__ == "__main__":
       main()
        
