# import libraries
from urllib.request import urlopen
from bs4 import BeautifulSoup
from datetime import datetime
from datetime import timedelta
import re
import csv


# define start and stop dates
current_date = '2018-02-01'
stop_date = '2019-02-20'

# convert date strings to datetime
current = datetime.strptime(current_date, "%Y-%m-%d")
stop = datetime.strptime(stop_date, "%Y-%m-%d")

csvData = []

while current <= stop:
    # update current_date string for url modification
    current_date = current.strftime('%Y-%m-%d')
    current_date = current_date.replace("-", "")
    # print(current_date)

    # specify the url with current_date
    news_page = 'https://www.wsj.com/news/archive/' + current_date

    # query the website and return the html to the variable ‘page’
    page = urlopen(news_page)

    # parse the html using beautiful soup and store in variable `soup`
    soup = BeautifulSoup(page, 'html.parser')

    # find article titles and append with date to array 
    for link in soup.findAll('a', href=re.compile('^https://www.wsj.com/articles/')):
        link_string = link.string
        # remove repeated junk articles
        if link_string != 'What’s News: World-Wide' and link_string != 'What’s News: Business & Finance' and link_string != 'Pepper...and Salt' and link_string != 'About the Newsroom' and link_string != '':
            # remove non-ASCII characters and replace with '
            if (link_string):
                link_final = ""
                for i in link_string:
                    if ord(i) < 128:
                        link_final += i
                    else:
                        link_final += '\''
                csvData.append([current_date, link_final]) # append article title to csvData
    
    # increase day by one
    current = current + timedelta(days=1)

# write csvData to 2D csv file
with open("wsj_titles.csv","w+") as my_csv:
    csvWriter = csv.writer(my_csv,delimiter=',')
    csvWriter.writerows(csvData)