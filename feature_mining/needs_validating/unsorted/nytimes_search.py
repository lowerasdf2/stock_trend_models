import json
import nytimes
import time

# Create search object using api key
search_obj = nytimes.get_article_search_obj ('cf000e36ee0a40c897f817790686153a')
field_limit = "type_of_material, news_desk, score, "

with open('nytimes.json', 'w') as fp:
    json.dump(search_obj.article_search(q='Google', begin_date='20181110', page=0), fp, indent=1)
