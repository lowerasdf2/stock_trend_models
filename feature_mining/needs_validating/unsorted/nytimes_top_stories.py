import json
import nytimes
import time

# Create search object using api key
search_obj = nytimes.get_article_search_obj ('cf000e36ee0a40c897f817790686153a')

with open('nytimes.json', 'w') as fp:
    json.dump(search_obj.article_search(q='Alphabet', begin_date='20181101', page=1), fp, indent=1)
