import python_forex_quotes
import csv
from datetime import datetime

rawData = []
forexSymbols = []

client = python_forex_quotes.ForexDataClient('RbysPYlL3f2ymdv7kx55d1HPdic0Zal1')

with open('forexSymbols.csv', 'r') as my_csv:  # create array of individual arrays of forex symbols
    csvReader = csv.reader(my_csv, delimiter=',')
    forexArray = list(csvReader)

for i in range(len(forexArray)):               # convert individual forex arrays to master array of strings
    forexSymbols.append(forexArray[i][0])

csvData = client.getQuotes(forexSymbols)       # get data in dictionary form

for i in range(len(forexSymbols)):             # create array of raw data from dictionary 
    for metric, value in csvData[i].items():
        rawData.append([metric, value])

for i in range(len(rawData)//5):                # get individual forex exchange data
    symbol = rawData[5 * (i-1)][1]
    bid = rawData[1 + 5 * (i-1)][1]
    ask = rawData[2 + 5 * (i-1)][1]
    price = rawData[3 + 5 * (i-1)][1]
    unixTS = int(rawData[4 + 5 * (i-1)][1])
    utcTS = datetime.utcfromtimestamp(unixTS).strftime('%Y-%m-%dT%H:%M:%SZ')

    bidArray = [utcTS, bid]
    askArray = [utcTS, ask]
    priceArray = [utcTS, price]

    with open("/path to %s_bid.csv" % (symbol), "a") as my_csv:
        csvWriter = csv.writer(my_csv,delimiter=',')
        csvWriter.writerow(bidArray)

    with open("/path to %s_ask.csv" % (symbol), "a") as my_csv:
        csvWriter = csv.writer(my_csv,delimiter=',')
        csvWriter.writerow(askArray)

    with open("/path to %s_price.csv" % (symbol), "a") as my_csv:
        csvWriter = csv.writer(my_csv,delimiter=',')
        csvWriter.writerow(priceArray)

