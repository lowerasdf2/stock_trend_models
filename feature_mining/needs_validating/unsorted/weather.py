import pandas as pd
import numpy as np
from sklearn import preprocessing
import numbers
import pyowm

class Weather():

    def __init__(self):
        self.OWM_KEY = 'b5062dd407e8fe4d0a2e1eadf6c29841'
        self.pkl_path = './../data/current_weather_data.pkl'
        # NOTE: only some attributes are relevant
        # self.current_atrs = ['_clouds','_detailed_status','_dewpoint','_heat_index',
        #                      '_humidex','_humidity','_pressure','_rain',
        #                      '_reference_time','_snow','_status','_sunrise_time',
        #                      '_sunset_time','_temperature','_visibility_distance',
        #                      '_weather_code','_weather_icon_name','_wind']
        self.current_atrs = ['_clouds','_humidity','_pressure',
                             '_reference_time','_sunrise_time',
                             '_sunset_time','_temperature','_visibility_distance',
                             '_weather_code' ,'_wind']
        self.city_ids = pd.read_pickle('./../data/cityids.pkl')
        self.wm = pyowm.OWM(self.OWM_KEY)
        self.df = {}

    def collect_weather_current(self):
        self.df = {}
        for city_id in self.city_ids[10:60]:
            print(city_id)
            w = self.wm.weather_at_id(city_id).get_weather()
            data = {}
            for atr in self.current_atrs:
                data[atr[1:]] = getattr(w, atr)
            self.df[city_id] = data
        self.df = pd.DataFrame(self.df).T
        self.df.fillna(value=np.nan, inplace=True)
        self.df = self.df[sorted(self.df.columns)]

    def expand_numerical_data(self):
        def chcol(attrname):
            return lambda x : x[attrname]
        for c in [c for c in self.df.columns if isinstance(self.df[c][self.df.index[0]],dict)]:
            for k in [k for k in self.df[c][self.df.index[0]].keys() if isinstance(self.df[c][self.df.index[0]][k], numbers.Number)]:
                try:
                    self.df[k] = self.df[c].apply(chcol(k))
                except:
                    pass
            if c not in self.df[c][self.df.index[0]].keys():
                del self.df[c]

    def mms_normalize(self):
        self.df = (self.df-self.df.mean())/(self.df.max()-self.df.min())

def main():
    w = Weather()
    w.collect_weather_current()
    w.expand_numerical_data()
    w.mms_normalize()
    w.df.to_pickle(w.pkl_path)

if __name__ == '__main__':
    main()
