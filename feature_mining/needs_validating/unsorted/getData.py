from apscheduler.schedulers.blocking import BlockingScheduler
import datetime
import sys
sys.path.append('/home/mattiboochannel/data/minuteForex')
import getMinuteForex
sys.path.append('/home/mattiboochannel/data/minuteStocks')
import stocks


def getForex():     # requires two functions because 1Forge only allows 1000 requests per day per API key
    dt = datetime.datetime.now()
    if dt.hour < 12:
        getMinuteForex.getMinuteForex1()
    else:
        getMinuteForex.getMinuteForex2()
    print("Got Forex Data")

def getStocks():
    stocks.getStocks()
    print("Got Stock Data")



scheduler = BlockingScheduler()
scheduler.add_job(getForex, trigger='cron', minute='*')
scheduler.add_job(getStocks, trigger='cron', minute='*')
scheduler.start()