import pandas as pd
import os

def mergeData():
    firsttime = 0
    for folder in os.listdir():
        if (os.path.isdir("PATH HERE" % (folder))):
            for filename in os.listdir("PATH HERE/%s" % (folder)):
                if (filename.endswith('csv')):
                    print("%s, %s" % (folder, filename))
                    datacol = pd.read_csv('PATH HERE/%s/%s' % (folder, filename), header=None, names = [0, '%s' % (filename)])
                    if (firsttime == 0):
                        data = datacol
                        firsttime = 1
                    else:  
                        data = pd.merge(data, datacol, on=0, how = 'outer')
    return(data)