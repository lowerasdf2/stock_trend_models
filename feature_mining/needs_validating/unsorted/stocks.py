from iexfinance import Stock
import pandas as pd
import time
import csv
import os

def symbols():
    sym = pd.read_csv('companyList.csv')['Symbol']
    for i, s in enumerate(sym):
        try:
            Stock(s).get_quote()
        except:
            del sym[i]
    sym.to_pickle('./../../data/iex_finance/symbols.pkl')


def main(iters, stocks, attrs, file='./../../data/iex_finance/stocks.csv', wait=1):
    indexer = 'latestUpdate'

    latest_collect = 0

    # if not os.path.exists(file): os.system(f"touch {file}")
    file = open(file, 'w')
    writer = csv.writer(file)
    writer.writerow(['TIME'] + attrs)

    i = 0
    while i < iters:
        row = []
        quote = pd.DataFrame(Stock(stocks).get_quote())
        quote.to_pickle('quote.pkl')
        new_collect = quote.loc[indexer].max()
        if new_collect > latest_collect:
            row.append(new_collect)
            for attr in attrs:
                row.append(dict(quote.loc[attr]))
            latest_collect = new_collect
            writer.writerow(row)
        time.sleep(wait)
        i += 1

if __name__ == '__main__':
    stocks = pd.read_pickle('./../../data/iex_finance/companies.pkl')
    attrs = ['avgTotalVolume','change','changePercent','close',
             'high','latestPrice','low','open']
    main(5, stocks, attrs)
