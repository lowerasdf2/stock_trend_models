from iexfinance import Stock
import pandas as pd
import csv
import os
import sys
import time
from datetime import datetime
from iexfinance import get_available_symbols

def writeData(quote, attrs):
    files = []
    for attr in attrs:
        files.append("%s.csv" % (attr))
    for i, file in enumerate(files):
        f = os.path.join('/home/mattiboochannel/data/dataFiles/minuteStocksData/', file)
        file = open(f, 'a')
        fn = [attrs[-1], attrs[i]]
        writer = csv.DictWriter(file, fieldnames=fn)
        date = datetime.utcnow().strftime('%Y-%m-%dT%H:%M:00Z')
        row = {fn[0]: date,
               fn[1]: dict(quote.loc[attrs[i]])}
        writer.writerow(row)
        file.close()

def getStocks():
    symbols = get_available_symbols(output_format='pandas')
    attrs   = ['avgTotalVolume','high','low']
    i = 0
    firstTime = 0
    rawData = pd.DataFrame()
    while i < len(symbols)/100:
        symbolsArray = []    
        for dic in symbols[100*i:99+100*i]:
            symbolsArray.append(dic['symbol'])
        newData = pd.DataFrame(Stock(symbolsArray).get_quote())
        newData['attributes'] = newData.index
        if (firstTime == 0):
                rawData = newData
                firstTime = 1
        else:
                rawData = rawData.reset_index().merge(newData, how="left").set_index('index')
        i += 1
    writeData(rawData, attrs)

getStocks()