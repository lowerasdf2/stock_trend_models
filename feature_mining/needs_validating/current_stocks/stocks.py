from iexfinance import Stock
import pandas as pd
import csv
import os
import time

def collect(self, symbols, attrs):
    files = [f"{attr}.csv" for attr in attrs]
    quote = pd.DataFrame(Stock(symbols).get_quote())
    for i, file in enumerate(files):
        file = open(file, 'w')
        fn = [attrs[-1], attrs[i]]
        writer = csv.DictWriter(file, fieldnames=fn)
        row = {fn[0]: quote.loc[attrs[-1]].min(),
               fn[1]: dict(quote.loc[attrs[i]])}
        print(row)
        writer.writerow(row)
        file.close()

def main():
    symbols = ['MSFT','AAPL','GOOG']
    attrs   = ['avgTotalVolume','high','low', 'latestUpdate']
    while True:
        collect(symbols, attrs)
        time.sleep(5)

if __name__ == '__main__':
    main()
