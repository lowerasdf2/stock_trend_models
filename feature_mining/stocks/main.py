import stocks
import csv
import time
import os
from google.cloud import pubsub_v1 as pubsub

alphavantage_refresh_interval = 15

def alphavantage_refresh_stock(event, context):
    file = open('sp_500.csv')
    file.readline()

    stocknum = event['attributes']['stocknum']

    if stocknum:
        for i in range(stocknum-1):
            file.readline()
        nextstock = stocknum + 1

    with csv.reader(file) as stock:
        end_time = time.time() + alphavantage_refresh_interval
        stocks.insert_intraday_data(stock[0], '1min')

    while time.time() < end_time:
        time.sleep(1)

    publisher = pubsub.PublisherClient()
    topic_name = 'projects/{project_id}/topics/{topic}'.format(
        project_id=os.getenv('GOOGLE_CLOUD_PROJECT'),
        topic='stock-refresh')
    publisher.publish(topic_name, b'stock-refresh', stocknum=nextstock)
