from influxdb import InfluxDBClient
import urllib.request, json
import pandas as pd
import datetime as dt
import csv
import time

intraday_api_key = 'B1ACKE6QRVKTATIN'
daily_api_key = 'B1ACKE6QRVKTATIN'

def insert_daily_data(ticker):
    # JSON file with stock market data
    url_string = "https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol=%s&outputsize=full&apikey=%s"%(ticker,daily_api_key)

    # grab the data from the url and store date, low, high, volume, close, open values
    with urllib.request.urlopen(url_string) as url:
        # extract stock market data
        alpha_data = json.loads(url.read().decode())
        alpha_data = alpha_data['Time Series (Daily)']
        influx_data = alphavantage_to_influx(ticker, alpha_data)
        influxdb_insert(influx_data)

def insert_intraday_data(ticker, interval):
    # JSON file with stock market data
    url_string = "https://www.alphavantage.co/query?function=TIME_SERIES_INTRADAY&symbol=%s&interval=%s&outputsize=full&apikey=%s"%(ticker,interval,intraday_api_key)

    # grab the data from the url and store date, low, high, volume, close, open values
    with urllib.request.urlopen(url_string) as url:
        # extract stock market data
        alpha_data = json.loads(url.read().decode())
        alpha_data = alpha_data['Time Series ({})'.format(interval)]
        influx_data = alphavantage_to_influx(ticker, alpha_data, interval)
        influxdb_insert(influx_data)

def alphavantage_to_influx(ticker, alpha_data, interval = 'daily'):
    influx_data = []

    # append data to json array
    for k,v in alpha_data.items():

        if (interval == 'daily'):
            datetime = dt.datetime.strptime(k, '%Y-%m-%d')
        else:
            datetime = dt.datetime.strptime(k, '%Y-%m-%d %H:%M:%S')

        influx_data.append({
            'measurement': 'stock',
            'tags': {
                'symbol': ticker
            },
            'time': datetime,
            'fields': {
                "open": float(v['1. open']),
                "high": float(v['2. high']),
                "low": float(v['3. low']),
                "close": float(v['4. close']),
                "volume": float(v['5. volume'])
            }
        })

    return influx_data

def influxdb_insert(data):
    client = InfluxDBClient(
        host='34.69.112.28',
        port=8086,
        username='intraday_stocks_user',
        password='Inv3st!nS&p500!',
        database='intraday_stocks')

    client.write_points(data)
    result = client.query('select count(*) from stock')
    #result = client.query('select symbol, open, high, low, close, volume from stock order by time desc limit 100')
    print("Result: {0}".format(result))

insert_intraday_data('AAPL', '1min')
