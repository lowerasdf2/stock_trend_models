#!/anaconda2/envs/AI_club/bin/python python3

import re
import csv
import requests

def format_date(date):
	"""Takes a str "date" of form YYYY-MM-DD and makes it YYYY-MM-DDTHH:MM:SSZ (standard format)
	Note: As the data is updated monthly, I don't think the time of day really matters
	If it does, then I can implement it, let me know. I have it defaulting to noon atm
	"""
	return date + "T12:00:00Z"


SOURCE_URL = "https://fred.stlouisfed.org/data/FEDFUNDS.txt"
HEADER_REGEX = re.compile(r"DATE\s+VALUE")

def main():

	# Open the text file
	with requests.get(SOURCE_URL) as r:

		# Store the lines of the file
		text = r.text

	# Parse the lines of the text
	san_text = text.replace("\\r","")
	lines = list(map(str.strip, san_text.split("\n")))

	# Find the line which is the header for the data using a somewhat convoluted regex search
	header_line = [i for i, item in enumerate(lines) if re.search(HEADER_REGEX, item)][0]

	# Trim the lines list to only the lines containing data
	lines = lines[header_line+1:-1]

	# Write all the data to the CSV file
	with open("federal_funds_rate.csv", "w", newline="") as csvfile:
		writer = csv.writer(csvfile)
		for line in lines:
			data = re.split(r"\s+", line)
			data[0] = format_date(data[0])
			print(data)
			writer.writerow(data)

# Run program if file is run
if __name__ == "__main__":
	main()


