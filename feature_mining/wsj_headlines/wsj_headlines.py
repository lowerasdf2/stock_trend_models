import bs4
import datetime
import time
import threading
from threading import Timer

from urllib.request import urlopen as uReq
from bs4 import BeautifulSoup as soup

start = time.time()
PERIOD_OF_TIME = 30 # 30 seconds

def parseHeadlines():
    my_url = 'https://www.wsj.com/news/politics'

    # opening up connection, grabs page
    uClient = uReq(my_url)
    page_html = uClient.read()
    uClient.close()

    # html parsing
    page_soup = soup(page_html, "html.parser")

    # grabs each headline
    headlines = page_soup.findAll("h3", {"class": ["wsj-headline dj-sg wsj-card-feature heading-1", "wsj-headline dj-sg wsj-card-feature heading-3"]})
    return headlines


filename = "wsj_headlines.csv"
f = open(filename, "w")
headers = "headline\n"


def test():
    while True:
        for headline in parseHeadlines():
            title = headline.a.text
            time_stamp = datetime.datetime.now()

            print(str(time_stamp) + ", " + title + "\n")
            f.write(str(time_stamp) + ", " + title + "\n")
        time.sleep(5)

        if time.time() > start + PERIOD_OF_TIME:
            exit()
            f.close()
            break

test()
# exit()
# f.close()
