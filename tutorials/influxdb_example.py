# using https://github.com/influxdata/influxdb-python
# documentation at https://influxdb-python.readthedocs.io/en/latest/

from influxdb import InfluxDBClient

json_body = [
    {
        "measurement": "FEATURE",
        "tags": {
            "ATTRIBUTE_NAME": "ATTRIBUTE",
        },
        "time": "2009-11-10T23:00:00Z",
        "fields": {
            "data_field1": 127.46,
            "data_field2": 127.48,
        }
    }
]

client = InfluxDBClient(
    host='34.70.45.22',
    port=8086, username='testa',
    password='Unh@ckab13!',
    database='test1')

client.write_points(json_body)

result = client.query('select ATTRIBUTE_NAME, data_field1, data_field2 from FEATURE')
print("Result: {0}".format(result))
