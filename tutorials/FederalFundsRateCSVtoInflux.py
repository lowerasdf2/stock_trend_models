# using https://github.com/influxdata/influxdb-python
# documentation at https://influxdb-python.readthedocs.io/en/latest/

from influxdb import InfluxDBClient



json_body = [
]


# load data from csv

# convert to json objects and add to json body

example_json_object = {
    "measurement": "Monthly_Federal_Funds_Rate",
    "time": "2009-11-10T23:00:00Z",
    "fields": {
        "rate": 127.46,
    }
}


client = InfluxDBClient(
    host='34.70.45.22',
    port=8086, username='testa',
    password='Unh@ckab13!',
    database='test1')

client.write_points(json_body)
# MUST FIX QUERY BELOW
result = client.query('select ATTRIBUTE_NAME, data_field1, data_field2 from FEATURE')
print("Result: {0}".format(result))
