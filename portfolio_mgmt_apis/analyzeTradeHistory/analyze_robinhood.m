% clear all
close all
targetPrice=false;
%% map of target prices
targetPriceMap=containers.Map;
if targetPrice
    targetPriceMap('MU') = 62.62;
end
%% setdirs
stock_file=[filesep 'Users' filesep 'tammi' filesep 'Desktop' filesep 'github' filesep 'private_code' filesep 'Robinhood-master' filesep 'Robinhood' filesep 'orders.xlsx'];
symbolField='symbol';
transPriceField='price';
currPriceField='currentPrice';
quantField='shares';
transField='side';
%% init data containers
stockProfitOverTrans=containers.Map;
data_struct=table2struct( readtable(stock_file) );
%% cycle through all transactions for each stock Symbol
symbols=unique({data_struct.symbol});
summaryData=struct;
maxData=2;
profitOverTransFig=figure;
for symInd=1:length(symbols)
    currStocksHeld=0;
    totalProfit=0;
    transactionPrice=nan;
    foundCurrentPrice=false;
    numTrans=0;
    profitOverTrans=[0];
    quantOverTrans=[0];
    for t=1:length(data_struct)%:-1:1
        % check for proper Symbol
        if strcmp(data_struct(t).symbol,symbols{symInd})
            transactionPrice=(data_struct(t).price);
            if data_struct(t).shares == 0
                continue
            elseif isnan(transactionPrice)
                keyboard
            end
%             disp('new data')
%             data_struct(t)
            % if state is cancelled, skipo
            if strcmp(data_struct(t).state,'filled')
            elseif strcmp(data_struct(t).state,'cancelled') || ...
                    strcmp(data_struct(t).state,'confirmed') || ...
                    strcmp(data_struct(t).state,'rejected') || ...
                    strcmp(data_struct(t).state,'failed')
                continue
            else
                keyboard
            end
            % get price and number of stocks transacted
            quantity=data_struct(t).(quantField);
%             if gFin
            price=(data_struct(t).(transPriceField));
            if ~foundCurrentPrice
                currentPrice=data_struct(t).(currPriceField);
                foundCurrentPrice=true;
            end
%             else
%                 price=str2double(data_struct(t).(priceField));
%             end
            % sort buy and sell data
            if strcmp(data_struct(t).(transField),'buy')
                numTrans=numTrans+1;
                currStocksHeld=currStocksHeld+quantity;
                totalProfit=totalProfit-(quantity*price);
                quantOverTrans(numTrans+1)=currStocksHeld;%profitOverTrans(numTrans)-(quantity*price);
                profitOverTrans(numTrans+1)=totalProfit+(currStocksHeld*price);%profitOverTrans(numTrans)-(quantity*price);
            elseif strcmp(data_struct(t).(transField),'sell')
                numTrans=numTrans+1;
                currStocksHeld=currStocksHeld-quantity;
                totalProfit=totalProfit+(quantity*price);
                quantOverTrans(numTrans+1)=currStocksHeld;%profitOverTrans(numTrans)-(quantity*price);
                profitOverTrans(numTrans+1)=totalProfit+(currStocksHeld*price);%profitOverTrans(numTrans)+(quantity*price);
            else
                keyboard
            end
        end
    end
    if currStocksHeld > 0
        profitOverTrans(end)=profitOverTrans(end)-(currStocksHeld*price);
        profitOverTrans(end)=profitOverTrans(end)+(currStocksHeld*currentPrice);
    end
    if isKey(targetPriceMap,symbols{symInd})
        transactionPrice=targetPriceMap(symbols{symInd});
    end
    summaryData(symInd).Symbol=symbols{symInd};
    summaryData(symInd).currStocksHeld=currStocksHeld;
    realizedProfit=totalProfit;
    if currStocksHeld > 0
        breakEvenPrice=-realizedProfit/currStocksHeld;
        totalProfit=totalProfit+(currentPrice*currStocksHeld);
        % below 2 lines simulate selling at current price
%         profitOverTrans(end+1)=profitOverTrans(end)+(currentPrice*currStocksHeld);
%         quantOverTrans(end+1)=0;
%         profitOverTrans(2:end)=profitOverTrans(2:end)+(currentPrice*currStocksHeld);
    else
        breakEvenPrice=nan;
    end
    summaryData(symInd).profitOverTrans=profitOverTrans;
    stockProfitOverTrans(symbols{symInd})=profitOverTrans;

%     figure
%     subplot(2,1,1)
%     bar(profitOverTrans)
%     subplot(2,1,2)
%     bar(quantOverTrans)
    figure(profitOverTransFig)
%     x(1,:,:)=(profitOverTrans(quantOverTrans==0));
%     x(2,:,:)=(profitOverTrans(quantOverTrans==0))+1;
%     x(3,:,:)=(profitOverTrans(quantOverTrans==0))+2;
%     figure
%     bar(squeeze(x(2,:,:)))
%     hold on
%     bar(squeeze(x(1,:,:)))
    hold on
    data=profitOverTrans;%(quantOverTrans==0);
%     if strcmp(symbols{symInd},'IQ')
%         keyboard
%     end
%     if symInd==1
%     end
    if length(data) > 2
        plot(1:length(data),data)
        title(symbols{symInd})
        if length(data) > maxData
            maxData=length(data);                    
            plot([1:length(data)],zeros(length(data),1),'k')
        end
        x=1;
    end
    summaryData(symInd).totalProfit=totalProfit;
    summaryData(symInd).currentPrice=currentPrice;
    summaryData(symInd).breakEvenPrice=breakEvenPrice;
    
%     summaryData(symInd).percentGain=(totalProfit/summaryData(symInd).currentPrice)*100;
end

% num2str(sum(cell2mat({summaryData.totalProfit})))

disp(['Predicted total 1-yr profit = ' num2str(sum(cell2mat({summaryData.totalProfit})))])


figure
data=sort(cell2mat({summaryData.totalProfit}));
data=data(data~=0);
bar(1:length(data),data)

figure
plot(1:length(data),data)
hold on
plot(1:length(data),zeros(length(data),1),'k')
hold on
plot([length(data)/2,length(data)/2],[min(data),max(data)],'k-')
xlim([0,length(data)])
ylim([min(data),max(data)])
firstProfit=find(data>0,1,'first');
hold on
plot([firstProfit,firstProfit],[min(data),max(data)],'g-')
hold on
plot([0,0],[min(data),max(data)],'r')



% set(gca,'YScale','linear')
holdings=cell2mat({summaryData.currStocksHeld}).*cell2mat({summaryData.currentPrice});
holdings=num2cell(holdings);

[summaryData(:).holdings] = deal(holdings{:});

[data,inds]=sort(cell2mat({summaryData.holdings}),'descend');
summaryData_holdings=summaryData(inds);

[data,inds]=sort(cell2mat({summaryData.totalProfit}),'descend');
summaryData_profit=summaryData(inds);





