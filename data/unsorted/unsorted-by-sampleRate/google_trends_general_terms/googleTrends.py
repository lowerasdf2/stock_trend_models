import csv
from datetime import datetime, timedelta

with open('multiTimeline.csv', newline='') as csvfile:
    data = list(csv.reader(csvfile))

i = 3
while i < len(data):  #convert times to UTC format
    d = data[i][0]
    dt = datetime.strptime(d, "%Y-%m-%dT%X-06:00")
    utc = dt + timedelta(hours=5)  #data is released at 10:30 eastern time, converted to UTC

    utcString = utc.strftime('%Y-%m-%d %H:%M:%S')
    data[i][0] = utcString[0:10] + "T" + utcString[11:19] + "Z"
    
    i += 1

y = 1
while y < len(data[0]):    # iterates through columns of data
    dataArray = []
    z = 3
    while z < len(data):   # iterates through rows of data
        if data[z][y] != '':
           dataArray.append([data[z][0], data[z][y]])
        z += 1

    header = "search_term_" + data[2][y].lower()
    header = header.replace(":","_")
    header = header.replace(" ", "_")
    header = header.replace("__", "_")
    header = header.replace("/", "_")

    header = "trend_data/" + header
    
    with open("%s.csv" % (header),"w+") as my_csv:
        csvWriter = csv.writer(my_csv,delimiter=',')
        csvWriter.writerows(dataArray)

    y += 1
