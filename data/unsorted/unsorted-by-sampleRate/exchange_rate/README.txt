SOURCE NOTES: 

AQUIRED FROM: https://fred.stlouisfed.org/categories/94

Category: FRED > Categories > Money, Banking, & Finance > Exchange Rates > Daily Rates
Link: https://fred.stlouisfed.org/categories/94
README File Created: 2018-11-10
FRED (Federal Reserve Economic Data)
Economic Research Division
Federal Reserve Bank of St. Louis

======================================================================================
Series columns in the data directory sorted by title:

DATE RANGE: 1995-01-04 -- 2018-11-02

COLUMN                              ;Title; Units; Frequency; Seasonal Adjustment; Last Updated

DEXBZUS                             ;Brazil / U.S. Foreign Exchange Rate; Brazilian Reals to  1 U.S. $; D; NSA; 2018-11-05
DEXCAUS                             ;Canada / U.S. Foreign Exchange Rate; Canadian $ to  1 U.S. $; D; NSA; 2018-11-05
DEXCHUS                             ;China / U.S. Foreign Exchange Rate; Chinese Yuan to  1 U.S. $; D; NSA; 2018-11-05
DEXDNUS                             ;Denmark / U.S. Foreign Exchange Rate; Danish Kroner to  1 U.S. $; D; NSA; 2018-11-05
DEXHKUS                             ;Hong Kong / U.S. Foreign Exchange Rate; Hong Kong $ to  1 U.S. $; D; NSA; 2018-11-05
DEXINUS                             ;India / U.S. Foreign Exchange Rate; Indian Rupees to  1 U.S. $; D; NSA; 2018-11-05
DEXJPUS                             ;Japan / U.S. Foreign Exchange Rate; Japanese Yen to  1 U.S. $; D; NSA; 2018-11-05
DEXMAUS                             ;Malaysia / U.S. Foreign Exchange Rate; Malaysian Ringgit to  1 U.S. $; D; NSA; 2018-11-05
DEXMXUS                             ;Mexico / U.S. Foreign Exchange Rate; Mexican New Pesos to  1 U.S. $; D; NSA; 2018-11-05
DEXNOUS                             ;Norway / U.S. Foreign Exchange Rate; Norwegian Kroner to  1 U.S. $; D; NSA; 2018-11-05
DEXSIUS                             ;Singapore / U.S. Foreign Exchange Rate; Singapore $ to  1 U.S. $; D; NSA; 2018-11-05
DEXSFUS                             ;South Africa / U.S. Foreign Exchange Rate; South African Rand to  1 U.S. $; D; NSA; 2018-11-05
DEXKOUS                             ;South Korea / U.S. Foreign Exchange Rate; South Korean Won to  1 U.S. $; D; NSA; 2018-11-05
DEXSLUS                             ;Sri Lanka / U.S. Foreign Exchange Rate; Sri Lankan Rupees to  1 U.S. $; D; NSA; 2018-11-05
DEXSDUS                             ;Sweden / U.S. Foreign Exchange Rate; Swedish Kronor to  1 U.S. $; D; NSA; 2018-11-05
DEXSZUS                             ;Switzerland / U.S. Foreign Exchange Rate; Swiss Francs to  1 U.S. $; D; NSA; 2018-11-05
DEXTAUS                             ;Taiwan / U.S. Foreign Exchange Rate; New Taiwan $ to  1 U.S. $; D; NSA; 2018-11-05
DEXTHUS                             ;Thailand / U.S. Foreign Exchange Rate; Thai Baht to  1 U.S. $; D; NSA; 2018-11-05
DTWEXB                              ;Trade Weighted U.S. Dollar Index: Broad; Index Jan 1997=100; D; NSA; 2018-11-05
DTWEXM                              ;Trade Weighted U.S. Dollar Index: Major Currencies; Index Mar 1973=100; D; NSA; 2018-11-05
DTWEXO                              ;Trade Weighted U.S. Dollar Index: Other Important Trading Partners; Index Jan 1997=100; D; NSA; 2018-11-05
DEXUSAL                             ;U.S. / Australia Foreign Exchange Rate; U.S. $ to  1 Australian $; D; NSA; 2018-11-05
DEXUSEU                             ;U.S. / Euro Foreign Exchange Rate; U.S. $ to  1 Euro; D; NSA; 2018-11-05
DEXUSNZ                             ;U.S. / New Zealand Foreign Exchange Rate; U.S. $ to  1 New Zealand $; D; NSA; 2018-11-05
DEXUSUK                             ;U.S. / U.K. Foreign Exchange Rate; U.S. $ to  1 British Pound; D; NSA; 2018-11-05
DEXVZUS                             ;Venezuela / U.S. Foreign Exchange Rate; Venezuelan Bolivares to  1 U.S. $; D; NSA; 2018-11-05

BE CAREFUL THAT THE EXCHANGE RATES ARE NOT ORGANIZED AS OTHER TO USD OR USD TO OTHER, READ THE DOCUMENTATION ABOVE WHEN USING!
SOME DATA IS MISSING (THERE ARE DATES THAT SUCH DATA IS UNAVAILABLE, AND THIS IS DIFFERENT ACROSS COUNTRIES)
