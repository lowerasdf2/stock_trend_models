---
jupyter:
  jupytext:
    formats: ipynb,md
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.1'
      jupytext_version: 1.2.4
  kernelspec:
    display_name: Python 3
    language: python
    name: python3
---

```python
# Authors: Chris Endemann, Paul Pak


# Script Summary: Script to:
# 1. hold Feature class (temporarily; can move to separate file later)
# 2. Load in Features based on useful arguments (e.g. sample-rate, dataType)

```

```python
# import stuff
import pandas as pd
from sklearn.ensemble import ExtraTreesClassifier
import matplotlib.pyplot as plt

```

```python
# set global vars
baseDataDir='C:\\Users\\qualiaMachine\\Documents\\code\\stock_trend_models\\'
# baseDataDir='C:\\Users\qualiaMachine\Documents\code\stock_trend_models\'
```

```python
# STEP1. Finish this Feature Class file. Many methods should be created to make it easy to manipulate and query data from the Feature class
class Feature(object):
    # define attributes of class initialized when new Feature object is created
    def __init__(self,featName,attrNames,
                 globalSR,attrSRs,
                 globalDataType,attrDataTypes,
                 globalCat,attrCats,DF):
        # global feature name
        self.featName = featName
        
        # * 1*M string array, where M = #of feature attributes. 
        # Contains names for all attributes of feature (could be identical 
        # to feature name when there's only one attribute). The index of each name
        # in this list can be used to find the index used to store the data
        self.attrNames = attrNames
        
        # feature sampleRate (NaN if not identical across all attributes)
        self.globalSR = globalSR
        
        # 1*M array, where M = #of feature attributes. Contains sample-rates for all attributes. 
        # Index should correspond to attrNames
        self.attrSRs = attrSRs
        
        # dataType
        self.globalDataType = globalDataType # nan if not identical across attributes
        self.attrDataTypes = attrDataTypes # 1*M
        
        # data category (these will be manually decided upon; can use categories to query specific subsets)
        self.globalCat=globalCat # nan if not identical across attributes
        self.attrCats=attrCats
        
        # attributes dataframe (use pandas dataframe to build this)
        # N*M-dimensional, where N=totalSamples, M = #of feature attributes
        # Should be possible to make this matrix sparse to save space and not store 
        # any empty cells for missing data
        self.DF = DF
        

    # methods to report on object features (getters)
    def dataFrame(self):
        print(self.DF)

    def attributeCats(self):
        print(self.attrCats)
        
    def attributeDataTypes(self):
        print(self.attrDataTypes)
        
    def globalDataType(self):
        print(self.globalDataType)
   
    def attributeSampleRates(self):
        print(self.attrSRs)
    
    def globalSampleRates(self):
        print(self.globalSR)
        
    def attributeNames(self):
        print(self.attrNames)
        
    def featureName(self):
        print(self.featName)
    
```

```python
# STEP2. Use Feature class to load in a single Feature
# 0. Init Feature list

# 1. Load .csv file: use ...\stock_trend_models\data\unsorted-by-sampleRate\federal_funds_rate_testLoadData.csv to start
fileToRead=(baseDataDir + 'data\\unsorted-by-sampleRate\\federal_funds_rate\\federal_funds_rate_testLoadData.csv')
df = pd.read_csv(fileToRead)
# Sort dataframe by date
df = df.sort_values('Timestamp')
# Double check the result
df.head()

#Using ExtraTreesClassifier - extract the top ten features from dataset
data = pd.read_csv(fileToRead)
X = data.iloc[:,0:20]
Y = data.iloc[:,-1]
model = ExtraTreesClassifer()
model.fit(X,Y)
features_importance = pd.Series(model.feature_importances_,index = X.columns)
feat_importances.nlargest(10).plot(kind='barh')
plt.show()


# 2. Extract properties of Feature and create new Feature object: 
# newFeat=Feature(featName,attrNames,globalSR,attrSRs,
#                 globalDataType,attrDataTypes,
#                 globalCat,attrCats,DF)



```

```python
# STEP3. Write a loadData function to create lists of different Features based on different input arguments

# this function should be able to load data using a variety of different input arguments
# 1. sample-rate: load all Features of a given sample-rate
# 2. data-type: load all Features of a given data-type (e.g. text-data, numeric)
# 3. 

import pandas as pd
from pandas import read_csv
from pandas import DataFrame
import numpy as py

def loadData(sampleRateFolderName): 
    

    
    # 1. load a folder of sample rate files
    
    dataframes = []
    for fileName in fileNames:
        dataframes.append(pd.read_csv(fileName))
    
    DF = pd.concat(dataframes, ignore_index = True) # initializing DF - all attribute values
    featName = sampleRateFolderName # initializing global featName
    
    for file in dataframes:
        
        df = pd.read_csv(file)
        DF = df.to_numpy()  # initializing DF
        
        attrNames = list(data.columns) # initialing attrNames
        
        globalSR = []
        if (attrNames[1] == attrNames[2] == attrNames[3]):
            globalSR = attrNames[1]
        else:
            globalSR = 'NaN'
        attrSRs = [] 
        for col in df.columns():
            if(globalSR != 'NaN'):
                attrSRS.append(globalSR)# initializing attrSRs
            else:
                attrSRs.append(col)
        
        globalCat = 'FINANCE' # manually initialize globalCat and attrCats
        attrCats = 'Finance 1'
                
        newFeat = Feature(featName, attrNames, globalSR, attrSRS,
                     globalDataType, attrDataTypes, globalCat,
                     attrCats, DF)
        return DF
```

```python

```
