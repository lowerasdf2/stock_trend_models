---
title: 'Algo-Trading Documentation'
disqus: hackmd
---
Algo-Trading Documentation
===
<!-- ![downloads](https://img.shields.io/github/downloads/atom/atom/total.svg)
![build](https://img.shields.io/appveyor/ci/:user/:repo.svg)
![chat](https://img.shields.io/discord/:serverId.svg) -->

## Table of Contents

[TOC]
General Resources
===
1. 

Data Collection/Storage/Loading
===
1. Spreadsheet for tracking progress collecting & storing Features on: https://docs.google.com/spreadsheets/d/1EHw0cSXdYZMeRqklsjQ5XOztGJqeaKopK1dRiGI2soQ/edit#gid=0

2. Collecting data live
* Master branch will contain scripts that are live and storing data on Influx
* 


Code Utilities
===


Classes
---
### Feature Class

Functions
---
### load_data


Modeling
===

Misc
===
1. README HackMD edit link: https://hackmd.io/xNlik7ojSc2t3_fq8omLMA?both
