import tensorflow as tf
from tensorflow.contrib import rnn
import pandas as pd
import numpy as np
np.random.seed(1335)  # for reproducibility
from sklearn.preprocessing import MinMaxScaler
import os
import csv
import random
import time
import backtest as twp
from matplotlib import pyplot as plt
from sklearn import metrics, preprocessing
import timeit
from keras.models import Sequential
from keras.layers.core import Dense, Dropout, Activation
from keras.layers.recurrent import LSTM
from keras.optimizers import RMSprop, Adam, SGD

epochs2 = 10
n_classes = 2
target_stock = "ALDX" 
start_funds = 1000     # make sure same value in backtest.py

adam_learning_rate = 0.015

signalType = "capital"
gamma = 0.0
epsilon_arg = 0.2

def get_stock_data():

    data = pd.read_csv('/Users/alexvesel/Documents/UWMadison/AIStocks/dataFiles/yahooStockData/%sYahoo.csv' % (target_stock), header=0)
    data = data.add_suffix('_historical').add_prefix("%s_" % (target_stock))

    stockTicker = "%s_Close_historical" % target_stock

    first_index = data[stockTicker].first_valid_index()
    data = data.iloc[first_index:].reset_index().drop(['index'], axis=1)
    close_price = data[stockTicker].values

    # Drop date variables
    data = data.drop(data.columns[0], axis=1)

    data = data.diff().fillna(0)

    binary_class = []
    i = 0
    while i < len(data[stockTicker]):
        if (data[stockTicker][i] >= 0):
            binary_class.append([1, 0])
        else:
            binary_class.append([0, 1])
        i += 1

    binary_class.pop(0)
    binary_class.append([0, 1])
    
    scaler = MinMaxScaler()
    for column in data:
        if (column[-17:] == 'Volume_historical'):
            float_array = data[column].values.astype(float)
            scaled_array = scaler.fit_transform(float_array.reshape(-1, 1))
            data[column] = scaled_array

    # Dimensions of dataset
    n = data.shape[0]
    p = data.shape[1]

    n_stocks = p

    # Make data a numpy array
    data = data.values

    # Training and test data
    train_start = 0
    train_end = int(np.floor(0.8*n))
    test_start = train_end
    test_end = n
    data_train = data[np.arange(train_start, train_end), :]
    data_test = data[np.arange(test_start, test_end), :]
    close_price_train = close_price[train_start:train_end]
    close_price_test = close_price[test_start:test_end]

    # Build X and y
    X_train = data_train[:, :]
    y_train = binary_class[:train_end]
    X_test = data_test[:, :]
    y_test = binary_class[test_start:test_end]

    print('Total data length:', n)
    
    return X_train, y_train, X_test, y_test, n_stocks, close_price_train, close_price_test


def run_RLearning_network(z):
    # Hidden layer
    hidden_1 = tf.nn.sigmoid(tf.add(tf.matmul(z, W_hidden_1), bias_hidden_1))

    """hidden_2 = tf.nn.sigmoid(tf.add(tf.matmul(hidden_1, W_hidden_2), bias_hidden_2))
    hidden_3 = tf.nn.sigmoid(tf.add(tf.matmul(hidden_2, W_hidden_3), bias_hidden_3))
    hidden_4 = tf.nn.sigmoid(tf.add(tf.matmul(hidden_3, W_hidden_4), bias_hidden_4))"""

    # Output layer (must be transposed)
    out = tf.add(tf.matmul(hidden_1, W_out), bias_out)

    return out

def train_RLearning(z, m):
    global X_train, y_train, X_test, y_test, n_stocks, close_price_train, close_price_test, epsilon

    tv = tf.trainable_variables()

    output = run_RLearning_network(z)
    mse = tf.reduce_mean(tf.squared_difference(output, m))
    #opt = tf.train.GradientDescentOptimizer(0.5).minimize(mse)
    opt = tf.train.AdamOptimizer(learning_rate=adam_learning_rate).minimize(mse)

    soft_max = tf.nn.softmax(m)

    indata = np.asarray(y_train)
    learning_progress = []
    learning_output = np.zeros((1,3))
    h = 0
    #signal = pd.Series(index=np.arange(len(indata)))
    epsilon = epsilon_arg

    with tf.Session() as sess:

        sess.run(tf.global_variables_initializer())
        for i in range(epochs2):
            l = 0
            total_reward = 0
            signal = pd.Series(index=np.arange(len(indata)))
            state, xdata = init_state(indata)
            status = 1
            terminal_state = 0
            time_step = 0
            #while learning is still in progress
            while(status == 1):
                print(l)
                l += 1
                #We start in state S
                #Run the Q function on S to get predicted reward values on all the possible actions
                start_time = timeit.default_timer()
                qval = sess.run(output, feed_dict={z: state})
                #qval_soft = tf.nn.softmax(qval).eval()
                qval_soft = sess.run(soft_max, feed_dict={m: qval})
                elapsed = np.round(timeit.default_timer() - start_time, decimals=2)
                print(elapsed)
                if (random.random() < epsilon) and i != epochs2 - 1: #maybe choose random action if not the last epoch
                    action = np.random.randint(0,3) #assumes 3 different actions
                else: #choose best action from Q(s,a) values
                    action = (np.argmax(qval))
                #Take action, observe new state S'
                bt = twp.Backtest(pd.Series(data=[x for x in close_price_train]), signal, signalType=signalType)
                new_state, time_step, signal, terminal_state = take_action(state, xdata, qval_soft, action, signal, time_step, bt)
                #Observe reward
                reward = get_reward(new_state, time_step, xdata, signal, terminal_state, bt, i)
                total_reward += reward
                if terminal_state == 0: #non-terminal state
                    #Get max_Q(S',a)
                    newQ = sess.run(output, feed_dict={z: new_state})
                    maxQ = np.max(newQ)
                    learning_output[:] = qval[:]
                    update = (reward + (gamma * maxQ))
                    learning_output[0][action] = update #target output
                    sess.run(opt, feed_dict={z: state, m: learning_output})
                    state = new_state
                else: #terminal state (means that it is the last state)
                    update = reward
                """tvars = tf.trainable_variables()
                tvars_vals = sess.run(tvars)
                for var, val in zip(tvars, tvars_vals):
                    if(var.name == 'W_hidden_1:0'):
                        print(var.name, val)""" 
                if terminal_state == 1: #terminal state
                    bt = twp.Backtest(pd.Series(data=[x for x in close_price_train]), signal, signalType=signalType)
                    bt.data['delta'] = bt.data['shares'].diff().fillna(0)

                    plot = plt.figure()
                    plt.subplot(3,1,1)
                    bt.plotTrades()
                    plt.subplot(3,1,2)
                    bt.pnl.plot(style='x-')
                    plt.subplot(3,1,3)
                    plt.plot(learning_progress)
                    plt.savefig('%d.png' % (i), bbox_inches='tight', pad_inches=1, dpi=72)

                    status = 0

            eval_reward = evaluate_Q(y_test, sess, output, i)
            print("Epoch #: %s Reward: %f Epsilon: %f" % (i, eval_reward, epsilon))
            learning_progress.append((eval_reward))
            if epsilon > 0.1:
                epsilon -= (1.0/epochs2)


def init_state(data):
    xdata = data
    
    state = xdata[0:1]

    return state, xdata      

def take_action(state, xdata, qval, action, signal, time_step, bt):
    #if the current iteration is the last state ("terminal state") then set terminal_state to 1
    if (time_step == xdata.shape[0] - 1):
        state = xdata[time_step : time_step + 1]
        #state = np.append(state, cash_eff)
        terminal_state = 1
        signal.loc[time_step] = 0
        return state, time_step, signal, terminal_state

    cash_eff = bt.data['cash'].iloc[time_step]
    if (bt.data['value'].iloc[time_step] < 0):
        cash_eff += bt.data['value'].iloc[time_step]

    """if state == 1:
        signal.loc[time_step] = 1000
    if state == -1:
        signal.loc[time_step] = -1000"""

    #take action
    if action == 0:
        """signal.loc[time_step] = abs((qval[0, 0] - qval[0, 1]) * cash_eff)
        if abs(signal.loc[time_step]) > cash_eff:
            signal.loc[time_step] = cash_eff"""
        #signal.loc[time_step] = cash_eff * qval[0, action]
        signal.loc[time_step] = 100
    elif action == 1:
        """signal.loc[time_step] = (-1 * abs((qval[0, 1] - qval[0, 0]) * cash_eff))
        if abs(signal.loc[time_step]) > cash_eff:
            signal.loc[time_step] = -1 * cash_eff"""
        #signal.loc[time_step] = -1 * cash_eff * qval[0, action]
        signal.loc[time_step] = -100
    elif action == 2:
        signal.loc[time_step] = 0
    #signal.loc[time_step] = qval[0, np.argmax(qval)] * 500

    time_step += 1

    #move the market data window one step forward
    state = xdata[time_step : time_step + 1]
    #state = np.append(state, cash_eff)
    terminal_state = 0

    return state, time_step, signal, terminal_state

def get_reward(new_state, time_step, xdata, signal, terminal_state, bt, epoch=0):
    reward = 0
    cash_eff = bt.data['cash'].iloc[time_step - 1]
    if (bt.data['value'].iloc[time_step - 1] < 0):
        cash_eff += bt.data['value'].iloc[time_step - 1]
    signal.fillna(value=0, inplace=True)

    #log_return = ln(bt.data['shares'].iloc[time_step - 1])

    #if terminal_state == 0:
    if (time_step >= 0):
        #reward = (bt.pnl.iloc[time_step - 1] - bt.pnl.iloc[time_step - 2]) / start_funds
        """reward = (bt.pnl.iloc[time_step - 1] - bt.pnl.iloc[time_step - 2])
        if (bt.pnl.iloc[time_step - 1] == bt.pnl.iloc[time_step - 2]):
            reward -= 10"""
        #if (bt.pnl.iloc[time_step - 1] == bt.pnl.iloc[time_step - 2]):
        #    reward -= 10
        """if signal[time_step - 1] == 1000:
            reward += 1000
        if signal[time_step - 1] == -1000:
            reward -= 1000"""
        #reward = (bt.data['cash'].iloc[time_step - 1])
        if (bt.pnl.iloc[time_step - 1] > bt.pnl.iloc[time_step - 2] ):
            #reward += 2 * bt.pnl.iloc[time_step] - bt.pnl.iloc[time_step - 1]
            reward += 0.5
        if (bt.pnl.iloc[time_step - 1] < bt.pnl.iloc[time_step - 2] ):
            #reward += bt.pnl.iloc[time_step] - bt.pnl.iloc[time_step - 1]
            reward -= 1.5
        if (bt.pnl.iloc[time_step - 1] == bt.pnl.iloc[time_step - 2] ):
            reward -= 1
            
    return reward

def evaluate_Q(eval_data, sess, output, e):
    #This function is used to evaluate the perofrmance of the system each epoch, without the influence of epsilon and random actions
    signal = pd.Series(index=np.arange(len(eval_data)))
    eval_data = np.asarray(eval_data)
    state, xdata = init_state(eval_data)
    status = 1
    terminal_state = 0
    time_step = 0
    total_reward = 0
    while(status == 1):
        #We start in state S
        #Run the Q function on S to get predicted reward values on all the possible actions
        qval = sess.run(output, feed_dict={z: state})
        print(qval)
        action = (np.argmax(qval))
        #Take action, observe new state S'
        bt = twp.Backtest(pd.Series(data=[x for x in close_price_test]), signal, signalType=signalType)
        new_state, time_step, signal, terminal_state = take_action(state, xdata, qval, action, signal, time_step, bt)
        #Observe reward
        eval_reward = get_reward(new_state, time_step, xdata, signal, terminal_state, bt, e)
        total_reward += eval_reward
        state = new_state
        if terminal_state == 1: #terminal state
            status = 0
            #print_keras_weights()
            bt = twp.Backtest(pd.Series(data=[x for x in close_price_test]), signal, signalType=signalType)
            bt.data['delta'] = bt.data['shares'].diff().fillna(0)

            plot = plt.figure()
            plt.subplot(3,1,1)
            bt.plotTrades()
            plt.subplot(3,1,2)
            bt.pnl.plot(style='x-')
            plt.subplot(3,1,3)
            plt.savefig('Final%s.png' % (e), bbox_inches='tight', pad_inches=1, dpi=72)
    if (e == epochs2 - 1):
        #plot results
        bt = twp.Backtest(pd.Series(data=[x for x in close_price_train]), signal, signalType=signalType)
        bt.data['delta'] = bt.data['shares'].diff().fillna(0)

        print(bt.data)

        plt.show()
    return total_reward



X_train, y_train, X_test, y_test, n_stocks, close_price_train, close_price_test = get_stock_data()

print('Target Stock:', target_stock)

z = tf.placeholder('float', shape=(None, 2))
m = tf.placeholder('float', shape=(1, 3))

sigma = 1
weight_initializer = tf.variance_scaling_initializer(mode="fan_avg", distribution="uniform", scale=sigma)
#bias_initializer = tf.variance_scaling_initializer(mode="fan_avg", distribution="uniform", scale=sigma)
bias_initializer = tf.zeros_initializer()

# Model architecture parameters
n_stocks = 2
n_neurons_1 = 2
n_neurons_2 = 4
n_neurons_3 = 4
n_neurons_4 = 4
n_target = 3
# Layer 1: Variables for hidden weights and biases
W_hidden_1 = tf.Variable(weight_initializer([n_stocks, n_neurons_1]), name='W_hidden_1')
bias_hidden_1 = tf.Variable(bias_initializer([n_neurons_1]), name='bias_hidden_1')
# Layer 2: Variables for hidden weights and biases
W_hidden_2 = tf.Variable(weight_initializer([n_neurons_1, n_neurons_2]), name='W_hidden_2')
bias_hidden_2 = tf.Variable(bias_initializer([n_neurons_2]), name='bias_hidden_2')
# Layer 3: Variables for hidden weights and biases
W_hidden_3 = tf.Variable(weight_initializer([n_neurons_2, n_neurons_3]))
bias_hidden_3 = tf.Variable(bias_initializer([n_neurons_3]))
# Layer 4: Variables for hidden weights and biases
W_hidden_4 = tf.Variable(weight_initializer([n_neurons_3, n_neurons_4]))
bias_hidden_4 = tf.Variable(bias_initializer([n_neurons_4]))

# Output layer: Variables for output weights and biases
W_out = tf.Variable(weight_initializer([n_neurons_1, n_target]), name='W_out')
bias_out = tf.Variable(bias_initializer([n_target]), name='bias_out')

train_RLearning(z, m)
