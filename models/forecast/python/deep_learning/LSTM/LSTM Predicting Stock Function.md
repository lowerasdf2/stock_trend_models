
# LSTM Predicting Stock Function
Function that takes: <br>
<li> precondition: all data has been formatted correctly <br>
<li> input: data & hyperparams setup <br>
<li> output: tensor with row test_set, column time_step (according to the input) <br>  
    
[Source](https://www.datacamp.com/community/tutorials/lstm-python-stock-market)


```python
from pandas_datareader import data
import numpy as np
import tensorflow as tf
from sklearn.preprocessing import MinMaxScaler
import copy as copy
import matplotlib.pyplot as plt
import pandas as pd
import datetime as dt
import urllib.request, json 
import os
```


```python
def LSTM(data, D = 1, num_unrollings = 50, batch_size = 500, num_nodes = [200,200,150], dropout = 0.0, epochs = 30, valid_summary = 1, n_predict_once = 50):
    tf.reset_default_graph()
    n_layers = len(num_nodes)
    train_data, test_data, all_mid_data, trainSize, maxDataLen = process_data(data)
    
    dg = DataGeneratorSeq(train_data,5,5)
    u_data, u_labels = dg.unroll_batches()
    
    train_inputs, train_outputs = [],[]

    for ui in range(num_unrollings):
        train_inputs.append(tf.placeholder(tf.float32, shape=[batch_size,D],name='train_inputs_%d'%ui))
        train_outputs.append(tf.placeholder(tf.float32, shape=[batch_size,1], name = 'train_outputs_%d'%ui))
    
    lstm_cells = [tf.contrib.rnn.LSTMCell(num_units=num_nodes[li], state_is_tuple=True, initializer= tf.contrib.layers.xavier_initializer()) for li in range(n_layers)]

    drop_lstm_cells = [tf.contrib.rnn.DropoutWrapper(lstm, input_keep_prob=1.0,output_keep_prob=1.0-dropout, state_keep_prob=1.0-dropout) for lstm in lstm_cells]
    drop_multi_cell = tf.contrib.rnn.MultiRNNCell(drop_lstm_cells)
    multi_cell = tf.contrib.rnn.MultiRNNCell(lstm_cells)

    w = tf.get_variable('w',shape=[num_nodes[-1], 1], initializer=tf.contrib.layers.xavier_initializer())
    b = tf.get_variable('b',initializer=tf.random_uniform([1],-0.1,0.1))
    
    c, h = [],[]
    initial_state = []
    
    for li in range(n_layers):
      c.append(tf.Variable(tf.zeros([batch_size, num_nodes[li]]), trainable=False))
      h.append(tf.Variable(tf.zeros([batch_size, num_nodes[li]]), trainable=False))
      initial_state.append(tf.contrib.rnn.LSTMStateTuple(c[li], h[li]))

    all_inputs = tf.concat([tf.expand_dims(t,0) for t in train_inputs],axis=0)

    all_lstm_outputs, state = tf.nn.dynamic_rnn(
        drop_multi_cell, all_inputs, initial_state=tuple(initial_state),
        time_major = True, dtype=tf.float32)

    all_lstm_outputs = tf.reshape(all_lstm_outputs, [batch_size*num_unrollings,num_nodes[-1]])

    all_outputs = tf.nn.xw_plus_b(all_lstm_outputs,w,b)

    split_outputs = tf.split(all_outputs,num_unrollings,axis=0)
    
    loss = 0.0
    with tf.control_dependencies([tf.assign(c[li], state[li][0]) for li in range(n_layers)]+
                                 [tf.assign(h[li], state[li][1]) for li in range(n_layers)]):
      for ui in range(num_unrollings):
        loss += tf.reduce_mean(0.5*(split_outputs[ui]-train_outputs[ui])**2)

    global_step = tf.Variable(0, trainable=False)
    inc_gstep = tf.assign(global_step,global_step + 1) #to increment global_step
    tf_learning_rate = tf.placeholder(shape=None,dtype=tf.float32)
    tf_min_learning_rate = tf.placeholder(shape=None,dtype=tf.float32)

    learning_rate = tf.maximum(
        tf.train.exponential_decay(tf_learning_rate, global_step, decay_steps=1, decay_rate=0.5, staircase=True),
        tf_min_learning_rate)

    optimizer = tf.train.AdamOptimizer(learning_rate)
    gradients, v = zip(*optimizer.compute_gradients(loss))
    gradients, _ = tf.clip_by_global_norm(gradients, 5.0)
    optimizer = optimizer.apply_gradients(zip(gradients, v))

    sample_inputs = tf.placeholder(tf.float32, shape=[1,D])

    sample_c, sample_h, initial_sample_state = [],[],[]
    for li in range(n_layers):
      sample_c.append(tf.Variable(tf.zeros([1, num_nodes[li]]), trainable=False))
      sample_h.append(tf.Variable(tf.zeros([1, num_nodes[li]]), trainable=False))
      initial_sample_state.append(tf.contrib.rnn.LSTMStateTuple(sample_c[li],sample_h[li]))

    reset_sample_states = tf.group(*[tf.assign(sample_c[li],tf.zeros([1, num_nodes[li]])) for li in range(n_layers)],
                                   *[tf.assign(sample_h[li],tf.zeros([1, num_nodes[li]])) for li in range(n_layers)])

    sample_outputs, sample_state = tf.nn.dynamic_rnn(multi_cell, tf.expand_dims(sample_inputs,0),
                                       initial_state=tuple(initial_sample_state),
                                       time_major = True,
                                       dtype=tf.float32)

    with tf.control_dependencies([tf.assign(sample_c[li],sample_state[li][0]) for li in range(n_layers)]+
                                  [tf.assign(sample_h[li],sample_state[li][1]) for li in range(n_layers)]):  
      sample_prediction = tf.nn.xw_plus_b(tf.reshape(sample_outputs,[1,-1]), w, b)
    
    train_seq_length = train_data.size # Full length of the training data

    train_mse_ot = [] # Accumulate Train losses
    test_mse_ot = [] # Accumulate Test loss
    predictions_over_time = [] # Accumulate predictions

    session = tf.InteractiveSession()

    tf.global_variables_initializer().run()

    # Used for decaying learning rate
    loss_nondecrease_count = 0
    loss_nondecrease_threshold = 2 # If the test error hasn't increased in this many steps, decrease learning rate

    average_loss = 0

    # Define data generator
    data_gen = DataGeneratorSeq(train_data,batch_size,num_unrollings) 

    x_axis_seq = []

    # Points you start our test predictions from
    test_points_seq = np.arange(trainSize,maxDataLen-n_predict_once,n_predict_once).tolist() 
    
    for ep in range(epochs):       

        # ========================= Training =====================================
        for step in range(train_seq_length//batch_size):

            u_data, u_labels = data_gen.unroll_batches()

            feed_dict = {}
            for ui,(dat,lbl) in enumerate(zip(u_data,u_labels)):            
                feed_dict[train_inputs[ui]] = dat.reshape(-1,1)
                feed_dict[train_outputs[ui]] = lbl.reshape(-1,1)

            feed_dict.update({tf_learning_rate: 0.0001, tf_min_learning_rate:0.000001})

            _, l = session.run([optimizer, loss], feed_dict=feed_dict)

            average_loss += l

        # ============================ Validation ==============================
        if (ep+1) % valid_summary == 0:

          average_loss = average_loss/(valid_summary*(train_seq_length//batch_size))

          # The average loss
          if (ep+1)%valid_summary==0:
            print('Average loss at step %d: %f' % (ep+1, average_loss))

          train_mse_ot.append(average_loss)

          average_loss = 0 # reset loss

          predictions_seq = []

          mse_test_loss_seq = []

          # ===================== Updating State and Making Predicitons ========================
          for w_i in test_points_seq:
            mse_test_loss = 0.0
            our_predictions = []

            if (ep+1)-valid_summary==0:
              x_axis=[]

            feed_dict = {}

            current_price = all_mid_data[w_i-1]

            feed_dict[sample_inputs] = np.array(current_price).reshape(1,1)

            for pred_i in range(n_predict_once):

              pred = session.run(sample_prediction,feed_dict=feed_dict)

              our_predictions.append(np.asscalar(pred))

              feed_dict[sample_inputs] = np.asarray(pred).reshape(-1,1)

              if (ep+1)-valid_summary==0:
                x_axis.append(w_i+pred_i)

              mse_test_loss += 0.5*(pred-all_mid_data[w_i+pred_i])**2

            session.run(reset_sample_states)

            predictions_seq.append(np.array(our_predictions))

            mse_test_loss /= n_predict_once
            mse_test_loss_seq.append(mse_test_loss)

            if (ep+1)-valid_summary==0:
              x_axis_seq.append(x_axis)

          current_test_mse = np.mean(mse_test_loss_seq)

          if len(test_mse_ot)>0 and current_test_mse > min(test_mse_ot):
              loss_nondecrease_count += 1
          else:
              loss_nondecrease_count = 0

          if loss_nondecrease_count > loss_nondecrease_threshold :
                session.run(inc_gstep)
                loss_nondecrease_count = 0
                print('\tDecreasing learning rate by 0.5')

          test_mse_ot.append(current_test_mse)
          print('\tTest MSE: %.5f'%np.mean(mse_test_loss_seq))
          predictions_over_time.append(predictions_seq)
          print('\tFinished Predictions')
    
    best_epoch = test_mse_ot.index(min(test_mse_ot))    
    return predictions_over_time[best_epoch]
```

## Data Generator


```python
class DataGeneratorSeq(object):
    
    def __init__(self,prices,batch_size,num_unroll):
        self._prices = prices
        self._prices_length = len(self._prices) - num_unroll
        self._batch_size = batch_size
        self._num_unroll = num_unroll
        self._segments = self._prices_length //self._batch_size
        self._cursor = [offset * self._segments for offset in range(self._batch_size)]

    def next_batch(self):
        
        batch_data = np.zeros((self._batch_size),dtype=np.float32)
        batch_labels = np.zeros((self._batch_size),dtype=np.float32)
        
        for b in range(self._batch_size):
            if self._cursor[b]+1>=self._prices_length:
                #self._cursor[b] = b * self._segments
                self._cursor[b] = np.random.randint(0,(b+1)*self._segments)
                
            batch_data[b] = self._prices[self._cursor[b]]
            batch_labels[b]= self._prices[self._cursor[b]+np.random.randint(1,5)]
            
            self._cursor[b] = (self._cursor[b]+1)%self._prices_length
            
        return batch_data,batch_labels
    
    def unroll_batches(self):
            
        unroll_data,unroll_labels = [],[]
        init_data, init_label = None,None
        for ui in range(self._num_unroll):
            
            data, labels = self.next_batch()    

            unroll_data.append(data)
            unroll_labels.append(labels)

        return unroll_data, unroll_labels
    
    def reset_indices(self):
        for b in range(self._batch_size):
            self._cursor[b] = np.random.randint(0,min((b+1)*self._segments,self._prices_length-1))
```

## Data Preprocessing


```python
def process_data(data, smoothing_window_size = 100, EMA = 0.0, gamma = 0.1):
    high_prices = df.loc[:,'High'].as_matrix()
    low_prices = df.loc[:,'Low'].as_matrix()
    mid_prices = (high_prices+low_prices)/2.0
    
    maxDataLen = mid_prices.shape[0]
    
    trainSize=round(high_prices.shape[0]*.7)
    train_data = mid_prices[:trainSize]
    test_data = mid_prices[trainSize:]
    
    scaler = MinMaxScaler()
    train_data = train_data.reshape(-1,1)
    orig_train_data=copy.deepcopy(train_data);
    test_data = test_data.reshape(-1,1)
    
    for di in range(0,train_data.shape[0],smoothing_window_size):
        scaler.fit(train_data[di:di+smoothing_window_size,:])
        train_data[di:di+smoothing_window_size,:] = scaler.transform(train_data[di:di+smoothing_window_size,:])

    if di+smoothing_window_size < train_data.shape[0]:
        scaler.fit(train_data[di+smoothing_window_size:,:])
        train_data[di+smoothing_window_size:,:] = scaler.transform(train_data[di+smoothing_window_size:,:])
        
    scaler.fit(orig_train_data)
    
    train_data = train_data.reshape(-1)
    test_data = scaler.transform(test_data).reshape(-1)
    
    for ti in range(trainSize):
      EMA = gamma*train_data[ti] + (1-gamma)*EMA
      train_data[ti] = EMA

    all_mid_data = np.concatenate([train_data,test_data],axis=0)
    
    return train_data, test_data, all_mid_data, trainSize, maxDataLen
```

## For Testing Purpose
Note that the output of the function is a list of shape [len(test_data) / n_predict_once][n_predict_once]


```python
file = "data/stock_market_data-AAL.csv";
df = pd.read_csv(file)
prediction = LSTM(df, epochs = 2)
```

    /Users/bloomestjansenchandra/anaconda3/lib/python3.6/site-packages/ipykernel_launcher.py:2: FutureWarning: Method .as_matrix will be removed in a future version. Use .values instead.
      
    /Users/bloomestjansenchandra/anaconda3/lib/python3.6/site-packages/ipykernel_launcher.py:3: FutureWarning: Method .as_matrix will be removed in a future version. Use .values instead.
      This is separate from the ipykernel package so we can avoid doing imports until
    /Users/bloomestjansenchandra/anaconda3/lib/python3.6/site-packages/tensorflow/python/client/session.py:1645: UserWarning: An interactive session is already active. This can cause out-of-memory errors in some cases. You must explicitly call `InteractiveSession.close()` to release resources held by the other session(s).
      warnings.warn('An interactive session is already active. This can '


    Average loss at step 1: 7.281637
    	Test MSE: 0.12978
    	Finished Predictions
    Average loss at step 2: 5.318434
    	Test MSE: 0.10498
    	Finished Predictions



```python

```
