#!/usr/bin/env python
# coding: utf-8
# %%
# Authors: Alex Vessel, Chris Endemann

# Summary: Run Phased LSTM model from this article: https://towardsdatascience.com/using-lstms-for-stock-market-predictions-tensorflow-9e83999d4653

# Dependencies
#1. Install tradingWithPython: pip install tradingWithPython

# %%
import os
baseDataDir="C:\\Users\\qualiaMachine\\Google Drive\\AI_projects\\stock_trend_models\\data\\csv\\yahooStockData"
os.chdir(baseDataDir)

# %%
import tensorflow as tf

# %%
print(tf.__version__)

# %%
from tensorflow.contrib import rnn

# %%
import pandas as pd
import numpy as np
np.random.seed(1335)  # for reproducibility
from sklearn.preprocessing import MinMaxScaler
import os
import csv
import random
import time
#import backtest as twp
from matplotlib import pyplot as plt
from sklearn import metrics, preprocessing

epochs = 100
batch_size = 940
n_classes = 2
n_target = 3
rnn_size = 30
target_stock = "AMZN"  # ALDX

ratio_on = 0.75
use_peepholes = True

use_regularization = 0
regularization_type = 1  # 1 (lasso) or 2

use_Adam = 1
adam_learning_rate = 0.015

use_dropout = 1
output_keep_prob = 0.8

threshold = 0.1

print_tv = 0

def get_stock_data():

    data = pd.read_csv('AAPLYahoo.csv')
    data = data.add_suffix('_historical').add_prefix("%s_" % (target_stock))

    stockTicker = "%s_Close_historical" % target_stock

    first_index = data[stockTicker].first_valid_index()
    data = data.iloc[first_index:].reset_index().drop(['index'], axis=1)
    close_price = data[stockTicker].values

    # Drop date variables
    data = data.drop(data.columns[0], axis=1)

    data = data.diff().fillna(0)

    binary_class = []
    i = 0
    while i < len(data[stockTicker]):
        if (data[stockTicker][i] >= 0):
            binary_class.append([1, 0])
        else:
            binary_class.append([0, 1])
        i += 1

    binary_class.pop(0)
    binary_class.append([0, 1])

    scaler = MinMaxScaler()
    for column in data:
        if (column[-17:] == 'Volume_historical'):
            float_array = data[column].values.astype(float)
            scaled_array = scaler.fit_transform(float_array.reshape(-1, 1))
            data[column] = scaled_array

    # Dimensions of dataset
    n = data.shape[0]
    p = data.shape[1]
    
    n_stocks = p

    # Make data a numpy array
    data = data.values

    # Training and test data
    train_start = 0
    train_end = int(np.floor(0.8*n))
    test_start = train_end
    test_end = n
    data_train = data[np.arange(train_start, train_end), :]
    data_test = data[np.arange(test_start, test_end), :]
    close_price_train = close_price[train_start:train_end]
    close_price_test = close_price[test_start:test_end]

    # Build X and y
    X_train = data_train[:, :]
    y_train = binary_class[:train_end]
    X_test = data_test[:, :]
    y_test = binary_class[test_start:test_end]

    print('Total data length:', n)
    
    return X_train, y_train, X_test, y_test, n_stocks, close_price_train, close_price_test

def FNN(x):
    sigma = 1
    weight_initializer = tf.variance_scaling_initializer(mode="fan_avg", distribution="uniform", scale=sigma)
    #bias_initializer = tf.variance_scaling_initializer(mode="fan_avg", distribution="uniform", scale=sigma)
    bias_initializer = tf.zeros_initializer()

    # Model architecture parameters
    n_neurons_1 = 2
    n_neurons_2 = 4
    n_neurons_3 = 4
    n_neurons_4 = 4
    
    # Layer 1: Variables for hidden weights and biases
    W_hidden_1 = tf.Variable(weight_initializer([n_stocks, n_neurons_1]), name='W_hidden_1')
    bias_hidden_1 = tf.Variable(bias_initializer([n_neurons_1]), name='bias_hidden_1')
    # Layer 2: Variables for hidden weights and biases
    W_hidden_2 = tf.Variable(weight_initializer([n_neurons_1, n_neurons_2]), name='W_hidden_2')
    bias_hidden_2 = tf.Variable(bias_initializer([n_neurons_2]), name='bias_hidden_2')
    # Layer 3: Variables for hidden weights and biases
    W_hidden_3 = tf.Variable(weight_initializer([n_neurons_2, n_neurons_3]))
    bias_hidden_3 = tf.Variable(bias_initializer([n_neurons_3]))
    # Layer 4: Variables for hidden weights and biases
    W_hidden_4 = tf.Variable(weight_initializer([n_neurons_3, n_neurons_4]))
    bias_hidden_4 = tf.Variable(bias_initializer([n_neurons_4]))

    # Output layer: Variables for output weights and biases
    W_out = tf.Variable(weight_initializer([n_neurons_4, n_target]), name='W_out')
    bias_out = tf.Variable(bias_initializer([n_target]), name='bias_out')

    # Hidden layer
    hidden_1 = tf.nn.sigmoid(tf.add(tf.matmul(x, W_hidden_1), bias_hidden_1))

    hidden_2 = tf.nn.sigmoid(tf.add(tf.matmul(hidden_1, W_hidden_2), bias_hidden_2))
    hidden_3 = tf.nn.sigmoid(tf.add(tf.matmul(hidden_2, W_hidden_3), bias_hidden_3))
    hidden_4 = tf.nn.sigmoid(tf.add(tf.matmul(hidden_3, W_hidden_4), bias_hidden_4))

    # Output layer (must be transposed)
    out = tf.add(tf.matmul(hidden_4, W_out), bias_out)

    return out

def recurrent_neural_network(l):
    sigma = 1
    weight_initializer = tf.variance_scaling_initializer(mode="fan_avg", distribution="uniform", scale=sigma)
    #weight_initializer = tf.ones_initializer()
    bias_initializer = tf.zeros_initializer()
    
    input_weight = tf.Variable(weight_initializer([1, n_stocks]))
    
    layer = {'weights':tf.Variable(weight_initializer([rnn_size, n_classes])),
              'biases':tf.Variable(bias_initializer([n_classes]))}     
    #xw = tf.multiply(x, input_weight)
    xw = l
    #print(xw.shape) #?,6
    #print(input_weight.shape) #1,6
    #print(x.shape) #?,1
    xw = tf.reshape(xw, [-1, n_target, 1]) 

    lstm = rnn.PhasedLSTMCell(rnn_size, ratio_on=ratio_on, use_peepholes=use_peepholes)

    if (use_dropout):
        lstm = rnn.DropoutWrapper(lstm, output_keep_prob=output_keep_prob)

    outputs, states = tf.nn.dynamic_rnn(lstm, (t, xw), dtype=tf.float32)

    outputs = tf.reshape(outputs, shape = [n_target, -1, rnn_size])

    output = tf.add(tf.matmul(outputs[-1], layer['weights']), layer['biases'])

    return output

def train_rnn(x):
    global X_train, y_train, X_test, y_test, n_stocks, close_price_train, close_price_test, epsilon

    # Regularization
    tv = tf.trainable_variables()
    
    outputFNN = FNN(x)
    outputRNN = recurrent_neural_network(outputFNN)

    if (regularization_type == 1):
        regularization_cost = tf.reduce_sum([ tf.nn.l1_loss(v) for v in tv ])
    elif (regularization_type == 2):
        regularization_cost = tf.reduce_sum([ tf.nn.l2_loss(v) for v in tv ])

    if (use_regularization):
        mse = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits_v2(logits=outputRNN, labels=y)) + regularization_cost
    else:
        mse = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits_v2(logits=outputRNN, labels=y))

    if (use_Adam):
        opt = tf.train.AdamOptimizer(learning_rate=adam_learning_rate).minimize(mse)
    else:
        opt = tf.train.GradientDescentOptimizer(0.5).minimize(mse)

    correct = tf.equal(tf.argmax(outputRNN, 1), tf.argmax(y, 1))
    accuracy = tf.reduce_mean(tf.cast(correct, 'float'))
    
    soft_max = tf.nn.softmax(l)
    
    with tf.Session() as sess:

        sess.run(tf.global_variables_initializer())

        t_train = np.reshape(np.tile(np.array(range(n_target)), (X_train.shape[0], 1)), (X_train.shape[0], n_target, 1))
        t_test = np.reshape(np.tile(np.array(range(n_target)), (X_test.shape[0], 1)), (X_test.shape[0], n_target, 1))

        for e in range(epochs):
            # Minibatch training    
            for i in range(0, len(y_train) // batch_size):
                start = i * batch_size
                batch_x = X_train[start:start + batch_size]
                batch_t = t_train[start:start + batch_size]
                batch_y = y_train[start:start + batch_size]
                #print("test")
                #print(sess.run(output1, feed_dict={x: batch_x, y: batch_y, t: batch_t}).shape)
                # Run optimizer with batch
                tr_ce, tr_acc, _, predictions = sess.run([mse, accuracy, opt, outputRNN], feed_dict={x: batch_x, y: batch_y, t: batch_t})
        
            train_accuracy, train_predictions = sess.run([accuracy, outputRNN], feed_dict={x: X_train, y: y_train, t: t_train})
                        
            test_accuracy, test_predictions = sess.run([accuracy, outputRNN], feed_dict={x: X_test, y: y_test, t: t_test})
            test_predictions = sess.run(soft_max, feed_dict={l: test_predictions})
                        
            if (print_tv):
                tvars = tf.trainable_variables()
                tvars_vals = sess.run(tvars)
                for var, val in zip(tvars, tvars_vals):
                    print(var.name, val)  
            
            print(e, "\t Accuracy Test:", accuracy.eval({x:X_test, y:y_test, t: t_test}), "\t Accuracy Train:", train_accuracy, 
                "\t Thresh Test:", correct_above_threshold(y_test, test_predictions) , "CrosEnt Train", tr_ce)
            
def correct_above_threshold(batch_y, predictions):
    total_data_above_threshold = 0
    total_correct_above_threshold = 0
    soft_max = tf.nn.softmax(predictions).eval()

    i = 0
    while i < len(soft_max):
        diff = soft_max[i][0] - soft_max[i][1]
        if (abs(diff) >= threshold):
            total_data_above_threshold += 1
            if (batch_y[i] == [1, 0] and diff >= 0):
                total_correct_above_threshold += 1
            if (batch_y[i] == [0, 1] and diff < 0):
                total_correct_above_threshold += 1
        i += 1
    try:
        percent_correct = total_correct_above_threshold / total_data_above_threshold
        return(percent_correct, total_data_above_threshold)
    except:
        return("n/a")

def persistence_model(y_train, y_test):
    total_correct = 0
    i = 0
    len_y_train = len(y_train) - 1
    while i < len_y_train:
        if (y_train[i] == y_train[i+1]):
            total_correct += 1
        i += 1
    percent_correct = total_correct/len_y_train
    print("Train persistence accuracy:", percent_correct)

    total_correct = 0
    i = 0
    len_y_test = len(y_test) - 1
    while i < len_y_test:
        if (y_test[i] == y_test[i+1]):
            total_correct += 1
        i += 1
    percent_correct = total_correct/len_y_test
    print("Test persistence accuracy:", percent_correct)

def random_model(y_train, y_test):
    len_y_data = len(y_train)
    random_list = [0]
    for _ in range(0, len_y_data):
        k = random.randint(0, 1)
        if (k == 0):
            random_list.append([1, 0])
        else:
            random_list.append([0, 1])
    i = 0
    total_correct = 0
    while i < len_y_data:
        if (y_train[i] == random_list[i]):
            total_correct += 1
        i += 1 
    percent_correct = total_correct/len_y_data
    print("Train random accuracy:", percent_correct)

    len_y_data = len(y_test)
    random_list = [0]
    for _ in range(0, len_y_data):
        k = random.randint(0, 1)
        if (k == 0):
            random_list.append([1, 0])
        else:
            random_list.append([0, 1])
    i = 0
    total_correct = 0
    while i < len_y_data:
        if (y_test[i] == random_list[i]):
            total_correct += 1
        i += 1 
    percent_correct = total_correct/len_y_data
    print("Test random accuracy:", percent_correct)

X_train, y_train, X_test, y_test, n_stocks, close_price_train, close_price_test = get_stock_data()

persistence_model(y_train, y_test)
random_model(y_train, y_test)

print('Use regularization:', use_regularization, '\t Use Adam:', use_Adam)
print('Batch Size:', batch_size, '\t RNN Size:', rnn_size)
print('Target Stock:', target_stock)

x = tf.placeholder('float', shape=(None, n_stocks))
t = tf.placeholder('float', shape=(None, n_target, 1))
y = tf.placeholder('float')
l = tf.placeholder('float')

train_rnn(x)


# %%





# %%




