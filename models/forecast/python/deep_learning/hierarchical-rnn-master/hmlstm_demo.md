---
jupyter:
  jupytext:
    formats: ipynb,md
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.1'
      jupytext_version: 1.2.3
  kernelspec:
    display_name: Python 3
    language: python
    name: python3
---

```python
import numpy as np
import matplotlib.pyplot as plt
from hmlstm import HMLSTMNetwork, plot_indicators, prepare_inputs, get_text, convert_to_batches, viz_char_boundaries
import tensorflow as tf

%matplotlib inline
%load_ext autoreload
%autoreload 2
```

### Regression example

```python
# simulate multiresolution data
num_signals = 300
signal_length = 400
x = np.linspace(0, 50 * np.pi, signal_length)
signals = [np.random.normal(0, .5, size=signal_length) +
           (2 * np.sin(.6 * x + np.random.random() * 10)) +
           (5 * np.sin(.1* x + np.random.random() * 10))
    for _ in range(num_signals)]

split = int(num_signals * .8)
train = signals[:split]
test = signals[split:]
```

```python
# example signal
plt.plot(x, signals[5]);
```

```python
batches_in, batches_out = convert_to_batches(signals, batch_size=10)
```

```python
tf.reset_default_graph()
network = HMLSTMNetwork(input_size=1, task='regression', hidden_state_sizes=30,
                       embed_size=50, out_hidden_size=30, num_layers=2)
```

```python
# init = tf.global_variables_initializer()
# sess.run(init)
init = tf.initialize_all_variables()
sess = tf.Session()
sess.run(init)
```

```python
network.train(batches_in[:-1], batches_out[:-1], epochs=1, load_vars_from_disk=True,
              save_vars_to_disk=False, variable_path='./sinusoidal')
```

```python
boundaries = network.predict_boundaries(batches_in[-1])
print(boundaries)
```

```python
predictions = network.predict(batches_in[-1])
```

```python
plot_indicators(batches_out[-1][0], predictions[0], indicators=boundaries[0]);
```

### Text classificaiton example

```python
tf.reset_default_graph()
network = HMLSTMNetwork(output_size=27, input_size=27,
                        embed_size=2048, out_hidden_size=1024,
                        hidden_state_sizes=1024, 
                        task='classification')
```

```python
batches_in, batches_out = prepare_inputs(batch_size=2rt, truncate_len=1000, step_size=1000,
                                         text_path='text8.txt', num_batches=3000)
```

```python
start = 0
end = 20
```

```python
while end < len(batches_in):
    network.train(batches_in[start:end], batches_out[start:end], save_vars_to_disk=True, 
                  load_vars_from_disk=True, variable_path='./text8', epochs=1)
    start = end
    end += 20
```

```python
batch = -1
```

```python
truth = get_text(batches_out[batch][0])
predictions = network.predict(batches_in[batch], variable_path='./text8')
boundaries = network.predict_boundaries(batches_in[batch], variable_path='./text8')
viz_char_boundaries(truth, get_text(predictions[0]), indicators=boundaries[0])
```

```python

```
