function [condition_data, fsample, trial_times, trials] = load_condition_ecog_raw(args)
% Loads the raw data for a given patient and condition, along with some basic metadata. 
% Arguments:
% args.patientID    - (str) The ID of the patient to load, e.g. 'patient369LR'. 
% args.condition    - (str) The condition to load, e.g. 'OAAS5_1'.
% args.data_dir     - (str) The directory which contains the patientID/condition/
%                     subdirectory, which in turn contains the raw data. 
% args.ECoGchannels - [struct] As produced from ECoGChannelMap or load_batch_params.
% args.signalName   - (str) 
% args.dataPrefix   - (str) 
%
% Outputs:
% condition_data    - [nChans x nSamples] The raw data. 
% fsample           - The sampling rate of `condition_data`, in Hz. 
% trial_times       - [nTrials] The start time of each trial, in seconds. 
% trials            - trails.loclStd is an [n x 1] list of all the trail numbers 
%                     corresponding to local standard trails. Similarly for loclDev, globStd, globDev.

% Find the datafile for this condition. 
if args.rsOnly
    condition_dir = fullfile(args.data_dir, args.patientID, args.condition);
else
    condition_dir = fullfile(args.data_dir, args.patientID, 'LGD-OR', args.condition);
end
matfiles = dir(fullfile(condition_dir, '*.mat'));
if length(matfiles) ~= 1 
    warning('There should only be one datafile per condition.');
end
file = fullfile(condition_dir, matfiles.name);
% display(['Loading ' matfiles.name]);

% Get gain information. We assume it is the same across all files
% within a condition.
tmp = load(file,'gain');
gain = tmp.gain.(args.signalName);

% Get information used to map trial number to type and start time.
tmp = load(file,'FIDX');
trial_times = tmp.FIDX.time;
trials = struct;
if ~args.rsOnly
    [trials.loclStd, trials.globStd, ...
     trials.loclDev, trials.globDev] = get_LGDTrials(tmp.FIDX);
end

% Load data channel-by-channel.
nChannels = length(args.ECoGchannels); 
for i = 1 : nChannels
    % Strangely, ECoGchannels may not be indexed by channel number.
    chanNum=args.ECoGchannels(i).varID;
    dashInds=strfind(chanNum,'-');
    if isempty(dashInds)
        keyboard % CME added ROI_vec (all names) to varID (ch#) just as another sanity check for data storage procedure
    end
    chanNum=str2double(chanNum(1:dashInds(1)-1));
%     disp(['loading ch=' num2str(chanNum) ' for creation of condData.mat'])
    % Load only that channel's data.
    chanDataName = [args.dataPrefix num2str(chanNum, '%03d')];
    tmp = load(file, chanDataName);
    
    % Pre-allocate space for the data and save some channel-agnostic
    % parameters.
    if i == 1
        nPts = length(tmp.(chanDataName).dat); % number of samples
        condition_data = zeros(nChannels, nPts);
        fsample = tmp.(chanDataName).fs(1); % Hz
    end
    
    condition_data(i,:) = tmp.(chanDataName).dat;
    clear tmp
end

condition_data = condition_data * gain;

