function conditon_data = fieldtrip_continuous2trials(args, conditon_data)
% Segment the the continuous data from one condition into trials
% Arguments:
% condition_data: The FieldTrip-formatted continuous data.
% args.trial_times (nTrials x 1): the start time of each trial, in seconds.  
% args.trial_length (scalar): the duration of each trial, in seconds. 
% args.trials_by_type.loclStd (n x 1): all the trial numbers of local standard
%   trials. Similarly for loclDev, globStd, and globDev. 
% args.fifth_vowel (scalar): the time of 5th vowel onset relative to the start of the 
%   trial, in seconds. 
%
% Outputs:
% condition_data: The FieldTrip-formatted data, but trialed. 

% Before defining the trials, the entire recording is treated as one giant
% trial. The sampleinfo field tells FieldTrip which samples in a
% hypothetical "original" recording this giant trial corresponds to. 
nPts = length(conditon_data.time{1});
conditon_data.sampleinfo = [1, nPts];


% Convert trials to FieldTrip format
cfg         = args;
cfg.fsample = conditon_data.fsample;
[trl, trialinfo] = trials_iowa2fieldtrip(cfg);


% Fix rounding errors
% Due to rounding, a trial might be computed to extend past the end
% of the recording. Discard any such trials.
% TODO: Seems like there should be a better way to handle this.
trial_end_samples = trl(:, 2);
if any(trial_end_samples > nPts)
    warning('Rounding error. Discarding trial.')
    trials_to_keep = trial_end_samples <= nPts;
    trl = trl(trials_to_keep, :);
    trialinfo = trialinfo(trials_to_keep, :);
end

%% Segment the data into trials, based on the above.
cfg         = [];
cfg.trl     = trl;
conditon_data = ft_redefinetrial(cfg, conditon_data);
conditon_data.trialinfo = trialinfo;
        
end

