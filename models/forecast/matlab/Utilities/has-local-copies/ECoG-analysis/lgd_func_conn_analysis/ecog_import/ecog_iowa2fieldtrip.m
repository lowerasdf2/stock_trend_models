function dataFT = ecog_iowa2fieldtrip(args, data)
% Converts ECoG data (e.g. loaded by loadConditionECoG) into a 
% FieldTrip-compatible format. Does not segment the data into trials. 
%
% Arguments: 
% args.ECoGchannels - As produced by ECoGChannelMap or load_batch_params. 
% args.fsample      - The sampling rate of `data`, in Hz. 
% data              - An nChans x nPts matrix
%
% Outputs:
% dataFT            - dataFT.label is a 1xnChans cell-array containing string
%                     labels for each channel
%                   - dataFT.trial{1} the Nchan*Nsamples matrix passed as `data`.
%                   - dataFT.time{1} contains a time axis for the whole recording


[nChans,nPts] = size(data);

% Create informative, human-readable labels for each channel. 
for iChan = 1:nChans
    dataFT.label{iChan} = make_channel_label(iChan,args.ECoGchannels);
end


% Treat all the data as one giant trial. Will be fixed (segmented) later.  
dataFT.trial{1} = data;  
dataFT.fsample = args.fsample; % sampling frequency in Hz
dataFT.time{1} = (0:nPts-1) ./ dataFT.fsample; % One time point per sample, in seconds. 

