function [trl, trialinfo] = trials_iowa2fieldtrip(args)
% Arguments:
%   trial_times (nTrials x 1): the start time of each trial, in seconds.  
%   trial_length (scalar): - the duration of each trial, in seconds. 
%   trials_by_type.loclStd (n x 1): all the trial numbers of local standard
%       trials. Similarly for loclDev, globStd, and globDev. 
%   args.fifth_vowel (scalar): the time of 5th vowel onset relative to the start of the 
%       trial, in seconds. 
%   fsample (scalar): the sampling rate of of the FieldTrip data. 
%   trials_to_keep (nTrials x 1): Optional. Logical mask of trials to keep. 
%
% Outputs:
%   trl (nTrials x 3): Matrix containing the start, end, and t=0 sample
%       for each trial. 
%   trialinfo (nTrials x 2): Matrix containing a 1 or a 2 in the first row
%       if the trial was a local std/dev respectively, and a 3 or a 4 in the
%       second row if the trial was a glob std/dev respectively. 

nTrials = length(args.trial_times);
% Set trigger codes. 
trialinfo = zeros(nTrials, 2);
trialinfo(args.trials_by_type.loclStd, 1) = TrialTypes.Codes.LoclStd;
trialinfo(args.trials_by_type.loclDev, 1) = TrialTypes.Codes.LoclDev;
trialinfo(args.trials_by_type.globStd, 2) = TrialTypes.Codes.GlobStd;
trialinfo(args.trials_by_type.globDev, 2) = TrialTypes.Codes.GlobDev;

% Compute the first sample of each trial.
trial_start = round(args.trial_times .* args.fsample)';
% Compute the last sample of each trial.
trial_stop = round((args.trial_times + args.trial_length) .* args.fsample)';
% Define t=0 as the time of 5th vowel onset.
trial_offset = ones(nTrials,1) .* (args.fifth_vowel * args.fsample);

trl = [trial_start trial_stop -trial_offset];

if isfield(args, 'trials_to_keep')
    trl = trl(args.trials_to_keep, :);
    trialinfo = trialinfo(args.trials_to_keep, :);
end

