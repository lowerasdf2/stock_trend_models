function label = make_channel_label(iChan,ECoGchannels)
% Creates a descriptive label for the channel described by ECoGchannels(iChan). 
% The output is a string, e.g. 'R_PFC-Ch244'.

% label = sprintf('%s_%s-Ch%d', ECoGchannels(iChan).side, ...
%                               ECoGchannels(iChan).newROI, ...
%                               ECoGchannels(iChan).chanNum);
                          
% % % % CME version --> remove actual chan number
% % % label = sprintf('%s_%s', ECoGchannels(iChan).side, ...
% % %                               ECoGchannels(iChan).newROI);

% CME changed to AnatReg since it is more localized and will work better w/
% PCA. Changing back because oldROI/ROI seems to actually have better
% parcelattion (also matches wPLI)
% if length(unique({ECoGchannels(:).AnatReg})) > length(unique({ECoGchannels(:).oldROI}))
% %     keyboard % so CME can take note of when anatROI parcellation is superior
% end
label=sprintf('%s_%s', ECoGchannels(iChan).varCat);
