% Defines important properties of the LGD trials. 
classdef TrialProperties
   properties (Constant)
       % All times are in seconds. 
       Length = 1.4 % Trial length.
       Fifth_Vowel = 0.6; % Fifth vowel onset time.
       First_Vowel = 0.0; % The start of the trial is defined as the first-vowel onset.
       Vowel_Length = 0.1; % The duration of each vowel's presentation.
       Vowel_Spacing = 0.05; % Inter-vowel spacing.
       % The i-th entry is the time of onset of the i-th vowel. 
       Vowels = [0 TrialProperties.Vowel_Length * (1:4) + TrialProperties.Vowel_Spacing * (1:4)];
   end
end

