function channels = ECoGChannelMap(electrodeFile,electrodeSheet,catType)
%CHANNELMAP returns a struct mapping regions to electrode indices.
chanFileName = electrodeFile;
chanSheetName = electrodeSheet;
try
    [chanDataNum, chanDataTxt, chanDataRaw] = xlsread(chanFileName,chanSheetName);
    if size(chanDataNum,2) ~= size(chanDataTxt,2)
        %% this case can occur when final column(s) of spreadsheet contain non-numeric data (including nan/inf)
        % To make sure that that's the case, this section checks to make 
        % sure final numeric column w/ data in it has the correct data 
        % based on its column number
            
        % 1) convert single row of chanDataRaw to numeric
        % representation and find final column containing real numeric data
        rowData=chanDataRaw(2,:);
        for iCol=1:length(rowData)
            if strcmp(class(rowData{iCol}),'double') && ~isnan(rowData{iCol})
                finalCol=iCol;
            end
        end

        % 2) cross-reference data between chanDataRaw
        % (num-converted) and chanDataNum
        if rowData{finalCol} ~= chanDataNum(1,end)
            keyboard % problematic. investigate.
        end
    end
catch why
    keyboard
end
colNames = chanDataTxt(1,:);
chanDataTxt = chanDataTxt(2:end,:);
chanDataRaw = chanDataRaw(2:size(chanDataTxt,1)+1,:);
numRows = size(chanDataNum,1);
txtRows = size(chanDataTxt,1);
rawRows = size(chanDataRaw,1);
nRows = min([numRows,txtRows,rawRows]);
chanDataTxt = chanDataTxt(1:nRows,:);
chanDataRaw = chanDataRaw(1:nRows,:);
chanDataNum = chanDataNum(1:nRows,:);

oldChanROICol = strcmp(colNames, 'ROI');%
% newChanROICol = contains(colNames,'New ROI (9-way)');
newChanROICol = strcmp(colNames,'newROI_9way');% CME changed to match updated excel sheet
AEP_lat_loc_col = strcmp(colNames,'AEPLat_Loc');
HiG_lat_loc_col = strcmp(colNames,'HiGLat_Loc');
AEP_lat_glob_col = strcmp(colNames,'AEPLat_Glob');
HiG_lat_glob_col = strcmp(colNames,'HiGLat_Glob');

% chanLocCol = strcmp(colNames,'Anatomical region (Atlas)');
chanLocCol = strcmp(colNames,'anatomicalRegion_Atlas'); % CME changed to match updated excel sheet
chanAnatRegCol = strcmp(colNames,'anatReg');
if sum(chanAnatRegCol) == 0
    chanAnatRegCol = chanLocCol;
end
% chanNumCol = strcmp(colNames,'Channel #');
chanNumCol = strcmp(colNames,'channelNum');% CME changed to match updated excel sheet
contNumCol = strcmp(colNames,'Contact #');
chanSortCol = strcmp(colNames,'sortOrder');
% chanSideCol = strcmp(colNames,'Side');
chanSideCol = strcmp(colNames,'side'); % CME changed to match updated excel sheet
% mniXCol = strcmp(colNames,'MNI X');
% mniYCol = strcmp(colNames,'MNI Y');
% mniZCol = strcmp(colNames,'MNI Z');
mniXCol = strcmp(colNames,'MNI_X');
mniYCol = strcmp(colNames,'MNI_Y');
mniZCol = strcmp(colNames,'MNI_Z');
AEPLatLocCol = strcmp(colNames,'AEPLat_Loc');
HiGLatLocCol = strcmp(colNames,'HiGLat_Loc');
AEPLatGlobCol = strcmp(colNames,'AEPLat_Glob');
HiGLatGlobCol = strcmp(colNames,'HiGLat_Glob');

if sum(isnan(chanDataNum(:,chanNumCol))) > 0
    error(['Problem with channel numbers in Excel sheet ' chanSheetName]);
end
[sortedChanDataNum,~] = sortrows(chanDataNum,find(chanNumCol));
[sortedChanDataRaw,~] = sortrows(chanDataRaw,find(chanNumCol));

nChans = size(sortedChanDataNum,1);
channels(1:nChans) = struct('varID',[],'varCat',[],'catType',[],'newROI',[],'oldROI',[],...
    'AnatReg',[],'location',[],'side',[],'xCoord',[],...
    'yCoord',[],'zCoord',[],'sortOrder',[],'AEPLatLoc',[],...
    'HiGLatLoc',[],'AEPLatGlob',[],'HiGLatGlob',[]);
for iChan = 1:nChans
    channels(iChan).varID = [num2str(sortedChanDataNum(iChan,chanNumCol)) '-' sortedChanDataRaw{iChan,newChanROICol} '-'  sortedChanDataRaw{iChan,oldChanROICol} '-' sortedChanDataRaw{iChan,chanAnatRegCol}];
    %% Add catType to batchParams
    channels(iChan).catType=catType;
    %% Replace ROI-type label being utilized with varCat 
    if strcmp(catType,'anatROIs')
        channels(iChan).varCat=[sortedChanDataRaw{iChan,chanSideCol} '_' sortedChanDataRaw{iChan,chanAnatRegCol}];
    elseif strcmp(catType,'standROIs')
        channels(iChan).varCat=[sortedChanDataRaw{iChan,chanSideCol} '_' sortedChanDataRaw{iChan,oldChanROICol}];
    else
        keyboard % unexpected case. add code or wtf?
    end
    %% store all roi labels avail just for reference later on
    channels(iChan).AnatReg = sortedChanDataRaw{iChan,chanAnatRegCol};
    if sum(newChanROICol) ~= 0
        channels(iChan).newROI = sortedChanDataRaw{iChan,newChanROICol};
    end
    if sum(oldChanROICol) ~= 0
        channels(iChan).oldROI = sortedChanDataRaw{iChan,oldChanROICol};
    end
    channels(iChan).location = sortedChanDataRaw{iChan,chanLocCol};
    channels(iChan).side = sortedChanDataRaw{iChan,chanSideCol};
    channels(iChan).xCoord = sortedChanDataNum(iChan,mniXCol);
    channels(iChan).yCoord = sortedChanDataNum(iChan,mniYCol);
    channels(iChan).zCoord = sortedChanDataNum(iChan,mniZCol);
    if sum(chanSortCol)>0
        channels(iChan).sortOrder = sortedChanDataNum(iChan,chanSortCol);
    end
    if sum(AEPLatLocCol) ~= 0 && sum(HiGLatLocCol) ~= 0 && ...
            sum(AEPLatGlobCol) ~= 0 && sum(HiGLatGlobCol) ~= 0 
        if find(AEPLatLocCol) > size(sortedChanDataNum,2)
            channels(iChan).AEPLatLoc=nan;
        else
            channels(iChan).AEPLatLoc = sortedChanDataNum(iChan,AEPLatLocCol);
        end
        if find(HiGLatLocCol) > size(sortedChanDataNum,2)
            channels(iChan).HiGLatLoc=nan;
        else
            channels(iChan).HiGLatLoc = sortedChanDataNum(iChan,HiGLatLocCol);
        end
        if find(AEPLatGlobCol) > size(sortedChanDataNum,2)
            channels(iChan).AEPLatGlob=nan;
        else
            channels(iChan).AEPLatGlob = sortedChanDataNum(iChan,AEPLatGlobCol);
        end
        if find(HiGLatGlobCol) > size(sortedChanDataNum,2)
            channels(iChan).HiGLatGlob=nan;
        else
            channels(iChan).HiGLatGlob = sortedChanDataNum(iChan,HiGLatGlobCol);
        end
    end
end

