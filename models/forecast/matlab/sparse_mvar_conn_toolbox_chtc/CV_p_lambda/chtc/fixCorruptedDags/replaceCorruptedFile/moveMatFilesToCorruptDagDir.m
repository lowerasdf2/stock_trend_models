function moveMatFilesToCorruptDagDir(pars)

specDagDir=createFileEnd_fitECoG(pars,1);
if pars.cv.genDimRed.ch.PCA.run
    specDagDir=specDagDir(strfind(specDagDir,['PCAAC']):end);
else
    keyboard % double check below line works
    specDagDir=specDagDir(strfind(specDagDir,['CV' num2str(pars.cv.p.percData*100)]):end);
end
corruptedDagFolder=[pars.config.dirs.baseCorruptDagDir filesep ...
    pars.dataType.varSelect filesep pars.dataType.dataID filesep pars.dataType.condition ...
    filesep specDagDir filesep];
matFiles=dir([pwd filesep '*.mat']);
for iFile=1:length(matFiles)
    if ~isempty(strfind(matFiles(iFile).name,'pars.mat')) || ~isempty(strfind(matFiles(iFile).name,'dataEpochs_'))
        continue
    else
        corruptFile=[corruptedDagFolder filesep matFiles(iFile).name];
        if ~exist(corruptFile,'file')
            keyboard
        else
            delete(corruptFile)
            if exist(corruptFile,'file')
                keyboard
            else
                movefile([pwd filesep matFiles(iFile).name],corruptFile)
                if ~exist(corruptFile,'file')
                    keyboard
                end
            end
        end
    end
end