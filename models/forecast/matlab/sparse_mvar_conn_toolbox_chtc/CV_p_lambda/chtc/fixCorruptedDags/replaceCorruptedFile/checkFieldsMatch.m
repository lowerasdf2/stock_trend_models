function checkFieldsMatch(dagParsUsed,pars)

%% Recursively check all fields

curStruct_dagPars=dagParsUsed;
curFields=fields(curStruct_dagPars);
for iField=1:length(curFields)
    if strcmp(curFields{iField},'onlyLoadSaveData')
        continue
    end
    moreFields=true;
    curStruct_dagPars=dagParsUsed;
    curStruct_pars=pars;
    while moreFields
        [moreFields,curStruct_dagPars,curStruct_pars]=checkNextField(curStruct_dagPars.(curFields{iField}),curStruct_pars.(curFields{iField}));
    end
end
    
% allFields=fields(dagParsUsed);
% for iField=1:length(allFields)
%     fieldName=allFields{iField};
%     if strcmp(fieldName,'onlyLoadSaveData')
%         continue
%     end
%     try
%         allFields2=fields(dagParsUsed.(fieldName));
%         for iField2=1:length(allFields)
%             fieldName=allFields{iField};
%         end
%     catch
%         if strcmp(class(dagParsUsed.(fieldName)),'char')
%             if ~strcmp(dagParsUsed.(fieldName),pars.(fieldName))
%                 keyboard % oh shit
%             end
%         else
%             if dagParsUsed.(fieldName)~=pars.(fieldName)
%                 keyboard % oh shit
%             end
%         end
%     end
% end

function [moreFields,curStruct_dagPars,curStruct_pars]=checkNextField(curStruct_dagPars,curStruct_pars)
    try
        curFields2=fields(curStruct_dagPars);
        oldCurStruct_dagPars=curStruct_dagPars;
        oldCurStruct_pars=curStruct_pars;
        for iField2=1:length(curFields2)
            moreFields=true;
            curStruct_dagPars=oldCurStruct_dagPars;
            curStruct_pars=oldCurStruct_pars;
            while moreFields
                [moreFields,curStruct_dagPars,curStruct_pars]=checkNextField(oldCurStruct_dagPars.(curFields2{iField2}),oldCurStruct_pars.(curFields2{iField2}));
            end
        end
    catch
        moreFields=false;
        if strcmp(class(curStruct_dagPars),'char')
            if ~strcmp(curStruct_dagPars,curStruct_pars)
                keyboard % oh shit
            end
        elseif strcmp(class(curStruct_dagPars),'cell')
            if ~strcmp(curStruct_dagPars{1},curStruct_pars{1})
                keyboard % oh shit
            end
        else
            try
                if curStruct_dagPars~=curStruct_pars
                    keyboard % oh shit
                end
            catch why
                keyboard
            end
        end
    end
end

end