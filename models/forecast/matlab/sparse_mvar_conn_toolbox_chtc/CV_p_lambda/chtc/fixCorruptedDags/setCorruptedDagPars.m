function pars=setCorruptedDagPars(pars)
keyboard % check pars relevant to fixCorruptedDagFiles
% patient369LR
%     pars.run.fixCorruptedDagFiles.('OAAS5').failedChs=[72,78,95];%68
%     pars.run.fixCorruptedDagFiles.('OAAS5').foldLvls=[2,2,2];%2
%     pars.run.fixCorruptedDagFiles.('OAAS5').pKs=[3,4,3];%1
%     pars.run.fixCorruptedDagFiles.('OAAS5').lwKs=[4,5,4];%3
%     pars.run.fixCorruptedDagFiles.('OAAS5').modelOrders=[14,14,14];%14
%     pars.run.fixCorruptedDagFiles.('OAAS5').failScript={'lFit','lFit','lFit'};%'lFit'
%     pars.run.fixCorruptedDagFiles.('OAAS5').cvType={'p','p','p'};%'p'
pars.run.fixCorruptedDagFiles.('OAAS4t3').failedChs=[7,9,27,89];
pars.run.fixCorruptedDagFiles.('OAAS4t3').foldLvls=[2,2,2,2];
pars.run.fixCorruptedDagFiles.('OAAS4t3').pKs=[4,3,4,4];
pars.run.fixCorruptedDagFiles.('OAAS4t3').lwKs=[3,3,4,2];
pars.run.fixCorruptedDagFiles.('OAAS4t3').modelOrders=[14,14,14,14];
pars.run.fixCorruptedDagFiles.('OAAS4t3').failScript={'lFit','lFit','lFit','lFit'};
pars.run.fixCorruptedDagFiles.('OAAS4t3').cvType={'p','p','p','p'};