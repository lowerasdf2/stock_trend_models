function createDagAndSubmitFiles(pars,cond,condorUser,createSubmitFiles)
%% Create submit files
if createSubmitFiles == true
    dagOutputDir=[pars.dirs.baseCodeDir filesep 'chtc' filesep 'prepDags' filesep 'submitFiles'];
    createSubFile_lassoFit_singleVar(pars,dagOutputDir,condorUser)% 8GB of disk should be sufficient unless we run all sleep data in one model
    createSubFile_stitchSingleVarParams_measureErr(pars,dagOutputDir,condorUser)
    createSubFile_construct_debiased_model_wBestLambdas(pars,dagOutputDir,condorUser)
    createSubFile_measureAvgModelOrderErr(pars,dagOutputDir,condorUser)
    createSubFile_setModelOrder(pars,dagOutputDir,condorUser)
end
%% Load data so we know how many channels there are
loadFile=getEpochFilename(pars); 
loadDir=getEpochSaveDir(pars);
if ~exist([loadDir filesep loadFile],'file')
    keyboard
end
load([loadDir filesep loadFile])
nVars=size(dataEpochs{1},1);
%% Create DAG
main_createDag(pars,cond,nVars)

end

