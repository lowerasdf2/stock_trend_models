function createMasterDagAndSubdags(pars,localDagDir,dagSubDir,chtcBaseDir,cond,nVar)
%% Splits up work into subdags
% 1) create a single subdag/dag for each model-order tested
% 2) create final dag for everything after CV_p (set_model_order + CV_lambda)
% 3) stitch above two dag-types into one master dag file

%% set some vars used by all dags
chtcSubFileDir=[chtcBaseDir  '/submitFiles/'];
chtcDagDir=[chtcBaseDir  'dags/' pars.dataType.globalDataCat '/' pars.dataType.varSelect '/' pars.dataType.catType '/' pars.dataType.dataID '/' cond '/'  dagSubDir '/'];
%% create pcaStr
if pars.cv.genDimRed.ch.PCA.run == 1
    pcaStr=['pca' num2str(pars.cv.genDimRed.ch.PCA.perc*100)];
else
    pcaStr='';
end
%% set global job vars (these are vars appended to all jobs and help read in specific datasets)
globalJobVars=['_epochLen$' num2str(pars.dataType.epochLen*1000) ...
    '_catType$' pars.dataType.catType ...
    '_sampleRate$' num2str(pars.dataType.sampleRate) ...
    '_dataSeg$' num2str(pars.dataType.shortenSeg.bool) ...
    '_dataTypeStr$' pars.dataType.globalDataCat ...
    '_dataID$' pars.dataType.dataID ...
    '_cond$' cond ...
    '_varSelect$' pars.dataType.varSelect ...
    '_pcaStr$' pcaStr ...
    '_numFoldsLvl1$' num2str(pars.cv.p.K) ...
    '_numFoldsLvl2$' num2str(pars.cv.p.dimRed.ch.lasso.K)];

%% 1) create dag for each model order
if pars.cv.p.run
    for modelOrder=pars.cv.p.modelOrderRange
        createModelOrderSubdag()
    end
end
%% 2) create cv_lambda dag
if pars.cv.lambda.run
    createCVlambdaSubdag()
end
%% 3) create masterDag
if pars.cv.nested.bool
    createMasterDag()
end

function createMasterDag()
    %% create param fields for placing jobs into structs
    dagLines=initDagLines('master');
    if pars.cv.p.run
        for modelOrder=pars.cv.p.modelOrderRange
            subdagName=[pars.dataType.globalDataCat '_' pars.dataType.dataID '_' cond '_' num2str(nVar) 'vars_mo' num2str(modelOrder)];
            jobName=subdagName;
            dagLines.jobNames.moDags{length(dagLines.jobNames.moDags)+1}=jobName;
        end 
    end
    if pars.cv.lambda.run
        subdagName=[pars.dataType.globalDataCat '_' pars.dataType.dataID '_' cond '_' num2str(nVar) 'vars_lambdaCV'];
        jobName=subdagName;
        dagLines.jobNames.lambdaDag{1}=jobName;
    end
    %% once jobs are stored, construct dag
    constructDag('master',dagLines)
    
end % end createMasterDag
function createCVlambdaSubdag()
    %% A)create param fields for placing jobs into structs
    dagLines=initDagLines('lambda');
    %% setModelOrder: 
    if ~pars.run.createDag.singleVar.bool
        % program vars: 
        % transferFiles+initDir vars: $(dagSubdir),$(dataTypeStr),$(dataID),$(cond),$(varSelect),$(pcaStr),$(numFoldsLvl1),$(numFoldsLvl2),$(kFolder),$(tarFileName)
        cvType='p';
        bashInputOrigin=pars.nicknames('setModelOrder');
        jobName = ['bashInputOrigin$' bashInputOrigin globalJobVars '_cvType$' cvType '_cvLevel$' num2str(nan) '_modelOrder$' num2str(nan) ...
            '_tarFileName$modelOrderErrs_nModelOrders$' num2str(length(pars.cv.p.modelOrderRange))];
        dagLines.jobNames.setModelOrder={jobName};
        dagLines.dependencies.setModelOrder.pCV.kNaN={jobName};
    end
    %% Now that model order is determined, do another round of CV (single layer this time) for lambda hyperparameter to determine finalized lambda vals
    % Note: optimized lambdas used during CV_p process use a smaller
    % portion of training data per fold given it uses nested CV. This
    % round avoids that bias by using entire folds for CV_lambda
    % process.
    modelOrder=nan;
    for kInd_lambda=1:pars.cv.lambda.K
        if ~pars.run.createDag.specCVks.bool || (pars.run.createDag.specCVks.bool && sum(pars.run.createDag.specCVks.kList==kInd_lambda) > 0)
            dagLines=storeKfoldIndJobs(globalJobVars,'lambda',dagLines,pars,nVar,modelOrder,kInd_lambda);
        end
    end
    %% once jobs are stored, construct dag
    constructDag('lambda',dagLines)
end % end createCVlambdaSubdag
function createModelOrderSubdag()
    %% A)create param fields for placing jobs into structs
    dagLines=initDagLines('p');
    %% store kFoldInd jobs in dagLines
    for kInd_p=1:pars.cv.p.K
        if ~pars.run.createDag.specCVks.bool || (pars.run.createDag.specCVks.bool && sum(pars.run.createDag.specCVks.kList==kInd_p) > 0)
            dagLines=storeKfoldIndJobs(globalJobVars,'p',dagLines,pars,nVar,modelOrder,kInd_p);
        end
    end
    %% store measureAvgModelOrderErr jobs in dagLines
    % program vars: $(modelOrder)
    % transferFiles+initDir vars: $(dagSubdir),$(dataTypeStr),$(dataID),$(cond),$(varSelect),$(pcaStr),$(numFoldsLvl1),$(numFoldsLvl2),$(kFolder),$(tarFileName)
    cvLevel=1;% this stitch uses models trained at 2nd layer of CV process
    cvType='p';
    bashInputOrigin=pars.nicknames('measureAvgModelOrderErr');
    jobName = ['bashInputOrigin$' bashInputOrigin globalJobVars '_cvType$' cvType '_cvLevel$' num2str(cvLevel) '_modelOrder$' num2str(modelOrder) ...
        '_tarFileName$debiased_nModelOrders$' num2str(length(pars.cv.p.modelOrderRange))];
    dagLines.jobNames.measureAvgModelOrderErr{length(dagLines.jobNames.measureAvgModelOrderErr)+1}=jobName;
    dagLines.dependencies.measureAvgModelOrderErr.pCV.(['mo' num2str(modelOrder)])={jobName};
    %% once jobs are stored, construct dag
    constructDag('p',dagLines)
end % end createModelOrderSubdag
function constructDag(dagType,dagLines)
    %% Open new file
    if strcmp(dagType,'p')
        endFile='';
        if pars.run.createDag.lvl1CVonly
            endFile=[endFile '_lvl1only'];
        end
        if pars.run.createDag.debias_measMOerr_only
            endFile=[endFile '_fix'];
        end
        if pars.run.createDag.specCVks.bool
            endFile=[endFile '_kInds'];
            for kIndind=1:length(pars.run.createDag.specCVks.kList)
                if kIndind==length(pars.run.createDag.specCVks.kList)
                    endFile=[endFile num2str(pars.run.createDag.specCVks.kList(kIndind))];
                else
                    endFile=[endFile num2str(pars.run.createDag.specCVks.kList(kIndind)) '-'];
                end
            end            
        end
        dagFile=[localDagDir filesep pars.dataType.globalDataCat '_' pars.dataType.dataID '_' cond '_' num2str(nVar) 'vars_mo' num2str(modelOrder) endFile '.dag'];
    elseif strcmp(dagType,'lambda')
        if ~pars.run.createDag.singleVar.bool
            dagFile=[localDagDir filesep pars.dataType.globalDataCat '_' pars.dataType.dataID '_' cond '_' num2str(nVar) 'vars_lambdaCV.dag'];
        else
            dagFile=[localDagDir filesep pars.dataType.globalDataCat '_' pars.dataType.dataID '_' cond '_' num2str(nVar) 'vars_lambdaCV_singleVar' num2str(pars.run.createDag.singleVar.ch) '.dag'];
        end
    elseif strcmp(dagType,'master')
        dagFile=[localDagDir filesep pars.dataType.globalDataCat '_' pars.dataType.dataID '_' cond '_' num2str(nVar) 'vars.dag'];
    end
    fileID=fopen(dagFile, 'w');
    %% Print jobs to file for dag
    printAllDagJobNames(fileID,dagType,dagLines)
    %% Use all stored jobs to first list job, then list all necessary VARS to send to submit file; also add post/pre-scripts here
    printAllJobVars(fileID,dagType,dagLines)
    %% Set dependencies
    setAndPrintDependencies(fileID,dagType,dagLines)
    %% Add retry statements
    printAllRetries(fileID,dagType,dagLines);
    %% close file
    fclose(fileID);
end % end constructDag
function printAllDagJobNames(fileID,dagType,dagLines)
    %% Print jobs to file for dag
    if strcmp(dagType,'p') || strcmp(dagType,'lambda')
        subdagBool=false;
        %% all submit files except setModelOrder have reached version 2 since new ones needed additional vars added to load variable dataEpovars files (based on data window location/length)
        if ~pars.run.createDag.debias_measMOerr_only
            printJobNames(fileID,chtcSubFileDir,dagLines.jobNames.lassoFit_singleVar,['lassoFit_singleVar_v' num2str(pars.run.createDag.ver)],subdagBool)
            printJobNames(fileID,chtcSubFileDir,dagLines.jobNames.stitchSingleVarParams_measureErr,['stitchSingleVarParams_measureErr_v' num2str(pars.run.createDag.ver)],subdagBool)
        end
        printJobNames(fileID,chtcSubFileDir,dagLines.jobNames.construct_debiased_model_wBestLambdas,['construct_debiased_model_wBestLambdas_v' num2str(pars.run.createDag.ver)],subdagBool)
        if strcmp(dagType,'p')
            printJobNames(fileID,chtcSubFileDir,dagLines.jobNames.measureAvgModelOrderErr,['measureAvgModelOrderErr_v' num2str(pars.run.createDag.ver)],subdagBool)
        else
            printJobNames(fileID,chtcSubFileDir,dagLines.jobNames.setModelOrder,['setModelOrder_v' num2str(pars.run.createDag.ver)],subdagBool)
        end
    elseif strcmp(dagType,'master')
        subdagBool=true;
        printJobNames(fileID,chtcDagDir,dagLines.jobNames.moDags,nan,subdagBool)
        printJobNames(fileID,chtcDagDir,dagLines.jobNames.lambdaDag,nan,subdagBool)
    else
        keyboard
    end
end % end printAllDagJobNames
function printAllJobVars(fileID,dagType,dagLines)
    if ~strcmp(dagType,'master')
        %% Use all stored jobs to first list job, then list all necessary VARS to send to submit file; also add post/pre-scripts here
        if ~pars.run.createDag.debias_measMOerr_only
            % lassoFit_singleVar
            printJobVars(pars,fileID,dagSubDir,dagLines.jobNames.lassoFit_singleVar)
            % stitchSingleVarParams_measureErr
            printJobVars(pars,fileID,dagSubDir,dagLines.jobNames.stitchSingleVarParams_measureErr,true,nVar)
        end
        % construct_debiased_model_wBestLambdas
        printJobVars(pars,fileID,dagSubDir,dagLines.jobNames.construct_debiased_model_wBestLambdas,true,nVar)
        if strcmp(dagType,'p')
            % measureAvgModelOrderErr
            printJobVars(pars,fileID,dagSubDir,dagLines.jobNames.measureAvgModelOrderErr,true,nVar)
        else
            % setModelOrder
            printJobVars(pars,fileID,dagSubDir,dagLines.jobNames.setModelOrder,true,nVar)
        end
    end
end % printAllJobVars
function setAndPrintDependencies(fileID,dagType,dagLines)
    if strcmp(dagType,'p') || strcmp(dagType,'lambda')
        % first convert all job names to their shorter names so I don't break
        % file system on condor with these long-ass names
        jobFields=fields(dagLines.dependencies);
        for jobTypeInd=1:length(jobFields)
            jobField=jobFields{jobTypeInd};
            try
                cvTypeFields=fields(dagLines.dependencies.(jobField));
            catch why
                keyboard
            end
            for cvTypeInd=1:length(cvTypeFields)
                cvTypeField=cvTypeFields{cvTypeInd};
                try
                    paramFields=fields(dagLines.dependencies.(jobField).(cvTypeField));
                catch 
                    jobs=dagLines.dependencies.(jobField).(cvTypeField);
                    for iJob=1:length(jobs)
                        dagLines.dependencies.(jobField).(cvTypeField){iJob}=shortenJobName(jobs{iJob});
                    end
                    continue
                end
                for paramInd=1:length(paramFields)
                    paramField=paramFields{paramInd};
                    try
                        kFields=fields(dagLines.dependencies.(jobField).(cvTypeField).(paramField));
                    catch
                        jobs=dagLines.dependencies.(jobField).(cvTypeField).(paramField);
                        for iJob=1:length(jobs)
                            dagLines.dependencies.(jobField).(cvTypeField).(paramField){iJob}=shortenJobName(jobs{iJob});
                        end
                        continue
                    end
                    for kInd=1:length(kFields)
                        kField=kFields{kInd};
                        try
                            cvLevels=fields(dagLines.dependencies.(jobField).(cvTypeField).(paramField).(kField));
                        catch
                            jobs=dagLines.dependencies.(jobField).(cvTypeField).(paramField).(kField);
                            for iJob=1:length(jobs)
                                dagLines.dependencies.(jobField).(cvTypeField).(paramField).(kField){iJob}=shortenJobName(jobs{iJob});
                            end
                            continue
                        end
                        for cvLvlInd=1:length(cvLevels)
                            cvLvlField=cvLevels{cvLvlInd};
                            jobs=dagLines.dependencies.(jobField).(cvTypeField).(paramField).(kField).(cvLvlField);
                            for iJob=1:length(jobs)
                                dagLines.dependencies.(jobField).(cvTypeField).(paramField).(kField).(cvLvlField){iJob}=shortenJobName(jobs{iJob});
                            end
                        end
                        clear cvLevels
                    end
                    clear kFields
                end
                clear paramFields
            end
            clear cvTypeFields
        end
        clear jobFields
    end
    if strcmp(dagType,'p')
        parentJobsOfAvgModelOrderErr={};
        for kInd_p=1:pars.cv.p.K
            if ~pars.run.createDag.specCVks.bool || (pars.run.createDag.specCVks.bool && sum(pars.run.createDag.specCVks.kList==kInd_p) > 0)
                printKfoldIndJobDependencies('p',fileID,dagLines,pars,nVar,modelOrder,kInd_p)
                % parents for avgModelOrderErr jobs: modelOrderX-kIndX_construct_debiased_model_wBestLambdas
                cvType='p';
                cvLevel=1;
                parentJobsOfAvgModelOrderErr{length(parentJobsOfAvgModelOrderErr)+1}=dagLines.dependencies.construct_debiased_model_wBestLambdas.([cvType 'CV']).(['mo' num2str(modelOrder)]).(['k' num2str(kInd_p)]).(['cvLevel' num2str(cvLevel)]){1};   
            end
        end
        %% connect modelOrderX-kIndX_construct_debiased_model_wBestLambdas to avgModelOrderError
        fprintf(fileID, 'PARENT ');
        if ~pars.run.createDag.specCVks.bool && length(parentJobsOfAvgModelOrderErr) ~= pars.cv.p.K
            crash
        elseif pars.run.createDag.specCVks.bool && length(parentJobsOfAvgModelOrderErr) ~= length(pars.run.createDag.specCVks.kList)
            crash
        end
        for jobInd=1:length(parentJobsOfAvgModelOrderErr)
            fprintf(fileID, [parentJobsOfAvgModelOrderErr{jobInd} ' ']);
        end
        fprintf(fileID, 'CHILD ');
        childJobs=dagLines.dependencies.measureAvgModelOrderErr.pCV.(['mo' num2str(modelOrder)]);
        if length(childJobs) > 1
            crash
        end
        fprintf(fileID, [childJobs{1} ' \n']);
    elseif strcmp(dagType,'lambda')
        %% connect setModelOrder to final model fits (k*lvl1fit)+(lvl0fit)
        modelOrder=nan;
        cvLevel=1;
        cvType='lambda';
        if ~pars.run.createDag.singleVar.bool
            fprintf(fileID, 'PARENT ');
            parentJobs=dagLines.dependencies.setModelOrder.pCV.kNaN;
            if length(parentJobs) > 1
                crash
            end
            for jobInd=1:length(parentJobs)
                fprintf(fileID, [parentJobs{jobInd} ' ']);
            end
            fprintf(fileID, 'CHILD ');
        else
            fprintf(fileID, 'PARENT ');
        end
        % child1
        parents_stitch_measErr={};
        for kInd=1:pars.cv.lambda.K
            if ~pars.run.createDag.specCVks.bool || (pars.run.createDag.specCVks.bool && sum(pars.run.createDag.specCVks.kList==kInd) > 0)
                try
                    childJobs=dagLines.dependencies.lassoFit_singleVar.([cvType 'CV']).(['mo' num2str(modelOrder)]).(['k' num2str(kInd)]).(['cvLevel' num2str(cvLevel)]);
                catch why
                    keyboard
                end
                if length(childJobs) ~= nVar && ~pars.run.createDag.singleVar.bool
                    crash
                end
                for iJob=1:length(childJobs)
                    fprintf(fileID, [childJobs{iJob} ' ']);
                    parents_stitch_measErr{length(parents_stitch_measErr)+1}=childJobs{iJob};
                end
            end
        end
        % child2
        cvLevel=0;
        kInd=nan;
        childJobs=dagLines.dependencies.lassoFit_singleVar.([cvType 'CV']).(['mo' num2str(modelOrder)]).(['k' num2str(kInd)]).(['cvLevel' num2str(cvLevel)]);
        if length(childJobs) ~= nVar && ~pars.run.createDag.singleVar.bool
            crash
        else
            for iJob=1:length(childJobs)
                if pars.run.createDag.singleVar.bool && iJob==length(childJobs)
                    fprintf(fileID, [' CHILD ' childJobs{iJob}]);
                else
                    fprintf(fileID, [childJobs{iJob} ' ']);
                end
            end
            fprintf(fileID, [childJobs{1} '\n']);
        end
        
        %% connect final CV_lvl1_lambda to stitch_meas_err
        if ~pars.run.createDag.singleVar.bool
            cvLevel=1;
            fprintf(fileID, 'PARENT ');
            if ~pars.run.createDag.specCVks.bool && length(parents_stitch_measErr) ~= nVar*pars.cv.lambda.K
                crash
            end
            for jobInd=1:length(parents_stitch_measErr)
                fprintf(fileID, [parents_stitch_measErr{jobInd} ' ']);
            end
            fprintf(fileID, 'CHILD ');
            childJobs=dagLines.dependencies.stitchSingleVarParams_measureErr.([cvType 'CV']).(['mo' num2str(modelOrder)]).(['k' num2str(nan)]).(['cvLevel' num2str(cvLevel)]);   
            if length(childJobs) ~= 1
                crash
            end
            fprintf(fileID, [childJobs{1} ' \n']);
        end
        %% connect (stitchSingleVarParams_measureErr & fitLvl0CVlambda) to ...
        %         construct_debiased_model_wBestLambdas 
        % parent1: lassoFit_lvl0
        if ~pars.run.createDag.singleVar.bool
            cvLevel=0;
            kInd=nan;
            parentJobs1=dagLines.dependencies.lassoFit_singleVar.([cvType 'CV']).(['mo' num2str(modelOrder)]).(['k' num2str(kInd)]).(['cvLevel' num2str(cvLevel)]);
            fprintf(fileID, 'PARENT ');
            if length(parentJobs1) ~= (nVar)
                crash
            end
            for jobInd=1:length(parentJobs1)
                fprintf(fileID, [parentJobs1{jobInd} ' ']);
            end
            % parent2: stitchSingleVarParams_measureErr
            cvLevel=1;
            kInd=nan;
            parentJobs2=dagLines.dependencies.stitchSingleVarParams_measureErr.([cvType 'CV']).(['mo' num2str(modelOrder)]).(['k' num2str(kInd)]).(['cvLevel' num2str(cvLevel)]);
            if length(parentJobs2) ~= 1
                crash
            end
            for jobInd=1:length(parentJobs2)
                fprintf(fileID, [parentJobs2{jobInd} ' ']);
            end
            % child: modelOrderX-kIndX_construct_debiased_model_wBestLambdas
            cvLevel=0;
            kInd=nan;
            fprintf(fileID, 'CHILD ');
            childJobs=dagLines.dependencies.construct_debiased_model_wBestLambdas.([cvType 'CV']).(['mo' num2str(modelOrder)]).(['k' num2str(kInd)]).(['cvLevel' num2str(cvLevel)]);   
            if length(childJobs) ~= 1
                crash
            end
            fprintf(fileID, [childJobs{1} ' \n']);
        end
    elseif strcmp(dagType,'master')
        fprintf(fileID, 'PARENT ');
        parentJobs=dagLines.jobNames.moDags;
        for jobInd=1:length(parentJobs)
            try
                fprintf(fileID, [parentJobs{jobInd} ' ']);
            catch why
                keyboard
            end
        end
        fprintf(fileID, 'CHILD ');
        childJobs=dagLines.jobNames.lambdaDag;
        fprintf(fileID, [childJobs{1} ' \n']);
    else
        keyboard
    end
end % end setAndPrintDependencies
function printAllRetries(fileID,dagType,dagLines)
    if ~strcmp(dagType,'master')
        if ~pars.run.createDag.debias_measMOerr_only
            printRetries(fileID,pars.run.createDag.NUM_RETRIES,dagLines.jobNames.lassoFit_singleVar)
            printRetries(fileID,pars.run.createDag.NUM_RETRIES,dagLines.jobNames.stitchSingleVarParams_measureErr)
        end
        printRetries(fileID,pars.run.createDag.NUM_RETRIES,dagLines.jobNames.construct_debiased_model_wBestLambdas)
        if strcmp(dagType,'p')
            printRetries(fileID,pars.run.createDag.NUM_RETRIES,dagLines.jobNames.measureAvgModelOrderErr)
        elseif strcmp(dagType,'lambda')
            printRetries(fileID,pars.run.createDag.NUM_RETRIES,dagLines.jobNames.setModelOrder)
        else
            keyboard
        end
    end
end % end printAllRetries
function dagLines=initDagLines(dagType)
    dagLines=struct;
    if strcmp(dagType,'p') || strcmp(dagType,'lambda')
        dagLines.jobNames.lassoFit_singleVar={};
        dagLines.jobNames.stitchSingleVarParams_measureErr={};
        dagLines.jobNames.construct_debiased_model_wBestLambdas={};
        if strcmp(dagType,'p')
            dagLines.jobNames.measureAvgModelOrderErr={};
        elseif strcmp(dagType,'lambda')
            dagLines.jobNames.setModelOrder={};
        end
    elseif strcmp(dagType,'master')
        dagLines.jobNames.moDags={};
        dagLines.jobNames.lambdaDag={};
    else
        keyboard
    end
end % dagLines



end % end createMasterDagAndSubDags