function printKfoldIndJobDependencies(cvType,fileID,dagLines,pars,nVar,modelOrder,kInd)
%% connect modelOrderX-kIndX_fitLvl2CVp to ...
%     modelOrderX-kIndX_stitchSingleVarParams_measureErr
if strcmp(cvType,'p')
    highestLvl=2;
else
    crash
    highestLvl=1;
    cvLevel=highestLvl;
    fprintf(fileID, 'PARENT ');
    parentJobs=dagLines.dependencies.setModelOrder;
    if length(parentJobs) > 1
        crash
    end
    for jobInd=1:length(parentJobs)
        fprintf(fileID, [parentJobs{jobInd} ' ']);
    end
    fprintf(fileID, 'CHILD ');
    childJobs=dagLines.dependencies.lassoFit_singleVar.([cvType 'CV']).(['mo' num2str(modelOrder)]).(['k' num2str(kInd)]).(['cvLevel' num2str(cvLevel)]);
    if length(childJobs) ~= nVar
        crash
    end
    fprintf(fileID, [childJobs{1} ' \n']);
end

if ~pars.run.createDag.lvl1CVonly || ~strcmp(cvType,'p')
    cvLevel=highestLvl;
    parentJobs=dagLines.dependencies.lassoFit_singleVar.([cvType 'CV']).(['mo' num2str(modelOrder)]).(['k' num2str(kInd)]).(['cvLevel' num2str(cvLevel)]);
    fprintf(fileID, 'PARENT ');
    if length(parentJobs) ~= (pars.cv.p.dimRed.ch.lasso.K*nVar)
        crash
    end
    for jobInd=1:length(parentJobs)
        fprintf(fileID, [parentJobs{jobInd} ' ']);
    end
    cvLevel=highestLvl;
    fprintf(fileID, 'CHILD ');
    childJobs=dagLines.dependencies.stitchSingleVarParams_measureErr.([cvType 'CV']).(['mo' num2str(modelOrder)]).(['k' num2str(kInd)]).(['cvLevel' num2str(cvLevel)]);   
    if length(childJobs) ~= 1
        crash
    end
    fprintf(fileID, [childJobs{1} ' \n']);
end

%% connect (modelOrderX-kIndX_stitchSingleVarParams_measureErr & modelOrderX-kIndX-fitLvl1CVp) to ...
%         modelOrderX-kIndX_construct_debiased_model_wBestLambdas 
if ~pars.run.createDag.debias_measMOerr_only
    % parent1: lassoFit_lvl1
    cvLevel=highestLvl-1;
    parentJobs1=dagLines.dependencies.lassoFit_singleVar.([cvType 'CV']).(['mo' num2str(modelOrder)]).(['k' num2str(kInd)]).(['cvLevel' num2str(cvLevel)]);
    fprintf(fileID, 'PARENT ');
    if length(parentJobs1) ~= (nVar)
        crash
    end
    for jobInd=1:length(parentJobs1)
        fprintf(fileID, [parentJobs1{jobInd} ' ']);
    end
    % parent2: stitchSingleVarParams_measureErr
    cvLevel=highestLvl;
    parentJobs2=dagLines.dependencies.stitchSingleVarParams_measureErr.([cvType 'CV']).(['mo' num2str(modelOrder)]).(['k' num2str(kInd)]).(['cvLevel' num2str(cvLevel)]);
    if length(parentJobs2) ~= 1
        crash
    end
    for jobInd=1:length(parentJobs2)
        fprintf(fileID, [parentJobs2{jobInd} ' ']);
    end
    % child: modelOrderX-kIndX_construct_debiased_model_wBestLambdas
    fprintf(fileID, 'CHILD ');
    cvLevel=highestLvl-1;
    childJobs=dagLines.dependencies.construct_debiased_model_wBestLambdas.([cvType 'CV']).(['mo' num2str(modelOrder)]).(['k' num2str(kInd)]).(['cvLevel' num2str(cvLevel)]);   
    if length(childJobs) ~= 1
        crash
    end
    fprintf(fileID, [childJobs{1} ' \n']);
end
