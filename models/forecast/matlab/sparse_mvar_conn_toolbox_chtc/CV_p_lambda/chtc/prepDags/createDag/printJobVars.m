function printJobVars(pars,fileID,dagSubDir,jobs,varargin)
if ~isempty(varargin)
    zipSingleChFilesPreScriptBool=varargin{1};
    nVar=varargin{2};
else
    zipSingleChFilesPreScriptBool=false;
end
for jobInd = 1:length(jobs)
    fullJobName = jobs{jobInd};%
    endVarInds=strfind(fullJobName,'_');
    job=fullJobName;%(endVarInds(1)+1:end);
    endVarInds=strfind(job,'_');
    begVarInds=strfind(job,'$');
    if length(endVarInds)+1 ~= length(begVarInds)
        keyboard
    end
    nModelOrders=nan;
    for iVar=1:length(begVarInds)
        if iVar == 1
            varStr=job(1:endVarInds(iVar)-1);
        elseif iVar ~= length(begVarInds)
            try
                varStr=job(endVarInds(iVar-1)+1:endVarInds(iVar)-1);
            catch why
                keyboard
            end
        else
            varStr=job(endVarInds(end)+1:end);
        end
        dashInd=strfind(varStr,'$');
        varName=varStr(1:dashInd-1);
        varVal=varStr(dashInd+1:end);
        if strcmp(varName,'pcaStr')
            if ~isempty(varVal)
                varVal=['_' varVal];
            end
        elseif strcmp(varName,'kStr')
            if ~isempty(varVal)
                varVal=['_' varVal];
            end
        elseif strcmp(varName,'dataSeg')
            if strcmp(varVal,'1')
                varVal=['mins' num2str(pars.dataType.shortenSeg.startMin) '-' num2str(pars.dataType.shortenSeg.segLen+pars.dataType.shortenSeg.startMin) '_'];
            else
                varVal='';
            end
        elseif strcmp(varName, 'epochLen')
            varVal=num2str(str2double(varVal)/1000);
        end
        shortName=shortenJobName(fullJobName);
        fprintf(fileID, ['VARS ' shortName ' ' varName '="' varVal '"\n']);
        if strcmp(varName,'dataTypeStr')
            dataTypeStr=varVal;
        elseif strcmp(varName,'varSelect')
            varSelect=varVal;
        elseif strcmp(varName,'dataID')
            dataID=varVal;
        elseif strcmp(varName,'cond')
            cond=varVal;
        elseif strcmp(varName,'numFoldsLvl1')
            numFoldsLvl1=varVal;
        elseif strcmp(varName,'numFoldsLvl2')
            numFoldsLvl2=varVal;
        elseif strcmp(varName,'lvl1kInd')
            lvl1kInd=varVal;
        elseif strcmp(varName,'cvType')
            cvType=varVal;
        elseif strcmp(varName,'cvLevel')
            cvLevel=varVal;
            if pars.run.createDag.lvl1CVonly && ~strcmp(varVal,'1')
                continue
            end
            if strcmp(cvLevel,'0')
                x=1;
            end
        elseif strcmp(varName,'modelOrder')
            modelOrder=varVal;
        elseif strcmp(varName,'nModelOrders')
            nModelOrders=varVal;
        elseif strcmp(varName,'bashInputOrigin')
            bashInputOrigin=varVal;
        end
    end
    shortName=shortenJobName(fullJobName);
    fprintf(fileID, ['VARS ' shortName ' ' 'dagSubDir="' dagSubDir '"\n']);

    
    if zipSingleChFilesPreScriptBool
        % dataTypeStr=$1
        % varSelect=$2
        % dataID=$3
        % cond=$4
        % dagSubDir=$5
        % numFoldsLvl1=$6 # let's us know how many files there should be per channel during 2nd round of meas_err_stitch
        % numFoldsLvl2=$7 # let's us know how many files there should be per channel during 1st round of meas_err_stitch
        % lvl1kInd=$8 # only needed for 1st round of meas_err_stitch
        % cvType=$9
        % cvLevel=$10
        % nVar=$11
        % modelOrder=$12
        try
            if contains(fullJobName,'measOrderErr') || contains(fullJobName,'setModelOrder')
                lvl1kInd=nan;
            end
            scriptVarsStr=[dataTypeStr ' ' varSelect ' ' dataID ' ' cond ' ' dagSubDir ' ' ...
                num2str(numFoldsLvl1) ' ' num2str(numFoldsLvl2) ' ' num2str(lvl1kInd) ' ' cvType ... 
                ' ' num2str(cvLevel) ' ' num2str(nVar) ' ' num2str(modelOrder) ' ' bashInputOrigin ' ' num2str(nModelOrders) ' ' pars.dataType.catType];
        catch why
            keyboard
        end
        if ~pars.run.createDag.debias_measMOerr_only || (pars.run.createDag.debias_measMOerr_only && ~strcmp(bashInputOrigin,'debias'))
            fprintf(fileID, ['SCRIPT PRE ' shortName ' /home/endemann/ecog_connectivity/bashScripts/pre/zipAllSingleChOutput.sh ' scriptVarsStr '\n']);
        end
    end
end

