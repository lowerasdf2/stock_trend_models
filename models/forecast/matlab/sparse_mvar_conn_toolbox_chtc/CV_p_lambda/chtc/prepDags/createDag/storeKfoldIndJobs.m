function dagLines=storeKfoldIndJobs(globalJobVars,cvType,dagLines,pars,nVar,modelOrder,kInd)
if strcmp(cvType,'p')
    maxK=pars.cv.p.dimRed.ch.lasso.K;
    highestLvl=2;
else
    maxK=1;
    highestLvl=1;
end
if ~pars.run.createDag.lvl1CVonly || ~strcmp(cvType,'p')
    for kInd_pLasso=1:maxK
        kInd_pLassoVar=kInd_pLasso;
        for iVar=1:nVar
            if pars.run.createDag.singleVar.bool && (pars.run.createDag.singleVar.var~=iVar)
                continue
            end
            %% A1) lassoFit_singleVar jobs (lvl2cv)
            % program vars: $(cvType) $(modelOrder) $(lvl1kInd) $(kInd_pLasso) $(iVar) $(cvLevel)
            % transferFiles+initDir vars: $(dagSubdir),$(patient),$(cond),$(numFoldsLvl1),$(numFoldsLvl2),$(dataTypeStr),$(varSelect),$(pcaStr)
            cvLevel=highestLvl;
            if cvLevel == 1 
                kInd_pLassoVar=nan;
                summaryCVfile='summaryCVp';
            else
                summaryCVfile='pars';
            end
            if (kInd_pLasso == 1 && iVar == 1) || (pars.run.createDag.singleVar.bool && kInd_pLasso == 1 && iVar == pars.run.createDag.singleVar.ch)
                crash=false;
                % init structs
                try
                    x=dagLines.dependencies.lassoFit_singleVar.([cvType 'CV']).(['mo' num2str(modelOrder)]).(['k' num2str(kInd)]).(['cvLevel' num2str(cvLevel)]);
                    crash=true; % above assignment shouldn't work
                catch
                    dagLines.dependencies.lassoFit_singleVar.([cvType 'CV']).(['mo' num2str(modelOrder)]).(['k' num2str(kInd)]).(['cvLevel' num2str(cvLevel)])={};
                end
                if crash
                    crashNow
                end
            end
            jobName = ['bashInputOrigin$' pars.nicknames('lassoFit_singleVar') ...
                globalJobVars '_iVar$' num2str(iVar) '_cvType$' cvType '_cvLevel$' num2str(cvLevel)...
                '_modelOrder$' num2str(modelOrder)  '_lvl1kInd$' num2str(kInd) '_kIndPlasso$' num2str(kInd_pLassoVar) ...
                '_summaryCVfile$' summaryCVfile];
            dagLines.jobNames.lassoFit_singleVar{length(dagLines.jobNames.lassoFit_singleVar)+1}=jobName;
            dagLines.dependencies.lassoFit_singleVar.([cvType 'CV']).(['mo' num2str(modelOrder)]).(['k' num2str(kInd)]).(['cvLevel' num2str(cvLevel)]){length(dagLines.dependencies.lassoFit_singleVar.([cvType 'CV']).(['mo' num2str(modelOrder)]).(['k' num2str(kInd)]).(['cvLevel' num2str(cvLevel)]))+1}=jobName;

        end
    end
end
%% A1) lassoFit_singleVar jobs (lvl1cv)
for iVar = 1:nVar
    if pars.run.createDag.singleVar.bool && (pars.run.createDag.singleVar.ch~=iVar)
        continue
    end
    cvLevel=highestLvl-1;
    if cvLevel==0 && kInd>1
        continue
    end
    if cvLevel==0
        kIndVar=nan;
        summaryCVfile='summaryCVp';
    else
        kIndVar=kInd;
        summaryCVfile='pars';
    end

    kInd_pLasso=nan;
    if iVar == 1 || (pars.run.createDag.singleVar.bool && iVar == pars.run.createDag.singleVar.ch)
        % init structs
        crash=false;
        try
            x=dagLines.dependencies.lassoFit_singleVar.([cvType 'CV']).(['mo' num2str(modelOrder)]).(['k' num2str(kIndVar)]).(['cvLevel' num2str(cvLevel)]);
            crash=true; % shouldn't be able to perform above assignment
        catch
            dagLines.dependencies.lassoFit_singleVar.([cvType 'CV']).(['mo' num2str(modelOrder)]).(['k' num2str(kIndVar)]).(['cvLevel' num2str(cvLevel)])={};
        end
        if crash
            crashNow
        end
    end
    jobName = ['bashInputOrigin$' pars.nicknames('lassoFit_singleVar') ...
        globalJobVars '_iVar$' num2str(iVar) '_cvType$' cvType '_cvLevel$' num2str(cvLevel) ...
        '_modelOrder$' num2str(modelOrder)  '_lvl1kInd$' num2str(kIndVar) '_kIndPlasso$' num2str(kInd_pLasso) ...
        '_summaryCVfile$' summaryCVfile];
    dagLines.jobNames.lassoFit_singleVar{length(dagLines.jobNames.lassoFit_singleVar)+1}=jobName;
    try
        numFilesStored=length(dagLines.dependencies.lassoFit_singleVar.([cvType 'CV']).(['mo' num2str(modelOrder)]).(['k' num2str(kIndVar)]).(['cvLevel' num2str(cvLevel)]));
    catch why
        keyboard
    end
    dagLines.dependencies.lassoFit_singleVar.([cvType 'CV']).(['mo' num2str(modelOrder)]).(['k' num2str(kIndVar)]).(['cvLevel' num2str(cvLevel)]){numFilesStored+1}=jobName;
    %     end
end
%% (A2) stitchSingleVarParams_measureErr: Stick outputs together into one parameter file, measure error, save out avgCVerror varied by lambda (avg'd over 2nd layer of folds)
% program vars: $(cvType) $(cvLevel) $(modelOrder) $(lvl1kInd)
% transferFiles+initDir vars: $(dagSubdir),$(dataTypeStr),$(patient),$(cond),$(varSelect),$(pcaStr),$(numFoldsLvl1),$(numFoldsLvl2),$(kFolder),$(tarFileName)
if ~pars.run.createDag.singleVar.bool
    cvLevel=highestLvl;% this stitch uses models trained at 2nd layer of CV process
    if ~(cvLevel==1 && kInd>1) % this is so lambdaCv only runs this once after all kInds(lvl1) complete running
        if cvLevel==1
            summaryCVfile='summaryCVp';
            tarFileName='allFiles';
            kIndVar=nan;
        else
            summaryCVfile='pars';
            tarFileName=['k' num2str(kInd)];
            kIndVar=kInd;
        end

        bashInputOrigin=pars.nicknames('stitchSingleVarParams_measureErr');
        jobName = ['bashInputOrigin$' bashInputOrigin ...
            globalJobVars '_cvType$' cvType '_cvLevel$' num2str(cvLevel) ...
            '_modelOrder$' num2str(modelOrder)  '_lvl1kInd$' num2str(kIndVar) ...
            '_summaryCVfile$' summaryCVfile ...
            '_kFolder$' 'k' num2str(kIndVar) '_tarFileName$' tarFileName '_nModelOrders$' num2str(length(pars.cv.p.modelOrderRange))];
        dagLines.jobNames.stitchSingleVarParams_measureErr{length(dagLines.jobNames.stitchSingleVarParams_measureErr)+1}=jobName;
        crash=false;    
        try
            x=dagLines.dependencies.stitchSingleVarParams_measureErr.([cvType 'CV']).(['mo' num2str(modelOrder)]).(['k' num2str(kIndVar)]).(['cvLevel' num2str(cvLevel)]);
            crash=true; % should crash trying above
        catch
            dagLines.dependencies.stitchSingleVarParams_measureErr.([cvType 'CV']).(['mo' num2str(modelOrder)]).(['k' num2str(kIndVar)]).(['cvLevel' num2str(cvLevel)])={jobName};
        end
        if crash
            crashNow
        end
    end
end
%% (A3) Construct biased model params (concatenate fitted params across channels)
% program vars: $(cvType) $(cvLevel) $(modelOrder) $(lvl1kInd)
% transferFiles+initDir vars: $(dagSubdir),$(dataTypeStr),$(patient),$(cond),$(numFoldsLvl1),$(numFoldsLvl2),$(varSelect),$(pcaStr),$(kFolder),$(tarFileName)
% Using level1Fold for debiasingTrainSet and optimal lambdas (determined @stepB), debias model to construct final model @kInd
if ~pars.run.createDag.singleVar.bool
    cvLevel=highestLvl-1;% controls what kind of trainingSet to use for debiased param estimates
    if ~(cvLevel==0 && kInd>1) % this is so lambdaCv only runs this once after all kInds(lvl1) complete running
        bashInputOrigin=pars.nicknames('construct_debiased_model_wBestLambdas');
        if cvLevel == 0
            summaryCVfile='summaryCVp';
            kStr='';
            tarFileName='allFiles';
            kIndVar=nan;
        else
            summaryCVfile='pars';
            kStr=['K' num2str(kInd)];
            tarFileName=['k' num2str(kInd)];
            kIndVar=kInd;
        end
        jobName = ['bashInputOrigin$' bashInputOrigin globalJobVars ...
            '_cvType$' cvType '_cvLevel$' num2str(cvLevel) '_modelOrder$' ...
            num2str(modelOrder)  '_lvl1kInd$' num2str(kIndVar) ...
            '_kStr$' kStr '_summaryCVfile$' summaryCVfile ...
            '_kFolder$' 'k' num2str(kIndVar) '_tarFileName$' tarFileName '_nModelOrders$' num2str(length(pars.cv.p.modelOrderRange))];
        dagLines.jobNames.construct_debiased_model_wBestLambdas{length(dagLines.jobNames.construct_debiased_model_wBestLambdas)+1}=jobName;
        crash=false;
        try
            x=dagLines.dependencies.construct_debiased_model_wBestLambdas.([cvType 'CV']).(['mo' num2str(modelOrder)]).(['k' num2str(kIndVar)]).(['cvLevel' num2str(cvLevel)]);
            crash=true;
        catch
            dagLines.dependencies.construct_debiased_model_wBestLambdas.([cvType 'CV']).(['mo' num2str(modelOrder)]).(['k' num2str(kIndVar)]).(['cvLevel' num2str(cvLevel)])={jobName};
        end
        if crash
            crashNow
        end
    end
end
