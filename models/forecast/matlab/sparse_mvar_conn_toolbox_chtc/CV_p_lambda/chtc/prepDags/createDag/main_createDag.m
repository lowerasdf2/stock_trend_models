function main_createDag(pars,cond,nVars)
%% Global Vars
dagDir=getDagDir(pars,cond);
if ~exist(dagDir)
    mkdir(dagDir)
end
copyfile([getEpochSaveDir(pars) filesep getEpochFilename(pars)],[dagDir filesep getEpochFilename(pars)])
if ~pars.cv.nested.bool
    mkdir([dagDir filesep 'pCV'])
    file=[dagDir filesep 'pCV' filesep 'modelOrderErrs.tar.gz'];
    fileID=fopen(file, 'w');
    fclose(fileID);
end
%% save pars.mat file in save folder as DAG so that file can be loaded properly using submit file (avoiding having to list excessive number of params on submit file directly)
save([dagDir filesep 'pars.mat'],'pars')
directory=[dagDir filesep 'log'];
if ~exist(directory,'dir')
    mkdir(directory)
end
directory=[dagDir filesep 'err'];
if ~exist(directory,'dir')
    mkdir(directory)
end
directory=[dagDir filesep 'out'];
if ~exist(directory,'dir')
    mkdir(directory)
end

%% create nicknames for each jobtype in dag
pars=setJobNicknames(pars);

%% Define baseline location of submit files
if pars.cv.p.run && pars.cv.lambda.run
    chtcBaseDir='/home/endemann/ecog_connectivity/';
    createMasterDagAndSubdags(pars,dagDir,getDagDirName(pars),chtcBaseDir,cond,nVars);
else
    disp('TO-DO: code dag that optimizes single hyperparameter. Skipping dag-creation for now.')
end






