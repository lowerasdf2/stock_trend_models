function finalJobName=shortenJobName(job)

finalJobName=job;
unders=strfind(job,'_');
dashes=strfind(job,'$');
finalJobName=finalJobName([1,dashes(1)-1,dashes(1):end]);
for underInd=1:length(unders)
    newUnders=strfind(finalJobName,'_');
    newDashes=strfind(finalJobName,'$');
    finalJobName=finalJobName([1:newUnders(underInd)+1,newDashes(underInd+1)-1:end]);
end