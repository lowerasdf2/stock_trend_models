function printJobNames(fileID,subFileDir_or_dagDir,jobs,subFileName,subdagBool)

for iJob=1:length(jobs)
    job=jobs{iJob};
    if subdagBool
        fprintf(fileID, ['SUBDAG EXTERNAL ' job ' ' subFileDir_or_dagDir job '.dag\n']);
    else
        job=shortenJobName(job);
        fprintf(fileID, ['JOB ' job ' ' subFileDir_or_dagDir subFileName '.sub\n']);
    end
end