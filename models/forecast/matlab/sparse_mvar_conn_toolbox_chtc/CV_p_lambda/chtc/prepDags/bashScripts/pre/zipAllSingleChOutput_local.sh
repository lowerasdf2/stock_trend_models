#!/bin/bash
####################################################################################################
####################################################################################################
####################################################################################################
####################################################################################################
createFilePattern()
{
echo Running createFilePattern...
echo runType: $runType
filePattern=''
if [[ $runType == "stitchMeasErr" ]];then
    filePattern="${cvType}CV_singleVarParams_var${iCh}_lvl${cvLevel}fold_${fileEnd}.mat"
elif [[ $runType == "debias" ]];then
    if [[ $iCh == $maxIts ]];then
        if [[ $cvType == "lambda" ]];then
            filePattern="${cvType}CV_avgCvErrByLambda*.mat"
        else
            filePattern="${cvType}CV_avgCvErrByLambda_${fileEnd}.mat"
        fi
    else
        if [[ $cvLevel == 0 ]];then
            filePattern="${cvType}CV_singleVarParams_var${iCh}_${fileEnd}.mat"
        else
            filePattern="${cvType}CV_singleVarParams_var${iCh}_lvl${cvLevel}fold_${fileEnd}.mat"
        fi
    fi
elif [[ $runType == "measOrderErr" ]];then
    if [[ "$iCh" -gt "1" ]];then
        continue
    fi
    if [[ $numFoldsLvl2 == 0 ]];then
    	filePattern="${cvType}CV_fullyConnected_${fileEnd}.mat"
    else
    	filePattern="${cvType}CV_sparseModel_${fileEnd}.mat"
    fi
elif [[ $runType == "setModelOrder" ]];then
    if [[ "$iCh" -gt "1" ]];then
        continue
    fi
    filePattern="avgModelOrderErr${fileEnd}.mat"
else
    exit 1
fi
echo ...finished running createFilePattern.
}
####################################################################################################
####################################################################################################
####################################################################################################
####################################################################################################
echo begin script, numArgs = "$#"
if [[ $# == 15 ]]
then
    echo -e "nArgs is correct\n"
else
    echo "nArgs is incorrect. Exiting..."
    exit 1
fi
# Assign input vars
dataTypeStr=$1
chType=$2
patient=$3
cond=$4
dagSubDir=$5
numFoldsLvl1=$6 # let's us know how many files there should be per channel during 2nd round of meas_err_stitch
numFoldsLvl2=$7 # let's us know how many files there should be per channel during 1st round of meas_err_stitch
lvl1kInd=$8 # only needed for 1st round of meas_err_stitch
cvType=$9
cvLevel=${10}
nCh=${11}
modelOrder=${12}
runType=${13}
nModelOrders=${14}
catType=${15}
maxIts=$nCh
# print shell args
echo printing args...
echo dataTypeStr: $dataTypeStr
echo chType: $chType
echo patient: $patient
echo cond: $cond
echo dagSubDir: $dagSubDir
echo numFoldsLvl1: $numFoldsLvl1
echo numFoldsLvl2: $numFoldsLvl2
echo lvl1kInd: $lvl1kInd
echo cvType: $cvType
echo cvLevel: $cvLevel
echo nCh: $nCh
echo modelOrder: $modelOrder
echo runType: $runType
echo nModelOrders: $nModelOrders
echo maxIts: $maxIts
echo -e "done printing args.\n"
### necessary for counting files: https://stackoverflow.com/questions/11307257/is-there-a-bash-command-which-counts-files
shopt -s nullglob
### dir where file count will take place
origDir=$(pwd)
echo  "origDir: $origDir"
# dataDir="/home/endemann/ecog_connectivity/dags/$dataTypeStr/$chType/$catType/$patient/$cond/$dagSubDir/"
dataDir="A:/dags/$dataTypeStr/$chType/$catType/$patient/$cond/$dagSubDir/"
cdToTarDir="${dataDir}/${cvType}CV/p${modelOrder}/lvl${cvLevel}fold/"
echo  "cdToTarDir: $cdToTarDir"
tarFilesBool=true;
####################################################################################################
####################################################################################################
####################################################################################################
####################################################################################################
if [[ $cvType = "p" ]] # as opposed to 'lambda'
then
    if [[ $cvLevel == 2 ]] && [[ $runType == "stitchMeasErr" ]];then
        fileEnd="p${modelOrder}_K${lvl1kInd}_lwK*"
        nFilesPerCh=$numFoldsLvl2
        tarFileName="k${lvl1kInd}/"
    elif [[ $cvLevel == 1 ]];then
        if [[ $runType == "stitchMeasErr" ]] || [[ $runType == "debias" ]];then
            fileEnd="p${modelOrder}_K${lvl1kInd}"
            nFilesPerCh=1
            tarFileName="k${lvl1kInd}/"
        elif [[ $runType == "measOrderErr" ]];then
            fileEnd="p${modelOrder}_K*"
            nFilesPerCh=$numFoldsLvl1
            tarFileName="debiased/"
        else
            echo "Unexpected condition hit. Exiting at line 70..."
            exit 1
        fi
    elif [[ $runType == "setModelOrder" ]];then
        fileEnd="_p*"
        nFilesPerCh=$nModelOrders
        tarFileName="modelOrderErrs"
        cdToTarDir="$dataDir/${cvType}CV/"
    else
        echo "Unexpected condition hit. Exiting at line 79..."
        exit 1
    fi
elif [[ $cvType = "lambda" ]];then
    tarFileName="allFiles/"
    if [[ $cvLevel == 1 ]];then
        nFilesPerCh=$numFoldsLvl1
        fileEnd="p*_K*"
    elif [[ $cvLevel == 0 ]];then
        nFilesPerCh=1
        fileEnd="allData_p*"
    else
        echo "Unexpected condition hit. Exiting at line 91..."
        exit 1
    fi
else
    echo "Unexpected condition hit. Exiting at line 95..." 
    exit 1
fi
if [[ $runType == "debias" ]];then
    maxIts=$((maxIts+1))
fi
echo maxIts: $maxIts
for ((iCh=1;iCh<=maxIts;iCh++))
do
    if [[ $runType == "measOrderErr" ]] && [[ "$iCh" -gt "1" ]] 
    then
        continue
    fi
    ### Determine how many files/channel there should be
    # if [[ $cvType = "p" ]] # as opposed to 'lambda'
    # then
    #     nFilesPerCh=$numFoldsLvl2
    #     fileEnd="p${modelOrder}_K${lvl1kInd}_lwK*"
    #     tarFileName="k${lvl1kInd}"
    # else
    #     nFilesPerCh=$numFoldsLvl1
    #     fileEnd="p${modelOrder}_K*"
    #     tarFileName=""
    # 
    #debias,measOrderErr,setModelOrder,stitchMeasErr
    echo looping through channel ... $iCh
    createFilePattern
    echo "filePattern: $filePattern"
    dirFilePattern="${dataDir}${filePattern}"
    # dirFilePattern="${dataDir}${cvType}CV_singleChParams_ch${iCh}_lvl${cvLevel}fold_*_${dataTypeStr}_${chType}chs_*_${fileEnd}.mat"
    fileList=($dirFilePattern)
    nFiles=$(echo ${#fileList[@]})
    echo dataDir: "$dataDir"
    echo fileEnd: "$fileEnd"
    echo fileList: $fileList
    echo "fileList:" "$fileList"
    echo dirFilePattern: "$dirFilePattern"
    echo nFiles: $nFiles
    echo nFilesPerCh: $nFilesPerCh
    echo checking file count
    if [[ $nFiles != $nFilesPerCh ]] 
    then
        tarFilesBool=false;
        echo 'Incorrect number of files discovered'
        exit 1
    fi
    echo checking if bool was flipped
    if [[ $tarFilesBool == false ]] 
    then
        echo false
    else
        echo true
    fi
done

# create output dir and move files if all files are accounted for
totalFiles=0
for ((iCh=1;iCh<=maxIts;iCh++))
do
    if [[ $tarFilesBool == true ]]
    then
        if [[ $runType == "measOrderErr" ]] && [[ "$iCh" -gt "1" ]] 
        then
            continue
        fi
        if [[ $runType == "setModelOrder" ]] && [[ "$iCh" -gt "1" ]] 
        then
            continue
        fi
        echo looping through channel ... $iCh
        echo tarFiles is true...
        createFilePattern
        dirFilePattern="${dataDir}${filePattern}"
        echo dirFilePattern "$dirFilePattern"
        echo tarFilesBool=true,moving files for ch $iCh

        if [[ $runType == "setModelOrder" ]];then
            saveDir=$dataDir/${cvType}CV/$tarFileName/
        else
            saveDir=$dataDir/${cvType}CV/p${modelOrder}/lvl${cvLevel}fold/$tarFileName/
        fi
        ### if doesn't already exist, make directory to house cvType_cvLevel output
        if [ ! -d $saveDir ]; then
          mkdir -p $saveDir
        fi
        ### move files to above directory
        pathLen=${#dataDir} 
        for fullfilepath in $dirFilePattern; do
            filename=${fullfilepath:$pathLen}
            #echo moving file...
            echo filename: "$filename"
            mv $fullfilepath $saveDir/$filename
        done
        dirFilePattern="${saveDir}${filePattern}"
        ### check for proper number of files in new folder that I just moved files to
        echo dirFilePattern: "$dirFilePattern"
        fileList=($dirFilePattern)
        echo fileList: $fileList
        nFiles=$(echo ${#fileList[@]})
        echo nFiles: $nFiles
        if [[ $nFiles != $nFilesPerCh ]]
        then
            tarFilesBool=false;
            echo 'Incorrect number of files discovered after moving files...'
            echo $nFiles
            echo $nFilesPerCh
            exit 1
        fi
        # count total files
        ((totalFiles+=nFilesPerCh))
    fi
done
echo totalFiles: $totalFiles
### tar new folder
if [[ $tarFilesBool == true ]] 
then
    echo tarFiles set to true....
    if [[ $cvType = "p" ]] # as opposed to 'lambda'
    then
        if [[ $cvLevel == 2 ]] && [[ $runType == "stitchMeasErr" ]];then
            tarFileName="k${lvl1kInd}"
        elif [[ $cvLevel == 1 ]];then
            if [[ $runType == "stitchMeasErr" ]] || [[ $runType == "debias" ]];then
                tarFileName="k${lvl1kInd}"
            elif [[ $runType == "measOrderErr" ]];then
                tarFileName="debiased"
            else
                exit 1
            fi
        elif [[ $runType == "setModelOrder" ]];then
            tarFileName="modelOrderErrs"
        else
            exit 1
        fi
    elif [[ $cvType = "lambda" ]];then
        tarFileName="allFiles"
    else
        exit 1
    fi

    echo $cdToTarDir    
    cd $cdToTarDir
    echo tarFileName: "$tarFileName"
    echo taring file ...
    # first tar w/out removing files and check size of tar file is reasonable
    tar -czf ${tarFileName}.tar.gz $tarFileName
    filesSize=$(wc -c ${tarFileName}.tar.gz | awk '{print $1}')
    echo filesSize: $filesSize
    if [[ "$filesSize" -gt $((200 * $totalFiles)) ]] 
    then
        echo fileSize greather than 200 x totalFiles
    else
        echo fileSize suspiciously small. exiting...
        exit 1
    fi
    # now make copy of tar file in separate folder
    echo cdToTarDir: $cdToTarDir
    dirToCheckTarFile=${cdToTarDir}dirToCheckTarFile
    echo dirToCheckTarFile: $dirToCheckTarFile
    mkdir -p $dirToCheckTarFile
    cp $cdToTarDir/${tarFileName}.tar.gz $dirToCheckTarFile/${tarFileName}.tar.gz 
    cd $dirToCheckTarFile
    # untar copy and check number of files
    echo untarring stuff...
    tar --no-overwrite-dir -C $dirToCheckTarFile -zxvf ${tarFileName}.tar.gz 
# elif [[ $runType == "debias" ]];then
#     if [[ $iCh == $maxIts ]];then
#         if [[ $cvType == "lambda" ]];then
#             filePattern="${cvType}CV_avgCvErrByLambda*.mat"
#         else
#             filePattern="${cvType}CV_avgCvErrByLambda_${fileEnd}.mat"
#         fi
    # iCh='*' # so that I count all files when I create filePattern string
    # createFilePattern
    # echo "filePattern: $filePattern"
    # echo "dirToCheckTarFile: $dirToCheckTarFile"
    # echo "tarFileName: $tarFileName"
    if [[ $runType == "setModelOrder" ]] 
    then
        dirFilePattern="${dirToCheckTarFile}/${tarFileName}/avgModelOrderErr*"
    else
        dirFilePattern="${dirToCheckTarFile}/${tarFileName}/${cvType}CV*"
    fi
    echo dirFilePattern: "$dirFilePattern"
    fileList=($dirFilePattern)
    echo fileList: $fileList
    nFiles=$(echo ${#fileList[@]})
    echo nFiles: $nFiles
    if [[ $totalFiles == $nFiles ]]
    then
        # delete files
        echo removing dirs....
        echo $dirToCheckTarFile
        rm -r $dirToCheckTarFile
        echo $cdToTarDir/$tarFileName
        rm -r $cdToTarDir/$tarFileName
    else
        echo totalFiles does not match nFiles
        exit 1
    fi
    cd $origDir
fi
# find . -maxdepth 1 -name "pCV_singleChParams_ch1_lvl2fold_*_p1_K1_lwK*.mat" | wc -l