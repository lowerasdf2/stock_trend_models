function createSubFile_lassoFit_singleVar(pars,outputDir,condorUser)
%%
file=[outputDir filesep 'lassoFit_singleVar_v' num2str(pars.run.createDag.ver) '.sub'];
fileID=fopen(file, 'w');
fprintf(fileID, '# lassoFit_singleVar template submit file\n');
fprintf(fileID, 'universe=vanilla\n');
fprintf(fileID, 'log=log/lassoFit_singleVar$(iVar)_mo$(modelOrder)_$(cvType)$(cvLevel)_lvl1kInd$(lvl1kInd)_kIndPlasso$(kIndPlasso).log\n');
fprintf(fileID, 'error=err/lassoFit_singleVar$(iVar)_mo$(modelOrder)_$(cvType)$(cvLevel)_lvl1kInd$(lvl1kInd)_kIndPlasso$(kIndPlasso).err\n');
fprintf(fileID, 'output=out/lassoFit_singleVar$(iVar)_mo$(modelOrder)_$(cvType)$(cvLevel)_lvl1kInd$(lvl1kInd)_kIndPlasso$(kIndPlasso).out\n');
fprintf(fileID, ['executable=/home/' condorUser '/ecog_connectivity/executables/run_lassoFit_singleVar_v' num2str(pars.run.createDag.ver) '.sh\n']);
% cvType=varargin{1};
% modelOrder=varargin{2};
% lvl1kInd=varargin{3};
% kInd_pLasso=varargin{4};
% iVar=varargin{5};
% cvLevel=varargin{6};
%% program vars
fprintf(fileID, 'arguments=v94 $(cvType) $(modelOrder) $(lvl1kInd) $(kIndPlasso) $(iVar) $(cvLevel)\n');
fprintf(fileID, 'should_transfer_files=YES\n');
fprintf(fileID, 'when_to_transfer_output=ON_EXIT\n');
%% transferFiles+initDir vars: $(dagSubdir),$(dataID),$(cond),$(numFoldsLvl1),$(numFoldsLvl2),$(dataTypeStr),$(varSelect),$(pcaStr)
fprintf(fileID, ['initialDir=' getInitChtcDir() '\n']);
fprintf(fileID, ['transfer_input_files='... 
    'http://proxy.chtc.wisc.edu/SQUID/r2018a.tar.gz,' ...
    'pars.mat,' ...
    '$(summaryCVfile).mat,' ...
    '/home/endemann/ecog_connectivity/libs/libX11.so.6,/home/endemann/ecog_connectivity/libs/libICE.so.6,' ...
    '/home/endemann/ecog_connectivity/transfer_input_files/lassoFit_singleVar_v' num2str(pars.run.createDag.ver) ',' ...      dataEpochs_trainTestInds_epLen1.4_mins0-0.1_2folds_2subfolds_125hz_pca60
    'dataEpochs_trainTestInds_epLen$(epochLen)_$(dataSeg)$(numFoldsLvl1)folds_$(numFoldsLvl2)subfolds_$(sampleRate)hz$(pcaStr).mat\n']); 
fprintf(fileID, 'requirements=(OpSysMajorVer==7)\n');
fprintf(fileID, 'request_cpus=1\n');
fprintf(fileID, '+MemoryUsage=1\n');
fprintf(fileID, 'request_memory=MAX({4000, MemoryUsage * 3/2})\n');
fprintf(fileID, 'periodic_hold=(MemoryUsage >=( (RequestMemory) * 5/4 ) )  && (JobStatus==2)\n');
fprintf(fileID, 'periodic_release=(JobStatus==5) && ((CurrentTime - EnteredCurrentStatus) > 180) && NumJobStarts < 10 && (HoldReasonCode=!=13)\n');
fprintf(fileID, 'request_disk=8GB\n');% need at least 5.5 GB for Matlab, alone. 8 is probably sufficient unless we want to fit one model to all sleep data
fprintf(fileID, '+WantFlocking=true\n');% for HTCondor pools on campus
fprintf(fileID, '+WantGlidein=true\n');%f or Open Science Grid access
fprintf(fileID, 'queue 1');
fclose(fileID);

