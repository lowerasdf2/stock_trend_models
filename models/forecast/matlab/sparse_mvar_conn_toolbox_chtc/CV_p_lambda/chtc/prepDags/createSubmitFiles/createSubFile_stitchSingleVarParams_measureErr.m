function createSubFile_stitchSingleVarParams_measureErr(pars,outputDir,condorUser)
%%
file=[outputDir filesep 'stitchSingleVarParams_measureErr_v' num2str(pars.run.createDag.ver) '.sub'];
fileID=fopen(file, 'w');
fprintf(fileID, '# stitchSingleVarParams_measureErr template submit file\n');
fprintf(fileID, 'universe=vanilla\n');
fprintf(fileID, 'log=log/stitchSingleVarParams_measureErr_$(cvType)$(cvLevel)_mo$(modelOrder)_lvl1kInd$(lvl1kInd).log\n'); 
fprintf(fileID, 'error=err/stitchSingleVarParams_measureErr_$(cvType)$(cvLevel)_mo$(modelOrder)_lvl1kInd$(lvl1kInd).err\n');
fprintf(fileID, 'output=out/stitchSingleVarParams_measureErr_$(cvType)$(cvLevel)_mo$(modelOrder)_lvl1kInd$(lvl1kInd).out\n');
fprintf(fileID, ['executable=/home/' condorUser '/ecog_connectivity/executables/run_stitchSingleVarParams_measureErr_v' num2str(pars.run.createDag.ver) '.sh\n']);
%% program vars: $(cvType) $(modelOrder) $(lvl1kInd) $(cvLevel)
% cvType=varargin{1};
% modelOrder=varargin{2};
% level1kInd=varargin{3};
% cvLevel=varargin{4};
fprintf(fileID, 'arguments=v94 $(cvType) $(cvLevel) $(modelOrder) $(lvl1kInd)\n');
fprintf(fileID, 'should_transfer_files=YES\n');
fprintf(fileID, 'when_to_transfer_output=ON_EXIT\n');
%% transferFiles+initDir vars: $(dagSubdir),$(dataID),$(cond),$(numFoldsLvl1),$(numFoldsLvl2),$(dataTypeStr),$(varSelect),$(pcaStr),$(kFolder),$(tarFileName)
fprintf(fileID, ['initialDir=' getInitChtcDir() '\n']);
fprintf(fileID, ['transfer_input_files='... 
    'http://proxy.chtc.wisc.edu/SQUID/r2018a.tar.gz,' ...
    'pars.mat,' ...
    '/home/endemann/ecog_connectivity/libs/libX11.so.6,/home/endemann/ecog_connectivity/libs/libICE.so.6,' ...
    '$(summaryCVfile).mat,' ...
    '$(cvType)CV/p$(modelOrder)/lvl$(cvLevel)fold/$(tarFileName).tar.gz,' ...
    '/home/endemann/ecog_connectivity/transfer_input_files/stitchSingleVarParams_measureErr_v' num2str(pars.run.createDag.ver) ',' ...
    'dataEpochs_trainTestInds_epLen$(epochLen)_$(dataSeg)$(numFoldsLvl1)folds_$(numFoldsLvl2)subfolds_$(sampleRate)hz$(pcaStr).mat\n']); 
fprintf(fileID, 'requirements=(HasJava==true)&&(OpSysMajorVer==7)\n');
fprintf(fileID, 'request_cpus=1\n');
fprintf(fileID, '+MemoryUsage=1\n');
fprintf(fileID, 'request_memory=MAX({3000, MemoryUsage * 3/2})\n');
fprintf(fileID, 'periodic_hold=(MemoryUsage >=( (RequestMemory) * 5/4 ) )  && (JobStatus==2)\n');
fprintf(fileID, 'periodic_release=(JobStatus==5) && ((CurrentTime - EnteredCurrentStatus) > 180) && NumJobStarts < 10 && (HoldReasonCode=!=13)\n');
fprintf(fileID, 'request_disk=10GB\n');% need at least 5.5 GB for Matlab, alone. 8 is probably sufficient unless we want to fit one model to all sleep data
fprintf(fileID, '+WantFlocking=true\n');% for HTCondor pools on campus
% fprintf(fileID, '+WantGlidein=true\n');%f or Open Science Grid access
fprintf(fileID, 'queue 1');
fclose(fileID);
