function createSubFile_setModelOrder(pars,outputDir,condorUser)
%%
file=[outputDir filesep 'setModelOrder_v' num2str(pars.run.createDag.ver) '.sub'];
fileID=fopen(file, 'w');
fprintf(fileID, '# setModelOrder template submit file\n');
fprintf(fileID, 'universe=vanilla\n');
fprintf(fileID, 'log=log/setModelOrder.log\n');
fprintf(fileID, 'error=err/setModelOrder.err\n');
fprintf(fileID, 'output=out/setModelOrder.out\n');
fprintf(fileID, ['executable=/home/' condorUser '/ecog_connectivity/executables/run_setModelOrder_v' num2str(pars.run.createDag.ver) '.sh\n']);
%% program vars: none
fprintf(fileID, 'arguments=v94\n');
fprintf(fileID, 'should_transfer_files=YES\n');
fprintf(fileID, 'when_to_transfer_output=ON_EXIT\n');
%% transferFiles+initDir vars: $(dagSubdir),$(dataTypeStr),$(dataID),$(cond),$(numFoldsLvl1),$(numFoldsLvl2),$(varSelect),$(pcaStr),$(kFolder),$(tarFileName)
fprintf(fileID, ['initialDir=' getInitChtcDir() '\n']);
fprintf(fileID, ['transfer_input_files='... 
    'http://proxy.chtc.wisc.edu/SQUID/r2018a.tar.gz,' ...
    '/home/endemann/ecog_connectivity/libs/libX11.so.6,/home/endemann/ecog_connectivity/libs/libICE.so.6,' ...
    'pars.mat,' ...
    '$(cvType)CV/$(tarFileName).tar.gz,' ...
    '/home/endemann/ecog_connectivity/transfer_input_files/setModelOrder_v' num2str(pars.run.createDag.ver) '\n']); 
fprintf(fileID, 'requirements=(HasJava==true)&&(OpSysMajorVer==7)\n');
fprintf(fileID, 'request_cpus=1\n');
fprintf(fileID, '+MemoryUsage=1\n');
fprintf(fileID, 'request_memory=MAX({3000, MemoryUsage * 3/2})\n');
fprintf(fileID, 'periodic_hold=(MemoryUsage >=( (RequestMemory) * 5/4 ) )  && (JobStatus==2)\n');
fprintf(fileID, 'periodic_release=(JobStatus==5) && ((CurrentTime - EnteredCurrentStatus) > 180) && NumJobStarts < 10 && (HoldReasonCode=!=13)\n');
fprintf(fileID, 'request_disk=10GB\n');% need at least 5.5 GB for Matlab, alone. 8 is probably sufficient unless we want to fit one model to all sleep data
fprintf(fileID, '+WantFlocking=true\n');% for HTCondor pools on campus
% fprintf(fileID, '+WantGlidein=true\n');%f or Open Science Grid access
fprintf(fileID, 'queue 1');
fclose(fileID);
