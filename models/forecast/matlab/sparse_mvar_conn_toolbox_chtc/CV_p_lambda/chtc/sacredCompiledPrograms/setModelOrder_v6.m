function setModelOrder_v6()
%% load pars from pwd
load([pwd filesep 'pars.mat'])
if pars.cv.nested.bool || pars.cv.p.run
    %% init error vecs
    testErrorByMO=nan(length(pars.cv.p.modelOrderRange),1);
    trainErrorByMO=testErrorByMO;

    %% cycle through m.o. values and avg error over all kInds
    tarFolder='modelOrderErrs';
    untar([tarFolder '.tar.gz'])
    files=dir([tarFolder filesep '*.mat']);
    for iFile=1:length(files)
        movefile([tarFolder filesep files(iFile).name],files(iFile).name)
    end 
    rmdir([pwd filesep tarFolder])

    dataInd=0;
    summaryData=struct;
    for modelOrder=pars.cv.p.modelOrderRange
        dataInd=dataInd+1;
        summaryData(dataInd).modelOrder=modelOrder;
        saveFile=['avgModelOrderErr_p' num2str(modelOrder) '.mat'];
        load([pwd filesep saveFile],'errors')

        %% avg error over folds
        if ~(sum(sum(isnan(errors.testErrors))) == 0 && sum(sum(errors.testErrors==0)) == 0)
            errors.testErrors
            error('errors.testErrors contains nans or zeroes')
            crash
        elseif size(errors.testErrors,2) ~= pars.cv.p.K
            errors.testErrors
            error(['errors.testErrors contains ' num2str(size(errors.testErrors,2)) ' cols, but pars.cv.p.K=' num2str(pars.cv.p.K)])
        else
            avgTestErr=mean(errors.testErrors,2);
            avgTrainErr=mean(errors.trainErrors,2);
        end

        %% avg error across channels
        testErrorByMO(dataInd)=mean(avgTestErr);
        trainErrorByMO(dataInd)=mean(avgTrainErr);

        summaryData(dataInd).errors.testErrors=errors.testErrors;
        summaryData(dataInd).avgdOverFoldsTestErr=avgTestErr;
        summaryData(dataInd).avgdOverChsTestErr=testErrorByMO(dataInd);

        summaryData(dataInd).errors.trainErrors=errors.trainErrors;
        summaryData(dataInd).avgdOverFoldsTrainErr=avgTrainErr;
        summaryData(dataInd).avgdOverChsTrainErr=trainErrorByMO(dataInd);
    end
    %% find best model order programmically
    [minVal,minInd]=min(testErrorByMO);
    bestMO=pars.cv.p.modelOrderRange(minInd);
    disp(['Best model-order=' num2str(bestMO)])
elseif pars.cv.lambda.run
    bestMO=pars.cv.nested.manualMO;
    summaryData=[];
    disp(['Using manually set model-order=' num2str(bestMO)])
else 
    crash
end
%% save data
cv_saveFile=['summaryCVp.mat'];
save([pwd filesep cv_saveFile],'summaryData','bestMO')
load([pwd filesep cv_saveFile]) % ensure outfile is not corrupted

%% plot and save figs
if pars.cv.p.run
    cv_over_mo=figure;
    plot(pars.cv.p.modelOrderRange,trainErrorByMO,'-o')
    hold on
    plot(pars.cv.p.modelOrderRange,testErrorByMO,'-o')
    ylim([max(trainErrorByMO)*.5,testErrorByMO(1)*1.2])
    legend({'train','test'},'Location','SouthWest')
    suptitle([pars.dataType.dataID '-' pars.dataType.condition '-' pars.dataType.globalDataCat])
    saveas(cv_over_mo,[pwd filesep cv_saveFile(1:end-4) '.png'])
    savefig(cv_over_mo,[pwd filesep cv_saveFile(1:end-4) '.fig'])
    for iFile=1:length(files)
        delete([pwd filesep files(iFile).name])
    end
end


