function stitchSingleVarParams_measureErr_v6(varargin)
% varargin
cvType=varargin{1};
cvLevel=str2double(varargin{2});
modelOrder=str2double(varargin{3});
lvl1kInd=str2double(varargin{4});
if length(varargin) > 4
    varargin
    crash
end
% disp(['cvType=' cvType])
% disp(['modelOrder=' num2str(modelOrder)])
% disp(['lvl1kInd=' num2str(lvl1kInd)])
% disp(['cvLevel=' num2str(cvLevel)])

%% load pars from pwd
load([pwd filesep 'pars.mat'])

%% assign input vars to 'pars' structure. These input vars could be there own vars instead, 
% but this was more convenient given past code.Pars is not set before this 
% program due to fact that program runs as a standalone item w/in DAG pipeline
% modelOrder=modelOrder;
% kInd=kInd;
% cvLevel=cvLevel;
% clear modelOrder kInd cvLevel
if strcmp(cvType,'p')
    lambdaWeights=pars.cv.p.dimRed.ch.lasso.lambdaWeightRange(end):-pars.cv.p.dimRed.ch.lasso.inc:pars.cv.p.dimRed.ch.lasso.lambdaWeightRange(1);
elseif strcmp(cvType,'lambda')
    lambdaWeights=pars.cv.lambda.lambdaWeightRange(end):-pars.cv.lambda.inc:pars.cv.lambda.lambdaWeightRange(1);
else
    crash
end

%% set some local constants
np=length(lambdaWeights);
if cvLevel==2
    %% these params allow us to stitch together params estimated at all lambdas when using nested CV process
    kInds=[1:pars.cv.p.dimRed.ch.lasso.K];
    baseSaveFileName=['pCV'];
    disp(['Finding best lambdas via stitching together params estimated using nested CV, m.o.=' num2str(modelOrder) '.'])
elseif cvLevel==1
    %% these params allow us to stitch together params estimated when using entire fold of training data
    if pars.cv.lambda.K ~= pars.cv.p.K
        crash
    else
        kInds=[1:pars.cv.lambda.K];
    end
    if ~strcmp(cvType,'lambda')
        crash
    else
        baseSaveFileName=['lambdaCV'];
    end
    cv_saveFile=['summaryCVp.mat'];
    load([pwd filesep cv_saveFile],'summaryData','bestMO')
    modelOrder=bestMO;
elseif cvLevel==0
    crash % don't actually need this program for cvlevel0
end
plotParamsByLambda=false;

for kInd=kInds
    if cvLevel==1
        pars.cv.p.dimRed.ch.lasso.currK=nan;
        lvl1kInd=kInd;
    elseif cvLevel==2
        pars.cv.p.dimRed.ch.lasso.currK=kInd;
    else
        crash
    end

    %% prep subtrain set
    [trainSet,testSet]=splitDataEpochs(pars,cvType,lvl1kInd,pars.cv.p.dimRed.ch.lasso.currK,cvLevel);
    if kInd > 1
        plotParamsByLambda=false;
    else
        nVar=size(trainSet{1},1);
        %% init cumulative error over K_lasso folds, one error value estimated for each lambda
        modelOrderForEachCh=modelOrder*ones(nVar,1);
        testErrors=zeros(nVar,np); 
        trainErrors=zeros(nVar,np);
    end
    for s=1:length(trainSet)
        data=trainSet{s};
    %     data=data(1:nVar,:);%keyboard%0);% leave this for all parfor/var tests
        [Y Z]=buildMats(data,modelOrder); % builds design matrix (H) along with outcome variable matrix (Y); design matrix is size (T-p) * (nVar*p)
        if s == 1
            H_train=Z;
            Y_train=Y;
        else
            H_train=vertcat(H_train,Z);
            Y_train=vertcat(Y_train,Y);
        end
    end
    for s=1:length(testSet)
        data=testSet{s};
    %     data=data(1:nVar,:);%keyboard%0);% leave this for all parfor/var tests
        [Y Z]=buildMats(data,modelOrder); % builds design matrix (H) along with outcome variable matrix (Y); design matrix is size (T-p) * (nVar*p)
        if s == 1
            H_test=Z;
            Y_test=Y;
        else
            H_test=vertcat(H_test,Z);
            Y_test=vertcat(Y_test,Y);
        end
    end
    %% fill in sparseMap (cols arranged in blocks; each block representing one model; blocks arranged from most sparse models to least sparse models)
    sparseMap=buildSparseMap(pars,cvType,nVar,modelOrder,np,cvLevel,lvl1kInd);
    if plotParamsByLambda
        paramsByLambdaWeightFig_standView=figure('units','normalized','outerposition',[0 0 1 1]);
        paramsByLambdaWeightFig_connView=figure('units','normalized','outerposition',[0 0 1 1]);
        numCols=np;
        numRows=3;
        climBool=false;
    end
    for c=1:np
        if plotParamsByLambda
            if c == 1
                regularParamsMap=((H_train'*H_train)\(H_train'*Y_train)); 
                regularParamsMap_A_hat=nan(size(regularParamsMap'));
                for iVar=1:nVar
                    for iP=1:modelOrder
                        regularParamsMap_A_hat(iVar,[1:nVar]+((iP-1)*nVar))=regularParamsMap(iP:modelOrder:end,iVar);
                    end
                end  
            end
            lambdaWeight=lambdaWeights(c);
            %% plot SSE model  
            figure(paramsByLambdaWeightFig_standView)
            plotRow=1;
            plotCols=c;%1:np;
            plotInds = (numCols*plotRow)-(numCols-plotCols);
            subplot(numRows,np,plotInds)
            if climBool
                imagesc(regularParamsMap_A_hat,clim)
            else
                imagesc(regularParamsMap_A_hat)
            end
            set(gca, 'XTickLabel', [])    
            set(gca, 'YTickLabel', [])   
            if c == 1
                title({['SSE Optimized Params (no lasso penalty)']})
            end
            figure(paramsByLambdaWeightFig_connView)
            plotCols=c;%1:np;
            plotInds = (numCols*plotRow)-(numCols-plotCols);
            subplot(numRows,np,plotInds)
            if climBool
                imagesc(regularParamsMap,clim)
            else
                imagesc(regularParamsMap)
            end
            if c == 1
                title({['SSE Optimized Params (no lasso penalty)']})
            end
            set(gca, 'XTickLabel', [])    
            set(gca, 'YTickLabel', [])   
        end
        curA=sparseMap(:,(c-1)*nVar+1:c*nVar);
        curA_biased=curA;
        testErr=zeros(size(Y_test));
        trainErr=zeros(size(Y_train));
        %% Assume debiasing (replace non-zero coefs with simply least squares estimates of params)
        for d=1:nVar
            curA(:,d)=LargeLS2_orig(Y_train(:,d),H_train,1,modelOrderForEachCh,1,curA(:,d));
        end
        numConnections=sum(sum(curA~=0))/modelOrder;
        % testErr
        testErr=Y_test-H_test*curA;
        Qhat_test=testErr'*testErr;
        Q_hats_test{kInd,c}=Qhat_test;
        N_test{kInd,c}=size(testErr,1);
        testErrors(:,c)=testErrors(:,c)+(diag(Qhat_test)./N_test{kInd,c}); % diag(Qhat_test) is the same as sum(testErr.^2)
        % trainerr
        trainErr=Y_train-H_train*curA;
        Qhat_train=trainErr'*trainErr;
        Q_hats_train{kInd,c}=Qhat_train;
        N_train{kInd,c}=size(trainErr,1);
        trainErrors(:,c)=trainErrors(:,c)+(diag(trainErr'*trainErr)./N_train{kInd,c}); % this is the same as sum(err.^2)

        %% plot stuff
        if plotParamsByLambda
            % first convert to standard VAR model param display
            A_hat=nan(size(curA'));
            for iVar=1:nVar
                for iP=1:modelOrder
                    A_hat(iVar,[1:nVar]+((iP-1)*nVar))=curA(iP:modelOrder:end,iVar);
                end
            end  
            A_hat_biased=nan(size(curA'));
            for iVar=1:nVar
                for iP=1:modelOrder
                    A_hat_biased(iVar,[1:nVar]+((iP-1)*nVar))=curA_biased(iP:modelOrder:end,iVar);
                end
            end  

            figure(paramsByLambdaWeightFig_standView)
            plotRow=2;
            plotCol=c;
            plotInd = (numCols*plotRow)-(numCols-plotCol);
            subplot(numRows,np,plotInd)
    %         clim=[0,.75];
            if climBool
                imagesc(A_hat,clim)%,clim)
            else
                imagesc(A_hat)%,clim)
            end
            set(gca, 'XTickLabel', [])    
            set(gca, 'YTickLabel', [])   
    %         if plotCol == 1
            title({['lambdaWeight=' num2str(lambdaWeight)],['numConn''s=' num2str(numConnections)]})
    %         else
    %             title({['numConn''s=' num2str(numConnections)]})
    %         end

            figure(paramsByLambdaWeightFig_connView);
            plotRow=2;
            plotInd = (numCols*plotRow)-(numCols-plotCol);
            subplot(numRows,np,plotInd)
            if climBool
                imagesc(curA,clim);
            else
                imagesc(curA);
            end
            set(gca, 'XTickLabel', [])    
            set(gca, 'YTickLabel', [])   
            title({['lambdaWeight=' num2str(lambdaWeight)],['numConn''s=' num2str(numConnections)]})

            figure(paramsByLambdaWeightFig_connView);
            plotRow=3;
            plotInd = (numCols*plotRow)-(numCols-plotCol);
            subplot(numRows,np,plotInd)
            if climBool
                imagesc(curA_biased,clim);
            else
                imagesc(curA_biased);
            end
            if c ~= 1
                set(gca, 'XTickLabel', [])    
                set(gca, 'YTickLabel', [])   
            end
            if plotCol == 1
                title('biased version of above params')
            end

            figure(paramsByLambdaWeightFig_standView);
            plotRow=3;
            plotInd = (numCols*plotRow)-(numCols-plotCol);
            subplot(numRows,np,plotInd)
            if climBool
                imagesc(A_hat_biased,clim);
            else
                imagesc(A_hat_biased);
            end
            if c ~= 1
                set(gca, 'XTickLabel', [])    
                set(gca, 'YTickLabel', [])   
            end
            if plotCol == 1
                title('biased version of above params')
            end
        end
    end
    if plotParamsByLambda 
        figure(paramsByLambdaWeightFig_standView)
        suptitle(['Standard View of Params'])
        saveas(paramsByLambdaWeightFig_standView,[measureAvgModelOrderErr_v5.dirs.cv_saveDir filesep baseSaveFileName '_paramsByLambdaWeightFig_standView' createFileEnd_fitECoG(pars) '.png'])
        savefig(paramsByLambdaWeightFig_standView,[measureAvgModelOrderErr_v5.dirs.cv_saveDir filesep baseSaveFileName '_paramsByLambdaWeightFig_standView' createFileEnd_fitECoG(pars) '.fig'])
%             close(gcf)

        figure(paramsByLambdaWeightFig_connView)
        suptitle(['Channel-wise Connections View of Params'])
        saveas(paramsByLambdaWeightFig_connView, [measureAvgModelOrderErr_v5.dirs.cv_saveDir filesep baseSaveFileName '_paramsByLambdaWeightFig_chWiseConnView' createFileEnd_fitECoG(pars) '.png'])
        savefig(paramsByLambdaWeightFig_connView, [measureAvgModelOrderErr_v5.dirs.cv_saveDir filesep baseSaveFileName '_paramsByLambdaWeightFig_chWiseConnView' createFileEnd_fitECoG(pars) '.fig'])
%         close(gcf)
    end
end
% ^i=length(testErrors) is least sparse model
% ^i=1 is most sparse model
[chErr i]=min(testErrors,[],2);
errors.testErrorsAcrossFolds=testErrors;
errors.i=i;
errors.trainErrorsAcrossFolds=trainErrors;
errors.N_test=N_test;
errors.N_train=N_train;
errors.qHatsTest=Q_hats_test;
errors.qHatsTrain=Q_hats_train;
errors.errorOrder='moreSparse->lessSparse';
errors.sparseMapOrder='moreSparse->lessSparse';
if cvLevel==2
    %% these params allow us to stitch together params estimated at all lambdas when using nested CV process
    kAppend=['_K' num2str(lvl1kInd)];
elseif cvLevel==1
    %% these params allow us to stitch together params estimated when using entire fold of training data
    kAppend=[''];
elseif cvLevel==0
    crash % don't actually need this program for cvlevel0
end
saveFile=[baseSaveFileName '_avgCvErrByLambda_p' num2str(modelOrder) kAppend '.mat'];
save([pwd filesep saveFile],'errors')
load([pwd filesep saveFile]) % ensures file is not corrupted when job finishes

