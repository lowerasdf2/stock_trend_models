function [singleVarParams]=lassoFit_singleVar_v6(varargin)
% varargin
% for iVar=1:6
%     class(varargin{iVar})
% end
cvType=varargin{1};
modelOrder=str2double(varargin{2});
lvl1kInd=str2double(varargin{3});
kInd_pLasso=str2double(varargin{4});
iVar=str2double(varargin{5});
cvLevel=str2double(varargin{6});
% disp(['cvType=' cvType])
% disp(['modelOrder=' num2str(modelOrder)])
% disp(['lvl1kInd=' num2str(lvl1kInd)])
% disp(['kInd_pLasso=' num2str(kInd_pLasso)])
% disp(['iVar=' num2str(iVar)])
% disp(['cvLevel=' num2str(cvLevel)])
%% load pars from pwd
load([pwd filesep 'pars.mat'])
%% assign input vars to 'pars' structure. These input vars could be there own vars instead, 
% but this was more convenient given past code.Pars is not set before this 
% program due to fact that program runs as a standalone item w/in DAG pipeline
if strcmp(cvType,'p')
    kInd_p=lvl1kInd;
    lvl1kInd=kInd_p;
    lambdaWeightRange=pars.cv.p.dimRed.ch.lasso.lambdaWeightRange;
    lambdaIts=pars.cv.p.dimRed.ch.lasso.its;
    lambdaInc=pars.cv.p.dimRed.ch.lasso.inc;
    SC_bool=pars.cv.p.dimRed.ch.lasso.SC_bool;
elseif strcmp(cvType,'lambda')
    kInd_lambda=lvl1kInd;
    lvl1kInd=kInd_lambda;
    lambdaWeightRange=pars.cv.lambda.lambdaWeightRange;
    lambdaIts=pars.cv.lambda.its;
    lambdaInc=pars.cv.lambda.inc;
    SC_bool=pars.cv.lambda.SC_bool;
else
    crash
end
% kInd=kInd_p; % use 'kInd' instead since both cvLambda and cvMO use same var for foldInt
% kInd_pLasso=kInd_pLasso;
% iVar=iVar;
% cvLevel=cvLevel;
% if cv_level0, load best model order file
if strcmp(cvType,'lambda') && (cvLevel == 1 || cvLevel == 0)
    cv_saveFile=['summaryCVp.mat'];
    load([pwd filesep cv_saveFile],'summaryData','bestMO')
    modelOrder=bestMO;
elseif strcmp(cvType,'lambda')
    crash
end

%% load data epochs, split into train/test sets
[trainSet,testSet]=splitDataEpochs(pars,cvType,lvl1kInd,kInd_pLasso,cvLevel);

%% stimVec
if pars.modelParams.l~=0
    error('code is untested/wrong')
    u_singleTrial=zeros(size(trainSet{1},2)*(1000/pars.dataType.sampleRate),1);
    vowelOn=[1:100,150:250,300:400,450:550,600:700];%ms
    u_singleTrial(vowelOn)=1;  
    if pars.downsampleBool
        u_singleTrial=downsample(u_singleTrial,(1000/pars.dataType.sampleRate));
    end
    if length(u_singleTrial) ~= size(trainSet{1},2)
        keyboard
    end
    u = num2cell(repmat(u_singleTrial, 1, 1, length(trainSet)), [1, 2]);   
else
    % RS params
    u=zeros(size(trainSet{1},2),1);
    u = num2cell(repmat(u, 1, 1, length(trainSet)), [1, 2]);   
end

%% Fit data
disp('Running mvarx_fit_singleVar...')
[singleVarParams] = mvarx_lassoFit_singleVar(pars,trainSet,iVar,u,modelOrder,pars.modelParams.l,...
    SC_bool,lambdaInc,...
    lambdaIts,lambdaWeightRange);   

%% save out results
if cvLevel==0
    fileAppend='_allData';
    lwKstr='';
    kStr='';
elseif cvLevel==1
    fileAppend='_lvl1fold';
    lwKstr='';
    kStr=['_K' num2str(lvl1kInd)];
elseif cvLevel==2
    fileAppend='_lvl2fold';
    lwKstr=['_lwK' num2str(kInd_pLasso)];
    kStr=['_K' num2str(lvl1kInd)];
else
    crash
end

cv_saveFile=[cvType 'CV_singleVarParams_var' num2str(iVar) fileAppend ...
    '_p' num2str(modelOrder) kStr lwKstr '.mat'];
save([pwd filesep cv_saveFile],'singleVarParams')
load([pwd filesep cv_saveFile]) % will crash program when file saved out is corrupted


