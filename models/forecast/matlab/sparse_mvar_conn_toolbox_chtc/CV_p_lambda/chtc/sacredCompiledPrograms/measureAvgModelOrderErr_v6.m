function measureAvgModelOrderErr_v6(varargin)
modelOrder=str2double(varargin{1});
%% load pars from pwd
load([pwd filesep 'pars.mat'])
%% assign input vars to 'pars' structure. These input vars could be there own vars instead, 
% but this was more convenient given past code.Pars is not set before this 
% program due to fact that program runs as a standalone item w/in DAG pipeline
pars.cv.p.currMO=modelOrder;
%% set some local constants
cvLevel=1;
untar('debiased.tar.gz')
files=dir(['debiased' filesep '*.mat']);
for iFile=1:length(files)
    movefile(['debiased' filesep files(iFile).name],files(iFile).name)
end 
rmdir([pwd filesep 'debiased'])

% untar folder of final models of each pInd-kInd
for lvl1kInd=1:pars.cv.p.K
    %% load final model params for this particular fold of the data
    if pars.cv.p.dimRed.ch.lasso.bool
        startFile=['pCV_sparseModel'];
    else
        startFile=['pCV_fullyConnected'];
    end
    cv_saveFile=[startFile ...
            '_p' num2str(pars.cv.p.currMO) '_K' num2str(lvl1kInd) '.mat'];
    load([pwd filesep cv_saveFile],'Ahat_byCh','Ahat_lagBlks')
    
    if lvl1kInd==1
        %% init error vectors (measuring error for each outer fold i.e. the folds containing more data per fold)
        pars.dataType.nVar=size(Ahat_byCh,2);
        testErrors=zeros(pars.dataType.nVar,pars.cv.p.K); 
        trainErrors=zeros(pars.dataType.nVar,pars.cv.p.K);
    end
    
    %% construct train/test set & design matrices
    kInd_lasso=nan;
    [trainSet,testSet]=splitDataEpochs(pars,'p',lvl1kInd,kInd_lasso,cvLevel);        
    for s=1:length(trainSet)
        data=trainSet{s};
        [Y Z]=buildMats(data,pars.cv.p.currMO); % builds design matrix (H) along with outcome variable matrix (Y); design matrix is size (T-p) * (pars.dataType.nVar*p)
        if s == 1
            H_train=Z;
            Y_train=Y;
        else
            H_train=vertcat(H_train,Z);
            Y_train=vertcat(Y_train,Y);
        end
    end
    for s=1:length(testSet)
        data=testSet{s};
        [Y Z]=buildMats(data,pars.cv.p.currMO); % builds design matrix (H) along with outcome variable matrix (Y); design matrix is size (T-p) * (pars.dataType.nVar*p)
        if s == 1
            H_test=Z;
            Y_test=Y;
        else
            H_test=vertcat(H_test,Z);
            Y_test=vertcat(Y_test,Y);
        end
    end
    
    %% Calculate error
    testErr=Y_test-H_test*Ahat_byCh;
    Qhat_test=testErr'*testErr;
    Q_hats_test{lvl1kInd}=Qhat_test;
    N_test{lvl1kInd}=size(testErr,1);
    testErrors(:,lvl1kInd)=testErrors(:,lvl1kInd)+(diag(Qhat_test)./N_test{lvl1kInd}); % diag(Qhat_test) is the same as sum(testErr.^2)
    % trainErr
    trainErr=Y_train-H_train*Ahat_byCh;
    Qhat_train=trainErr'*trainErr;
    Q_hats_train{lvl1kInd}=Qhat_train;
    N_train{lvl1kInd}=size(trainErr,1);
    trainErrors(:,lvl1kInd)=trainErrors(:,lvl1kInd)+(diag(Qhat_train)./N_train{lvl1kInd}); % this is the same as sum(err.^2)

end
errors.trainErrors=trainErrors;
errors.Q_hats_train=Q_hats_train;
errors.N_train=N_train;
errors.testErrors=testErrors;
errors.Q_hats_test=Q_hats_test;
errors.N_test=N_test;

%% save out error varied by foldInd 
saveFile=['avgModelOrderErr_p' num2str(pars.cv.p.currMO) '.mat'];
save([pwd filesep saveFile],'errors')
load([pwd filesep saveFile]) % ensure outfile is not corrupted

for iFile=1:length(files)
    delete([pwd filesep files(iFile).name])
end 

