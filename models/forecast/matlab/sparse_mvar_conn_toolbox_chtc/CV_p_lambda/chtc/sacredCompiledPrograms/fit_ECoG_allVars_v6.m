function fit_ECoG_allVars_v6(p,cvLevel,kInd_p)    
%% set some vars
cvType='p'; % hardcoded
cvLevel=str2double(cvLevel);
if cvLevel == 0
    % load file containing best model order 
    cv_saveFile=['summaryCVp.mat'];
    load([pwd filesep cv_saveFile],'summaryData','bestMO')
    p=bestMO;
else
    p=str2double(p); % model order
end
kInd_p=str2double(kInd_p);
lvl1kInd=kInd_p;
kInd_pLasso=nan;

%% load pars from pwd
load([pwd filesep 'pars.mat'])

%% load data epochs, split into train/test sets
[trainSet,testSet]=splitDataEpochs(pars,cvType,lvl1kInd,kInd_pLasso,cvLevel);

%% stimVec
if pars.modelParams.l~=0
    error('code is untested and probably wrong') % unprepped
    u_singleTrial=zeros(size(trainSet{1},2)*(1000/pars.dataType.sampleRate),1);
    vowelOn=[1:100,150:250,300:400,450:550,600:700];%ms
    u_singleTrial(vowelOn)=1;  
    if pars.downsampleBool
        u_singleTrial=downsample(u_singleTrial,(1000/pars.dataType.sampleRate));
    end
    if length(u_singleTrial) ~= size(trainSet{1},2)
        keyboard
    end
    u = num2cell(repmat(u_singleTrial, 1, 1, length(trainSet)), [1, 2]);   
else
    % RS params
    u=zeros(size(trainSet{1},2),1);
    u = num2cell(repmat(u, 1, 1, length(trainSet)), [1, 2]);   
end

%% Fit data 
wavelet_bool=false;
[Ahat_lagBlks,B_hat,Q_hat,~,~,~,~,~]=mvarx_fit_allChs(trainSet, u, p, pars.modelParams.l, pars.modelParams.modelStimEffects,...
    wavelet_bool,pars.cv.plotParams_bool);
%% Convert to Ahat_byCh and save out 
Ahat_byCh=zeros(size(Ahat_lagBlks'));
nVar=size(Ahat_lagBlks,1);
for chBlk=1:nVar
    for interactCh=1:nVar
        for lag=1:p
            Ahat_byCh(((interactCh-1)*p)+(lag),chBlk)=Ahat_lagBlks(chBlk,interactCh+((lag-1)*nVar));
        end
    end
end 
% % % close all
% % % figure
% % % subplot(2,1,1)
% % % imagesc(Ahat_lagBlks)
% % % subplot(2,1,2)
% % % imagesc(Ahat_byCh)
% % % figure
% % % subplot(2,1,1)
% % % imagesc(x.Ahat_lagBlks)
% % % subplot(2,1,2)
% % % imagesc(x.Ahat_byCh)

%% Save data
% Outfile should look like this: pCV_p$(p)_K$(kInd_p).mat s.t. it matches pattern of output of construct_debiased_model, "pCV_sparseModel_p$(p)_K$(kInd_p).mat"
if cvLevel == 1 % measuring error @kInd, so program's level is set to 1, but errorVariedByLambda is determined by level2fold errors
    baseSaveFileName=[cvType 'CV'];
    kStr=['_K' num2str(lvl1kInd)];
elseif cvLevel == 0
    baseSaveFileName=[cvType 'CV'];
    kStr='';
else
    crash
end
if cvLevel == 1
    cv_saveFile=[baseSaveFileName '_fullyConnected' ...
            '_p' num2str(p) kStr];
    cv_saveFileFig=cv_saveFile;
    cv_saveFile=[cv_saveFile '.mat'];
elseif cvLevel == 0
    cv_saveFile=['finalFullyConnectedModel' ...
        '_p' num2str(p) '.mat'];
    cv_saveFileFig=['finalFullyConnectedModel' ...
        '_p' num2str(p)];
else
    crash
end
save([pwd filesep cv_saveFile],'Ahat_byCh','Ahat_lagBlks')
load([pwd filesep cv_saveFile]) % insures files aren't corrupt after being saved out

