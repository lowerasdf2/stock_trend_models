function [origDataSaveFile,dataSaveDir]=getDimRedDataFileNameAndDir(pars,cond)

dataSaveDir=[pars.dirs.dimRedData filesep pars.dataType.globalDataCat ...
    filesep pars.dataType.varSelect filesep pars.dataType.catType filesep  ...
    pars.dataType.dataID filesep cond filesep];%[pars.dirs.dataDir filesep patientParams.patientID filesep pars.dataType.condition filesep];
origDataSaveFile=['dimRedData'];
fileEnd=['_' num2str(pars.dataType.sampleRate) 'hz'];
if pars.cv.genDimRed.ch.PCA.run
    if strcmp(pars.cv.genDimRed.ch.PCA.threshType,'PERC')
        fileEnd=[fileEnd '_pca' num2str(pars.cv.genDimRed.ch.PCA.perc*100)];
    else
        keyboard
    end
end
origDataSaveFile=[origDataSaveFile fileEnd '.mat'];