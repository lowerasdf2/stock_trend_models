function fileExistsBool=dimRedDataFileExists(pars,conds)   
fileExistsBool=true;
for cond=conds
    cond=cond{1};
    [origDataSaveFile,dataSaveDir]=getDimRedDataFileNameAndDir(pars,cond);
    if ~exist([dataSaveDir filesep origDataSaveFile],'file')
        fileExistsBool=false;
    end
end

