function [pars]=setPars_modelOrder(pars,p_K,p_percData)
%% set CVp params
if pars.cv.p.run
    pars.cv.p.K=p_K;
    % pars.cv.p.modelOrderRange=p_mo_range;
    pars.cv.p.percData=p_percData;
    % % % end
end
%% set CV_p w/ lasso params
if pars.cv.p.dimRed.ch.lasso.bool
    pars=setPars_modelOrder_lasso(pars,p_K);
end