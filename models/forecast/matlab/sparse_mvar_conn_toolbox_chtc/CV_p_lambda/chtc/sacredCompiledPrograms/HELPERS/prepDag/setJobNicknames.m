function pars=setJobNicknames(pars)
pars.nicknames=containers.Map;
pars.nicknames('lassoFit_singleVar')='lassoFit';
pars.nicknames('stitchSingleVarParams_measureErr')='stitchMeasErr';
pars.nicknames('construct_debiased_model_wBestLambdas')='debias';
pars.nicknames('measureAvgModelOrderErr')='measOrderErr';
pars.nicknames('setModelOrder')='setModelOrder';