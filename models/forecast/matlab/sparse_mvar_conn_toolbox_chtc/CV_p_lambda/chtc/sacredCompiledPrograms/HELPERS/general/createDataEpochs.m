function [dataSaveDir,dataSaveFile]=createDataEpochs(pars)
%% load data saved out for each condition/pp
[origDataSaveFile,dataSaveDir]=getDimRedDataFileNameAndDir(pars,pars.dataType.condition);
if ~exist([dataSaveDir filesep origDataSaveFile],'file')
    keyboard
    error(['Need to create data input files: ' dataSaveDir filesep origDataSaveFile])
    crash
else
    load([dataSaveDir filesep origDataSaveFile])
end
dataInfo.varCat=dimRedCondData.varCat;
dataInfo.fsample=dimRedCondData.fsample;
if isfield(dimRedCondData,'trial_times')
    %% If any trials end after the recording is over, identify them.
    dimRedCondData.trials_to_keep=(dimRedCondData.trial_times + TrialProperties.Length) <= max(dimRedCondData.time);
    if any(~trials_to_keep)
        warning('%d trials extend past end of recording. Discarding these.', ...
                numel(dimRedCondData.trials_to_keep) - nnz(dimRedCondData.trials_to_keep));
    end
    dataInfo.trial_times=dimRedCondData.trial_times;
    dataInfo.trials_by_type=dimRedCondData.trials_by_type;
elseif strcmp(pars.dataType.globalDataCat,'OR-EV')
    keyboard
end

%% Store trial info
if strcmp(pars.dataType.globalDataCat,'OR-RS')
    rsData=true;
elseif strcmp(pars.dataType.globalDataCat,'OR-EV')
    rsData=false;
    keyboard % should I bother to store this trial info?
    cfg                 = [];
    cfg.trial_times     = trial_times(trials_to_keep);
    cfg.trials_by_type  = trials_by_type;
    pars.trials_by_type = trials_by_type;
    cfg.fsample         = dimRedCondData.fsample;
    cfg.fifth_vowel     = TrialProperties.Fifth_Vowel;
    cfg.trial_length    = TrialProperties.Length;
    [trl, trialinfo] = trials_iowa2fieldtrip(cfg);
    cfg.trialinfo       = trialinfo;
elseif strcmp(pars.dataType.globalDataCat,'minute')
    % do nothing
    rsData=true;
else
    error('Need to add another check to detect if data is resting state or not for loading purposes in below lines.')
end

%% If using shorter segment than experiment length, shorten data now
if pars.dataType.shortenSeg.bool
    startInd=(dimRedCondData.fsample*60*pars.dataType.shortenSeg.startMin)+1;
    finalInd=(dimRedCondData.fsample*60*pars.dataType.shortenSeg.segLen) + (dimRedCondData.fsample*60*pars.dataType.shortenSeg.startMin);
    disp(['Creating epochs to fit model to mins ' num2str(pars.dataType.shortenSeg.startMin) '-' ...
        num2str(pars.dataType.shortenSeg.startMin+pars.dataType.shortenSeg.segLen) ' min(s) of expt.'])
    dimRedCondData.data=dimRedCondData.data(:,startInd:finalInd);
end
%% Split up data into epochs (trials) by cfg.trial_length (driven) or by  pars.dataType.epochLen 
if rsData
    n_epoch = floor((size(dimRedCondData.data,2)./dimRedCondData.fsample)./ pars.dataType.epochLen);
else
    n_epoch=length(cfg.trial_times);
end
shiftSize=floor(pars.dataType.epochLen*dimRedCondData.fsample);

begInd=1;
endInd=shiftSize;
clear dataEpochs
if n_epoch < 1
    crash
end
for epochInd = 1:n_epoch
    try
        dataEpochs{epochInd}=dimRedCondData.data(:,begInd:endInd);
    catch why
        crash
    end
    begInd=begInd+shiftSize;
    endInd=endInd+shiftSize;
end

%% Remove outliers
% dataEpochs=
% mahalDist_epochOutliers(dataEpochs);
% keyboard % finalize outlier schtuff
disp('WARNING: Need to finish outlier detection in createDataEpochs.m file once I start processing new data...')
%% Create train/test indices
rng(1); % set seed
indices_cv_p=crossvalind('Kfold', n_epoch, pars.cv.p.K); 
% index corresponds to which fold to put observation in and act as test/validation set (rest of indices act as train)
nEpochPerFold=size(dataEpochs(indices_cv_p ~= 1),2);
if pars.cv.p.dimRed.ch.lasso.bool
    indices_cv_p_lasso=crossvalind('Kfold', nEpochPerFold,pars.cv.p.dimRed.ch.lasso.K); % index corresponds to which fold to put observation in and act as test/validation set (rest of indices act as train)
end
nVar=size(dataEpochs{1},1);
pars.dataType.nVar=nVar;
disp(['nVar=' num2str(nVar)])
dataSaveFile=getEpochFilename(pars);

%% check for error I encountered on Condor
if pars.cv.p.dimRed.ch.lasso.bool
    for kInd=1:pars.cv.p.K
        outerFoldTrainSet=dataEpochs(indices_cv_p~=kInd);
        if length(outerFoldTrainSet) == length(indices_cv_p_lasso)
            % standard
        elseif (length(outerFoldTrainSet)+1) == length(indices_cv_p_lasso)
            % this (i.e. indices_cv_p_lasso(1:end-1)) is needed sometimes for last fold of data due to rounding errors
        else 
            keyboard; % this case is unexpected. what's going on?
        end
    end
end
%% check if this file already exists; if it does, make sure that contents in existing file match these contents, otherwise keyboard and investigate why/how data/ROI-labels were updated
fullFileDir=[dataSaveDir filesep dataSaveFile];
if exist(fullFileDir,'file')
    x=load(fullFileDir);
    if x.n_epoch ~= n_epoch
        keyboard
    elseif norm(dataEpochs{1}-x.dataEpochs{1},'fro')>1e-7
        keyboard
    elseif ~isequal(indices_cv_p,x.indices_cv_p)
        keyboard
    elseif isfield(x,'indices_cv_p_lasso') && (~isequal(indices_cv_p_lasso,x.indices_cv_p_lasso))
        keyboard
    end
    if ~isfield(x,'dataInfo')
        save(fullFileDir,'dataEpochs','n_epoch','indices_cv_p','indices_cv_p_lasso','dataInfo')
    end
else
    if pars.cv.p.dimRed.ch.lasso.bool
        save(fullFileDir,'dataEpochs','n_epoch','indices_cv_p','indices_cv_p_lasso','dataInfo')
    else
        save(fullFileDir,'dataEpochs','n_epoch','indices_cv_p','dataInfo')
    end
end
% delete([dataSaveDir filesep origDataSaveFile])