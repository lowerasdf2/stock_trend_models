function displayParams(pars)

if pars.cv.p.run
    disp('CV.p config...')
    disp(['K_p=' num2str(pars.cv.p.K)])
    if pars.cv.p.dimRed.ch.lasso.bool == 1
        disp(['Determining best model-order to use via ' num2str(pars.cv.p.K) '-folds CV procedure w/ lasso'])
    else
        disp(['Determining best model-order to use via ' num2str(pars.cv.p.K) '-folds CV procedure'])
    end
    if pars.cv.p.dimRed.ch.lasso.bool == 1
        disp(['Lasso params: K_l=' num2str(pars.cv.p.dimRed.ch.lasso.K) ', its=' num2str(pars.cv.p.dimRed.ch.lasso.its) ', inc=' num2str(pars.cv.p.dimRed.ch.lasso.inc) ', weightRange=[' num2str(pars.cv.p.dimRed.ch.lasso.lambdaWeightRange(1)) ',' num2str(pars.cv.p.dimRed.ch.lasso.lambdaWeightRange(end)) ']'])   
    end
    disp('... end CV.p config.')
end
if pars.cv.lambda.run
    [fileEnd,genParams,loadParsStr,runParsStr]=createFileEnd_fitECoG(pars);
    %% display run params (CV_l)
    disp(['CV.lambda config...'])
    disp([runParsStr])
    disp('... end CV.lambda config.')
end
%% params that used to be looped through
if strcmp(pars.dataType.globalDataCat, 'OR-RS')
    disp('Running OR resting state data (no sleep, just anesth.).')
elseif strcmp(pars.dataType.globalDataCat, 'OR-EV')
    disp('Running OR evoked data.')
    if pars.stimOn
        disp(['Using l = ' num2str(pars.modelParams.l) ' for exogenous impulse response length.'])
    end
elseif strcmp(pars.dataType.dataID,'FOREX')
    disp('Testing 3rdParty dataset.')
else
    crash
end
if pars.cv.fowardChain_bool
    disp('Forward chaining is on.')
    if pars.cv.fowardChain_movingTarget
        disp('Moving target is on.')
    end
else
    disp('Forward chaining is off.')
end
if strcmp(pars.dataType.varSelect,'RESP')
    disp('Using only channels that respond to typical stim or deviant stim.')
elseif strcmp(pars.dataType.varSelect,'ALL')
    disp('Using all channels.')
elseif strcmp(pars.dataType.varSelect,'EXEMP')
    disp('Using "exemplary" channels.')
elseif strcmp(pars.dataType.varSelect,'AUDITORY')
    disp('Using "AUDITORY" channels.')
else
    disp(['Using "' pars.dataType.varSelect '" vars.'])
end
%% Display params that I'm not looping through (keep set one way)
% wavebases
if pars.cv.genDimRed.time.wavebases
    disp('Using wavelet bases for feature reduction (and running regular MVAR(X) for comparison).')
end
if pars.run.bools.pcaOnly
    crash
    disp('Running PCA only.')
end
% sample rate
if pars.dataType.downsampleBool
    disp(['Downsampled data to ' num2str(pars.dataType.sampleRate) ' Hz'])
else
    keyboard % I don't have orig samplerate stored anywhere.... maybe I should
end
