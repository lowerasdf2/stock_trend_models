function [tempMap,varargout]=stsincsolver3_orig(modelOrder,c,ind,useFreeParamMap,maxLambda,Y_full,Y,H,G,KH,KG,varargin)

% [BlockAnswer,options]=stsincsolver2(Y,H,G,KH,KG,varargin)
inc=0.01;
XT=zeros(size(H,2),size(G,2));
startL=1;
endL=0.1;
opts.disp=0; % should suppress eigs output, but doesn't
%l1=eigs(H*H'); l1=l1(1);
l1=eigs(H'*H,1,'LM',opts); % CME changed from H*H' to H'*H since answer is the same
% % % [b,l1b]=eigs(H*H',1,'LM',opts); % CME changed from H*H' to H'*H since answer is the same

l2=eig(G'*G); l2=max(l2);



if nargin>5
    options=varargin{1};
else
    %options=struct([]);
    options.createdby='stsincsolver.m';
end

stepSize=1/(l1*l2)*options.stepWeight; % ^ see above note
if ~isfield(options,'step')
    options.step=stepSize;
end

%%
nVar=size(Y_full,2);
chList=1:nVar;
atmp=sparse(modelOrder*nVar,1);
%% add self-connections for channels proceeding channel 'c' (acts as a rough
% estimate for the effect of certain channels[1 through c-1] ON channel
% 'c') prior to reducing params via lasso penalty
for c2=1:c-1
    ind2=(c2-1)*modelOrder+1:c2*modelOrder;
    atmp(ind2)=(H(:,ind2)'*H(:,ind2))\(H(:,ind2)'*Y_full(:,c2==chList)); 
end

atmp(ind)=(H(:,ind)'*H(:,ind))\(H(:,ind)'*Y); % solves univariate model (channel 'c''s self-connections
options.X=atmp;
if useFreeParamMap
    % assume useFreeParams
    options.freeParameterMap=zeros(nVar,1);
    options.freeParameterMap(c)=1;
end
options.lambdaMax=maxLambda;

%%
if ~isfield(options,'X')
    options.X=XT;
end
% if isfield(options,'lcurve')
%     lcflag=1;
%     options.lcurve=zeros(2,round((startL-endL)/inc)+1);
% end
if isfield(options,'startL')
    startL=options.startL;
end
if isfield(options,'endL')
    endL=options.endL;
end
% Extra condition to allow "backwards" solving
if isfield(options,'inc')
    inc=options.inc;
end
if startL==endL
    np=1;
else
    np=round((startL-endL)/inc+1);
end
tempMap=sparse(sum(KH),np*sum(KG));
if startL==endL
    % no lasso
    options.its=1;
else
    options.its=zeros(round((startL-endL)/inc+1),1);
end
% condition added Jan. 09
if isfield(options,'lambdaMax')
    M=options.lambdaMax;
else
    % condition added Jan. 09
    if isfield(options,'freeParameterMap')
        % ls solution with no reg.
        X=LargeLS2_orig(Y,H,G,KH,KG,options.freeParameterMap,inf);
        R=Y-H*X*G';
        M=stsmaxlambda_orig(H,G,R,KH,KG,options);
        options.X=X;
    else
        M=stsmaxlambda_orig(H,G,Y,KH,KG,options);
    end
end
% Next condition added June, 2009
if isfield(options,'verbose')
    verbose=options.verbose;
else
    verbose=1;
end

clear l1 l2 p XT
if verbose
    h=waitbar(0,sprintf('stsincsolver2.m Iterations (%d total)',np));
end

its=zeros(np,1);
lambdaWeights=endL:inc:startL;
lambdaWeights=startL:-inc:endL;%start w/ max sparsity
for trialType=2:2
    if trialType == 1
        lambdaWeights=endL:inc:startL;
    else
        lambdaWeights=startL:-inc:endL;
    end
%     tic
    for iTrials=1:1
        ind=1;
        for c=lambdaWeights % CME changed this so that I could see what smaller values of lambda do in the code; orig code is: startL:-inc:endL

            %[X,options]=emstsSpaRSA(Y,H,G,KH,KG,c*M,options);
            [X,options]=sts2_orig(Y,H,G,KH,KG,c*M,options);
            options.X=X;
            its(ind)=options.its;

            %BlockIndex(:,(ind-1)*length(KG)+1:ind*length(KG))=findblocks(options.X
            %,KH,KG);
            % This might cause memory issues.
            tempMap(:,(ind-1)*sum(KG)+1:ind*sum(KG))=options.X;

        %     if lcflag
        %         % Use lambda=1 so pen is not multiplied by lambda
        %         [cst mis pen]=stscost(options.X,Y,H,G,KH,KG,1);
        %         options.lcurve(:,ind)=[mis;pen];
        %     end
            ind=ind+1;
            if verbose
                waitbar(ind/np,h);
            end
        end
    end
%     toc
end

% figure
% imagesc(BlockAnswer(:,end-3:end)')
% colorbar

if verbose
    close(h)
end
options.its=its;
%save Tempres %plot(its)
if max(options.its)>=options.maxits
    t=find(options.its>=options.maxits,1,'last');
    disp(['Tolerance not met for some cases.  The last of these was ' num2str(t) ', lambdaWeight=' num2str(lambdaWeights(t))])
end
if nargout>1
    varargout{1}=options;
end