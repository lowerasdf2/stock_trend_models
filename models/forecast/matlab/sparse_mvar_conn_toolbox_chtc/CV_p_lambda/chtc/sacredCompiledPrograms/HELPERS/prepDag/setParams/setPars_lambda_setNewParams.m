function [pars]=setPars_lambda_setNewParams(pars,p_K)%cond_model_order,lambda_K,lambda_lambdaWeightRange,lambda_inc,lambda_its,lambda_percData,lambda_lasso_plotParamsByLambda,lambda_SC)
if pars.cv.lambda.run
    cond_model_order=1; % 0=avg over conditions (not an option for condor pipeline yet)
    lambda_K=p_K;%has to copy p_K to avoid having to save out an additional param in train/test indices file;
    lambda_lambdaWeightRange=[.1,1];%[.1,1];
    lambda_inc=.05;%.05;  % was .05
    if pars.dataType.USE_MIN_PARAMS
        lambda_its=50;% was 5000
    else
        lambda_its=5000;% was 5000
    end
    lambda_percData=1;
    lambda_lasso_plotParamsByLambda=0;
    lambda_SC=1;
    % % % % % %% Specify whether using CV_p or CV_lambda (OR-RS, OR-EV, CRU-RS-ZZZ, CRU-EV-ZZZ, CRU-RS, CRU-EV)
    % % % % % if strcmp(pars.runType,'LOCAL')
    % % % % %     if ~strcmp(pars.cv.lambda.modelOrderChoice,'Manual')
    % % % % %         %% give option to use cond. model order if not manually set
    % % % % %         prompt = {'Conditional MO','#Folds','Lambda weight range','Inc','Max Iterations','%training set to keep (0,100]','Plot params varied by lambda?'};
    % % % % %         defaultans = {'0','5','[.4,1]','.05','10000','100','1'};
    % % % % %         num_lines = [ones(size(defaultans')) ones(size(defaultans'))*120];
    % % % % %         Params = inputdlg(prompt, 'Set params for CV_lambda procedure.', num_lines, defaultans);
    % % % % %         pars.cv.lambda.condModelOrder=str2double(Params{1});
    % % % % %         pars.cv.lambda.K=str2double(Params{2});
    % % % % %         pars.cv.lambda.lambdaWeightRange=eval(Params{3});
    % % % % %         C2=num2cell(str2double(Params(4:end)));
    % % % % %         [pars.cv.lambda.inc,pars.cv.lambda.its,pars.cv.lambda.percData,pars.cv.lambda.lasso_plotParamsByLambda] = C2{:};
    % % % % %         pars.cv.lambda.percData=pars.cv.lambda.percData/100;
    % % % % %     else
    % % % % %         %% since manual model-order, don't give option to use conditional
    % % % % %         % model order
    % % % % %         prompt = {'#Folds','Lambda weight range','Inc','Max Iterations','%training set to keep (0,100]','Plot params varied by lambda?'};
    % % % % %         defaultans = {'5','[.4,1]','.05','10000','100','1'};
    % % % % %         num_lines = [ones(size(defaultans')) ones(size(defaultans'))*120];
    % % % % %         Params = inputdlg(prompt, 'Set params for CV_lambda procedure.', num_lines, defaultans);
    % % % % %         pars.cv.lambda.condModelOrder=0;
    % % % % %         pars.cv.lambda.K=str2double(Params{1});
    % % % % %         pars.cv.lambda.lambdaWeightRange=eval(Params{2});
    % % % % %         C2=num2cell(str2double(Params(3:end)));
    % % % % %         [pars.cv.lambda.inc,pars.cv.lambda.its,pars.cv.lambda.percData,pars.cv.lambda.lasso_plotParamsByLambda] = C2{:};
    % % % % %         pars.cv.lambda.percData=pars.cv.lambda.percData/100;
    % % % % %     end
    % % % % %     pars.cv.lambda.SC_bool=1; % don't leave this to choice unless there's a good reason
    % % % % % else
    pars.cv.lambda.condModelOrder=cond_model_order;
    pars.cv.lambda.K=lambda_K;
    pars.cv.lambda.lambdaWeightRange=lambda_lambdaWeightRange;
    pars.cv.lambda.inc=lambda_inc;
    pars.cv.lambda.its=lambda_its;
    pars.cv.lambda.percData=lambda_percData;
    pars.cv.lambda.lasso_plotParamsByLambda=lambda_lasso_plotParamsByLambda;
    pars.cv.lambda.SC_bool=lambda_SC;
    % % % % % end
end