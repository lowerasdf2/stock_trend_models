% function [A chErr i]=lmCVFit_orig(Y,H,modelOrder,nDiv,varargin)
function [chParamsVariedByLambda]=lmCVFit_orig_singleVar(iVar,stepWeight,...
    inc, maxits, lambda_weightRange,Y_train,H_train,modelOrder,varargin)
%
% Aest=lmCVFit(Y,H,modelOrder,nDiv,[chList],[SC])
%
% Solves Aest = argmin norm(Y-H*Aest,'fro')^2 + lambda * gL(Aest)
% using cross-validation to select lambda.  gL is a group Lasso penalty.
%
% Input:    Y, H        Matrices created by buildMats from time series
%                       data.
%           modelOrder  Model order (memory) of multivariate autoregressive
%                       system.
%           nDiv        Number of held out groups of data (i.e. 10 for
%                       tenfold CV).
%           chList      Specifies which channels to use (default 1:nNodes)
%
% Uses solver stsincsolver2.m
%
% Written by: Andrew Bolstad

% created from BlockCVFit on 2/24/2011 for tweaking

% addpath 'C:\Documents and Settings\Andrew\My Documents\SparseNet\Simulations\Free Parameter EM\'
%addpath 'C:\Documents and Settings\Andrew\My Documents\Free Parameter EM'

[T fCh]=size(Y_train);
[T2 nChp]=size(H_train);

if T~=T2
    error('Incompatable data sizes.')
end
clear T2
nVar=nChp/modelOrder;
if round(nVar)~=nVar
    error('Size of H does not agree with modelOrder.')
end

% set some params
options.verbose=0;
options.penalty=2;
options.useNbpen=0;
options.startL=lambda_weightRange(end); options.endL=lambda_weightRange(1);%.45;
options.inc=inc;
options.maxits=maxits;
options.stepWeight=stepWeight;
if options.startL==options.endL 
    % this case is for fully connected models
    np=1;
else
    np=round((options.startL-options.endL)/options.inc+1);
end
if fCh~=nVar
    crash % cme added this becuz assume I will never encounter this
    % don't do all channels
    chList=varargin{1};
    %mCh=setdiff(1:nVar,chList);
    Y2=zeros(T,nVar);
    Y2(:,chList)=Y_train;
    Yt=Y2;
    clear Y2
%     M=1.1*calcLmax2(Y2,H,modelOrder); % boost for trial var.
%     M=1.1*stsmaxlambda(H,G,Y2,KH,KG,options);
else
    chList=iVar;%1:nVar;
    Yt=Y_train;
%     M=1.1*calcLmax2(Y,H,modelOrder); % boost for trial var.
%     M=1.1*stsmaxlambda(H,G,Y,KH,KG,options);
end

useFreeParamMap=0;
if nargin>5
    if isnan(varargin{2}) || varargin{2} % isnan case corresponds to solving fully connected models (no lasso at all)
        useFreeParamMap=1;
    end
end

% add freeParamMap part (FIX!!!!)
modelOrderForEachCh=modelOrder*ones(nVar,1); % varName used to be KH
chBoolVec=ones(size(Y_train,2),1); % varName used to be KG
maxLambdas=zeros(nVar,1); %varName used to be 'M'
if useFreeParamMap
    X=zeros(size(H_train,2),nVar);
    for c=1:nVar
        % ls solution with no reg.
        freeParameterMap=zeros(nVar,1);
        freeParameterMap(c)=1;
        % Solve for params one channel at a time
        X(:,c)=LargeLS2_orig(Yt(:,c),H_train,1,modelOrderForEachCh,1,freeParameterMap,inf); % retrieves self-connected parameter estimates 
    end
    R=Y_train-H_train*X; % Uses this instead becuz all params are zero except self connections
% % %     R1=Y'-A_hat*H'; % this is wrong given format of H and manner in which params are solved for (using H's formatting)
% % %     srt=reshape(reshape(1:fCh*modelOrder,fCh,modelOrder)',fCh*modelOrder,1);
% % %     R2=Y-H*A_hat(:,srt)'; % R2_represenation = R_representation;
% % %     R3=Y-H*A_hat'; % R1 == R3; 
% % %     R=Y-H*X(srt,:); % This is incorrect because H is formatted with adjacent columns representing data from the same channel at different lags. This design matrix is used to solve for X, so need to keep X the same to calculate 1-step error
%% cme commented below line out since maxLambdas gets written over later
% % %     maxLambdas=stsmaxlambda_orig(H,size(Y,2),R,modelOrderForEachCh,chBoolVec,options);
    options.X=X;
else
    R=Y_train; % This is the error when all params are zero
end
for c=1:nVar
    maxLambdas(c)=1.1*stsmaxlambda_orig(H_train,1,R(:,c),modelOrderForEachCh,1,options);
%     maxLambdas(c)=1*stsmaxlambda_orig(H,1,R(:,c),modelOrderForEachCh,1,options);
end

% testing
% A=M;
% return

debias=1;

%% Calculate sparse answer on H8 -------------------------------


% Solve sparse problem for each column
sparseMap=sparse(modelOrder*nVar,np*fCh);
chParamsVariedByLambda={};
atmp=sparse(modelOrder*nVar,1);
modelOrderForEachCh=modelOrder*ones(nVar,1);


for c=chList 
    ind=(c-1)*modelOrder+1:c*modelOrder;
    [chParamsVariedByLambda{c}]=stsincsolver3_orig(modelOrder,c,ind,useFreeParamMap,maxLambdas(c),...
        Y_train,Y_train(:,c),H_train,1,modelOrderForEachCh,1,options);
end

for c = chList
    sparseMap(:,c:fCh:((np-1)*fCh+c))=chParamsVariedByLambda{c};
end
% % % 
% % % %% Find standard model params as well
% % % % regularParamsMap=((H_train'*H_train)\(H_train'*Y_train)); 
% % % regularParamsMap=((H_train'*H_train)\(H_train'*Y_train)); 
% % % regularParamsMap_A_hat=nan(size(regularParamsMap'));
% % % for iVar=1:nVar
% % %     for iP=1:modelOrder
% % %         regularParamsMap_A_hat(iVar,[1:nVar]+((iP-1)*nVar))=regularParamsMap(iP:modelOrder:end,iVar);
% % %     end
% % % end  
% % % % % %     figure
% % % % % %     subplot(1,2,1)
% % % % % %     imagesc(regularParamsMap_A_hat)
% % % % % %     subplot(1,2,2)
% % % % % %     imagesc(regularParamsMap)
% % % % % %     suptitle({['TrainSet Standard VAR Model Params, nVar=' num2str(nVar) ',p=' num2str(modelOrder)], [num2str(numConnections) ' conn.''s remain']})
% % % 
% % % if plotParamsByLambda
% % %     climBool=false;
% % %     if climBool
% % %         cMin=min(min(regularParamsMap_A_hat));
% % %         cMax=max(max(regularParamsMap));
% % %         clim=[cMin,cMax];
% % %     end
% % % end
% % % %% sparseMap stores param estimates as follows: first nVar columns contain param est.'s for all chans using minLambda, lambda gets smaller with each block of columns. Last block is maxLambda param estimates.
% % % % use sparseMap to calc error on validation data (H1,Y1)
% % % if plotParamsByLambda
% % %     paramsByLambdaWeightFig_standView=figure('units','normalized','outerposition',[0 0 1 1]);
% % %     paramsByLambdaWeightFig_connView=figure('units','normalized','outerposition',[0 0 1 1]);
% % %     numCols=np;
% % %     numRows=3;
% % %     lambdaWeights=lambda_weightRange(1):inc:lambda_weightRange(end);
% % % end
% % % for c=1:np
% % %     if plotParamsByLambda
% % %         lambdaWeight=lambdaWeights(c);
% % %         %% plot SSE model  
% % %         figure(paramsByLambdaWeightFig_standView)
% % %         plotRow=1;
% % %         plotCols=c;%1:np;
% % %         plotInds = (numCols*plotRow)-(numCols-plotCols);
% % %         subplot(numRows,np,plotInds)
% % %         if climBool
% % %             imagesc(regularParamsMap_A_hat,clim)
% % %         else
% % %             imagesc(regularParamsMap_A_hat)
% % %         end
% % %         set(gca, 'XTickLabel', [])    
% % %         set(gca, 'YTickLabel', [])   
% % %         if c == 1
% % %             title({['SSE Optimized Params (no lasso penalty)']})
% % %         end
% % %         figure(paramsByLambdaWeightFig_connView)
% % %         plotCols=c;%1:np;
% % %         plotInds = (numCols*plotRow)-(numCols-plotCols);
% % %         subplot(numRows,np,plotInds)
% % %         if climBool
% % %             imagesc(regularParamsMap,clim)
% % %         else
% % %             imagesc(regularParamsMap)
% % %         end
% % %         if c == 1
% % %             title({['SSE Optimized Params (no lasso penalty)']})
% % %         end
% % %         set(gca, 'XTickLabel', [])    
% % %         set(gca, 'YTickLabel', [])   
% % %     end
% % %     curA=sparseMap(:,(c-1)*fCh+1:c*fCh);
% % %     curA_biased=curA;
% % %     testErr=zeros(size(Y_test));
% % %     trainErr=zeros(size(Y_train));
% % %     %% Assume debiasing (replace non-zero coefs with simply least squares estimates of params)
% % %     for d=1:fCh
% % %         curA(:,d)=LargeLS2_orig(Y_train(:,d),H_train,1,modelOrderForEachCh,1,curA(:,d));
% % %     end
% % %     numConnections=sum(sum(curA~=0))/modelOrder;
% % %     % testErr
% % %     testErr=Y_test-H_test*curA;
% % %     Qhat_test=testErr'*testErr;
% % %     Q_hats_test{tSet,c}=Qhat_test;
% % %     testErrors(:,c)=testErrors(:,c)+diag(Qhat_test); % diag(Qhat_test) is the same as sum(testErr.^2)
% % %     % trainerr
% % %     trainErr=Y_train-H_train*curA;
% % %     Qhat_train=trainErr'*trainErr;
% % %     Q_hats_train{tSet,c}=Qhat_train;
% % %     trainErrors(:,c)=trainErrors(:,c)+diag(trainErr'*trainErr); % this is the same as sum(err.^2)
% % % 
% % %     %% plot stuff
% % %     if plotParamsByLambda
% % %         % first convert to standard VAR model param display
% % %         A_hat=nan(size(curA'));
% % %         for iVar=1:nVar
% % %             for iP=1:modelOrder
% % %                 A_hat(iVar,[1:nVar]+((iP-1)*nVar))=curA(iP:modelOrder:end,iVar);
% % %             end
% % %         end  
% % %         A_hat_biased=nan(size(curA'));
% % %         for iVar=1:nVar
% % %             for iP=1:modelOrder
% % %                 A_hat_biased(iVar,[1:nVar]+((iP-1)*nVar))=curA_biased(iP:modelOrder:end,iVar);
% % %             end
% % %         end  
% % % 
% % %         figure(paramsByLambdaWeightFig_standView)
% % %         plotRow=2;
% % %         plotCol=c;
% % %         plotInd = (numCols*plotRow)-(numCols-plotCol);
% % %         subplot(numRows,np,plotInd)
% % % %         clim=[0,.75];
% % %         if climBool
% % %             imagesc(A_hat,clim)%,clim)
% % %         else
% % %             imagesc(A_hat)%,clim)
% % %         end
% % %         set(gca, 'XTickLabel', [])    
% % %         set(gca, 'YTickLabel', [])   
% % % %         if plotCol == 1
% % %         title({['lambdaWeight=' num2str(lambdaWeight)],['numConn''s=' num2str(numConnections)]})
% % % %         else
% % % %             title({['numConn''s=' num2str(numConnections)]})
% % % %         end
% % % 
% % %         figure(paramsByLambdaWeightFig_connView);
% % %         plotRow=2;
% % %         plotInd = (numCols*plotRow)-(numCols-plotCol);
% % %         subplot(numRows,np,plotInd)
% % %         if climBool
% % %             imagesc(curA,clim);
% % %         else
% % %             imagesc(curA);
% % %         end
% % %         set(gca, 'XTickLabel', [])    
% % %         set(gca, 'YTickLabel', [])   
% % %         title({['lambdaWeight=' num2str(lambdaWeight)],['numConn''s=' num2str(numConnections)]})
% % % 
% % % %         if plotCol == 1
% % % %             title({'chWise-conn''s','debiased'})
% % % %         end
% % % 
% % %         figure(paramsByLambdaWeightFig_connView);
% % %         plotRow=3;
% % %         plotInd = (numCols*plotRow)-(numCols-plotCol);
% % %         subplot(numRows,np,plotInd)
% % %         if climBool
% % %             imagesc(curA_biased,clim);
% % %         else
% % %             imagesc(curA_biased);
% % %         end
% % %         if c ~= 1
% % %             set(gca, 'XTickLabel', [])    
% % %             set(gca, 'YTickLabel', [])   
% % %         end
% % %         if plotCol == 1
% % %             title('biased version of above params')
% % %         end
% % % 
% % %         figure(paramsByLambdaWeightFig_standView);
% % %         plotRow=3;
% % %         plotInd = (numCols*plotRow)-(numCols-plotCol);
% % %         subplot(numRows,np,plotInd)
% % %         if climBool
% % %             imagesc(A_hat_biased,clim);
% % %         else
% % %             imagesc(A_hat_biased);
% % %         end
% % %         if c ~= 1
% % %             set(gca, 'XTickLabel', [])    
% % %             set(gca, 'YTickLabel', [])   
% % %         end
% % %         if plotCol == 1
% % %             title('biased version of above params')
% % %         end
% % %     end
% % % 
% % % % % %         if c == 1
% % % % % %             figure
% % % % % %             imagesc(curA_biased)
% % % % % %             title('"biased" parameters')
% % % % % %             figure
% % % % % %             imagesc(curA)
% % % % % %             title('"debiased" parameters')        
% % % % % %         end
% % % %         if c == np
% % % %             colorbar
% % % %         end
% % % 
% % %     if c == 1 && plotParamsByLambda
% % % % % %             figure
% % % % % %             if climBool
% % % % % %                 imagesc(A_hat,clim)
% % % % % %             else
% % % % % %                 imagesc(A_hat)
% % % % % %             end
% % % % % %             title('sparse model with lambda=0 trained on one fold of trainSet')
% % %     end
% % % end
% % % if plotParamsByLambda 
% % %     figure(paramsByLambdaWeightFig_standView)
% % %     suptitle(['Standard View of Params, stepWeight=' num2str(stepWeight)])
% % %     saveas(paramsByLambdaWeightFig_standView,[plotSaveDir filesep 'p' num2str(modelOrder) '_paramsByLambdaWeightFig_standView_' cv_saveFileName '.png'])
% % %     savefig(paramsByLambdaWeightFig_standView,[plotSaveDir filesep 'p' num2str(modelOrder) '_paramsByLambdaWeightFig_standView_' cv_saveFileName '.fig'])
% % % %         close(gcf)
% % % 
% % %     figure(paramsByLambdaWeightFig_connView)
% % %     suptitle(['Channel-wise Connections View of Params, stepWeight=' num2str(stepWeight)])
% % %     saveas(paramsByLambdaWeightFig_connView, [plotSaveDir filesep 'p' num2str(modelOrder) '_paramsByLambdaWeightFig_chWiseConnView_' cv_saveFileName '.png'])
% % %     savefig(paramsByLambdaWeightFig_connView, [plotSaveDir filesep 'p' num2str(modelOrder) '_paramsByLambdaWeightFig_chWiseConnView_' cv_saveFileName '.fig'])
% % % %         close(gcf)
% % % end
% % % 
% % % 
% % % %% post-training stuff (make different function for stuff below this line)
% % % % end
% % % [chErr i]=min(testErrors,[],2);
% % % %errors, t, i
% % % 
% % % %% now that we know which lambda values to use, can solve for params using all data 
% % % sparseMap2=sparseMap; % CME saving old version for comparison purposes
% % % sparseMap=sparse(modelOrder*nVar,np*fCh);
% % % tempMap=sparse(modelOrder*nVar,np);
% % % atmp=sparse(modelOrder*nVar,1);
% % % modelOrderForEachCh=modelOrder*ones(nVar,1);
% % % % % % for c=chList
% % % % % %     options.lambdaMax=maxLambdas(c);
% % % % % %     ind=(c-1)*modelOrder+1:c*modelOrder;
% % % % % %     atmp(ind)=(H(:,ind)'*H(:,ind))\(H(:,ind)'*Y(:,c==chList));
% % % % % %     
% % % % % %     options.X=atmp;
% % % % % %     
% % % % % %     if useFreeParamMap
% % % % % %         % assume useFreeParams
% % % % % %         options.freeParameterMap=zeros(nVar,1);
% % % % % %         options.freeParameterMap(c)=1;
% % % % % %     end
% % % % % %     
% % % % % %     tempMap=stsincsolver2_orig(Y(:,c==chList),H,1,modelOrderForEachCh,1,options);
% % % % % %     sparseMap(:,c:fCh:((np-1)*fCh+c))=tempMap;
% % % % % % end
% % % sparseMap3={};
% % % try
% % %     parfor c=chList    % 1:fCh
% % %         ind=(c-1)*modelOrder+1:c*modelOrder;
% % %         sparseMap3{c}=stsincsolver3_orig(modelOrder,c,ind,useFreeParamMap,maxLambdas(c),...
% % %             Y,Y(:,c==chList),H,1,modelOrderForEachCh,1,options);
% % %     end
% % % catch why
% % %     disp('final parfor loop crashed. using serial loop instead.')
% % %     for c=chList    % 1:fCh
% % %         ind=(c-1)*modelOrder+1:c*modelOrder;
% % %         sparseMap3{c}=stsincsolver3_orig(modelOrder,c,ind,useFreeParamMap,maxLambdas(c),...
% % %             Y,Y(:,c==chList),H,1,modelOrderForEachCh,1,options);
% % %     end
% % % end
% % % for c = chList
% % %     sparseMap(:,c:fCh:((np-1)*fCh+c))=sparseMap3{c};
% % % end
% % % 
% % % %% Use final sparse model to decide which connections to keep. Solve for remaining connections using least squares (LargeLS2_orig).
% % % A=sparse(nChp,fCh);
% % % for d=1:fCh
% % %     % (i(d)-1)*fCh+1:i(d)*fCh
% % %     curA=sparseMap(:,chList(d)+(i(d)-1)*fCh);
% % %     A(:,d)=LargeLS2_orig(Y(:,d),H,1,modelOrderForEachCh,1,curA);
% % %     numConnections=sum(sum(A~=0))/modelOrder;
% % % end
% % % 
% % % % % % figure
% % % % % % subplot(2,1,1)
% % % % % % imagesc(A)
% % % % % % title('final-debiased')
% % % % % % subplot(2,1,2)
% % % % % % imagesc(sparseMap(:,chList+(i-1)*fCh))
% % % % % % title('final-biased')
% % % 
% % % testErr_final=Y-H*A;
% % % Q_hat_final_test=testErr_final'*testErr_final;
% % % 
% % % cvLambdaData.winningLambdaInds=i;
% % % cvLambdaData.cvTestErrors=testErrors;
% % % cvLambdaData.cvTrainErrors=trainErrors;
% % % cvLambdaData.finalSparseMap=sparseMap;
% % % cvLambdaData.finalOneStepErr=testErr_final;
% % % cvLambdaData.Q_hat_final_test=Q_hat_final_test;
% % % cvLambdaData.Q_hats_test=Q_hats_test;
% % % cvLambdaData.Q_hats_train=Q_hats_train;
% % % cvLambdaData.maxLambdas=maxLambdas;
% % % 
% % % % % % plotA=nan(size(A'));
% % % % % % for iVar=1:nVar
% % % % % %     for iP=1:modelOrder
% % % % % %         plotA(iVar,[1:nVar]+((iP-1)*nVar))=A(iP:modelOrder:end,iVar);
% % % % % %     end
% % % % % % end  
% % % % % % figure
% % % % % % subplot(1,2,1)
% % % % % % imagesc(plotA)
% % % % % % subplot(1,2,2)
% % % % % % imagesc(A)
% % % % % % colorbar
% % % % % % suptitle(['final sparse model, ' num2str(numConnections) ' conn.''s remain'])
% % % 
% % % %% Find standard model params as well
% % % % % % % regularParamsMap=((H_train'*H_train)\(H_train'*Y_train)); 
% % % % % % regularParamsMap=((H'*H)\(H'*Y)); 
% % % % % % numConnections=sum(sum(regularParamsMap~=0))/modelOrder;
% % % % % % regularParamsMap_A_hat=nan(size(regularParamsMap'));
% % % % % % for iVar=1:nVar
% % % % % %     for iP=1:modelOrder
% % % % % %         regularParamsMap_A_hat(iVar,[1:nVar]+((iP-1)*nVar))=regularParamsMap(iP:modelOrder:end,iVar);
% % % % % %     end
% % % % % % end  
% % % % % % figure
% % % % % % subplot(1,2,1)
% % % % % % imagesc(regularParamsMap_A_hat)
% % % % % % subplot(1,2,2)
% % % % % % imagesc(regularParamsMap)
% % % % % % suptitle({['Standard VAR Model Params, nVar=' num2str(nVar) ',p=' num2str(modelOrder)], [num2str(numConnections) ' conn.''s remain']})
% % % % % % close all
