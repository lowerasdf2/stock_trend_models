function [pars]=setPars_PCA(pars,params)

%% Specify whether using PCA or not
if strcmp(pars.config.runType,'LOCAL')
    prompt = {'PCA (0/1)', 'PCA_perc'};
    defaultans = {'1','.70'};
    num_lines = [ones(size(defaultans')) ones(size(defaultans'))*40];
    Params = inputdlg(prompt, 'Set PCA params', num_lines, defaultans);
    pars.cv.genDimRed.ch.PCA.run=str2double(Params(1));
    pars.cv.genDimRed.ch.PCA.perc=str2double(Params(2));
else
    pars.cv.genDimRed.ch.PCA.run=params{4};
    pars.cv.genDimRed.ch.PCA.perc=params{5};
end
if length(pars.cv.genDimRed.ch.PCA.run) == 1
    pars.cv.genDimRed.ch.PCA.run=pars.cv.genDimRed.ch.PCA.run;
else
    crash
end
