function [pars]=setPars_varSelect(pars,params)

%% Specify whether using CV_p or CV_lambda (OR-RS, OR-EV, CRU-RS-ZZZ, CRU-EV-ZZZ, CRU-RS, CRU-EV)
if strcmp(pars.config.runType,'LOCAL')
    %% Set channel list keys to loop through ('ALL','RESP','EXEMP')
    % ALL=all channels
    % RESP=stim/dev responsive channels
    % EXEMP=exemplary channels
    % pars.dataType.varSelects={'ALL','RESP'};%'EXEMP'
    % pars.dataType.varSelect={'RESP'};%,'ALL'};%'EXEMP'
    prompt = {'varSelect {''ALLVARS'',''RESP'',''EXEMP'',''AUDITORY''}'};
    defaultans = {'{''AUDITORY''}'};
    num_lines = [ones(size(defaultans')) ones(size(defaultans'))*40];
    Params = inputdlg(prompt, 'Set channel type', num_lines, defaultans);
    pars.dataType.varSelect=eval(Params{1});
else
    pars.dataType.varSelect=params{3};
end
if length(pars.dataType.varSelect) == 1
    pars.dataType.varSelect=pars.dataType.varSelect{1};
else
    crash
end
%% varSelect
pars.dataType.varSelect=pars.dataType.varSelect{1};
