function [trainSet,testSet]=splitDataEpochs(pars,cv_type,kInd,kInd_pLasso,cvLevel)
%this code is just to remind you that lambdaCV and modelOrderCV use same
%#folds and use same var as int for fold# being trained on
if pars.cv.p.run && pars.cv.lambda.run
    if pars.cv.lambda.K ~= pars.cv.p.K
        crash % otherwise train/test indices will be off
    end
end
% kInd controls which fold is being trained on. Assigns CVlambda and
% CVmodelOrder train/test inds
% CV_stage=1 if running CV on model-order or want training set associated
% w/ CV_p's fold of data
% CV_stage=2 if running CV on model-order w/ lasso or want training set associated
% w/ CV_p_lasso (2nd-stage fold)
data_saveFile=getEpochFilename(pars); 
load([pwd filesep data_saveFile])

%% prep train/test sets
if cvLevel == 0 % for final param estimates
    trainSet=dataEpochs;
    testSet=[];
    percData=1;
elseif cvLevel == 1 % regular CV
    trainSet=dataEpochs(indices_cv_p~=kInd);
    testSet=dataEpochs(indices_cv_p==kInd);
    if strcmp(cv_type,'lambda') && pars.cv.lambda.run
        percData=pars.cv.lambda.percData;
    elseif strcmp(cv_type,'p') && pars.cv.p.run 
        percData=pars.cv.p.percData;
    else
        crash
    end
elseif cvLevel == 2 % nested CV
    outerFoldTrainSet=dataEpochs(indices_cv_p~=kInd);
    if length(outerFoldTrainSet) == length(indices_cv_p_lasso)
        % standard
        trainSet=outerFoldTrainSet(indices_cv_p_lasso~=kInd_pLasso);
        testSet=outerFoldTrainSet(indices_cv_p_lasso==kInd_pLasso);
    elseif (length(outerFoldTrainSet)+1) == length(indices_cv_p_lasso)
        % this (i.e. indices_cv_p_lasso(1:end-1)) is needed sometimes for last fold of data due to rounding errors
        trainSet=outerFoldTrainSet(indices_cv_p_lasso(1:end-1)~=kInd_pLasso);
        testSet=outerFoldTrainSet(indices_cv_p_lasso(1:end-1)==kInd_pLasso);
    else 
        disp(['length(outerFoldTrainSet): ' num2str(length(outerFoldTrainSet))])
        disp(['length(indices_cv_p_lasso): ' num2str(length(indices_cv_p_lasso))])
        crash;
    end
    % nested CV shortcuts (i.e. reduce size of training set w/in each
    % subfold
    if pars.cv.p.run && pars.cv.p.dimRed.ch.lasso.bool 
        percData=pars.cv.p.dimRed.ch.lasso.percData;
    elseif pars.cv.p.run 
        percData=pars.cv.p.percData;
    else
        crash
    end
else
    crash
end
%% For testing pipeline, can opt to reduce size of training sets (worse performance, faster fitting)
if percData < 1
    crash % don't want this anymore
    newTrainSet={};
    ind=1;
    for dataInd=1:round(1/percData):length(trainSet)
        newTrainSet{ind}=trainSet{dataInd};
        ind=ind+1;
    end
    trainSet=newTrainSet;
    clear newTrainSet
end
