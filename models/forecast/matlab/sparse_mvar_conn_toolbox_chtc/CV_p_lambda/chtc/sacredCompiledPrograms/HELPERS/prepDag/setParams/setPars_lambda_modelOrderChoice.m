function [pars]=setPars_lambda_modelOrderChoice(pars,varargin)

%% Specify whether using CV_p or CV_lambda (OR-RS, OR-EV, CRU-RS-ZZZ, CRU-EV-ZZZ, CRU-RS, CRU-EV)
if strcmp(pars.runType,'LOCAL')
    answer=questdlg('Set model-order manually or use previously run CV_p procedure??', ...
        'Guess or use CV data', ...
        'Manual','CV','Cancel','Cancel');
    switch answer
        case 'Manual'
            pars.cv.lambda.modelOrderChoice=answer;
            prompt = {'Model-Order'};
            defaultans = {'5'};
            num_lines = [ones(size(defaultans')) ones(size(defaultans'))*50];
            Params = inputdlg(prompt, 'Set model-order manually.', num_lines, defaultans);
            pars.cv.lambda.manualModelOrder=str2double(Params{1});
        case 'CV'
            answer=questdlg('Which type of model do you want to use to infer best model-order?', ...
            'Optimal Model-order context', ...
            'CV_P_LASSO','CV_P','Cancel','Cancel');
            % Handle response
            switch answer
                case 'CV_P_LASSO'
                    %% CV_P_LASSO: Best model order is selected from performing nested CV that
                    % runs lasso models at each model-order (preferred method but takes
                    % forever)
                    keyboard
                    pars.cv.lambda.modelOrderChoice=answer;
                case 'CV_P'
                    %% CV_P: Best model order is selected from performing nested CV w.r.t.
                    % standard models tested at each model order
                    pars.cv.lambda.modelOrderChoice=answer;
                case 'Cancel'
                    crash
                    return
            end
        case 'Cancel'
            crash
            return
    end
else
    pars.cv.lambda.modelOrderChoice=varargin{1};
end