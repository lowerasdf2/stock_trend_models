function pars = setStaticVars(pars)
%% Note: some of these vars might be outdated
pars.cv.plotParams_bool=false; % plot model params during CV
pars.cv.plotOneStepError=false;
% Set loop options for running forward chain method or not when doing CV
pars.cv.fowardChain_bools=false;%,true];% use forward-chaining during CV (see https://stats.stackexchange.com/questions/14099/using-k-fold-cross-validation-for-time-series-model-selection)
pars.cv.fowardChain_bool=pars.cv.fowardChain_bools(1);% use forward-chaining during CV (see https://stats.stackexchange.com/questions/14099/using-k-fold-cross-validation-for-time-series-model-selection)
pars.cv.fowardChain_movingTarget=false;

%%
% Use wavelet basis matrix for dim. reduction or no
pars.cv.genDimRed.time.wavebases=false;
pars.cv.genDimRed.time.compare_wc_norm=false;
% Run markov model code
pars.run.bools.markovModel=false;%keyboard
% Use PCA for dim. reduction
pars.cv.genDimRed.ch.PCA.norm_cond=true; % prior to concatenating data across conditions, normalize w/in condition s.t. variance is measured relative to total power of the signal w/in each condition
if ~pars.cv.genDimRed.ch.PCA.norm_cond
    keyboard % add this param to saveFile if trying anything but default (true)
end
pars.cv.genDimRed.ch.PCA.threshType='PERC';
pars.cv.genDimRed.ch.PCA.plot_hi_vs_lo=false;%keyboard
pars.run.bools.pcaOnly=false; %keyboard % only run PCA (skip everything else) to figure out how many PCs to keep across ROIs; how similar low-pass PC space is to high-pass PC space
pars.electrode_file=[pars.dirs.electrodeSpreadsheet filesep 'LGD electrodes for analysis - sorted by ROI.xlsx'];
% Downsample data?
pars.dataType.downsampleBool=true;

%% set corrolary params (params that are set based on values of others)
if strcmp(pars.dataType.globalDataCat, 'OR-RS') || strcmp(pars.dataType.globalDataCat, 'minute') || strcmp(pars.dataType.globalDataCat, 'daily')
    pars.dataType.rsData=1; % 
elseif strcmp(pars.dataType.globalDataCat, 'OR-EV') || strcmp(pars.dataType.globalDataCat, 'EXO')
    pars.dataType.rsData=0;
else
    error('Need to add new check to determine if modeling exogenous variables based on specific dataset being analyzed')
end
% set duration of stim effect
% % if pars.dataType.rsData
pars.modelParams.l=0;
% % else
% %     pars.modelParams.l=10;
% % end
% % % set stim on/off
% % if pars.modelParams.l == 0
% %     pars.modelParams.modelStimEffects=false;
% % else
% %     pars.modelParams.modelStimEffects=true;
% % end