function epochFile=getEpochFilename(pars)

epochFile=['dataEpochs_trainTestInds'];
epochFile=[epochFile '_epLen' num2str(pars.dataType.epochLen)];
if pars.dataType.shortenSeg.bool
    epochFile=[epochFile '_mins' num2str(pars.dataType.shortenSeg.startMin) '-' num2str(pars.dataType.shortenSeg.startMin+pars.dataType.shortenSeg.segLen)];
end
if pars.cv.p.run
    epochFile=[epochFile '_' num2str(pars.cv.p.K) 'folds'];
    if pars.cv.p.dimRed.ch.lasso.bool
        epochFile=[epochFile '_' num2str(pars.cv.p.dimRed.ch.lasso.K) 'subfolds' ];
    end
end
epochFile=[epochFile '_' num2str(pars.dataType.sampleRate) 'hz'];
if pars.cv.genDimRed.ch.PCA.run == 1
    pcaStr=['_pca' num2str(pars.cv.genDimRed.ch.PCA.perc*100)];
else
    pcaStr='';
end
epochFile=[epochFile pcaStr '.mat'];


