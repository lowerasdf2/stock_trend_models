function [p_mo_range,p_percData,load_K_p]=setLoopedCVpars(p_K,varSelect)
if pars.dataType.USE_MIN_PARAMS
%                             p_mo_range=[5:5:25];
    p_mo_range=nan;% setting this elsewhere [2:2:20,40,60,80,100];%[5];
else
    p_mo_range=nan; % setting this elsehwere now [1,2,50,100:100:500];%[10,20,40,80,100:50:300,345];%[2:2:20,30:10:100,120:20:200,240:40:500]; % customize as you like (I typically use [2:2:20] as default for real data)
end
p_percData=1;
%% setPars_lambda_setLoadParams -> not needed for chtc pipeline (kept to match local version of pipeline)
load_K_p=p_K;
load_varSelect=varSelect;
load_dataTypeStr=dataTypeStr;
load_pcaBool=pcaBool;%nan;
load_pcaPerc=pcaPerc;%nan;
load_percData=p_percData; % leave at 1 ALWAYS; shorten segment if you want to speed up training for shittier model results
%% setPars_lambda_setNewParams (not the same as setPars_modelOrder_lasso!)
cond_model_order=1; % 0=avg over conditions (not an option for condor pipeline yet)
lambda_K=p_K;%has to copy p_K to avoid having to save out an additional param in train/test indices file;
lambda_lambdaWeightRange=[.1,1];%[.1,1];
lambda_inc=.05;%.05;  % was .05
if pars.dataType.USE_MIN_PARAMS
    lambda_its=50;% was 5000
else
    lambda_its=5000;% was 5000
end
lambda_percData=1;
lambda_lasso_plotParamsByLambda=0;
lambda_SC=1;
%% setPars_modelOrder_lasso
if pars.cv.p.dimRed.ch.lasso.bool
    if pars.dataType.USE_MIN_PARAMS
        p_lasso_lambdaWeightRange=[.5,1];
        p_lasso_inc=.5; 
    else
        p_lasso_lambdaWeightRange=[.1,1];
        p_lasso_inc=.1; % was .1
    end
    if pars.dataType.USE_MIN_PARAMS
        p_lasso_its=50;
    else
        p_lasso_its=5000; % default 5000
    end
    p_lasso_percData=1;
    p_lasso_plotParamsByLambda=0;%0;
    p_lasso_K=p_K;%this one doesn't actually need to copy p_K, but seems like a good idea anyways
    p_lasso_SC_bool=1;%1;

    %% check some input
    if p_lasso_SC_bool ~= 1 || lambda_SC ~= 1
        error('We decided to use only SC models previously')
    end
else
    % set vars to turn lasso off
    p_lasso_lambdaWeightRange=1;
    p_lasso_inc=0;
    p_lasso_its=0;
    p_lasso_percData=1;
    p_lasso_plotParamsByLambda=false;
    p_lasso_SC_bool=true;
    p_lasso_K=0; % setting to one allows us to use original DAG (which was designed to CV over to hyperparams) to run CV for just model-order (igoring sparsity)
end