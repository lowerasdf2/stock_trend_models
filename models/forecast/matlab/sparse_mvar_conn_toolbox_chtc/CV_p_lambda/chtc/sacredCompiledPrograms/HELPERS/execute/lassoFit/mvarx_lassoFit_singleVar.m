function [chParamsVariedByLambda] = mvarx_lassoFit_singleVar(pars,train_data,iVar,u,p,l,...
    lasso_SC_bool,lasso_inc,...
    lasso_its,lasso_weightRange)

%% BEGIN LASSO :)
if l~=0
    crash % need to account for this yet
end
% create H/Y
for s=1:length(train_data)
    data=train_data{s};
%     data=data(1:nVar,:);%keyboard%0);% leave this for all parfor/var tests
    [Y Z]=buildMats(data,p); % builds design matrix (H) along with outcome variable matrix (Y); design matrix is size (T-p) * (nVar*p)
    if s == 1
        fullZ=Z;
        fullY=Y;
    else
        fullZ=vertcat(fullZ,Z);
        fullY=vertcat(fullY,Y);
    end
end

if ~isnan(lasso_SC_bool) && ~lasso_SC_bool
    disp('nonSC models tend to have weird maxLambdas... all params go to zero w/ lambda weight set to like 70%')
    crash
end
disp(['Running lasso model for p=' num2str(p) ', ch=' num2str(iVar)])
stepWeight=1; % should always be one. Prev. used to help me understand lasso code better
%%
if pars.cv.p.dimRed.ch.lasso.bool
    [chParamsVariedByLambda]=lmCVFit_orig_singleVar(iVar,stepWeight,...
        lasso_inc, lasso_its, lasso_weightRange,fullY,fullZ,p,[],lasso_SC_bool);         
else
    crash 
    lasso_weightRange=0;
    lasso_inc=0;
%     keyboard
%     % solve fully connected model for single channel
%     modelOrderForEachCh=p*ones(size(data,1),1); % varName used to be KH
% %     chBoolVec=ones(size(data,1),1); % varName used to be KG
%     connBools=ones(size(data,1),1);
%     freeParameterMap=ones(size(fullZ,2),size(data,1));
%     nVar=size(data,1);
%     G=ones(1,nVar);
%     Xhat=LargeLS2_orig(fullY(:,iVar),fullZ,G,modelOrderForEachCh,connBools,freeParameterMap)
end
 

