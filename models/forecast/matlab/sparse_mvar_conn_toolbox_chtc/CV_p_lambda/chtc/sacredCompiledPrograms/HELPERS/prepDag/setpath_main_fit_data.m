function setpath_main_fit_data(config)

%% remember to add the '..' for chtc version
addpath(genpath(config.dirs.base3rdPartyCode))
% model code, CV code, quantify fits code
addpath(genpath([config.dirs.baseCodeDir]))
addpath(genpath([config.dirs.baseCodeDir filesep '..' filesep 'Utilities'])) % project-specific utilities
addpath(genpath([config.dirs.baseCodeDir filesep '..' filesep 'fit_ECoG_data']))
addpath(genpath([config.dirs.baseCodeDir filesep '..' filesep '3rdParty\eeg_meg_analysis-master'])) % this has markov model code, I believe
addpath(genpath([config.dirs.baseCodeDir filesep '..' filesep '3rdParty\moutlier1'])) % contains code for detecting outliers based on mhal. distance
% stuff for loading in data
addpath([config.dirs.baseCodeDir filesep 'misc' filesep 'MIBcode\lgd_func_conn_analysis\'])
addpath([config.dirs.baseCodeDir filesep 'misc' filesep 'MIBcode\lgd_func_conn_analysis\data_selection'])
%     addpath([pars.config.dirs.baseCodeDir filesep '..' filesep 'MIBcode\lgd_func_conn_analysis\constants']) % files I need from here will be moved to utilities
addpath([config.dirs.baseCodeDir filesep 'misc' filesep 'MIBcode\lgd_func_conn_analysis\dependencies\'])
addpath([config.dirs.baseCodeDir filesep 'misc' filesep 'MIBcode\lgd_func_conn_analysis\dbt_computation'])
