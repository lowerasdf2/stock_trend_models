function vecM=calcLmax2(Y,H,p,varargin)
%
% vecM=calcLmax2(Y,H,p,[simpleL1],[options]);

[T1 m]=size(Y);
[T2 mp]=size(H); % m should = mp/p

vecM=zeros(m,1);

options.penalty=2;
options.useNbpen=0;
options.verbose=0;

A=sparse(m*p,m);

% Could make this optional
useFreeParams=1;
R=Y;
if useFreeParams
    %freeParameterMap=speye(m); % m is number of channels
    % solve for free variables
    for c=1:m
        ind=(c-1)*p+1:c*p;
        A(ind,c)=(H(:,ind)'*H(:,ind))\(H(:,ind)'*Y(:,c));
    end
    % find lambda_max same way, but use difference instead.
    R=R-H*A;
end
KH=p*ones(m,1);

% Added to calculate simple L_1 max. lambda with free params.
if nargin>3
    if varargin{1}==1
        KH=ones(m*p,1);
    end
end

for c=1:m
    vecM(c)=stsmaxlambda(H,1,R(:,c),KH,1,options);
end
