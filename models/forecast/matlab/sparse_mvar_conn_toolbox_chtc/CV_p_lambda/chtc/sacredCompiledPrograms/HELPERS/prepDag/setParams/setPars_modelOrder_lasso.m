function [pars]=setPars_modelOrder_lasso(pars,p_K)%,p_lasso_lambdaWeightRange,p_lasso_inc,p_lasso_its,p_lasso_percData,p_lasso_plotParamsByLambda,p_lasso_K,p_lasso_SC_bool);
if pars.cv.p.dimRed.ch.lasso.bool
    if pars.dataType.USE_MIN_PARAMS
        p_lasso_lambdaWeightRange=[.5,1];
        p_lasso_inc=.5; 
    else
        p_lasso_lambdaWeightRange=[.1,1];
        p_lasso_inc=.1; % was .1
    end
    if pars.dataType.USE_MIN_PARAMS
        p_lasso_its=50;
    else
        p_lasso_its=5000; % default 5000
    end
    p_lasso_percData=1;
    p_lasso_plotParamsByLambda=0;%0;
    p_lasso_K=p_K;%this one doesn't actually need to copy p_K, but seems like a good idea anyways
    p_lasso_SC_bool=1;%1;
    lambda_SC=1;
    %% check some input
    if p_lasso_SC_bool ~= 1 || lambda_SC ~= 1
        error('We decided to use only SC models previously')
    end
else
    % set vars to turn lasso off
    p_lasso_lambdaWeightRange=1;
    p_lasso_inc=0;
    p_lasso_its=0;
    p_lasso_percData=1;
    p_lasso_plotParamsByLambda=false;
    p_lasso_SC_bool=true;
    p_lasso_K=0; % setting to one allows us to use original DAG (which was designed to CV over to hyperparams) to run CV for just model-order (igoring sparsity)
end
% % % if strcmp(pars.runType,'LOCAL')
% % %     answer=questdlg('Use lasso model when testing each model-order?', ...
% % %         'CV_p & lasso', ...
% % %         'Yes','No','Cancel','Cancel');
% % %     % Handle response
% % %     switch answer
% % %         case 'Yes'
% % %             pars.cv.p.dimRed.ch.lasso.bool=true;
% % %         case 'No'
% % %             pars.cv.p.dimRed.ch.lasso.bool=false;
% % %         case 'Cancel'
% % %             crash
% % %             return
% % %     end
% % %     if pars.cv.p.dimRed.ch.lasso.bool
% % %         prompt = {'Lambda weight range','Inc','Max Iterations','%training set to keep (0,100]', 'Plot params varied by lambda?', 'numFolds (K)'};
% % %         defaultans = {'[0,1]','.2','10000','100','1','5'};
% % %         Params = inputdlg(prompt, 'Set lasso params', 1, defaultans);
% % %         pars.cv.p.dimRed.ch.lasso.lambdaWeightRange=eval(Params{1});
% % %         C2=num2cell(str2double(Params(2:end)));
% % %         [pars.cv.p.dimRed.ch.lasso.inc,pars.cv.p.dimRed.ch.lasso.its,...
% % %             pars.cv.p.dimRed.ch.lasso.percData,pars.cv.p.dimRed.ch.lasso.plotParamsByLambda,...
% % %             pars.cv.p.dimRed.ch.lasso.K] = C2{:};
% % %         pars.cv.p.dimRed.ch.lasso.percData=pars.cv.p.dimRed.ch.lasso.percData/100;
% % %         if pars.cv.p.dimRed.ch.lasso.percData < .1 || pars.cv.p.dimRed.ch.lasso.percData > 1
% % %             keyboard % probs an accident, looking for 0-100 response
% % %         end
% % %         pars.cv.p.dimRed.ch.lasso.SC_bool=true; % seems like the only realistic version of the model; ignore user input / don't ask
% % %     else
% % %         pars.cv.p.dimRed.ch.lasso.SC_bool=nan;
% % %         pars.cv.p.dimRed.ch.lasso.percData=nan;
% % %         pars.cv.p.dimRed.ch.lasso.K=nan;
% % %         pars.cv.p.dimRed.ch.lasso.inc=nan;
% % %         pars.cv.p.dimRed.ch.lasso.its=nan;
% % %         pars.cv.p.dimRed.ch.lasso.lambdaWeightRange=nan;
% % %         pars.cv.p.dimRed.ch.lasso.plotParamsByLambda=false;
% % %     end

% % % else
% % % if pars.cv.p.dimRed.ch.lasso.bool
pars.cv.p.dimRed.ch.lasso.lambdaWeightRange=p_lasso_lambdaWeightRange;
pars.cv.p.dimRed.ch.lasso.inc=p_lasso_inc;
pars.cv.p.dimRed.ch.lasso.its=p_lasso_its;
pars.cv.p.dimRed.ch.lasso.percData=p_lasso_percData;
pars.cv.p.dimRed.ch.lasso.plotParamsByLambda=p_lasso_plotParamsByLambda;
pars.cv.p.dimRed.ch.lasso.K=p_lasso_K;
pars.cv.p.dimRed.ch.lasso.SC_bool=p_lasso_SC_bool;
% % % else
% % %     pars.cv.p.dimRed.ch.lasso.lambdaWeightRange=nan;
% % %     pars.cv.p.dimRed.ch.lasso.inc=nan;
% % %     pars.cv.p.dimRed.ch.lasso.its=nan;
% % %     pars.cv.p.dimRed.ch.lasso.percData=nan;
% % %     pars.cv.p.dimRed.ch.lasso.plotParamsByLambda=nan;
% % %     pars.cv.p.dimRed.ch.lasso.K=0; % set to 0 so DAG that typically runs 2 rounds of CV (2 hyperparams) can still be used to run single round of CV for just model-order
% % %     pars.cv.p.dimRed.ch.lasso.SC_bool=nan;
% % % end
% % % end