function pars=setCreateDagPars(pars)
%     keyboard % keyboard here to remind you to double check these params
pars.run.createDag.useSubDags=true; % new way to create dags. should probs just always leave this set to true.
pars.run.createDag.NUM_RETRIES=5; % sets numRetries to assign to each jobType. Retries prevent one-off errors from killing dags
pars.run.createDag.lvl1CVonly=false; % set this to true to fix corrupted zip files outputted in lvl1 (added some checks to pre/post scripts so hopefully don't need this moving forward)
pars.run.createDag.debias_measMOerr_only=false; % set this true to create dag that ONLY constructs debiased model params of given model order and outputs model-order error (this was needed to fix some output at one point)
pars.run.createDag.specCVks.bool=false; % set true to run nested CV procedure only for certain k-folds (needed to fix a broken dag at one point)
pars.run.createDag.specCVks.kList=[1];
pars.run.createDag.ver=6;% which version of compiled programs (and submit files) to run
%
pars.run.createDag.singleVar.bool=false; % this functionality is not fully tested yet
pars.run.createDag.singleVar.var=10;
