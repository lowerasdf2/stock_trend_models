function pars=setModelOrderRange(pars)
if pars.dataType.USE_MIN_PARAMS
    pars.cv.p.modelOrderRange=5;
else
    dataSaveDir=getEpochSaveDir(pars);
    origDir=pwd;
    cd(dataSaveDir) % have to cd to file containing epochs s.t. splitDataEpochs can run from pwd (just like it has to on HTC servers)
    cv_type='p';
    kInd=1;
    if pars.cv.p.dimRed.ch.lasso.bool
        kInd_pLasso=1;
        cvLevel=2;
    else
        kInd_pLasso=nan;
        cvLevel=1;
    end
    [trainSet,testSet]=splitDataEpochs(pars,cv_type,kInd,kInd_pLasso,cvLevel);
    minModelParsToSamplesRatio=.5; % increase from .5 to allow for testing model-orders likely to overfit
    maxModelOrder=calcOverfitPoint(pars,minModelParsToSamplesRatio,trainSet);
    if maxModelOrder<=10
        pars.cv.p.modelOrderRange=[2:1:maxModelOrder];
    elseif maxModelOrder<=30
        pars.cv.p.modelOrderRange=[3:3:maxModelOrder];
    elseif maxModelOrder<=50
        pars.cv.p.modelOrderRange=[2,5:5:25,30:10:maxModelOrder];
    elseif maxModelOrder<=150
        pars.cv.p.modelOrderRange=[2,5,10:10:maxModelOrder];
    elseif maxModelOrder<=500
        pars.cv.p.modelOrderRange=[2,5];
        x=pars.cv.p.modelOrderRange(end)+pars.cv.p.modelOrderRange(end-1);
        while x<maxModelOrder
            pars.cv.p.modelOrderRange=[pars.cv.p.modelOrderRange,x];
            x=round((pars.cv.p.modelOrderRange(end)*.5)+pars.cv.p.modelOrderRange(end-1));
        end
    end

    cd(origDir)
end
%% check relevant params for conflicts
if pars.dataType.epochLen*pars.dataType.sampleRate <= max(pars.cv.p.modelOrderRange)
    disp('Max model-order cannot equal/exceend pars.dataType.epochLen*pars.dataType.sampleRate')
    keyboard
end
if isempty(pars.cv.p.modelOrderRange)
    keyboard
end
%% display MO range
disp(['Model-order range = ' num2str(pars.cv.p.modelOrderRange(1)) '-' num2str(pars.cv.p.modelOrderRange(end))])
