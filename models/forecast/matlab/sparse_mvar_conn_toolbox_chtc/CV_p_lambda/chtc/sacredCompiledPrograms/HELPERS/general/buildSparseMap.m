function sparseMap=buildSparseMap(pars,cvType,nVar,modelOrder,np,cvLevel,lvl1kInd)
%% fill in sparseMap (cols arranged in blocks; each block representing one model; blocks arranged from most sparse models to least sparse models)
if cvLevel == 0
    fileAppend='_allData';
    lwKstr='';
    kStr='';
elseif cvLevel == 1
    fileAppend='_lvl1fold';
    lwKstr='';
    kStr=['_K' num2str(lvl1kInd)];
elseif cvLevel ==2
    fileAppend='_lvl2fold';
    lwKstr=['_lwK' num2str(pars.cv.p.dimRed.ch.lasso.currK)];
    kStr=['_K' num2str(lvl1kInd)];
end
%% Set some vars in case need to move down a single dir so we can untar w/out windows issues
parentDir=pwd;
childDir=strfind(parentDir,filesep);
childDir=parentDir(childDir(end)+1:end);
mvDownOneDir=false;

try
    if strcmp(cvType,'p') && (cvLevel==2 || cvLevel==1)
        tarFilename=['k' num2str(lvl1kInd)];
        untarAndMoveFiles(tarFilename)
    elseif strcmp(cvType,'lambda') && cvLevel==1
        tarFilename='allFiles';
        if lvl1kInd==1
            untarAndMoveFiles(tarFilename)
        end
    elseif strcmp(cvType,'lambda') && cvLevel==0
        tarFilename='allFiles';
        if isnan(lvl1kInd)
            untarAndMoveFiles(tarFilename)
        else
            disp(['lvl1kInd=' num2str(lvl1kInd)])
            crash
        end
    else
        % Need to add section for prescript again
    %     crash % need to add code for this case
        crash
    end

    sparseMap=sparse(modelOrder*nVar,np*nVar);  
    for iVar=1:nVar
        cv_saveFile=[cvType 'CV_singleVarParams_var' num2str(iVar) fileAppend ...
            '_p' num2str(modelOrder) kStr lwKstr '.mat'];
        load([pwd filesep cv_saveFile])
        sparseMap(:,iVar:nVar:((np-1)*nVar+iVar))=singleVarParams{iVar};
    end

    %% delete untarred files to prevent extra daga from being outputted on chtc server
    if (strcmp(cvType,'p') && (cvLevel==2 || cvLevel==1)) 
        for iFile=1:length(files)
            if ~contains(files(iFile).name,'avgCvErrByLambda')
                delete([pwd filesep files(iFile).name])
            end
        end
        rmdir([pwd filesep tarFilename],'s') % delete tar folder
    elseif strcmp(cvType,'lambda') && cvLevel==1
        if lvl1kInd==pars.cv.lambda.K
            files=dir([pwd filesep cvType 'CV*.mat']);
            for iFile=1:length(files)
                delete([pwd filesep files(iFile).name])
            end
            rmdir([pwd filesep tarFilename],'s') % delete tar folder
        end
    elseif strcmp(cvType,'lambda') && cvLevel==0
        for iFile=1:length(files)
            if ~contains(files(iFile).name,'avgCvErrByLambda')
                delete([pwd filesep files(iFile).name])
            end
        end
        rmdir([pwd filesep tarFilename],'s') % delete tar folder
    else
        crash
    end
    if mvDownOneDir
        cd(parentDir)
    end
catch why
    cd(parentDir) % cd back to orig dir even if function fails
    crash
end

%% Helper functions
function untarAndMoveFiles(tarFilename)
    % try standard method
    untar([pwd filesep tarFilename '.tar.gz'])
    files=dir([pwd filesep tarFilename filesep '*.mat']);
    if isempty(files) % becuz windows is intimidated by long path names
        mvDownOneDir=true;
        cd([pwd filesep '..' filesep ])
        untar([parentDir filesep tarFilename '.tar.gz'],pwd)
        files=dir([pwd filesep tarFilename filesep '*.mat']);
        if isempty(files)
            error('untar failed.')
        else
            for iFile=1:length(files)
                movefile([tarFilename filesep files(iFile).name],[pwd filesep files(iFile).name])
            end 
        end
    else
        for iFile=1:length(files)
            movefile([tarFilename filesep files(iFile).name],[pwd filesep files(iFile).name])
        end 
    end

end

end

