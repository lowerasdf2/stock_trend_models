function [fileEnd,genParams,loadParams,runParams] = createFileEnd_fitECoG(pars,varargin)
if ~isempty(varargin)
    skipLoadAppend=true;
else
    skipLoadAppend=false;
end
oldName=true;
%% add params from most generally applied to most specific
loadParams=[];
runParams=[];
genParams=[];
fileEnd=['_' num2str(pars.dataType.sampleRate) 'hz_epochLen' num2str(pars.dataType.epochLen)];
fileEnd=[fileEnd '_' pars.dataType.globalDataCat];
fileEnd=[fileEnd '_' pars.dataType.varSelect];
if pars.cv.fowardChain_bool
    if pars.cv.fowardChain_movingTarget
        fileEnd=[fileEnd '_fwdChainMT'];
    else
        fileEnd=[fileEnd '_fwdChain'];
    end
end

if pars.cv.genDimRed.time.wavebases
    keyboard % make sure this param runs ONLY wavelets version (not both regular and wavelets like latest code did)
    fileEnd=[fileEnd '_waveMVARX'];
end
if pars.dataType.shortenSeg.bool
    fileEnd=[fileEnd '_min' num2str(pars.dataType.shortenSeg.startMin) '-' num2str(pars.dataType.shortenSeg.startMin+pars.dataType.shortenSeg.segLen)];
end
if pars.cv.genDimRed.ch.PCA.run
    fileEnd=[fileEnd '_PCAAC'];
    if strcmp(pars.cv.genDimRed.ch.PCA.threshType,'PERC')
        fileEnd=[fileEnd num2str(pars.cv.genDimRed.ch.PCA.perc*100)];
    else
        keyboard
    end
end
genParams=fileEnd;
if pars.cv.p.run
% % %     fileEnd=[fileEnd '_CV' num2str(100*pars.cv.p.percData)];
    try
        fileEnd=[fileEnd '_mr' num2str(pars.cv.p.modelOrderRange(1)) '-' num2str(pars.cv.p.modelOrderRange(end))];
    catch why
        keyboard
    end
    fileEnd=[fileEnd '_Kp' num2str(pars.cv.p.K)];
    if pars.cv.p.dimRed.ch.lasso.bool
        fileEnd=[fileEnd '_lwK' num2str(pars.cv.p.dimRed.ch.lasso.K)];
        if pars.cv.p.dimRed.ch.lasso.SC_bool
%             fileEnd=[fileEnd '_lasSC' num2str(pars.cv.p.dimRed.ch.lasso.percData*100)];
            fileEnd=[fileEnd '_lasSC'];
        else
%             fileEnd=[fileEnd '_las' num2str(pars.cv.p.dimRed.ch.lasso.percData*100)];
            fileEnd=[fileEnd '_las'];
        end
        fileEnd=[fileEnd '_w' num2str(pars.cv.p.dimRed.ch.lasso.lambdaWeightRange(1)*100) '-' num2str(pars.cv.p.dimRed.ch.lasso.lambdaWeightRange(end)*100)];
        fileEnd=[fileEnd '_inc' num2str(pars.cv.p.dimRed.ch.lasso.inc*100)];
%         if oldName
        fileEnd=[fileEnd '_its' num2str(pars.cv.p.dimRed.ch.lasso.its)];
%         end
    end
end
%% lambda
if pars.cv.lambda.run
    %% start with loadfile params
    if ~skipLoadAppend
        if strcmp(pars.cv.lambda.modelOrderChoice,'CV_P') || strcmp(pars.cv.lambda.modelOrderChoice,'CV_P_LASSO')
%             fileEnd=[fileEnd '_loadCVp' num2str(100*pars.cv.lambda.loadCV.p.percData)];
%             fileEnd=[fileEnd '_loadCVp'];
%             fileEnd=[fileEnd '_mr' num2str(pars.cv.lambda.loadCV.p.modelOrderRange(1)) '-' num2str(pars.cv.lambda.loadCV.p.modelOrderRange(end))];
%             fileEnd=[fileEnd '_Kp' num2str(pars.cv.lambda.loadCV.p.K)];
            if ~strcmp(pars.cv.lambda.modelOrderChoice,'CV_P')
                fileEnd=[fileEnd '_lwK' num2str(pars.cv.p.dimRed.ch.lasso.K)];
                if pars.cv.p.dimRed.ch.lasso.SC_bool
                    fileEnd=[fileEnd '_lasSC' num2str(pars.cv.p.dimRed.ch.lasso.percData*100)];
                else
                    fileEnd=[fileEnd '_las' num2str(pars.cv.p.dimRed.ch.lasso.percData*100)];
                end
                fileEnd=[fileEnd '_w' num2str(pars.cv.p.dimRed.ch.lasso.lambdaWeightRange(1)*100) '-' num2str(pars.cv.p.dimRed.ch.lasso.lambdaWeightRange(end)*100)];
                fileEnd=[fileEnd '_inc' num2str(pars.cv.p.dimRed.ch.lasso.inc*100)];
%                 if oldName
                fileEnd=[fileEnd '_its' num2str(pars.cv.p.dimRed.ch.lasso.its)];
%                 end
            end
        elseif strcmp(pars.cv.lambda.modelOrderChoice,'MANUAL')
            fileEnd=[fileEnd '_ManualMO' num2str(pars.cv.lambda.manualModelOrder)];
        else
            crash
        end
    end
    %% then with runCV_lambda params
%     fileEnd=[fileEnd '_runCVl' num2str(100*pars.cv.lambda.percData)];
%     if oldName
    fileEnd=[fileEnd '_CVl'];
%     else
%         fileEnd=[fileEnd '_CVl'];
%     end
    if ~pars.cv.lambda.condModelOrder
        fileEnd=[fileEnd '_staticModelOrder'];
    end
    if ~pars.cv.lambda.SC_bool
        crash % assumed default
    else
        if oldName
            fileEnd=[fileEnd 'SC'];
        end
    end
    fileEnd=[fileEnd '_w' num2str(pars.cv.lambda.lambdaWeightRange(1)*100) '-' num2str(pars.cv.lambda.lambdaWeightRange(end)*100)];
    fileEnd=[fileEnd '_inc' num2str(pars.cv.lambda.inc*100)];
%     if oldName
    fileEnd=[fileEnd '_its' num2str(pars.cv.lambda.its)];
%     end
    fileEnd=[fileEnd '_lwK' num2str(pars.cv.lambda.K)];

    if strcmp(pars.cv.lambda.modelOrderChoice,'CV_P') || strcmp(pars.cv.lambda.modelOrderChoice,'CV_P_LASSO')
        loadParsBegInd=strfind(fileEnd,'loadCVp');
        loadParsEndInd=strfind(fileEnd,'CVl');
        loadParams=fileEnd(loadParsBegInd:loadParsEndInd-2);
    else
        loadParsBegInd=strfind(fileEnd,'ManualMO');
        loadParsEndInd=strfind(fileEnd,'CVl');
        loadParams=fileEnd(loadParsBegInd:loadParsEndInd-2);
    end
    runParams=fileEnd(loadParsEndInd:end);
end






