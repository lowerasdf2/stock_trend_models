function pars=setDirs(pars)

pars.dirs.baseCodeDir=pars.dirs.localPath;

baseDataDir='A:\'; %'\\Odin\o\data\ecog_connectivity\'
pars.dirs.baseResultsDir='R:\';%'\\MEMORYBANKS\Data\PassiveEphys\Analysis Results\Chris\ecog_connectivity\';
pars.dirs.baseCondDataDir='G:\data'; % identical to batchParams dir at the moment

pars.dirs.baseCondDataDir='C:\Users\qualiaMachine\Google Drive\AI_projects\stock_trend_models\data\MATLAB\';
pars.dirs.baseResultsDir=pars.dirs.baseCondDataDir;
baseDataDir=pars.dirs.baseCondDataDir;

pars.dirs.base3rdPartyCode=[pars.dirs.baseCodeDir filesep '3rdPartyExtensions' filesep 'matlab' filesep];
pars.dirs.electrodeSpreadsheet=[baseDataDir filesep 'electrode_file_sync' filesep 'Electrode MNI coordinates and ROI' filesep];
pars.dirs.dimRedData=[baseDataDir filesep 'dimRedCondData']; %A:\dimRedCondData
% tried to use regular results folder, but filepath is too long to use
% copy/movefile
pars.dirs.baseCorruptDagDir=[baseDataDir filesep 'corruptedDags\'];
pars.dirs.baseChtcDagDir=[baseDataDir filesep 'dags\'];
pars.dirs.postHocAnalyze=[pars.dirs.baseCodeDir filesep 'analyze_final_models'];

