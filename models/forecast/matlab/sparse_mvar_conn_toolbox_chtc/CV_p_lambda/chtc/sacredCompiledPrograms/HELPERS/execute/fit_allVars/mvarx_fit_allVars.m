function [A_hat, B_hat, Q_hat, Epsilons, b_hat, Z,R_ZZ,R_XZ] = mvarx_fit_allVars(train_data, u, p, l, stimOn_bool,...
    wavelet_bool,plotParams_bool)
if wavelet_bool
    crash % need to finish/verify that code
end
cvLambdaData=[];
b_hat=[];
Z=[];
R_ZZ=[];
R_XZ=[];
totalMOvars=p;

%% MVARX_FIT Fit MVARX model to data
% [A, B, Q, W, n_spl] = mvarx_fit(X, u, p, l)
%
% Find an MVARX model that fits the data with the following relation
% X(:, n) = A * [X(:, n - 1); X(:, n - 2); X(:, n - 3); ... ; X(:, n - p)]
%          + B * [u(n); u(n - 1); ... ; u(n - l + 1)] + W(:, n - n_o)
%          for n = n_o + 1, ..., N where n_o = max(p, l - 1)
%
% X - Data, can be either a matrix or a cell
%       -if X is an M-by-N matrix, X would be the measurements from M 
%        channels/electrodes in a window of N samples
%       -if X is a 1-by-J cell, each cell is measurements from M channels in
%        an epoch/trial, X{j} is an M-by-N_j matrix for j = 1, 2, ..., J
%        assuming there are J epochs
% u - stimulation
%       -if X is a matrix, u should be a 1-by-N vector, representing the application
%        of stimulation in time
%       -if X is a cell, u should also be a 1-by-J cell, and u{j} is an 1-by-N_j
%        vector for j = 1, 2, ..., J
% p - MVARX model autoregressive order
% l - MVARX model direct feedforward effect length
%
% A - MVARX autoregressive coefficient matrix (M-by-Mp)
% B - MVARX direct feedforward matrix (M-by-l)
% W - MVARX residual 
%       -if X is a matrix then W will be an M-by-(N - n_o) matrix 
%       -if X is a cell then W wil be a 1-by-J cell, with each cell W{j} being 
%        an M-by-(N_j - n_o) matrix
% Q - MVARX residual covariance matrix E[Q] = E[W(:, n)* W(:, n).'] for all n
% n_spl - number of samples in data
%       -if X is a matrix then n_spl is a scalar and n_spl = N - n_o
%       -if X is a cell then n_spl is a cell and n_spl{j} = N_j - n_o for j = 1, 2, ..., J
Q_hat=[];
Epsilons=[];
% % % if ~isempty(varargin)
% % %     if ~wavelet_bool
% % %         keyboard
% % %     end
% % %     N=varargin{1};
% % %     q=varargin{2};
% % % end

if ~stimOn_bool
    B_hat=[];
end
if ~wavelet_bool
    b_hat=[];
end

n_o =max(totalMOvars, l - 1);
%% X is a cell
train_data_subset = train_data;% now reducing datset before entering this function(1:round(1/standModel_percData):end);
n_epoch = size(train_data_subset, 2);    % number of trials/epochs
M = size(train_data_subset{1}, 1);       % channels
Data_cltd = cell(1, n_epoch);
Z_cltd = cell(1, n_epoch);
n_spl = cell(1, n_epoch);

for j = 1:n_epoch
% % %         if pars.wavebases
% % %             newX = Data{j};
% % %             ord=M * p + l;
% % %             W=wavebases3(ord);
% % %             for iVar=1:size(newX,1)
% % %                 ord = size(W,1);
% % %                 y(:,1) = newX(iVar,ord+1:end);
% % %                 X = toeplitz(newX(iVar,ord:end-1),newX(iVar,ord:-1:1));
% % %                 b = inv((X*W)'*X*W)*(X*W)'*y;
% % %                 a = W*b;
% % %             end
% % %         end
    n_spl{j} = size(train_data_subset{j}, 2) - n_o;
    epoch_data = train_data_subset{j}(:, n_o + 1:end);
    X_train_full=train_data_subset{j};
% % %         if pars.wavebases
% % %             Z = zeros(p + l, n_spl{j});
% % %         else
    Z_full = zeros(M * totalMOvars + l, n_spl{j});
    varCt=1;
    for i = [1:p]
        shiftedSignal=train_data_subset{j}(:, n_o + 1 - i:end - i);
        % In the multivariate case, rows aare filled in for each channel 
        % for each time-shift into the past (all channels fill rows before moving to next time-step)
        Z_full((varCt - 1) * M + 1:varCt * M, :) = shiftedSignal;
        varCt=varCt+1;
    end
    if stimOn_bool
        D = toeplitz([u{j}(1); zeros(l - 1, 1)], u{j});
        Z_full(M * totalMOvars + 1:end, :) = D(:, n_o + 1:end);
    end
    if wavelet_bool
        Z = zeros((M * ((N+1)*q))+l, n_spl{j});
        if stimOn_bool
            Z(end-l+1:end, :) = D(:, n_o + 1:end);
        end
        W = wavebases4(N,q,totalMOvars);
        for iVar=1:size(X_train_full,1)
            y = X_train_full(iVar,n_o+1:end);
            z = toeplitz(X_train_full(iVar,n_o:end-1),X_train_full(iVar,n_o:-1:1));
            z_trans = z*W;
            fillRows=iVar:M:size(Z,1)-l;
            Z(fillRows,:) = z_trans';
%             Z_fliplr = fliplr(Z);
            % b = inv((X*W)'*X*W)*(X*W)'*y;
%             b = (z*W)\y; 
        end
    else
        Z=Z_full;
% % %             Z = zeros(M * totalMOvars + l, n_spl{j});
% % %             for i = 1:totalMOvars
% % %                 shiftedSignal=Data{j}(:, n_o + 1 - i:end - i);
% % %                 % In the multivariate case, rows aare filled in for each channel 
% % %                 % for each time-shift into the past (all channels fill rows before moving to next time-step)
% % %                 Z((i - 1) * M + 1:i * M, :) = shiftedSignal;
% % %             end
% % %             if stimOn_bool
% % %                 D = toeplitz([u{j}(1); zeros(l - 1, 1)], u{j});
% % %                 Z(M * totalMOvars + 1:end, :) = D(:, n_o + 1:end);
% % %             end
    end
    if j == 1
        %% wc
% % %             W=wavebases4(pars);
% % %             W=flipud(W);
% % %             figure
% % %             for order=1:24
% % %                 plot(W(order,:))
% % %                 hold on
% % %                 pause
% % %             end
% % %             newW=zeros(totalMOvars*M,size(W,2));
% % %             globalRow=1;
% % % %             globalCol=1;
% % %             for iP=1:totalMOvars
% % %                 for iVar = 1:M
% % %                     newW(globalRow,:)=W(iP,:);
% % %                     globalRow=globalRow+1;
% % %                 end
% % %             end
% % %             if stimOn_bool
% % %                 % add stim effect
% % %                 stimMemW=W(1:l,:);
% % %                 newW=vertcat(newW,stimMemW);
% % %             end
% % %             W=newW;
% % %             R_XZ_wc = (X*Z')*W;
% % %             R_ZZ_wc = ((Z'*W)'*(Z'*W));

        R_XZ = (epoch_data*(Z'));
%             R_XZ1 = (X_train*(Z(1,:)'));

        R_ZZ = (Z*Z');
    else
        R_XZ = R_XZ + (epoch_data*Z');%(X*(Z'*W));
        R_ZZ = R_ZZ + (Z*Z');%((Z'*W)'*(Z'*W));
    end
%       inv((X*W)'*X*W)*(X*W)'*y;
%       R_XZ_new = R_XZ_new + (X*Z_new.');
%     	R_ZZ_new = R_ZZ_new + (Z_new*Z_new.');
    Data_cltd{j} = epoch_data;
%         if wavelet_bool
    Z_cltd{j} = Z_full;
%         else
%             Z_cltd{j} = Z;
%         end
end

if ~wavelet_bool
    theta=R_XZ / R_ZZ; % orig model
    %% below 2 lines was a test I ran to make sure I knew how to solve one variable at a time (2-ch system)
%         theta_singleVar=[R_XZ(1,:) / R_ZZ;...
%             R_XZ(2,:) / R_ZZ]

% % %         while ~istable(R_ZZ)
% % %             R_ZZ=R_ZZ*.9;
% % %         end
    if sum(sum(isnan(theta)))
        keyboard
    end
    A_hat = theta(:, 1:M * totalMOvars);  
    if stimOn_bool
        B_hat = theta(:, M * totalMOvars + 1:end);
    else
        B_hat = [];
    end
else
    crash % it's been too long since this was used... verify it's correct.
    theta = R_XZ / R_ZZ;
    if sum(sum(isnan(theta)))
        keyboard
    end
    if stimOn_bool
        b_hat=theta(end-l,:);
    else
        b_hat=[];
    end
    W=wavebases4(N,q,totalMOvars);
    newA=nan(size(X_train_full,1),size(X_train_full,1)*totalMOvars);
    for iVar=1:M
        for otherCh=1:M
            tempA=theta(iVar,otherCh:M:end)*W';
            newA(iVar,otherCh:M:end)=tempA;
        end
    end
    A_hat=newA;
    theta_wc_summed = theta;%(b*W); % wc
    if stimOn_bool
        B_hat = theta_wc_summed(:, M * totalMOvars + 1:end);
    else
        B_hat=[];
    end
end

% % %         W=wavebases4(pars);
% % %         newW=zeros(size(W,1)*M,size(W,2));
% % %         globalRow=1;
% % %         for iP=1:totalMOvars
% % %             for iVar = 1:M
% % %                 newW(globalRow,:)=W(iP,:);
% % %                 globalRow=globalRow+1;
% % %             end
% % %         end
% % %         if stimOn_bool
% % %             % add stim effect
% % %             stimMemW=W(1:l,:);
% % %             newW=vertcat(newW,stimMemW);
% % %         end
% % %         W=newW;
% % %         df=size(newW,2);
% % %         newestW=newW;
% % %         for iVar=2:M
% % %             newestW=horzcat(newestW,repmat(newW(:,1),1,df));
% % %         end


%% old method (didn't sum over epochs)
if wavelet_bool 
    crash
    %% prev. final solution
%             W=wavebases3(totalMOvars*M);
%             y = X_full(:,(totalMOvars*M)+1:end);
%             correctedZ = Z(:,size(Z,2)-size(y,2)+1:end)';
%             b = inv((correctedZ*W')'*correctedZ*W')*(correctedZ*W')'*y'; 
%             A_wc = (W'*b)';
    %% new final solution
%             y = X_full(:,totalMOvars+1:end);
%             correctedZ = Z(:,size(Z,2)-size(y,2)+1:end)';
% %             b = inv((correctedZ*W')'*correctedZ*W')*(correctedZ*W')'*y'; 
%             b = (correctedZ*W)\y';
%             % b = (Z*W)\y;  <====> b = inv((X*W)'*X*W)*(X*W)'*y;
%             theta_wc = (W*b)';
end

%         theta2=(R_XZ*W) / (R_ZZ*W); 
%         A = theta(:, 1:M * totalMOvars); 
%         B = theta(:, M * totalMOvars + 1:end);
%% Equivalent equations
% EQUIVALENT TO ==> theta = R_XZ*inv(R_ZZ);
% EQUIVALENT TO ==> theta = R_XZ*inv(R_ZZ);
% EQUIVALENT TO ==> theta = R_ZZ\R_XZ';
%% theta is also equal to below equation for 1 epoch, see:http://www.phdeconomics.sssup.it/documents/Lesson17.pdf 
%     theta2 = ((inv(Z*Z')*Z)*X')';
%     figure
%     subplot(2,1,1)
%     imagesc(theta)
%     subplot(2,1,2)
%     imagesc(theta2)

%% Plot params?
if plotParams_bool
    keyboard
    figure
    cmin = min([exp(A_hat(:)); exp(A_hat(:))]);
    cmax = max([exp(A_hat(:));exp(A_hat(:))]);
    subplot(311); imagesc((A_hat(:,1:end))); title('$A\_wavebases$', 'Interpreter', 'latex'); colorbar;
    subplot(312); imagesc((A_hat(:,1:size(A_hat,2)))); title('$A\_mvar$', 'Interpreter', 'latex'); colorbar;
    subplot(313); imagesc((A_hat(:,1:size(A_hat,2)))-(A_hat(:,1:end))); title('Difference', 'Interpreter', 'latex'); colorbar;
    pause(1)
    if pars.simData
        suptitle(['simData recoverd params: p=' num2str(p) ', q=' num2str(q) ', N=' num2str(N)] )
    else
        if pars.rsOnly
            suptitle([pars.ppID ' RS data: p=' num2str(p) ', q=' num2str(q) ', N=' num2str(N)] )
        else
            suptitle([pars.ppID ' Evoked data: p=' num2str(p) ', q=' num2str(q) ', N=' num2str(N) ', l=' num2str(l)])
        end
    end
    if ~pars.simData
        if pars.useCV
            if pars.rsOnly
                savefig(gcf,[pars.baseResultsDir '\CV_p\A_matrices\' pars.optimizeCond '_RS_' pars.ppID '_p' num2str(p) '_q' num2str(q) '_N' num2str(N) '_l' num2str(l) '_ch' num2str(pars.cvChList(1)) '-' num2str(pars.cvChList(end)) '.fig'])
                saveas(gcf,[pars.baseResultsDir '\CV_p\A_matrices\' pars.optimizeCond '_RS_' pars.ppID '_p' num2str(p) '_q' num2str(q) '_N' num2str(N) '_l' num2str(l) '_ch' num2str(pars.cvChList(1)) '-' num2str(pars.cvChList(end)) '.png'])
            else
                savefig(gcf,[pars.baseResultsDir '\CV_p\A_matrices\' pars.optimizeCond '_evoked_' pars.ppID '_p' num2str(p) '_q' num2str(q) '_N' num2str(N) '_l' num2str(l) '_ch' num2str(pars.cvChList(1)) '-' num2str(pars.cvChList(end))  '.fig'])
                saveas(gcf,[pars.baseResultsDir '\CV_p\A_matrices\' pars.optimizeCond '_evoked_' pars.ppID '_p' num2str(p) '_q' num2str(q) '_N' num2str(N) '_l' num2str(l) '_ch' num2str(pars.cvChList(1)) '-' num2str(pars.cvChList(end))  '.png'])
            end
            close(gcf)
        end
    else
        close(gcf);
    end
    if stimOn_bool
        figure
        cmin = min([B_hat(:); B_hat(:)]);
        cmax = max([B_hat(:); B_hat(:)]);
        subplot(211); imagesc(B_hat(:,1:end)/2); title('$B\_wavebases$', 'Interpreter', 'latex'); colorbar;
        subplot(212); imagesc(B_hat(:,1:end).^2); title('$B\_mvar$', 'Interpreter', 'latex'); colorbar;
    end
end

%% Error and covariance matrix --> can leave this commented out for CV procedure temporarily
Epsilons = cell(1, n_epoch);
for j = 1:n_epoch
    if stimOn_bool
%             keyboard % make sure we're all set here for wavelets and norm
        Epsilons{j} = Data_cltd{j} - theta * Z_cltd{j};
    else
        % wavelet case (testing no stim first)
        Epsilons{j} = Data_cltd{j} - A_hat * Z_cltd{j};
    end
end
Q_hat = (cell2mat(Epsilons)*cell2mat(Epsilons).') / sum(cell2mat(n_spl));   
n_spl=n_spl{1}; % we don't need to return whole vector 


