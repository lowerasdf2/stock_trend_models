function pars=setPostHocPars(pars)
% keyboard % check pars relevant to post-hoc analysis
%% conn measurement
pars.run.postHoc.conn.method='PDC'; %DC
pars.run.postHoc.conn.finalROIlist={'HGPM','HGAL','PT','PP','STG','AudRel','PFC','SensMot','Other'}; % list of ROIS to summarize connectivity measurement within
pars.run.postHoc.conn.plotChordConn=true; % use chord plot as opposed to my directed graph code
% if processNewData
%     pars.run.postHoc.conn.useAnatSubdivs=false; % some older models used anatomical subdivisions to define ROIs
% else
%     pars.run.postHoc.conn.useAnatSubdivs=true; % some older models used anatomical subdivisions to define ROIs
% end
% one cond
% if processNewData
%     pars.run.postHoc.conn.oneCond.bool=true;
% else
pars.run.postHoc.conn.oneCond.bool=false;
% end
pars.run.postHoc.conn.oneCond.name='OAAS1';
% all but one cond
pars.run.postHoc.conn.skipCond.bool=false;
pars.run.postHoc.conn.skipCond.name='OAAS1'; % which condition to skip, if any