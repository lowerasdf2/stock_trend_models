function dagDirName=getDagDirName(pars)
if ~pars.cv.genDimRed.ch.PCA.run
    keyboard % pretty sure code is slightly different when no PCA (see legacy code below)
    specDagDir=createFileEnd_fitECoG(pars,1);
    if pars.cv.genDimRed.ch.PCA.run
        specDagDir=specDagDir(strfind(specDagDir,['PCAAC']):end);
    else
        keyboard % double check below line works
        specDagDir=specDagDir(strfind(specDagDir,['CV' num2str(pars.cv.p.percData*100)]):end);
    end
end
dagDirName=createFileEnd_fitECoG(pars,1);
% dagDir=['epLen' num2str(pars.dataType.epochLen) '_sr' num2str(pars.dataType.sampleRate) '_'];
dagDirName=['epLen' num2str(pars.dataType.epochLen) '_sr' num2str(pars.dataType.sampleRate) '_'...
    dagDirName(strfind(dagDirName,pars.dataType.varSelect)+length(pars.dataType.varSelect)+1:end)];
if ~pars.cv.nested.bool && pars.cv.lambda.run
    dagDirName=['manualMO' num2str(pars.cv.nested.manualMO) '_' dagDirName];
end