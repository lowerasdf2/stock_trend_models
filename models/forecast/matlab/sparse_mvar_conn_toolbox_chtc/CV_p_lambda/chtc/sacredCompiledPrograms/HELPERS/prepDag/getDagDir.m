function dagDir=getDagDir(pars,cond)

%% create dagDir
dagDir=[pars.dirs.baseChtcDagDir filesep pars.dataType.globalDataCat filesep pars.dataType.varSelect filesep pars.dataType.catType filesep pars.dataType.dataID filesep cond];
dagDirName=getDagDirName(pars);
dagDir=[dagDir filesep dagDirName];