function maxModelOrder=calcOverfitPoint(pars,minModelParsToSamplesRatio,trainSet)

nTrainEpochs=length(trainSet);

nCh=size(trainSet{1},1);
if pars.cv.p.dimRed.ch.lasso.bool
    nModelParamsPerLag=round(min(pars.cv.p.dimRed.ch.lasso.lambdaWeightRange)*(nCh^2));
else
    nModelParamsPerLag=nCh^2;
end
maxModelOrder=round(minModelParsToSamplesRatio*...
    nTrainEpochs*sampPerEpoch/(nModelParamsPerLag + minModelParsToSamplesRatio*nTrainEpochs));



