function [pars]=setPars_lambda_setLoadParams(pars,load_K_p,load_pcaBool,load_pcaPerc,load_sr,load_percData)

%% Specify whether using CV_p or CV_lambda (OR-RS, OR-EV, CRU-RS-ZZZ, CRU-EV-ZZZ, CRU-RS, CRU-EV)
% % % % % % if strcmp(pars.runType,'LOCAL')
% % % % % %     %% transferred params (params to use to find proper saveFile with error x model-order data; can add dialogue boxes for this later
% % % % % %     if strcmp(pars.cv.lambda.modelOrderChoice,'CV_P')
% % % % % %         prompt = {'#Folds','Channel-type (ALL/RESP/AUDITORY)','Data-type (OR-RS/NA)','PCA (0/1)', 'PCA_perc','Sampling Rate (Hz)','Model-order Range','%training set to keep',};
% % % % % %         defaultans = {'5','ALLVARS','OR-RS','1','.70','250','[1,50]','100'};
% % % % % %         num_lines = [ones(size(defaultans')) ones(size(defaultans'))*120];
% % % % % %         Params = inputdlg(prompt, 'Set generic params (e.g. channel-type) previously used to determine optimal model-order', num_lines, defaultans);
% % % % % %         pars.cv.lambda.loadCV.p.K=str2double(Params(1));
% % % % % %         pars.cv.lambda.loadCV.varSelects=Params(2);
% % % % % %         pars.cv.lambda.loadCV.dataTypeStrs=Params(3);
% % % % % %         pars.cv.lambda.loadCV.PCA.run=str2double(Params(4));
% % % % % %         pars.cv.lambda.loadCV.PCA.perc=str2double(Params(5));
% % % % % %         pars.cv.lambda.loadCV.sr=str2double(Params(6));
% % % % % %         pars.cv.lambda.loadCV.p.modelOrderRange=eval(Params{7});
% % % % % %         pars.cv.lambda.loadCV.p.percData=(str2double(Params(8)))/100;
% % % % % %         
% % % % % %     elseif strcmp(pars.cv.lambda.modelOrderChoice,'CV_P_LASSO')
% % % % % %         keyboard  %% other params to add to request of input dialogue
% % % % % %         % set params used to run lasso models within CV_p procedure (check
% % % % % %         % save files)
% % % % % %         pars.cv.lambda.loadCV.lambda.lambdaWeightRange=[.2,1];
% % % % % %         pars.cv.lambda.loadCV.lambda.inc=.2;
% % % % % %         pars.cv.lambda.loadCV.lambda.its=10000;
% % % % % %         pars.cv.lambda.loadCV.lambda.percData=1;
% % % % % %         pars.cv.lambda.loadCV.lambda.K=3; keyboard % default as 3, just?
% % % % % %         loadCV.p.dimRed.ch.lasso.bool % use this format for loadVars regarding model-order_lambda
% % % % % %     elseif strcmp(pars.cv.lambda.modelOrderChoice,'Manual')
% % % % % %     else
% % % % % %         keyboard
% % % % % %     end
% % % % % %     if strcmp(pars.cv.lambda.modelOrderChoice,'CV_P') || strcmp(pars.cv.lambda.modelOrderChoice,'CV_P_LASSO')
% % % % % %         if pars.cv.lambda.loadCV.PCA.run ~= pars.cv.lambda.loadCV.PCA.run
% % % % % %             disp(['WARNING: PCA bool (' num2str(pars.cv.lambda.loadCV.PCA.run) ') does not match setting used to determine model order.'])
% % % % % %             crash
% % % % % %         end
% % % % % %         if pars.cv.lambda.loadCV.PCA.perc ~= pars.cv.genDimRed.ch.PCA.perc
% % % % % %             disp(['WARNING: PCA_perc (' num2str(pars.cv.genDimRed.ch.PCA.perc) ') does not match setting used to determine model order.'])
% % % % % %             crash
% % % % % %         end
% % % % % %     end
% % % % % % else
pars.cv.lambda.loadCV.p.K=load_K_p;
pars.cv.lambda.loadCV.PCA.run=load_pcaBool;
pars.cv.lambda.loadCV.PCA.perc=load_pcaPerc;
pars.cv.lambda.loadCV.sr=load_sr;
% pars.cv.lambda.loadCV.p.modelOrderRange=load_mo_range;
pars.cv.lambda.loadCV.p.percData=load_percData;
% % % % % % end

% % % %% transfer over generic params: dataTypeStrs, PCA stuff, sample-rate
% % % % if over-writing params with load params, run these below lines. 
% % % % Note: for some params, it might be beneficial to allow the lambda CV procedure to use
% % % % parameters other than what was used to uncover the best model order.
% % % if strcmp(pars.cv.lambda.modelOrderChoice,'CV_P') || strcmp(pars.cv.lambda.modelOrderChoice,'CV_P_LASSO')
% % %     pars.dataType.globalDataCat=pars.cv.lambda.loadCV.dataTypeStr;
% % %     pars.dataType.globalDataCat=pars.cv.lambda.loadCV.dataTypeStrs{1};
% % %     pars.cv.lambda.loadCV.PCA.run=pars.cv.lambda.loadCV.PCA.run;
% % %     pars.cv.genDimRed.ch.PCA.perc=pars.cv.lambda.loadCV.PCA.perc;
% % %     pars.dataType.sampleRate=pars.cv.lambda.loadCV.sr;
% % %     pars.dataType.varSelects=pars.cv.lambda.loadCV.varSelects;
% % %     if length(pars.dataType.varSelects) > 1
% % %         keyboard
% % %     end
% % %     pars.dataType.varSelect=pars.cv.lambda.loadCV.varSelects{1};
% % %     if length(pars.cv.lambda.loadCV.PCA.run) == 1 && pars.cv.lambda.loadCV.PCA.run == 0
% % %         pars.cv.genDimRed.ch.PCA.run=0;
% % %     elseif length(pars.cv.lambda.loadCV.PCA.run) == 1 && pars.cv.lambda.loadCV.PCA.run == 1
% % %         pars.cv.genDimRed.ch.PCA.run=1;
% % %     else
% % %         keyboard
% % %     end
% % % end
% % % if length(pars.cv.fowardChain_bools) == 1 && pars.cv.fowardChain_bools == 0
% % %     pars.cv.fowardChain_bool=0;
% % % elseif length(pars.cv.fowardChain_bools) == 1 && pars.cv.fowardChain_bools == 1
% % %     pars.cv.fowardChain_bool=1;
% % % else
% % %     keyboard
% % % end