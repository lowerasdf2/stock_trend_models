function construct_debiased_model_wBestLambdas_v6(varargin)%cvType,cvLevel,modelOrder,lvl1kInd)
% vars loaded: pars, trainTestIndices_dataFile, avgCvErrByLambda, summaryCV_modelOrder(for cvLevel0)
% varargin
cvType=varargin{1};
cvLevel=str2double(varargin{2});
modelOrder=str2double(varargin{3});
lvl1kInd=str2double(varargin{4});
if length(varargin) > 4
    varargin
    crash
end
% disp(['cvType=' cvType])
% disp(['modelOrder=' num2str(modelOrder)])
% disp(['lvl1kInd=' num2str(lvl1kInd)])
% disp(['cvLevel=' num2str(cvLevel)])

%% load pars from pwd
load([pwd filesep 'pars.mat'])
if cvLevel == 0
    %% these params allow us to replaced biased params (estimated using pars.cv.lambda.K folds) w/ params estimated when using ALL data
    % load file containing best model order 
    cv_saveFile=['summaryCVp.mat'];
    load([pwd filesep cv_saveFile],'summaryData','bestMO')
    modelOrder=bestMO;
    lvl1kInd=nan;
elseif ~cvLevel==1
    crash;
end

%% construct H and Y
[trainSet,testSet]=splitDataEpochs(pars,cvType,lvl1kInd,nan,cvLevel);
pars.dataType.nVar=size(trainSet{1},1);
for s=1:length(trainSet)
    data=trainSet{s};
%     data=data(1:pars.dataType.nVar,:);%keyboard%0);% leave this for all parfor/var tests
    [Y Z]=buildMats(data,modelOrder); % builds design matrix (H) along with outcome variable matrix (Y); design matrix is size (T-p) * (pars.dataType.nVar*p)
    if s == 1
        fullZ=Z;
        fullY=Y;
    else
        fullZ=vertcat(fullZ,Z);
        fullY=vertcat(fullY,Y);
    end
end

%% set some constants
nVarp=modelOrder*pars.dataType.nVar;
if strcmp(cvType,'p')
    lambdaWeights=pars.cv.p.dimRed.ch.lasso.lambdaWeightRange(end):-pars.cv.p.dimRed.ch.lasso.inc:pars.cv.p.dimRed.ch.lasso.lambdaWeightRange(1);
elseif strcmp(cvType,'lambda')
    lambdaWeights=pars.cv.lambda.lambdaWeightRange(end):-pars.cv.lambda.inc:pars.cv.lambda.lambdaWeightRange(1);
else
    crash
end
np=length(lambdaWeights);

%% load in sparseMap trained on all data (cols arranged in blocks; each block representing one model; blocks arranged from most sparse models to least sparse models)
sparseMap=buildSparseMap(pars,cvType,pars.dataType.nVar,modelOrder,np,cvLevel,lvl1kInd);

%% Load error file, retrieve best indices
if cvLevel == 1 % measuring error @kInd, so program's level is set to 1, but errorVariedByLambda is determined by level2fold errors
    baseSaveFileName=[cvType 'CV'];
    kStr=['_K' num2str(lvl1kInd)];
elseif cvLevel == 0
    baseSaveFileName=[cvType 'CV'];
    kStr='';
else
    crash
end
saveFile=[baseSaveFileName '_avgCvErrByLambda_p' num2str(modelOrder) kStr '.mat'];
load([pwd filesep saveFile],'errors')
delete([pwd filesep saveFile])
modelOrderForEachCh=modelOrder*ones(pars.dataType.nVar,1);

%% Solve for remaining connections using least squares or 'debiased' version of params (LargeLS2_orig).
A=sparse(nVarp,pars.dataType.nVar);
biasedA=A;
chList=1:pars.dataType.nVar;
for d=1:pars.dataType.nVar
    % (i(d)-1)*fCh+1:i(d)*fCh
    curA=sparseMap(:,chList(d)+(errors.i(d)-1)*pars.dataType.nVar);
    biasedA(:,d)=curA;
    A(:,d)=LargeLS2_orig(fullY(:,d),fullZ,1,modelOrderForEachCh,1,curA);
    numConnections=sum(sum(A~=0))/modelOrder;
end

Ahat_byCh=A;
Ahat_lagBlks=nan(size(Ahat_byCh'));
for nVar=1:pars.dataType.nVar
    for iP=1:modelOrder
        Ahat_lagBlks(nVar,[1:pars.dataType.nVar]+((iP-1)*pars.dataType.nVar))=Ahat_byCh(iP:modelOrder:end,nVar);
    end
end    
if cvLevel==0
    finalParams=figure;
    subplot(1,3,1)
    imagesc(biasedA)
    title('biased-byCh')
    subplot(1,3,2)
    imagesc(Ahat_byCh)
    title('debiased-byCh')
    subplot(1,3,3)
    imagesc(Ahat_lagBlks)
    title('debiased-byLag')
    suptitle([pars.dataType.dataID '-' pars.dataType.condition '-' pars.dataType.globalDataCat])
end
%% set savefile name
if cvLevel == 1
    cv_saveFile=[baseSaveFileName '_sparseModel' ...
            '_p' num2str(modelOrder) kStr];
    cv_saveFileFig=cv_saveFile;
    cv_saveFile=[cv_saveFile '.mat'];
elseif cvLevel == 0
    cv_saveFile=['finalSparseModel' ...
        '_p' num2str(modelOrder) '.mat'];
    cv_saveFileFig=['finalSparseModel' ...
        '_p' num2str(modelOrder)];
else
    crash
end
%% save final model's params
errors.lambda.lambdaWeightRange=pars.cv.lambda.lambdaWeightRange;
errors.p.lambdaWeightRange=pars.cv.p.dimRed.ch.lasso.lambdaWeightRange;
save([pwd filesep cv_saveFile],'Ahat_byCh','Ahat_lagBlks','errors')
load([pwd filesep cv_saveFile]) % ensure save file is not corrupted

%% save fig
if cvLevel==0
    saveas(finalParams,[pwd filesep cv_saveFileFig '.png'])
    savefig(finalParams,[pwd filesep cv_saveFileFig '.fig'])
    close all
end

