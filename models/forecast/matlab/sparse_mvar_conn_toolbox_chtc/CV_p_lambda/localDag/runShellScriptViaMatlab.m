function runShellScriptViaMatlab(pars,cvType,cvLevel,kInd_p,nVar,modelOrder,runType)

%% set lambda_K 
if ~pars.cv.lambda.run
    lambda_K=0;
else
    lambda_K=pars.cv.lambda.K;
end

%% setup and run scipt
scriptName='zipAllSingleChOutput_local.sh';
try
    linuxDir=pars.dirs.baseCodeDir;
catch why
    keyboard
end
linuxDir(strfind(linuxDir,'\'))='/';
scriptPath=['"' linuxDir 'chtc' '/' 'prepDags' '/' 'bashScripts' '/' 'pre' '/' scriptName];

cmdStr=['C:\cygwin64\bin\bash --login -c  ' scriptPath ' ' ...
    pars.dataType.globalDataCat ' ' ...
    pars.dataType.varSelect ' ' ...
    pars.dataType.dataID ' ' ...
    pars.dataType.condition ' ' ...
    getDagDirName(pars) ' ' ... %%%%%
    num2str(pars.cv.p.K) ' ' ...
    num2str(lambda_K) ' ' ... 
    num2str(kInd_p) ' ' ...
    cvType ' ' ...
    num2str(cvLevel) ' ' ...
    num2str(nVar) ' ' ...
    num2str(modelOrder) ' ' ...
    pars.nicknames(runType) ' ' ...
    num2str(length(pars.cv.p.modelOrderRange)) ' ' ...
    pars.dataType.catType '"'];
%             cmdStr=['C:\cygwin64\bin\bash --login -c  ' scriptPath ' 1"' ];
[exitStatus,scriptOut]=system(cmdStr);
if exitStatus ~= 0
    keyboard
end

%% move tar file to pwd since that's where it would be after submitting job
% dir where file count will take place
origDir=pwd;
dataDir=pwd;
cdToTarDir=[dataDir filesep cvType 'CV' filesep 'p' num2str(modelOrder) filesep 'lvl' num2str(cvLevel) 'fold' filesep];%default for most tars
if strcmp(cvType,"p")
    numFoldsLvl1=pars.cv.p.K;
    numFoldsLvl2=pars.cv.p.dimRed.ch.lasso.K;
    if cvLevel == 2 && strcmp(runType,'stitchSingleVarParams_measureErr')
        % for zipping all files needed for measuring error varied by lambda
        fileEnd=['p' num2str(modelOrder) '_K' num2str(kInd_p) '_lwK*'];
        nFilesPerCh=numFoldsLvl2;
        tarFileName=['k' num2str(kInd_p)];
    elseif cvLevel == 1
        if strcmp(runType,'stitchSingleVarParams_measureErr') || strcmp(runType,'construct_debiased_model_wBestLambdas')
            fileEnd=['p' num2str(modelOrder) '_K' num2str(kInd_p)];
            nFilesPerCh=1;
            tarFileName=['k' num2str(kInd_p)];
        elseif strcmp(runType,'measureAvgModelOrderErr')
            fileEnd=['p' num2str(modelOrder) '_K*'];
            nFilesPerCh=numFoldsLvl1;
            tarFileName='debiased';
        else
            crash
        end
    elseif strcmp(runType,'setModelOrder')
        fileEnd=['_p*'];
        nFilesPerCh=length(pars.cv.p.modelOrderRange);
        tarFileName='modelOrderErrs';
        cdToTarDir=[dataDir filesep cvType 'CV' filesep];
    else
        crash
    end
elseif strcmp(cvType,'lambda')
    numFoldsLvl1=pars.cv.lambda.K;
    numFoldsLvl2=nan;
    tarFileName=['allFiles'];
    if cvLevel==1
        nFilesPerCh=numFoldsLvl1;
        fileEnd='p*_K*';
%         cdToTarDir=[dataDir filesep cvType 'CV' filesep];
    elseif cvLevel==0
        nFilesPerCh=1;
        fileEnd='allData_p*';
%         cdToTarDir=[dataDir filesep cvType 'CV' filesep];
    else
        crash
    end
else
    crash
end
fileSize=dir([cdToTarDir filesep tarFileName '.tar.gz']);
if isempty(fileSize) || fileSize.bytes < 500
    keyboard % there must've been an error with the shell script if you encounter this.... or I guess it's possible the dataset is ultra-small, but that is unlikely to occur.
end
copyfile([cdToTarDir filesep tarFileName '.tar.gz'],[origDir filesep tarFileName '.tar.gz'])




