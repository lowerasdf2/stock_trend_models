function main_fit_data_chtc()

%% load pars var 
load([pwd filesep 'pars.mat'],'pars'); % save(pars) 

%% Remove everything from path except for folder that gets compiled
restoredefaultpath
addpath(genpath([pars.dirs.baseCodeDir '\chtc\sacredCompiledPrograms\v6']))
addpath(genpath([pars.dirs.baseCodeDir filesep 'chtc' filesep 'localDag']))

%% Begin condor pipeline
disp(['Running CV_p_lasso procedure on condition ' pars.dataType.condition '...'])

%% previously split data into epochs, stored train/test indices for each fold of CV_p and CV_p_lasso; saved in condor input dir
dataSaveDir=getEpochSaveDir(pars);% [pars.dirs.dimRedData filesep pars.dataType.dataID filesep pars.dataType.condition filesep];%[pars.dirs.dataDir filesep patientParams.patientID filesep pars.dataType.condition filesep];
dataSaveFile=getEpochFilename(pars); 

%% add job nicknames to pars
pars=setJobNicknames(pars);
% % % % % % % % % % % % % % % end
 % copy file from input data repo to current working directory (e.g.
% what Condor would do)
try
    copyfile([dataSaveDir filesep dataSaveFile],[pwd filesep dataSaveFile])
catch
    if exist([dataSaveDir filesep dataSaveFile],'file')
        % sometimes this fails
        x=load([dataSaveDir filesep dataSaveFile]);
        if length(fields(x))==5 && isfield(x,'dataEpochs') && isfield(x,'dataInfo') ...
                 && isfield(x,'indices_cv_p') && isfield(x,'indices_cv_p_lasso') && isfield(x,'n_epoch')
             dataEpochs=x.dataEpochs;
             dataInfo=x.dataInfo;
             indices_cv_p=x.indices_cv_p;
             indices_cv_p_lasso=x.indices_cv_p_lasso;
             n_epoch=x.n_epoch;
             save([pwd filesep dataSaveFile],'dataEpochs','dataInfo','indices_cv_p','indices_cv_p_lasso','n_epoch')
             clear dataEpochs dataInfo indices_cv_p indices_cv_p_lasso n_epoch
        else
            keyboard % my catch method is faulty if above is false. Need to add fields to save file because dataEpochs file was updated?
        end
    else
        error('dataEpochs file does not exist')
    end
end
% determine number of channels to loop through
load([pwd filesep dataSaveFile])
nVar=size(dataEpochs{1},1);
%% Loop through m.o.'s
if pars.cv.p.run
% % %     %% If optimizing single hyperparam (model-order), use pars.cv.lambda.K to control number of folds used in CV process, and set pars.cv.p.K to 1
% % %     % This approach allows us to use the same code to run CV to optimize
% % %     % single hyperparam and to optimize 2 hyperparams 
% % %     if ~pars.cv.lambda.run
% % %         maxKp=1;
% % %         pars.cv.p.dimRed.ch.lasso.K=pars.cv.p.K;
% % %     else
% % %         maxKp=pars.cv.p.K;
% % %     end
    for modelOrder=pars.cv.p.modelOrderRange
        if pars.run.bools.fixCorruptedDagFiles
            fixCorruptedDagFiles=pars.run.fixCorruptedDagFiles; % just to shorten var name
            if sum(fixCorruptedDagFiles.(pars.dataType.condition).modelOrders==modelOrder) == 0
                continue
            else
                paramInds.moMatch=fixCorruptedDagFiles.(pars.dataType.condition).modelOrders==modelOrder;
            end
        end
        disp(['Testing model performance w/ model-order=' num2str(modelOrder)])% kFold=' num2str(kInd) ' (level1 CV)'])
        for kInd_p=1:pars.cv.p.K
            if pars.run.bools.fixCorruptedDagFiles
                if sum(fixCorruptedDagFiles.(pars.dataType.condition).pKs(paramInds.moMatch)==kInd_p) == 0
                    continue
                else
                    paramInds.pksMatch=fixCorruptedDagFiles.(pars.dataType.condition).pKs==kInd_p;
                end
            end
            disp(['Beginning kFold=' num2str(kInd_p) ' (level-1 CVp)'])
            for kInd_pLasso=1:pars.cv.p.dimRed.ch.lasso.K
                if pars.run.bools.fixCorruptedDagFiles
                    if sum(fixCorruptedDagFiles.(pars.dataType.condition).lwKs==kInd_pLasso) == 0
                        continue
                    else
                        paramInds.lksMatch=fixCorruptedDagFiles.(pars.dataType.condition).lwKs==kInd_pLasso;
                    end
                end
                disp(['Beginning kFold2=' num2str(kInd_pLasso) ' (level-2 CVp)'])
                %% (A) Fit single channels using 2nd-level-CV folds of training data
                % Output: 'singleVarParams_kInd_kIndLasso'  
                cvLevel=2;
                cvType='p';
                if pars.run.bools.fixCorruptedDagFiles
                    if sum(fixCorruptedDagFiles.(pars.dataType.condition).foldLvls==cvLevel)==0
                        continue
                    else
                        paramInds.lvlMatch=fixCorruptedDagFiles.(pars.dataType.condition).foldLvls==cvLevel;
                    end
                    if sum(contains(fixCorruptedDagFiles.(pars.dataType.condition).cvType,cvType))==0 keyboard % replace contains with strcmp?
                        continue
                    else
                        paramInds.cvtypeMatch=contains(fixCorruptedDagFiles.(pars.dataType.condition).cvType,cvType);keyboard % replace contains with strcmp?
                    end
                    if sum(contains(fixCorruptedDagFiles.(pars.dataType.condition).failScript,'lFit'))==0 keyboard % replace contains with strcmp?
                        continue
                    else
                        paramInds.scriptMatch=contains(fixCorruptedDagFiles.(pars.dataType.condition).failScript,'lFit');
                    end
                    booleanMatrix=paramInds.moMatch;
                    booleanMatrix=[booleanMatrix;paramInds.pksMatch];
                    booleanMatrix=[booleanMatrix;paramInds.lksMatch];
                    booleanMatrix=[booleanMatrix;paramInds.lvlMatch];
                    booleanMatrix=[booleanMatrix;paramInds.cvtypeMatch];
                    booleanMatrix=[booleanMatrix;paramInds.scriptMatch];
                    dataInds=find(sum(booleanMatrix)==length(fields(paramInds)));
                    if isempty(dataInds)
                        continue
                    end
                    corruptVarInds=fixCorruptedDagFiles.(pars.dataType.condition).failedChs(dataInds);
                    for iVar=corruptVarInds
                        lassoFit_singleVar_v6(cvType,num2str(modelOrder),num2str(kInd_p),num2str(kInd_pLasso),num2str(iVar),num2str(cvLevel));%dirs updated
                        moveMatFilesToCorruptDagDir(pars);
                    end
                else
%                     parfor iVar=1:nVar
                    for iVar=1:nVar % I switch to non-parfor version when I want to debug
                        lassoFit_singleVar_v6(cvType,num2str(modelOrder),num2str(kInd_p),num2str(kInd_pLasso),num2str(iVar),num2str(cvLevel));%dirs updated
                    end
                end
            end
            %% Run post script using same bash script I use on condor: this script places all of the pLasso files outputed during outlerlevel of kFold into a k# folder
            % Places lvl2fold output files that belong to lvl1K# into: \dagDir\pCV\p#\lvl2fold\k#.tar.gz
            if pars.cv.p.dimRed.ch.lasso.K~=0 % zero implies no lasso fitting here
                if ~pars.run.bools.fixCorruptedDagFiles 
                    runShellScriptViaMatlab(pars,cvType,cvLevel,kInd_p,nVar,modelOrder,'stitchSingleVarParams_measureErr')
                end
                %% (B) Stick outputs together into one parameter file, measure error, save out avgCVerror varied by lambda (avg'd over all folds in 2nd layer)
                % Input: indices for train/test split, param files (one for each channel)
                % Output: pCV_avgCvErrByLambda_p#_K#.mat
                cvLevel=2;% this stitch uses models trained at 2nd layer of CV process
                cvType='p';
                if pars.run.bools.fixCorruptedDagFiles
                    if sum(fixCorruptedDagFiles.(pars.dataType.condition).foldLvls==cvLevel)==0
                        continue
                    else
                        paramInds.lvlMatch=fixCorruptedDagFiles.(pars.dataType.condition).foldLvls==cvLevel;
                    end
                    if sum(contains(fixCorruptedDagFiles.(pars.dataType.condition).cvType,cvType))==0
                        continue
                    else
                        paramInds.cvtypeMatch=contains(fixCorruptedDagFiles.(pars.dataType.condition).cvType,cvType);
                    end
                    if sum(contains(fixCorruptedDagFiles.(pars.dataType.condition).failScript,'stitch'))==0
                        continue
                    else
                        paramInds.scriptMatch=contains(fixCorruptedDagFiles.(pars.dataType.condition).failScript,'stitch');
                    end
                    booleanMatrix=paramInds.moMatch;
                    booleanMatrix=[booleanMatrix;paramInds.pksMatch];
                    booleanMatrix=[booleanMatrix;paramInds.lksMatch];
                    booleanMatrix=[booleanMatrix;paramInds.lvlMatch];
                    booleanMatrix=[booleanMatrix;paramInds.cvtypeMatch];
                    booleanMatrix=[booleanMatrix;paramInds.scriptMatch];
                    dataInds=find(sum(booleanMatrix)==length(fields(paramInds)));
                    keyboard
                    stitchSingleVarParams_measureErr_v6(cvType,num2str(cvLevel),num2str(modelOrder),num2str(kInd_p));%dirs updated
                    moveMatFilesToCorruptDagDir(pars);
                else
                    stitchSingleVarParams_measureErr_v6(cvType,num2str(cvLevel),num2str(modelOrder),num2str(kInd_p));%dirs updated
                end
            end
            %% (C or A2) Train (debiased) models using level1fold; fits params for all vals of lambda (could be adapted to prevent this) 
            % --> step D selects only those models needed (determined at step B)
            % Output: 'singleVarParams_fullTrainSet_kInd'  
            cvLevel=1;
            kInd_pLasso=nan;
            cvType='p';
            if pars.run.bools.fixCorruptedDagFiles
                if sum(isnan(fixCorruptedDagFiles.(pars.dataType.condition).lwKs))==0
                    continue
                else
                    paramInds.pksMatch=isnan(fixCorruptedDagFiles.(pars.dataType.condition).lwKs);
                end
                if sum(fixCorruptedDagFiles.(pars.dataType.condition).foldLvls==cvLevel)==0
                    continue
                else
                    paramInds.lvlMatch=fixCorruptedDagFiles.(pars.dataType.condition).foldLvls==cvLevel;
                end
                if sum(contains(fixCorruptedDagFiles.(pars.dataType.condition).cvType,cvType))==0
                    continue
                else
                    paramInds.cvtypeMatch=contains(fixCorruptedDagFiles.(pars.dataType.condition).cvType,cvType);
                end
                if sum(contains(fixCorruptedDagFiles.(pars.dataType.condition).failScript,'stitch'))==0
                    continue
                else
                    paramInds.scriptMatch=contains(fixCorruptedDagFiles.(pars.dataType.condition).failScript,'stitch');
                end
                booleanMatrix=paramInds.moMatch;
                booleanMatrix=[booleanMatrix;paramInds.pksMatch];
                booleanMatrix=[booleanMatrix;paramInds.lksMatch];
                booleanMatrix=[booleanMatrix;paramInds.lvlMatch];
                booleanMatrix=[booleanMatrix;paramInds.cvtypeMatch];
                booleanMatrix=[booleanMatrix;paramInds.scriptMatch];
                dataInds=find(sum(booleanMatrix)==length(fields(paramInds)));
                if isempty(dataInds)
                    continue
                end
                corruptVarInds=fixCorruptedDagFiles.(pars.dataType.condition).failedChs(dataInds);
                for iVar=corruptVarInds
                    lassoFit_singleVar_v6(cvType,num2str(modelOrder),num2str(kInd_p),num2str(kInd_pLasso),num2str(iVar),num2str(cvLevel));%dirs updated
                    moveMatFilesToCorruptDagDir(pars);
                end
            else
                if pars.cv.lambda.run
                    % fit lasso models using entire outerfold of data.
                    % output results for all options of lamdbda
                    for iVar=1:nVar
%                     parfor iVar=1:nVar
                        lassoFit_singleVar_v6(cvType,num2str(modelOrder),num2str(kInd_p),num2str(kInd_pLasso),num2str(iVar),num2str(cvLevel));%dirs updated
                    end
                else
                    % OR simply fit fully connected model using entire
                    % outerfold of data.
%                     keyboard % Outfile should look like this: pCV_p$(modelOrder)_K$(kInd_p).mat s.t. it matches pattern of output of construct_debiased_model, "pCV_sparseModel_p$(modelOrder)_K$(kInd_p).mat"
                    fit_allVars_v6(num2str(modelOrder),num2str(cvLevel),num2str(kInd_p))    
                end
            end
            
            if pars.cv.p.dimRed.ch.lasso.bool
                %% Run prescript of construct_debiased_model_wBestLambdas_v6
                % places lvl1fold files into
                % dagDir\pCV\p#\lvl1fold\k#.tar.gz file
                if ~pars.run.bools.fixCorruptedDagFiles
                    runShellScriptViaMatlab(pars,cvType,cvLevel,kInd_p,nVar,modelOrder,'construct_debiased_model_wBestLambdas')
                end
            
                %% (D) Construct debiased model params (concatenate fitted params across channels) 
                % Using level1Fold for debiasingTrainSet and optimal lambdas (determined @stepB), debias model to construct final model @kInd
                % Outfile: pCV_sparseModel_p$(modelOrder)_K$(kInd_p).mat
                cvLevel=1;% controls what kind of trainingSet to use for debiased param estimates
                cvType='p';
                if pars.run.bools.fixCorruptedDagFiles
                    if sum(contains(fixCorruptedDagFiles.(pars.dataType.condition).failScript,'lFit'))~=length(fixCorruptedDagFiles.(pars.dataType.condition).failScript)
                        keyboard % new corrupt file from running script other than lFit; add code here
                    end
                else
                    construct_debiased_model_wBestLambdas_v6(cvType,num2str(cvLevel),num2str(modelOrder),num2str(kInd_p));
                end
            end
        end
        %% pre-script of measureAvgModelOrderErr_v6
        if ~pars.run.bools.fixCorruptedDagFiles
            % This one places all of the pCV_sparseModel_p$(modelOrder)_K$(lvl1K#).mat files
            % (which represent best sparse model optimized on each lvl1K# fold of data)
            % into dagDir\pCV\p$(modelOrder)\lvl1fold\debiased.tar.gz 
            runShellScriptViaMatlab(pars,cvType,cvLevel,kInd_p,nVar,modelOrder,'measureAvgModelOrderErr')
        end
        %% (E) avg test error across K-folds (outer/1st fold)
        % Uses best lambdas determined @stepB and sums error of models
        % estimated @stepC at those particular lambas. These models are
        % estimated using larger training sets (level1folds) than those
        % used to determine optimal lambdas.
        % Output: Saves out estimated model-error for this particular model-order    
        if pars.run.bools.fixCorruptedDagFiles
            if sum(contains(fixCorruptedDagFiles.(pars.dataType.condition).failScript,'lFit'))~=length(fixCorruptedDagFiles.(pars.dataType.condition).failScript)
                keyboard % new corrupt file from running script other than lFit; add code here
            end
        else
            measureAvgModelOrderErr_v6(num2str(modelOrder));
        end
    end
end
%% (F) setModelOrder: Within each condition, select best model-order based on error files (each input file = avgError across folds for one model-order; one input for each model-order tested)
% Input: 1 file for each hyperparam tested; file contains error of
% hyperparam
% Output: errorsByVars file (summary of errors across hyperparams);
% also contains best model order to use
cvType='p';
cvLevel=nan;
modelOrder=nan;
kInd_p=nan;
if ~pars.run.bools.fixCorruptedDagFiles && pars.cv.p.run
    runShellScriptViaMatlab(pars,cvType,cvLevel,kInd_p,nVar,modelOrder,'setModelOrder')
end
if pars.run.bools.fixCorruptedDagFiles
    if sum(contains(fixCorruptedDagFiles.(pars.dataType.condition).failScript,'lFit'))~=length(fixCorruptedDagFiles.(pars.dataType.condition).failScript)
        keyboard % new corrupt file from running script other than lFit; add code here
    end
else
    setModelOrder_v6();
end

%% Now that model order is determined, do another round of CV (single layer this time) for lambda hyperparameter to determine finalized lambda vals
% Note: optimized lambdas used during CV_p process use a smaller
% portion of training data per fold given it uses nested CV. This
% round avoids that bias by using entire folds for CV_lambda
% process.
%% (G1)
if pars.cv.lambda.run
    for kInd_LASSO=1:pars.cv.lambda.K
        disp(['Beginning kFold=' num2str(kInd_LASSO) ' (last round of CV: level-1 CV_lambda)'])
        % Output: 'singleVarParams_kInd_kIndLasso'  
        cvLevel=1;
        kInd_pLasso=nan;
        cvType='lambda';
        modelOrder=nan;
        if pars.run.bools.fixCorruptedDagFiles
            if sum(fixCorruptedDagFiles.(pars.dataType.condition).foldLvls==cvLevel) > 0
                keyboard % new corrupt file w/ different settings than encountered previously; add code here to take care of this
            end
        else
            for iVar=1:nVar
%             parfor iVar=1:nVar
                % kInd_LASSO takes place of kInd_p here
                lassoFit_singleVar_v6(cvType,num2str(modelOrder),num2str(kInd_LASSO),num2str(kInd_pLasso),num2str(iVar),num2str(cvLevel));%dirs updated
            end
        end
    end
    %% Replicate post-script on condor: store all chFiles in folder that makes sense
    cvLevel=1;
    cvType='lambda';
    kInd=nan;
    modelOrder=nan;
    if ~pars.run.bools.fixCorruptedDagFiles
        runShellScriptViaMatlab(pars,cvType,cvLevel,kInd_p,nVar,modelOrder,'stitchSingleVarParams_measureErr')
    end
    %% (H) Stick outputs together into one parameter file, measure error, save out avgCVerror varied by lambda (avg'd pars.cv.lambda.K folds)
    % Input: indices for train/test split, param files (one for each channel)
    % Output: 'nestedCV_kInd_avgCvErrByLambda' file  
    cvType='lambda';
    cvLevel=1;
    modelOrder=nan;
    lvl1kInd=nan;
    if pars.run.bools.fixCorruptedDagFiles
        if sum(contains(fixCorruptedDagFiles.(pars.dataType.condition).failScript,'lFit'))~=length(fixCorruptedDagFiles.(pars.dataType.condition).failScript)
            keyboard % new corrupt file from running script other than lFit; add code here
        end
    else
        stitchSingleVarParams_measureErr_v6(cvType,num2str(cvLevel),num2str(modelOrder),num2str(lvl1kInd));%dirs updated
    end
    %% (G2) Fit biased model using ALL data (****this can run independently after model-order is determined)
    % Output: 'singleVarParams_kInd_kIndLasso'  
    cvLevel=0;
    kInd_pLasso=nan;
    lvl1kInd=nan;
    cvType='lambda';
    modelOrder=nan;
    if pars.run.bools.fixCorruptedDagFiles
        if sum(fixCorruptedDagFiles.(pars.dataType.condition).foldLvls==cvLevel) > 0
            keyboard % new corrupt file w/ different settings than encountered previously; add code here to take care of this
        end
    else
        for iVar=1:nVar
%         parfor iVar=1:nVar
            % kInd_LASSO takes place of kInd_p here
            lassoFit_singleVar_v6(cvType,num2str(modelOrder),num2str(lvl1kInd),num2str(kInd_pLasso),num2str(iVar),num2str(cvLevel));%dirs updated
        end
    end
    %% Replicate post-script on condor: store all chFiles in folder that makes sense
    cvType='lambda';
    cvLevel=0;
    modelOrder=nan;
    lvl1kInd=nan;
    kInd_p=nan;
    if ~pars.run.bools.fixCorruptedDagFiles
        runShellScriptViaMatlab(pars,cvType,cvLevel,kInd_p,nVar,modelOrder,'construct_debiased_model_wBestLambdas')
    end     

    %% (I)
    % Construct biased model params (concatenate fitted params across channels) 
    % when using ALL data for trainSet (none left out/no folds),
    % optimal lambdas, and optimal m.o. -> debias model to construct
    % FINAL_MODEL used for extracting connectivity estimates.
    cvType='lambda';
    cvLevel=0;
    modelOrder=nan;
    lvl1kInd=nan;
    if pars.run.bools.fixCorruptedDagFiles
        if sum(fixCorruptedDagFiles.(pars.dataType.condition).foldLvls==cvLevel) > 0
            keyboard % new corrupt file w/ different settings than encountered previously; add code here to take care of this
        end
    else
        construct_debiased_model_wBestLambdas_v6(cvType,num2str(cvLevel),num2str(modelOrder),num2str(lvl1kInd));%dirs updated
    end
elseif pars.cv.p.run && ~pars.cv.p.dimRed.ch.lasso.bool
    modelOrder=nan;
    cvLevel=0;
    kInd_p=nan;
    fit_allVars_v6(num2str(modelOrder),num2str(cvLevel),num2str(kInd_p))  
end




