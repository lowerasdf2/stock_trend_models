function measPCAerr(pars,batchParams)

%% pca back-projection program
% determine if PCA produces too much error based on arbitrary
% subjectiveness


% set folder containing final models, load final model
pars.config.dirs.baseResultsDir='M:\PassiveEphys\Analfysis Results\Chris\ecog_connectivity\chtc_complete\';%'M:\PassiveEphys\Analysis Results\Chris\ecog_connectivity\chtc\complete\';
addpath([pars.config.dirs.baseCodeDir filesep '..' filesep  'plot_connectivity\paul-kassebaum-mathworks-circularGraph-3a7926b'])
addpath([pars.config.dirs.baseCodeDir filesep '..' filesep  '3rdParty' filesep 'blockMVAR'])
dagDir=createDagDirName(pars);
roiErrsByCond=containers.Map;
for cond=batchParams.cond
    cond=cond{1};
    if ~strcmp(cond,'OAAS5')
        disp('skipping all cons but OAAS5')
        continue
    end
    pars.dataType.condition=cond;
    %% step1: load in finalized params (AHAT_pca)
    ppDir=[pars.config.dirs.baseResultsDir filesep pars.dataType.globalDataCat filesep pars.dataType.varSelect filesep pars.dataType.dataID filesep pars.dataType.condition filesep dagDir];
    finalModelFile=dir([ppDir filesep 'finalSparseModel*.mat']);
    if isempty(finalModelFile)
        keyboard
    end
    finalModelFile=finalModelFile(1).name;
    % load final model
    loadData=load([ppDir filesep finalModelFile]);
    AHAT_pca=loadData.Ahat_lagBlks;
    AHAT_byVar_pca=loadData.Ahat_byVar;
    clear loadData
    modelOrder=size(AHAT_pca,2)/size(AHAT_pca,1);
    %% load epochData, focus on first epoch
    epochDir=[pars.config.dirs.dimRedData pars.dataType.dataID filesep pars.dataType.condition filesep];
    epochFile=getEpochFilename(pars); 
    epochFile=dir([epochDir filesep epochFile]);
    if length(epochFile) ~=1
        keyboard
    else
        loadData=load([epochDir filesep epochFile(1).name],'dataEpochs','dataInfo');
        dataEpochs=loadData.dataEpochs;
        dataInfo=loadData.dataInfo;
        clear loadData
    end
    roiErrs=[];
    if pars.cv.genDimRed.ch.PCA.run
        %% step2: produce 1-step error (YHAT_pca) using AHAT_pca and DESIGN_pca
        [Y_pca DESIGN_pca]=buildMats(horzcat(dataEpochs{:}),modelOrder); % builds design matrix (Z) along with outcome variable matrix (Y); design matrix is size (T-p) * (nVar*p)
        YHAT_pca=DESIGN_pca*AHAT_byVar_pca;
        err=(Y_pca-YHAT_pca)';
        Q=err*err';
        chErr=diag(Q)./size(Y_pca,1);

        %% step3: load in pca coefs
        coefDir=[pars.config.dirs.baseResultsDir filesep 'PCA' filesep 'coefByROImaps' filesep];
        fileName=[pars.dataType.dataID '_' num2str(pars.dataType.sampleRate) 'hz_' pars.dataType.globalDataCat '_' pars.dataType.varSelect '_PCAA' num2str(pars.cv.genDimRed.ch.PCA.perc*100) '.mat'];
        loadData=load([coefDir filesep fileName]);
        coefMap=loadData.coefMap;
        clear loadData
        roiKeys=keys(coefMap);
        for roiInd=1:length(roiKeys)
            roi=roiKeys{roiInd};
            chInds=find(strcmp(dataInfo.label,roi));
            YHAT_1ROI_pca=YHAT_pca(:,chInds)';
            Y_1ROI_pca=Y_pca(:,chInds)';
            coefs=coefMap(roi);
            %% create back-projected data from 1-step predictions of pca_data (y_hat)
            YHAT=(YHAT_1ROI_pca'*coefs(:,1:length(chInds))')';
            Y_backProjected=(Y_1ROI_pca'*coefs(:,1:length(chInds))')';
            
            plotLen=300;
            dataTraceFig=figure;
            nVarSingleRoi=size(YHAT,1);
            Y=loadOrigData();
            chInds=find(contains(Y.label,roi));
            Y=Y.trial{1}(chInds,modelOrder+1:end);
            for iVar=1:nVarSingleRoi
                subplot(nVarSingleRoi,1,iVar)
                plot(1:plotLen,Y(iVar,1:plotLen))
                hold on
                plot(1:plotLen,YHAT(iVar,1:plotLen))
                xlim([1,plotLen])
            end
% % %             figure(dataTraceFig)
% % %             suptitle(roi(3:end))
% % %             figDir='M:\PassiveEphys\Analysis Results\Chris\ecog_connectivity\PCA\backProjectionErrCheck\';
% % %             figName=[roi(3:end) '-pca' num2str(pars.cv.genDimRed.ch.PCA.perc*100)];
% % %             saveas(dataTraceFig,[figDir filesep figName '.png'])
% % %             savefig(dataTraceFig,[figDir filesep figName '.fig'])
            err=(Y(:,1:size(YHAT,2))-YHAT);
            Q=err*err';
            chErr=diag(Q)./size(YHAT,2);
            roiErrs(length(roiErrs)+1:length(roiErrs)+length(chErr))=chErr;
            
            backProjectionOnlyFig=figure;
            nVarSingleRoi=size(YHAT,1);
           
            for iVar=1:nVarSingleRoi
                subplot(nVarSingleRoi,1,iVar)
                plot(1:plotLen,Y(iVar,1:plotLen))
                hold on
                plot(1:plotLen,Y_backProjected(iVar,1:plotLen))
                xlim([1,plotLen])
            end
            figure(backProjectionOnlyFig)
            suptitle(roi(3:end))
            figDir='M:\PassiveEphys\Analysis Results\Chris\ecog_connectivity\PCA\backProjectionErrCheck\';
            figName=[roi(3:end) '-backProjectPca' num2str(pars.cv.genDimRed.ch.PCA.perc*100)];
            saveas(dataTraceFig,[figDir filesep figName '.png'])
            savefig(dataTraceFig,[figDir filesep figName '.fig'])
           
        end
    else
        roiKeys=unique(dataInfo.label);
        [Y DESIGN]=buildMats(horzcat(dataEpochs{:}),modelOrder); % builds design matrix (Z) along with outcome variable matrix (Y); design matrix is size (T-p) * (nVar*p)
        Y=Y';
        Y=(zscore(Y'))';
        YHAT=zscore(DESIGN*AHAT_byVar_pca)';
        YHAT=zscore(YHAT);
        for roiInd=1:length(roiKeys)
            roi=roiKeys{roiInd};
            chInds=find(contains(dataInfo.label,roi));
            plotLen=300;
            dataTraceFig=figure;
            nVarSingleRoi=length(chInds);
            for iVar=1:nVarSingleRoi
                subplot(nVarSingleRoi,1,iVar)
                try
                    plot(1:plotLen,Y(chInds(iVar),1:plotLen))
                catch why
                    keyboard
                end
                hold on
                plot(1:plotLen,YHAT(chInds(iVar),1:plotLen))
                xlim([1,plotLen])
            end
            figure(dataTraceFig)
            suptitle(roi(3:end))
            err=(Y(chInds,:)-YHAT(chInds,:));
            Q=err*err';
            chErr=diag(Q)./size(YHAT,2);
            roiErrs(length(roiErrs)+1:length(roiErrs)+length(chErr))=chErr;
        end
    end
    roiErrsByCond(cond)=roiErrs;
end
% keyboard
mean(roiErrs)

function condData=loadOrigData()

%%  compare y and y_hat 
batchParams=load_batch_params_CME(pars);
if length(batchParams) > 1
    keyboard % using batchParms var instead of patientParams, so use this to double check that batchParams does not have more than one dataID's data stored
end
%% Convert it to fieldtrip format, load selected data
cfg = [];
if strcmp(pars.dataType.globalDataCat,'OR-RS')
    cfg.rsOnly=true;
elseif strcmp(pars.dataType.globalDataCat,'OR-EV')
    cfg.rsOnly=false;
else
    error('Need to add another check to detect if data is resting state or not for loading purposes in below lines.')
end
cfg.condition      = pars.dataType.condition;
cfg.patientID      = pars.dataType.dataID;
cfg.ECoGchannels   = batchParams.ECoGchannels;
cfg.signalName     = batchParams.signalName;
cfg.dataPrefix     = batchParams.dataPrefix;
cfg.data_dir=LocalPaths.Ecog_Data_Dir;
cfg.dataType       = pars.dataType;
%             cfg.data_dir       = pars.config.dirs.baseDataDir;
cfg.downsample     = 0;%300; % Not downsampling w/ field trip anymore, doing it on my own afterwards
[condData, trial_times, trials_by_type] = load_condition_ecog(cfg);
condData.trial_times=condData;
condData.trial_times=trials_by_type;
% trialTimesMap(cond)=trial_times;
% trialsByTypeMap(cond)=trials_by_type;
% downsample
if pars.downsampleBool
    downSampleFactor = condData.fsample/pars.dataType.sampleRate;
    condData.trial = {downsample(condData.trial{1}',downSampleFactor)'};
    condData.fsample = condData.fsample/downSampleFactor;
end
% trialLengthsMap(condition{1})=size(condData.trial{1},2);
% timeVecMap(condition{1})=condData.time;
% normalize w/in condition before concatenating across conditions
if pars.cv.genDimRed.ch.PCA.norm_cond
    condData.trial{1}=zscore(condData.trial{1}')';
end

end


end