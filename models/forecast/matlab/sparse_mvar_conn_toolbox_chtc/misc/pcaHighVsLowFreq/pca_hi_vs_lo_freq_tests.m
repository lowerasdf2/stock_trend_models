function pca_hi_vs_lo_freq_tests()
%% create high-pass and low-pass filtered data versions
fpass=4;
fpass=30;
if use_zscore
    B_lopass=zscore(lowpass(A,fpass,pars.dataType.sampleRate));
    B_hipass=zscore(highpass(A,fpass,pars.dataType.sampleRate));
else
    B_lopass=lowpass(A,fpass,pars.dataType.sampleRate);
    B_hipass=highpass(A,fpass,pars.dataType.sampleRate);
end

if useSVD % standard PCA option
    %% Keep maxDataPts data points for SVD
    hyperParamsString=[hyperParamsString '_svdVers'];
    [COEFF_lopass SCORE_lopass]=pca(B_lopass);
    [COEFF_hipass SCORE_hipass]=pca(B_hipass);
else
    hyperParamsString=[hyperParamsString '_covVers'];
    [COEFF_lopass SCORE_lopass]=pcacov(cov(B_lopass));
    [COEFF_hipass SCORE_hipass]=pcacov(cov(B_hipass));
end

%% Transform original data using coefs/eigenvectors extracted using only high/low-pass filtered data
SCORE_B_lopass=B*COEFF_lopass;
SCORE_B_hipass=B*COEFF_hipass;
cumsumvar_B_lopass=cumsum(var(SCORE_B_lopass)) / sum(var(SCORE_B_lopass));
cumsumvar_B_hipass=cumsum(var(SCORE_B_hipass)) / sum(var(SCORE_B_hipass));
var_B_lopass=(var(SCORE_B_lopass)) / sum(var(SCORE_B_lopass));
var_B_hipass=(var(SCORE_B_hipass)) / sum(var(SCORE_B_hipass));

hi_lo_cumVar_fig=figure;
data=[cumsumVar;cumsumvar_B_lopass;cumsumvar_B_hipass];
%     bar(data')
plot(data')
title({[dataIDparams.patientID ': ' strrep(roi,'_','-')],[ 'Cumulative Variance Explained: Unfiltered vs. Low-feq. vs. Hi-freq']})
%     set(gca,'YScale','lin')

figDir=[pars.dirs.baseResultsDir filesep 'PCA_hi_lo_freq' filesep 'cumVar' filesep pars.dataType.varSelect];
figName=[roi '_' dataIDparams.patientID createFileEnd_fitECoG(pars) '-hiVsLoFreq-cumVar' hyperParamsString];
if ~exist(figDir,'dir')
    mkdir(figDir)
end
savefig(hi_lo_cumVar_fig,[figDir filesep figName])
saveas(hi_lo_cumVar_fig,[figDir filesep figName],'png')

hi_lo_var_fig=figure;
data=[var_B;var_B_lopass;var_B_hipass];
%     bar(data')
plot(data')
title({[dataIDparams.patientID ': ' strrep(roi,'_','-')],[ 'Variance Explained Per PC: Unfiltered vs. Low-feq. vs. Hi-freq']})
%     set(gca,'YScale','lin')

figDir=[pars.dirs.baseResultsDir filesep 'PCA_hi_lo_freq' filesep 'varPerPC' filesep pars.dataType.varSelect];
figName=[roi '_' dataIDparams.patientID createFileEnd_fitECoG(pars) '-hiVsLoFreq-varPerPC' hyperParamsString];
if ~exist(figDir,'dir')
    mkdir(figDir)
end
savefig(hi_lo_var_fig,[figDir filesep figName])
saveas(hi_lo_var_fig,[figDir filesep figName],'png')


%% Plot raw signal, score of raw signal ,score using lopass eigenvectors, score using hipass eigenvectors
rawDataTraces=figure('units','normalized','outerposition',[0 0 1 1]);
pca_scores=figure('units','normalized','outerposition',[0 0 1 1]);
PSD_fig=figure('units','normalized','outerposition',[0 0 1 1]);
numRows=6;
numCols=6;
if numCols > size(B,2)
    numCols=size(B,2);
end
plotInd=1;
startInd=1;
endInd=500;
fs=pars.dataType.sampleRate;
for iVar=1:numCols
    figure(rawDataTraces)
    rowInd=1;
    plotInd=(numCols*rowInd)-(numCols-iVar);
    subtightplot(numRows,numCols,plotInd)
    plot(B(startInd:endInd,iVar)')
    xlim([0,endInd])
    set(gca,'xticklabel',{[]})    
    set(gca,'yticklabel',{[]})   
    if iVar == 1
        ylabel({['Raw Data Signals','(d.s. to ' num2str(pars.dataType.sampleRate) ' Hz)']})
    end
    title(['Ch' num2str(iVar)])
    ax=gca;
    k=.03;
    ax.TickLength=[k, k]; % Make tick marks longer.
    ax.LineWidth=k*20; % Make tick marks thicker.

    figure(PSD_fig)
    subtightplot(numRows,numCols,plotInd)
    x=B(startInd:endInd,iVar)';
    y=fft(x);
    n=length(x);          % number of samples
    f=(0:n-1)*(fs/n);     % frequency range
    power=abs(y).^2/n;    % power of the DFT
    [Pxx,F]=periodogram(x,[],length(x),fs);
    semilogy(F, Pxx)
    xlim([0,max(f)/2])
    ylim([10^-5,10^0])
    set(gca,'XScale','log')
    xticks([1,10,100])
    xticklabels({'1','10','100'})
    set(gca,'xticklabel',{[]})    
    set(gca,'yticklabel',{[]})    
    if iVar == 1
        ylabel('Log Power - Raw Data')
    end
    title(['Ch' num2str(iVar)])
    ax=gca;
    k=.05;
    ax.TickLength=[k, k]; % Make tick marks longer.
    ax.LineWidth=k*20; % Make tick marks thicker.

    figure(rawDataTraces)
    rowInd=rowInd+1;
    plotInd=(numCols*rowInd)-(numCols-iVar);
    subtightplot(numRows,numCols,plotInd)
    plot(B_hipass(startInd:endInd,iVar)')
    xlim([0,endInd])
    set(gca,'xticklabel',{[]})    
    set(gca,'yticklabel',{[]})   
    if iVar == 1
        ylabel('High Pass (30 Hz) Data')
    end
    ax=gca;
    k=.03;
    ax.TickLength=[k, k]; % Make tick marks longer.
    ax.LineWidth=k*20; % Make tick marks thicker.

    figure(PSD_fig)
    subtightplot(numRows,numCols,plotInd)
    x=B_hipass(startInd:endInd,iVar)';
    y=fft(x);
    n=length(x);          % number of samples
    f=(0:n-1)*(fs/n);     % frequency range
    power=abs(y).^2/n;    % power of the DFT
    [Pxx,F]=periodogram(x,[],length(x),fs);
    semilogy(F, Pxx)
    xlim([0,max(f)/2])
    ylim([10^-5,10^0])
    set(gca,'XScale','log')
    xticks([1,10,100])
    xticklabels({'1','10','100'})
    set(gca,'xticklabel',{[]})    
    set(gca,'yticklabel',{[]})   
    if iVar == 1
        ylabel('Log Power - Highpass')
    end
    ax=gca;
    k=.05;
    ax.TickLength=[k, k]; % Make tick marks longer.
    ax.LineWidth=k*20; % Make tick marks thicker.

    figure(rawDataTraces)
    rowInd=rowInd+1;
    plotInd=(numCols*rowInd)-(numCols-iVar);
    subtightplot(numRows,numCols,plotInd)
    plot(B_lopass(startInd:endInd,iVar)')
    xlim([0,endInd])
    set(gca,'xticklabel',{[]})    
    set(gca,'yticklabel',{[]})   
    if iVar == 1
        ylabel('Low Pass (4 Hz) Data')
    end
    ax=gca;
    k=.03;
    ax.TickLength=[k, k]; % Make tick marks longer.
    ax.LineWidth=k*20; % Make tick marks thicker.

    figure(PSD_fig)
    subtightplot(numRows,numCols,plotInd)
    x=B_lopass(startInd:endInd,iVar)';
    y=fft(x);
    n=length(x);          % number of samples
    f=(0:n-1)*(fs/n);     % frequency range
    power=abs(y).^2/n;    % power of the DFT
    [Pxx,F]=periodogram(x,[],length(x),fs);
    semilogy(F, Pxx)
    xlim([0,max(f)/2])
    ylim([10^-5,10^0])
    set(gca,'XScale','log')
    xticks([1,10,100])
    xticklabels({'1','10','100'})
    set(gca,'xticklabel',{[]})    
    set(gca,'yticklabel',{[]})   
    if iVar == 1
        ylabel('Log Power - Lowpass')
    end
    ax=gca;
    k=.05;
    ax.TickLength=[k, k]; % Make tick marks longer.
    ax.LineWidth=k*20; % Make tick marks thicker.

    figure(rawDataTraces)
    rowInd=rowInd+1;
    plotInd=(numCols*rowInd)-(numCols-iVar);
    subtightplot(numRows,numCols,plotInd)
    plot(SCORE(startInd:endInd,iVar)')
    xlim([0,endInd])
    set(gca,'xticklabel',{[]})    
    set(gca,'yticklabel',{[]})   
    if iVar == 1
        ylabel({'PCA scores', 'Raw data eigvecs'})
    end
    ax=gca;
    k=.03;
    ax.TickLength=[k, k]; % Make tick marks longer.
    ax.LineWidth=k*20; % Make tick marks thicker.

    figure(PSD_fig)
    subtightplot(numRows,numCols,plotInd)
    x=SCORE(startInd:endInd,iVar)';
    y=fft(x);
    n=length(x);          % number of samples
    f=(0:n-1)*(fs/n);     % frequency range
    power=abs(y).^2/n;    % power of the DFT
    [Pxx,F]=periodogram(x,[],length(x),fs);
    semilogy(F, Pxx)
    xlim([0,max(f)/2])
    ylim([10^-5,10^0])
    set(gca,'XScale','log')
    xticks([1,10,100])
    xticklabels({'1','10','100'})
    set(gca,'xticklabel',{[]})    
    set(gca,'yticklabel',{[]})  
    if iVar == 1
        ylabel({'Log Power','Scores','Raw signal eigvecs'})
    end
    ax=gca;
    k=.05;
    ax.TickLength=[k, k]; % Make tick marks longer.
    ax.LineWidth=k*20; % Make tick marks thicker.

    figure(rawDataTraces)
    rowInd=rowInd+1;
    plotInd=(numCols*rowInd)-(numCols-iVar);
    subtightplot(numRows,numCols,plotInd)
    plot(SCORE_B_hipass(startInd:endInd,iVar)')
    xlim([0,endInd])
    set(gca,'xticklabel',{[]})    
    set(gca,'yticklabel',{[]})   
    if iVar == 1
        ylabel({'PCA scores','Highpass eigvecs'})
    end
    ax=gca;
    k=.03;
    ax.TickLength=[k, k]; % Make tick marks longer.
    ax.LineWidth=k*20; % Make tick marks thicker.

    figure(PSD_fig)
    subtightplot(numRows,numCols,plotInd)
    x=SCORE_B_hipass(startInd:endInd,iVar)';
    y=fft(x);
    n=length(x);          % number of samples
    f=(0:n-1)*(fs/n);     % frequency range
    power=abs(y).^2/n;    % power of the DFT
    [Pxx,F]=periodogram(x,[],length(x),fs);
%         plot(F,10*log10(Pxx))
    semilogy(F, Pxx)
    xlim([0,max(f)/2])
    ylim([10^-5,10^0])
    set(gca,'XScale','log')
    xticks([1,10,100])
    xticklabels({'1','10','100'})
    set(gca,'xticklabel',{[]})    
    set(gca,'yticklabel',{[]})  
    if iVar == 1
        ylabel({'Log Power','Scores','Highpass eigvecs'})
    end
    ax=gca;
    k=.05;
    ax.TickLength=[k, k]; % Make tick marks longer.
    ax.LineWidth=k*20; % Make tick marks thicker.

    figure(rawDataTraces)
    rowInd=rowInd+1;
    plotInd=(numCols*rowInd)-(numCols-iVar);
    subtightplot(numRows,numCols,plotInd)
    plot(SCORE_B_lopass(startInd:endInd,iVar)')
    xlim([0,endInd])
    if iVar ~= 1
        set(gca,'xticklabel',{[]})    
        set(gca,'yticklabel',{[]})   
    end
    if iVar == 1
        ylabel({'PCA scores','Lowpass eigvecs'})
    end
    ax=gca;
    k=.03;
    ax.TickLength=[k, k]; % Make tick marks longer.
    ax.LineWidth=k*20; % Make tick marks thicker.

    figure(PSD_fig)
    subtightplot(numRows,numCols,plotInd)
    x=SCORE_B_lopass(startInd:endInd,iVar)';
    y=fft(x);
    n=length(x);          % number of samples
    f=(0:n-1)*(fs/n);     % frequency range
    power=abs(y).^2/n;    % power of the DFT
    [Pxx,F]=periodogram(x,[],length(x),fs);
    semilogy(F, Pxx)
    xlim([0,max(f)/2])
    ylim([10^-5,10^0])
    set(gca,'XScale','log')
    xticks([1,10,100])
    xticklabels({'1','10','100'})
    if iVar ~= 1
        set(gca,'xticklabel',{[]})    
        set(gca,'yticklabel',{[]})    
    end
    if iVar == 1
        ylabel({'Log Power','Scores', 'Lowpass eigvecs'})
    end
    ax=gca;
    k=.05;
    ax.TickLength=[k, k]; % Make tick marks longer.
    ax.LineWidth=k*20; % Make tick marks thicker.
end
figure(rawDataTraces)
figDir=[pars.dirs.baseResultsDir filesep 'PCA_hi_lo_freq' filesep 'rawDataTraces' filesep pars.dataType.varSelect];
figName=[roi '_' dataIDparams.patientID createFileEnd_fitECoG(pars) '-hiVsLoFreq-rawDataTraces' hyperParamsString];
if ~exist(figDir,'dir')
    mkdir(figDir)
end
savefig(gcf,[figDir filesep figName])
saveas(gcf,[figDir filesep figName],'png')

figure(PSD_fig)
figDir=[pars.dirs.baseResultsDir filesep 'PCA_hi_lo_freq' filesep 'PSD' filesep pars.dataType.varSelect];
figName=[roi '_' dataIDparams.patientID createFileEnd_fitECoG(pars) '-hiVsLoFreq-PSD' hyperParamsString];
if ~exist(figDir,'dir')
    mkdir(figDir)
end
savefig(gcf,[figDir filesep figName])
saveas(gcf,[figDir filesep figName],'png')

%% Compare angles between eigenvectors
% plot them 
% % %     figure
% % %     subplot(3,1,1)
% % %     imagesc(COEFF)
% % %     subplot(3,1,2)
% % %     imagesc(COEFF_lopass)
% % %     subplot(3,1,3)
% % %     imagesc(COEFF_hipass)
% compare just 1st eigenvector
% % %     figure
% % %     imagesc(horzcat(COEFF(:,1),COEFF_lopass(:,1),COEFF_hipass(:,1)))
% % %     
a=subspace(COEFF,COEFF_lopass);
b=subspace(COEFF,COEFF_hipass);
c=subspace(COEFF_hipass,COEFF_lopass);
figure
bar([a b c])
%     set(gca,'YScale','log')
% % %     keyboard
% % %     cumAngleDif_uv=[];
% % %     cumAngleDif_uw=[];
% % %     for eigenVec=1:size(COEFF,1)
% % %         u=COEFF(:,eigenVec);
% % %         v=COEFF_lopass(:,eigenVec);
% % %         w=COEFF_hipass(:,eigenVec);
% % %         
% % %         CosTheta_uv=dot(u,v)/(norm(u)*norm(v));
% % %         ThetaInDegrees_uv=acosd(CosTheta_uv);
% % % 
% % %         CosTheta_uw=dot(u,w)/(norm(u)*norm(w));
% % %         ThetaInDegrees_uw=acosd(CosTheta_uw);
% % %         
% % %         if ThetaInDegrees_uv > 180 || ThetaInDegrees_uw > 180
% % %             disp('fix')
% % %         end
% % %         if eigenVec == 1
% % %             cumAngleDif_uv(eigenVec)=ThetaInDegrees_uv;
% % %             cumAngleDif_uw(eigenVec)=ThetaInDegrees_uw;
% % %         else
% % %             cumAngleDif_uv(eigenVec)=cumAngleDif_uv(eigenVec-1)+ThetaInDegrees_uv;
% % %             cumAngleDif_uw(eigenVec)=cumAngleDif_uw(eigenVec-1)+ThetaInDegrees_uw;
% % %         end
% % %     end
% % %     cumAngleDif_uv=cumAngleDif_uv./180;
% % %     cumAngleDif_uw=cumAngleDif_uw./180;

% % %     figure
% % %     plot(cumAngleDif_uv)
% % %     hold on
% % %     plot(cumAngleDif_uw)
%     xlim([1,15])
% % %     set(gca,'YScale','log')
% % %     set(gca,'XScale','log')
% % %     title({'Cumulative Angle Difference Between EigVecs', '(Divided by 180 for normalization)'})
% % %     legend({'lowpass vs. unfiltered vectors','highpass vs. unfiltered vectors'},'Location','SouthEast')
figDir=[pars.dirs.baseResultsDir filesep 'PCA_hi_lo_freq' filesep 'cumVecAngleDif' filesep pars.dataType.varSelect];
figName=[roi '_' dataIDparams.patientID createFileEnd_fitECoG(pars) '-hiVsLoFreq-cumVecAngleDif' hyperParamsString];
if ~exist(figDir,'dir')
    mkdir(figDir)
end
savefig(gcf,[figDir filesep figName])
saveas(gcf,[figDir filesep figName],'png')

%% Calculate PCA Similarity Factor (Spca): https://www.researchgate.net/publication/221351631_A_PCA-based_similarity_measure_for_multivariate_time_series
% this just gives the identity matrix... why?
% % %     trace((((COEFF_lopass*COEFF_hipass')*COEFF_hipass))*COEFF_lopass')

%% Calculate 'eros', also foundin above paper
% compare two matrices at a time; can compare hi-vs-lo or hi-vs-norm or
% lo-vs-norm

% % %     COEFF_lopass
% % %     COEFF_hipass
% % %     LATENT_B_lopass
% % %     LATENT_B_hipass
% % %     
% % %     w=weight_vector([LATENT_B_lopass,LATENT_B_hipass])
% % %     function weight_vector(eigenVals)
% % %         for matrixInd=1:size(eigenVals,2)
% % %             
% % %         end
% % %     end

%% Compare low/high-freq projections of data with orginal PCA scores of entire signal, unfiltered
% % %     figure
% % %     subInds=5;
% % %     for subInd=1:subInds
% % %         subplot(subInds,1,subInd)
% % %         plot(SCORE_B_lopass(1:1000,subInd))
% % %         hold on
% % %         plot(SCORE_B_hipass(1:1000,subInd))
% % %         hold on
% % %         plot(SCORE(1:1000,subInd))
% % %     end
% % %     figure
% % %     scoreDifLo=sum(abs(SCORE-SCORE_B_lopass));
% % %     scoreDifHi=sum(abs(SCORE-SCORE_B_hipass));
% % %     data=[scoreDifLo;scoreDifHi];
% % %     bar(data')
%% Compare covariance matrices (low-pass and high-pass data)
corr_B=corrcoef(B);
corr_lo=corrcoef(B_lopass(:,:));
%     cov_lo2=B_lopass'*B_lopass;
corr_hi=corrcoef(B_hipass(:,:));

%     cov_B=cov(B);
%     D=sqrt(diag(cov_B));
%     DInv=inv(D);
%     R=DInv * cov_B * Dinv; /** correlation matrix **/
%     figure
%     subplot(2,1,1)
%     imagesc(cov_B-corr(A))

%     figure
%     subplot(2,1,1)
%     imagesc(cov_lo)
%     subplot(2,1,2)
%     imagesc(cov_lo2)

% % %     figure
% % %     subplot(3,1,1)
% % %     imagesc(cov_B)
% % %     subplot(3,1,2)
% % %     imagesc(cov_lo)
% % %     subplot(3,1,3)
% % %     imagesc(cov_hi)

% % %     figure
% % %     normCovB=cov_B - min(cov_B(:));
% % %     normCovB=normCovB ./ max(normCovB(:)); % *
% % %     normCovLo=cov_lo - min(cov_lo(:));
% % %     normCovLo=normCovLo ./ max(normCovLo(:)); % *
% % %     normCovHi=cov_hi - min(cov_hi(:));
% % %     normCovHi=normCovHi ./ max(normCovHi(:)); % *

%% 
% % %     figure
% % %     subplot(3,1,1)
% % %     imagesc(normCovB)
% % %     title('normCov')
% % %     colorbar
% % %     subplot(3,1,2)
% % %     imagesc(normCovLo)
% % %     title('normCovLo')
% % %     colorbar
% % %     subplot(3,1,3)
% % %     imagesc(normCovHi)
% % %     title('normCovHi')
% % %     colorbar

%% 
figure
subplot(3,1,1)
imagesc(corrcov(corr_B))
title('corr')
colorbar
subplot(3,1,2)
imagesc(corrcov(corr_lo))
title('corrLo')
colorbar
subplot(3,1,3)
imagesc(corrcov(corr_hi))
title('corrHi')
colorbar

%% plot dif. between correlation matrices
figure('units','normalized','outerposition',[0 0 1 1]);
subplot(4,1,1)
imagesc(corr_B)
title({[dataIDparams.patientID ': ' strrep(roi,'_','-')],'corr-unfiltered'})
colorbar
try
    caxis([0,1])
catch
    keyboard
end

subplot(4,1,2)
imagesc(corr_lo)
title({'corrlo'})
colorbar
try
    caxis([0,1])
catch
    keyboard
end
subplot(4,1,3)
imagesc(corr_hi)
title('corrhi')
colorbar
try
    caxis([0,1])
catch
    keyboard
end
% most of the difference between these (cov_lo-cov_hi) exists on the diagonal: 
% interpretation: variance of each channel is quite different, but interactions btw other variables is similar?
subplot(4,1,4)
data=abs(corr_hi-corr_lo);
imagesc(data)
colorbar
try
    caxis([0,1])
catch
    keyboard
end
title(['corrhi - corrlo'])

figDir=[pars.dirs.baseResultsDir filesep 'PCA_hi_lo_freq' filesep 'covComparison' filesep pars.dataType.varSelect];
figName=[roi '_' dataIDparams.patientID createFileEnd_fitECoG(pars) '-hiVsLoFreq-covComparison' hyperParamsString];
if ~exist(figDir,'dir')
    mkdir(figDir)
end
savefig(gcf,[figDir filesep figName])
saveas(gcf,[figDir filesep figName],'png')
