addpath('\\Banksdesktop\d\Box Sync\My documents\Data and analysis\ECoG-analysis\circularGraph')
close all
clear all

myColorMap = [1,0,0; 1,0,0.3; 1,0,0.6; 1,0,0.9; 0.6,0.6,0; 0,1,0; 0,0,1; 0,1,1; 0.7,0.7,0.7; 1,0,0; 1,0,0.3; 1,0,0.6; 1,0,0.9; 0.6,0.6,0; 0,1,0; 0,0,1; 0,1,1; 0.7,0.7,0.7];
N=size(myColorMap,1);
d = rand(N,1); % The diagonal values
t = triu(bsxfun(@min,d,d.').*rand(N),1); % The upper trianglar random values
M = diag(d)+t+t.';
M(M<( mean(M(:)) - std(M(:)) ))=0;
textToDisplay={'one','two','three','four','five','6','7','8','9','one','two','three','four','five','6','7','8','9'};
% maxVal=1;
% M(M>mean(M(:)))=M(M>mean(M(:)))*1.5;
% M(M<mean(M(:)))=M(M<mean(M(:)))*.5;
% M(1,1)=1.5;
% M(2,2)=0;
% M(3,3)=.2;
for ind=1:18
    M(ind,ind)=(ind+1)/10;
end
M(17,17)=0;
M(15,15)=0;
M(13,13)=0;

% figure
% imagesc(M)
% save('W:\code\Chris\gitUploads\ecog_connectivity\misc\adjMat.mat','M')
% load('W:\code\Chris\gitUploads\ecog_connectivity\misc\adjMat.mat','M')
figure
subplot(2,1,1)
imagesc(M)
M(M<mean(M(:))-std(M(:))/2)=0;
M(M<mean(M(:)))=0;
subplot(2,1,2)
imagesc(M)
figure
maxVal=max(M(:));
myCircularGraph_withSelfConn(M,...
    'Colormap',myColorMap,...
    'Label',textToDisplay,...
    'MaxVal',maxVal);

% myCircularGraph(M,...
%     'Colormap',myColorMap,...
%     'Label',textToDisplay,...
%     'MaxVal',maxVal);

