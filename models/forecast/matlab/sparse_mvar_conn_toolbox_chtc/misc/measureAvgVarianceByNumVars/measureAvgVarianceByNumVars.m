clear all
close all
%
% load('M:\PassiveEphys\Analysis Results\Chris\ecog_connectivity\PCA_cumvar_curves\cumVarAcrossROIs_allPps_ALLchs_standMVARX_PCAAC70.mat')
% load('M:\PassiveEphys\Analysis Results\Chris\ecog_connectivity\PCA_cumvar_curves\cumVarAcrossROIs_allPps_RESPchs_standMVARX_PCAAC80.mat')
% load('M:\PassiveEphys\Analysis Results\Chris\ecog_connectivity\PCA_cumvar_curves\cumVarAcrossROIs_allPps_RESPchs_standMVARX_PCAAC70_OR-RS.mat')
load('M:\PassiveEphys\Analysis Results\Chris\ecog_connectivity\PCA_cumvar_curves\cumVarAcrossROIs_allPps_ALLchs_standMVARX_PCAAC70_OR-RS.mat')


% 
pps=fields(cumVarAcrossROIs_allPps);
roiInds=containers.Map;
varianceSummary_1ch=cell(length(pps),20);
varianceSummary_2ch=cell(length(pps),20);
varianceSummary_3ch=cell(length(pps),20);
numChsPerRoi=cell(length(pps),20);
roiFields={};
for ppInd = 1:length(pps)
    pp=pps{ppInd};
    rois=fields(cumVarAcrossROIs_allPps.(pp));
    for roiInd=1:length(rois)
        roi=rois{roiInd};
        if ~isKey(roiInds,roi)
            roiInds(roi)=length(keys(roiInds))+1;
            roiFields{roiInds(roi)}=roi;
        end
        cumVar=cumVarAcrossROIs_allPps.(pp).(roi);
        numChsPerRoi{ppInd,roiInds(roi)}=length(cumVar);
        varianceSummary_1ch{ppInd,roiInds(roi)}=cumVar(1);
        if length(cumVar) > 1
            varianceSummary_2ch{ppInd,roiInds(roi)}=cumVar(2);
        else
            varianceSummary_2ch{ppInd,roiInds(roi)}=nan;
        end
        if length(cumVar) > 2
            varianceSummary_3ch{ppInd,roiInds(roi)}=cumVar(3);
        else
            varianceSummary_3ch{ppInd,roiInds(roi)}=nan;
        end
    end
end
%% Remove underlines from roi fields
% eraseStr='_';
% wrapper = @(x) erase( x, eraseStr) ;
old='_';
new='-';
wrapper = @(x) strrep( x, old, new) ;
roiFields_xtickLabels = cellfun( wrapper, roiFields ,'UniformOutput', false ) ;

%% replace empty cells w/ zeros
numChsPerRoi((cellfun(@isempty,numChsPerRoi)))={0};
varianceSummary_1ch((cellfun(@isempty,varianceSummary_1ch)))={0};
varianceSummary_2ch((cellfun(@isempty,varianceSummary_2ch)))={0};
varianceSummary_3ch((cellfun(@isempty,varianceSummary_3ch)))={0};

%% get rid of extra columns
varianceSummary_1ch=varianceSummary_1ch(:,1:length(keys(roiInds)));
varianceSummary_2ch=varianceSummary_2ch(:,1:length(keys(roiInds)));
varianceSummary_3ch=varianceSummary_3ch(:,1:length(keys(roiInds)));
numChsPerRoi=numChsPerRoi(:,1:length(keys(roiInds)));

varianceAcrossROIsFig_sorted=figure;
varianceAcrossROIsFig=figure;
endChCt=3;
numChsPerRoi=cell2mat(numChsPerRoi);
for chCt=1:endChCt
    if chCt==1
        varianceSummary=varianceSummary_1ch;
    elseif chCt == 2
        varianceSummary=varianceSummary_2ch;
    else
        varianceSummary=varianceSummary_3ch;
    end
    %% weight perc. variance accounted for by small number of channels (1-3) by percentVars remaining
    weightStuff=true;
    if weightStuff
        varianceSummary=(cell2mat(varianceSummary).*(1-(chCt./numChsPerRoi)));
    else
        varianceSummary=cell2mat(varianceSummary);
    end
    yMin=0;
    yMax=.8;
    
    %% replace 0's (whereby num chs accounting for variance is equal to total
    % chs in ROI) with infinities; NaN symbolizes fact that no channels in ROI
    % for that specific Pp
    % Follow-up: Just kidding. In order to calculate a mean score I have to
    % convert Infs to nans
    varianceSummary(varianceSummary==0) = nan;%inf; 
    
    %% sort data structure by how much variance accounted for with few chans
    avgVarianceAcrossROIs=nanmean(varianceSummary);
    [avgVarianceAcrossROIs,indexOrder]=sort(avgVarianceAcrossROIs);
    varianceSummary_sorted=varianceSummary(:,indexOrder);
    numVars_sorted=numChsPerRoi(:,indexOrder);
    numVars_sorted(numVars_sorted==0)=nan;
    avgNumVars_sorted=nanmean(numVars_sorted);
    
    %% plot data
    figure(varianceAcrossROIsFig_sorted)
    subplot(3,1,chCt)
    for ppInd=1:length(pps)
        hold on
        scatter(1:length(roiInds),varianceSummary_sorted(ppInd,:))
    end
    xlim([0,length(roiInds)+1])
    xticks(1:length(roiInds))
    xticklabels(roiFields_xtickLabels(indexOrder))
    ylim([yMin,yMax])
    if chCt == 1
        title([num2str(chCt) ' PC'])
    else
        title([num2str(chCt) ' PCs'])
    end
    if chCt == 3
        suptitle({'ROIs sorted by data redundancy: percVarExplained*[1-(numPCsKept/totalChsInROI)]'})
    end
    figure(varianceAcrossROIsFig)
    subplot(3,1,chCt)
    for ppInd=1:length(pps)
        hold on
        scatter(1:length(roiInds),varianceSummary(ppInd,:))
    end
    xticks(1:length(roiInds))
    xticklabels(roiFields_xtickLabels)
    ylim([yMin,yMax])
    
    %% convert back to cell matrix
    varianceSummary=num2cell(varianceSummary);

    %% convert to structs
    varianceSummary=cell2struct(varianceSummary,roiFields,2);
    if chCt==1
        varianceSummary_1ch=varianceSummary;
    elseif chCt == 2
        varianceSummary_2ch=varianceSummary;
    else
        varianceSummary_3ch=varianceSummary;
    end
end
% numChsPerRoi=cell2struct(numChsPerRoi,roiFields,2);



