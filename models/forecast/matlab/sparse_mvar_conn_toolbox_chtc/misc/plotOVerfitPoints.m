function maxModelOrder=plotOVerfitPoints(pars,minModelParsToSamplesRatio,trainSet)

% % % nTrainEpochs=length(trainSet);
samps=[250*5*60,250*1*60,250*.5*50];
maxMOs=[];
sampPerEpochInd=1;
for sampPerEpoch=samps
% sampPerEpoch=250*60;%size(trainSet{1},2);
    nTrainEpochs=1;
    nChs=[1:10,15:5:30,40:10:200];
    
    for lassoBool=[false,true]
        varCtInd=1;
        for nCh=nChs
            varCtInd+varCtInd+1;
        % % %     nCh=size(trainSet{1},1);
            if lassoBool%%%pars.cv.p.dimRed.ch.lasso.bool
                nModelParamsPerLag=round(min(pars.cv.p.dimRed.ch.lasso.lambdaWeightRange)*(nCh^2));
            else
                nModelParamsPerLag=nCh^2;
            end
            maxModelOrder=round(minModelParsToSamplesRatio*...
                nTrainEpochs*sampPerEpoch/(nModelParamsPerLag + minModelParsToSamplesRatio*nTrainEpochs));
            maxMOs(lassoBool+1,sampPerEpochInd,varCtInd)=maxModelOrder;
            varCtInd=varCtInd+1;
        end
    end
    sampPerEpochInd=sampPerEpochInd+1;
end
keyboard
%%
figure
for subplotInd=1:3
    subtightplot(1,3,subplotInd)
    plot(nChs,squeeze(maxMOs(1,subplotInd,:)),'LineWidth',2)
    set(gca,'LineWidth',1.3);
    hold on 
    plot(nChs,squeeze(maxMOs(2,subplotInd,:)),'LineWidth',2)
    set(gca,'LineWidth',1.3);
    ylim([0,100])
    xlim([0,200])
    if subplotInd~=1
        set(gca,'xticklabel',{[]})
        set(gca,'yticklabel',{[]})
    end
    if subplotInd==1
        title('5min of data, 250Hz')
%         ylabel('maxModelOrder')
        ylabel('maxModelOrder','fontweight','bold','fontsize',16)
    elseif subplotInd==2
        title('1min of data, 250Hz')
    else
        title('30 sec of data, 250Hz')
    end
    set(gca,'YScale','log')
    
    axesH = gca;
    axesH.XAxis.TickLabelInterpreter = 'latex';
    axesH.XAxis.TickLabelFormat      = '\\textbf{%g}';
    axesH.YAxis.TickLabelInterpreter = 'latex';
    axesH.YAxis.TickLabelFormat      = '\\textbf{%g}';
end
suptitle('nVars (nChs) vs. maxModelOrder')
%% y-axis=maxModelOrder
%% x-axis=variableCount 


