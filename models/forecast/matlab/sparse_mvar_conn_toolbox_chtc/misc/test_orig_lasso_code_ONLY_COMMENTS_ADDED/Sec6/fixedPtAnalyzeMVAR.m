function [isStable crossCor err]=fixedPtAnalyzeMVAR(A,delay,varargin)
% [isStable crossCor err]=fixedPtAnalyzeMVAR(A,delay,varargin)
%
% This function is similar to AnalyzeMVAR, but uses fixed point iteration
% to estimate correlation matrices rather than calculating them explicitly.
% This is necessary for very large systems.  The output err stores the
% Frobenius norm of the change in cor. matrices between iterations.
%
% Calculates cross correlation matrices and checks stability of a
% multivariate autoregressive system described by A = [A_1 A_2 ... A_p].
% The system is described by x(t) = sum_{i=1}^p A_i x(t-i) + u.  Matrix A
% is N by N*p where N is the number of nodes in the system and p is the
% order of the MVAR model.  crossCor is an N by N correlation matrix equal
% to E[x(t) x(t-delay)].  Setting delay = NaN will produce an N*p by N*p
% correlation matrix: E[ [x(t); ... x(t-p+1)] [x(t)' ... x(t-p+1)'] ].
%
% An optional third input specifies the driving noise covariance
% E[u(t) u(t)'].  Default is E[u(t) u(t)'] = I.
%
% Written by: Andrew Bolstad

[nNodes np]=size(A);

modelOrder=np/nNodes;
if floor(modelOrder)~=modelOrder
    % Bad input
    disp('Bad system matrix.')
    return
end

bigA=[A;eye((modelOrder-1)*nNodes),zeros((modelOrder-1)*nNodes,nNodes)];

if issparse(bigA)
    isStable=abs(eigs(bigA,1))<1;
else
    isStable=(max(abs(eig(bigA)))<1); % < or leq?
end

if nargin>3
    Ru=varargin{2};
else
    Ru=speye(nNodes);
end
bigRu=zeros(np,np);
bigRu(1:nNodes,1:nNodes)=Ru;

% fixed pt section
maxIts=1000;
its=0;
temp=1;
crossCor=zeros(np);
nextCC=zeros(np);
err=zeros(maxIts,1);
bAt=bigA';
if ~isStable
    its=maxIts;
end
while its<maxIts && temp>100*eps
    nextCC=bigA*crossCor*bAt+bigRu;
    temp=norm(nextCC-crossCor,'fro');
    err(its+1)=temp; %norm(nextCC-crossCor,'fro');
    crossCor=nextCC;
    its=its+1;
    if ~mod(its,500)
        sprintf('Fixed Point Iteration %d, Error %0.5g\n',its,err(its))
    end
end
%figure; plot(err)


% larger time offsets
% delay=NaN;
d=abs(delay);
if isnan(d)
    % Dummy branch
elseif d<modelOrder-1
    crossCor=crossCor(1:nNodes,d*nNodes+1:(d+1)*nNodes);
else
    % Calc. cross cor
    % Maybe not the best way:
    %crossCor=crossCor(1:nNodes,np-nNodes+1:np);
    for c=1:d-modelOrder+1
        %crossCor=A(:,1:nNodes)*crossCor*A(:,1:nNodes)';
        crossCor=[A*crossCor;crossCor(nNodes+1:np,:)];
    end
    crossCor=crossCor(1:nNodes,np-nNodes+1:np);
end
if delay<0
    crossCor=crossCor';
end