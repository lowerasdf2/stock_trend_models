function [ Zn ] = normData( Z )
%normData  Normalize the data so that E[x_i(t)]=1
%   [ Zn ] = normData( Z )
%
% Written by: Andrew Bolstad

% assume channels by time
[nCh T]=size(Z);
wts=T./diag(Z*Z');

Zn=diag(sqrt(wts))*Z;

end

