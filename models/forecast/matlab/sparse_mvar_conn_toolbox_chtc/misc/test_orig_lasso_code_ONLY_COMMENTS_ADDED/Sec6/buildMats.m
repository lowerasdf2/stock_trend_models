function [Y H]=buildMats(Data,p)
% [Y H]=buildMats(Data,p)
%
% Converts data matrix Z to a problem of the form Y = H * a.
%
% Input:    Data            nNodes by t time series matix
%           p               Order of model
%
% Output:   Y               t-p by nNodes matrix whose columns are given by
%                           [x_i(p+1) x_i(p+2) ... ]'
%           H               t-p by nNodes*p matrix whose columns form
%                           t-p by p blocks given by:
%                               [ x_i(p)   x_i(p-1) ... x_i(1);
%                                 x_i(p+1) x_i(p)   ... x_i(2);
%                                   ...     ...     ...
%                                 x_i(t-1) x_i(t-2) ... x_i(t-p+1) ]
%
% Written by: Andrew Bolstad

[m T]=size(Data);

H=zeros(T-p,p*m);
Y=Data(:,p+1:T)';
for ii = 1:p
    H(:,(p-ii)*m+1:(p-ii+1)*m)=Data(:,ii:T-p-1+ii)';
end

% 'sort' matrices
temp=reshape(reshape(1:m*p,m,p)',m*p,1);
H=H(:,temp);
