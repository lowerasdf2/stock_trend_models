% genTable1.m

addpath ..\SimNets

fnames{1}='Circle_RepScores';
fnames{2}='Haufe_Ex1';
% fnames{3}='ParallelNets';
fnames{3}='Parallel';
fnames{4}='Winterhalder_RepScores';
% fnames{5}='';

numNetworks=length(fnames);

nameLen=0;
for c=1:numNetworks
    if length(fnames{c})>nameLen
        nameLen=length(fnames{c});
    end
end
    

for c=1:numNetworks
    % load the network
    load(fnames{c},'A');
    numCh=size(A,1);
    
    % calculate scores with and without: (a) self connections (b)
    % normalization
    sA=checkConditions(A);
    orig_psi_max=max(sA(:));
    
    sA=checkConditions(A,[],1);
    orig_tildepsi_max=max(sA(:));
    
    % normalize
    [CCnrm Rnrm Anrm D]=normChn(A);
    
    sA=checkConditions(Anrm,D(1:numCh,1:numCh)*D(1:numCh,1:numCh));
    nrm_psi_max=max(sA(:));
    
    sA=checkConditions(Anrm,D(1:numCh,1:numCh)*D(1:numCh,1:numCh),1);
    nrm_tildepsi_max=max(sA(:));
    
    % report results
    spaces=repmat(' ',1,nameLen-length(fnames{c}));
    fprintf(1,['%s' spaces '\t%5.4f\t%5.4f\t%5.4f\t%5.4f\t\n'],...
        fnames{c},orig_psi_max,orig_tildepsi_max,...
        nrm_psi_max,nrm_tildepsi_max)
end