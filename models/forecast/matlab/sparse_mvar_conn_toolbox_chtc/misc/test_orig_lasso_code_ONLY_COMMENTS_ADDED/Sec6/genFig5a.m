% genFig5a.m

% add paths to necessary files
addpath ..\Sec5
addpath ..\SimNets
addpath ..\stsincsolver2

% set parameters
T=150;
p=4;
cvFactor=10;
nTrials=30;

% free self connections?
SC=0;

% circle network -- figure 4
load Parallel A

fprintf(1,'\nThis may take a few minutes.  Type Ctrl-C to stop.\n\n');

% run monte carlo
cntMat=zeros(size(A,1));
for c=1:nTrials
    Z=genData(A,T);
    [Y H]=buildMats(Z,p);
    Aest=lmCVFit(Y,H,p,cvFactor,[],SC);
    % the 1 is a dummy to specify formatting.  transpose required.
    P=powermap(Aest,1)';
    
    cntMat=cntMat+(P~=0);
end
cntMat4a=cntMat/nTrials;

% display results
nNodes=length(cntMat);

% find true mapping
srt=reshape(reshape(1:nNodes*p,nNodes,p)',nNodes*p,1);
Aalt=A(:,srt)';
trueMap=zeros(nNodes);
for c=1:nNodes
    drv=unique(ceil(find(Aalt(:,c))/p));
    trueMap(drv,c)=1;
end

% find most common fp
tmp=cntMat4a.*~trueMap;
[a b]=find(tmp==max(tmp(:)));

fprintf(1,'\nConnection percentages from %d Monte Carlo trials using %d time samples.\n',nTrials,T);
fprintf(1,'From \\ To\t\t');
for c=1:nNodes
    fprintf(1,'Node %d\t\t',c);
end
fprintf(1,'\n');
for c=1:nNodes
    fprintf(1,'Node %d   \t\t',c);
    for d=1:nNodes
        if trueMap(c,d)
            % true connection
            mrk='*';
        elseif ismember(c,a) && ismember(d,b)
            mrk='#';
        else
            mrk=' ';
        end
        fprintf(1,'%6.2f%s\t\t',100*cntMat4a(c,d),mrk);
    end
    fprintf(1,'\n');
end
fprintf(1,'\n* indicates true connection\n# indicates most common false positive\n\n');