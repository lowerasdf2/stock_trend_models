function [CCnrm Rnrm Anrm D]=normChn(A,varargin)
% [CCnrm Rnrm Anrm Diroot]=normChn(A,[crossCor])
%
% Normalizes MVAR system x(t) = sum_{i=1}^p A_i x(t-i) + u where A = [A_1
% A_2 ... A_p] and x(t) is an N vector of measurements from N nodes at
% time t.  The normalized system has E[x_i(t)^2] = 1 for all i and is
% described by Anrm.
%
% CCnrm is E[[x(t); x(t-1); ... x(t-p+1)] [x(t); x(t-1); ... x(t-p+1)]']
%
% Rnrm is CCnrm rearranged so that each block is the cross correlation
% between two nodes at delays 0,1,...,p-1:
%    (i,j)^th block of Rnrm is E[x_i(t:-1:t-p+1) x_j(t:-1:t-p+1)']
%
% Diroot is a diagnal of the inverse square root of the power in each
% channel.
%
% The optional input crossCor is the cross corellation of the original
% system.  If it is known, some computation is not needed.
%
% Written by: Andrew Bolstad

[numCh np]=size(A);
modelOrder=np/numCh;

if nargin==1
%     [isStable crossCor]=AnalyzeMVAR(A,NaN);
    [isStable crossCor]=fixedPtAnalyzeMVAR(A,NaN);
else
    crossCor=varargin{1};
end
srt=reshape(reshape(1:numCh*modelOrder,numCh,modelOrder)',numCh*modelOrder,1);

pows=diag(crossCor(1:numCh,1:numCh));
D=kron(speye(modelOrder),diag(1./sqrt(pows)));

CCnrm=D*crossCor*D;
Rnrm=CCnrm(srt,srt);
Anrm=D(1:numCh,1:numCh)*A/D;
%R=crossCor(srt,srt);