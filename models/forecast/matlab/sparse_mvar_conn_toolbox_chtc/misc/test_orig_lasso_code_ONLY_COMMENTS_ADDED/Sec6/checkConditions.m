function [sA sMx sMn]=checkConditions(A,varargin)
%
% [sA sMx sMn]=checkConditions(A,[noiseCov],[noSelfCon])
%
% Calculates the "false connection score" of a sparse MVAR network.  Matrix
% sA holds the scores for each connection; i.e. the sA(i,j) is the false
% connection score for the (nonexistant) link from node i to node j.
% sA(i,j) = 0 if there is a true connection (or if the false alarm score is
% zero).
%
% A should be nNodes by nNodes*p, A=[A1 A2 ... Ap].
% Optional noiseCov sets the noise covariance (default: I).  This should be
%     used when the data are normalized (bec. noise is no longer white).
% noSelfCon = 1 calculates the false connection score if self connections
%     are not penalized.
%
% Outputs sMx and sMn are upper and lower bounds (entrywise) of sA based on
% singular values.
%
% Written by: Andrew Bolstad

noSelfCon=0;
if nargin>2
    noSelfCon=varargin{2};
end

[nNodes np]=size(A);

p=np/nNodes;
if floor(p)~=p
    % Bad input
    disp('Bad system matrix.')
    return
end

sA=zeros(nNodes);
sMx=sA; sMn=sA;

Ru=speye(nNodes);
if nargin>1
    if ~isempty(varargin{1})
        Ru=varargin{1};
    end
end

%[isStable crossCor]=AnalyzeMVAR(A,NaN,Ru);
[isStable crossCor]=fixedPtAnalyzeMVAR(A,NaN,Ru);

srt=reshape(reshape(1:nNodes*p,nNodes,p)',nNodes*p,1);
R=crossCor(srt,srt);
%figure; imagesc(R)
Aalt=A(:,srt)';

% Loop through each channel
for c=1:nNodes
    drvind=find(Aalt(:,c));
    drv=unique(ceil(drvind/p));
    ndrv=setdiff(1:nNodes,drv);
    % drvind correction (in case group contains zeros)
    drvind=(repmat(p*drv(:),1,p)-ones(numel(drv),1)*(p-1:-1:0))';
    drvind=drvind(:);
    Raa=R(drvind,drvind);
    
    Rac=R(drvind,setdiff(1:np,drvind));
    
%     Rac=[];
%     for d=setdiff(1:nNodes,drv)
%         Rac=[Rac,R(drvind,(d-1)*p+1:d*p)];
%     end
    %size(Raa)
    %figure; imagesc(Rac)
    Theta_c=-Raa\Rac;
    %figure; imagesc(Theta_c)
    
    % Option for removing self connection part
    if noSelfCon
        selfind=(c-1)*p+1:c*p;
        tmp=find(max(selfind)==drvind);
        Theta_c(tmp-p+1:tmp,:)=0;
    end
    
    acurc=Aalt(drvind,c);
    for d=1:length(drv) % loop through drivers
        acurn=acurc((d-1)*p+1:d*p)/norm(acurc((d-1)*p+1:d*p));
        %size(acur), size(Theta_c)
        
        vc=Theta_c((d-1)*p+1:d*p,:)'*acurn;
        %figure; plot(vc)
        vc=reshape(vc,p,length(vc)/p);
        
        for ii=1:length(ndrv)
            %norm(vc(:,ii))
            sA(ndrv(ii),c)=sA(ndrv(ii),c)+norm(vc(:,ii));
            csvds=svd(full(Theta_c((d-1)*p+1:d*p,(ii-1)*p+1:ii*p)));
            sMx(ndrv(ii),c)=sMx(ndrv(ii),c)+max(csvds);
            sMn(ndrv(ii),c)=sMn(ndrv(ii),c)+min(csvds);
        end
        
    end
end