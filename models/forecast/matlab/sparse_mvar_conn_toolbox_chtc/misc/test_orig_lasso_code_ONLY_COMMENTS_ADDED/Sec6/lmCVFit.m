function [A chErr i]=lmCVFit(Y,H,modelOrder,nDiv,varargin)
%
% Aest=lmCVFit(Y,H,modelOrder,nDiv,[chList],[SC])
%
% Solves Aest = argmin norm(Y-H*Aest,'fro')^2 + lambda * gL(Aest)
% using cross-validation to select lambda.  gL is a group Lasso penalty.
%
% Input:    Y, H        Matrices created by buildMats from time series
%                       data.
%           modelOrder  Model order (memory) of multivariate autoregressive
%                       system.
%           nDiv        Number of held out groups of data (i.e. 10 for
%                       tenfold CV).
%           chList      Specifies which channels to use (default 1:nNodes)
%
% Uses solver stsincsolver2.m
%
% Written by: Andrew Bolstad

% created from BlockCVFit on 2/24/2011 for tweaking

% addpath 'C:\Documents and Settings\Andrew\My Documents\SparseNet\Simulations\Free Parameter EM\'
%addpath 'C:\Documents and Settings\Andrew\My Documents\Free Parameter EM'

[T fCh]=size(Y);
[T2 nChp]=size(H);

if T~=T2
    error('Incompatable data sizes.')
end
clear T2
nCh=nChp/modelOrder;
if round(nCh)~=nCh
    error('Size of H does not agree with modelOrder.')
end

% set some params
options.verbose=0;
options.penalty=2;
options.useNbpen=0;
options.startL=1; options.endL=.45;
options.inc=0.05;
np=round((options.startL-options.endL)/options.inc+1);

if fCh~=nCh
    % don't do all channels
    chList=varargin{1};
    %mCh=setdiff(1:nCh,chList);
    Y2=zeros(T,nCh);
    Y2(:,chList)=Y;
    Yt=Y2;
    clear Y2
%     M=1.1*calcLmax2(Y2,H,modelOrder); % boost for trial var.
%     M=1.1*stsmaxlambda(H,G,Y2,KH,KG,options);
else
    chList=1:nCh;
    Yt=Y;
%     M=1.1*calcLmax2(Y,H,modelOrder); % boost for trial var.
%     M=1.1*stsmaxlambda(H,G,Y,KH,KG,options);
end

useFreeParamMap=0;
if nargin>5
    if varargin{2}
        useFreeParamMap=1;
    end
end

% add freeParamMap part (FIX!!!!)
KH=modelOrder*ones(nCh,1);
KG=ones(size(Y,2),1);
M=zeros(nCh,1);
if useFreeParamMap
    X=zeros(size(H,2),nCh);
    for c=1:nCh
        % ls solution with no reg.
        freeParameterMap=zeros(nCh,1);
        freeParameterMap(c)=1;
        X(:,c)=LargeLS2(Yt(:,c),H,1,KH,1,freeParameterMap,inf);
    end
    R=Y-H*X;
    M=stsmaxlambda(H,size(Y,2),R,KH,KG,options);
    options.X=X;
else
    R=Y;
end
for c=1:nCh
    M(c)=1.1*stsmaxlambda(H,1,R(:,c),KH,1,options);
end

% testing
% A=M;
% return

maxSsz=ceil(T/nDiv); %ceil((T-pMax)/10);
S=zeros(nDiv,maxSsz);
for c=1:nDiv
    cset=c:nDiv:T;
    S(c,1:length(cset))=cset;
end

debias=1;

% Store results
errors=zeros(fCh,np);

for tSet=1:nDiv
    % H8 -- old name
    inind=setdiff(1:nDiv,tSet);
    ind=setdiff(reshape(S(inind,:),(nDiv-1)*maxSsz,1),0);
    H8=zeros(length(ind),modelOrder*nCh);
    for c=1:nCh
        H8(:,(c-1)*modelOrder+1:c*modelOrder)=...
            H(ind,(c-1)*modelOrder+1:(c-1)*modelOrder+modelOrder);
    end
    Y8=Y(ind,:);
    
    % H1 -- 1 set -- score current
    ind=setdiff(S(tSet,:),0);
    H1=zeros(length(ind),modelOrder*nCh);
    for c=1:nCh
        H1(:,(c-1)*modelOrder+1:c*modelOrder)=...
            H(ind,(c-1)*modelOrder+1:(c-1)*modelOrder+modelOrder);
    end
    Y1=Y(ind,:);
    
    % Calculate sparse answer on H8 -------------------------------
    
    % Solve sparse problem for each column
    sparseMap=sparse(modelOrder*nCh,np*fCh);
    tempMap=sparse(modelOrder*nCh,np);
    atmp=sparse(modelOrder*nCh,1);
    KH=modelOrder*ones(nCh,1);
    for c=chList    % 1:fCh
        options.lambdaMax=M(c);
        ind=(c-1)*modelOrder+1:c*modelOrder;
        atmp(ind)=(H8(:,ind)'*H8(:,ind))\(H8(:,ind)'*Y8(:,c==chList));
        
        % testing        
%         figure; plot(atmp)
%         A=0;
%         return
        
        options.X=atmp;
        if useFreeParamMap
            % assume useFreeParams
            options.freeParameterMap=zeros(nCh,1);
            options.freeParameterMap(c)=1;
        end
        
        % testing
%         A=options.freeParameterMap;
%         figure; plot(Y8(:,c==chList))
%         return
        
        % check this for not all channels case
        %size(Y8)
        tempMap=stsincsolver2(Y8(:,c==chList),H8,1,KH,1,options);
        sparseMap(:,c:fCh:((np-1)*fCh+c))=tempMap;
        
    end
    
    % use sparseMap to calc error on validation data (H1,Y1)
    for c=1:np
        curA=sparseMap(:,(c-1)*fCh+1:c*fCh);
        err=zeros(size(Y1));
        % Assume debiasing
        for d=1:fCh
            curA(:,d)=LargeLS2(Y8(:,d),H8,1,KH,1,curA(:,d));
        end
        err=Y1-H1*curA;
        errors(:,c)=errors(:,c)+diag(err'*err);
    end
    % End sparse section
end

[chErr i]=min(errors,[],2);
%errors, t, i

% estimate on whole data set -- new part
sparseMap=sparse(modelOrder*nCh,np*fCh);
tempMap=sparse(modelOrder*nCh,np);
atmp=sparse(modelOrder*nCh,1);
KH=modelOrder*ones(nCh,1);
for c=chList
    options.lambdaMax=M(c);
    ind=(c-1)*modelOrder+1:c*modelOrder;
    atmp(ind)=(H(:,ind)'*H(:,ind))\(H(:,ind)'*Y(:,c==chList));
    
    options.X=atmp;
    
    if useFreeParamMap
        % assume useFreeParams
        options.freeParameterMap=zeros(nCh,1);
        options.freeParameterMap(c)=1;
    end
    
    tempMap=stsincsolver2(Y(:,c==chList),H,1,KH,1,options);
    sparseMap(:,c:fCh:((np-1)*fCh+c))=tempMap;
end


A=sparse(nChp,fCh);
for d=1:fCh
    % (i(d)-1)*fCh+1:i(d)*fCh
    curA=sparseMap(:,chList(d)+(i(d)-1)*fCh);
    A(:,d)=LargeLS2(Y(:,d),H,1,KH,1,curA);
end