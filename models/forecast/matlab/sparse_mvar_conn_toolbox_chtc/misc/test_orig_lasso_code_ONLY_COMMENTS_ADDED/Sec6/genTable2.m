% genTable2.m

addpath ..\SimNets
fname='Parallel';
connections=[1 2; 2 1; 1 3; 1 4; 1 5; 1 6; 3 2; 4 2; 5 2; 6 2];

load(fname,'A')

numCh=size(A,1);

% calculate scores with and without: (a) self connections (b)
% normalization
sA=checkConditions(A);
sAt=checkConditions(A,[],1);

% normalize
[CCnrm Rnrm Anrm D]=normChn(A);

sAn=checkConditions(Anrm,D(1:numCh,1:numCh)*D(1:numCh,1:numCh));
sAnt=checkConditions(Anrm,D(1:numCh,1:numCh)*D(1:numCh,1:numCh),1);

% display
conns=numCh*(connections(:,2)-1)+connections(:,1);
scores=[sA(conns) sAt(conns) sAn(conns) sAnt(conns)];
for c=1:length(conns)
    fprintf(1,'%d to %d:\t%5.4f\t%5.4f\t%5.4f\t%5.4f\n',connections(c,:),...
        scores(c,:));
end