function [chanSide, chanROI, chanNum, sortOrder] = getChannelInfo(params)

for iChan = 1:length(params.ECoGchannels)
    chanSide{iChan} = params.ECoGchannels(iChan).side;
    chanROI{iChan} = params.ECoGchannels(iChan).newROI;
    chanNum(iChan) = params.ECoGchannels(iChan).chanNum;
    if isfield(params.ECoGchannels(iChan),'sortOrder')
        sortOrder(iChan) = params.ECoGchannels(iChan).sortOrder;
    else
        sortOrder(iChan) = NaN;
    end
end
