function [batchParams,setName] = getBatchParams_sleep(electrodeFilePath)%,defaultPath)
batchParams = struct;
% computer = 'desktopPC';
computer = 'Mac';
if strcmp(computer,'Mac')
    computerSpecPrefix = '/Users/bankslaptop/Box Sync/PHI_Data-A5309-Banks/Banks-UIowa-LDS/My documents/';
else
    computerSpecPrefix = 'D:\Box Sync\PHI_Data-A5309-Banks\Banks-UIowa-LDS\My documents\';
end

if ~exist('electrodeFilePath','var')
    electrodeFilePath = [computerSpecPrefix 'Data and analysis' filesep ...
        'ECoG data' filesep 'Electrode MNI coordinates and ROI' filesep];
end


trialLength = 2; %sec
trialOverlap = 0.25; %fractional overlap
% setName = 'origSet';
setName = 'hierarchySet'; %Kirill's Aug 2017 area segmentation

%% Patients 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%369LR
electrodeFile = 'LGD electrodes for analysis - sorted by ROI.xlsx';
electrodeSheet = 'R369_CRU_Sleep';
patientID = '369LR';
patientID = ['patient' patientID];
% dataPath = [defaultPath patientID filesep 'Sleep data' filesep];
recDate = 'yymdd';
analysisSeg = [384,622]/2;

ECoGchan = ECoGChannelMap_tableRead([electrodeFilePath electrodeFile],electrodeSheet);
% exempChans.nums = [48,105,50,205,171,168,148,238,251]; 
% exempChans.ROIs = {'HGPM','HGAL','PT','STGP','STGM','MTG','SMG','AG','IFGop','MFG'};
% origSet:
% exempChans.nums = [47,105,50,188,221,171,168,238,251]; 
% exempChans.ROIs = {'R_HGPM','R_HGAL','R_PT','R_STGP','R_STGM','R_MTG','R_SMG','R_IFGop','R_MFG'};
% localSet:
% exempChans.nums = [46,48,99,101,103,105,107,50,52]; 
% exempChans.ROIs = {'R_HGPM','R_HGPM','R_HGPM','R_HGPM','R_HGAL','R_HGAL','R_HGAL','R_PT','R_PT'};
% % newSet (via bivarGranger):
% exempChans.nums = [46,106,92,221,85,79,82,144,223,89,45,112]; 
% exempChans.ROIs = {'R_HGPM','R_HGAL','R_PP','R_STGM','R_STGA','R_MTG',...
%     'R_TP','R_AngG','R_PreG','R_lOFC','R_Ins','R_Amyg'};
% exempChans.setName = setName;
% newSet (via bivarGranger) trimmed to 10:
% exempChans.nums = [46,106,92,221,85,79,82,89,45,112]; 
% exempChans.ROIs = {'R_HGPM','R_HGAL','R_PP','R_STGM','R_STGA','R_MTG',...
%     'R_TP','R_lOFC','R_Ins','R_Amyg'};
exempChans.setName = setName;
% % Seven ROI set:
% exempChans.nums = [48,105,50,92,181,171,238];
% exempChans.ROIs = {'R_HGPM','R_HGAL','R_PT','R_PP','R_STG','R_Aud-rel','R_PFC'};
% hierarchySet:
exempChans.nums = [48,105,50,92,181,212,171,174,238];
exempChans.ROIs = {'R_HGPM','R_HGAL','R_PT','R_PP','R_STGP','R_STGM','R_MTG','R_SMG','R_PFC'};

batchParams.(patientID) = struct( ...
    'recDate', recDate,'ECoGchannels', ECoGchan, ...%'dataPath', dataPath,...
    'trialLength',trialLength, 'trialOverlap',trialOverlap);   
batchParams.(patientID).exempChans = exempChans;

%%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %372L
electrodeFile = 'LGD electrodes for analysis - sorted by ROI.xlsx';
electrodeSheet = 'L372_CRU_Sleep';
patientID = '372L';
patientID = ['patient' patientID];
% dataPath = [defaultPath patientID filesep 'Sleep data' filesep];
recDate = 'yymdd';
% analysisSeg = [578,808]/2; %minutes into recording
analysisSeg = [568,1037]/2; %minutes into recording

ECoGchan = ECoGChannelMap_tableRead([electrodeFilePath electrodeFile],electrodeSheet);
% origSet:
% exempChans.nums = [79,101,82,172,133,185,191,253,234];
% exempChans.ROIs = {'L_HGPM','L_HGAL','L_PT','L_STGP','L_STGM','L_MTG','L_SMG','L_IFGop','L_MFG'};
% % newSet (via bivarGranger):
% exempChans.nums = [80,101,82,74,148,134,120,145,151,71,125,123];
% exempChans.ROIs = {'L_HGPM','L_HGAL','L_PT','L_PP','L_STGP','L_STGM',...
%     'L_STSU','L_MTG','L_PostG','L_Ins','L_Amyg','L_Hipp'};
% exempChans.setName = setName;
% newSet (via bivarGranger) trimmed to 10:
% exempChans.nums = [80,101,82,74,148,134,120,71,125,123];
% exempChans.ROIs = {'L_HGPM','L_HGAL','L_PT','L_PP','L_STGP','L_STGM',...
%     'L_STSU','L_Ins','L_Amyg','L_Hipp'};
exempChans.setName = setName;
% % sevenROISet:
% exempChans.nums = [80,100,81,73,165,170,245];
% exempChans.ROIs = {'L_HGPM','L_HGAL','L_PT','L_PP','L_STG','L_Aud-rel','L_PFC'};
% hierarchySet:
exempChans.nums = [80,100,81,73,165,134,170,174,245];
exempChans.ROIs = {'L_HGPM','L_HGAL','L_PT','L_PP','L_STGP','L_STGM','L_MTG','L_SMG','L_PFC'};

batchParams.(patientID) = struct( ...
    'recDate', recDate,'ECoGchannels', ECoGchan, ...%'dataPath', dataPath,...
    'trialLength',trialLength, 'trialOverlap',trialOverlap, 'analysisSeg', analysisSeg);   
batchParams.(patientID).exempChans = exempChans;
% 
%%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%376R
% batchParams = struct;
electrodeFile = 'LGD electrodes for analysis - sorted by ROI.xlsx';
electrodeSheet = 'R376_CRU_Sleep';
patientID = '376R';
patientID = ['patient' patientID];
% dataPath = [defaultPath patientID filesep 'Sleep data' filesep];
recDate = 'yymdd';
% analysisSeg = [135,660]/2; %minutes into recording
analysisSeg = [135,1440]/2; %minutes into recording
ECoGchan = ECoGChannelMap_tableRead([electrodeFilePath electrodeFile],electrodeSheet);
% origSet
% exempChans.nums = [78,82,49,221,253,203,190,133,147];
% exempChans.ROIs = {'R_HGPM','R_HGAL','R_PT','R_STGP','R_STGM','R_MTG','R_SMG','R_IFGop','R_MFG'};
% % newSet (via bivarGranger):
% exempChans.nums = [46,82,74,214,247,254,55,107,119,255,54,71];
% exempChans.ROIs = {'R_HGPM','R_HGAL','R_PP','R_STGP','R_STGP','R_STGM',...
%     'R_STSL','R_MTG','R_IFGop','R_PostG','R_Ins','R_Ins'};
% exempChans.setName = setName;
% newSet (via bivarGranger) trimmed to 10:
% exempChans.nums = [46,82,74,247,254,55,107,119,54,71];
% exempChans.ROIs = {'R_HGPM','R_HGAL','R_PP','R_STGP','R_STGM',...
%     'R_STSL','R_MTG','R_IFGop','R_Ins','R_Ins'};
exempChans.setName = setName;
% % sevenROISet:
% exempChans.nums = [47,81,48,74,246,224,132];
% exempChans.ROIs = {'R_HGPM','R_HGAL','R_PT','R_PP','R_STG','R_Aud-rel','R_PFC'};
% hierarchySet:
exempChans.nums = [47,81,48,74,246,253,228,224,132];
exempChans.ROIs = {'R_HGPM','R_HGAL','R_PT','R_PP','R_STGP','R_STGM','R_MTG','R_SMG','R_PFC'};

batchParams.(patientID) = struct( ...
    'recDate', recDate,'ECoGchannels', ECoGchan, ...%'dataPath', dataPath,...
    'trialLength',trialLength, 'trialOverlap',trialOverlap, 'analysisSeg', analysisSeg);   
batchParams.(patientID).exempChans = exempChans;


%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%384B
electrodeFile = 'LGD electrodes for analysis - sorted by ROI.xlsx';
electrodeSheet = 'B384_CRU_Sleep';
patientID = '384B';
patientID = ['patient' patientID];
recDate = 'yymdd';
dataPrefix = 'LFPx_RZ2_chn';
signalName = 'LFPx';
ECoGchan = ECoGChannelMap_tableRead([electrodeFilePath electrodeFile],electrodeSheet);
% origSet:
% exempChans.nums = [78,74,57,46,238,235,230,220]; % pmHG,alHG,PT,STG,MTG,SMG,IFG,MFG
% exempChans.ROIs = {'L_STGP','L_STGM','L_IFGop','L_MFG','R_STGP','R_STGM','R_MTG','R_MFG'};
% nineROISet:
% exempChans.nums = [249,78,80,57,85,238,230,2,200];
% exempChans.ROIs = {'L_PP','L_STG','L_Aud-rel','L_PFC','L_Other','R_STG','R_Aud-rel','R_PFC','R_Other'};
% % sevenROISet:
% exempChans.nums = [249,78,75,80,57,238,235,230,2]; 
% exempChans.ROIs = {'L_PP','L_STGP','L_STGM','L_SMG','L_PFC','R_STGP','R_STGM','R_MTG','R_PFC'};
% hierarchySet:
exempChans.nums = [249,78,75,80,57,238,235,230,2]; 
exempChans.ROIs = {'L_PP','L_STGP','L_STGM','L_SMG','L_PFC','R_STGP','R_STGM','R_MTG','R_PFC'};

exempChans.setName = setName;

batchParams.(patientID) = struct( ...
    'recDate', recDate,'ECoGchannels', ECoGchan, ...%'dataPath', dataPath,...
    'trialLength',trialLength, 'trialOverlap',trialOverlap);   
batchParams.(patientID).exempChans = exempChans;
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%399R
electrodeFile = 'LGD electrodes for analysis - sorted by ROI.xlsx';
electrodeSheet = 'R399_CRU_Sleep';
patientID = '399R';
patientID = ['patient' patientID];
recDate = 'yymdd';
dataPrefix = 'LFPx_RZ2_chn';
signalName = 'LFPx';
ECoGchan = ECoGChannelMap_tableRead([electrodeFilePath electrodeFile],electrodeSheet);
% hierarchySet:
exempChans.nums = [17,21,40,11,173,243,175,133];
exempChans.ROIs = {'R_HGPM','R_HGAL','R_PT','R_PP','R_STG','R_Aud-rel','R_SMG','R_PFC'};

exempChans.setName = setName;

batchParams.(patientID) = struct( ...
    'recDate', recDate,'ECoGchannels', ECoGchan, ...%'dataPath', dataPath,...
    'trialLength',trialLength, 'trialOverlap',trialOverlap);   
batchParams.(patientID).exempChans = exempChans;

%%
%400L
electrodeFile = 'LGD electrodes for analysis - sorted by ROI.xlsx';
electrodeSheet = 'L400_CRU_Sleep';
patientID = '400L';
patientID = ['patient' patientID];
recDate = 'yymdd';
dataPrefix = 'LFPx_RZ2_chn';
signalName = 'LFPx';
ECoGchan = ECoGChannelMap_tableRead([electrodeFilePath electrodeFile],electrodeSheet);

% sevenROISet:
% exempChans.nums = [48,53,83,57,210,189,197];
% exempChans.ROIs = {'L_HGPM','L_HGAL','L_PT','L_PP','L_STG','L_Aud-rel','L_PFC'};
% hierarchySet:
exempChans.nums = [48,53,83,57,210,209,189,197];
exempChans.ROIs = {'L_HGPM','L_HGAL','L_PT','L_PP','L_STGM','L_MTG','L_AG','L_PFC'};

exempChans.setName = setName;

batchParams.(patientID) = struct( ...
    'recDate', recDate,'ECoGchannels', ECoGchan, ... %'dataPath', dataPath,...
    'dataPrefix',dataPrefix, 'signalName',signalName, ...
    'trialLength',trialLength, 'trialOverlap',trialOverlap);
batchParams.(patientID).exempChans = exempChans;
%%
%403L
electrodeFile = 'LGD electrodes for analysis - sorted by ROI.xlsx';
electrodeSheet = 'L403_CRU_Sleep';
patientID = '403L';
patientID = ['patient' patientID];
recDate = 'yymdd';
dataPrefix = 'LFPx_RZ2_chn';
signalName = 'LFPx';
ECoGchan = ECoGChannelMap_tableRead([electrodeFilePath electrodeFile],electrodeSheet);
% analysisSeg = [1,457]/2; %minutes into recording
analysisSeg = [1,1150]/2; %minutes into recording

% sevenROISet:
% exempChans.nums = [48,53,83,57,210,189,197];
% exempChans.ROIs = {'L_HGPM','L_HGAL','L_PT','L_PP','L_STG','L_Aud-rel','L_PFC'};
% hierarchySet:
exempChans.nums = [45,48,107,101,172,228,193,223,168];
exempChans.ROIs = {'L_HGPM','L_HGAL','L_PT','L_PP','L_STGM','L_STGP','L_MTG','L_SMG','L_PFC'};

exempChans.setName = setName;

batchParams.(patientID) = struct( ...
    'recDate', recDate,'ECoGchannels', ECoGchan, ... %'dataPath', dataPath,...
    'dataPrefix',dataPrefix, 'signalName',signalName, ...
    'trialLength',trialLength, 'trialOverlap',trialOverlap,'analysisSeg', analysisSeg);
batchParams.(patientID).exempChans = exempChans;
%%
%409L
electrodeFile = 'LGD electrodes for analysis - sorted by ROI.xlsx';
electrodeSheet = 'L409_CRU_Sleep';
patientID = '409L';
patientID = ['patient' patientID];
recDate = 'yymdd';
dataPrefix = 'LFPx_RZ2_chn';
signalName = 'LFPx';
ECoGchan = ECoGChannelMap_tableRead([electrodeFilePath electrodeFile],electrodeSheet);
% analysisSeg = [255,744]/2; %minutes into recording
analysisSeg = [255,1324]/2; %minutes into recording

% hierarchySet:
% exempChans.nums = [99,69,71,104,208,219,203,218,224];
% exempChans.ROIs = {'L_HGPM','L_HGAL','L_PT','L_PP','L_STGM','L_STGP','L_MTG','L_SMG','L_PFC'};
exempChans.nums = [33,248,246,87,254,26];
exempChans.ROIs = {'L_HGPM','L_STGM','L_STGP','L_MTG','L_SMG','L_PFC'};

exempChans.setName = setName;

batchParams.(patientID) = struct( ...
    'recDate', recDate,'ECoGchannels', ECoGchan, ...%'dataPath', dataPath,...
    'trialLength',trialLength, 'trialOverlap',trialOverlap, 'analysisSeg', analysisSeg);   
batchParams.(patientID).exempChans = exempChans;

%%
%423L
electrodeFile = 'LGD electrodes for analysis - sorted by ROI.xlsx';
electrodeSheet = 'L423_CRU_Sleep';
patientID = '423L';
patientID = ['patient' patientID];
recDate = 'yymdd';
dataPrefix = 'LFPx_RZ2_chn';
signalName = 'LFPx';
ECoGchan = ECoGChannelMap_tableRead([electrodeFilePath electrodeFile],electrodeSheet);
% analysisSeg = [94,690]/2; %30-sec epoch number divided by 2 gives minutes into recording
analysisSeg = [94,1407]/2; %30-sec epoch number divided by 2 gives minutes into recording

% hierarchySet:
exempChans.nums = [100,105,59,73,152,157,141,197,182];
exempChans.ROIs = {'L_HGPM','L_HGAL','L_PT','L_PP','L_STGM','L_STGP','L_MTG','L_SMG','L_PFC'};

exempChans.setName = setName;

batchParams.(patientID) = struct( ...
    'recDate', recDate,'ECoGchannels', ECoGchan, ...%'dataPath', dataPath,...
    'trialLength',trialLength, 'trialOverlap',trialOverlap, 'analysisSeg', analysisSeg);   
batchParams.(patientID).exempChans = exempChans;

