% How to use this file:
%   (1) Edit the paths however you wish so they they point to where you will be keeping 
%   each type of data. Although assumptions are made about the structure within each
%   directory, the directories themselves can be placed anywhere relative to each other. 
%   (2) Rename this file LocalPaths.m. If you are using git for version control, 
%   only this template will be synced across machines. This way, each 
%   person / machine can have their own paths saved on their own machine, without 
%   messing up anybody else's, or requiring you to change paths when sharing scripts. 

classdef LocalPaths_Template
   properties (Constant)
       % This directory does not need to exist, and is just defiend for convenience. 
       LGD_Data_Dir = '/hdd1/lgd_data';

       % These must be defined. 
       Electrode_File = fullfile(LocalPaths.LGD_Data_Dir, 'LGD electrodes for analysis - sorted by ROI - Box v46 Feb 19 2018.xlsx');
       Ecog_Data_Dir = fullfile(LocalPaths.LGD_Data_Dir, 'ecog-data');
       DBT_Data_Dir = fullfile(LocalPaths.LGD_Data_Dir, 'dbt-data');
       DBT_Plot_Dir = fullfile(LocalPaths.LGD_Data_Dir, 'dbt-plots');
       WPLI_Data_Dir = fullfile(LocalPaths.LGD_Data_Dir, 'wpli-data');
       WPLI_Plot_Dir = fullfile(LocalPaths.LGD_Data_Dir, 'wpli-plots');
   end
end
