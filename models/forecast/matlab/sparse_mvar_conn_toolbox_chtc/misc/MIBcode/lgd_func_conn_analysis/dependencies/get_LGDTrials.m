function [loclStd,globStd,loclDev,globDev] = get_LGDTrials(trialData)
%Load LGD data file and determine trial numbers that correspond to local
%standards, local deviants, global standards and global deviants
%note that onset of the 5th vowel (which defines deviance) is 600 ms after
%stimulus onset (i.e. FIDX.time(n)+600 (ms), where n is trial number)

%Input parameters
instr=[1:10 111:120 221:230 331:340]; %trial numbers that corresponds to the verbal instructions (to be removed from analysis)

%Determine sequence order
seqorder=[trialData.evnt(11) trialData.evnt(121) trialData.evnt(231) trialData.evnt(341)]+1;
%Key to FIDX.evnt codes:
%0 = /aaaaa/ (LS)
%1 = /aaaai/ (LD)
%2 = /iiiii/ (LS)
%3 = /iiiia/ (LD)
%4 = "this time press the button"
%5 = "when you hear this sound"
%6 = "once again, press the button"
%7 = silence


%Find trial numbers that correspond to the eight trial subsets
fidnum=find(trialData.evnt==1); %Find all relevant FIDX
seqnum=find(seqorder==1); %Number of the sequence of interest
rare1_1=fidnum(fidnum<(110*seqnum+1) & fidnum>(110*(seqnum-1))); %Find relevant FIDX within the relevant sequence

fidnum=find(trialData.evnt==1);
seqnum=find(seqorder==2);
freq1_2=fidnum(fidnum<(110*seqnum+1) & fidnum>(110*(seqnum-1)+10));

fidnum=find(trialData.evnt==3);
seqnum=find(seqorder==3);
rare3_3=fidnum(fidnum<(110*seqnum+1) & fidnum>(110*(seqnum-1)));

fidnum=find(trialData.evnt==3);
seqnum=find(seqorder==4);
freq3_4=fidnum(fidnum<(110*seqnum+1) & fidnum>(110*(seqnum-1)+10));

fidnum=find(trialData.evnt==0);
seqnum=find(seqorder==1);
freq0_1=fidnum(fidnum<(110*seqnum+1) & fidnum>(110*(seqnum-1)+10));

fidnum=find(trialData.evnt==0);
seqnum=find(seqorder==2);
rare0_2=fidnum(fidnum<(110*seqnum+1) & fidnum>(110*(seqnum-1)));

fidnum=find(trialData.evnt==2);
seqnum=find(seqorder==3);
freq2_3=fidnum(fidnum<(110*seqnum+1) & fidnum>(110*(seqnum-1)+10));

fidnum=find(trialData.evnt==2);
seqnum=find(seqorder==4);
rare2_4=fidnum(fidnum<(110*seqnum+1) & fidnum>(110*(seqnum-1)));

%Define ld, ls, gd, gs

loclDev=setdiff([rare1_1 freq1_2 rare3_3 freq3_4],instr); %Local deviant trial numbers
loclStd=setdiff([freq0_1 rare0_2 freq2_3 rare2_4],instr); %Local standard trial numbers
globDev=setdiff([rare1_1 rare0_2 rare3_3 rare2_4],instr); %Global deviant trial numbers
globStd=setdiff([freq0_1 freq1_2 freq2_3 freq3_4],instr); %Global standard trial numbers