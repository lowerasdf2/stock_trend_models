function pairs = get_interlvl_pairs(channel_labels)
% Get all pairs of channel that span two different levels of the hierarchy. 
%
% Arguments: 
% channel_labels - A cell array of channel labels, from which level information 
%                  can be extracted using e.g. lvl_from_label().
%
% Outputs: 
% pairs - An [n x 2] array of integers. Row [a b] exists if and only if 
%         channel_labels{a} is in one level, and channel_labels{b} is in a 
%         different level. 
pairs = [];
lvls = cellfun(@(x) lvl_from_label(x), channel_labels, 'UniformOutput', false);
nChannels = numel(channel_labels);
for chanA = 1 : nChannels
    for chanB = chanA : nChannels
        if ~strcmp(lvls{chanA}, lvls{chanB})
            pairs = [pairs ; chanA chanB];
        end
    end
end
