function lvl = lvl_from_label(channel_label)
% Return the level that a channel belongs to, based on it's label. 
% E.g. lvl_from_label('R_HGAL-Ch255') returns 'NonCore'. 
% Levels and ROIs are defined according to the ROIs.m file. 
roi = roi_from_label(channel_label);
for level = {'Core', 'NonCore', 'AudRel', 'PFC'}
    if ismember(roi, ROIs.(level{:}))
        lvl = level{:};
        return
    end
end
error('No level found');
