function labels = get_exempchan_labels(patientParams)
% Get the labels of the exemplary channels for this patient. 
% patientParams is an element of the array returned by load_batch_params.m
% The exemplar channel labels are returned as a cell array. 
label_idxs = find(ismember([patientParams.varInfo.chanNum], ...
                           patientParams.exempChans.nums));
labels = arrayfun(@(x) make_channel_label(x, patientParams.varInfo), ...
                  label_idxs, 'UniformOutput', false);

        
