function roi = roi_from_label(channel_label)
% Returns the ROI of the channel specified by channel_label. 
% E.g. roi_from_label('R_HGAL-Ch255') returns 'HGAL'. 
% ROIs are defined accoridng to the ROIs.m file. 

% Search for matches to pattern, e.g. extracts Aud-rel from R_Aud-rel-Ch136
tokens = regexp(channel_label, '[LR]_([A-Za-z-]*)-Ch\d+', 'tokens');
% Make sure only one match was found. 
assert(numel(tokens) == 1 && numel(tokens{:}) == 1, 'More than one ROI detected.');
roi = tokens{1}{1};

