function pairs = get_interroi_pairs(channel_labels)
% Get all pairs of channel that span two different ROIs. 
%
% Arguments: 
% channel_labels - A cell array of channel labels, from which ROI information 
%                  can be extracted using e.g. roi_from_label().
%
% Outputs: 
% pairs - An [n x 2] array of integers. Row [a b] exists if and only if 
%         channel_labels{a} is in one ROI, and channel_labels{b} is in a 
%         different ROI. 

pairs = [];
rois = cellfun(@(x) roi_from_label(x), channel_labels, 'UniformOutput', false);
nChannels = numel(channel_labels);
for chanA = 1 : nChannels
    for chanB = chanA : nChannels
        if ~strcmp(rois{chanA}, rois{chanB})
            pairs = [pairs ; chanA chanB];
        end
    end
end

