# lgd_func_conn_analysis
Code to assist with functional connectivity analysis of the Iowa auditory LGD data. 

## A quick guide to the code. 
You probably will interact with the code in roughly the following order: 
- `LocalPaths_Template.m`: Defines filesystem paths in a way that makes sharing scripts easier. 
  - You should edit this file according to the instructions it contains before doing anything else. 
- `constants`: Contains definitions of important, constant parameters (e.g. ROI names) used by many scripts. 
- `ecog_import`: Contains code for reading reading, converting, and working with the Iowa ECoG data.
- `dbt_computation`: Contains code for computing the demodulated band transform (DBT). 
  - ECoG data are loaded and converted to FieldTrip format channel-by-channel.
  - The DBT is performed channel-by-channel on the entire, continuous recording, then chopped up into trials and saved channel-by-chanel. 
  - Start with `dbt_main.m`
- `chtc_submit`: Contains code and instructions used to generate the files needed to start CHTC computations. 
  - This directory contains a README walkthrough of the steps needed to use the CHTC.
  - The code in this directory needs to be run locally, not on the CHTC. The files it generates, however, will be moved to the CHTC and used there. 
  - Make sure to edit `CHTCPaths_Template.m` before starting with `chtc_setup_main.m`.
  - So far only inter-roi channel comparisons have been computed, in order to save time and space. If you want to do within-roi WPLIs, this file is what you need to change. 
    - You should NOT have to recompile any of the code that runs of the CHTC for this. 
- `chtc_build`: Contains code and resources used/generated to produce the programs that compute the WPLI on the CHTC.
  - Most of this will only ever run on the "build node". 
- `chtc_repair`: Contains code used to detect computations that failed/where corrupted/were never started by the CHTC, and fix them. 
  - Start with `fix_wpli_main.m`.
- `data_aggregation`: Contains code used to take DBT and CHTC WPLI results, which are scattered across thousands of files, and average/aggreagte them into a useable form. 
  - Start with `aggregate_data_main.m`.
- `plotting`: Self-explanatory. 
- `wpli_computation`: Contains code used to compute the WPLI, both on the CHTC and locally (e.g. if you just want to compute WPLI between a small subset of exemplary channels). 
  - Use `wpli_main.m` for local computations only. You never have to open this folder if you're doing computations on the CHTC (though code here will be used). 
- `data_selection`: Contains code used to select subsets of the data (e.g. pairs of channels that span ROIs or levels of the hierarchy).
- `dependencies`: Contains all of the code needed for other functions to run (e.g. FieldTrip), so that this repository can be frozen/self-contained.
- `deprecated`: Contains some older functions which are no longer used, but might be useful for later reference. 

## TODOs
- Store cfg structures with returned results, FieldTrip style. 
- Experiment with a bipolar reference (a la Karoui et. al). 
