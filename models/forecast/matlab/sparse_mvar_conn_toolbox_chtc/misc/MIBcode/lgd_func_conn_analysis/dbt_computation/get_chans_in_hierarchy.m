function idx = get_chans_in_hierarchy(channel_labels)
% idx is an array which contains X if and only if channel_labels{X} describes a
%   channel in the hierarchy. E.g. channels with labels like 'R_Other-Ch123' are excluded. 

% Get the ROI of each channel
rois = cellfun(@(x) roi_from_label(x), channel_labels, 'UniformOutput', false);
% Get the indices of each ROI that is in the hierarchy. 
idx = find(cellfun(@(x) ismember(x, ROIs.All), rois));

