function OAASVal = getOAASVal(cond)

sPos = strfind(cond,'S');
cond = cond(sPos(1)+1:end);
underscorePos = strfind(cond,'_');
if ~isempty(underscorePos)
    cond = cond(1:underscorePos(1)-1);
end
tPos = strfind(cond,'t');
if isempty(tPos)
    OAASVal = str2num(cond);
else
    OAASVal = 0.5*(str2num(cond(1:tPos(1)-1)) + str2num(cond(tPos(1)+1:end)));
end

% switch cond
%     case 'OAAS5'
%         OAASVal = 5;
%     case 'OAAS5t4'
%         OAASVal = 4.5;
%     case 'OAAS4'
%         OAASVal = 4;
%     case 'OAAS4t3'
%         OAASVal = 3.5;
%     case 'OAAS3t4'
%         OAASVal = 3.5;
%     case 'OAAS3'
%         OAASVal = 3;
%     case 'OAAS3t2'
%         OAASVal = 2.5;
%     case 'OAAS2t3'
%         OAASVal = 2.5;
%     case 'OAAS3t1'
%         OAASVal = 2;
%     case 'OAAS2'
%         OAASVal = 2;
%     case 'OAAS1'
%         OAASVal = 1;
%     case 'OAAS1_1'
%         OAASVal = 1;
%     case 'OAAS1_2'
%         OAASVal = 1;
%     otherwise
%         OAASVal = NaN;
% end
