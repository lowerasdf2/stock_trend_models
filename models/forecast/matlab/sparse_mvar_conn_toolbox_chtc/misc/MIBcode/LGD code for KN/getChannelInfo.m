function [chanROI, chanNum] = getChannelInfo(params)

for iChan = 1:length(params.ECoGchannels)
    chanROI{iChan} = params.ECoGchannels(iChan).ROI;
    chanNum(iChan) = params.ECoGchannels(iChan).chanNum;
end
