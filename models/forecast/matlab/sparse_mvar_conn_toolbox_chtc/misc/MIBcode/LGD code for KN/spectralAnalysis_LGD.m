function spectralAnalysis_LGD()

% To do: vary length of segment window with frequency band

%% General parameters
if ~exist('outPath', 'var')
    outPath = '/Users/bankslaptop/Box Sync/My documents/Data and analysis/ECoG data/';
%     outPath = 'C:\Users\Banks_admin\Box Sync\My documents\Data and analysis\ECoG data\';
end
% Ensure output directory exists
if ~exist(outPath, 'dir')
    mkdir(outPath);
end
disp(['Data will be saved to `' outPath '`']);
% tinyCriterion = 0.02;

% %% Freq analysis params
freqTicks = [1,3,10,30];
freqsOfInterest = 1:2:80; %Hz
bandFreqs = [4,8,13,30];
specYLims = [1.e-3, 1.e3];
nmlzSpecYLims = [1.e-5, 1.e1];
epochLength = 1.4; %seconds
% freqSmoothing = [2,2,2,8,10,20]; %delta, theta, alpha, beta, gamma, high gamma
% bands = {'delta','theta','alpha','beta','gamma','highGamma'};


if ~exist('batchParams', 'var')
    getBatchParams_LGD;
end

%% Main analysis loop
patientIDs = fieldnames(batchParams)';
for iPatient = 1:length(patientIDs)
    name = patientIDs{iPatient};
    params = batchParams.(patientIDs{iPatient});
    conditions = fieldnames(params)';
    for iCond = 1:length(conditions)
        cond = conditions{iCond};
        % cond = cond{1};
        disp('------------------------');
        disp(['Condition: ' cond]);
        disp('------------------------');
        [loadedData,params.(cond)] = loadECoGData_LGD(params.(cond));
        % loadedData is matrix of nChan x nSamples
        [params.(cond).chanROI, params.(cond).chanNum] = getChannelInfo(params.(cond));
        [data_ECoG] = convertToFTFormat(loadedData,params.(cond));
        % segment data into trials of length trialLength
        trialStart = round(params.(cond).trialTimes./params.(cond).dT)';
        trialStop = round((params.(cond).trialTimes + epochLength)./params.(cond).dT)';
        trialStop = trialStop(trialStop<=size(loadedData,2));
        trialStart = trialStart(1:length(trialStop));
        trialOffset = zeros(length(trialStop),1); %trial onset corresponds to stim onset
        cfg         = [];
        cfg.trl = [trialStart trialStop trialOffset];
        data_ECoG   = ft_redefinetrial(cfg, data_ECoG);
        
        % downsample the data to speed up component analysis
        cfg = [];
        cfg.resamplefs = 200;
        cfg.detrend    = 'yes';
        % the following avoids numeric round off issues in the time axes upon resampling
        data_ECoG.time(1:end) = data_ECoG.time(1);
        data_ECoGds = ft_resampledata(cfg, data_ECoG);
        
        % Compute time-freq spectra
        ECoGchan = params.(cond).ECoGchannels;
        cfg              = [];
        cfg.output       = 'pow';
        cfg.pad        = 'nextpow2';
%         cfg.channel      = [ECoGchan(params.(cond).exempChans.nums).sortOrder]';
%         nChans = length(cfg.channel);
        cfg.method     = 'mtmconvol';
        cfg.taper      = 'dpss';
        cfg.foi        = freqsOfInterest;
        cfg.toi        = 0:0.05:1.4;
        cfg.t_ftimwin  = 0.25 * ones(size(cfg.foi));  % use a fixed window length of 0.2 seconds
        cfg.tapsmofrq  = nan(size(cfg.foi));  % start with NaN for all frequencies
        cfg.tapsmofrq(:)          = 20;       % use 20Hz smoothing for the high frequencies
        cfg.tapsmofrq(cfg.foi<50) = 10;       % use 10Hz smoothing for the intermediate frequencies
        cfg.tapsmofrq(cfg.foi<30) = 5;        % use 5Hz smoothing (minimum) for the low frequencies
        cfg.tapsmofrq(cfg.foi<20) = 4;        % use 5Hz smoothing (minimum) for the low frequencies
%         cfg.tapsmofrq(cfg.foi<10) = 1;        % use 5Hz smoothing (minimum) for the low frequencies
        cfg.keeptrials = 'no';
        
        trialSet(1).trialNums = params.(cond).loclStd;
        trialSet(2).trialNums = params.(cond).loclDev;
        trialSet(3).trialNums = params.(cond).globStd;
        trialSet(4).trialNums = params.(cond).globDev;
        trialSetLabel = {'loclStd','loclDev','globStd','globDev'};
        
        for iSet = 1:length(trialSet)
            cfg.trials = trialSet(iSet).trialNums;
            cfg.trials = cfg.trials(cfg.trials<=length(data_ECoGds.trial));
            ECoG_pow.(name).(cond).(trialSetLabel{iSet}) = ft_freqanalysis(cfg, data_ECoGds);
        end
    end %Loop over conditions
    batchParams.(patientIDs{iPatient}) = params;
end
save([outPath 'ECoG_pow_LGD.mat'],'ECoG_pow','batchParams');

%% Plot time-freq spectra of exemplary channels
patientIDs = fieldnames(batchParams)';
for iPatient = 1:length(patientIDs)
    name = patientIDs{iPatient};
    params = batchParams.(patientIDs{iPatient});
    conditions = fieldnames(params)';
    for iCond = 1:length(conditions)
        cond = conditions{iCond};
        if iCond == 1
            chans = params.(cond).exempChans;
            nChans = length(chans.nums);
            nCols = ceil(sqrt(length(chans.nums)));
            nRows = ceil(length(chans.nums)/nCols);
            panToLabel = (nRows-1)*nCols + 1;
            minVal = ones(1,nChans)*1.e12;
            maxVal = zeros(1,nChans);
        end
        trialSetLabel = {'loclStd','loclDev','globStd','globDev'};
        for iSet = 1:length(trialSetLabel)
            tfSpecFigName{iSet} = [name '-' cond '-' trialSetLabel{iSet} '-tfSpectra-ExempChans'];
            tfSpecFig(iSet) = figure('Name',tfSpecFigName{iSet});
            for iChan = 1:nChans
                timeVals = ECoG_pow.(name).(cond).(trialSetLabel{iSet}).time;
                freqVals = ECoG_pow.(name).(cond).(trialSetLabel{iSet}).freq;
                specToPlot = squeeze(ECoG_pow.(name).(cond).(trialSetLabel{iSet}).powspctrm(iChan,:,:));
                minVal(iChan) = min(minVal(iChan),min(specToPlot(:)));
                maxVal(iChan) = max(maxVal(iChan),max(specToPlot(:)));
                subplot(nRows,nCols,iChan);
                contourf(timeVals,freqVals,specToPlot,'LineStyle','none','LevelStep',(maxVal(iChan)-minVal(iChan))/50);
                ax = gca;
                ax.YScale = 'log';
                ax.YTick = freqTicks;
                ax.YTickLabel = freqTicks;
                ax.XLim = [min(timeVals), max(timeVals)];
                ax.Title.String = [params.(cond).exempChans.ROIs{iChan} '-Ch' num2str(params.(cond).exempChans.nums(iChan))];
                if iChan == panToLabel
                    ax.XLabel.String = 'Time (sec)';
                    ax.YLabel.String = 'Freq (Hz)';
                end
            end
        end
        for iSet = 1:length(trialSetLabel)
            figure(tfSpecFig(iSet));
            for iChan = 1:nChans
                subplot(nRows,nCols,iChan);
                caxis(gca,[minVal(iChan) 0.9*maxVal(iChan)]);
            end
            saveas(tfSpecFig(iSet),[outPath tfSpecFigName{iSet}]);
        end
    end
end

%% Plot time-freq ratio spectra of exemplary channels
patientIDs = fieldnames(batchParams)';
for iPatient = 1:length(patientIDs)
    name = patientIDs{iPatient};
    params = batchParams.(patientIDs{iPatient});
    conditions = fieldnames(params)';
    for iCond = 1:length(conditions)
        cond = conditions{iCond};
        if iCond == 1
            chans = params.(cond).exempChans;
            nChans = length(chans.nums);
            nCols = ceil(sqrt(length(chans.nums)));
            nRows = ceil(length(chans.nums)/nCols);
            panToLabel = (nRows-1)*nCols + 1;
            minVal = ones(1,nChans)*1.e12;
            maxVal = zeros(1,nChans);
        end
        trialSetLabel = {'loclStd','loclDev','globStd','globDev'};
        ratioSetLabel = {'Local','Global'};
        for iRatio = 1:length(ratioSetLabel)
            tfRatioFigName{iRatio} = [name '-' cond '-' ratioSetLabel{iRatio} '-tfRatio-DevVsStd-ExempChans'];
            tfRatioFig(iRatio) = figure('Name',tfRatioFigName{iRatio});
            for iChan = 1:nChans
                timeVals = ECoG_pow.(name).(cond).(trialSetLabel{iRatio}).time;
                freqVals = ECoG_pow.(name).(cond).(trialSetLabel{iRatio}).freq;
                ratioToPlot = ...
                    squeeze(ECoG_pow.(name).(cond).(trialSetLabel{2*iRatio}).powspctrm(iChan,:,:))./...
                    squeeze(ECoG_pow.(name).(cond).(trialSetLabel{2*iRatio-1}).powspctrm(iChan,:,:));
                minVal(iChan) = min(minVal(iChan),min(ratioToPlot(:)));
                maxVal(iChan) = max(maxVal(iChan),max(ratioToPlot(:)));
                subplot(nRows,nCols,iChan);
                contourf(timeVals,freqVals,ratioToPlot,'LineStyle','none','LevelStep',(maxVal(iChan)-minVal(iChan))/50);
                ax = gca;
                ax.YScale = 'log';
                ax.YTick = freqTicks;
                ax.YTickLabel = freqTicks;
                ax.XLim = [min(timeVals), max(timeVals)];
                ax.Title.String = [params.(cond).exempChans.ROIs{iChan} '-Ch' num2str(params.(cond).exempChans.nums(iChan))];
                if iChan == panToLabel
                    ax.XLabel.String = 'Time (sec)';
                    ax.YLabel.String = 'Freq (Hz)';
                end
            end
        end
        for iRatio = 1:length(ratioSetLabel)
            figure(tfRatioFig(iRatio));
            for iChan = 1:nChans
                subplot(nRows,nCols,iChan);
                caxis(gca,[minVal(iChan) 0.9*maxVal(iChan)]);
            end
           saveas(tfRatioFig(iRatio),[outPath tfRatioFigName{iRatio}]);
        end
    end
end

%% Plot time-freq spectra of exemplary ROIs
patientIDs = fieldnames(batchParams)';
for iPatient = 1:length(patientIDs)
    name = patientIDs{iPatient};
    params = batchParams.(patientIDs{iPatient});
    conditions = fieldnames(params)';
    for iCond = 1:length(conditions)
        cond = conditions{iCond};
        ECoGchan = batchParams.(name).(cond).ECoGchannels;
        if iCond == 1
            chans = params.(cond).exempChans;
            nROIs = length(chans.ROIs);
            for iChan = 1:length(ECoGchan)
                tempROI{iChan} = ECoGchan(iChan).ROI;
            end
            % Get channel numbers corresponding to ROIs
            chansToPlot = [];
            for iROI = 1:length(chans.ROIs)
                roiIndex = strcmp(tempROI,chans.ROIs{iROI});
                ROIs(iROI).chans = [ECoGchan(roiIndex).sortOrder];
                chansToPlot = [chansToPlot ROIs(iROI).chans];
                ROIs(iROI).label = chans.ROIs{iROI};                
            end
            minVal = ones(1,nROIs)*1.e12;
            maxVal = zeros(1,nROIs);
            nCols = ceil(sqrt(length(chans.ROIs)));
            nRows = ceil(length(chans.ROIs)/nCols);
            panToLabel = (nRows-1)*nCols + 1;
        end
        trialSetLabel = {'loclStd','loclDev','globStd','globDev'};
        for iSet = 1:length(trialSetLabel)
            tfSpecFigName{iSet} = [name '-' cond '-' trialSetLabel{iSet} '-tfSpectra-ExemplaryROIs'];
            tfSpecFig(iSet) = figure('Name',tfSpecFigName{iSet});
            timeVals = ECoG_pow.(name).(cond).(trialSetLabel{iSet}).time;
            freqVals = ECoG_pow.(name).(cond).(trialSetLabel{iSet}).freq;
            for iROI = 1:nROIs
                specToPlot = squeeze(mean(ECoG_pow.(name).(cond).(trialSetLabel{iSet}).powspctrm(ROIs(iROI).chans,:,:),1));
                minVal(iROI) = min(minVal(iROI),min(specToPlot(:)));
                maxVal(iROI) = max(maxVal(iROI),max(specToPlot(:)));
                subplot(nRows,nCols,iROI);
                contourf(timeVals,freqVals,specToPlot,'LineStyle','none','LevelStep',(maxVal(iROI)-minVal(iROI))/50);
                ax = gca;
                ax.YScale = 'log';
                ax.YTick = freqTicks;
                ax.YTickLabel = freqTicks;
                ax.XLim = [min(timeVals), max(timeVals)];
                ax.Title.String = params.(cond).exempChans.ROIs{iROI};
                if iROI == panToLabel
                    ax.XLabel.String = 'Time (sec)';
                    ax.YLabel.String = 'Freq (Hz)';
                end
            end
        end
        for iSet = 1:length(trialSetLabel)
            figure(tfSpecFig(iSet));
            for iROI = 1:nROIs
                subplot(nRows,nCols,iROI);
                caxis(gca,[minVal(iROI) 0.9*maxVal(iROI)]);
            end
            % saveas(tfSpecFig(iSet),[outPath tfSpecFigName{iSet}]);
        end
    end
end

%% Plot time-freq ratio spectra of exemplary ROIs
patientIDs = fieldnames(batchParams)';
for iPatient = 1:length(patientIDs)
    name = patientIDs{iPatient};
    params = batchParams.(patientIDs{iPatient});
    conditions = fieldnames(params)';
    for iCond = 1:length(conditions)
        cond = conditions{iCond};
        ECoGchan = batchParams.(name).(cond).ECoGchannels;
        if iCond == 1
            chans = params.(cond).exempChans;
            nROIs = length(chans.ROIs);
            for iChan = 1:length(ECoGchan)
                tempROI{iChan} = ECoGchan(iChan).ROI;
            end
            % Get channel numbers corresponding to ROIs
            chansToPlot = [];
            for iROI = 1:length(chans.ROIs)
                roiIndex = strcmp(tempROI,chans.ROIs{iROI});
                ROIs(iROI).chans = [ECoGchan(roiIndex).sortOrder];
                chansToPlot = [chansToPlot ROIs(iROI).chans];
                ROIs(iROI).label = chans.ROIs{iROI};                
            end
            minVal = ones(1,nROIs)*1.e12;
            maxVal = zeros(1,nROIs);
            nCols = ceil(sqrt(length(chans.ROIs)));
            nRows = ceil(length(chans.ROIs)/nCols);
            panToLabel = (nRows-1)*nCols + 1;
        end
        trialSetLabel = {'loclStd','loclDev','globStd','globDev'};
        ratioSetLabel = {'Local','Global'};
        for iRatio = 1:length(ratioSetLabel)
            tfRatioFigName{iRatio} = [name '-' cond '-' ratioSetLabel{iRatio} '-tfRatio-DevVsStd'];
            tfRatioFig(iRatio) = figure('Name',tfRatioFigName{iRatio});
            timeVals = ECoG_pow.(name).(cond).(trialSetLabel{iRatio}).time;
            freqVals = ECoG_pow.(name).(cond).(trialSetLabel{iRatio}).freq;
            for iROI = 1:nROIs
                ratioToPlot = ...
                    squeeze(mean(ECoG_pow.(name).(cond).(trialSetLabel{2*iRatio}).powspctrm(ROIs(iROI).chans,:,:),1));
                    squeeze(mean(ECoG_pow.(name).(cond).(trialSetLabel{2*iRatio-1}).powspctrm(ROIs(iROI).chans,:,:),1));
                minVal(iROI) = min(minVal(iROI),min(ratioToPlot(:)));
                maxVal(iROI) = max(maxVal(iROI),max(ratioToPlot(:)));
                subplot(nRows,nCols,iROI);
                contourf(timeVals,freqVals,ratioToPlot,'LineStyle','none','LevelStep',(maxVal(iROI)-minVal(iROI))/50);
                ax = gca;
                ax.YScale = 'log';
                ax.YTick = freqTicks;
                ax.YTickLabel = freqTicks;
                ax.XLim = [min(timeVals), max(timeVals)];
                ax.Title.String = params.(cond).exempChans.ROIs{iROI};
                if iROI == panToLabel
                    ax.XLabel.String = 'Time (sec)';
                    ax.YLabel.String = 'Freq (Hz)';
                end
            end
        end
        for iRatio = 1:length(ratioSetLabel)
            figure(tfRatioFig(iRatio));
            for iROI = 1:nROIs
                subplot(nRows,nCols,iROI);
                caxis(gca,[minVal(iROI) 0.9*maxVal(iROI)]);
            end
            % saveas(tfRatioFig(iRatio),[outPath tfRatioFigName{iRatio}]);
        end
    end
end
%% Plot spectra of exemplary channels
patientIDs = fieldnames(batchParams)';
for iPatient = 1:length(patientIDs)
    name = patientIDs{iPatient};
    figName = [name '-PowerSpec-ExemplaryChans'];
    powFig = figure('Name',figName);
    params = batchParams.(patientIDs{iPatient});
    conditions = fieldnames(params)';
    for iCond = 1:length(conditions)
        cond = conditions{iCond};
        if iCond == 1
            chans = params.(cond).exempChans;
            nCols = ceil(sqrt(length(chans.nums)));
            nRows = ceil(length(chans.nums)/nCols);
            panToLabel = (nRows-1)*nCols + 1;
        end
        ECoGchan = batchParams.(name).(cond).ECoGchannels;
        for iChan = 1:length(chans.nums)
            chanToPlot = ECoGchan(chans.nums(iChan)).sortOrder;
            chanLab = ECoGchan(chans.nums(iChan)).ROI;
            subplot(nRows,nCols,iChan);
            hold on
            plot(ECoG_pow.(name).(cond).freq,ECoG_pow.(name).(cond).powspctrm(chanToPlot,:),'LineWidth',2);
            if iCond == length(conditions)
                for iBand = 1:length(bandFreqs)                    
                    plot([bandFreqs(iBand),bandFreqs(iBand)],specYLims,':k');
                end
                ax = gca;
                ax.XScale = 'log';
                ax.XLim = [min(freqsOfInterest),max(freqsOfInterest)];
                ax.YLim = specYLims;
                ax.YScale = 'log';
                ax.Title.String = ['Ch' num2str(chans.nums(iChan)) '-' chans.ROIs{iChan}];
                if iChan == panToLabel
                    legend(conditions,'Location','SouthWest');
                    ax.XLabel.String = 'Freq (Hz)';
                    ax.YLabel.String = 'Power';
                end
            end
        end
    end
    saveas(powFig,[outPath figName]);
end

%% Plot spectra of exemplary ROIs
patientIDs = fieldnames(batchParams)';
for iPatient = 1:length(patientIDs)
    name = patientIDs{iPatient};
    params = batchParams.(patientIDs{iPatient});
    conditions = fieldnames(params)';
    figName = [name '-PowerSpec-ExemplaryROIs'];
    powFig = figure('Name',figName);
    for iCond = 1:length(conditions)
        cond = conditions{iCond};
        %bands = fieldnames(ECoG_conn.(name).(cond))';
        ECoGchan = batchParams.(name).(cond).ECoGchannels;
        if iCond == 1
            if exist('ROIs','var')
                clear ROIs
            end
            chans = params.(cond).exempChans;
            for iChan = 1:length(ECoGchan)
                tempROI{iChan} = ECoGchan(iChan).ROI;
            end
            % Get channel numbers corresponding to ROIs
            for iROI = 1:length(chans.ROIs)
                roiIndex = strcmp(tempROI,chans.ROIs{iROI});
                ROIs(iROI).chans = [ECoGchan(roiIndex).sortOrder];
                ROIs(iROI).label = chans.ROIs{iROI};                
            end
            nCols = ceil(sqrt(length(chans.ROIs)));
            nRows = ceil(length(chans.ROIs)/nCols);
            panToLabel = (nRows-1)*nCols + 1;
        end
        for iROI = 1:length(chans.ROIs)
            chansToPlot = ROIs(iROI).chans;
            tempSpec = ECoG_pow.(name).(cond).powspctrm(chansToPlot,:);
            % Normalize spectra before averaging
            for iChan = 1:length(ROIs(iROI).chans)
%                 if iCond == 1
                    ROIs(iROI).sumVal(iCond,iChan) = sum(tempSpec(iChan,:));
%                 end
                tempSpec(iChan,:) = tempSpec(iChan,:)/ROIs(iROI).sumVal(1,iChan);
            end
            subplot(nRows,nCols,iROI);
            hold on
            plot(ECoG_pow.(name).(cond).freq,mean(tempSpec,1),'LineWidth',2);
            if iCond == length(conditions)
                for iBand = 1:length(bandFreqs)                    
                    plot([bandFreqs(iBand),bandFreqs(iBand)],nmlzSpecYLims,':k');
                end
                ax = gca;
                ax.XScale = 'log';
                ax.XLim = [min(freqsOfInterest),max(freqsOfInterest)];
                ax.YLim = nmlzSpecYLims;
                ax.YScale = 'log';
                ax.Title.String = chans.ROIs{iROI};
                if iROI == panToLabel
                    legend(conditions,'Location','SouthWest');
                    ax.XLabel.String = 'Freq (Hz)';
                    ax.YLabel.String = 'Nmlz power';
                end
            end
        end
    end
    saveas(powFig,[outPath figName]);
end

