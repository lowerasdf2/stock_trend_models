%Plot power difference between standard and deviant trials in the LGD
%experiment, 1 channel at a time

clear all;
%close all;
tic

%% Input parameters
indir = '/Users/grahamfindlay/Box Sync/ECoG data/LGD_data/ecog-data/patient369LR/LGD-OR/OAAS5_1/';
% indir='/Users/bankslaptop/Box Sync/My documents/Data and analysis/ECoG data/patient369LR/LGD-OR/OAAS5_1/'; %Input directory
% indir='D:\Box Sync\My documents\Data and analysis\ECoG data\patient369LR\LGD-OR\OAAS5_1\'; %Input directory
blk='369-130'; %Block number
chan=49; %Channel number

%% Analysis parameters
fs=1000; %Sampling rate
freqranges=[[4 8];[8 14];[14 30];[30 70];[70 150]]; %Frequency bands
%default: theta [4 8]; alpha [8 14]; beta [14 30]; gamma [30 70]; high gamma [70 150]);
bws=[1 2 4 10 20]; %Bandwidths (center frequency step sizes) for each band
%default: [1 2 4 10 20] (For finer frequency resolution, use bws=[0.5 1 2 5 10];
epochrange=[0 1400]; %Define trial (ms re time of event code)
trialrej=1; %
numstd=5;
local=1; %set to 1 to plot LOCAL deviant - standard; any other value will plot GLOBAL comparison

%% Display parameters
xrange=[600 1400]; %Time range to plot (ms re time of event code)
zrange=[-6 6]; %Range of dB values to plot
logfreq=1; %Toggle log scale for y-axis (0=linear, 1=log)
ntimes=2; %Number of times for 2D interpolation (to make TF plot look smoother)
disptext=1; %Toggle tick and axis labels (0=no, 1=yes)

%% Calculate additional variables
plotrange=(xrange(1):xrange(2))-epochrange(1)+1;
instr=[1:10 111:120 221:230 331:340]; %instruction trials, to be removed from analysis

figure;
load([indir blk '_SPECIALevents_DBT1.mat'],['LFPx_RZ2_chn' sprintf('%03d', chan)],'FIDX');
datatemp=eval(['double(LFPx_RZ2_chn' sprintf('%03d', chan) '.dat)']);
data=zeros(ceil(length(datatemp)/1000)*1000+1001,1)+eps; %pad with eps to next second
data(1:length(datatemp))=datatemp;

%% Find trial numbers that correspond to the eight trial subsets (global standards and deviants in each of the four 100-trial sub-blocks)
seqorder=[FIDX.evnt(11) FIDX.evnt(121) FIDX.evnt(231) FIDX.evnt(341)]+1; %Determine sequence order
fidnum=find(FIDX.evnt==1); %Find all relevant FIDX
seqnum=find(seqorder==1); %Number of the sequence of interest
rare1_1=fidnum(fidnum<(110*seqnum+1) & fidnum>(110*(seqnum-1))); %Find relevant FIDX within the relevant sequence
fidnum=find(FIDX.evnt==1);
seqnum=find(seqorder==2);
freq1_2=fidnum(fidnum<(110*seqnum+1) & fidnum>(110*(seqnum-1)+10));
fidnum=find(FIDX.evnt==3);
seqnum=find(seqorder==3);
rare3_3=fidnum(fidnum<(110*seqnum+1) & fidnum>(110*(seqnum-1)));
fidnum=find(FIDX.evnt==3);
seqnum=find(seqorder==4);
freq3_4=fidnum(fidnum<(110*seqnum+1) & fidnum>(110*(seqnum-1)+10));
fidnum=find(FIDX.evnt==0);
seqnum=find(seqorder==1);
freq0_1=fidnum(fidnum<(110*seqnum+1) & fidnum>(110*(seqnum-1)+10));
fidnum=find(FIDX.evnt==0);
seqnum=find(seqorder==2);
rare0_2=fidnum(fidnum<(110*seqnum+1) & fidnum>(110*(seqnum-1)));
fidnum=find(FIDX.evnt==2);
seqnum=find(seqorder==3);
freq2_3=fidnum(fidnum<(110*seqnum+1) & fidnum>(110*(seqnum-1)+10));
fidnum=find(FIDX.evnt==2);
seqnum=find(seqorder==4);
rare2_4=fidnum(fidnum<(110*seqnum+1) & fidnum>(110*(seqnum-1)));

%% Noisy trial rejection
rejtrials=[];
if trialrej==1;
    rej=find(abs(data)>std(data)*numstd);
    if isempty(rej)==0;
        trialtimes=[round(FIDX.time*fs)+epochrange(1); round(FIDX.time*fs)+epochrange(2)]'; %trial beginning and end, in pts
        for ii=1:length(FIDX.time)
            for jj=1:length(rej)
                if rej(jj) > trialtimes(ii,1) && rej(jj) < trialtimes(ii,2)
                    rejtrials=[rejtrials ii];
                end
            end
        end
        rejtrialind=unique(rejtrials);
    else
        rejtrialind=[];
    end
else rejtrialind=[];
end

%% Define ld, ls, gd, gs
ld=setdiff(setdiff([rare1_1 freq1_2 rare3_3 freq3_4],rejtrialind),instr);
ls=setdiff(setdiff([freq0_1 rare0_2 freq2_3 rare2_4],rejtrialind),instr);
gd=setdiff(setdiff([rare1_1 rare0_2 rare3_3 rare2_4],rejtrialind),instr);
gs=setdiff(setdiff([freq0_1 freq1_2 freq2_3 freq3_4],rejtrialind),instr);

%% Perform DBT on each frequency band of interest;
for ecogband=1:size(freqranges,1); %ECoG band loop (e.g. theta, alpha, beta, gamma, high gamma);
    bw=bws(ecogband); %bandwidth (Hz)
    freqrange=squeeze(freqranges(ecogband,:)); %frequency range
    fftpadval=2000/(4*bw)-1; %fft padding value (to achieve 1K sampling rate of the output)
    B=dbt(data,fs,bw,'offset',freqrange(1),'fftpad',fftpadval,'lowpass',freqrange(2)-bw); %DBT structure
    f=B.frequency; %frequency vector
    [A,tt] = choptf(epochrange/fs,FIDX.time,B);
    erbp=20*log10(abs(A)); %Convert amplitude to dB
    
    erbp_l=squeeze(mean(erbp(plotrange,:,ld),3))-squeeze(mean(erbp(plotrange,:,ls),3)); %local deviance
    erbp_g=squeeze(mean(erbp(plotrange,:,gd),3))-squeeze(mean(erbp(plotrange,:,gs),3)); %global deviance
    
    if local ==1;
        dataplot=interp2(erbp_l,ntimes); %Average across trials and interpolate for plotting
    else
        dataplot=interp2(erbp_g,ntimes); %Average across trials and interpolate for plotting
    end
    if logfreq==1;
        f=log2(f);
    end;
    hold on;
    imagesc(tt(plotrange),f,dataplot',zrange);
    axis tight;
    if disptext==0;
        set(gca,'xtick', [ ]); set(gca,'ytick', [ ]);
        grid off; axis off; %remove ticks and labels
    else
        xlabel('Time (s)') % x-axis label
        if logfreq==0
            ylabel('Frequency (Hz)') % y-axis label
        else
            ylabel('Log2(Frequency)') % y-axis label
        end;
    end; %of the disptext loop
end; %of the ecogband loop

toc