clear all;
%close all;
indir='D:\Data\Anesthesia LGD\';
%blk='376-141';
blk='362-076';
load([indir blk '_SPECIALevents_DBT1.mat'],'Inpt_RZ2_chn001','FIDX');
trialcodes=FIDX.evnt;
trialtimes=FIDX.time;
buttonpress=double(Inpt_RZ2_chn001.dat);

figure, plot(trialtimes.*1000,trialcodes,'b'); hold on; plot(buttonpress,'r');