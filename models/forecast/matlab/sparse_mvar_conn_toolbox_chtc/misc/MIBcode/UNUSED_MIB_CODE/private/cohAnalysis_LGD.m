function cohAnalysis_LGD()

% To do: vary length of segment window with frequency band

%% General parameters
if ~exist('outPath', 'var')
    outPath = '/Users/bankslaptop/Box Sync/My documents/Data and analysis/ECoG data/';
%     outPath = 'C:\Users\Banks_admin\Box Sync\My documents\Data and analysis\ECoG data\';
end
% Ensure output directory exists
if ~exist(outPath, 'dir')
    mkdir(outPath);
end
disp(['Data will be saved to `' outPath '`']);
tinyCriterion = 0.02;

%% Freq analysis params
% %% Freq analysis params
freqsOfInterest = 1:2:40; %Hz
bandFreqs = [4,8,13,30];
specYLims = [1.e-2, 1.e0];

%% Main analysis loop
if ~exist('batchParams', 'var')
    getBatchParams;
end

patientIDs = fieldnames(batchParams)';
for iPatient = 1:length(patientIDs)
    name = patientIDs{iPatient};
    params = batchParams.(patientIDs{iPatient});
    conditions = fieldnames(params)';
    for iCond =1:length(conditions)
        cond = conditions{iCond};
        ECoGchan = batchParams.(name).(cond).ECoGchannels;
        if iCond == 1
            chans = params.(cond).exempChans;
        end
        % cond = cond{1};
        disp('------------------------');
        disp(['Condition: ' cond]);
        disp('------------------------');
        [loadedData,params.(cond)] = loadECoGData(params.(cond));
        % loadedData is matrix of nChan x nSamples
        [params.(cond).chanSide, params.(cond).chanROI, params.(cond).chanNum,...
            params.(cond).sortOrder] = getChannelInfo(params.(cond));
        [data_ECoG] = convertToFTFormat(loadedData,params.(cond));
        % segment data into trials of length trialLength
        cfg         = [];
        cfg.length  = params.(cond).trialLength;
        cfg.overlap = params.(cond).trialOverlap;
        data_ECoG   = ft_redefinetrial(cfg, data_ECoG);
        
        % downsample the data to speed up component analysis
        cfg = [];
        cfg.resamplefs = 200;
        cfg.detrend    = 'yes';
        % the following avoids numeric round off issues in the time axes upon resampling
        data_ECoG.time(1:end) = data_ECoG.time(1);
        data_ECoGds = ft_resampledata(cfg, data_ECoG);
        nTrials = length(data_ECoGds.trial);
        [nChans,nPtsPerTrial] = size(data_ECoGds.trial{1,1});
        data_ECoGds.sampleinfo = [1+(0:nTrials-1)*nPtsPerTrial; (1:nTrials)*nPtsPerTrial]';
        
        % compute Fourier spectra, to be used for cross-spectral density computation.
        cfg            = [];
        cfg.method     = 'mtmfft';
%         if iPatient == 1
%             cfg.trials = ceil(nTrials/2):nTrials;
%         end
%         cfg.channel    =  [ECoGchan(chans.nums).sortOrder]';
        cfg.output     = 'fourier';
        cfg.foi        = freqsOfInterest;
        cfg.tapsmofrq  = 1;
        cfg.pad        = 'nextpow2';
        cfg.keeptrials = 'yes';
        FFT_ECoG       = ft_freqanalysis(cfg, data_ECoGds);
        
        % Compute coherence spectra
        cfg=[];
        cfg.method  ='coh';
        cfg.jackknife = 'yes';
        ECoG_conn.(name).(cond).coh = ft_connectivityanalysis(cfg,FFT_ECoG);
    end %Loop over conditions
end
save([outPath 'ECoG_coh_conn.mat'],'ECoG_conn','batchParams');

%% Plot coherence spectra
patientIDs = fieldnames(batchParams)';
for iPatient = 1:length(patientIDs)
    name = patientIDs{iPatient};
    figName = [name '-CoherenceSpec-ExemplaryChans'];
    cohFig = figure('Name',figName);
    params = batchParams.(patientIDs{iPatient});
    conditions = fieldnames(params)';
    for iCond = 1:length(conditions)
        cond = conditions{iCond};
        if iCond == 1
            chans = params.(cond).exempChans;
            nCols = length(chans.nums)-1;
            nRows = nCols;
            panToLabel = (nRows-1)*nCols + 1;
        end
        for iChan = 2:length(chans.nums)
            for jChan = 1:iChan-1
                iRow = iChan-1;
                iCol = jChan;
                iPan = iCol+(iRow-1)*nCols;
                subplot(nRows,nCols,iPan);
                hold on
                plot(ECoG_conn.(name).(cond).coh.freq,...
                    squeeze(ECoG_conn.(name).(cond).coh.cohspctrm(iChan,jChan,:)),'LineWidth',2);
                if iCond == length(conditions)
                    for iBand = 1:length(bandFreqs)
                        plot([bandFreqs(iBand),bandFreqs(iBand)],specYLims,':k');
                    end
                    ax = gca;
                    ax.XScale = 'log';
                    ax.XLim = [min(freqsOfInterest),max(freqsOfInterest)];
                    ax.YLim = specYLims;
                    ax.YScale = 'log';
                    if iCol == iRow
                        ax.Title.String = [chans.ROIs{jChan} '-' num2str(chans.nums(jChan))...
                            ':'  chans.ROIs{iChan} '-' num2str(chans.nums(iChan))];
                    else
                        ax.Title.String = ['   :'  chans.ROIs{iChan} '-' num2str(chans.nums(iChan))];
                    end
                    if iChan == panToLabel
                        legend(conditions,'Location','SouthWest');
                        ax.XLabel.String = 'Freq (Hz)';
                        ax.YLabel.String = 'Coherence';
                    end
                end
            end
        end
    end
    legend(conditions,'Location','SouthWest');
    saveas(cohFig,[outPath figName]);
end

%% Plot coherence spectra - by ROI
patientIDs = fieldnames(batchParams)';
for iPatient = 1:length(patientIDs)
    name = patientIDs{iPatient};
    figName = [name '-CoherenceSpec-ExemplaryROIs'];
    cohFig = figure('Name',figName);
    params = batchParams.(patientIDs{iPatient});
    conditions = fieldnames(params)';
    for iCond = 1:length(conditions)
        cond = conditions{iCond};
        ECoGchan = batchParams.(name).(cond).ECoGchannels;
        if iCond == 1
            if exist('ROIs','var')
                clear ROIs
            end
            chans = params.(cond).exempChans;
            for iChan = 1:length(ECoGchan)
                tempROI{iChan} = ECoGchan(iChan).ROI;
            end
            % Get channel numbers corresponding to ROIs
            for iROI = 1:length(chans.ROIs)
                roiIndex = strcmp(tempROI,chans.ROIs{iROI});
                ROIs(iROI).chans = [ECoGchan(roiIndex).sortOrder];
                ROIs(iROI).label = chans.ROIs{iROI};                
            end
            chans = params.(cond).exempChans;
            nCols = length(chans.ROIs)-1;
            nRows = nCols;
            panToLabel = (nRows-1)*nCols + 1;
        end
        tempMeas = NaN(length(chans.ROIs),length(chans.ROIs),length(ECoG_conn.(name).(cond).coh.freq));
        for iROI = 2:length(chans.ROIs)
            for jROI = 1:iROI-1
                valROIMat = ECoG_conn.(name).(cond).coh.cohspctrm(ROIs(iROI).chans,ROIs(jROI).chans,:);
                tempMeas(iROI,jROI,:) = mean(mean(valROIMat,1,'omitnan'),2,'omitnan');
            end
        end
        for iROI = 2:length(chans.ROIs)
            for jROI = 1:iROI-1
                iRow = iROI-1;
                iCol = jROI;
                iPan = iCol+(iRow-1)*nCols;
                subplot(nRows,nCols,iPan);
                hold on
                plot(ECoG_conn.(name).(cond).coh.freq,...
                    squeeze(tempMeas(iROI,jROI,:)),'LineWidth',2);
                if iCond == length(conditions)
                    for iBand = 1:length(bandFreqs)
                        plot([bandFreqs(iBand),bandFreqs(iBand)],specYLims,':k');
                    end
                    ax = gca;
                    ax.XScale = 'log';
                    ax.XLim = [min(freqsOfInterest),max(freqsOfInterest)];
                    ax.YLim = specYLims;
                    ax.YScale = 'log';
                    if iCol == iRow
                        ax.Title.String = [chans.ROIs{jROI} '-' ...
                            ':'  chans.ROIs{iROI}];
                    else
                        ax.Title.String = ['   :'  chans.ROIs{iROI}];
                    end
                    if iChan == panToLabel
                        legend(conditions,'Location','SouthWest');
                        ax.XLabel.String = 'Freq (Hz)';
                        ax.YLabel.String = 'Coherence';
                    end
                end
            end
        end
    end
    legend(conditions,'Location','SouthWest');
    saveas(cohFig,[outPath figName]);
end
