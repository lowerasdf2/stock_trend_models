function [loadedData, stageData, params] = loadECoGData_sleep_justScored(params,thisPath)
%loadECoGData  Load and concatenate ECoG data from Howard lab.
%
% Returns an array of filtered data organized as follows:
%   (channel) x (sample)
%
% params.sleepData.stage
% params.sleepData.time
% params.sleepData.key

nChans = length(params.ECoGchannels);
signalName = 'LFPx';
% Get all data files
dataFiles = dir([thisPath '*.mat']);
theseFiles = {dataFiles.name};
theseFiles = theseFiles(~contains(theseFiles,'sleepData'));
loadedData = [];
stageData = [];
stageKey = [];
for jFile = 1:length(theseFiles)
    fileName = theseFiles{jFile};
    fileObj = matfile([thisPath fileName]);
    fileDetails = whos(fileObj);
    dataFields = {fileDetails.name};
    if sum(contains(dataFields,'STAGE'))==2
        load([thisPath fileName],dataFields{contains(dataFields,'STAGE')});
        STAGE = STAGE';
        STAGE_KEY = STAGE_KEY';
    end
    if sum(~isnan(STAGE))>0
        stageIndex = ~isnan(STAGE);
        chanFields = dataFields(contains(dataFields,signalName));
        chanNum = zeros(1,length(chanFields));
        chanStr = {};
        for iChan = 1:length(chanFields)
            thisChan = chanFields{iChan};
            chanStr{iChan} = thisChan(strfind(thisChan,signalName)+length(signalName):strfind(thisChan,'_')-1);
            chanNum(iChan) = str2num(chanStr{iChan});
        end
        [chanNum,sortIndex] = sort(chanNum);
        chanStr = chanStr(sortIndex);
        chanFields = chanFields(sortIndex);
        for iChan = 1:nChans
            thisChan = params.ECoGchannels(iChan).chanNum;
            display(['Loading ch#' num2str(thisChan)]);
            dataToLoad = chanFields{chanNum==thisChan};
            thisData = load([thisPath fileName],dataToLoad);
            if iChan == 1
                params.nPts = length(thisData.(dataToLoad).dat);
                tempData = zeros(nChans,params.nPts);
                params.dT = 1/thisData.(dataToLoad).fs; %sec
            end
            tempData(iChan,:) = thisData.(dataToLoad).dat;
        end
        loadedData = [loadedData tempData(:,stageIndex)];
        stageData = [stageData STAGE(stageIndex)];
        stageKey = [stageKey STAGE_KEY];
    end
end %loop over data files
params.nPts = size(loadedData,2);
params.STAGE_KEY = unique(stageKey);

