function [loadedData, STAGE, STAGE_KEY, params] = loadECoGData_eachSleepFile(params,fileName)
% sleepDataPath = [dataPath thisName filesep 'Sleep data' filesep];
%loadECoGData  Load and concatenate ECoG data from Howard lab.
%
% Returns an array of filtered data organized as follows:
%   (channel) x (sample)
%
% params.sleepData.stage
% params.sleepData.time
% params.sleepData.key

nChans = length(params.ECoGchannels);
signalName = 'LFPx';

%% Load ECoG data
% Get all data files
display(['Loading ' fileName]);
fileObj = matfile(fileName);
fileDetails = whos(fileObj);
dataFields = {fileDetails.name};
if sum(contains(dataFields,'STAGE'))==2
    load(fileName,dataFields{contains(dataFields,'STAGE')});
    STAGE = STAGE';
    STAGE_KEY = STAGE_KEY';
end
chanFields = dataFields(contains(dataFields,signalName));
chanNum = zeros(1,length(chanFields));
chanStr = {};
for iChan = 1:length(chanFields)
    thisChan = chanFields{iChan};
    chanStr{iChan} = thisChan(strfind(thisChan,signalName)+length(signalName):strfind(thisChan,'_')-1);
    chanNum(iChan) = str2num(chanStr{iChan});
end
[chanNum,sortIndex] = sort(chanNum);
chanStr = chanStr(sortIndex);
chanFields = chanFields(sortIndex);
for iChan = 1:nChans
    thisChan = params.ECoGchannels(iChan).chanNum;
    display(['Loading ch#' num2str(thisChan)]);
    dataToLoad = chanFields{chanNum==thisChan};
    tempData = load(fileName,dataToLoad);
    if iChan == 1
        params.nPts = length(tempData.(dataToLoad).dat);
        loadedData = zeros(nChans,params.nPts);
        params.dT = 1/tempData.(dataToLoad).fs; %sec
    end
    loadedData(iChan,:) = tempData.(dataToLoad).dat;
end
