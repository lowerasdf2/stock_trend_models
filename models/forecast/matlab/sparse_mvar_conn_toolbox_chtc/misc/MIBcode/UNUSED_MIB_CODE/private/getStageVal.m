function stageVal = getStageVal(stage)

switch stage
    case 'W'
        stageVal = 1;
    case 'N1'
        stageVal = 2;
    case 'N2'
        stageVal = 3;
    case 'N3'
        stageVal = 4;
    case 'R'
        stageVal = 5;
    otherwise
        stageVal = NaN;
end