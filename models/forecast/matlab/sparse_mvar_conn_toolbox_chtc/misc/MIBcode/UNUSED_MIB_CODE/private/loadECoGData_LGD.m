function [loadedData, params] = loadECoGData_LGD(params,pathToLoad)
%loadECoGData  Load and concatenate ECoG data from Howard lab.
% params = params.(cond);
%
% Returns an array of filtered data organized as follows:
%   (channel) x (sample)
%

nChannels = length(params.ECoGchannels);

%% Load ECoG data

% Get all data files
dataFiles = dir(fullfile(pathToLoad, '*.mat'));
for jFile = 1:length(dataFiles)
    display(['Loading ' dataFiles(jFile).name]);
    for iChan = 1:nChannels
        thisChan = params.ECoGchannels(iChan).chanNum;
        if thisChan < 10
            chanStr = ['00' num2str(thisChan)];
        elseif thisChan < 100
            chanStr = ['0' num2str(thisChan)];
        else
            chanStr = num2str(thisChan);
        end
        chanDataName = [params.dataPrefix chanStr];
        tempData = load(fullfile(pathToLoad, dataFiles(jFile).name), ...
            chanDataName);
        if iChan == 1
            params.nPts = length(tempData.(chanDataName).dat);
            loadedData = zeros(nChannels,params.nPts);
            params.dT = 1/tempData.(chanDataName).fs(1); %sec
            tempGain = load(fullfile(pathToLoad, dataFiles(jFile).name),'gain');
            params.gain = tempGain.gain.(params.signalName);
        end
        loadedData(iChan,:) = tempData.(chanDataName).dat; 
        clear tempData
    end
    load(fullfile(pathToLoad, dataFiles(jFile).name),'FIDX');
    trialData = FIDX;
    params.trialTimes = trialData.time;
    [params.loclStd,params.globStd,params.loclDev,params.globDev] = get_LGDTrials(trialData);
end

loadedData = loadedData*params.gain;

