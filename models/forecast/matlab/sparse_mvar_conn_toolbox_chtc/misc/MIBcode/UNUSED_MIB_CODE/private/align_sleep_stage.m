function out = align_sleep_stage(sleepStageFile,data,evnt)
% function out = align_sleep_stage(subject,data,evnt)

% use:  out = align_sleep_stag(subject_id, data_structure)
%
%    out.STAGE :  array indicating sleep stage as number, interpolated onto
%                 the sampling of data.dat.
%
%    out.STAGE_KEY: Key for out.STAGE

% subject = '372';

% fn = dir('362*_denoised.mat');
%  ld = load(fn(k).name,'evnt','blkdat','li9');
% d = ld.li9;

% sleepfn =dir(sprintf('%s*.txt',subject));

%stagedat = importfile(sleepfn.name);
stagedat = importfile(sleepStageFile);

[STAGE_KEY,~,stage] =unique(stagedat(:,4));

timestamp = cellfun(@(d,t) datenum(sprintf('%s %s',d,t)),stagedat(:,2),stagedat(:,3));
%%
for k = 1:length(data)
    
    if ~isfield(data,'time_fmt')
      %  ld = load(fn(k).name);
        x = d;
        tst = evnt;

        [tdt_tstamp,evon] = get_timestamp(evnt);
        

        if all(isnan(tdt_tstamp))

            t0 = datenum(ld.blkdat.date);
        else
            t0  = nanmean(tdt_tstamp-evnt.time(evon)/24/3600);
        end
        
         tdtT = (0:length(data.dat)-1)'/x.fs(1)./3600/24 +t0;
    else
       switch data.time_fmt
           case 'unix_usec'
               sc = 1e-6;
               offs = datenum('1/1/1970');
           case 'unix_msec'
                sc = 1e-3;
                offs = datenum('1/1/1970');
           case 'unix_sec'
               sc = 1;
               offs = datenum('1/1/1970');
           otherwise
               sc = 1;
               offs = 0;
       end
       
       blockLength = length(data.dat)/length(data.TimeStamp);
       nlxT = data.TimeStamp*sc/24/3600 + offs;
       tdtT = repmat(nlxT',blockLength,1)+repmat((0:blockLength-1)'/data.fs(1)/24/3600,1,length(nlxT));
       
    end
        
    out(k).STAGE = interp1(timestamp,stage,tdtT(:),'nearest');
    out(k).STAGE_KEY=STAGE_KEY;
%      save(fn(k).name,'-append','STAGE','STAGE_KEY');
%    save(fn(k).name,'-struct','ld');
end
    