function stagesOut = myStageSort(stagesIn)
stageOrder = {'W','R','N1','N2','N3'};
stagesOut = stagesIn(~contains(stagesIn,'N/A'));
stageRef = 1:length(stagesOut);
stageIndex = zeros(1,length(stagesOut));
for iStage = 1:length(stagesOut)
    stageIndex(iStage) = stageRef(contains(stagesOut,stageOrder{iStage}));
end
stagesOut = stagesOut(stageIndex);

