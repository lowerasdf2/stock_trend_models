function preprocess_SleepData(sleepStageFile,dataPath,dirList)
% Function to downsample sleep data from Howard lab, restrict data to that
% time period corresponding to sleep staging, and save the data into one
% data structure. 
%
% sleepStageFile is a text file containing sleep stage info that is read by
% align_sleep_stage.m
%
% dataPath is the path to the folder containing the specific recording
% epochs subfolders
%
% dirList is the list of subdirectories containing the data for each
% recording epoch
%
% It is assumed that data are stored in subdirectories corresponding to
% recording epochs, with each channel stored in a different file.
% Expected structure of data file is structured array, with the following
% fields:
%       fs = original sampling rate, in Hz
%       dat = Nx1 array of sampled data
%       TimeStamp = time stamps for each recording block (512 sampled data pts)
%
% The names of the single channel data files are assumed to be of the form 
% 'chanToken|chanNum|_XXXXX_nameToken.mat', e.g. 
% 'LFPx99_0001_li99_denoised.mat' for channel 99.
%
nameToken = 'denoised';
chanToken = 'LFPx';

fsDS = 250; % Downsampled rate, Hz

if ~exist('sleepStageFile','var')
    sleepStageFile= '369_sleep_stage.txt';
end

if ~exist('dataPath','var')
    % Get directories of data
    defaultPath = '/Users/bankslaptop/Box Sync/My documents/Data and analysis/ECoG data/';
    % defaultPath = 'D:\Box Sync\My documents\Data and analysis\Data and analysis\ECoG data\';
    patientID = '369LR';
    patientID = ['patient' patientID];
    dataPath = [defaultPath patientID filesep 'Sleep data' filesep];
end

if ~exist('dirList','var')
    dirList = dir([dataPath '*-*']);
end

for iDir = 1:length(dirList)
    if dirList(iDir).isdir
        % Load in one file from directory to test whether it overlaps with
        % sleep staging
        subDir = [dataPath dirList(iDir).name filesep];
        fileList = dir([subDir '*.mat']);
        loadData = myDataLoad([subDir fileList(1).name]); 
        isLoaded = 1; %To avoid reloading first file in loop below
        stageData = align_sleep_stage([dataPath sleepStageFile],loadData);
        % stageData.STAGE will have NaNs for time points not overlapping
        % with sleep scoring period.
        if sum(isnan(stageData.STAGE))<length(stageData.STAGE)
            % Then at least some data points in testData.dat overlap with
            % sleep staging period. data will hold all channels of data for
            % this recording epoch.
            data = struct;
            % Find index limits of sleep staging
            iStart = find(~isnan(stageData.STAGE),1,'first');
            iStop = find(~isnan(stageData.STAGE),1,'last');
            stageData.STAGE = stageData.STAGE(~isnan(stageData.STAGE));
            % resample data and save as single file
            [fsorig, fsres] = rat(loadData.fs/fsDS); % account for non-integer fs
            for iFile = 1:length(fileList)
                tempData = struct;
                if ~isLoaded
                    loadData = myDataLoad([subDir fileList(iFile).name]);
                end
                isLoaded = 0;
                % Restrict data to sleep staging period
                loadData.dat = loadData.dat(iStart:iStop);
                % Resample data
                tempData.dat = resample(double(loadData.dat),fsres,fsorig);
                tempData.fs = fsDS;
                % Extract field name for data and channel number from
                % individual channel file name
                nameTokPos = strfind(fileList(iFile).name,nameToken);
                dataName = fileList(iFile).name(1:nameTokPos-2);
                underscorePos = strfind(fileList(iFile).name,'_');
                tempData.chan = ...
                    str2num(fileList(iFile).name(strfind(fileList(iFile).name,chanToken)+4:underscorePos(1)-1));
                % Add these data to data structure
                data.(dataName) = tempData;
            end
            data.STAGE = stageData.STAGE(1:fsorig:end);
            data.STAGE_KEY = stageData.STAGE_KEY;
            save([subDir dirList(iDir).name '.mat'],'data','-v7.3');
        end
    end
end


