function segSet = assignTrialsToSegments(stageTrial,nTrialsPerSeg)

%Grab consecutive nTrrialsPerSeg trials with uniform sleep stage

diffVec = diff(stageTrial);
stageTransitions = find(diffVec ~= 0);
