function writeConcatSleepData(params,sleepDataPath,patientNum)
% Load & save ECoG data
% sleepDataPath = [dataPath thisName filesep 'Sleep data' filesep];

nChans = length(params.ECoGchannels);
signalName = 'LFPx';

% Get all data files
dataFiles = dir([sleepDataPath patientNum '*.mat']);
stageData = [];
tempSTAGE_KEY = [];
nPtsTotal = 0;
for jFile = 1:length(dataFiles)
    display(['Determining data size...' dataFiles(jFile).name]);
    fileObj = matfile([sleepDataPath dataFiles(jFile).name]);
    fileDetails = whos(fileObj);
    dataFields = {fileDetails.name};
    chanFields = dataFields(contains(dataFields,signalName));
    iChan = 1;
    thisData = load([sleepDataPath dataFiles(jFile).name],chanFields{iChan});
    nPtsTotal = nPtsTotal+length(thisData.(chanFields{iChan}).dat);
end
loadedData = zeros(nChans,nPtsTotal);
stageData = zeros(1,nPtsTotal);
iStart = 1;
for jFile = 1:length(dataFiles)
    display(['Loading ' dataFiles(jFile).name]);
    fileObj = matfile([sleepDataPath dataFiles(jFile).name]);
    fileDetails = whos(fileObj);
    dataFields = {fileDetails.name};
    if sum(contains(dataFields,'STAGE'))==2
        load([sleepDataPath dataFiles(jFile).name],dataFields{contains(dataFields,'STAGE')});
        stageData(iStart:iStart+length(STAGE)-1) = STAGE';
        tempSTAGE_KEY = [tempSTAGE_KEY STAGE_KEY'];
    end
    chanFields = dataFields(contains(dataFields,signalName));
    chanNum = zeros(1,length(chanFields));
    chanStr = {};
    for iChan = 1:length(chanFields)
        thisChan = chanFields{iChan};
        chanStr{iChan} = thisChan(strfind(thisChan,signalName)+length(signalName):strfind(thisChan,'_')-1);
        chanNum(iChan) = str2num(chanStr{iChan});
    end
    [chanNum,sortIndex] = sort(chanNum);
    chanStr = chanStr(sortIndex);
    chanFields = chanFields(sortIndex);
    for iChan = 1:nChans
        thisChan = params.ECoGchannels(iChan).chanNum;
        display(['Loading ch#' num2str(thisChan)]);
        dataToLoad = chanFields{chanNum==thisChan};
        thisData = load([sleepDataPath dataFiles(jFile).name],dataToLoad);
        if iChan == 1
            tempData = zeros(nChans,length(thisData.(dataToLoad).dat));
        end
        tempData(iChan,:) = thisData.(dataToLoad).dat;
        if iChan == 1 && jFile == 1
            params.dT = 1/thisData.(dataToLoad).fs; %sec
        end
        loadedData(:,iStart:iStart+length(thisData.(dataToLoad).dat)-1) = tempData;
    end
    iStart = iStart+length(thisData.(dataToLoad).dat);
end
STAGE_KEY = unique(tempSTAGE_KEY);
dT = params.dT;
chanPars = struct();
for iChan = 1:nChans
    chanPars(iChan).num = params.ECoGchannels(iChan).chanNum;
    chanPars(iChan).name = params.ECoGchannels(iChan).oldROI;
end
display(['Saving ' patientNum '_concatData.mat']);
save([sleepDataPath patientNum '_concatData.mat'],'loadedData','dT','chanPars','stageData','STAGE_KEY');

