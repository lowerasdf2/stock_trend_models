function sleepVal = getSleepVal(stage)

switch stage
    case 'W'
        sleepVal = 1;
    case 'N1'
        sleepVal = 2;
    case 'N2'
        sleepVal = 3;
    case 'N3'
        sleepVal = 4;
    case 'R'
        sleepVal = 5;
    otherwise
        sleepVal = NaN;
end