function [modOrd] = getMVARModelOrder(data_All,chanToProc)
% [modOrd] = getMVARModelOrder(data_ECoGds,chanToProc);
% data_All = data_ECoGds;
modelOrder = [4,8,12,16,20,24,28,32]; % sample points
nIters = 10;

nTrials = length(data_All.trial);
nPts = size(data_All.trial{1,1},2);
CV = zeros(nIters,length(modelOrder));
for iTer = 1:nIters
    % Partition data into training and testing sets.
    randTrials = randperm(nTrials);
    trainSet = randTrials(1:floor(end/2));
    testSet = randTrials(ceil(end/2):end);
    cfg         = [];
    cfg.trials  = trainSet;
    data_MVAR   = ft_redefinetrial(cfg, data_All);
    for iOrd = 1:length(modelOrder)
        % fit multivariate autoregressive model to data
        cfg         = [];
        cfg.order   = modelOrder(iOrd);
        cfg.method = 'bsmart';
        cfg.channel = chanToProc'; %Nx1 array
        mdata       = ft_mvaranalysis(cfg, data_MVAR);
        
        for iTrial = testSet
            data = data_All.trial{1,iTrial}(chanToProc,:);
            data_hat = zeros(size(data));
            data_hat(:,1:modelOrder(iOrd)) = data(:,1:modelOrder(iOrd));
            errSig = 0;
            for iTime = modelOrder(iOrd)+1:nPts
                for k=1:modelOrder(iOrd)
                    data_hat(:,iTime) = data_hat(:,iTime) + squeeze(mdata.coeffs(:,:,k))*data(:,iTime-k);
                end
                errSig = errSig + norm(data(:,iTime)-data_hat(:,iTime))^2;
            end
            CV(iTer,iOrd) = CV(iTer,iOrd) + errSig/(nPts-modelOrder(iOrd));
        end
    end
    CV(iTer,:) = CV(iTer,:)/length(testSet);
end
CV_mod = mean(CV,1);
modOrd = modelOrder(CV_mod == min(CV_mod));

% figure()
% plot(modelOrder,CV_mod,'o-')


