%% General parameters
% computer = 'Mac'; 
computer = 'desktopPC';
if strcmp(computer,'Mac')
    computerSpecPrefix = '/Users/bankslaptop/Box Sync/My documents/';
else
    computerSpecPrefix = 'D:\Box Sync\My documents\';
end

if ~exist('defaultPath','var')
    dataPath = [computerSpecPrefix 'Data and analysis' filesep 'ECoG data' filesep];
end
if ~exist('electrodeFilePath','var')
    electrodeFilePath = [computerSpecPrefix 'Data and analysis' filesep ...
        'ECoG data' filesep 'Electrode MNI coordinates and ROI' filesep];
end
if ~exist('outPath','var')
    outPath = [computerSpecPrefix 'Data and analysis' filesep 'ECoG data' filesep];
end

% Ensure output directory exists
if ~exist(outPath, 'dir')
    mkdir(outPath);
end
disp(['Data will be saved to `' outPath '`']);

if ~exist('batchParams', 'var')
    [batchParams,setName] = getBatchParams_sleep(electrodeFilePath);%,defaultPath);
end

timeFilePath = [computerSpecPrefix 'Data and analysis' filesep 'ECoG data' filesep];
timeFileName = 'ECoG_subj_sleepStartTimes.xlsx';
timeSheetName = 'subjData';

[timeDataNum, timeDataTxt, timeDataRaw] = xlsread([timeFilePath timeFileName],timeSheetName);

nSubj = size(timeDataRaw,1);
zeroTime = datenum('01-jan-1970 00:00:00'); %Unix time start
for iSubj = 1:nSubj
    dStr = timeDataRaw{iSubj,2};
    unixToD(iSubj) = datenum(dStr,'yyyy-mm-dd HH:MM:SS')-zeroTime; %sec
end

%% Write concatenated sleep data for Tononi crew
patientIDs = fieldnames(batchParams)';
for iPatient = 2:3%1:length(patientIDs)
    thisName = patientIDs{iPatient};
    params = batchParams.(thisName);
    disp('------------------------');
    disp(['Patient ' thisName]);
    disp('------------------------');
    patientNum = thisName(strfind(thisName,'patient')+length('patient'):end-1);
    sleepDataPath = [dataPath thisName filesep 'Sleep data' filesep];
    writeConcatSleepData(params,sleepDataPath,patientNum);
end
%% Read sleep data and extract features for Vaclav Kremen

% Freq analysis params
tinyCriterion = 0.02;
bandFreqs = [0.1,4;4,8;8,12;12,30;30,55;65,125];
nmlzFreqs = [0.1,25];
bands = {'delta relative','theta relative','alpha relative','beta relative','loGam relative','hiGam relative'};
dsFs = 250;
trialLength = 30;
trialOverlap = 0;
patientIDs = fieldnames(batchParams)';
for iPatient = 8%1:length(patientIDs)

    feature_struct = struct;
    % feature_struct will hold data for Vaclav Kremen's auto sleep scoring
    % algorithm.
    % feature_struct.channels - 
    %   Nx1 cell, holds name of electrodes used (e.g. Frontal_Grid_01.mef). 
    %   It doesn�t really matter. N is number of electrodes.
    % feature_struct.Fs - 
    %   double, holds sampling frequency of data
    % feature_struct.lead_sz_start_times - 
    %   Yx1 double, uUTC times of start times of lead seizures (Y is number of seizures)
    % feature_struct.sz_start_times - 
    %   Zx1 double, uUTC times of start times of 
    %   non-lead seizures (Z is number of non-lead seizures)
    % feature_struct.clip_labels - 
    %   Kx1 - labels of each epoch, can be use for selecting different data based 
    %   on SNR, differentiating artifacts or similar. Do not need to be filled in. 
    %   Can be score (0, 1, 2, etc.). K is number of 30 seconds epochs.
    % feature_struct.time_stamps - 
    %   Kx1 - double, is uUTC time stamps of start of each epoch. It has K number 
    %   of 30 seconds epochs. Each row contains uUTC time of start of each scoring 
    %   epoch in msec (15digits) - https://www.epochconverter.com/
    % feature_struct.channel_features - 
    %   KxNxL - double, is matrix that hold L number of features calculated on 
    %   each electrode from number of N electrodes for each 30 second epochs (total K epochs).
    % feature_struct.features_names - 
    %   Lx1 cell - strings with names of features extracted from data, e.g.
    %   'Delta Relative' (0.5-4Hz)
    %   'Theta Relative' (4-8Hz)
    %   'Alpha Relative' (8-12Hz)
    %   'Beta Relative' (12-30Hz)
    %   '30-55Hz Relative'
    %   '65-115 Hz Relative'
    %   '125-175 Hz Relative'
    %   '185-235 Hz Relative'
    % feature_struct.local_time_zone - 
    %   string, specify local time zone where data was measured. If not specified 
    %   it takes it as UTC. Please use Matlab local time zone strings: 
    %   https://www.mathworks.com/help/matlab/ref/timezones.html, e.g. 'America/New_York'.
    %
    % We want to extract relative power in given frequency band (PIB) in each 
    % 30 second epoch for each electrode. 

    feature_struct.features_names = bands';
    feature_struct.band_freqs = bandFreqs;
    feature_struct.nmlz_band = nmlzFreqs;
    feature_struct.local_time_zone = 'America/Chicago';
    tempStageData = [];
    tempStageKey = [];
    sleepData = struct;
    thisName = patientIDs{iPatient};
    sleepDataPath = [dataPath thisName filesep 'Sleep data' filesep];
    params = batchParams.(thisName);
    iPos = strfind(thisName,'patient')+length('patient');
    patientNum = thisName(iPos:iPos+2);
    dataFiles = dir([sleepDataPath patientNum '*.mat']);
    firstFile = dataFiles(1).name;
    underscorePos = strfind(firstFile,'_');
    patientBlock = [firstFile(1:underscorePos(1)-1) '-' firstFile(underscorePos(1)+1:underscorePos(2)-1)];
    timeOfDay = unixToD(contains(timeDataRaw(:,1),patientBlock)); %sec
    tOffset = timeOfDay; %sec
    counter = 0;
    Fs = [];
    for jFile = 1:length(dataFiles)
        fileName = [sleepDataPath dataFiles(jFile).name];
        [loadedData, STAGE, STAGE_KEY, params] = loadECoGData_eachSleepFile(params,fileName);
        Fs(jFile) = 1/params.dT;
        tempStageData = [tempStageData STAGE];
        tempStageKey = [tempStageKey STAGE_KEY];
        % loadedData is matrix of nChan x nSamples
        [params.chanSide, params.chanROI, params.chanNum] = ...
            getChannelInfo_sleepScoring(params);
        [data_ECoG] = convertToFTFormat(loadedData,params);
        %%%%%%%%%%%% NOTE %%%%%%%%%%%%%%%%
        % Need to add EEG etc channels into Excel spreadsheet, and change
        % manner in which data.label is created in convertToFTFormat.
        if jFile == 1
            feature_struct.channels = data_ECoG.label';
        end
        if Fs(jFile) > 400
            % downsample the data to speed up component analysis
            cfg = [];
            cfg.resamplefs = dsFs;
            cfg.detrend    = 'yes';
            % the following line avoids numeric round off issues in the time axes upon resampling
            data_ECoGds = ft_resampledata(cfg, data_ECoG);
            data_ECoGds.sampleinfo = [1, size(data_ECoGds.trial{1,1},2)];
            params.dT = 1/dsFs;
            Fs(jFile) = dsFs;
        else
            data_ECoGds = data_ECoG;
        end
        clear data_ECoG
        % segment data into trials of length trialLength with overlap
        cfg         = [];
        cfg.length  = trialLength;
        cfg.overlap = trialOverlap;
        data_ECoGds   = ft_redefinetrial(cfg, data_ECoGds);

        for iSegment = 1:length(data_ECoGds.trial)
            disp(['Patient ' thisName ' file ' dataFiles(jFile).name ' segment ' num2str(iSegment) ' of ' num2str(length(data_ECoGds.trial))]);
            counter = counter + 1;            
            seg_dat = [];
            seg_dat.trial = data_ECoGds.trial(iSegment);
            seg_dat.time = data_ECoGds.time(iSegment);
            seg_dat.fsample = data_ECoGds.fsample;
            seg_dat.label = data_ECoGds.label;
            seg_dat.sampleinfo = data_ECoGds.sampleinfo(iSegment,:);
                        
            cfg            = [];
            cfg.method     = 'mtmfft';
            cfg.output     = 'pow';
            cfg.taper      = 'dpss';
            cfg.tapsmofrq  = 1;
            cfg.pad        = 'nextpow2';
            cfg.foi        = 0:0.25:Fs(jFile)/2;
            cfg.keeptrials = 'no';
            cfg.feedback   = 'no';
            POW_ECoG       = ft_freqanalysis(cfg, seg_dat);
            
            feature_struct.time_stamps(counter) = 1000*(seg_dat.time{1}(1) + tOffset); %msec            
            for iBand = 1:length(bands)
                freqIndex = POW_ECoG.freq>=bandFreqs(iBand,1) & POW_ECoG.freq<bandFreqs(iBand,2);
                nmlzIndex = POW_ECoG.freq>=nmlzFreqs(1) & POW_ECoG.freq<nmlzFreqs(2);
                feature_struct.channel_features(counter,:,iBand)...
                    = mean(POW_ECoG.powspctrm(:,freqIndex),2)./mean(POW_ECoG.powspctrm(:,nmlzIndex),2);
            end
        end % Loop over segments
        tOffset = feature_struct.time_stamps(counter)/1000 + trialLength; %sec
    end % Loop over files
    if length(unique(Fs)) == 1
        feature_struct.Fs = Fs(1);
    else
        feature_struct.Fs = Fs;
    end
    params.STAGE_KEY = unique(tempStageKey);
    sleepData.stage = tempStageData;
    sleepData.key = unique(tempStageKey);
    sleepData.time = ((0:length(tempStageData)-1)*params.dT+timeOfDay)*1000;
    save([sleepDataPath 'sleepData_' patientNum '.mat'],'feature_struct','sleepData');
end %loop over patients

% dataFiles = dir([dataPath patientNum '*.mat']);
% loadedData = [];
% dataTok{1} = 'LFPx';
% dataTok{2} = '_li';
% for iFile = 1:3
%     for iChan = 1:length(chansToUse)
%         thisChan = chansToUse(iChan);
%         chanDataName = [dataTok{1} num2str(thisChan) dataTok{2} num2str(thisChan)];
%         % display(['Loading ' chanDataName]);
%         tempData = load([dataFiles(iFile).name], chanDataName);
%         if iChan == 1
%             tempLoadedData = zeros(length(chansToUse),length(tempData.(chanDataName).dat));
%             dT = 1/tempData.(chanDataName).fs(1); %sec
%         end
%         tempLoadedData(iChan,:) = tempData.(chanDataName).dat;
%     end
%     loadedData = [loadedData tempLoadedData];
% end
% 
% save('concatData.mat','loadedData','dT','chansToUse');
%% Read in just sleep stage data and ad that info to extracted features for Vaclav Kremen

signalName = 'LFPx';
patientIDs = fieldnames(batchParams)';
for iPatient = 1:length(patientIDs) %iPatient = length(patientIDs)
    tempStageData = [];
    tempStageKey = [];
    sleepData = struct;
    thisName = patientIDs{iPatient};
    sleepDataPath = [dataPath thisName filesep 'Sleep data' filesep];
    params = batchParams.(thisName);
    iPos = strfind(thisName,'patient')+length('patient');
    patientNum = thisName(iPos:iPos+2);
    dataFiles = dir([sleepDataPath patientNum '*.mat']);
    firstFile = dataFiles(1).name;
    underscorePos = strfind(firstFile,'_');
    patientBlock = [firstFile(1:underscorePos(1)-1) '-' firstFile(underscorePos(1)+1:underscorePos(2)-1)];
    timeOfDay = unixToD(contains(timeDataRaw(:,1),patientBlock)); %sec
    tOffset = timeOfDay; %sec
    load([sleepDataPath 'sleepData_' patientNum '.mat'],'feature_struct');
    for jFile = 1:length(dataFiles)
        fileName = [sleepDataPath dataFiles(jFile).name];
        [STAGE, STAGE_KEY] = loadECoGData_justStageData(fileName);
        tempStageData = [tempStageData STAGE];
        tempStageKey = [tempStageKey STAGE_KEY];
        if jFile == 1
            fileObj = matfile(fileName);
            fileDetails = whos(fileObj);
            dataFields = {fileDetails.name};
            chanFields = dataFields(contains(dataFields,signalName));
            dataToLoad = chanFields{1};
            tempData = load(fileName,dataToLoad);
            params.dT = 1/tempData.(dataToLoad).fs; %sec
        end
    end
    %Get dT from one data file.
    params.STAGE_KEY = unique(tempStageKey);
    sleepData.stage = tempStageData;
    sleepData.key = unique(tempStageKey);
    sleepData.time = ((0:length(tempStageData)-1)*params.dT+timeOfDay)*1000;
    save([sleepDataPath 'sleepData_' patientNum '.mat'],'feature_struct','sleepData');
end %loop over patients
