function [loadedData, stageData, params] = loadECoGData_sleep(params,dataPath)
%loadECoGData  Load and concatenate ECoG data from Howard lab.
%
% Returns an array of filtered data organized as follows:
%   (channel) x (sample)
%
% params.sleepData.stage
% params.sleepData.time
% params.sleepData.key

nChan = length(params.ECoGchannels);

%% Load ECoG data
% Get all data files
dataFiles = dir([dataPath '*.mat']);
loadedData = [];
stageData = [];
for jFile = 1:length(dataFiles)
    display(['Loading ' dataFiles(jFile).name]);
    load([dataPath dataFiles(jFile).name],'data');
    fields = fieldnames(data);
    fields = fields(contains(fields,'LFPx'));
    chan = zeros(1,length(fields));
    for iChan = 1:length(fields)
        chan(iChan) = data.(fields{iChan}).chan;
    end
    for iChan = 1:nChan
        thisChan = params.ECoGchannels(iChan).chanNum;
        chanIndx = find(chan==thisChan);
        if iChan == 1
            nPts = length(data.(fields{chanIndx}).dat);
            tempData = zeros(nChan,nPts);
        end
        tempData(iChan,:) = data.(fields{chanIndx}).dat;
        if iChan == 1 && jFile == 1
            params.dT = 1/data.(fields{chanIndx}).fs; %sec
        end
    end
    loadedData = [loadedData tempData];
    stageData = [stageData data.STAGE'];
end
params.nPts = size(loadedData,2);
params.STAGE_KEY = data.STAGE_KEY;
