function [corrP] = my_fdrCorrect(tempP,pCrit)

tempP = tempP(:);
tempP = (tempP(~isnan(tempP)));
fdrVals = mafdr(tempP,'BHFDR','true');
[tempSort,sortIndx] = sortrows(fdrVals);
critIndx = find(tempSort<pCrit,1,'last');
corrP = tempP(sortIndx(critIndx));
