nChans = length(chanToProc);
nPts = size(data_ECoGds.trial{1,1},2);
sigma = mdata.noisecov;
R = chol(sigma);
data_hat = (randn(nPts,nChans)*R)';
for iTime = modelOrder+1:nPts
    for k=1:modelOrder
        data_hat(:,iTime) = data_hat(:,iTime) + squeeze(mdata.coeffs(:,:,k))*data_hat(:,iTime-k);
    end
end

figure()
for iChan = 1:nChans
    subplot(nChans,1,iChan)
    plot(1:nPts,data_ECoGds.trial{1,1}(chanToProc(iChan),:))
    hold on
    plot(1:nPts,data_hat(iChan,:))
end
