function testGrangerAnalysis_LGD()
%% General parameters
% computer = 'Mac'; % 'desktopPC';
computer = 'desktopPC';
if strcmp(computer,'Mac')
    computerSpecPrefix = '/Users/bankslaptop/Box Sync/My documents/';
else
    computerSpecPrefix = 'D:\Box Sync\My documents\';
end

if ~exist('defaultPath','var')
    defaultPath = [computerSpecPrefix 'Data and analysis' filesep 'ECoG data' filesep];
end
if ~exist('electrodeFilePath','var')
    electrodeFilePath = [computerSpecPrefix 'Data and analysis' filesep ...
        'ECoG data' filesep 'Electrode MNI coordinates and ROI' filesep];
end
if ~exist('outPath','var')
    outPath = [computerSpecPrefix 'Data and analysis' filesep 'ECoG data' filesep];
end

% Ensure output directory exists
if ~exist(outPath, 'dir')
    mkdir(outPath);
end
disp(['Data will be saved to `' outPath '`']);

if exist([outPath 'ECoG_conn_LGD.mat'],'file')
    load([outPath 'ECoG_conn_LGD.mat'],'ECoG_conn_LGD','batchParams');
else
    ECoG_conn_LGD = struct;
    if ~exist('batchParams', 'var')
        batchParams = getBatchParams_LGD(electrodeFilePath,defaultPath);
    end
end

% Connectivity analysis parameters
calcFlag.mvarGranger = 0;
calcFlag.fourierGranger = 0;
calcFlag.pdc = 0;
calcFlag.dtf = 1;
dsFs = 240;
trialToPlot = -1;
pgcYLims = [0,10];
dtfYLims = [0,10];
mGYLims = [0,10];
bandFreqs = [4,8,13,30,80];

% %% Freq analysis params
freqsOfInterest = 2:80; %Hz
freqTicks = [3,10,30];
specYLims = [1.e-3, 1.e3];
nmlzSpecYLims = [1.e-5, 1.e1];
epochLength = 1.4; %seconds
refChans = {'HGPM'};
compChans = {'PT','STGP','IFGop'};
% freqSmoothing = [2,2,2,8,10,20]; %delta, theta, alpha, beta, gamma, high gamma
bands = {'delta','theta','alpha','beta','gamma','highGamma'};
trialSetLabel = {'loclStd','loclDev','globStd','globDev'};


%% Main analysis loop
patientIDs = fieldnames(batchParams)';
for iPatient = 1:length(patientIDs)
    name = patientIDs{iPatient};
    params = batchParams.(patientIDs{iPatient});
    conditions = fieldnames(params)';
    for iCond =1:length(conditions)
        cond = conditions{iCond};
        disp('------------------------');
        disp(['Condition: ' cond]);
        disp('------------------------');
        [loadedData,params.(cond)] = loadECoGData_LGD(params.(cond));
        % loadedData is matrix of nChan x nSamples
        [params.(cond).chanSide, params.(cond).chanROI, params.(cond).chanNum,...
            params.(cond).sortOrder] = getChannelInfo(params.(cond));
        [data_ECoG] = convertToFTFormat(loadedData,params.(cond));
        % Note: data_ECoG is now sorted according to sortOrder.
        % params....ECoGChannel is not sorted.

        % downsample the data to speed up component analysis
        cfg = [];
        cfg.resamplefs = dsFs;
        cfg.detrend    = 'yes';
        % the following line avoids numeric round off issues in the time axes upon resampling
        data_ECoGds = ft_resampledata(cfg, data_ECoG);
        data_ECoGds.sampleinfo = [1, size(data_ECoGds.trial{1,1},2)];
        clear data_ECoG

        % segment data into trials of length trialLength
        dsdT = 1/dsFs;
        trialStart = round(params.(cond).trialTimes./dsdT)';
        trialStop = round((params.(cond).trialTimes + epochLength)./dsdT)';
        trialStop = trialStop(trialStop<=size(loadedData,2));
        trialStart = trialStart(1:length(trialStop));
        trialOffset = zeros(length(trialStop),1); %trial onset corresponds to stim onset
        cfg         = [];
        cfg.trl = [trialStart trialStop trialOffset];
        data_ECoGds   = ft_redefinetrial(cfg, data_ECoGds);
                
        % select subset of channels for model fit
        if iCond == 1
            chanToProc = [];
            chans = params.(cond).exempChans;
            for iChan = 1:length(chans.nums)
                thisIndex = params.(cond).chanNum == chans.nums(iChan);
                chanToProc(iChan) = params.(cond).sortOrder(thisIndex);
            end
            nChans = length(chanToProc);
        end
        
        trialSet(1).trialNums = params.(cond).loclStd;
        trialSet(2).trialNums = params.(cond).loclDev;
        trialSet(3).trialNums = params.(cond).globStd;
        trialSet(4).trialNums = params.(cond).globDev;
        
        for iTrialSet = 1:length(trialSetLabel)
            if calcFlag.fourierGranger || calcFlag.pdc || calcFlag.dtf
                cfg           = [];
                cfg.output    = 'powandcsd';
                cfg.method    = 'mtmfft';
                cfg.taper     = 'dpss';
                cfg.tapsmofrq = 2;
                cfg.pad       = 'nextpow2';
                cfg.channel   = chanToProc'; %Nx1 array
                cfg.trials    = trialSet(iTrialSet).trialNums;
                cfg.trials    = cfg.trials(cfg.trials<=length(data_ECoGds.trial));
                freq = ft_freqanalysis(cfg, data_ECoGds);

                if calcFlag.fourierGranger
                    cfg                     = [];
                    cfg.method              = 'granger';
                    cfg.granger             = [];
                    cfg.granger.conditional = 'yes';
                    cfg.granger.sfmethod    = 'multivariate';
                    
                    
                    
                    
                    ECoG_conn_LGD.fGranger.(name).(cond).(trialSetLabel{iTrialSet}) = ft_connectivityanalysis(cfg, freq);
                end

                if calcFlag.pdc
                    % Compute Partial Directed Coherence spectra
                    cfg           = [];
                    cfg.method    = 'pdc';
                    ECoG_conn_LGD.pdc.(name).(cond).(trialSetLabel{iTrialSet}) = ft_connectivityanalysis(cfg, freq);
                end

                if calcFlag.dtf
                    % Compute Directed Transfer Function spectra
                    cfg           = [];
                    cfg.method    = 'dtf';
                    ECoG_conn_LGD.dtf.(name).(cond).(trialSetLabel{iTrialSet}) = ft_connectivityanalysis(cfg, freq);
                end            
            elseif calcFlag.mvarGranger
                % Model fit
                if iTrialSet == 1
                    % Identify optimal model order
                    [modOrd] = getMVARModelOrder(data_ECoGds,chanToProc);
                    % modOrd = 16;
                end
                
                % fit multivariate autoregressive model to data
                cfg         = [];
                cfg.order   = modOrd;
                cfg.toolbox = 'bsmart';
                cfg.channel = chanToProc'; %Nx1 array
                cfg.keeptrials = 'yes';
                cfg.trials = trialSet(iTrialSet).trialNums;
                cfg.trials = cfg.trials(cfg.trials<=length(data_ECoGds.trial));
                mdata       = ft_mvaranalysis(cfg, data_ECoGds);
                
                %Compute spectral transfer function
                cfg        = [];
                cfg.method = 'mvar';
                cfg.keeptrials = 'yes';
                mfreq      = ft_freqanalysis(cfg, mdata);
                
                % Compute Granger spectra
                cfg           = [];
                cfg.method    = 'granger';
                cfg.granger.conditional = 'yes';
                ECoG_conn_LGD.mGranger.(name).(cond).(trialSetLabel{iTrialSet}) = ft_connectivityanalysis(cfg, mfreq);
            end % mvar Granger
        end
    end
    batchParams.(patientIDs{iPatient}) = params;
end
save([outPath 'ECoG_conn_LGD.mat'],'ECoG_conn_LGD','batchParams');

%% Plot data loop
lineColors = {'b','r','g','m'};
lineWidths = [1,2];
patientIDs = fieldnames(batchParams)';
for iPatient = 1:length(patientIDs)
    name = patientIDs{iPatient};
    params = batchParams.(patientIDs{iPatient});
    conditions = fieldnames(params)';
    for iTrialSet = 1:2:length(trialSetLabel)
        for iCond =1:length(conditions)
            cond = conditions{iCond};
            chans = params.(cond).exempChans;
            nChans = length(chans.ROIs);

            if calcFlag.fourierGranger
                fGrangerStd = ECoG_conn_LGD.fGranger.(name).(cond).(trialSetLabel{iTrialSet});
                fGrangerDev = ECoG_conn_LGD.fGranger.(name).(cond).(trialSetLabel{iTrialSet+1});
                if iCond == 1
                    fGrangerFig = figure('Name', [name ' - ' trialSetLabel{iTrialSet+1} ...
                        ' vs ' trialSetLabel{iTrialSet} ' - fGranger spectra']);
                    rowVec = zeros(1,size(fGrangerStd.labelcmb,2));
                    colVec = zeros(1,size(fGrangerStd.labelcmb,2));
                    for iChan = 1:nChans
                        rowVec(contains(fGrangerStd.labelcmb(:,1),chans.ROIs{iChan})) = iChan;
                        colVec(contains(fGrangerStd.labelcmb(:,2),chans.ROIs{iChan})) = iChan;
                    end
                    nVec = length(rowVec);
                    % The following two lines are just to get blank subplots
                    % plotted in figure below
                    rowVec = [rowVec 1:nChans];
                    colVec = [colVec 1:nChans];
                    p_fG = zeros(1,length(conditions));
                end
                % Row influences column
                figure(fGrangerFig);
                for iPair = 1: size(fGrangerStd.labelcmb,1)
                    iRow=rowVec(iPair);
                    iCol=colVec(iPair);
                    subplot(nChans,nChans,(iRow-1)*nChans+iCol);
                    if iRow ~= iCol
                        hold on
                        ratioToPlot = fGrangerDev.grangerspctrm(iPair,:)./fGrangerStd.grangerspctrm(iPair,:);
                        if iRow == 1 && iCol == nChans
                            p_fG(iCond) = plot(fGrangerStd.freq, ratioToPlot,'LineWidth',2);
                        else
                            plot(fGrangerStd.freq, ratioToPlot,'LineWidth',2);
                        end
                        for iBand = 1:length(bandFreqs)
                            plot([bandFreqs(iBand),bandFreqs(iBand)],dtfYLims,':k');
                        end
                        ax = gca;
                        ax.YLim = fGYLims;
                        ax.XScale = 'log';
                        ax.XLim = [1,dsFs/2];
                        if iCol == nChans
                            text(1.2*dsFs/2,0.8*mean(fGYLims),chans.ROIs{iRow},'Interpreter','none');
                        end
                        if iRow == 1
                            text(0.9,1.15*fGYLims(2),chans.ROIs{iCol},'Interpreter','none');
                        end
                        if ~(iCol == 1 && iRow == nChans)
                            ax.XTickLabel = {};
                            ax.YTickLabel = {};
                        end
                    end
                end
                if iCond == length(conditions)
                    legend(p_fG,conditions);
                end
            end

            if calcFlag.pdc
                pdcStd = ECoG_conn_LGD.pdc.(name).(cond).(trialSetLabel{iTrialSet});
                pdcDev = ECoG_conn_LGD.pdc.(name).(cond).(trialSetLabel{iTrialSet+1});
                if iCond == 1
                    pdcFig = figure('Name', [name ' - ' trialSetLabel{iTrialSet+1} ...
                        ' vs ' trialSetLabel{iTrialSet} ' - PDC spectra']);
                    p_pdc = zeros(1,length(conditions));
                end
                % Row influences column
                figure(pdcFig);
                for iRow=1:nChans
                    for iCol=1:nChans
                        subplot(nChans,nChans,(iRow-1)*nChans+iCol);
                        hold on
                        ratioToPlot = squeeze(pdcDev.dtfspctrm(iRow,iCol,:)./pdcStd.dtfspctrm(iRow,iCol,:));
                        if iRow ~= iCol
                            if iRow == 1 && iCol == nChans
                                p_pdc(iCond) = plot(pdcStd.freq, ratioToPlot,'LineWidth',2);
                            else
                                plot(pdcStd.freq, ratioToPlot,'LineWidth',2);
                            end
                            for iBand = 1:length(bandFreqs)
                                plot([bandFreqs(iBand),bandFreqs(iBand)],pdcYLims,':k');
    %                             set(get(get(h,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
                            end
                        end
                        if iCol == nChans
                            text(1.2*dsFs/2,0.8*mean(pdcYLims),chans.ROIs{iRow},'Interpreter','none');
                        end
                        if iRow == 1
                            text(0.9,1.15*pdcYLims(2),chans.ROIs{iCol},'Interpreter','none');
                        end
                        ax = gca;
                        ax.YLim = pdcYLims;
                        ax.YTick = [0,0.2,0.4,0.6];
                        ax.XScale = 'log';
                        ax.XLim = [1,dsFs/2];
                        ax.XTick = [1,10,100];
                        if ~(iCol == 1 && iRow == nChans)
                            ax.XTickLabel = {};
                            ax.YTickLabel = {};
                        else
                            ax.YLabel.String = 'PDC';
                            ax.XLabel.String = 'Freq (Hz)';
                        end
                    end
                end
                if iCond == length(conditions)
                    legend(p_pdc,conditions);
                end
            end

            if calcFlag.dtf
                dtfStd = ECoG_conn_LGD.dtf.(name).(cond).(trialSetLabel{iTrialSet});
                dtfDev = ECoG_conn_LGD.dtf.(name).(cond).(trialSetLabel{iTrialSet+1});
                if iCond == 1
                    dtfFig = figure('Name', [name ' - ' trialSetLabel{iTrialSet+1} ...
                        ' vs ' trialSetLabel{iTrialSet} ' - DTF spectra']);
                    p_dtf = zeros(1,length(conditions));
                end
                % Row influences column
                figure(dtfFig);
                for iRow=1:nChans
                    for iCol=1:nChans
                        subplot(nChans,nChans,(iRow-1)*nChans+iCol);
                        hold on
                        ratioToPlot = squeeze(dtfDev.dtfspctrm(iRow,iCol,:)./dtfStd.dtfspctrm(iRow,iCol,:));
                        if iRow ~= iCol
                            if iRow == 1 && iCol == nChans
                                p_dtf(iCond) = plot(dtfStd.freq, ratioToPlot,'LineWidth',2);
                            else
                                plot(dtfStd.freq, ratioToPlot,'LineWidth',2);
                            end
                            for iBand = 1:length(bandFreqs)
                                plot([bandFreqs(iBand),bandFreqs(iBand)],dtfYLims,':k');
    %                             set(get(get(h,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
                            end
                        end
                        if iCol == nChans
                            text(1.2*dsFs/2,0.8*mean(dtfYLims),chans.ROIs{iRow},'Interpreter','none');
                        end
                        if iRow == 1
                            text(0.9,1.15*dtfYLims(2),chans.ROIs{iCol},'Interpreter','none');
                        end
                        ax = gca;
                        ax.YLim = dtfYLims;
                        ax.YTick = [0,0.2,0.4,0.6];
                        ax.XScale = 'log';
                        ax.XLim = [1,dsFs/2];
                        ax.XTick = [1,10,100];
                        if ~(iCol == 1 && iRow == nChans)
                            ax.XTickLabel = {};
                            ax.YTickLabel = {};
                        else
                            ax.YLabel.String = 'DTF';
                            ax.XLabel.String = 'Freq (Hz)';
                        end
                    end
                    if iRow == 1 && iCol == nChans && iCond == length(conditions)
                        legend(p_dtf,conditions,'Interpreter','none');
                    end
                end
            end % dtf loop

            if calcFlag.mvarGranger
                mGranger = ECoG_conn_LGD.mGranger.(name).(cond);          
                if iCond == 1
                    mGrangerFig = figure('Name', [name ' - mvar Granger spectra']);
                    p_mG = zeros(1,length(conditions));
                end
                % Row influences column
                figure(mGrangerFig);
                for iRow=1:nChans
                    for iCol=1:nChans
                        subplot(nChans,nChans,(iRow-1)*nChans+iCol);
                        hold on
                        if iRow ~= iCol
                            if iRow == 1 && iCol == nChans
                                p_mG(iCond) = plot(mGranger.freq, squeeze(mGranger.grangerspctrm(iRow,iCol,:)),'LineWidth',2);
                            else
                                plot(mGranger.freq, squeeze(mGranger.grangerspctrm(iRow,iCol,:)),'LineWidth',2);
                            end
                            for iBand = 1:length(bandFreqs)
                                plot([bandFreqs(iBand),bandFreqs(iBand)],dtfYLims,':k');
                            end
                        end
                        if iCol == nChans
                            text(1.2*dsFs/2,0.8*mean(mGYLims),chans.ROIs{iRow},'Interpreter','none');
                        end
                        if iRow == 1
                            text(0.9,1.15*mGYLims(2),chans.ROIs{iCol},'Interpreter','none');
                        end
                        ax = gca;
                        ax.YLim = mGYLims;
                        ax.YTick = [0,0.2,0.4,0.6];
                        ax.XScale = 'log';
                        ax.XLim = [1,dsFs/2];
                        ax.XTick = [1,10,100];
                        if ~(iCol == 1 && iRow == nChans)
                            ax.XTickLabel = {};
                            ax.YTickLabel = {};
                        else
                            ax.YLabel.String = 'mGranger';
                            ax.XLabel.String = 'Freq (Hz)';
                        end
                    end
                    if iRow == 1 && iCol == nChans && iCond == length(conditions)
                        legend(p_mG,conditions);
                    end
                end
            end % mvar Granger loop         
        end % loop over conditions
        if calcFlag.dtf
            saveas(dtfFig,[outPath name ' - ' trialSetLabel{iTrialSet} ' - DTF spectra']);
        end
        if calcFlag.pdc
            saveas(pdcFig,[outPath name ' - ' trialSetLabel{iTrialSet} ' - PDC spectra']);
        end
        if calcFlag.fourierGranger
            saveas(fGrangerFig,[outPath name ' - ' trialSetLabel{iTrialSet} ' - fGranger spectra']);
        end
        if calcFlag.mvarGranger
            saveas(mGrangerFig,[outPath name ' - ' trialSetLabel{iTrialSet} ' - mGranger spectra']);
        end
    end %Local versus global
end % loop over patients
