function convertECoGToTable()
%% ECoG_pow
computer = 'desktopPC';
% computer = 'Mac'; 
% if strcmp(computer,'Mac')
computerSpecPrefix = '\\Banksdesktop\d/Box Sync/My documents/';
% else
%     computerSpecPrefix = 'D:\Box Sync\My documents\';
% end
if ~exist('electrodeFilePath','var')
    electrodeFilePath = [computerSpecPrefix 'Data and analysis' filesep ...
        'ECoG data' filesep 'Electrode MNI coordinates and ROI' filesep];
end

if ~exist('dataPath','var')
    dataPath = [computerSpecPrefix 'Data and analysis' filesep 'ECoG data' filesep];
end
if ~exist('outPath','var')
    outPath = [computerSpecPrefix 'Data and analysis' filesep 'ECoG data' filesep];
end

[~,setName] = getBatchParams(electrodeFilePath);%,defaultPath);
if exist([outPath 'ECoG_pow_' setName '.mat'],'file')
    load([outPath 'ECoG_pow_' setName '.mat'],'ECoG_pow','batchParams');
    patientIDs = fieldnames(batchParams);
else
    error('ECoG_pow.mat does not exist!');
end

% %% Freq analysis params
freqsOfInterest = 1:120; %Hz
bandFreqs = [1,4,8,13,30,80];
bands = {'delta','theta','alpha','beta','gamma'};
level.Core = {'HGPM'};
level.NonCore = {'HGAL','PT','PP','STG'};
level.AudRel = {'Aud-rel','Auditory-related'};
level.PFC = {'PFC'};
level.Other = {'Thalamus','Other','SensMot','Sensorimotor'};
levelNames = fieldnames(level);

cellData = {};
thisRow = 0;
patientIDs = fieldnames(batchParams)';
for iPatient = 1:length(patientIDs)
    thisName = patientIDs{iPatient};
    params = batchParams.(thisName);
    ECoGchan = params.ECoGchannels;
    nChans = length([ECoGchan.chanNum]);
    conditions = params.newCond;
    for iChan = 1:nChans
        noMatch = 1;
        iLev = 1;
        thisLevel = {};
        while iLev<=length(levelNames) && noMatch
            roiIndex = find(strcmp(level.(levelNames{iLev}),params.chanROI{iChan}));
            if ~isempty(roiIndex)
                thisLevel = levelNames(iLev);
                noMatch = 0;
            else
                iLev = iLev+1;
            end
        end
        if isempty(thisLevel)
            error(['No level match for ch#' num2str(ECoGchan(iChan).chanNum) ' in ' thisName]);
        end
        for iCond = 1:length(conditions)
            thisCond = conditions{iCond};
            OAASVal = getOAASVal(thisCond);
            if iCond == 1
                brainState = 'predrg';
                sumPow = sum(ECoG_pow.(thisName).(thisCond).powspctrm(iChan,:));
            elseif OAASVal>=3
                brainState = 'sedate';
            else
                brainState = 'unresp';
            end
            for iBand = 1:length(bands)                
                thisBand = bands{iBand};
                thisRow = thisRow+1;
                freqIndex = ECoG_pow.(thisName).(thisCond).freq>=bandFreqs(iBand)...
                    & ECoG_pow.(thisName).(thisCond).freq<bandFreqs(iBand+1);
                mnVal = ...
                    mean(ECoG_pow.(thisName).(thisCond).powspctrm(iChan,freqIndex));
                mnNmlz = ...
                    mean(ECoG_pow.(thisName).(thisCond).powspctrm(iChan,freqIndex))/sumPow;
                cellData{thisRow,1}  = thisName;
                cellData{thisRow,2}  = params.chanNum(iChan);
                cellData{thisRow,3}  = ECoGchan(iChan).AnatReg;
                cellData{thisRow,4}  = params.chanROI{iChan};
                cellData{thisRow,5}  = params.chanSide{iChan};
                cellData{thisRow,6}  = thisLevel;
                cellData{thisRow,7}  = ECoGchan(iChan).xCoord;
                cellData{thisRow,8}  = ECoGchan(iChan).yCoord;
                cellData{thisRow,9}  = ECoGchan(iChan).zCoord;
                cellData{thisRow,10} = OAASVal;
                cellData{thisRow,11} = brainState;
                cellData{thisRow,12} = thisBand;
                cellData{thisRow,13} = mnVal;
                cellData{thisRow,14} = mnNmlz;
            end
        end
    end
end
varNames = {'subj','chan','anatReg','ROI','side','hierLev','xCoord','yCoord',...
    'zCoord','OAAS','state','band','pow','nmlzPow'};
specPowTable = cell2table(cellData,'VariableNames',varNames);
    
save([outPath 'ECoG_specPowTable.mat'],'specPowTable');

%% ECoG wPLI
computer = 'desktopPC';
% computer = 'Mac'; 
if strcmp(computer,'Mac')
    computerSpecPrefix = '/Users/bankslaptop/Box Sync/My documents/';
else
    computerSpecPrefix = 'D:\Box Sync\My documents\';
end
if ~exist('electrodeFilePath','var')
    electrodeFilePath = [computerSpecPrefix 'Data and analysis' filesep ...
        'ECoG data' filesep 'Electrode MNI coordinates and ROI' filesep];
end

if ~exist('dataPath','var')
    dataPath = [computerSpecPrefix 'Data and analysis' filesep 'ECoG data' filesep];
end
if ~exist('outPath','var')
    outPath = [computerSpecPrefix 'Data and analysis' filesep 'ECoG data' filesep];
end

[~,setName] = getBatchParams(electrodeFilePath);%,defaultPath);
if exist([outPath 'ECoG_conn_' setName '.mat'],'file')
    load([outPath 'ECoG_conn_' setName '.mat'],'ECoG_conn','batchParams');
    if ~isfield(ECoG_conn,'WPLI')
        error(['wPLI not a field in ECoG_conn_' setName '.mat!']);
    end
else
    error(['ECoG_conn_' setName '.mat does not exist!']);
end

% %% Freq analysis params
tinyCriterion = 0.01;
freqsOfInterest = [2,6,10,20,40,80]; %delta, theta, alpha, beta, gamma, high gamma
freqSmoothing = [2,2,2,8,10,20]; %delta, theta, alpha, beta, gamma, high gamma
bands = {'delta','theta','alpha','beta','gamma'};%,'highGamma'};
dsFs = 250;
segLen = 60; %in seconds. analysis will be of first and last segLen seconds in the conditions that are to be split 
justRunNew = 0;
bandFreqs = [1,4,8,13,30,80];
bands = {'delta','theta','alpha','beta','gamma'};
nBands = length(bands);
level.Core = {'HGPM'};
level.NonCore = {'HGAL','PT','PP','STG'};
level.AudRel = {'Aud-rel','Auditory-related'};
level.PFC = {'PFC'};
level.Other = {'Thalamus','Other','SensMot','Sensorimotor'};
levelNames = fieldnames(level);

varNames = {'subj','chanPair','anatRegs','ROIs','sides','hierLevs','xCoord1','yCoord1',...
    'zCoord1','xCoord2','yCoord2','zCoord2','OAAS','state','band','wpliVal','wpliZ'};

thisRow = 0;
patientIDs = fieldnames(batchParams)';
%Determine number of elements in cellData
nRows = 0;
for iPatient = 1:length(patientIDs)
    thisName = patientIDs{iPatient};
    params = batchParams.(thisName);
    ECoGchan = params.ECoGchannels;
    nChans = length([ECoGchan.chanNum]);
    nConds = length(params.newCond);
    nRowsThisSubj = ((nChans^2-nChans)/2)*nConds*nBands;
    nRows = nRows + nRowsThisSubj;
end
cellData = cell(nRows,length(varNames));
for iPatient = 1:length(patientIDs)
    thisName = patientIDs{iPatient};
    display(['Processing ' thisName]);
    params = batchParams.(thisName);
    ECoGchan = params.ECoGchannels;
    nChans = length([ECoGchan.chanNum]);
    conditions = params.newCond;
    chanLevel = {};
    for iChan = 1:nChans
        noMatch = 1;
        iLev = 1;
        while iLev<=length(levelNames) && noMatch
            roiIndex = find(strcmp(level.(levelNames{iLev}),params.chanROI{iChan}));
            if ~isempty(roiIndex)
                chanLevel{iChan} = levelNames(iLev);
                noMatch = 0;
            else
                iLev = iLev+1;
            end
        end
        if isempty(chanLevel{iChan})
            error(['No level match for ch#' num2str(ECoGchan(iChan).chanNum) ' in ' thisName]);
        end
    end
    for iChan = 2:nChans
        for jChan = 1:iChan-1
            display(['Ch' num2str(iChan) '-Ch' num2str(jChan)]);
            for iCond = 1:length(conditions)
                thisCond = conditions{iCond};
                OAASVal = getOAASVal(thisCond);
                if iCond == 1
                    brainState = 'predrg';
                elseif OAASVal>=3
                    brainState = 'sedate';
                else
                    brainState = 'unresp';
                end
                for iBand = 1:length(bands)                
                    thisBand = bands{iBand};
                    thisRow = thisRow+1;
                    wpliVal = ...
                        abs(ECoG_conn.WPLI.(thisName).(thisCond).(thisBand).wpli_debiasedspctrm(iChan,jChan));
                    tempMeas = abs(ECoG_conn.WPLI.(thisName).(thisCond).(thisBand).wpli_debiasedspctrm(iChan,jChan));
                    tempSEM = ECoG_conn.WPLI.(thisName).(thisCond).(thisBand).wpli_debiasedspctrmsem(iChan,jChan);
                    wpliZScore = tempMeas./tempSEM;
                    cellData{thisRow,1}  = thisName;
                    cellData{thisRow,2}  = [num2str(params.chanNum(iChan)) '-' num2str(params.chanNum(jChan))];
                    cellData{thisRow,3}  = [ECoGchan(iChan).AnatReg '-' ECoGchan(jChan).AnatReg];
                    cellData{thisRow,4}  = [params.chanROI{iChan} '-' params.chanROI{jChan}];
                    cellData{thisRow,5}  = [params.chanSide{iChan} '-' params.chanSide{jChan}];
                    cellData{thisRow,6}  = [char(chanLevel{iChan}) '-' char(chanLevel{jChan})];
                    cellData{thisRow,7}  = ECoGchan(iChan).xCoord;
                    cellData{thisRow,8}  = ECoGchan(iChan).yCoord;
                    cellData{thisRow,9}  = ECoGchan(iChan).zCoord;
                    cellData{thisRow,10} = ECoGchan(jChan).xCoord;
                    cellData{thisRow,11} = ECoGchan(jChan).yCoord;
                    cellData{thisRow,12} = ECoGchan(jChan).zCoord;
                    cellData{thisRow,13} = OAASVal;
                    cellData{thisRow,14} = brainState;
                    cellData{thisRow,15} = thisBand;
                    cellData{thisRow,16} = wpliVal;
                    cellData{thisRow,17} = wpliZScore;
                end
            end
        end
    end
end
wpliTable = cell2table(cellData,'VariableNames',varNames);
save([outPath 'ECoG_wpliTable.mat'],'wpliTable','-v7');

%% ECoG gPDC
computer = 'desktopPC';
% computer = 'Mac'; 
if strcmp(computer,'Mac')
    computerSpecPrefix = '/Users/bankslaptop/Box Sync/My documents/';
else
    computerSpecPrefix = 'D:\Box Sync\My documents\';
end
if ~exist('electrodeFilePath','var')
    electrodeFilePath = [computerSpecPrefix 'Data and analysis' filesep ...
        'ECoG data' filesep 'Electrode MNI coordinates and ROI' filesep];
end

if ~exist('dataPath','var')
    dataPath = [computerSpecPrefix 'Data and analysis' filesep 'ECoG data' filesep];
end
if ~exist('outPath','var')
    outPath = [computerSpecPrefix 'Data and analysis' filesep 'ECoG data' filesep];
end

[~,setName] = getBatchParams(electrodeFilePath);%,defaultPath);
if exist([outPath 'ECoG_conn_' setName '.mat'],'file')
    load([outPath 'ECoG_conn_' setName '.mat'],'ECoG_conn','batchParams');
    if ~isfield(ECoG_conn,'WPLI')
        error(['wPLI not a field in ECoG_conn_' setName '.mat!']);
    end
else
    error(['ECoG_conn_' setName '.mat does not exist!']);
end

measToPlot = {'mGranger','fGranger','pdc','dtf','gpdc','ddtf'};
specName = {'grangerspctrm','grangerspctrm','pdcspctrm','dtfspctrm',...
    'gpdcspctrm','ddtfspctrm'};
iMeas = 5;
thisMeas = measToPlot{iMeas};
thisSpec = specName{iMeas};
% %% Freq analysis params
dsFs = 250;
segLen = 60; %in seconds. analysis will be of first and last segLen seconds in the conditions that are to be split 
justRunNew = 0;
bandFreqs = [1,4,8,13,30,80];
bands = {'delta','theta','alpha','beta','gamma'};
nBands = length(bands);
level.Core = {'HGPM'};
level.NonCore = {'HGAL','PT','PP','STG'};
level.AudRel = {'Aud-rel','Auditory-related'};
level.PFC = {'PFC'};
level.Other = {'Thalamus','Other','SensMot','Sensorimotor'};
levelNames = fieldnames(level);

varNames = {'subj','chanPair','anatRegs','ROIs','sides','hierLevs','xCoord1','yCoord1',...
    'zCoord1','xCoord2','yCoord2','zCoord2','OAAS','state','band','connVal'};

thisRow = 0;
patientIDs = fieldnames(batchParams)';
%Determine number of elements in cellData
nRows = 0;
for iPatient = 1:length(patientIDs)
    thisName = patientIDs{iPatient};
    params = batchParams.(thisName);
    chans = params.exempChans;
    nChans = length(chans.nums);
    nConds = length(params.newCond);
    nRowsThisSubj = ((nChans^2-nChans)/2)*nConds*nBands;
    nRows = nRows + nRowsThisSubj;
end
cellData = cell(nRows,length(varNames));
for iPatient = 1:length(patientIDs)
    thisName = patientIDs{iPatient};
    display(['Processing ' thisName]);
    params = batchParams.(thisName);
    ECoGchan = params.ECoGchannels;
    chans = params.exempChans;
    nChans = length(chans.ROIs);
    conditions = params.newCond;
    chanLevel = {};
    for iChan = 1:nChans
        noMatch = 1;
        iLev = 1;
        while iLev<=length(levelNames) && noMatch
            thisChan = find([ECoGchan.chanNum] == chans.nums(iChan));
            roiIndex = find(strcmp(level.(levelNames{iLev}),ECoGchan(thisChan).newROI));
            if ~isempty(roiIndex)
                chanLevel{iChan} = levelNames(iLev);
                noMatch = 0;
            else
                iLev = iLev+1;
            end
        end
        if isempty(chanLevel{iChan})
            error(['No level match for ch#' num2str(ECoGchan(iChan).chanNum) ' in ' thisName]);
        end
    end
    for iChan = 1:nChans
        for jChan = 1:nChans
            if iChan ~= jChan
                thisChan = find([ECoGchan.chanNum] == chans.nums(iChan));
                thatChan = find([ECoGchan.chanNum] == chans.nums(jChan));
                display(['Ch' num2str(iChan) '-Ch' num2str(jChan)]);
                for iCond = 1:length(conditions)
                    thisCond = conditions{iCond};
                    OAASVal = getOAASVal(thisCond);
                    if iCond == 1
                        brainState = 'predrg';
                    elseif OAASVal>=3
                        brainState = 'sedate';
                    else
                        brainState = 'unresp';
                    end
                    for iBand = 1:length(bands)                
                        thisBand = bands{iBand};
                        thisRow = thisRow+1;
                        freqIndex = ECoG_conn.(thisMeas).(thisName).(thisCond).freq>=bandFreqs(iBand)...
                            & ECoG_conn.(thisMeas).(thisName).(thisCond).freq<bandFreqs(iBand+1);
                        connVal = ...
                            mean(squeeze(ECoG_conn.(thisMeas).(thisName).(thisCond).(thisSpec)(iChan,jChan,freqIndex)));
                        cellData{thisRow,1}  = thisName;
                        cellData{thisRow,2}  = [num2str(chans.nums(iChan)) '-' num2str(chans.nums(jChan))];
                        cellData{thisRow,3}  = [ECoGchan(thisChan).AnatReg '-' ECoGchan(thatChan).AnatReg];
                        cellData{thisRow,4}  = [chans.ROIs{iChan} '-' chans.ROIs{jChan}];
                        cellData{thisRow,5}  = [params.chanSide{thisChan} '-' params.chanSide{thatChan}];
                        cellData{thisRow,6}  = [char(chanLevel{iChan}) '-' char(chanLevel{jChan})];
                        cellData{thisRow,7}  = ECoGchan(thisChan).xCoord;
                        cellData{thisRow,8}  = ECoGchan(thisChan).yCoord;
                        cellData{thisRow,9}  = ECoGchan(thisChan).zCoord;
                        cellData{thisRow,10} = ECoGchan(thatChan).xCoord;
                        cellData{thisRow,11} = ECoGchan(thatChan).yCoord;
                        cellData{thisRow,12} = ECoGchan(thatChan).zCoord;
                        cellData{thisRow,13} = OAASVal;
                        cellData{thisRow,14} = brainState;
                        cellData{thisRow,15} = thisBand;
                        cellData{thisRow,16} = connVal;
                    end
                end
            end
        end
    end
end
gpdcTable = cell2table(cellData,'VariableNames',varNames);
save([outPath 'ECoG_gpdcTable.mat'],'gpdcTable');

