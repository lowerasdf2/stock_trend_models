function grangerAnalysis()
%% General parameters
if ~exist('defaultPath','var')
%     defaultPath = '/Users/bankslaptop/Box Sync/My documents/Data and analysis/ECoG data/';
    defaultPath = 'D:\Box Sync\My documents\Data and analysis\ECoG data\';
end
if ~exist('electrodeFilePath','var')
%     electrodeFilePath = '/Users/bankslaptop/Box Sync/My documents/Data and analysis/ECoG data/Electrode MNI coordinates and ROI/';
    electrodeFilePath = 'D:\Box Sync\My documents\Data and analysis\ECoG data\Electrode MNI coordinates and ROI\';
end
if ~exist('batchParams', 'var')
    batchParams = getBatchParams(electrodeFilePath,defaultPath);
end
if ~exist('outPath','var')
%     outPath = '/Users/bankslaptop/Box Sync/My documents/Data and analysis/ECoG data/';
    outPath = 'D:\Box Sync\My documents\Data and analysis\ECoG data/';
end
% Ensure output directory exists
if ~exist(outPath, 'dir')
    mkdir(outPath);
end
disp(['Data will be saved to `' outPath '`']);

tinyCriterion = 0.02;

% Connectivity analysis parameters
calcFlag.granger = 0;
calcFlag.pdc = 0;
calcFlag.dtf = 1;
dsFs = 240;
trialToPlot = 1;
dtfYLims = [0,0.75];
bandFreqs = [4,8,13,30,80];

% Freq analysis params
freqsOfInterest = [2,6,10,20,40,80]; %delta, theta, alpha, beta, gamma, high gamma
freqSmoothing = [2,2,2,8,10,20]; %delta, theta, alpha, beta, gamma, high gamma
bands = {'delta','theta','alpha','beta','gamma'};%,'highGamma'};

%% Main analysis loop
patientIDs = fieldnames(batchParams)';
for iPatient = 1:length(patientIDs)
    name = patientIDs{iPatient};
    params = batchParams.(patientIDs{iPatient});
    conditions = fieldnames(params)';
    for iCond =1:length(conditions)
        cond = conditions{iCond};
        % cond = cond{1};
        disp('------------------------');
        disp(['Condition: ' cond]);
        disp('------------------------');
        [loadedData,params.(cond)] = loadECoGData(params.(cond));
        % loadedData is matrix of nChan x nSamples
        [params.(cond).chanSide, params.(cond).chanROI, params.(cond).chanNum,...
            params.(cond).sortOrder] = getChannelInfo(params.(cond));
        [data_ECoG] = convertToFTFormat(loadedData,params.(cond));
        cfg = [];
        cfg.resamplefs = dsFs;
        cfg.detrend    = 'yes';
        % the following line avoids numeric round off issues in the time axes upon resampling
        data_ECoGds = ft_resampledata(cfg, data_ECoG);
        data_ECoGds.sampleinfo = [1, size(data_ECoGds.trial{1,1},2)];
        clear data_ECoG
        % segment data into trials of length trialLength with overlap
        cfg         = [];
        cfg.length  = params.(cond).trialLength;
        cfg.overlap = params.(cond).trialOverlap;
        data_ECoGds   = ft_redefinetrial(cfg, data_ECoGds);
        % select subset of channels for model fit
        if iCond == 1
            chanToProc = [];
            chans = params.(cond).exempChans;
            for iChan = 1:length(chans.nums)
                thisIndex = params.(cond).chanNum == chans.nums(iChan);
                chanToProc(iChan) = params.(cond).sortOrder(thisIndex);
            end
            nChans = length(chanToProc);
        end

        fourier_cfg           = [];
        fourier_cfg.output    = 'powandcsd';
        fourier_cfg.method    = 'mtmfft';
        fourier_cfg.taper     = 'dpss';
        fourier_cfg.tapsmofrq = 2;
        fourier_cfg.pad       = 'nextpow2';
        fourier_cfg.channel   = chanToProc'; %Nx1 array
        fourier_freq = ft_freqanalysis(fourier_cfg, data_ECoGds);

        if calcFlag.granger
            fourier_granger_cfg                     = [];
            fourier_granger_cfg.method              = 'granger';
        %     fourier_granger_cfg.granger.feedback    = 'yes';
            fourier_granger_cfg.granger             = [];
            fourier_granger_cfg.granger.conditional = 'yes';
            fourier_granger_cfg.granger.sfmethod = 'multivariate';
            ECoG_conn.fGranger.(name).(cond) = ft_connectivityanalysis(fourier_granger_cfg, fourier_freq);
        end
        
        if calcFlag.pdc
            % Compute Partial Directed Coherence spectra
            cfg           = [];
            cfg.method    = 'pdc';
            ECoG_conn.pdc.(name).(cond) = ft_connectivityanalysis(cfg, fourier_freq);
        end
        
        if calcFlag.dtf
            % Compute Directed Transfer Function spectra
            cfg           = [];
            cfg.method    = 'dtf';
            ECoG_conn.dtf.(name).(cond) = ft_connectivityanalysis(cfg, fourier_freq);
        end
    end % loop over conditions
end % loop over patients
save([outPath 'ECoG_conn.mat'],'ECoG_conn','batchParams');

%% Plot data loop
patientIDs = fieldnames(batchParams)';
for iPatient = 1:length(patientIDs)
    name = patientIDs{iPatient};
    params = batchParams.(patientIDs{iPatient});
    conditions = fieldnames(params)';
    for iCond =1:length(conditions)
        cond = conditions{iCond};
        chans = params.(cond).exempChans;
        if calcFlag.granger
            fourier_granger = ECoG_conn.fGranger.(name).(cond);
            if iCond == 1
                fGrangerFig = figure('Name', [name ' - Granger spectra']);
                rowVec = zeros(1,size(fourier_granger.labelcmb,2));
                colVec = zeros(1,size(fourier_granger.labelcmb,2));
                for iChan = 1:nChans
                    rowVec(contains(fourier_granger.labelcmb(:,1),chans.ROIs{iChan})) = iChan;
                    colVec(contains(fourier_granger.labelcmb(:,2),chans.ROIs{iChan})) = iChan;
                end
            end
            % Row influences column
            figure(fGrangerFig);
            for iPair = 1: size(fourier_granger.labelcmb,1)
                iRow=rowVec(iPair);
                iCol=colVec(iPair);
                subplot(nChans,nChans,(iRow-1)*nChans+iCol);
                hold on
                plot(fourier_granger.freq, fourier_granger.grangerspctrm(iPair,:),'LineWidth',2);
                for iBand = 1:length(bandFreqs)                    
                    plot([bandFreqs(iBand),bandFreqs(iBand)],dtfYLims,':k');
                end
                ax = gca;
                ax.YLim = [0,0.25];
                ax.XScale = 'log';
                ax.XLim = [1,dsFs/2];
                if iCol == 1
                    ax.Title.Interpreter = 'none';
                    ax.Title.String = chans.ROIs{iRow};
                elseif iRow == 1
                    ax.Title.Interpreter = 'none';
                    ax.Title.String = chans.ROIs{iCol};
                end
                if ~(iCol == 1 && iRow == nChans)
                    ax.XTickLabel = {};
                    ax.YTickLabel = {};
                end
            end
            if iCond == length(conditions)
                legend(conditions);
            end
        end
        
        if calcFlag.pdc
            pdc = ECoG_conn.pdc.(name).(cond);
            if iCond == 1
                pdcFig = figure('Name', [name ' - PDC spectra']);
            end
            % Row influences column
            figure(pdcFig);
            for iRow=1:nChans
                for iCol=1:nChans
                    subplot(nChans,nChans,(iRow-1)*nChans+iCol);
                    hold on
                    plot(pdc.freq, squeeze(pdc.pdcspctrm(iRow,iCol,:)),'LineWidth',2);
                    for iBand = 1:length(bandFreqs)                    
                        plot([bandFreqs(iBand),bandFreqs(iBand)],dtfYLims,':k');
                    end
                    ax = gca;
                    ax.YLim = [0,0.5];
                    ax.XScale = 'log';
                    ax.XLim = [1,dsFs/2];
                    ax.XTick = [1,10,100];
                    if iCol == 1
                        ax.Title.Interpreter = 'none';
                        ax.Title.String = chans.ROIs{iRow};
                    elseif iRow == 1
                        ax.Title.Interpreter = 'none';
                        ax.Title.String = chans.ROIs{iCol};
                    end
                end
            end
            if iCond == length(conditions)
                legend(conditions);
            end
        end
        
        if calcFlag.dtf
            dtf = ECoG_conn.dtf.(name).(cond);
            if iCond == 1
                dtfFig = figure('Name', [name ' - DTF spectra']);
            end
            % Row influences column
            figure(dtfFig);
            for iRow=1:nChans
                for iCol=1:nChans
                    subplot(nChans,nChans,(iRow-1)*nChans+iCol);
                    hold on
                    if iRow ~= iCol
                        plot(dtf.freq, squeeze(dtf.dtfspctrm(iRow,iCol,:)),'LineWidth',2);
                        for iBand = 1:length(bandFreqs)                    
                            plot([bandFreqs(iBand),bandFreqs(iBand)],dtfYLims,':k');
                        end
                    end
                    if iCol == nChans
                        text(1.2*dsFs/2,0.5,chans.ROIs{iRow},'Interpreter','none');
                    end
                    if iRow == 1
                        text(0.9,0.85,chans.ROIs{iCol},'Interpreter','none');
                    end
                    ax = gca;
                    ax.YLim = dtfYLims;
                    ax.YTick = [0,0.2,0.4,0.6];
                    ax.XScale = 'log';
                    ax.XLim = [1,dsFs/2];
                    ax.XTick = [1,10,100];
                    if ~(iCol == 1 && iRow == nChans)
                        ax.XTickLabel = {};
                        ax.YTickLabel = {};
                    else
                        ax.YLabel.String = 'DTF';
                        ax.XLabel.String = 'Freq (Hz)';
                    end
                end
                if iRow == 1 && iCol == nChans && iCond == length(conditions)
                    legend(conditions);
                end
            end
        end
    end % loop over conditions
    saveas(dtfFig,[outPath name ' - DTF spectra']);
end % loop over patients

%%
        % Connectivity analysis
        % Model fit
        % Identify optimal model order
        % [modOrd] = getMVARModelOrder(data_ECoGds,chanToProc);
    %     modOrd = 16;
    %     
    %     % fit multivariate autoregressive model to data
    %     cfg         = [];
    %     cfg.order   = modOrd;
    %     cfg.toolbox = 'bsmart';
    %     cfg.channel = chanToProc'; %Nx1 array
    %     mdata       = ft_mvaranalysis(cfg, data_ECoGds);
    %     
    % iTrial = trialToPlot;
    % data = data_ECoG.trial{1,iTrial}(chanToProc,:);
    % data_hat = zeros(size(data));
    % data_hat(:,1:modOrd) = data(:,1:modOrd);
    % for iTime = modOrd+1:nPts
    %     for k=1:modOrd
    %         data_hat(:,iTime) = data_hat(:,iTime) + squeeze(mdata.coeffs(:,:,k))*data(:,iTime-k);
    %     end
    % end
    % figure()
    % for iChan = 1:nChans
    %     subplot(nChans,1,iChan)
    %     plot(1:nPts,data(iChan,:))
    %     hold on
    %     plot(1:nPts,data_hat(iChan,:))
    % end    %     %Compute spectral transfer function
    %     cfg        = [];
    %     cfg.method = 'mvar';
    %     mfreq      = ft_freqanalysis(cfg, mdata);

    %     % Alternatively, compute spectra to be used in connectivity analysis
    %     % directly from data
    %     cfg           = [];
    %     cfg.method    = 'mtmfft';
    %     cfg.taper     = 'dpss';
    %     cfg.output    = 'fourier';
    %     cfg.tapsmofrq = 2;
    %     cfg.pad     = 'nextpow2';
    %     cfg.channel = chanToProc'; %Nx1 array
    %     freq          = ft_freqanalysis(cfg, data_ECoGds);
    %     
    %     % Compute coherence spectra and compare model-based and direct measures
    %     cfg           = [];
    %     cfg.method    = 'coh';
    %     coh           = ft_connectivityanalysis(cfg, freq);
    %     cohm          = ft_connectivityanalysis(cfg, mfreq);
    %     
    %     % figure()
    %     % cfg           = [];
    %     % cfg.parameter = 'cohspctrm';
    %     % cfg.zlim      = [0 1];
    %     % ft_connectivityplot(cfg, coh, cohm);
    %     
    %     figure('Name', [name '-' cond ' - Cross-coherence spectra'])
    %     for iRow=1:nChans
    %         for iCol=1:nChans
    %             subplot(nChans,nChans,(iRow-1)*nChans+iCol);
    %             hold on
    %             if iRow ~= iCol
    %                 plot(coh.freq, squeeze(coh.cohspctrm(iRow,iCol,:)),'LineWidth',1);
    %                 plot(cohm.freq, squeeze(cohm.cohspctrm(iRow,iCol,:)),'LineWidth',1);
    %             else
    %                 plot(freq.freq, squeeze(mean(abs(freq.fourierspctrm(:,iRow,:)),1)),'LineWidth',1);
    %                 %             plot(mfreq.freq, squeeze(abs(mfreq.crsspctrm(iRow,iRow,:))),'LineWidth',1);
    %             end
    %             ax = gca;
    %             %             ax.YLim = [0,0.1];
    %             ax.XScale = 'log';
    %             ax.XLim = [1,50];
    %             if iCol == 1
    %                 ax.Title.Interpreter = 'none';
    %                 ax.Title.String = chans.ROIs{iRow};
    %             elseif iRow == 1
    %                 ax.Title.Interpreter = 'none';
    %                 ax.Title.String = chans.ROIs{iCol};
    %             end
    %         end
    %     end

    %     % Compute Granger spectra
    %     cfg           = [];
    %     cfg.method    = 'granger';
    %     cfg.granger.conditional = 'yes';
    %     granger       = ft_connectivityanalysis(cfg, mfreq);
    %     
    %     % figure()
    %     % cfg           = [];
    %     % cfg.parameter = 'grangerspctrm';
    %     % cfg.zlim      = [0 0.1];
    %     % ft_connectivityplot(cfg, granger);
    %     
    %     if iCond == 1
    %         grangerFig = figure('Name', [name ' - Granger spectra']);
    %     end
    %     % Row influences column
    %     figure(grangerFig);
    %     for iRow=1:nChans
    %         for iCol=1:nChans
    %             subplot(nChans,nChans,(iRow-1)*nChans+iCol);
    %             hold on
    %             plot(granger.freq, squeeze(granger.grangerspctrm(iRow,iCol,:)),'LineWidth',2);
    %             ax = gca;
    %             %         ax.YScale = 'log';
    %             ax.YLim = [0,0.25];
    %             ax.XScale = 'log';
    %             ax.XLim = [1,50];
    %             if iCol == 1
    %                 ax.Title.Interpreter = 'none';
    %                 ax.Title.String = chans.ROIs{iRow};
    %             elseif iRow == 1
    %                 ax.Title.Interpreter = 'none';
    %                 ax.Title.String = chans.ROIs{iCol};
    %             end
    %         end
    %     end
    %     if iCond == length(conditions)
    %         legend(conditions);
    %     end

