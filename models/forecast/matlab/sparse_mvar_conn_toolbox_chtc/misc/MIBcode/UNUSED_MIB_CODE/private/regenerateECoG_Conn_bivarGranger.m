%% General parameters
computer = 'Mac'; % 'desktopPC';
% computer = 'desktopPC';
if strcmp(computer,'Mac')
    computerSpecPrefix = '/Users/bankslaptop/Box Sync/My documents/';
else
    computerSpecPrefix = 'D:\Box Sync\My documents\';
end

if ~exist('dataPath','var')
    dataPath = [computerSpecPrefix 'Data and analysis' filesep 'ECoG data' filesep];
end
if ~exist('electrodeFilePath','var')
    electrodeFilePath = [computerSpecPrefix 'Data and analysis' filesep ...
        'ECoG data' filesep 'Electrode MNI coordinates and ROI' filesep];
end
if ~exist('outPath','var')
    outPath = [computerSpecPrefix 'Data and analysis' filesep 'ECoG data' filesep];
end

% Ensure output directory exists
if ~exist(outPath, 'dir')
    mkdir(outPath);
end
disp(['Data will be saved to `' outPath '`']);

batchParams = getBatchParams(electrodeFilePath);%,defaultPath);

% Connectivity analysis parameters
calcFlag.mvarGranger = 0;
calcFlag.fourierGranger = 1;
calcFlag.pdc = 0;
calcFlag.dtf = 0;
dsFs = 240;
trialToPlot = 1;
dtfYLims = [0,0.75];
mGYLims = [0,0.25];
fGYLims = [0,0.25];
bandFreqs = [4,8,13,30,80];

%%
patientIDs = fieldnames(batchParams)';
for iPatient = 1:length(patientIDs)
    thisName = patientIDs{iPatient};
    load([outPath thisName '-bivarGranger.mat'],'ECoG_conn_bivar');
%     load([outPath name '-bivarGranger.mat'],'conn','bivarSpecSum');
%     params = batchParams.(patientIDs{iPatient});
%     ECoG_conn_bivar = struct;
%     conditions = fieldnames(params)';
%     cond = conditions{1};
%     for iPair = 1:length(conn)
%         ECoG_conn_bivar.(cond).pair(iPair).fGranger = conn(iPair).fGranger;
%         ECoG_conn_bivar.(cond).bivarSpecSum = bivarSpecSum;
%     end
%     for iCond = 2:length(conditions)
    params = batchParams.(patientIDs{iPatient});
    % ECoG_conn_bivar = struct;
    conditions = fieldnames(params)';
    for iCond = 1:length(conditions)
        cond = conditions{iCond};
        % cond = cond{1};
        disp('------------------------');
        disp(['Patient: ' thisName ' - Condition: ' cond]);
        disp('------------------------');
        [loadedData,params.(cond)] = loadECoGData(params.(cond),[dataPath filesep thisName filesep cond filesep]);
        % loadedData is matrix of nChan x nSamples
        [params.(cond).chanSide, params.(cond).chanROI, params.(cond).chanNum,...
            params.(cond).sortOrder] = getChannelInfo(params.(cond));
        [data_ECoG] = convertToFTFormat(loadedData,params.(cond));
        
        % downsample the data to speed up component analysis
        cfg = [];
        cfg.resamplefs = dsFs;
        cfg.detrend    = 'yes';
        % the following line avoids numeric round off issues in the time axes upon resampling
        data_ECoGds = ft_resampledata(cfg, data_ECoG);
        data_ECoGds.sampleinfo = [1, size(data_ECoGds.trial{1,1},2)];
        clear data_ECoG
        
        % segment data into trials of length trialLength with overlap
        cfg         = [];
        cfg.length  = params.(cond).trialLength;
        cfg.overlap = params.(cond).trialOverlap;
        data_ECoGds   = ft_redefinetrial(cfg, data_ECoGds);
        
        % select subset of channels for analysis
        if iCond == 1
            chanToProc = [];
            chans = params.(cond).ECoGchannels;
            for iChan = 1:length(chans)
                if chans(iChan).AEPLatLoc>0 || chans(iChan).AEPLatGlob>0 ...
                        || chans(iChan).HiGLatLoc>0 || chans(iChan).HiGLatGlob>0 
                    chanToProc = [chanToProc params.(cond).sortOrder(iChan)];
                end
            end
            nChans = length(chanToProc);
        end
        
        cfg           = [];
        cfg.output    = 'powandcsd';
        cfg.method    = 'mtmfft';
        cfg.taper     = 'dpss';
        cfg.tapsmofrq = 2;
        cfg.pad       = ceil(max(cellfun(@numel, data_ECoGds.time)/data_ECoGds.fsample));
        cfg.foi       = 1:80;
        cfg.channel   = chanToProc'; %Nx1 array
        freq = ft_freqanalysis(cfg, data_ECoGds);
        for iChan = 1:length(freq.label)
            ChPos = strfind(freq.label{iChan},'Ch');
            ECoG_conn_bivar.(cond).chanLabel{iChan,1} = freq.label{iChan}(1:ChPos(1)-2);
            ECoG_conn_bivar.(cond).chanNum(iChan,1) = str2num(freq.label{iChan}(ChPos(1)+2:end));
        end
                
        bivarSpecSum = [];
        bivarSpecMax = [];
        iCount = 0;
        for iChan = 1:length(freq.label)
            for jChan = iChan+1:length(freq.label)
                iCount = iCount+1;
                ECoG_conn_bivar.(cond).bivarSpecSum(iChan,jChan) = ...
                    sum(ECoG_conn_bivar.(cond).pair(iCount).fGranger.grangerspctrm(1,2,1:40),3);
                ECoG_conn_bivar.(cond).bivarSpecSum(jChan,iChan) = ...
                    sum(ECoG_conn_bivar.(cond).pair(iCount).fGranger.grangerspctrm(2,1,1:40),3);
                ECoG_conn_bivar.(cond).bivarSpecMax(iChan,jChan) = ...
                    max(ECoG_conn_bivar.(cond).pair(iCount).fGranger.grangerspctrm(1,2,1:40),[],3);
                ECoG_conn_bivar.(cond).bivarSpecMax(jChan,iChan) = ...
                    max(ECoG_conn_bivar.(cond).pair(iCount).fGranger.grangerspctrm(2,1,1:40),[],3);
            end
        end
    end %conditions
    save([outPath thisName '-bivarGranger.mat'],'ECoG_conn_bivar','params');
end % loop over patients
% 
%%
tempConn = struct;
tempParams = struct;
patientIDs = fieldnames(batchParams)';
for iPatient = 1:length(patientIDs)
    thisName = patientIDs{iPatient};
    disp('------------------------');
    disp(['Patient: ' thisName]);
    disp('------------------------');
    load([outPath thisName '-bivarGranger.mat'],'ECoG_conn_bivar','params');
    tempConn.(thisName) = ECoG_conn_bivar;
    tempParams.(thisName) = params;
end % loop over patients
clear ECoG_conn_bivar;
ECoG_conn_bivar = tempConn;
clear batchParams;
batchParams = tempParams;
save([outPath 'ECoG_conn_bivarGranger.mat'],'ECoG_conn_bivar','batchParams');


% 