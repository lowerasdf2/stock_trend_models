function channels = ECoGChannelMap_tableRead(electrodeFile,electrodeSheet)
%CHANNELMAP returns a struct mapping regions to electrode indices.
% electrodeFile= [electrodeFilePath electrodeFile]
chanFileName = electrodeFile;
chanSheetName = electrodeSheet;

chanData = readtable(chanFileName,'Sheet',chanSheetName);
varNames_Read = chanData.Properties.VariableNames;
varNames_Wanted{1} = 'channelNum';  channelsFields{1} = 'chanNum';
varNames_Wanted{2} = 'contactNum';  channelsFields{2} = 'contNum';
varNames_Wanted{3} = 'sortOrder';   channelsFields{3} = 'sortOrder';
varNames_Wanted{4} = 'side';        channelsFields{4} = 'side';
varNames_Wanted{5} = 'ROI';         channelsFields{5} = 'oldROI';
varNames_Wanted{6} = 'newROI_9way'; channelsFields{6} = 'newROI';
varNames_Wanted{7} = 'newROINum';   channelsFields{7} = 'newROINum'; 
varNames_Wanted{8} = 'anatomicalRegion_Atlas';  channelsFields{8} = 'location';
varNames_Wanted{9} = 'anatReg';     channelsFields{9} = 'AnatReg';
varNames_Wanted{10}= 'MNI_X';       channelsFields{10} ='xCoord';
varNames_Wanted{11}= 'MNI_Y';       channelsFields{11} ='yCoord'; 
varNames_Wanted{12}= 'MNI_Z';       channelsFields{12} ='zCoord';
varNames_Wanted{13}= 'AEPLat_Loc';  channelsFields{13} ='AEPLatLoc';
varNames_Wanted{14}= 'HiGLat_Loc';  channelsFields{14} ='HiGLatLoc';
varNames_Wanted{15}= 'AEPLat_Glob'; channelsFields{15} ='AEPLatGlob';
varNames_Wanted{16}= 'HiGLat_Glob'; channelsFields{16} ='HiGLatGlob';

chanData = sortrows(chanData,'channelNum');
nChans = size(chanData,1);

if ~ismember('anatReg', chanData.Properties.VariableNames) && ...
        ismember('anatomicalRegion_Atlas', chanData.Properties.VariableNames)
    varNames_Wanted{9} = 'anatomicalRegion_Atlas';
end


for iField = 1:length(channelsFields)
    if ismember(varNames_Wanted{iField}, chanData.Properties.VariableNames)
        if iscell(chanData.(varNames_Wanted{iField})(1))
            for iChan = 1:nChans
                channels(iChan).(channelsFields{iField}) = chanData.(varNames_Wanted{iField}){iChan};
            end
        else
            for iChan = 1:nChans
                channels(iChan).(channelsFields{iField}) = chanData.(varNames_Wanted{iField})(iChan);
            end
        end
    end
end
% 
% if ismember('contactNum', chanData.Properties.VariableNames)
%     for iChan = 1:nChans
%         channels(iChan).contNum = chanData.contactNum(iChan);
%     end
% end
% if ismember('anatReg', chanData.Properties.VariableNames)
%     for iChan = 1:nChans
%         channels(iChan).AnatReg = chanData.anatReg{iChan};
%     end
% elseif ismember('anatomicalRegion_Atlas', chanData.Properties.VariableNames)
%     for iChan = 1:nChans
%         channels(iChan).AnatReg = chanData.anatomicalRegion_Atlas{iChan};
%     end
% end
% if ismember('newROI_9way', chanData.Properties.VariableNames)
%     for iChan = 1:nChans
%         channels(iChan).newROI = chanData.newROI_9way{iChan};
%     end
% end
% if ismember('ROI', chanData.Properties.VariableNames)
%     for iChan = 1:nChans
%         channels(iChan).oldROI = chanData.ROI{iChan};
%     end
% end
% if ismember('anatomicalRegion_Atlas', chanData.Properties.VariableNames)
%     for iChan = 1:nChans
%         channels(iChan).location = chanData.anatomicalRegion_Atlas{iChan};
%     end
% end
% if ismember('side', chanData.Properties.VariableNames)
%     for iChan = 1:nChans
%         channels(iChan).side = chanData.side{iChan};
%     end
% end
% if ismember('MNI_X', chanData.Properties.VariableNames)
%     for iChan = 1:nChans
%         channels(iChan).xCoord = chanData.MNI_X(iChan);
%     end
% end
% if ismember('MNI_Y', chanData.Properties.VariableNames)
%     for iChan = 1:nChans
%         channels(iChan).yCoord = chanData.MNI_Y(iChan);
%     end
% end
% if ismember('MNI_Z', chanData.Properties.VariableNames)
%     for iChan = 1:nChans
%         channels(iChan).zCoord = chanData.MNI_Z(iChan);
%     end
% end
% if ismember('sortOrder', chanData.Properties.VariableNames)
%     for iChan = 1:nChans
%         channels(iChan).sortOrder = chanData.sortOrder(iChan);
%     end
% end
% if ismember('AEPLatLoc', chanData.Properties.VariableNames)
%     for iChan = 1:nChans
%         channels(iChan).AEPLatLoc = chanData.AEPLatLoc(iChan);
%     end
% end
% if ismember('HiGLatLoc', chanData.Properties.VariableNames)
%     for iChan = 1:nChans
%         channels(iChan).HiGLatLoc = chanData.HiGLatLoc(iChan);
%     end
% end
% if ismember('AEPLatGlob', chanData.Properties.VariableNames)
%     for iChan = 1:nChans
%         channels(iChan).AEPLatGlob = chanData.AEPLatGlob(iChan);
%     end
% end
% if ismember('HiGLatGlob', chanData.Properties.VariableNames)
%     for iChan = 1:nChans
%         channels(iChan).HiGLatGlob = chanData.HiGLatGlob(iChan);
%     end
% end


% colNames = chanDataTxt(1,:);
% chanDataTxt = chanDataTxt(2:end,:);
% chanDataRaw = chanDataRaw(2:size(chanDataTxt,1)+1,:);
% numRows = size(chanDataNum,1);
% txtRows = size(chanDataTxt,1);
% rawRows = size(chanDataRaw,1);
% nRows = min([numRows,txtRows,rawRows]);
% chanDataTxt = chanDataTxt(1:nRows,:);
% chanDataRaw = chanDataRaw(1:nRows,:);
% chanDataNum = chanDataNum(1:nRows,:);
% 
% oldChanROICol = contains(colNames,'ROI');
% newChanROICol = contains(colNames,'New ROI (9-way)');
% chanLocCol = contains(colNames,'Anatomical region (Atlas)');
% chanAnatRegCol = contains(colNames,'AnatReg');
% if sum(chanAnatRegCol) == 0
%     chanAnatRegCol = chanLocCol;
% end
% chanNumCol = contains(colNames,'Channel #');
% contNumCol = contains(colNames,'Contact #');
% chanSortCol = contains(colNames,'sortOrder');
% chanSideCol = contains(colNames,'Side');
% mniXCol = contains(colNames,'MNI X');
% mniYCol = contains(colNames,'MNI Y');
% mniZCol = contains(colNames,'MNI Z');
% AEPLatLocCol = contains(colNames,'AEPLat_Loc');
% HiGLatLocCol = contains(colNames,'HiGLat_Loc');
% AEPLatGlobCol = contains(colNames,'AEPLat_Glob');
% HiGLatGlobCol = contains(colNames,'HiGLat_Glob');

% if sum(isnan(chanDataNum(:,chanNumCol))) > 0
%     error(['Problem with channel numbers in Excel sheet ' chanSheetName]);
% end
