function [STAGE, STAGE_KEY] = loadECoGData_justStageData(fileName)
% sleepDataPath = [dataPath thisName filesep 'Sleep data' filesep];
%Load sleep stage ECoG data from Howard lab.
%
% Returns an array of filtered data organized as follows:
%   (channel) x (sample)
%
% params.sleepData.stage
% params.sleepData.time
% params.sleepData.key

%% Load ECoG data
% Get all data files
display(['Loading ' fileName]);
fileObj = matfile(fileName);
fileDetails = whos(fileObj);
dataFields = {fileDetails.name};
if sum(contains(dataFields,'STAGE'))==2
    load(fileName,dataFields{contains(dataFields,'STAGE')});
    STAGE = STAGE';
    STAGE_KEY = STAGE_KEY';
end
