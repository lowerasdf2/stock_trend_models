function [loadedData, stageData, params] = loadECoGData_allNightSleep(params,sleepDataPath,patientNum)
% sleepDataPath = [dataPath thisName filesep 'Sleep data' filesep];
%loadECoGData  Load and concatenate ECoG data from Howard lab.
%
% Returns an array of filtered data organized as follows:
%   (channel) x (sample)
%
% params.sleepData.stage
% params.sleepData.time
% params.sleepData.key

nChans = length(params.ECoGchannels);
signalName = 'LFPx';

%% Load ECoG data
% Get all data files
dataFiles = dir([sleepDataPath patientNum '*.mat']);
loadedData = [];
stageData = [];
tempSTAGE_KEY = [];
for jFile = 1:length(dataFiles)
    display(['Loading ' dataFiles(jFile).name]);
    fileObj = matfile(dataFiles(jFile).name);
    fileDetails = whos(fileObj);
    dataFields = {fileDetails.name};
    if sum(contains(dataFields,'STAGE'))==2
        load([sleepDataPath dataFiles(jFile).name],dataFields{contains(dataFields,'STAGE')});
        stageData = [stageData STAGE'];
        tempSTAGE_KEY = [tempSTAGE_KEY STAGE_KEY'];
    end
    chanFields = dataFields(contains(dataFields,signalName));
    chanNum = zeros(1,length(chanFields));
    chanStr = {};
    for iChan = 1:length(chanFields)
        thisChan = chanFields{iChan};
        chanStr{iChan} = thisChan(strfind(thisChan,signalName)+length(signalName):strfind(thisChan,'_')-1);
        chanNum(iChan) = str2num(chanStr{iChan});
    end
    [chanNum,sortIndex] = sort(chanNum);
    chanStr = chanStr(sortIndex);
    chanFields = chanFields(sortIndex);
    for iChan = 1:nChans
        thisChan = params.ECoGchannels(iChan).chanNum;
        dataToLoad = chanFields{chanNum==thisChan};
        thisData = load([sleepDataPath dataFiles(jFile).name],dataToLoad);
        if iChan == 1
            nPts = length(thisData.(dataToLoad).dat);
            tempData = zeros(nChans,nPts);
        end
        tempData(iChan,:) = thisData.(dataToLoad).dat;
        if iChan == 1 && jFile == 1
            params.dT = 1/thisData.(dataToLoad).fs; %sec
        end
    end
    loadedData = [loadedData tempData];
end
params.STAGE_KEY = unique(tempSTAGE_KEY);
params.nPts = size(loadedData,2);
dT = params.dT;
chansToUse = [params.ECoGchannels.chanNum];
save([sleepDataPath patientNum '_concatData.mat'],'loadedData','dT','chansToUse');

