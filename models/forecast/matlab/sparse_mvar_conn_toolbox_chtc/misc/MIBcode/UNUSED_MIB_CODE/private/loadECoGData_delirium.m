function [loadedData, params] = loadECoGData_delirium(params,pathToLoad)
%loadECoGData  Load and concatenate ECoG data from Howard lab.
% pathToLoad = [dataPath thisName filesep 'Delirium data' filesep thisCond filesep];
% params = params.(thisCond);
%
% Returns an array of filtered data organized as follows:
%   (channel) x (sample)
%

nChan = length(params.ECoGchannels);

%% Load ECoG data

% Get all data files
dataFiles = dir([pathToLoad '*.mat']);
if isempty(dataFiles)
    error(['No data in ' pathToLoad '!']);
end
loadedData = [];
for jFile = 1:length(dataFiles)
    for iChan = 1:nChan
        thisChan = params.ECoGchannels(iChan).chanNum;
        if thisChan < 10
            chanStr = ['00' num2str(thisChan)];
        elseif thisChan < 100
            chanStr = ['0' num2str(thisChan)];
        else
            chanStr = num2str(thisChan);
        end
        chanDataName = [params.dataPrefix chanStr];
        % display(['Loading ' chanDataName]);
        tempData = load([pathToLoad dataFiles(jFile).name], ...
            chanDataName);
        if iChan == 1
            params.nPts = length(tempData.(chanDataName).dat);
            tempLoadedData = zeros(nChan,params.nPts);
            params.dT = 1/tempData.(chanDataName).fs(1); %sec
%             tempGain = load([pathToLoad dataFiles(jFile).name],'gain');
%             params.gain = tempGain.gain.(params.signalName);
            params.gain = 1;
        end
        tempLoadedData(iChan,:) = tempData.(chanDataName).dat; 
        clear tempData
    end
    loadedData = [loadedData tempLoadedData];
end
loadedData = loadedData*params.gain;
