%% General parameters
computer = 'Mac'; % 'desktopPC';
% computer = 'desktopPC';
if strcmp(computer,'Mac')
    computerSpecPrefix = '/Users/bankslaptop/Box Sync/My documents/';
else
    computerSpecPrefix = 'D:\Box Sync\My documents\';
end

if ~exist('dataPath','var')
    dataPath = [computerSpecPrefix 'Data and analysis' filesep 'ECoG data' filesep];
end
if ~exist('electrodeFilePath','var')
    electrodeFilePath = [computerSpecPrefix 'Data and analysis' filesep ...
        'ECoG data' filesep 'Electrode MNI coordinates and ROI' filesep];
end
if ~exist('outPath','var')
    outPath = [computerSpecPrefix 'Data and analysis' filesep 'ECoG data' filesep];
end

% Ensure output directory exists
if ~exist(outPath, 'dir')
    mkdir(outPath);
end
disp(['Data will be saved to `' outPath '`']);

batchParams = getBatchParams(electrodeFilePath);%,defaultPath);

% Connectivity analysis parameters
calcFlag.mvarGranger = 0;
calcFlag.fourierGranger = 1;
calcFlag.pdc = 0;
calcFlag.dtf = 0;
dsFs = 240;
trialToPlot = 1;
dtfYLims = [0,0.75];
mGYLims = [0,0.25];
fGYLims = [0,0.25];
bandFreqs = [4,8,13,30,80];

%%
patientIDs = fieldnames(batchParams)';
for iPatient = 1:length(patientIDs)
    thisName = patientIDs{iPatient};
%     load([outPath name '-bivarGranger.mat'],'conn','bivarSpecSum');
%     params = batchParams.(patientIDs{iPatient});
%     ECoG_conn_bivar = struct;
%     conditions = params.cond;
%     cond = conditions{1};
%     for iPair = 1:length(conn)
%         ECoG_conn_bivar.(cond).pair(iPair).fGranger = conn(iPair).fGranger;
%         ECoG_conn_bivar.(cond).bivarSpecSum = bivarSpecSum;
%     end
%     for iCond = 2:length(conditions)
    params = batchParams.(patientIDs{iPatient});
    ECoG_conn_bivar = struct;
    conditions = params.cond;
    for iCond = 1:length(conditions)
        cond = conditions{iCond};
        % cond = cond{1};
        disp('------------------------');
        disp(['Condition: ' cond]);
        disp('------------------------');
        [loadedData,params] = loadECoGData(params,[dataPath filesep thisName filesep cond filesep]);
        % loadedData is matrix of nChan x nSamples
        [params.chanSide, params.chanROI, params.chanNum,...
            params.sortOrder] = getChannelInfo(params);
        [data_ECoG] = convertToFTFormat(loadedData,params);
        
        % downsample the data to speed up component analysis
        cfg = [];
        cfg.resamplefs = dsFs;
        cfg.detrend    = 'yes';
        % the following line avoids numeric round off issues in the time axes upon resampling
        data_ECoGds = ft_resampledata(cfg, data_ECoG);
        data_ECoGds.sampleinfo = [1, size(data_ECoGds.trial{1,1},2)];
        clear data_ECoG
        
        % segment data into trials of length trialLength with overlap
        cfg         = [];
        cfg.length  = params.trialLength;
        cfg.overlap = params.trialOverlap;
        data_ECoGds   = ft_redefinetrial(cfg, data_ECoGds);
        
        % select subset of channels for analysis
        if iCond == 1
            chanToProc = [];
            chans = params.ECoGchannels;
            for iChan = 1:length(chans)
                if chans(iChan).AEPLatLoc>0 || chans(iChan).AEPLatGlob>0 ...
                        || chans(iChan).HiGLatLoc>0 || chans(iChan).HiGLatGlob>0 
                    chanToProc = [chanToProc params.chanNum(iChan)];
                end
            end
            nChans = length(chanToProc);
        end
        
        cfg           = [];
        cfg.output    = 'powandcsd';
        cfg.method    = 'mtmfft';
        cfg.taper     = 'dpss';
        cfg.tapsmofrq = 2;
        cfg.pad       = ceil(max(cellfun(@numel, data_ECoGds.time)/data_ECoGds.fsample));
        cfg.foi       = 1:80;
        cfg.channel   = chanToProc'; %Nx1 array
        freq = ft_freqanalysis(cfg, data_ECoGds);
        for iChan = 1:length(freq.label)
            ChPos = strfind(freq.label{iChan},'Ch');
            ECoG_conn_bivar.(cond).chanLabel{iChan,1} = freq.label{iChan}(1:ChPos(1)-2);
            ECoG_conn_bivar.(cond).chanNum(iChan,1) = str2num(freq.label{iChan}(ChPos(1)+2:end));
        end
        
        cfg                     = [];
        cfg.method              = 'granger';
        cfg.granger             = [];
        % cfg.granger.conditional = 'yes';
        cfg.granger.sfmethod    = 'multivariate';
        cfg.channel   = chanToProc'; %Nx1 array
        
        bivarSpecSum = [];
        bivarSpecMax = [];
        iCount = 0;
        for iChan = 1:length(freq.label)
            for jChan = iChan+1:length(freq.label)
                iCount = iCount+1;
                cfg.channel   = [iChan,jChan];
                ECoG_conn_bivar.(cond).pair(iCount).fGranger = ft_connectivityanalysis(cfg, freq);
                ECoG_conn_bivar.(cond).bivarSpecSum(iChan,jChan) = ...
                    sum(ECoG_conn_bivar.(cond).pair(iCount).fGranger.grangerspctrm(1,2,1:40),3);
                ECoG_conn_bivar.(cond).bivarSpecSum(jChan,iChan) = ...
                    sum(ECoG_conn_bivar.(cond).pair(iCount).fGranger.grangerspctrm(2,1,1:40),3);
                ECoG_conn_bivar.(cond).bivarSpecMax(iChan,jChan) = ...
                    max(ECoG_conn_bivar.(cond).pair(iCount).fGranger.grangerspctrm(1,2,1:40),[],3);
                ECoG_conn_bivar.(cond).bivarSpecMax(jChan,iChan) = ...
                    max(ECoG_conn_bivar.(cond).pair(iCount).fGranger.grangerspctrm(2,1,1:40),[],3);
            end
        end
    end %conditions
    save([outPath thisName '-bivarGranger.mat'],'ECoG_conn_bivar','params');
end % loop over patients
% 
%% Cluster analysis based on fGranger sum
% load('ECoG_conn_origSet.mat','batchParams','ECoG_conn');
patientIDs = fieldnames(batchParams)';
nodeData = struct;
for iPatient = 1:length(patientIDs)
    thisName = patientIDs{iPatient};
    load([dataPath thisName '-bivarGranger.mat']);
    conditions = fieldnames(batchParams.(thisName));
    for iCond = 1:length(conditions)
        cond = conditions{iCond};
        chanParams = batchParams.(thisName).(cond).ECoGchannels;
        nChans = size(ECoG_conn_bivar.(cond).bivarSpecSum,1);
        bivarSpecSum = [];
        bivarSpecMax = [];
        iCount = 0;
        for iChan = 1:nChans
            for jChan = iChan+1:nChans
                iCount = iCount+1;
                thisGranger = ECoG_conn_bivar.(cond).pair(iCount).fGranger;
                bivarSpecSum(iChan,jChan) = sum(thisGranger.grangerspctrm(1,2,1:40),3);
                bivarSpecSum(jChan,iChan) = sum(thisGranger.grangerspctrm(2,1,1:40),3);
            end
        end
        if iCond == 1
            maxSum = bivarSpecSum;
        else
            maxSum = max(maxSum,bivarSpecSum);
        end
    end
    chanNums = [];
    [chanNums(:,1),chanNums(:,2)] = find(maxSum>prctile(maxSum(:),99.5));
    for iNode = 1:size(chanNums,1)
        nodeData.(thisName).nodesOfInterest{iNode,1} = chanNums(iNode,1);
        nodeData.(thisName).nodesOfInterest{iNode,2} = chanNums(iNode,2);
        nodeData.(thisName).nodesOfInterest{iNode,3} = chanParams(chanNums(iNode,1)).ROI;
        nodeData.(thisName).nodesOfInterest{iNode,4} = chanParams(chanNums(iNode,2)).ROI;
    end
    
    maskMat = maxSum>prctile(maxSum(:),99);
    testMat = maxSum;
    testMat(~maskMat) = NaN;
    iStart = 0;
    dataToClust = [];
    for iRow = 1:size(testMat,1)
        keepNodes = find(~isnan(testMat(iRow,:)));
        if ~isempty(keepNodes)
            iCount = 0;
            for iNode = 1:length(keepNodes)
                if abs(keepNodes(iNode) - iRow) > 4
                    iCount = iCount+1;
                    dataToClust(iCount+iStart,1) = iRow;
                    dataToClust(iCount+iStart,2) = keepNodes(iNode);
                end
            end
            iStart = size(dataToClust,1);
        end
    end
    for iRow = 1:size(dataToClust,1)
        if dataToClust(iRow,2) > dataToClust(iRow,1)
            temp = dataToClust(iRow,1);
            dataToClust(iRow,1) = dataToClust(iRow,2);
            dataToClust(iRow,2) = temp;
        end
    end
    clustFig = figure('Name',[thisName 'Granger conn clusters']);
    plot(dataToClust(:,1),dataToClust(:,2),'o')
    hold on
    plot([0,size(maskMat,1)+1],[0,size(maskMat,1)+1],'--');
    ax = gca;
    ax.XLim = [0,size(maskMat,1)+1];
    ax.YLim = [0,size(maskMat,1)+1];
    clustVals = [];
    [clustVals(:,1),clustVals(:,2)] = ginput(100);

    figure(clustFig);
    for iClust = 1:size(clustVals,1)
        plot(clustVals(iClust,1),clustVals(iClust,2),'kx','MarkerSize',12)
    end
    chanVals = sort(unique(round(clustVals(:))));    
    for iNode = 1:length(chanVals)
        nodeData.(thisName).nodesToKeep{iNode,1} = chanVals(iNode);
        nodeData.(thisName).nodesToKeep{iNode,2} = [chanParams(chanVals(iNode)).side '-' chanParams(chanVals(iNode)).ROI];
        nodeData.(thisName).nodesToKeep{iNode,3} = chanParams(chanVals(iNode)).chanNum;
    end

end

%% Cluster analysis based on fGranger max
maxCrit = 95.;
% nClust = [17,14,18];
% load('ECoG_conn_origSet.mat','batchParams','ECoG_conn');
patientIDs = fieldnames(batchParams)';
nodeData_max = struct;
for iPatient = 4:length(patientIDs)
    thisName = patientIDs{iPatient};
    conditions = fieldnames(batchParams.(thisName));
    cond = conditions{1};
    chanParams = batchParams.(thisName).(cond).ECoGchannels;
    load([dataPath thisName '-bivarGranger.mat']);
    if isfield(ECoG_conn_bivar.(cond),'bivarSpecMax')
        maxMax = ECoG_conn_bivar.(cond).bivarSpecMax;
    else
        maxMax = [];
        iCount = 0;
        for iChan = 1:length(chanParams)
            for jChan = iChan+1:length(chanParams)
                iCount = iCount+1;
                maxMax(iChan,jChan) = max(ECoG_conn_bivar.(cond).pair(iCount).fGranger.grangerspctrm(1,2,:),[],3);
                maxMax(jChan,iChan) = max(ECoG_conn_bivar.(cond).pair(iCount).fGranger.grangerspctrm(2,1,:),[],3);
            end
        end
    end
    for iCond = 2:length(conditions)
        cond = conditions{iCond};
        maxMax = max(maxMax,ECoG_conn_bivar.(cond).bivarSpecMax);
    end
    chanNums = [];
    [chanNums(:,1),chanNums(:,2)] = find(maxMax>prctile(maxMax(:),maxCrit));
    for iNode = 1:size(chanNums,1)
        nodeData_max.(thisName).nodesOfInterest{iNode,1} = chanNums(iNode,1);
        nodeData_max.(thisName).nodesOfInterest{iNode,2} = chanNums(iNode,2);
        nodeData_max.(thisName).nodesOfInterest{iNode,3} = chanParams(chanNums(iNode,1)).ROI;
        nodeData_max.(thisName).nodesOfInterest{iNode,4} = chanParams(chanNums(iNode,2)).ROI;
    end
    
    maskMat = maxMax>prctile(maxMax(:),maxCrit);
    testMat = maxMax;
    testMat(~maskMat) = NaN;
    iStart = 0;
    dataToClust = [];
    for iRow = 1:size(testMat,1)
        keepNodes = find(~isnan(testMat(iRow,:)));
        if ~isempty(keepNodes)
            iCount = 0;
            for iNode = 1:length(keepNodes)
                if abs(keepNodes(iNode) - iRow) > 4
                    iCount = iCount+1;
                    dataToClust(iCount+iStart,1) = iRow;
                    dataToClust(iCount+iStart,2) = keepNodes(iNode);
                end
            end
            iStart = size(dataToClust,1);
        end
    end
    for iRow = 1:size(dataToClust,1)
        if dataToClust(iRow,2) > dataToClust(iRow,1)
            temp = dataToClust(iRow,1);
            dataToClust(iRow,1) = dataToClust(iRow,2);
            dataToClust(iRow,2) = temp;
        end
    end
    clustFig = figure('Name',[thisName 'Granger conn clusters - max']);
    plot(dataToClust(:,1),dataToClust(:,2),'o')
    hold on
    plot([0,size(maskMat,1)+1],[0,size(maskMat,1)+1],'--');
    ax = gca;
    ax.XLim = [0,size(maskMat,1)+1];
    ax.YLim = [0,size(maskMat,1)+1];
    clustVals = [];
    [clustVals(:,1),clustVals(:,2)] = ginput(100);

    figure(clustFig);
    for iClust = 1:size(clustVals,1)
        plot(clustVals(iClust,1),clustVals(iClust,2),'kx','MarkerSize',12)
    end
    
    chanVals = sort(unique(round(clustVals(:))));    
    for iNode = 1:length(chanVals)
        nodeData_max.(thisName).nodesToKeep{iNode,1} = chanVals(iNode);
        nodeData_max.(thisName).nodesToKeep{iNode,2} = [chanParams(chanVals(iNode)).side '-' chanParams(chanVals(iNode)).ROI];
        nodeData_max.(thisName).nodesToKeep{iNode,3} = chanParams(chanVals(iNode)).chanNum;
    end

%     opts = statset('Display','final');
%     [idx,C] = kmeans(dataToClust,nClust(iPatient),'Distance','cityblock',...
%         'Replicates',5,'Options',opts);

end

%%
% Plot bivariate Granger conn matrices
patientIDs = fieldnames(batchParams)';
nodeData = struct;
for iPatient = 1:length(patientIDs)
    thisName = patientIDs{iPatient};
    display(['Loading ' thisName '-bivarGranger.mat']);
    load([dataPath thisName '-bivarGranger.mat']);
    conditions = fieldnames(batchParams.(thisName));
    for iCond = 1:length(conditions)
        cond = conditions{iCond};
        nChans = size(ECoG_conn_bivar.(cond).bivarSpecSum,1);
        bivarSpec.(thisName).(cond).sum = [];
        iCount = 0;
        for iChan = 1:nChans
            for jChan = iChan+1:nChans
                iCount = iCount+1;
                thisGranger = ECoG_conn_bivar.(cond).pair(iCount).fGranger;
                bivarSpec.(thisName).(cond).sum(iChan,jChan) = sum(thisGranger.grangerspctrm(1,2,1:40),3);
                bivarSpec.(thisName).(cond).sum(jChan,iChan) = sum(thisGranger.grangerspctrm(2,1,1:40),3);
            end
        end
%         if iCond == 1
%             figName = [thisName '-'  '-bivarGrangerConnMatrix'];
%             matFig = figure('Name',figName);
%             maxVal = 0;
%         end
%         maxVal = max(maxVal,prctile(bivarSpec.(cond).sum(:),97.5));
    end
%     for iCond = 1:length(conditions)
%         cond = conditions{iCond};
%         figure(matFig);
%         subplot(1,length(conditions),iCond);
%         imagesc(bivarSpec.(thisName).(cond).sum/maxVal); axis square;
%         ax = gca;
%         ax.XAxisLocation = 'top';
%         ax.Title.String = cond;
%         ax.XLabel.String = 'Chan #';
%         ax.YLabel.String = 'Chan #';
%     end
%     for iCond = 1:length(conditions)
%         subplot(1,length(conditions),iCond);
%         caxis(gca,[0, 1]);
%         colorbar('southoutside');
%     end
%     saveas(matFig,[outPath figName]);
%     clear figName
%     clear matFig
end



