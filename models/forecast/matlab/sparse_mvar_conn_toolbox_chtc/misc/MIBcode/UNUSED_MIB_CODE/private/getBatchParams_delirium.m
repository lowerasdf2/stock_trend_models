% Resting state parameters
function [batchParams,setName] = getBatchParams_delirium(electrodeFilePath)%,defaultPath)
batchParams = struct;
% if ~exist('defaultPath','var')
%     defaultPath = '/Users/bankslaptop/Box Sync/My documents/Data and analysis/ECoG data/';
%     % defaultPath = 'D:\Box Sync\My documents\Data and analysis\Data and analysis\ECoG data\';
% end
if ~exist('electrodeFilePath','var')
%     electrodeFilePath = '/Users/bankslaptop/Box Sync/My documents/Data and analysis/ECoG data/Electrode MNI coordinates and ROI/';
    electrodeFilePath = 'D:\Box Sync\My documents\Data and analysis\ECoG data\Electrode MNI coordinates and ROI\';
end

trialLength = 4; %sec
trialOverlap = 0.25; %fractional overlap
% setName = 'origSet'; %origSet was determined from LGD resps by eye before stats
% setName = 'newSet'; %newSet was constrained only by bivariate Granger
% setName = 'LGDSet'; %LGDSet was constrained by LGD responses analyzed with stats and bivariate Granger
% setName = 'nineROISet'; %nineROI is Kirill's Aug 2017 area segmentation
% setName = 'sevenROISet'; %nineROI is Kirill's Aug 2017 area segmentation
% setName = 'hierarchySet'; %up to 9 ROIs w/LGD resps: [HGPM, HGAL, PT, PP, STGP, STGM, SMG or AG, MTG, MFG or IFGop]
setName = 'deliriumSet';

%% Patients 
%400L
electrodeFile = 'LGD electrodes for analysis - sorted by ROI.xlsx';
electrodeSheet = 'L400_CRU';
patientID = '400L';
patientID = ['patient' patientID];
recDate = 'yymdd';
dataPrefix = 'LFPx';
signalName = 'LFPx';
ECoGchan = ECoGChannelMap_tableRead([electrodeFilePath electrodeFile],electrodeSheet);

% sevenROISet:
% exempChans.nums = [48,53,83,57,210,189,197];
% exempChans.ROIs = {'L_HGPM','L_HGAL','L_PT','L_PP','L_STG','L_Aud-rel','L_PFC'};
% % hierarchySet:
% exempChans.nums = [48,53,83,57,210,209,189,197];
% exempChans.ROIs = {'L_HGPM','L_HGAL','L_PT','L_PP','L_STGM','L_MTG','L_AG','L_PFC'};
% deliriumSet:
exempChans.nums = [2,12,38,56,67,77,85,91,213,129,146,164,196];
exempChans.ROIs = {'L_TP','L_OrbG','L_FusG','L_Amyg','L_Hipp','L_Ins','L_ACG','L_mACG','R_mACG','L_SFG','L_SPL','L_SMG','L_IFGpop'};

exempChans.setName = setName;

cond = {};
cond{1} = 'ctrl1';
cond{2} = 'ctrl2';
cond{3} = 'ctrl3';
cond{4} = 'ctrl4';
cond{5} = 'dlrm1';
cond{6} = 'dlrm2';
cond{7} = 'recv1';
batchParams.(patientID) = struct( ...
    'recDate', recDate,'ECoGchannels', ECoGchan, ... %'dataPath', dataPath,...
    'dataPrefix',dataPrefix, 'signalName',signalName, ...
    'trialLength',trialLength, 'trialOverlap',trialOverlap);
batchParams.(patientID).exempChans = exempChans;
batchParams.(patientID).cond = cond;
%%
%403L
electrodeFile = 'LGD electrodes for analysis - sorted by ROI.xlsx';
electrodeSheet = 'L403_CRU';
patientID = '403L';
patientID = ['patient' patientID];
recDate = 'yymdd';
dataPrefix = 'LFPx';
signalName = 'LFPx';
ECoGchan = ECoGChannelMap_tableRead([electrodeFilePath electrodeFile],electrodeSheet);

% NOTE: for this patient, these are channel numbers, not contact numbers.
% That is, these numbers match the recorded data, not the brain map pdfs.
% % hierarchySet:
% exempChans.nums = [45,48,107,101,228,172,193,200,168];
% exempChans.ROIs = {'L_HGPM','L_HGAL','L_PT','L_PP','L_STGP','L_STGM','L_MTG','L_SMG','L_PFC'};
% deliriumSet:
exempChans.nums = [13,2,8,54,112,103,145,214,158,155];
exempChans.ROIs = {'L_TP','L_OrbG','L_FusG','L_Amyg','L_Ins','L_Hipp','L_TFG','L_SMG','L_IFGpop','L_IFG_por'};

exempChans.setName = setName;

cond = {};
cond{1} = 'ctrl1';
cond{2} = 'ctrl2';
cond{3} = 'dlrm1';
batchParams.(patientID) = struct( ...
    'recDate', recDate,'ECoGchannels', ECoGchan, ... %'dataPath', dataPath,...
    'dataPrefix',dataPrefix, 'signalName',signalName, ...
    'trialLength',trialLength, 'trialOverlap',trialOverlap);
batchParams.(patientID).exempChans = exempChans;
batchParams.(patientID).cond = cond;
%%
%405L
electrodeFile = 'LGD electrodes for analysis - sorted by ROI.xlsx';
electrodeSheet = 'L405_CRU_001-006';
patientID = '405L';
patientID = ['patient' patientID];
recDate = 'yymdd';
dataPrefix = 'LFPx';
signalName = 'LFPx';
ECoGchan = ECoGChannelMap_tableRead([electrodeFilePath electrodeFile],electrodeSheet);

% NOTE: for this patient, these are channel numbers, not contact numbers.
% That is, these numbers match the recorded data, not the brain map pdfs.
% deliriumSet:
exempChans.nums = [116,16,15,111,34,78,233,59,51,133,224,218,66,65,53];
exempChans.ROIs = {'L_TP','L_mACG','L_mACG','L_ITG','L_Hipp','L_Amyg','L_SFG','L_OrbG','L_OrbG','L_MFG','L_IFGpop','L_SMG','L_Ins','L_Ins','L_Ins'};

exempChans.setName = setName;

cond = {};
cond{1} = 'dlrm1';
cond{2} = 'ctrl1';
batchParams.(patientID) = struct( ...
    'recDate', recDate,'ECoGchannels', ECoGchan, ... %'dataPath', dataPath,...
    'dataPrefix',dataPrefix, 'signalName',signalName, ...
    'trialLength',trialLength, 'trialOverlap',trialOverlap);
batchParams.(patientID).exempChans = exempChans;
batchParams.(patientID).cond = cond;
%%
%405L
electrodeFile = 'LGD electrodes for analysis - sorted by ROI.xlsx';
electrodeSheet = 'L405_CRU_007-118';
patientID = '405L_2';
patientID = ['patient' patientID];
recDate = 'yymdd';
dataPrefix = 'LFPx';
signalName = 'LFPx';
ECoGchan = ECoGChannelMap_tableRead([electrodeFilePath electrodeFile],electrodeSheet);

% NOTE: for this patient, these are channel numbers, not contact numbers.
% deliriumSet:
exempChans.nums = [116,16,15,111,34,78,201,59,51,133,256,250,66,65,53];
exempChans.ROIs = {'L_TP','L_mACG','L_mACG','L_ITG','L_Hipp','L_Amyg','L_SFG','L_OrbG','L_OrbG','L_MFG','L_IFGpop','L_SMG','L_Ins','L_Ins','L_Ins'};

exempChans.setName = setName;

cond = {};
cond{1} = 'ctrl2';
cond{2} = 'ctrl3';
cond{3} = 'ctrl4';
cond{4} = 'ctrl5';
cond{5} = 'ctrl6';

batchParams.(patientID) = struct( ...
    'recDate', recDate,'ECoGchannels', ECoGchan, ... %'dataPath', dataPath,...
    'dataPrefix',dataPrefix, 'signalName',signalName, ...
    'trialLength',trialLength, 'trialOverlap',trialOverlap);
batchParams.(patientID).exempChans = exempChans;
batchParams.(patientID).cond = cond;
%%
%423L
electrodeFile = 'LGD electrodes for analysis - sorted by ROI.xlsx';
electrodeSheet = 'L423_CRU';
patientID = '423L';
patientID = ['patient' patientID];
recDate = 'yymdd';
dataPrefix = 'LFPx';
signalName = 'LFPx';
ECoGchan = ECoGChannelMap_tableRead([electrodeFilePath electrodeFile],electrodeSheet);

% NOTE: for this patient, these are channel numbers, not contact numbers.
% deliriumSet:
exempChans.nums = [169,2,26,190,72,111,247,198,204];
exempChans.ROIs = {'L_TFG','L_OrbG','L_TP','L_IFGpop','L_Ins','L_Hipp','L_FusG','L_SMG','L_SPL'};

exempChans.setName = setName;

cond = {};
cond{1} = 'dlrm1';
cond{2} = 'dlrm2';
cond{3} = 'dlrm3';
cond{4} = 'ctrl1';
%cond{5} = 'ctrl2';
cond{5} = 'ctrl3';
cond{6} = 'ctrl4';
cond{7} = 'ctrl5';
cond{8} = 'ctrl6';
cond{9} = 'ctrl7';
cond{10} = 'ctrl8';

batchParams.(patientID) = struct( ...
    'recDate', recDate,'ECoGchannels', ECoGchan, ... %'dataPath', dataPath,...
    'dataPrefix',dataPrefix, 'signalName',signalName, ...
    'trialLength',trialLength, 'trialOverlap',trialOverlap);
batchParams.(patientID).exempChans = exempChans;
batchParams.(patientID).cond = cond;
