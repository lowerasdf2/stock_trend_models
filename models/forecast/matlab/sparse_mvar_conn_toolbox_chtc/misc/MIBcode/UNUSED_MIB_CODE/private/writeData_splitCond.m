function writeData_splitCond()
%% General parameters
computer = 'desktopPC';
% computer = 'Mac'; 
if strcmp(computer,'Mac')
    computerSpecPrefix = '/Users/bankslaptop/Box Sync/My documents/';
else
    computerSpecPrefix = 'D:\Box Sync\My documents\';
end

if ~exist('defaultPath','var')
    dataPath = [computerSpecPrefix 'Data and analysis' filesep 'ECoG data' filesep];
end
if ~exist('electrodeFilePath','var')
    electrodeFilePath = [computerSpecPrefix 'Data and analysis' filesep ...
        'ECoG data' filesep 'Electrode MNI coordinates and ROI' filesep];
end
if ~exist('outPath','var')
    outPath = [computerSpecPrefix 'Data and analysis' filesep 'ECoG data' filesep];
end

% Ensure output directory exists
if ~exist(outPath, 'dir')
    mkdir(outPath);
end
disp(['Data will be saved to `' outPath '`']);

[batchParams_gen,~] = getBatchParams(electrodeFilePath);%,defaultPath);
batchParams = batchParams_gen;

dsFs = 240;
justRunNew = 0;
segLen = 60; %in seconds. analysis will be of first and last segLen seconds in the conditions that are to be split 

%% Main analysis loop
patientIDs = fieldnames(batchParams)';
patientSet = 1:length(patientIDs);
if justRunNew
    patientSet = patientSet(patientIndx);
end
for iPatient = patientSet
    thisName = patientIDs{iPatient};
    params = batchParams.(patientIDs{iPatient});
    conditions = params.cond;
    newConditions = conditions;
    splitOut = [dataPath thisName filesep 'splitData'];
    if ~exist(splitOut, 'dir')
        mkdir(splitOut);
    end

    for iCond =1:length(conditions)
        thisCond = conditions{iCond};
        tempConditions = [];
        disp('------------------------');
        disp([thisName ' condition: ' thisCond]);
        disp('------------------------');
        disp('...splitting condition');
        if contains(thisCond,'t')
            tPos = strfind(thisCond,'t');
            val = [];
            val{1} = thisCond(tPos-1);
            val{2} = thisCond(tPos+1);
            duplCount = sum(contains(newConditions,['OAAS' val{1}]) & ~ contains(newConditions,['OAAS' val{1} 't']));
            tempConditions{1} = ['OAAS' val{1} '_' num2str(duplCount)];
            duplCount = sum(contains(newConditions,['OAAS' val{2}]) & ~ contains(newConditions,['OAAS' val{2} 't']));
            tempConditions{2} = ['OAAS' val{2} '_' num2str(duplCount)];
        else
            duplCount = sum(contains(newConditions,thisCond) & ~ contains(newConditions,[thisCond 't']));
            tempConditions{1} = [thisCond '_' num2str(duplCount)];
            tempConditions{2} = [thisCond '_' num2str(duplCount+1)];
        end
        if iCond==length(conditions)
            newConditions(length(newConditions)) = tempConditions(1);
            newConditions(length(newConditions)+1) = tempConditions(2);
        else
            iPos = find(contains(newConditions,thisCond));
            newConditions(iPos+1:length(newConditions)+1) = newConditions(iPos:end);
            newConditions(iPos) = tempConditions(1);
            newConditions(iPos+1) = tempConditions(2);
        end
        if iCond == 1
            [loadedData,params] = ...
                loadECoGData(params,[dataPath thisName filesep thisCond filesep]);
            % loadedData is matrix of nChan x nSamples
            [params.chanSide, params.chanROI, params.chanNum,...
                params.sortOrder] = getChannelInfo(params);
        else
            [loadedData,~] = ...
                loadECoGData(params,[dataPath thisName filesep thisCond filesep]);
        end
        [data_ECoG] = convertToFTFormat(loadedData,params);
        % patient 399R has artifact for first ~10 secs in ctrl condition
        if strcmp(thisName,'patient399R') && iCond == 1
            data_ECoG.trial{1} = data_ECoG.trial{1}(:,1.5e4:end);
            data_ECoG.sampleinfo = [1,size(data_ECoG.trial{1},2)];
            data_ECoG.time{1} = data_ECoG.time{1}(1:data_ECoG.sampleinfo(2));
        end

        if params.dT < 0.0025
            % downsample the data to speed up component analysis
            cfg = [];
            cfg.resamplefs = dsFs;
            cfg.detrend    = 'yes';
            % the following line avoids numeric round off issues in the time axes upon resampling
            data_ECoGds = ft_resampledata(cfg, data_ECoG);
            data_ECoGds.sampleinfo = [1, size(data_ECoGds.trial{1,1},2)];
        else
            data_ECoGds = data_ECoG;
        end
        clear data_ECoG
        
        segSet = [];
        trialStopTime = data_ECoGds.time{1}(end);
        dataSeg = [0,segLen;trialStopTime-segLen,trialStopTime];
        for iSeg = 1:length(tempConditions)
            indexNums = [find(data_ECoGds.time{1}>=dataSeg(iSeg,1),1,'first'),...
                find(data_ECoGds.time{1}<=dataSeg(iSeg,2),1,'last')];
            
            segData = data_ECoGds;
            segData.sampleinfo = indexNums-indexNums(1)+1;
            segData.trial{1} = segData.trial{1}(:,indexNums(1):indexNums(2));
            segData.time{1} = segData.time{1}(indexNums(1):indexNums(2));
            % segment data into trials of length trialLength
            cfg         = [];
            cfg.length  = params.trialLength;
            cfg.overlap = params.trialOverlap;
            segData   = ft_redefinetrial(cfg, segData);
            save([splitOut filesep tempConditions{iSeg} filesep],'segData');            
        end % loop over segments
    end %Loop over conditions
end
