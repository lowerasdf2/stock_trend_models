function [loadData] = myDataLoad(fileName)

display(['Loading ' fileName]);
loadData = load(fileName);
field = fieldnames(loadData);
loadData = loadData.(field{1});

