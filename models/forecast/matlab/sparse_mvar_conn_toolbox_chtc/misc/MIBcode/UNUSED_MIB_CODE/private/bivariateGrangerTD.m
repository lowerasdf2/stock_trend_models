%% General parameters
computer = 'Mac'; % 'desktopPC';
% computer = 'desktopPC';
if strcmp(computer,'Mac')
    computerSpecPrefix = '/Users/bankslaptop/Box Sync/My documents/';
else
    computerSpecPrefix = 'D:\Box Sync\My documents\';
end

if ~exist('dataPath','var')
    dataPath = [computerSpecPrefix 'Data and analysis' filesep 'ECoG data' filesep];
end
if ~exist('electrodeFilePath','var')
    electrodeFilePath = [computerSpecPrefix 'Data and analysis' filesep ...
        'ECoG data' filesep 'Electrode MNI coordinates and ROI' filesep];
end
if ~exist('outPath','var')
    outPath = [computerSpecPrefix 'Data and analysis' filesep 'ECoG data' filesep];
end

% Ensure output directory exists
if ~exist(outPath, 'dir')
    mkdir(outPath);
end
disp(['Data will be saved to `' outPath '`']);

batchParams = getBatchParams(electrodeFilePath);%,defaultPath);

% Connectivity analysis parameters
calcFlag.mvarGranger = 0;
calcFlag.fourierGranger = 1;
calcFlag.pdc = 0;
calcFlag.dtf = 0;
dsFs = 240;
trialToPlot = 1;
dtfYLims = [0,0.75];
mGYLims = [0,0.25];
fGYLims = [0,0.25];
bandFreqs = [4,8,13,30,80];

%% Time-domain Granger
% Assuming data1 and data2 to be variables that consist of a single channel:

patientIDs = fieldnames(batchParams)';
for iPatient = 1:length(patientIDs)
    name = patientIDs{iPatient};
%     load([outPath name '-bivarGranger.mat'],'conn','bivarSpecSum');
%     params = batchParams.(patientIDs{iPatient});
%     ECoG_conn_bivar = struct;
%     conditions = fieldnames(params)';
%     cond = conditions{1};
%     for iPair = 1:length(conn)
%         ECoG_conn_bivar.(cond).pair(iPair).fGranger = conn(iPair).fGranger;
%         ECoG_conn_bivar.(cond).bivarSpecSum = bivarSpecSum;
%     end
%     for iCond = 2:length(conditions)
    params = batchParams.(patientIDs{iPatient});
    ECoG_conn_bivarTD = struct;
    conditions = fieldnames(params)';
    for iCond = 1:length(conditions)
        cond = conditions{iCond};
        % cond = cond{1};
        disp('------------------------');
        disp(['Condition: ' cond]);
        disp('------------------------');
        [loadedData,params.(cond)] = loadECoGData(params.(cond),[dataPath filesep name filesep cond filesep]);
        % loadedData is matrix of nChan x nSamples
        [params.(cond).chanSide, params.(cond).chanROI, params.(cond).chanNum,...
            params.(cond).sortOrder] = getChannelInfo(params.(cond));
        [data_ECoG] = convertToFTFormat(loadedData,params.(cond));
        
        % downsample the data to speed up component analysis
        cfg = [];
        cfg.resamplefs = dsFs;
        cfg.detrend    = 'yes';
        % the following line avoids numeric round off issues in the time axes upon resampling
        data_ECoGds = ft_resampledata(cfg, data_ECoG);
        data_ECoGds.sampleinfo = [1, size(data_ECoGds.trial{1,1},2)];
        clear data_ECoG
        
        % segment data into trials of length trialLength with overlap
        cfg         = [];
        cfg.length  = params.(cond).trialLength;
        cfg.overlap = params.(cond).trialOverlap;
        data_ECoGds   = ft_redefinetrial(cfg, data_ECoGds);
        
        % select subset of channels for analysis
        if iCond == 1
            chanToProc = [];
            chans = params.(cond).ECoGchannels;
            for iChan = 1:length(chans)
                if chans(iChan).AEPLatLoc>0 || chans(iChan).AEPLatGlob>0 ...
                        || chans(iChan).HiGLatLoc>0 || chans(iChan).HiGLatGlob>0
                    chanToProc = [chanToProc params.(cond).sortOrder(params.(cond).chanNum == chans(iChan).chanNum)];
                end
            end
            nChans = length(chanToProc);
        end
                
        iCount = 0;
        for iChan = 1:nChans
            for jChan = iChan+1:nChans
                iCount = iCount+1;

                % compute parametric Granger
                % Model fit
                % Identify optimal model order
                [modOrd] = 16; %getMVARModelOrder(data_ECoGds,chanToProc(iChan));
                % fit multivariate autoregressive model to data
                cfg         = [];
                cfg.order   = modOrd;
                cfg.toolbox = 'bsmart';
                cfg.channel = chanToProc(iChan);
                mvar1       = ft_mvaranalysis(cfg, data_ECoGds);
                
                %[modOrd] = getMVARModelOrder(data_ECoGds,chanToProc(jChan));
                cfg         = [];
                cfg.order   = modOrd;
                cfg.toolbox = 'bsmart';
                cfg.channel = chanToProc(jChan);
                mvar2       = ft_mvaranalysis(cfg, data_ECoGds);
                
                %[modOrd] = getMVARModelOrder(data_ECoGds,[chanToProc(iChan),chanToProc(jChan)]);
                cfg         = [];
                cfg.order   = modOrd;
                cfg.toolbox = 'bsmart';
                cfg.channel = [chanToProc(iChan),chanToProc(jChan)];
                mvar12       = ft_mvaranalysis(cfg, data_ECoGds);
                
                GC1to2 = log(mvar2.noisecov(1,1)./mvar12.noisecov(2,2));
                GC2to1 = log(mvar1.noisecov(1,1)./mvar12.noisecov(1,1));
                ECoG_conn_bivarTD.(cond).pair(iCount).mvar12 = mvar12;
                ECoG_conn_bivarTD.(cond).pair(iCount).mvar1 = mvar1;
                ECoG_conn_bivarTD.(cond).pair(iCount).mvar2 = mvar2;
                ECoG_conn_bivarTD.(cond).pair(iCount).TDGranger = [GC1to2,GC2to1];
            end
        end
    end %conditions
    save([outPath name '-bivarTDGranger.mat'],'ECoG_conn_bivarTD','params');
end % loop over patients

% 
%% Cluster analysis based on fGranger sum
% load('ECoG_conn_origSet.mat','batchParams','ECoG_conn');
patientIDs = fieldnames(batchParams)';
nodeData = struct;
for iPatient = 1:length(patientIDs)
    name = patientIDs{iPatient};
    conditions = fieldnames(batchParams.(name));
    cond = conditions{1};
    chanParams = batchParams.(name).(cond).ECoGchannels;
    load([dataPath name '-bivarGranger.mat']);
    maxSum = ECoG_conn_bivarTD.(cond).bivarSpecSum;
    for iCond = 2:length(conditions)
        cond = conditions{iCond};
        maxSum = max(maxSum,ECoG_conn_bivarTD.(cond).bivarSpecSum);
    end
    chanNums = [];
    [chanNums(:,1),chanNums(:,2)] = find(maxSum>prctile(maxSum(:),99.5));
    [~,sortIndx] = sort([chanParams.sortOrder]);
    chanParams = chanParams(sortIndx);
    for iNode = 1:size(chanNums,1)
        nodeData.(name).nodesOfInterest{iNode,1} = chanNums(iNode,1);
        nodeData.(name).nodesOfInterest{iNode,2} = chanNums(iNode,2);
        nodeData.(name).nodesOfInterest{iNode,3} = chanParams(chanNums(iNode,1)).ROI;
        nodeData.(name).nodesOfInterest{iNode,4} = chanParams(chanNums(iNode,2)).ROI;
    end
    
    maskMat = maxSum>prctile(maxSum(:),99);
    testMat = maxSum;
    testMat(~maskMat) = NaN;
    iStart = 0;
    dataToClust = [];
    for iRow = 1:size(testMat,1)
        keepNodes = find(~isnan(testMat(iRow,:)));
        if ~isempty(keepNodes)
            iCount = 0;
            for iNode = 1:length(keepNodes)
                if abs(keepNodes(iNode) - iRow) > 4
                    iCount = iCount+1;
                    dataToClust(iCount+iStart,1) = iRow;
                    dataToClust(iCount+iStart,2) = keepNodes(iNode);
                end
            end
            iStart = size(dataToClust,1);
        end
    end
    for iRow = 1:size(dataToClust,1)
        if dataToClust(iRow,2) > dataToClust(iRow,1)
            temp = dataToClust(iRow,1);
            dataToClust(iRow,1) = dataToClust(iRow,2);
            dataToClust(iRow,2) = temp;
        end
    end
    figure('Name',[name 'Granger conn clusters'])
    plot(dataToClust(:,1),dataToClust(:,2),'o')
    hold on
    plot([0,size(maskMat,1)+1],[0,size(maskMat,1)+1],'--');
    ax = gca;
    ax.XLim = [0,size(maskMat,1)+1];
    ax.YLim = [0,size(maskMat,1)+1];
end

%% Cluster analysis based on fGranger max
maxCrit = 95.;
% nClust = [17,14,18];
% load('ECoG_conn_origSet.mat','batchParams','ECoG_conn');
patientIDs = fieldnames(batchParams)';
nodeData_max = struct;
for iPatient = 4:length(patientIDs)
    name = patientIDs{iPatient};
    conditions = fieldnames(batchParams.(name));
    cond = conditions{1};
    chanParams = batchParams.(name).(cond).ECoGchannels;
    load([dataPath name '-bivarGranger.mat']);
    if isfield(ECoG_conn_bivarTD.(cond),'bivarSpecMax')
        maxMax = ECoG_conn_bivarTD.(cond).bivarSpecMax;
    else
        maxMax = [];
        iCount = 0;
        for iChan = 1:length(chanParams)
            for jChan = iChan+1:length(chanParams)
                iCount = iCount+1;
                maxMax(iChan,jChan) = max(ECoG_conn_bivarTD.(cond).pair(iCount).fGranger.grangerspctrm(1,2,:),[],3);
                maxMax(jChan,iChan) = max(ECoG_conn_bivarTD.(cond).pair(iCount).fGranger.grangerspctrm(2,1,:),[],3);
            end
        end
    end
    for iCond = 2:length(conditions)
        cond = conditions{iCond};
        maxMax = max(maxMax,ECoG_conn_bivarTD.(cond).bivarSpecMax);
    end
    chanNums = [];
    [chanNums(:,1),chanNums(:,2)] = find(maxMax>prctile(maxMax(:),maxCrit));
    [~,sortIndx] = sort([chanParams.sortOrder]);
    chanParams = chanParams(sortIndx);
    for iNode = 1:size(chanNums,1)
        nodeData_max.(name).nodesOfInterest{iNode,1} = chanNums(iNode,1);
        nodeData_max.(name).nodesOfInterest{iNode,2} = chanNums(iNode,2);
        nodeData_max.(name).nodesOfInterest{iNode,3} = chanParams(chanNums(iNode,1)).ROI;
        nodeData_max.(name).nodesOfInterest{iNode,4} = chanParams(chanNums(iNode,2)).ROI;
    end
    
    maskMat = maxMax>prctile(maxMax(:),maxCrit);
    testMat = maxMax;
    testMat(~maskMat) = NaN;
    iStart = 0;
    dataToClust = [];
    for iRow = 1:size(testMat,1)
        keepNodes = find(~isnan(testMat(iRow,:)));
        if ~isempty(keepNodes)
            iCount = 0;
            for iNode = 1:length(keepNodes)
                if abs(keepNodes(iNode) - iRow) > 4
                    iCount = iCount+1;
                    dataToClust(iCount+iStart,1) = iRow;
                    dataToClust(iCount+iStart,2) = keepNodes(iNode);
                end
            end
            iStart = size(dataToClust,1);
        end
    end
    for iRow = 1:size(dataToClust,1)
        if dataToClust(iRow,2) > dataToClust(iRow,1)
            temp = dataToClust(iRow,1);
            dataToClust(iRow,1) = dataToClust(iRow,2);
            dataToClust(iRow,2) = temp;
        end
    end
    clustFig = figure('Name',[name 'Granger conn clusters - max']);
    plot(dataToClust(:,1),dataToClust(:,2),'o')
    hold on
    plot([0,size(maskMat,1)+1],[0,size(maskMat,1)+1],'--');
    ax = gca;
    ax.XLim = [0,size(maskMat,1)+1];
    ax.YLim = [0,size(maskMat,1)+1];
    clustVals = [];
    [clustVals(:,1),clustVals(:,2)] = ginput(100);

    figure(clustFig);
    for iClust = 1:size(clustVals,1)
        plot(clustVals(iClust,1),clustVals(iClust,2),'kx','MarkerSize',12)
    end
    
    chanVals = sort(unique(round(clustVals(:))));    
    for iNode = 1:length(chanVals)
        nodeData_max.(name).nodesToKeep{iNode,1} = chanVals(iNode);
        nodeData_max.(name).nodesToKeep{iNode,2} = [chanParams(chanVals(iNode)).side '-' chanParams(chanVals(iNode)).ROI];
        nodeData_max.(name).nodesToKeep{iNode,3} = chanParams(chanVals(iNode)).chanNum;
    end

%     opts = statset('Display','final');
%     [idx,C] = kmeans(dataToClust,nClust(iPatient),'Distance','cityblock',...
%         'Replicates',5,'Options',opts);

end

%%
%% Network analysis - threshold via SEMs
patientIDs = fieldnames(batchParams)';

methodList = {'assortativity','betweenness','clustering_coef',...
    'degrees','density','edge_betweenness','transitivity'};

for iMethod = 1:length(methodList)
    thisMethod = methodList{iMethod};
    for iPatient = 1:length(patientIDs)
        thisName = patientIDs{iPatient};
        params = batchParams.(patientIDs{iPatient});
        conditions = fieldnames(params)';
        iCount = 0;
        for iCond = 1:length(conditions)
            iCount = iCount+1;
            cond = conditions{iCond};
            OAASVal = getOAASVal(cond);
            bands = fieldnames(ECoG_conn.WPLI.(thisName).(cond))';
            for iBand = 1:length(bands)
                thisBand = bands{iBand};
                tempConn = ECoG_conn.WPLI.(thisName).(cond).(thisBand);
                tempMeas = abs(tempConn.wpli_debiasedspctrm);
                tempMeas = tril(tempMeas); %Just lower triangular portion of matrix, since it is symmetric
                testTiny = tempMeas<tinyCriterion;
                tempMeas(testTiny) = NaN;
                tempSEM = tempConn.wpli_debiasedspctrmsem;
                tempConn.wpli_debiasedspctrm = tempMeas./tempSEM;
                cfg           = [];
                cfg.method    = thisMethod;
                cfg.parameter = 'wpli_debiasedspctrm';
                cfg.threshold = 2;
                tempNet = ft_networkanalysis(cfg,tempConn);
                tempVals = tempNet.(thisMethod);
                netStats.(thisMethod).(thisName).(thisBand).vals(iCount,:) = tempVals(:);
                netStats.(thisMethod).(thisName).(thisBand).OAASVal(iCount,:) = OAASVal;
            end
        end
    end
end

% methodList = {'assortativity','betweenness','clustering_coef',...
%     'degrees','density','edge_betweenness','transitivity'};

alpha=.05;
iCount = 0;
for iMethod = 1:length(methodList)
    thisMethod = methodList{iMethod};
    disp(['******' thisMethod ' stats*******']);
    for iBand = 1:length(bands)
        thisBand = bands{iBand};
        xVals = [];
        yVals = [];
        for iPatient = 1:length(patientIDs)
            thisName = patientIDs{iPatient};
            xVals = [xVals; netStats.(thisMethod).(thisName).(thisBand).OAASVal];
            theseVals = netStats.(thisMethod).(thisName).(thisBand).vals;
            yValsTemp = zeros(size(theseVals,1),1);
            for iRow = 1:size(theseVals,1)
                tempVals = theseVals(iRow,:);
                yValsTemp(iRow,1) = median(tempVals(tempVals~=0),'omitnan');
            end
            yVals = [yVals; yValsTemp];
        end
        xVals=[ones(size(xVals)) xVals];
        % do it!
        [rcoeff,confI,r,rint,stats]=regress(yVals,xVals,alpha);
        iCount = iCount+1;
        pVal(iCount) = stats(3);
        disp([thisBand ' stats:']);
        disp(['linear regression y=a+b*x: a=' num2str(rcoeff(1)) '; b=' num2str(rcoeff(2)) ...
            '; r^2=' num2str(stats(1)) '; p=' num2str(stats(3))]);
    end
end

[corrP] = my_fdrCorrect(pVal,alpha);
[corrected_p, h]=bonf_holm(sort(pVal),alpha);


yLim = [-0.5,0.5; 0,85; 0.1,0.3; 0,100; 0,0.3; 2,12; 0.05,0.3];
for iMethod = 1:length(methodList)
    thisMethod = methodList{iMethod};
    figName = [thisMethod ' - by OAASVal'];
    netFig = figure('Name',figName);
    for iPatient = 1:length(patientIDs)
        thisName = patientIDs{iPatient};
        for iBand = 1:length(bands)
            subplot(length(bands),1,iBand);
            hold on
            thisBand = bands{iBand};
            xValsToPlot = netStats.(thisMethod).(thisName).(thisBand).OAASVal;
            theseVals = netStats.(thisMethod).(thisName).(thisBand).vals;
            yValsToPlot = zeros(size(theseVals,1),1);
            for iRow = 1:size(theseVals,1)
                tempVals = theseVals(iRow,:);
                yValsToPlot(iRow,1) = median(tempVals(tempVals~=0),'omitnan');
            end
            plot(xValsToPlot,yValsToPlot,'-o');
            ax = gca;
            ax.XLim = [0.5 5.5];
            ax.XDir = 'rev';
            ax.XTick = [1:5];
            ax.YLim = yLim(iMethod,:);
            ax.Title.String = thisBand;
            if iBand == length(bands)
                ax.XLabel.String = 'OAAS';
                ax.YLabel.String = thisMethod;
            else
                ax.XTickLabel = [];
            end
            if iBand == 1
                legend(patientIDs);
            end
        end
    end
    saveas(netFig,[outPath figName]);
end

