function [data] = convertToFTFormat(loadedData,params)
% Converts ECoG data loaded by loadECoGData into FieldTrip-compatible
% format.
% loadedData is nChans x nPts matrix
% Assumes all channels are of interest

[nChans,nPts] = size(loadedData);
% % Next re-order data according to sortOrder
% [~, sortIndex] = sort(params.sortOrder);
% loadedData = loadedData(sortIndex,:);
% create labels based on sorted data
for iChan = 1:nChans
    data.label{iChan} = ...
        [params.chanSide{iChan} '_' params.chanROI{iChan} '-Ch' ...
        num2str(params.chanNum(iChan))];
end
% data.label is a 1xnChans cell-array containing string labels for
% each channel
data.fsample =   1/params.dT;
% sampling frequency in Hz, single number
data.sampleinfo = [1, nPts];
data.trial{1} = loadedData;      
% cell-array containing a data matrix for each
% trial (1 X Ntrial), each data matrix is a Nchan*Nsamples matrix
data.time{1} = [0:nPts-1]*params.dT;
% data.time is a cell-array containing a time axis for each
% trial (1 X Ntrial), each time axis is a 1*Nsamples vector
