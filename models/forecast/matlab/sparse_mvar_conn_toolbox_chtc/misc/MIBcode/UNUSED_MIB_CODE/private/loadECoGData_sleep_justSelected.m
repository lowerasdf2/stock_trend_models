function [loadedData, stageData, params] = loadECoGData_sleep_justSelected(params,thisPath)
%loadECoGData  Load and concatenate ECoG data from Howard lab.
%
% Returns an array of filtered data organized as follows:
%   (channel) x (sample)
%
% params.sleepData.stage
% params.sleepData.time
% params.sleepData.key

if ~isfield(params,'analysisSeg')
    error('Missing analysis segments parameter!');
else
    tLim = params.analysisSeg*60; %sec
    tStart = tLim(1);
    tStop = tLim(2);
end
nChans = length(params.ECoGchannels);
signalName = 'LFPx';
% Get all data files
dataFiles = dir([thisPath '*.mat']);
theseFiles = {dataFiles.name};
theseFiles = theseFiles(~contains(theseFiles,'sleepData'));
theseFiles = theseFiles(~contains(theseFiles,'stageVal'));
loadedData = [];
stageData = [];
stageKey = [];
tOffset = 0;
timeVec = [];
for jFile = 1:length(theseFiles)
    fileName = theseFiles{jFile};
    fileObj = matfile([thisPath fileName]);
    fileDetails = whos(fileObj);
    dataFields = {fileDetails.name};
    chanFields = dataFields(contains(dataFields,signalName));
    dataCheck = load([thisPath fileName],chanFields{1});
    nPts = length(dataCheck.(chanFields{1}).dat);
    dT = 1/dataCheck.(chanFields{1}).fs; %sec
    timeVec = (1:nPts)*dT+tOffset;
    timeIndex = timeVec>=tStart & timeVec<=tStop;
    if sum(timeIndex)>0
        load([thisPath fileName],dataFields{contains(dataFields,'STAGE')});
        STAGE = STAGE';
        STAGE_KEY = STAGE_KEY';
        chanNum = zeros(1,length(chanFields));
        chanStr = {};
        for iChan = 1:length(chanFields)
            thisChan = chanFields{iChan};
            chanStr{iChan} = thisChan(strfind(thisChan,signalName)+length(signalName):strfind(thisChan,'_')-1);
            chanNum(iChan) = str2num(chanStr{iChan});
        end
        [chanNum,sortIndex] = sort(chanNum);
        chanStr = chanStr(sortIndex);
        chanFields = chanFields(sortIndex);
        for iChan = 1:nChans
            thisChan = params.ECoGchannels(iChan).chanNum;
            display(['Loading ch#' num2str(thisChan)]);
            dataToLoad = chanFields{chanNum==thisChan};
            thisData = load([thisPath fileName],dataToLoad);
            if iChan == 1
                tempData = zeros(nChans,length(thisData.(dataToLoad).dat));
                params.dT = 1/thisData.(dataToLoad).fs; %sec
            end
            tempData(iChan,:) = thisData.(dataToLoad).dat;
        end
        loadedData = [loadedData tempData(:,timeIndex)];
        stageData = [stageData STAGE(timeIndex)];
        stageKey = [stageKey STAGE_KEY];
    end
    tOffset = tOffset + nPts*dT;
end %loop over data files
params.nPts = size(loadedData,2);
params.STAGE_KEY = unique(stageKey);

