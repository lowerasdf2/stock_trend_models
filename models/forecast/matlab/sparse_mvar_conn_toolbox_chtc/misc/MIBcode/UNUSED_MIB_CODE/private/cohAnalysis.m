function cohAnalysis()

%% General parameters
% computer = 'Mac'; % 'desktopPC';
computer = 'desktopPC';
if strcmp(computer,'Mac')
    computerSpecPrefix = '/Users/bankslaptop/Box Sync/My documents/';
else
    computerSpecPrefix = 'D:\Box Sync\My documents\';
end

if ~exist('defaultPath','var')
    dataPath = [computerSpecPrefix 'Data and analysis' filesep 'ECoG data' filesep];
end
if ~exist('electrodeFilePath','var')
    electrodeFilePath = [computerSpecPrefix 'Data and analysis' filesep ...
        'ECoG data' filesep 'Electrode MNI coordinates and ROI' filesep];
end
if ~exist('outPath','var')
    outPath = [computerSpecPrefix 'Data and analysis' filesep 'ECoG data' filesep];
end

% Ensure output directory exists
if ~exist(outPath, 'dir')
    mkdir(outPath);
end
disp(['Data will be saved to `' outPath '`']);

[batchParams_gen,setName] = getBatchParams(electrodeFilePath);%,defaultPath);
patientIDs_gen = fieldnames(batchParams_gen);

if exist([outPath 'ECoG_conn_' setName '_withCoh.mat'],'file')
    load([outPath 'ECoG_conn_' setName '_withCoh.mat'],'ECoG_conn','batchParams');
    patientIDs_load = fieldnames(ECoG_conn.coh);
    %Figure out if there are new patients in getBatchParams
    genIndx = ~ismember(patientIDs_gen,patientIDs_load);
    patientsToAdd = patientIDs_gen(genIndx);
    if ~isempty(patientsToAdd)
        for iPatient = 1:length(patientsToAdd)
            batchParams.(patientsToAdd{iPatient}) = batchParams_gen.(patientsToAdd{iPatient});
        end
    end
    %Now get indices for added patients, in case just want to analyze those
    patientIDs = fieldnames(batchParams);
    patientIndx = ~ismember(patientIDs,patientIDs_load);
else
    ECoG_conn = struct;
    batchParams = batchParams_gen;
    patientIDs = fieldnames(batchParams);
    patientIndx = 1:length(patientIDs);
end

% if isfield(ECoG_conn,'WPLI')
%     ECoG_conn = rmfield(ECoG_conn,'WPLI');
% end

% Freq analysis params
tinyCriterion = 0.01;
bands = {'delta','theta','alpha','beta','gamma'};%,'highGamma'};
dsFs = 250;
freqsOfInterest = 1:120; %Hz
bandFreqs = [4,8,13,30];
specYLims = [1.e-2, 1.e0];
justRunNew = 1;

%% Main analysis loop
patientIDs = fieldnames(batchParams)';
patientSet = 1:length(patientIDs);
if justRunNew
    patientSet = patientSet(patientIndx);
end
for iPatient = patientSet
    thisName = patientIDs{iPatient};
    params = batchParams.(patientIDs{iPatient});
    conditions = params.cond;
    for iCond = 1:length(conditions)
        thisCond = conditions{iCond};
        % cond = cond{1};
        disp('------------------------');
        disp(['Patient ' thisName ' - Condition: ' thisCond]);
        disp('------------------------');
        [loadedData,params] = loadECoGData(params,[dataPath filesep thisName filesep thisCond filesep]);
        % loadedData is matrix of nChan x nSamples
        [params.chanSide, params.chanROI, params.chanNum,...
            params.sortOrder] = getChannelInfo(params);
        [data_ECoG] = convertToFTFormat(loadedData,params);
        % patient 399R has artifact for first ~10 secs in ctrl condition
        if strcmp(thisName,'patient399R') && iCond == 1
            data_ECoG.trial{1} = data_ECoG.trial{1}(:,1.5e4:end);
            data_ECoG.sampleinfo = [1,size(data_ECoG.trial{1},2)];
            data_ECoG.time{1} = data_ECoG.time{1}(1:data_ECoG.sampleinfo(2));
        end
        
        if params.dT < 0.0025
            % downsample the data to speed up component analysis
            cfg = [];
            cfg.resamplefs = dsFs;
            cfg.detrend    = 'yes';
            % the following line avoids numeric round off issues in the time axes upon resampling
            data_ECoGds = ft_resampledata(cfg, data_ECoG);
            data_ECoGds.sampleinfo = [1, size(data_ECoGds.trial{1,1},2)];
        else
            data_ECoGds = data_ECoG;
        end
        clear data_ECoG

        % segment data into trials of length trialLength with overlap
        cfg         = [];
        cfg.length  = params.trialLength;
        cfg.overlap = params.trialOverlap;
        data_ECoGds   = ft_redefinetrial(cfg, data_ECoGds);
        
        % compute Fourier spectra, to be used for cross-spectral density computation.
        cfg            = [];
        cfg.method     = 'mtmfft';
        cfg.output     = 'fourier';
        cfg.foi        = freqsOfInterest;
        cfg.taper     = 'dpss';
        cfg.tapsmofrq = 1;
        cfg.pad       = ceil(max(cellfun(@numel, data_ECoGds.time)/data_ECoGds.fsample));
        cfg.keeptrials = 'yes';
        FFT_ECoG       = ft_freqanalysis(cfg, data_ECoGds);
        
        % Compute coherence spectra
        cfg=[];
        cfg.method  ='coh';
        cfg.jackknife = 'yes';
        ECoG_conn.coh.(thisName).(thisCond) = ft_connectivityanalysis(cfg,FFT_ECoG);
    end %Loop over conditions
    batchParams.(patientIDs{iPatient}) = params;
end
save([outPath 'ECoG_conn_' setName '_withCoh.mat'],'ECoG_conn','batchParams');

%% Plot coherence spectra - exemplary channels
patientIDs = fieldnames(batchParams)';
for iPatient = 1:length(patientIDs)
    thisName = patientIDs{iPatient};
    figName = [thisName '-CoherenceSpec-ExemplaryChans'];
    cohFig = figure('Name',figName);
    params = batchParams.(thisName);
    conditions = params.cond;
    for iCond = 1:length(conditions)
        thisCond = conditions{iCond};
        if iCond == 1
            chans = params.exempChans;
            nCols = length(chans.nums)-1;
            nRows = nCols;
            panToLabel = (nRows-1)*nCols + 1;
        end
        for iChan = 2:length(chans.nums)
            thisIndex = params.chanNum == chans.nums(iChan);
            thisChan = params.chanNum(thisIndex);
            for jChan = 1:iChan-1
                thatIndex = params.chanNum == chans.nums(jChan);
                thatChan = params.chanNum(thatIndex);
                iRow = iChan-1;
                iCol = jChan;
                iPan = iCol+(iRow-1)*nCols;
                subplot(nRows,nCols,iPan);
                hold on
                plot(ECoG_conn.coh.(thisName).(thisCond).freq,...
                    squeeze(ECoG_conn.coh.(thisName).(thisCond).cohspctrm(thisChan,thatChan,:)),'LineWidth',2);
                if iCond == length(conditions)
                    for iBand = 1:length(bandFreqs)
                        plot([bandFreqs(iBand),bandFreqs(iBand)],specYLims,':k');
                    end
                    ax = gca;
                    ax.XScale = 'log';
                    ax.XLim = [min(freqsOfInterest),max(freqsOfInterest)];
                    ax.YLim = specYLims;
                    ax.YScale = 'log';
                    if iCol == iRow
                        ax.Title.String = [chans.ROIs{jChan} '-' num2str(chans.nums(jChan))...
                            ':'  chans.ROIs{iChan} '-' num2str(chans.nums(iChan))];
                    else
                        ax.Title.String = ['   :'  chans.ROIs{iChan} '-' num2str(chans.nums(iChan))];
                    end
                    ax.Title.Interpreter = 'none';
                    if iChan == panToLabel
                        legend(conditions,'Location','SouthWest');
                        ax.XLabel.String = 'Freq (Hz)';
                        ax.YLabel.String = 'Coherence';
                    end
                end
            end
        end
    end
    legend(conditions,'Location','SouthWest');
    saveas(cohFig,[outPath figName]);
end

%% Plot coherence spectra - exemplary ROIs
patientIDs = fieldnames(batchParams)';
for iPatient = 1:length(patientIDs)
    thisName = patientIDs{iPatient};
    params = batchParams.(patientIDs{iPatient});
    conditions = params.cond;
    figName = [thisName '-CoherenceSpec-ExemplaryROIs'];
    cohFig = figure('Name',figName);
    for iCond = 1:length(conditions)
        thisCond = conditions{iCond};
        if iCond == 1
            if exist('ROIs','var')
                clear ROIs
            end
            if exist('tempROI','var')
                clear tempROI
            end
            chans = params.exempChans;
            for iChan = 1:length(params.chanROI)
                tempROI{iChan} = [params.chanSide{iChan} '_' params.chanROI{iChan}];
            end
            % Get channel numbers corresponding to ROIs
            for iROI = 1:length(chans.ROIs)
                roiIndex = strcmp(tempROI,chans.ROIs{iROI});
                ROIs(iROI).chans = params.chanNum(roiIndex);
                ROIs(iROI).label = chans.ROIs{iROI};                
            end
            nCols = length(chans.ROIs)-1;
            nRows = nCols;
            panToLabel = (nRows-1)*nCols + 1;
        end
        tempMeas = NaN(length(chans.ROIs),length(chans.ROIs),length(ECoG_conn.coh.(thisName).(thisCond).freq));
        for iROI = 2:length(chans.ROIs)
            for jROI = 1:iROI-1
                valROIMat = ECoG_conn.coh.(thisName).(thisCond).cohspctrm(ROIs(iROI).chans,ROIs(jROI).chans,:);
                tempMeas(iROI,jROI,:) = mean(mean(valROIMat,1,'omitnan'),2,'omitnan');
            end
        end
        for iROI = 2:length(chans.ROIs)
            for jROI = 1:iROI-1
                iRow = iROI-1;
                iCol = jROI;
                iPan = iCol+(iRow-1)*nCols;
                subplot(nRows,nCols,iPan);
                hold on
                plot(ECoG_conn.coh.(thisName).(thisCond).freq,...
                    squeeze(tempMeas(iROI,jROI,:)),'LineWidth',2);
                if iCond == length(conditions)
                    for iBand = 1:length(bandFreqs)
                        plot([bandFreqs(iBand),bandFreqs(iBand)],specYLims,':k');
                    end
                    ax = gca;
                    ax.XScale = 'log';
                    ax.XLim = [min(freqsOfInterest),max(freqsOfInterest)];
                    ax.YLim = specYLims;
                    ax.YScale = 'log';
                    if iCol == iRow
                        ax.Title.String = [chans.ROIs{jROI} '-' ...
                            ':'  chans.ROIs{iROI}];
                    else
                        ax.Title.String = ['   :'  chans.ROIs{iROI}];
                    end
                    ax.Title.Interpreter = 'none';
                    if iChan == panToLabel
                        legend(conditions,'Location','SouthWest');
                        ax.XLabel.String = 'Freq (Hz)';
                        ax.YLabel.String = 'Coherence';
                    end
                end
            end
        end
    end
    legend(conditions,'Location','SouthWest');
    saveas(cohFig,[outPath figName]);
end
%% Band coherence versus OAAS
bandFreqs = [1,4,8,13,30,80];

lev = struct; %Stores the channel numbers in each ROI
level = struct; %Stores the names of the ROIs in the hierarchy
%This order determines the order in the hierarchy specified in pairs below
% level.HGPM = {'HGPM'};
% level.STP = {'HGAL','PT','PP'};
% level.STG = {'STG'};
level.Core = {'HGPM'};
level.NonCore = {'STG','HGAL','PT','PP'};
level.AudRel = {'Aud-rel'};
level.PFC = {'PFC'};
levelNames = fieldnames(level);
nLevs = length(levelNames);
%Pairs specifies which pairs of areas we are focusing for connectivity 
%analysis as function of OAAS
pairs = [1,2; 1,3; 1,4; 2,3; 2,4; 3,4; 2,1; 3,1; 4,1; 3,2; 4,2; 4,3];
nPairs = size(pairs,1);
for iPair = 1:nPairs
    pairName{iPair} = [levelNames{pairs(iPair,1)} '-' levelNames{pairs(iPair,2)}];
end
yMax = ones(1,nPairs)*0.3;
yMax(1) = 0.6;
yMax(2:5) = 0.4;
levCoh = struct;

%
patientIDs = fieldnames(batchParams)';
for iPatient = 1:length(patientIDs)
    thisName = patientIDs{iPatient};
%     figName = [thisName '-WPLI by dist - Z thresh'];
%     distFig = figure('Name',figName);
    params = batchParams.(patientIDs{iPatient});
    conditions = params.cond;
    iCount = 0;
    for iCond = 1:length(conditions)
        iCount = iCount+1;
        thisCond = conditions{iCond};
        OAASVal = getOAASVal(thisCond);
        if iCond == 1
            if exist('lev','var')
                clear lev
            end
            chans = params.exempChans;
            % Get channel numbers corresponding to ROIs
            for iLev = 1:nLevs
                thisLevel = levelNames{iLev};
                lev(iLev).chans = [];
                for iROI = 1:length(level.(thisLevel))
                    roiIndex = strcmp(params.chanROI,level.(thisLevel){iROI});
                    lev(iLev).chans = [lev(iLev).chans, params.chanNum(roiIndex)];
                end
                lev(iLev).label = thisLevel;
            end
        end
        bands = fieldnames(ECoG_conn.WPLI.(thisName).(thisCond))';
        tempMeas = NaN(nLevs,nLevs,length(ECoG_conn.coh.(thisName).(thisCond).freq));
        for iLev = 2:nLevs
            for jLev = 1:iLev-1
                valLevMat = ECoG_conn.coh.(thisName).(thisCond).cohspctrm(lev(iLev).chans,lev(jLev).chans,:);
                tempMeas(iLev,jLev,:) = mean(mean(valLevMat,1,'omitnan'),2,'omitnan');
            end
        end
        for iBand = 1:length(bands)
            thisBand = bands{iBand};
            for iPair = 1:nPairs
                % Make sure to grab values from lower triangle of matrix;
                % upper is NaN
                thisLev = pairs(iPair,2);
                thatLev = pairs(iPair,1);
                levCoh.(thisName).(thisBand).meanVal(iPair,iCount) = mean(tempMeas(thisLev,thatLev,bandFreqs(iBand):bandFreqs(iBand+1)));
            end
            levCoh.(thisName).(thisBand).OAASVal(iCount) = OAASVal;
        end
    end
end

for iPatient = 1:length(patientIDs)
    thisName = patientIDs{iPatient};
    params = batchParams.(patientIDs{iPatient});
    conditions = params.cond;
    bands = fieldnames(ECoG_conn.WPLI.(thisName).(conditions{1}))';
    for iBand = 1:length(bands)
        thisBand = bands{iBand};
        uniqueCond = unique(levCoh.(thisName).(thisBand).OAASVal);
        uniqueCond = sort(uniqueCond,'descend');
        if length(uniqueCond) ...
                < length(levCoh.(thisName).(thisBand).OAASVal)
            meas = fieldnames(levCoh.(thisName).(thisBand));
            for iMeas = 1:length(meas)
                thisMeas = meas{iMeas};
                if ~strcmp(thisMeas,'OAASVal')
                    tempVals = levCoh.(thisName).(thisBand).(thisMeas);
                    levCoh.(thisName).(thisBand).(thisMeas) = zeros(nPairs,length(uniqueCond));
                    for iCond = 1:length(uniqueCond)
                        condIndex = levCoh.(thisName).(thisBand).OAASVal==uniqueCond(iCond);
                        levCoh.(thisName).(thisBand).(thisMeas)(:,iCond) = mean(tempVals(:,condIndex),2);
                    end
                end
            end
            levCoh.(thisName).(thisBand).OAASVal = uniqueCond;
        end
    end
end

for iBand = 1:length(bands)
    thisBand = bands{iBand};
    for iPair = 1:nPairs
        xVals = [];
        yVals = [];
        for iPatient = 1:length(patientIDs)
            thisName = patientIDs{iPatient};
            xVals = [xVals; levCoh.(thisName).(thisBand).OAASVal'];
            yVals = [yVals; levCoh.(thisName).(thisBand).meanVal(iPair,:)'];
        end
        xVals=[ones(size(xVals)) xVals];
        alpha=.05;
        % do it!
        [rcoeff,confI,r,rint,stats]=regress(yVals,xVals,alpha);
        disp([thisBand ' - ' pairName{iPair} ' stats:']);
        disp(['linear regression y=a+b*x: a=' num2str(rcoeff(1)) '; b=' num2str(rcoeff(2)) ...
            '; r^2=' num2str(stats(1)) '; p=' num2str(stats(3))]);
    end
end

figName = ['Hierarchy Coherence - by OAASVal'];
hierFig = figure('Name',figName);
for iPatient = 1:length(patientIDs)
    thisName = patientIDs{iPatient};
    for iBand = 1:length(bands)
        for iPair = 1:nPairs
            subplot(length(bands),nPairs,iPair+(iBand-1)*nPairs);
            hold on
            thisBand = bands{iBand};
            plot(levCoh.(thisName).(thisBand).OAASVal,levCoh.(thisName).(thisBand).meanVal(iPair,:),'-o');
            ax = gca;
            ax.XLim = [0.5 5.5];
            ax.XDir = 'rev';
            ax.XTick = [1:5];
            ax.YLim = [0.0 yMax(iPair)];
            if iPair == 1
                
            end
            if iPair == 1
                text(5,0.075,thisBand);
            end
            if iBand == length(bands)
                ax.XLabel.String = 'OAAS';
                ax.YLabel.String = 'Mean Coh';
            else
                ax.XTickLabel = [];
            end
            if iBand == 1
                ax.Title.String = pairName{iPair};
            end
            if iBand == length(bands) && iPair == nPairs
                legend(patientIDs);
            end
        end
    end
end
saveas(hierFig,[outPath figName]);
