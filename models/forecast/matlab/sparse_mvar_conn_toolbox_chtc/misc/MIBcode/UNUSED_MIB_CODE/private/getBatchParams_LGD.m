batchParams = struct;
defaultPath = '/Users/bankslaptop/Box Sync/My documents/Data and analysis/ECoG data/';
% defaultPath = 'C:\Users\Banks_admin\Box Sync\My documents\Data and analysis\ECoG data\';

%% Patients for WPLI analysis
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%369LR
electrodeFile = '369R_Electrode_Sites_KN_sorted_by_ROI.xlsx';
electrodeSheet = 'Rev 20170308';
patientID = '369LR';
patientID = ['patient' patientID];
recDate = 'yymdd';
dataPrefix = 'LFPx_RZ2_chn';
signalName = 'LFPx';
ECoGchan = ECoGChannelMap([defaultPath patientID filesep electrodeFile],electrodeSheet);
% exempChans.nums = [48,105,50,205,171,168,148,238,251]; 
% exempChans.ROIs = {'HGPM','HGAL','PT','STGP','STGM','MTG','SMG','AG','IFGop','MFG'};
exempChans.nums = [47,105,50,188,221,171,168,238,251]; 
exempChans.ROIs = {'HGPM','HGAL','PT','STGP','STGM','MTG','SMG','IFGop','MFG'};

cond{1} = 'OAAS5_1';
cond{2} = 'OAAS5_2';
cond{3} = 'OAAS5t4';
cond{4} = 'OAAS3t1';
for iCond = 1:length(cond)
    dataPath = [defaultPath patientID filesep 'LGD-OR' filesep cond{iCond} filesep];
    batchParams.(patientID).(cond{iCond}) = struct( ...
        'recDate', recDate,'ECoGchannels', ECoGchan, 'dataPath', dataPath,...
        'dataPrefix',dataPrefix, 'signalName',signalName);
    batchParams.(patientID).(cond{iCond}).exempChans = exempChans;
end

%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%372L
% batchParams = struct;
electrodeFile = '372L_Electrode_Sites_KN_sorted-By_ROI';
electrodeSheet = 'Rev 20170324';
patientID = '372L';
patientID = ['patient' patientID];
recDate = 'yymdd';
dataPrefix = 'LFPx_RZ2_chn';
signalName = 'LFPx';
ECoGchan = ECoGChannelMap([defaultPath patientID filesep electrodeFile],electrodeSheet);
exempChans.nums = [79,101,82,172,133,185,191,253,234]; % pmHG,alHG,PT,STG,MTG,SMG,IFG,MFG
exempChans.ROIs = {'HGPM','HGAL','PT','STGP','STGM','MTG','SMG','IFGop','MFG'};

cond{1} = 'OAAS5t4';
cond{2} = 'OAAS4t3';
cond{3} = 'OAAS2t1';
cond{4} = 'OAAS1';
for iCond = 1:length(cond)
    dataPath = [defaultPath patientID filesep 'LGD-OR' filesep cond{iCond} filesep];
    batchParams.(patientID).(cond{iCond}) = struct( ...
        'recDate', recDate,'ECoGchannels', ECoGchan, 'dataPath', dataPath,...
        'dataPrefix',dataPrefix, 'signalName',signalName);
    batchParams.(patientID).(cond{iCond}).exempChans = exempChans;
end

%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%376R
% batchParams = struct;
electrodeFile = '376R_Electrode_Sites_KN_sorted_by_ROI';
electrodeSheet = 'Rev 20170413';
patientID = '376R';
patientID = ['patient' patientID];
recDate = 'yymdd';
dataPrefix = 'LFPx_RZ2_chn';
signalName = 'LFPx';
ECoGchan = ECoGChannelMap([defaultPath patientID filesep electrodeFile],electrodeSheet);
exempChans.nums = [78,82,49,221,253,203,190,133,147]; % pmHG,alHG,PT,STG,MTG,SMG,IFG,MFG
exempChans.ROIs = {'HGPM','HGAL','PT','STGP','STGM','MTG','SMG','IFGop','MFG'};

cond{1} = 'OAAS5';
cond{2} = 'OAAS5t4';
cond{3} = 'OAAS4t1';
cond{4} = 'OAAS1';
for iCond = 1:length(cond)
    dataPath = [defaultPath patientID filesep 'LGD-OR' filesep cond{iCond} filesep];
    batchParams.(patientID).(cond{iCond}) = struct( ...
        'recDate', recDate,'ECoGchannels', ECoGchan, 'dataPath', dataPath,...
        'dataPrefix',dataPrefix, 'signalName',signalName);
    batchParams.(patientID).(cond{iCond}).exempChans = exempChans;
end
