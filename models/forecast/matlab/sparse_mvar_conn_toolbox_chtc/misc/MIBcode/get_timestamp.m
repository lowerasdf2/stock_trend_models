function [tstamp,tdt_time] = get_timestamp(evnt)

% [tstamp,tdt_time] = get_timestamp(evnt)
%
% Get matlab-usable timestamps from the the evnt struct. 
%
% Input:
%       evnt - Event struct saved in variable 'evnt'. 
% Outputs:
%         tstamp - matlab format universal time for each time stamp. 
%         tdt_time - TDT recording time (secs. from start of record).
%


nevs = 6;
intvl = .1;

gron= diff([0,evnt.time])>intvl & diff([evnt.time,Inf])<intvl& diff([evnt.time(2:end),Inf,Inf])<intvl;

ex(round(evnt.time*100))=evnt.evnt;
[T,t] = chopper([-.05 .2],evnt.time(gron),100);
T(T>length(ex))=length(ex);
T(T<1)=1;
T = flipud(T);

bitN = (cumsum(ex(T)>0)-1).*(ex(T)>0);

tstamp = sum(ex(T).*2.^(8*bitN))/1e3/(24*3600);

tstamp(sum(ex(T)>0)~=nevs)=nan;

tdt_time = evnt.time(gron);

tdt_time(isnan(tstamp))= [];
tstamp(isnan(tstamp))=[];