function [sortIndx] = chanSort(params,onlyAudHierarchy)

audHierarchy = {'HGPM','HGAL','PT','PP','STG','Aud-rel','PFC'};
ROIList = {'HGPM','HGAL','PT','PP','STG','Aud-rel','PFC','SensMot','Sensorimotor','Other'};
newROI = {params.ECoGchannels.newROI};
if isfield(params.ECoGchannels,'newROINum')
    newROINum = [params.ECoGchannels.newROINum];
else
    newROINum = zeros(1,length(newROI))+99;
    for iROI = 1:length(ROIList)
        newROINum(contains(newROI,ROIList{iROI})) = iROI;
    end
end
xCoord = [params.ECoGchannels.xCoord];
yCoord = [params.ECoGchannels.yCoord];
zCoord = [params.ECoGchannels.zCoord];
HGPMXPos = mean(xCoord(newROINum==1));
HGPMYPos = mean(yCoord(newROINum==1));
HGPMZPos = mean(zCoord(newROINum==1));
HGPMDist = ((xCoord-HGPMXPos).^2 + (yCoord-HGPMYPos).^2 + (zCoord-HGPMZPos).^2).^0.5;
HGPMDist(isnan(HGPMDist)) = 99;
mat2Sort = [newROINum' HGPMDist'];

[sortedMat,sortIndx] = sortrows(mat2Sort,[1,2]);

if onlyAudHierarchy
    sortIndx = sortIndx(sortedMat(:,1)<=length(audHierarchy));
end
