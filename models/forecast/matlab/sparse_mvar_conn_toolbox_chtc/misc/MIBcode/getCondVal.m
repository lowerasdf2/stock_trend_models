function condVal = getCondVal(cond)

switch cond
    case 'ctrl'
        condVal = 1;
    case 'dlrm'
        condVal = 2;
    case 'recv'
        condVal = 3;
    otherwise
        condVal = NaN;
end
