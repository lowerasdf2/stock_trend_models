function batch_ECoG_analysis(batchParams, outPath, bands, trialLength, trialOverlap)

if ~exist('batchParams', 'var')
    getBatchParams;
end

if ~exist('outPath', 'var')
    outPath = '/Users/bankslaptop/Box Sync/My documents/Data and analysis/ECoG data/';
%     outPath = 'C:\Users\Banks_admin\Box Sync\My documents\Data and analysis\ECoG data\';
end
% Ensure output directory exists
if ~exist(outPath, 'dir')
    mkdir(outPath);
end
disp(['Data will be saved to `' outPath '`']);

if ~exist('bands', 'var')
    bands = {'delta','theta','alpha','beta','gamma'};
end
if ~exist('trialLength', 'var')
    trialLength = 10; %seconds
end
if ~exist('trialOverlap', 'var')
    trialOverlap = 0;
end

patientIDs = fieldnames(batchParams)';
paramArray = cell(1, length(patientIDs));
for i = 1:length(patientIDs)
    paramArray{i} = batchParams.(patientIDs{i});
end
for i = 1:length(patientIDs)
    name = patientIDs{i};
    params = paramArray{i};
    conditions = fieldnames(params)';
    for cond = conditions
        cond = cond{1};
        disp('------------------------');
        disp(['Condition: ' cond]);
        disp('------------------------');
        [loadedData,params.(cond)] = loadECoGData(params.(cond));
        % loadedData is matrix of nChan x nSamples
        [params.(cond).chanLoc, params.(cond).chanNum] = getChannelInfo(params.(cond));
        [data] = convertToFTFormat(loadedData,params.(cond));
        % segment data into trials of length trialLength
        cfg         = [];
        cfg.length  = trialLength;
        cfg.overlap = trialOverlap;
        data        = ft_redefinetrial(cfg, data);
%         data.trialinfo  % this field is optional, but can be used to store
%         % trial-specific information, such as condition numbers,
%         % reaction times, correct responses etc. The dimensionality
%         % is Ntrial*M, where M is an arbitrary number of columns
    end
end
