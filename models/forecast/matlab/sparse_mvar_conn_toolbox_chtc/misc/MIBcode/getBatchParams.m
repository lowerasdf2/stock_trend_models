% Resting state parameters
function [batchParams,setName] = getBatchParams(electrodeFilePath)%,defaultPath)
batchParams = struct;
% computer = 'desktopPC';
computer = 'Mac';
if strcmp(computer,'Mac')
    computerSpecPrefix = '/Users/bankslaptop/Box Sync/PHI_Data-A5309-Banks/Banks-UIowa-LDS/My documents/';
else
    computerSpecPrefix = 'D:\Box Sync\PHI_Data-A5309-Banks\Banks-UIowa-LDS\My documents\';
end

if ~exist('defaultPath','var')
    dataPath = [computerSpecPrefix 'Data and analysis' filesep 'ECoG data' filesep];
end
if ~exist('electrodeFilePath','var')
    electrodeFilePath = [computerSpecPrefix 'Data and analysis' filesep ...
        'ECoG data' filesep 'Electrode MNI coordinates and ROI' filesep];
end

trialLength = 2; %sec
trialOverlap = 0.25; %fractional overlap
% setName = 'origSet'; %origSet was determined from LGD resps by eye before stats
% setName = 'newSet'; %newSet was constrained only by bivariate Granger
% setName = 'LGDSet'; %LGDSet was constrained by LGD responses analyzed with stats and bivariate Granger
% setName = 'nineROISet'; %nineROI is Kirill's Aug 2017 area segmentation
% setName = 'sevenROISet'; %nineROI is Kirill's Aug 2017 area segmentation
% setName = 'hierarchySet'; %up to 9 ROIs w/LGD resps: [HGPM, HGAL, PT, PP, STGP, STGM, SMG or AG, MTG, MFG or IFGop]
setName = 'splitCond'; % same as hierarchySet, except split mixed OAAS conds 

%% Patients 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%369LR
electrodeFile = 'LGD electrodes for analysis - sorted by ROI.xlsx';
electrodeSheet = 'R369_OR';
patientID = '369LR';
patientID = ['patient' patientID];
recDate = 'yymdd';
dataPrefix = 'LFPx_RZ2_chn';
signalName = 'LFPx';
ECoGchan = ECoGChannelMap_tableRead([electrodeFilePath electrodeFile],electrodeSheet);
% exempChans.nums = [48,105,50,205,171,168,148,238,251]; 
% exempChans.ROIs = {'HGPM','HGAL','PT','STGP','STGM','MTG','SMG','AG','IFGop','MFG'};
% origSet:
% exempChans.nums = [47,105,50,188,221,171,168,238,251]; 
% exempChans.ROIs = {'R_HGPM','R_HGAL','R_PT','R_STGP','R_STGM','R_MTG','R_SMG','R_IFGop','R_MFG'};
% newSet (via bivarGranger):
% exempChans.nums = [46,106,92,221,85,79,82,144,223,89,45,112]; 
% exempChans.ROIs = {'R_HGPM','R_HGAL','R_PP','R_STGM','R_STGA','R_MTG',...
%     'R_TP','R_AngG','R_PreG','R_lOFC','R_Ins','R_Amyg'};
% LGDSet:
% exempChans.nums = [48,105,49,92,181,222,171,174]; 
% exempChans.ROIs = {'R_HGPM','R_HGAL','R_PT','R_PP','R_STGP','R_STGM','R_MTG','R_SMG'};
% nineROISet:
% exempChans.nums = [48,105,50,92,181,171,14,238,248];
% exempChans.ROIs = {'R_HGPM','R_HGAL','R_PT','R_PP','R_STG','R_Aud-rel','R_Other','R_PFC','R_SensMot'};
% sevenROISet:
% exempChans.nums = [48,105,50,92,181,171,238];
% exempChans.ROIs = {'R_HGPM','R_HGAL','R_PT','R_PP','R_STG','R_Aud-rel','R_PFC'};
% hierarchySet:
exempChans.nums = [48,105,50,92,181,212,171,174,238];
exempChans.ROIs = {'R_HGPM','R_HGAL','R_PT','R_PP','R_STGP','R_STGM','R_MTG','R_SMG','R_PFC'};

exempChans.setName = setName;

cond = {};
cond{1} = 'OAAS5';
cond{2} = 'OAAS4t3';
% for iCond = 1:length(cond)
% %     dataPath = [defaultPath patientID filesep cond{iCond} filesep];
%     batchParams.(patientID).(cond{iCond}) = struct( ...
%         'recDate', recDate,'ECoGchannels', ECoGchan, ... %'dataPath', dataPath,...
%         'dataPrefix',dataPrefix, 'signalName',signalName, ...
%         'trialLength',trialLength, 'trialOverlap',trialOverlap);
%     batchParams.(patientID).(cond{iCond}).exempChans = exempChans;
% end
batchParams.(patientID) = struct( ...
    'recDate', recDate,'ECoGchannels', ECoGchan, ... %'dataPath', dataPath,...
    'dataPrefix',dataPrefix, 'signalName',signalName, ...
    'trialLength',trialLength, 'trialOverlap',trialOverlap);
batchParams.(patientID).exempChans = exempChans;
batchParams.(patientID).cond = cond;

%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%372L
electrodeFile = 'LGD electrodes for analysis - sorted by ROI.xlsx';
electrodeSheet = 'L372_OR';
patientID = '372L';
patientID = ['patient' patientID];
recDate = 'yymdd';
dataPrefix = 'LFPx_RZ2_chn';
signalName = 'LFPx';
ECoGchan = ECoGChannelMap_tableRead([electrodeFilePath electrodeFile],electrodeSheet);
% origSet:
% exempChans.nums = [79,101,82,172,133,185,191,253,234];
% exempChans.ROIs = {'L_HGPM','L_HGAL','L_PT','L_STGP','L_STGM','L_MTG','L_SMG','L_IFGop','L_MFG'};
% newSet (via bivarGranger):
% exempChans.nums = [80,101,82,74,148,134,120,145,151,71,125,123];
% exempChans.ROIs = {'L_HGPM','L_HGAL','L_PT','L_PP','L_STGP','L_STGM',...
%     'L_STSU','L_MTG','L_PostG','L_Ins','L_Amyg','L_Hipp'};
% LGDSet:
% exempChans.nums = [80,100,81,74,165,134,170,190];
% exempChans.ROIs = {'L_HGPM','L_HGAL','L_PT','L_PP','L_STGP','L_STGM','L_MTG','L_SMG'};
% nineROISet:
% exempChans.nums = [80,100,81,73,165,170,201,245,150];
% exempChans.ROIs = {'L_HGPM','L_HGAL','L_PT','L_PP','L_STG','L_Aud-rel','L_Other','L_PFC','L_SensMot'};
% sevenROISet:
% exempChans.nums = [80,100,81,73,165,170,245];
% exempChans.ROIs = {'L_HGPM','L_HGAL','L_PT','L_PP','L_STG','L_Aud-rel','L_PFC'};
% hierarchySet:
exempChans.nums = [80,100,81,73,165,134,170,174,245];
exempChans.ROIs = {'L_HGPM','L_HGAL','L_PT','L_PP','L_STGP','L_STGM','L_MTG','L_SMG','L_PFC'};


exempChans.setName = setName;

cond = {};
cond{1} = 'OAAS5';
cond{2} = 'OAAS3t2';
cond{3} = 'OAAS1';
% for iCond = 1:length(cond)
% %     dataPath = [defaultPath patientID filesep cond{iCond} filesep];
%     batchParams.(patientID).(cond{iCond}) = struct( ...
%         'recDate', recDate,'ECoGchannels', ECoGchan, ... %'dataPath', dataPath,...
%         'dataPrefix',dataPrefix, 'signalName',signalName, ...
%         'trialLength',trialLength, 'trialOverlap',trialOverlap);
%     batchParams.(patientID).(cond{iCond}).exempChans = exempChans;
% end
batchParams.(patientID) = struct( ...
    'recDate', recDate,'ECoGchannels', ECoGchan, ... %'dataPath', dataPath,...
    'dataPrefix',dataPrefix, 'signalName',signalName, ...
    'trialLength',trialLength, 'trialOverlap',trialOverlap);
batchParams.(patientID).exempChans = exempChans;
batchParams.(patientID).cond = cond;

%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%376R
electrodeFile = 'LGD electrodes for analysis - sorted by ROI.xlsx';
electrodeSheet = 'R376_OR';
patientID = '376R';
patientID = ['patient' patientID];
recDate = 'yymdd';
dataPrefix = 'LFPx_RZ2_chn';
signalName = 'LFPx';
ECoGchan = ECoGChannelMap_tableRead([electrodeFilePath electrodeFile],electrodeSheet);
% origSet:
% exempChans.nums = [78,82,49,221,253,203,190,133,147];
% exempChans.ROIs = {'R_HGPM','R_HGAL','R_PT','R_STGP','R_STGM','R_MTG','R_SMG','R_IFGop','R_MFG'};
% newSet (via bivarGranger):
% exempChans.nums = [46,83,74,214,247,254,55,107,119,255,54,71];
% exempChans.ROIs = {'R_HGPM','R_HGAL','R_PP','R_STGP','R_STGP','R_STGM',...
%     'R_STSL','R_MTG','R_IFGop','R_PostG','R_Ins','R_Ins'};
% LGDSet:
% exempChans.nums = [47,81,48,74,246,253,220,224];
% exempChans.ROIs = {'R_HGPM','R_HGAL','R_PT','R_PP','R_STGP','R_STGM','R_MTG','R_SMG'};
% nineROISet:
% exempChans.nums = [47,81,48,74,246,224,102,132,248];
% exempChans.ROIs = {'R_HGPM','R_HGAL','R_PT','R_PP','R_STG','R_Aud-rel','R_Other','R_PFC','R_SensMot'};
% sevenROISet:
% exempChans.nums = [47,81,48,74,246,224,132];
% exempChans.ROIs = {'R_HGPM','R_HGAL','R_PT','R_PP','R_STG','R_Aud-rel','R_PFC'};
% hierarchySet:
exempChans.nums = [47,81,48,74,246,253,228,224,132];
exempChans.ROIs = {'R_HGPM','R_HGAL','R_PT','R_PP','R_STGP','R_STGM','R_MTG','R_SMG','R_PFC'};

exempChans.setName = setName;

cond = {};
cond{1} = 'OAAS5';
cond{2} = 'OAAS4';
cond{3} = 'OAAS1';
% for iCond = 1:length(cond)
% %     dataPath = [defaultPath patientID filesep cond{iCond} filesep];
%     batchParams.(patientID).(cond{iCond}) = struct( ...
%         'recDate', recDate,'ECoGchannels', ECoGchan, ...%'dataPath', dataPath,...
%         'dataPrefix',dataPrefix, 'signalName',signalName, ...
%         'trialLength',trialLength, 'trialOverlap',trialOverlap);
%     batchParams.(patientID).(cond{iCond}).exempChans = exempChans;
% end
batchParams.(patientID) = struct( ...
    'recDate', recDate,'ECoGchannels', ECoGchan, ... %'dataPath', dataPath,...
    'dataPrefix',dataPrefix, 'signalName',signalName, ...
    'trialLength',trialLength, 'trialOverlap',trialOverlap);
batchParams.(patientID).exempChans = exempChans;
batchParams.(patientID).cond = cond;

%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%384B
electrodeFile = 'LGD electrodes for analysis - sorted by ROI.xlsx';
electrodeSheet = 'B384_OR';
patientID = '384B';
patientID = ['patient' patientID];
recDate = 'yymdd';
dataPrefix = 'LFPx_RZ2_chn';
signalName = 'LFPx';
ECoGchan = ECoGChannelMap_tableRead([electrodeFilePath electrodeFile],electrodeSheet);
% origSet:
% exempChans.nums = [78,74,57,46,238,235,230,220]; % pmHG,alHG,PT,STG,MTG,SMG,IFG,MFG
% exempChans.ROIs = {'L_STGP','L_STGM','L_IFGop','L_MFG','R_STGP','R_STGM','R_MTG','R_MFG'};
% nineROISet:
% exempChans.nums = [249,78,80,57,85,238,230,2,200];
% exempChans.ROIs = {'L_PP','L_STG','L_Aud-rel','L_PFC','L_Other','R_STG','R_Aud-rel','R_PFC','R_Other'};
% % sevenROISet:
% exempChans.nums = [249,78,75,80,57,238,235,230,2]; 
% exempChans.ROIs = {'L_PP','L_STGP','L_STGM','L_SMG','L_PFC','R_STGP','R_STGM','R_MTG','R_PFC'};
% hierarchySet:
exempChans.nums = [249,78,75,80,57,238,235,230,2]; 
exempChans.ROIs = {'L_PP','L_STGP','L_STGM','L_SMG','L_PFC','R_STGP','R_STGM','R_MTG','R_PFC'};

exempChans.setName = setName;

cond = {};
cond{1} = 'OAAS5';
cond{2} = 'OAAS1_1';
cond{3} = 'OAAS1_2';
% for iCond = 1:length(cond)
% %     dataPath = [defaultPath patientID filesep cond{iCond} filesep];
%     batchParams.(patientID).(cond{iCond}) = struct( ...
%         'recDate', recDate,'ECoGchannels', ECoGchan, ...%'dataPath', dataPath,...
%         'dataPrefix',dataPrefix, 'signalName',signalName, ...
%         'trialLength',trialLength, 'trialOverlap',trialOverlap);
%     batchParams.(patientID).(cond{iCond}).exempChans = exempChans;
% end
batchParams.(patientID) = struct( ...
    'recDate', recDate,'ECoGchannels', ECoGchan, ... %'dataPath', dataPath,...
    'dataPrefix',dataPrefix, 'signalName',signalName, ...
    'trialLength',trialLength, 'trialOverlap',trialOverlap);
batchParams.(patientID).exempChans = exempChans;
batchParams.(patientID).cond = cond;

%%
%394R
electrodeFile = 'LGD electrodes for analysis - sorted by ROI.xlsx';
electrodeSheet = 'R394_OR';
patientID = '394R';
patientID = ['patient' patientID];
recDate = 'yymdd';
dataPrefix = 'LFPx_RZ2_chn';
signalName = 'LFPx';
ECoGchan = ECoGChannelMap_tableRead([electrodeFilePath electrodeFile],electrodeSheet);

% sevenROISet:
% exempChans.nums = [46,50,12,54];
% exempChans.ROIs = {'R_HGPM','R_PT','R_Aud-rel','R_PFC'};
% hierarchySet:
exempChans.nums = [46,50,12,54];
exempChans.ROIs = {'R_HGPM','R_PT','R_Aud-rel','R_PFC'};

exempChans.setName = setName;

cond = {};
cond{1} = 'OAAS5';
cond{2} = 'OAAS4t3';
cond{3} = 'OAAS1';
% for iCond = 1:length(cond)
% %     dataPath = [defaultPath patientID filesep cond{iCond} filesep];
%     batchParams.(patientID).(cond{iCond}) = struct( ...
%         'recDate', recDate,'ECoGchannels', ECoGchan, ...%'dataPath', dataPath,...
%         'dataPrefix',dataPrefix, 'signalName',signalName, ...
%         'trialLength',trialLength, 'trialOverlap',trialOverlap);
%     batchParams.(patientID).(cond{iCond}).exempChans = exempChans;
% end
batchParams.(patientID) = struct( ...
    'recDate', recDate,'ECoGchannels', ECoGchan, ... %'dataPath', dataPath,...
    'dataPrefix',dataPrefix, 'signalName',signalName, ...
    'trialLength',trialLength, 'trialOverlap',trialOverlap);
batchParams.(patientID).exempChans = exempChans;
batchParams.(patientID).cond = cond;

%%
%399R
electrodeFile = 'LGD electrodes for analysis - sorted by ROI.xlsx';
electrodeSheet = 'R399_OR';
patientID = '399R';
patientID = ['patient' patientID];
recDate = 'yymdd';
dataPrefix = 'LFPx_RZ2_chn';
signalName = 'LFPx';
ECoGchan = ECoGChannelMap_tableRead([electrodeFilePath electrodeFile],electrodeSheet);

% sevenROISet:
% exempChans.nums = [17,21,40,11,173,207,243,133];
% exempChans.ROIs = {'R_HGPM','R_HGAL','R_PT','R_PP','R_STGP','R_STGM','R_MTG','R_PFC'};
% hierarchySet:
exempChans.nums = [17,21,40,11,173,243,175,133];
exempChans.ROIs = {'R_HGPM','R_HGAL','R_PT','R_PP','R_STG','R_Aud-rel','R_SMG','R_PFC'};

exempChans.setName = setName;

cond{1} = 'OAAS5';
cond{2} = 'OAAS3t2';
cond{3} = 'OAAS1';
% for iCond = 1:length(cond)
% %     dataPath = [defaultPath patientID filesep cond{iCond} filesep];
%     batchParams.(patientID).(cond{iCond}) = struct( ...
%         'recDate', recDate,'ECoGchannels', ECoGchan, ...%'dataPath', dataPath,...
%         'dataPrefix',dataPrefix, 'signalName',signalName, ...
%         'trialLength',trialLength, 'trialOverlap',trialOverlap);
%     batchParams.(patientID).(cond{iCond}).exempChans = exempChans;
% end
batchParams.(patientID) = struct( ...
    'recDate', recDate,'ECoGchannels', ECoGchan, ... %'dataPath', dataPath,...
    'dataPrefix',dataPrefix, 'signalName',signalName, ...
    'trialLength',trialLength, 'trialOverlap',trialOverlap);
batchParams.(patientID).exempChans = exempChans;
batchParams.(patientID).cond = cond;

%%
%400L
electrodeFile = 'LGD electrodes for analysis - sorted by ROI.xlsx';
electrodeSheet = 'L400_OR';
patientID = '400L';
patientID = ['patient' patientID];
recDate = 'yymdd';
dataPrefix = 'LFPx_RZ2_chn';
signalName = 'LFPx';
ECoGchan = ECoGChannelMap_tableRead([electrodeFilePath electrodeFile],electrodeSheet);

% sevenROISet:
% exempChans.nums = [48,53,83,57,210,189,197];
% exempChans.ROIs = {'L_HGPM','L_HGAL','L_PT','L_PP','L_STG','L_Aud-rel','L_PFC'};
% hierarchySet:
exempChans.nums = [48,53,83,57,210,209,189,197];
exempChans.ROIs = {'L_HGPM','L_HGAL','L_PT','L_PP','L_STGM','L_MTG','L_AG','L_PFC'};

exempChans.setName = setName;

cond = {};
cond{1} = 'OAAS4';
cond{2} = 'OAAS3';
cond{3} = 'OAAS3t1';
% for iCond = 1:length(cond)
% %     dataPath = [defaultPath patientID filesep cond{iCond} filesep];
%     batchParams.(patientID).(cond{iCond}) = struct( ...
%         'recDate', recDate,'ECoGchannels', ECoGchan, ...%'dataPath', dataPath,...
%         'dataPrefix',dataPrefix, 'signalName',signalName, ...
%         'trialLength',trialLength, 'trialOverlap',trialOverlap);
%     batchParams.(patientID).(cond{iCond}).exempChans = exempChans;
% end
batchParams.(patientID) = struct( ...
    'recDate', recDate,'ECoGchannels', ECoGchan, ... %'dataPath', dataPath,...
    'dataPrefix',dataPrefix, 'signalName',signalName, ...
    'trialLength',trialLength, 'trialOverlap',trialOverlap);
batchParams.(patientID).exempChans = exempChans;
batchParams.(patientID).cond = cond;

%%
%403L
electrodeFile = 'LGD electrodes for analysis - sorted by ROI.xlsx';
electrodeSheet = 'L403_OR';
patientID = '403L';
patientID = ['patient' patientID];
recDate = 'yymdd';
dataPrefix = 'LFPx_RZ2_chn';
signalName = 'LFPx';
ECoGchan = ECoGChannelMap_tableRead([electrodeFilePath electrodeFile],electrodeSheet);

% hierarchySet:
exempChans.nums = [45,48,75,69,172,196,225,232,168];
exempChans.ROIs = {'L_HGPM','L_HGAL','L_PT','L_PP','L_STGM','L_STGP','L_MTG','L_SMG','L_PFC'};

exempChans.setName = setName;

cond = {};
cond{1} = 'OAAS5';
cond{2} = 'OAAS3t1';
cond{3} = 'OAAS1';
% for iCond = 1:length(cond)
% %     dataPath = [defaultPath patientID filesep cond{iCond} filesep];
%     batchParams.(patientID).(cond{iCond}) = struct( ...
%         'recDate', recDate,'ECoGchannels', ECoGchan, ...%'dataPath', dataPath,...
%         'dataPrefix',dataPrefix, 'signalName',signalName, ...
%         'trialLength',trialLength, 'trialOverlap',trialOverlap);
%     batchParams.(patientID).(cond{iCond}).exempChans = exempChans;
% end
batchParams.(patientID) = struct( ...
    'recDate', recDate,'ECoGchannels', ECoGchan, ... %'dataPath', dataPath,...
    'dataPrefix',dataPrefix, 'signalName',signalName, ...
    'trialLength',trialLength, 'trialOverlap',trialOverlap);
batchParams.(patientID).exempChans = exempChans;
batchParams.(patientID).cond = cond;

%%
%405L
electrodeFile = 'LGD electrodes for analysis - sorted by ROI.xlsx';
electrodeSheet = 'L405_OR';
patientID = '405L';
patientID = ['patient' patientID];
recDate = 'yymdd';
dataPrefix = 'LFPx_RZ2_chn';
signalName = 'LFPx';
ECoGchan = ECoGChannelMap_tableRead([electrodeFilePath electrodeFile],electrodeSheet);

% hierarchySet:
% exempChans.nums = [99,69,71,104,208,219,203,218,224];
% exempChans.ROIs = {'L_HGPM','L_HGAL','L_PT','L_PP','L_STGM','L_STGP','L_MTG','L_SMG','L_PFC'};
exempChans.nums = [99,69,71,104,240,251,235,250,26];
exempChans.ROIs = {'L_HGPM','L_HGAL','L_PT','L_PP','L_STGM','L_STGP','L_MTG','L_SMG','L_PFC'};

exempChans.setName = setName;

cond = {};
cond{1} = 'OAAS4t5';
cond{2} = 'OAAS3t2';
cond{3} = 'OAAS1';
% for iCond = 1:length(cond)
% %     dataPath = [defaultPath patientID filesep cond{iCond} filesep];
%     batchParams.(patientID).(cond{iCond}) = struct( ...
%         'recDate', recDate,'ECoGchannels', ECoGchan, ...%'dataPath', dataPath,...
%         'dataPrefix',dataPrefix, 'signalName',signalName, ...
%         'trialLength',trialLength, 'trialOverlap',trialOverlap);
%     batchParams.(patientID).(cond{iCond}).exempChans = exempChans;
% end
batchParams.(patientID) = struct( ...
    'recDate', recDate,'ECoGchannels', ECoGchan, ... %'dataPath', dataPath,...
    'dataPrefix',dataPrefix, 'signalName',signalName, ...
    'trialLength',trialLength, 'trialOverlap',trialOverlap);
batchParams.(patientID).exempChans = exempChans;
batchParams.(patientID).cond = cond;

%%
%409L
electrodeFile = 'LGD electrodes for analysis - sorted by ROI.xlsx';
electrodeSheet = 'L409_OR';
patientID = '409L';
patientID = ['patient' patientID];
recDate = 'yymdd';
dataPrefix = 'LFPx_RZ2_chn';
signalName = 'LFPx';
ECoGchan = ECoGChannelMap_tableRead([electrodeFilePath electrodeFile],electrodeSheet);

% hierarchySet:
% exempChans.nums = [99,69,71,104,208,219,203,218,224];
% exempChans.ROIs = {'L_HGPM','L_HGAL','L_PT','L_PP','L_STGM','L_STGP','L_MTG','L_SMG','L_PFC'};
exempChans.nums = [33,248,246,87,254,26];
exempChans.ROIs = {'L_HGPM','L_STGM','L_STGP','L_MTG','L_SMG','L_PFC'};

exempChans.setName = setName;

cond = {};
cond{1} = 'OAAS5';
cond{2} = 'OAAS4t3';
cond{3} = 'OAAS3t1';
% for iCond = 1:length(cond)
% %     dataPath = [defaultPath patientID filesep cond{iCond} filesep];
%     batchParams.(patientID).(cond{iCond}) = struct( ...
%         'recDate', recDate,'ECoGchannels', ECoGchan, ...%'dataPath', dataPath,...
%         'dataPrefix',dataPrefix, 'signalName',signalName, ...
%         'trialLength',trialLength, 'trialOverlap',trialOverlap);
%     batchParams.(patientID).(cond{iCond}).exempChans = exempChans;
% end
batchParams.(patientID) = struct( ...
    'recDate', recDate,'ECoGchannels', ECoGchan, ... %'dataPath', dataPath,...
    'dataPrefix',dataPrefix, 'signalName',signalName, ...
    'trialLength',trialLength, 'trialOverlap',trialOverlap);
batchParams.(patientID).exempChans = exempChans;
batchParams.(patientID).cond = cond;

%%
%423L
electrodeFile = 'LGD electrodes for analysis - sorted by ROI.xlsx';
electrodeSheet = 'L423_OR';
patientID = '423L';
patientID = ['patient' patientID];
recDate = 'yymdd';
dataPrefix = 'LFPx_RZ2_chn';
signalName = 'LFPx';
ECoGchan = ECoGChannelMap_tableRead([electrodeFilePath electrodeFile],electrodeSheet);

% hierarchySet:
exempChans.nums = [100,105,59,73,152,157,141,197,182];
exempChans.ROIs = {'L_HGPM','L_HGAL','L_PT','L_PP','L_STGM','L_STGP','L_MTG','L_SMG','L_PFC'};

exempChans.setName = setName;

cond = {};
cond{1} = 'OAAS5';
cond{2} = 'OAAS5t4';
cond{3} = 'OAAS3t1';
cond{4} = 'OAAS1';
batchParams.(patientID) = struct( ...
    'recDate', recDate,'ECoGchannels', ECoGchan, ... %'dataPath', dataPath,...
    'dataPrefix',dataPrefix, 'signalName',signalName, ...
    'trialLength',trialLength, 'trialOverlap',trialOverlap);
batchParams.(patientID).exempChans = exempChans;
batchParams.(patientID).cond = cond;

