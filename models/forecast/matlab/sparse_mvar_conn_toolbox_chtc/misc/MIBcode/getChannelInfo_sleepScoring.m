function [chanSide, chanROI, chanNum, sortOrder] = getChannelInfo_sleepScoring(params)

for iChan = 1:length(params.ECoGchannels)
    chanSide{iChan} = params.ECoGchannels(iChan).side;
    chanNum(iChan) = params.ECoGchannels(iChan).chanNum;
    if isfield(params.ECoGchannels(iChan),'newROI')
        chanROI{iChan} = params.ECoGchannels(iChan).newROI;
    else
        chanROI{iChan} = params.ECoGchannels(iChan).oldROI;
    end
end
