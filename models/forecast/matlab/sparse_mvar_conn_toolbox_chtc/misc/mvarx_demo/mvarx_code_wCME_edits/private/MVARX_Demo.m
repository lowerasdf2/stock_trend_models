%% CME: "_demo" is appended to these files to distinguish from edited files used for fitting ecog data
% load MVARX models estimated from Chang, et al. Front Hum Neurosci. 2012; 6: 317. 
load('mdl_cltd.mat', 'mdl_F3_FA_5m')

m_ori = size(mdl_F3_FA_5m.Aw, 1);    % number of channel in the original model
m = 5;    % number of channels we will be using in this simulation
p = 3;    % MVARX AR order
l = 0;   % MVARX feedforward length (l=0 means you're only using endogenous variables in model)

%% (parameters use to simulate time series)
col_set = kron(ones(1, m), 1:p) + kron(0:m_ori:m_ori*(p-1), ones(1, m));
A = mdl_F3_FA_5m.Aw(1:m, col_set);     % MVARX A matrix 
B = mdl_F3_FA_5m.Bw(1:m, 1:l);         % MVARX B matrix
Q = 25 * mdl_F3_FA_5m.Qw(1:m, 1:m);    % MVARX Q matrix
figure
imagesc(Q)
%% Stability is neccesary requirement for simulation
while ~is_stbl(A)
    A = A * 0.9;
end
figure
imagesc(A)
%% create train of stimulation (exogenous input variable)
u = [zeros(1, 19), 1, zeros(1, 80)];  % train of stimulation

X_train = mvarx_data_gen_demo(A, B, Q, u);
wf_shift = (0:-20:(m-1)*(-20))';
plot((X_train + wf_shift(:, ones(1, 100)))', 'Color', [31,120,180] / 255);
set(gca, 'ytick', '')
xlabel('t')

trainSize=20;
testSize=10;
n_epoch = trainSize+testSize;
X = cell(1, n_epoch);
for i = 1:n_epoch
    X{i} = mvarx_data_gen_demo(A, B, Q, u);
end


X_train=X(1,1:trainSize);%use first 15 epochs as train set
X_test=X(1,trainSize+1:end);%use last 5 epochs as test set

n_epoch=length(X_train);

% create a 1-by-n_epoch cell, each cell is the train of stimulation for the epoch
u = num2cell(repmat(u, 1, 1, n_epoch), [1, 2]);   
modelErrsByModelOrder=[];
model_orders=1:5;
wavelet_bool=false;
stim_bool=false;
for p = model_orders
    [A_hat, B_hat, Q_hat, W, n_spl] = mvarx_fit_demo(X_train, u, p, l);
    trainErr=oneStepPredErr(X_train,p,l,A_hat,B_hat,wavelet_bool,stim_bool,u);
    testErr=oneStepPredErr(X_test,p,l,A_hat,B_hat,wavelet_bool,stim_bool,u);
    modelErrsByModelOrder(p,1)=trainErr;
    modelErrsByModelOrder(p,2)=testErr;
end
figure
plot(model_orders,modelErrsByModelOrder(:,1))
hold on
plot(model_orders,modelErrsByModelOrder(:,2))
[minVal,minInd]=min(modelErrsByModelOrder(:,2));
bestModelOrder=model_orders(minInd);

%% train final model based on above results
[A_hat, B_hat, Q_hat, W, n_spl] = mvarx_fit_demo(X_train, u, bestModelOrder, l);

% % % p=model_orders;
% % % M=size(Q_hat,1);
% % % Mdl = varm(M,p);
% % % EstMdl = estimate(Mdl,X_train{1}');
% % % params=[];
% % % for iP=1:p
% % %     params=[params,EstMdl.AR{iP}];
% % % end
% % % figure
% % % subplot(2,1,1)
% % % imagesc(params)
% % % subplot(2,1,2)
% % % imagesc(A_hat)



% % % 
% % % errorsByVars=struct;
% % % for row = 1:length(modelErrsByModelOrder)
% % %     errCovMats=modelErrsByModelOrder{row} ;
% % %     L=det(errCovMats);
% % %     paramCt=numel(errCovMats)*row;
% % %     nEpochs=n_epoch;
% % %     nTrainEpochs=floor(nEpochs);
% % %     epochLen=size(X_train{1},2);
% % %     errorsByVars(row).p=row;
% % % 
% % %     T=(epochLen-errorsByVars(row).p)*nTrainEpochs; % number of observations
% % % %     T=nEpochs;%(epochLen-row);%-errorsByVars(row).p)*nTrainEpochs*size(errCovMats,1); % number of observations
% % % 
% % %     errorsByVars(row).T=T;
% % % %                     errorsByVars(row).AIC = (-2*log(L))+((2*paramCt)/T);
% % %     errorsByVars(row).AIC = (log(L))+((2*(paramCt/T)));
% % % 
% % % end
% % % figure
% % % plot(cell2mat({errorsByVars.p}),cell2mat({errorsByVars.AIC}))
%% compare estimated params with params used to simulate data
figure
cmin = min([A_hat(:); A(:)]);
cmax = max([A_hat(:); A(:)]);
subplot(211); imagesc(A, [cmin, cmax]); title('$A$', 'Interpreter', 'latex'); colorbar;
subplot(212); imagesc(A_hat, [cmin, cmax]); title('$\hat A$', 'Interpreter', 'latex'); colorbar;
% % % % these next few lines are added by CME to display B diff
% % % if l > 0
% % %     figure
% % %     cmin = min([B_hat(:); B(:)]);
% % %     cmax = max([B_hat(:); B(:)]);
% % %     subplot(211); imagesc(B, [cmin, cmax]); title('$B$', 'Interpreter', 'latex'); colorbar;
% % %     subplot(212); imagesc(B_hat, [cmin, cmax]); title('$\hat B$', 'Interpreter', 'latex'); colorbar;
% % % end
% % % figure
% % % evoked_response = mean(reshape(cell2mat(X_train), m, size(u{1},2), []), 3);
% % % model_response = mvarx_data_gen_demo(A_hat, B_hat, [], u{1}, 'evoked', true);
% % % 
% % % wf_shift = (0:-20:(m-1)*(-20))';
% % % h1 = plot((evoked_response + wf_shift(:, ones(1, 100)))', 'Color', [31,120,180] / 255); hold on;
% % % h2 = plot((model_response + wf_shift(:, ones(1, 100)))', 'Color', [227,26,28] / 255);
% % % legend([h1(1), h2(1)], {'Evoked Response', 'Model Response'})
% % % 
% % % [H, p] = mvarx_residual_whiteness_demo(W{1})
% % % 
% % % [H, p] = mvarx_residual_whiteness_demo(cell2mat(W), 'kernel', 'PAR', 'L', 'log')
