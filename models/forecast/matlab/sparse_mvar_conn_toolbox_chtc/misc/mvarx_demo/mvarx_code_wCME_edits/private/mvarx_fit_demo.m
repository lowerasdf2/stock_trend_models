function [A, B, Q, W, n_spl] = mvarx_fit_demo(X, u, p, l)
%MVARX_FIT Fit MVARX model to data
% [A, B, Q, W, n_spl] = mvarx_fit(X, u, p, l)
%
% Find an MVARX model that fits the data with the following relation
% X(:, n) = A * [X(:, n - 1); X(:, n - 2); X(:, n - 3); ... ; X(:, n - p)]
%          + B * [u(n); u(n - 1); ... ; u(n - l + 1)] + W(:, n - n_o)
%          for n = n_o + 1, ..., N where n_o = max(p, l - 1)
%
% X - Data, can be either a matrix or a cell
%       -if X is an M-by-N matrix, X would be the measurements from M 
%        channels/electrodes in a window of N samples
%       -if X is a 1-by-J cell, each cell is measurements from M channels in
%        an epoch/trial, X{j} is an M-by-N_j matrix for j = 1, 2, ..., J
%        assuming there are J epochs
% u - stimulation
%       -if X is a matrix, u should be a 1-by-N vector, representing the application
%        of stimulation in time
%       -if X is a cell, u should also be a 1-by-J cell, and u{j} is an 1-by-N_j
%        vector for j = 1, 2, ..., J
% p - MVARX model autoregressive order
% l - MVARX model direct feedforward effect length
%
% A - MVARX autoregressive coefficient matrix (M-by-Mp)
% B - MVARX direct feedforward matrix (M-by-l)
% W - MVARX residual 
%       -if X is a matrix then W will be an M-by-(N - n_o) matrix 
%       -if X is a cell then W wil be a 1-by-J cell, with each cell W{j} being 
%        an M-by-(N_j - n_o) matrix
% Q - MVARX residual covariance matrix E[Q] = E[W(:, n)* W(:, n).'] for all n
% n_spl - number of samples in data
%       -if X is a matrix then n_spl is a scalar and n_spl = N - n_o
%       -if X is a cell then n_spl is a cell and n_spl{j} = N_j - n_o for j = 1, 2, ..., J

n_o = max(p, l - 1);

if ~iscell(X)
    keyboard
    % X is a matrix
    Data = X;

    [M, N] = size(Data);
    X = Data(:, n_o + 1:end);
    % Z is the design matrix
    Z = zeros(M * p + l, N - n_o);
    for i = 1:p
    	Z((i - 1) * M + 1:i * M, :) = Data(:, n_o + 1 - i:end - i);
    end

    D = toeplitz([u(1); zeros(l - 1, 1)], u);
    Z(p * M + 1:end, :) = D(:, n_o + 1:end);

    % solve the least squares problem
    theta = (X*Z.') / (Z*Z.');
    A = theta(:, 1:M * p);
    B = theta(:, M * p + 1:end);

    W = (X - theta * Z);
    Q = (W * W.') / (N - n_o);
    n_spl = N - n_o;
else
    % X is a cell
    Data = X;
    n_epoch = size(Data, 2);    % number of trials/epochs
    M = size(Data{1}, 1);       % channels

    R_XZ = zeros(M, M * p + l);
    R_ZZ = zeros(M * p + l);

    Data_cltd = cell(1, n_epoch);
    Z_cltd = cell(1, n_epoch);
    n_spl = cell(1, n_epoch);

    for j = 1:n_epoch
        n_spl{j} = size(Data{j}, 2) - n_o;

    	X = Data{j}(:, n_o + 1:end);
        Z = zeros(M * p + l, n_spl{j});

        for i = 1:p
            Z((i - 1) * M + 1:i * M, :) = Data{j}(:, n_o + 1 - i:end - i);
        end
        if l ~= 0
            D = toeplitz([u{j}(1); zeros(l - 1, 1)], u{j});
            Z(M * p + 1:end, :) = D(:, n_o + 1:end);
        end
        R_XZ = R_XZ + (X*Z.');
    	R_ZZ = R_ZZ + (Z*Z.');

    	Data_cltd{j} = X;
    	Z_cltd{j} = Z;
    end

    theta = R_XZ / R_ZZ; 
    %% this above is equiv to below for 1 epoch, see:http://www.phdeconomics.sssup.it/documents/Lesson17.pdf 
    %     theta2 = ((inv(Z*Z')*Z)*X')';
    %     theta3 = X/Z;
    %     figure
    %     numRows=5;
    %     subplot(numRows,1,1)
    %     imagesc(theta)
    %     subplot(numRows,1,2)
    %     imagesc(theta2)
    %     subplot(numRows,1,3)
    %     imagesc(theta3)
    %     subplot(numRows,1,4)
    %     imagesc(horzcat(theta4,zeros(K,K)))
    %     subplot(numRows,1,5)
    %     imagesc(horzcat(theta5,zeros(K,K)))
    
    %% Linear constraints: First see if I can rewrite vec(paramMatrix) as R*gamma+r: Let's impose a constraint of all coefs acting at the last lag to be zero (doing this first w/ no stim effect; l=0): following Lutkepohl, ch5, https://klevas.mif.vu.lt/~danas/VAR/fulltext4.pdf
    K=size(theta,1);% number of vars (chans)
%     constants=ones(K,1);
%     theta_w_const=horzcat(constants,theta);
%     vec_A=theta_w_const(:);
    const=false;
    pRemCt=0; % how many vars to eliminate
    pRem=2;%which model order to remove

    chRemCt=1; % how many channels to remove stim effects from
    lChRem=3; % which channels to remove stim effects from (code works for one so far)
    if chRemCt && pRemCt
        keyboard
    end
    numVars=(K.^2*(p-pRemCt))+(K*const)+(l*(K-chRemCt)); % number of vars estimated (not cancelled out) if eliminating one lag's coefs
% % %     if p == 1
% % %         keyboard
% % %         if const
% % %             keyboard
% % %             gamma=[constants,theta(:,1:end-1)]; % params to keep (or be estimated)
% % %         else
% % %             gamma=[theta(:,1:end-1)]; % params to keep (or be estimated)
% % %         end
% % %     else
% % %         if const
% % %             keyboard
% % %             gamma=[constants,theta(:,1:end-K)]; % params to keep (or be estimated)
% % %         elseif lChRem
% % %             gamma=[theta(:,1:end-l)]; % params to keep (or be estimated)
% % %         elseif pRemCt
% % %             gamma=[theta(:,1:end-K)]; % params to keep (or be estimated)
% % %         end
% % %     end
% % %     gamma_vec=gamma(:); % params to keep (or be estimated)
%     R=zeros(K*((K*p)+const+l),numVars);
% % % % %     R=eye(K*((K*p)+const+l),K*((K*p)+const+l));%numVars);
% % % % %     if pRemCt > 0
% % % % %         R(((pRem-1)*K^2)+1:((pRem-1)*K^2)+1+(K^2)-1,:)=0;
% % % % %     elseif chRemCt > 0
% % % % %         for rep=1:l
% % % % %             R(M^2 * p + 1:end,M^2 * p + (lChRem+((rep-1)*K)):M^2 * p + (lChRem+((rep-1)*K)))=0;
% % % % %         end
% % % % % %         R(
% % % % % %         (pRem-1)*K^2)+1+(K^2)-1,:)=0;
% % % % %     elseif chRemCt > 0 && pRemCt > 0
% % % % %         keyboard
% % % % %     end
% % % % % %     R(1:numVars,:)=eye(numVars);
% % % % %     r=zeros(K*((K*p)+const+l),1);
%     constrained_vec_A=R*gamma_vec; % behaves as expected :) able to cancel out last lag's coefficients
    
    %% Use full y_hat equation (except for innov. noise) to create 1-step predictions using constrained model
    if const
        Z_w_const=vertcat(ones(1,size(Z,2)),Z);
    else
        Z_w_const=Z;
    end
%     Z_w_const_vec=Z_w_const(:);
% % %     R_vec=R(:);
%     y=X(:);
%     y2=kron(Z_w_const',eye(K))*theta_w_const(:);
%     y3=kron(Z_w_const',eye(K))*((R*gamma_vec)+r);
    
    %% Now let's see if I can solve for params with constraints imposed
% % %     % These equations are pulled from slide 7: https://homepage.univie.ac.at/robert.kunst/pres11_var_pauls.pdf
% % %     z=X(:)-(kron(Z_w_const',eye(K))*r);
% % %     
% % %     % w/ Q estimate (EGLS):    
% % %     %     theta4=inv(R'*kron((Z_w_const*Z_w_const'),inv(Q))*R)*(R'*kron(Z_w_const,inv(Q))*z);
% % %     % w/out Q estimate (LS):
% % % %     theta4=inv(R'*kron((Z_w_const*Z_w_const'),eye(K))*R)*(R'*kron(Z_w_const,eye(K))*z);
% % %     % x = a\b is computed differently than x = inv(a)*b 
% % %     a=R'*kron((Z_w_const*Z_w_const'),eye(K))*R;
% % %     b=(R'*kron(Z_w_const,eye(K))*z);
% % % %     theta4=inv(a)*b;
% % %     theta5=pinv(a)*b;
% % % %     theta4=reshape(theta4,[K,(K*(p-1))+1]);
% % % %     theta5=a\b;
% % %     theta5=reshape(theta5,[K,(K*(p))+l+const]); 
% % % %     theta5=horzcat(theta5,zeros(K,K));
% % %     figure
% % %     subtightplot(2,1,1)
% % %     imagesc(theta)
% % %     colorbar
% % % 
% % %     subtightplot(2,1,2)
% % %     imagesc(theta5)
% % %     colorbar
    %% the above works, now need to add stimVec and see if I can eliminate stim effect on certain channels
    A = theta(:, 1:M * p);   
    if l > 0
        B = theta(:, M * p + 1:end);
    else
        B=[];
    end
    W = cell(1, n_epoch);
    for j = 1:n_epoch
        W{j} = Data_cltd{j} - theta * Z_cltd{j};
    end
    Q = (cell2mat(W)*cell2mat(W).') / sum(cell2mat(n_spl));   
end