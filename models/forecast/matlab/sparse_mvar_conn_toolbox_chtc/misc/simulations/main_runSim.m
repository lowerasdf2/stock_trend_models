close all
clear all

%% set path
localPath='W:\code\Chris\gitUploads\ecog-analysis\static-models\';
addpath(genpath([localPath '..' filesep 'Utilities']))
addpath([localPath filesep '3rdParty\blockMVAR'])
addpath('\\Banksdesktop\d\Box Sync\My documents\Data and analysis\ECoG-analysis\circularGraph')
addpath([localPath filesep 'mvarx_demo\mvarx_code']) % to simulate data
addpath([localPath filesep 'plot_connectivity\paul-kassebaum-mathworks-circularGraph-3a7926b'])
addpath('W:\code\Chris\gitUploads\ecog-analysis\static-models\fit_ECoG_data\one_step_pred_err')
%% sim param notes
% 1) Create a set of params with longer and shorter-range interactions
% 2) Verify that model params produce different networks at different
% frequency bands
% 3) See how well model can recover interactions (at all time-scales) with simulated data
% 4) Repeat above with wavelet transforms tossed into the mix
%% testing effects of PCA
% 1) Should maybe use each of my real vars as a 'source', and create redundant
% channels assumed to be in the source's spatial neighborhood

%% init/set some params
% s
nVar=10;
trueMos=[15:15:100];
% trueMos=20;
sampleRates=[300:-50:150];
sampleRates=101; % >= 101 Hz will include full gamma range (30-50 hz)
% load('mdl_cltd.mat', 'mdl_F3_FA_5m')
Q = eye(nVar,nVar);%25 * mdl_F3_FA_5m.Qw(1:m, 1:m);    % MVARX Q matrix
N_q_pairs=createNq_pairs(trueMos); % create list of N/q values 
N=1;
q=1;
pcaBool=false;
lassoBool=false;
for sampleRate=sampleRates
    simLens=[sampleRate*4];%round(sampleRate/16),round(sampleRate/4),sampleRate,round(sampleRate*4)];%,round(sampleRate/8),round(sampleRate/4),round(sampleRate/2),round(sampleRate),round(sampleRate*2),round(sampleRate*4)];
    for simLength=simLens
        % init
        u = [zeros(1,simLength)];  % train of stimulation
        B=[];
        allF1scores={};
        predMos=[];
        scoreInd=1;
        %% make results dir
        figDir=['M:\PassiveEphys\Analysis Results\Chris\ecog_connectivity\simulations' filesep num2str(sampleRate) 'hz' filesep filesep 'simLen' num2str(simLength) filesep];
        if ~exist(figDir,'dir')
            mkdir(figDir)
        end

        %% begin sims/tests
        numSubrowsPerRow=5;
        for modelOrder=trueMos
            %% Create param set
            trueMoFig=figure('units','normalized','outerposition',[0 0 1 1]);
            nRows=12;
            nCols=2;
            [A,conns]=designParams(sampleRate,nVar,modelOrder,N,q,pcaBool,lassoBool);
            %% Stability is neccesary requirement for simulation
            while ~is_stbl(A)
                A = A * 0.9;
            end

        % % %     plotRow=2;
        % % %     subplot(nRows,nCols,3:4)
        % % %     X_train = mvarx_data_gen_demo(A, B, Q, u);
        % % %     shiftSize=10;
        % % %     wf_shift = (0:-shiftSize:(m-1)*(-shiftSize))';
        % % %     plot((X_train + wf_shift(:, ones(1, simLength)))', 'Color', [31,120,180] / 255);
        % % %     xlim([0,simLength])
        % % %     set(gca, 'ytick', '')
        % % %     xlabel('t')

            %% Check parameters to verify that frequency networks vary signifantly from each other
            plotRow=2;
            startPlotInd=3;%(1:numSubrowsPerRow*nCols)+(plotRow*numSubrowsPerRow);
            startPlotInd=startPlotInd(1);
            [f1Scores,trueMoFig]=checkSimParamValidity(A,sampleRate,conns,trueMoFig,nRows,nCols,startPlotInd);
            allF1scores{scoreInd,1}=f1Scores;

            %% createSimSet and try to recover params
            waveBool=false;
            A_hat=simSet_recoverParams(A, B, Q, u, waveBool);
            predMo=size(A_hat,2)/size(A_hat,1);
            predMos=[predMos,predMo];

            A_hat_plot=A_hat;
            A_plot=A;
            addRed=0;
            if size(A_hat,2) > size(A,2)
                lineInd=size(A,2)+1;
                A_plot=[A,zeros(size(A,1),size(A_hat,2)-size(A,2))];
                addRed=1;
            elseif size(A_hat,2) < size(A,2)
                lineInd=size(A_hat,2)+1;
                A_hat_plot=[A_hat,zeros(size(A,1),size(A,2)-size(A_hat,2))];
                addRed=2;
            end
            figure(trueMoFig)
            plotRow=1;
            subplot(nRows,nCols,1:2)
            imagesc(A_plot)
            if addRed==1
                vline(lineInd,'r')
            end
            figure(trueMoFig)
            subplot(nRows,nCols,13:14)
            imagesc(A_hat_plot)
            if addRed==2
                vline(lineInd,'r')
            end

            plotRow=4;
            startPlotInd=15;%(1:numSubrowsPerRow*nCols)+(plotRow*numSubrowsPerRow);
            startPlotInd=startPlotInd(1);
            [f1Scores2]=checkSimParamValidity(A_hat,sampleRate,conns,trueMoFig,nRows,nCols,startPlotInd);
            allF1scores{scoreInd,2}=f1Scores2;
            scoreInd=scoreInd+1;

            %% 3) Compare wavelet model's ability to fit data with standard VAR model. Wavelets allow us to see further into the past at lower price
        %     Nq_pair=N_q_pairs{modelOrder}; % wavMO=(N*q)+q; % how many dataPts needed for given model-order when using wavelets
            waveBool=true;
            figure(trueMoFig)
            suptitle(['trueModelOrder=' num2str(modelOrder) ', predModelOrder=' num2str(predMo)])
            saveas(trueMoFig,[figDir 'mo' num2str(modelOrder) '.png'])
        end
        perfByBand=figure('units','normalized','outerposition',[0 0 1 1]);

        for iBand=1:5
            trainScores=[];
            testScores=[];
            for f1scoreInd=1:size(allF1scores,1)
                trainScore=mean(allF1scores{f1scoreInd,1}(iBand,5));
                testScore=mean(allF1scores{f1scoreInd,2}(iBand,5));
                trainScores(f1scoreInd)=trainScore;
                testScores(f1scoreInd)=testScore;
            end

            subplot(5,1,iBand)
            plot(trueMos,trainScores)
            hold on
            plot(trueMos,testScores)
        %     subplot(2,1,2)
        %     plot(trueMos,predMos)
        end
        figure(perfByBand)
        saveas(perfByBand,[figDir 'f1Scores.png'])
    end
end



function A_hat=simSet_recoverParams(A, B, Q, u,waveBool)

trainSize=200;
testSize=100;
n_epoch = trainSize+testSize;
X = cell(1, n_epoch);
for i = 1:n_epoch
    X{i}=mvarx_data_gen_demo(A,B,Q,u);
end
X_train=X(1,1:trainSize);%use first 15 epochs as train set
X_test=X(1,trainSize+1:end);%use last 5 epochs as test set

n_epoch=length(X_train);

% create a 1-by-n_epoch cell, each cell is the train of stimulation for the epoch
u = num2cell(repmat(u, 1, 1, n_epoch), [1, 2]);   
modelErrsByModelOrder=[];
model_orders=5:5:80;
wavelet_bool=false;
stim_bool=false;
l=0;
ind=1;
for p = model_orders
    [A_hat, B_hat, Q_hat, W, n_spl] = mvarx_fit_demo(X_train, u, p, l);
    trainErr=oneStepPredErr(X_train,p,l,A_hat,B_hat,wavelet_bool,stim_bool,u);
    testErr=oneStepPredErr(X_test,p,l,A_hat,B_hat,wavelet_bool,stim_bool,u);
    modelErrsByModelOrder(ind,1)=trainErr;
    modelErrsByModelOrder(ind,2)=testErr;
    ind=ind+1;
end

figure
plot(model_orders,modelErrsByModelOrder(:,1))
hold on
plot(model_orders,modelErrsByModelOrder(:,2))
[minVal,minInd]=min(modelErrsByModelOrder(:,2));
bestModelOrder=model_orders(minInd);

%% train final model based on above results
[A_hat, B_hat, Q_hat, W, n_spl] = mvarx_fit_demo(X_train, u, bestModelOrder, l);

end

function N_q_pairs=createNq_pairs(trueMos)
% % % %% Determine which combos of #levels (N) and #coefs (q) work for each model order option
N_q_pairs={};
% model order
for N=2:100 % when N=1, there is no dimensionality reduction (so skip)
    for q=1:1000
        clear Lstart L
        Lstart=nan(1,N+1);
        L=nan(1,N+2);
        for iCoef = 1:N+1
            if iCoef == 1
                Lstart(iCoef) = 0;
                L(iCoef) = q;
            else
                Lstart(iCoef) = q*2.^(iCoef-2);
                L(iCoef) = Lstart(iCoef);
            end
        end
        p_w=L(end-1)*2;% length of memory (order)
        if p_w <= max(trueMos)
            if length(N_q_pairs) < p_w
                N_q_pairs{p_w} = [N,q];
            elseif ~isempty(N_q_pairs{p_w})
                N_q_pairs{p_w}=[N_q_pairs{p_w};[N,q]];
            end
        end
    end
end
end
% % % 
% % % %% Create design matrix; transformwith wavelet bases
% % % Z = zeros((M * ((N+1)*q))+l, n_spl{j});
% % % if stimOn_bool
% % %     Z(end-l+1:end, :) = D(:, n_o + 1:end);
% % % end
% % % W = wavebases4(N,q,p);
% % % for iVar=1:size(X_train_full,1)
% % %     y = X_train_full(iVar,n_o+1:end);
% % %     z = toeplitz(X_train_full(iVar,n_o:end-1),X_train_full(iVar,n_o:-1:1));
% % %     z_trans = z*W;
% % %     fillRows=iVar:M:size(Z,1)-l;
% % %     Z(fillRows,:) = z_trans';
% % % %             Z_fliplr = fliplr(Z);
% % %     % b = inv((X*W)'*X*W)*(X*W)'*y;
% % % end
% % % 
% % % 
% % % % % % %% Generate example data
% % % % % % dataLen=10000;
% % % % % % x(1,:) = filter(1,[1, -1.9*cos(pi/4), .95^2],randn(dataLen,1)); % ground truth (g.t.) data vec 
% % % % % % % % % x(2,:) = filter(1,[1, -1.9*cos(pi/4), .95^2],randn(dataLen,1)); % ground truth (g.t.) data vec 
% % % % % % % % % x(3,:) = filter(1,[1, -1.9*cos(pi/4), .95^2],randn(dataLen,1)); % ground truth (g.t.) data vec 
% % % % % % 
% % % % % % 
% % % % % % %% Estimate constrained coefficients
% % % % % % [A,X] = getARcons(x,W);
% % % % % % 
% % % % % % %% Generate data from param estimates
% % % % % % rec_x(1:len) = x(1:len);
% % % % % % rec_x(len+1:length(x))=X*A;
% % % % % % figure
% % % % % % plot(x(1:1000));
% % % % % % hold on
% % % % % % plot(rec_x(1:1000));
% % % % % % norm(rec_x-x) % measure error
% % % % % % 
% % % % % % B=zeros(1,length(A));
% % % % % % Q=0.01;
% % % % % % u=zeros(10000,1);
% % % % % % 
% % % % % % x_start=x(1:len)
% % % % % % rec_x(1:len) = x(1:len);
% % % % % % A*x(len+1:end);
% % % % % % 
% % % % % % for xt = len+1:length(x)
% % % % % % %     rec_x(xt) = 
% % % % % % 
% % % % % % end
% % % % % % 
% % % % % % %% Generate data for several epochs/trials
% % % % % % n_epoch = 50;
% % % % % % X_rec = cell(1, n_epoch);
% % % % % % for i = 1:n_epoch
% % % % % %     X_rec{i} = mvar_data_gen(A', B, Q, u);
% % % % % % end
% % % % % % u = num2cell(repmat(u, 1, 1, n_epoch), [1, 2]);   
% % % % % % 
% % % % % % 
% % % % % % %% Generating data with param vector recovered from g.t data produces data that doesn't match generated data
% % % % % % figure
% % % % % % subplot(2,1,1)
% % % % % % plot(x(1:10000).^2)
% % % % % % subplot(2,1,2)
% % % % % % plot(x_rec(1:10000).^2)
% % % % % % 
% % % % % % %% However, recovering parameter vec from generated data (that doesn't match g.t. data) produces param vector that matches estimated param vector of g.t. data
% % % % % % A_rec = getARcons(x_rec,W);
% % % % % % keyboard
% % % % % % figure
% % % % % % plot((A(1:50))-.5)
% % % % % % hold on
% % % % % % plot(A_rec(1:50))
% % % % % % 
% % % % % % %%
% % % % % % function [a,X] = getARcons(x,W)
% % % % % % % estimates AR coefficients from time series x
% % % % % % % constrained to space spanned by W
% % % % % % % AR order given by number of rows in W
% % % % % % 
% % % % % % ord = size(W,1);
% % % % % % y(:,1) = x(ord+1:end);
% % % % % % X = toeplitz(x(ord:end-1),x(ord:-1:1));
% % % % % % b = inv((X*W)'*X*W)*(X*W)'*y;
% % % % % % a = W*b;
% % % % % % end
% % % % % % 
% % % % % % end
% % % % % % 
% % % % % %     