
   %%Time specifications:
   Fs = 250;                   % samples per second
   dt = 1/Fs;                   % seconds per sample
   StopTime = 3;             % seconds
   t = (0:dt:StopTime-dt)';     % seconds
   %%Sine wave:
   Fc = 4;                     % hertz
%    x = cos(2*pi*Fc*t);
   x = sin(pi/4*Fc*t);

   % Plot the signal versus time:
   figure;
   plot(t,x);
   xlabel('time (in seconds)');
   title('Signal versus Time');
   zoom xon;
   hold on
   x = sin((pi/4*Fc*t)+(pi)) ;
   plot(t,x);

   
t = linspace(0,2*pi) ;
x = sin(pi/4*t) ;
plot(t,x,'r')
hold on
x = sin(pi/4*t+pi/2) ;  % phase shift by pi/2, which is same as cos 
plot(t,x,'b')

