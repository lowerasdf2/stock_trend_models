function [A,conns]=designParams(sampleRate,nVar,modelOrder,varargin)

%% match these vars to varargin's contents
% ,N,q,pcaBool,lassoBool
%% Design set of params that ....
%% 1) ... produces variable networks at low-freq interactions and high freq. interactions. 
A=zeros(nVar,nVar*modelOrder);

%% init container of freq-dependent connections
conns=struct;
conns.delta.pairs=zeros(nVar,nVar);
conns.theta.pairs=zeros(nVar,nVar);
conns.alpha.pairs=zeros(nVar,nVar);
conns.beta.pairs=zeros(nVar,nVar);
conns.gamma.pairs=zeros(nVar,nVar);

%% Add self-connections. Each node operates at specific frequency.
hz_SC=[2,6,12,25,50,2,6,12,25,50];
for iVar=nVar:-1:1
    A=addCausalInteractions(A,iVar,iVar,hz_SC(iVar));
end
addOtherConns=true;
bandReps=2; % number of times to repeat node at given freq. (more cycles improves f1 estimates)
% for iCircle=1:2
if addOtherConns
%     startCh=startChs(iCircle);
    createCircleNetwork(bandReps)
end
% end

function createCircleNetwork(bandReps)
    %% Add delta causal interactions from ch1 and chs[2], 
    % theta causal interactions from ch2 and chs[3], 
    % alpha causal interactions from ch3 to ch4
    % beta causal interactions from ch4 to chs5 
    % gamma causal interactions from ch5 to chs1
    for iConn=1:bandReps*5
        hz=hz_SC(iConn);
        if iConn~=bandReps*5
            fromChs=iConn;
            toChs=iConn+1;
        else
            fromChs=iConn;
            toChs=1;
        end
        A=addCausalInteractions(A,fromChs,toChs,hz);
    end

end    
function A=addCausalInteractions(A,fromCh,toChs,hz)
if length(fromCh) > 1
    keyboard
end
if hz <= 4
    band='delta';
elseif hz <= 8
    band='theta';
elseif hz <= 14
    band='alpha';
elseif hz <= 30
    band='beta';
elseif hz <= 70
    band='gamma';
end
for toCon=1:length(toChs)
    conns.(band).pairs(fromCh,toChs)=1;
end

minMs=1000/hz; % minimum time needed (in ms) to capture 1 interaction at specified freq.
dTms=1/sampleRate*1000; % space between samples in ms
dCause=round(minMs/dTms/2); % space between interactions at specified freq. given sampling rate
% divide above by 2 to add troughs
moInds=[1:dCause:modelOrder];

interactInds=[];
allChInds=[];
for p=1:modelOrder
    if sum(moInds==p)>0
        interactInds=[interactInds,fromCh+(nVar*(p-1))];
    end
end
% x=diff(interactInds);
% addTroughs=round(x(1)/2):round(x(1)/2):interactInds(end);
% interactInds=sort([interactInds,addTroughs]);
if sum(fromCh==toChs)==length(fromCh) && length(fromCh)==length(toChs)
    coefs=ones(length(interactInds),1)*.8;%(.8+(.2*rand(1)));
else
    coefs=ones(length(interactInds),1)*.2;%(.2+(.6*rand(1)));
end
coefs(2:2:end)=coefs(2:2:end).*-1; % make SC's resonate

% if rand(1) < .5
%     coefs=coefs.*-1;
% end
% for iCoef=1:length(coefs)
%     if mod(iCoef,2)
%         coefs(iCoef)=-coefs(iCoef);
%     else
%         coefs(iCoef)=coefs(iCoef);
%     end
% end
extRateInd=1;
for ind=3:2:length(interactInds)
    for effect=[0,1]
        try
            coefs(ind+effect)=coefs(ind+effect)*(.6^(extRateInd));
        catch why
            continue
        end
    end
    extRateInd=extRateInd+1;
end
try
    for iVar=toChs
        A(iVar,interactInds)=coefs;
    end
catch why
    keyboard
end

end

%% 2) ... with different channel groups coordinating in an ROI-like fashion
% Greater channel count will increase compatibility between simulation and
% the models we want to run. Adding ROI-specific variance to channel-groups
% might increase realism... but difficult to say how different ROIs should
% behave.

end



