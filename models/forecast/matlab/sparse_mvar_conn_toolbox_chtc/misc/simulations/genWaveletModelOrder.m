function modelOrder=genWaveletModelOrder(N,q)
clear Lstart L
Lstart=nan(1,N+1);
L=nan(1,N+2);
for iCoef = 1:N+1
    if iCoef == 1
        Lstart(iCoef) = 0;
        L(iCoef) = q;
    else
        Lstart(iCoef) = q*2.^(iCoef-2);
        L(iCoef) = Lstart(iCoef);
    end
end
modelOrder=L(end-1)*2;% length of memory (order)
