function [f1Scores,trueMoFig]=checkSimParamValidity(A,sampleRate,conns,trueMoFig,nRows,nCols,startPlotInd)
f1Scores=[];
%% 
% Check legitimacy of params by plotting connectivity profiles across
% frequency ranges
Mv=nan(size(A,1),1);
for roiInd=1:size(A,1)
    Mv(roiInd)=1;%length(roiIndsMap(orig_labels{roiInd}));
end
nfft=500;
Su=eye(size(A,1));
[bDC,bPDC,mF,mG,bS,bP,bH,bAf,f]=block_fdMVAR(A,Su,Mv,nfft,sampleRate);

M=length(Mv);
Q=size(A,1);
cond='sim';
for i=1:M        
    for j=1:M    
        for iBand=1:length(FreqBands.Names)
            begFreq=FreqBands.Limits.(FreqBands.Names{iBand})(1);
            begFreq=find(f>=begFreq,1,'first');
            if iBand~=length(FreqBands.Names)
                endFreq=FreqBands.Limits.(FreqBands.Names{iBand})(2);
                endFreq=find(f>=endFreq,1,'first');
            else
                endFreq=nfft;
            end
            connMatrices.(cond).mF(iBand,i,j)=sum(mF(i,j,begFreq:endFreq))./length(begFreq:endFreq);
            connMatrices.(cond).mG(iBand,i,j)=sum(mG(i,j,begFreq:endFreq))./length(begFreq:endFreq);
            connMatrices.(cond).bDC(iBand,i,j)=sum(bDC(i,j,begFreq:endFreq))./length(begFreq:endFreq);
            connMatrices.(cond).bPDC(iBand,i,j)=sum(bPDC(i,j,begFreq:endFreq))./length(begFreq:endFreq);
        end
    end
end
measType='bPDC';
textToDisplay={'1','2','3','4','5','6','7','8','9','10'};
colorVals=[1,0,0; 1,0,0.6; 0.6,0.6,0; 0,0,1; 0.7,0.7,0.7;...
    1,0,0; 1,0,0.6; 0.6,0.6,0; 0,0,1; 0.7,0.7,0.7];
weightsFig=figure;
myColorMap=containers.Map;
for iColor=1:length(colorVals)
    myColorMap(textToDisplay{iColor})=colorVals(iColor,:);
end
% weightHist=figure;
bandNames={'delta','theta','alpha','beta','gamma'};
for iBand=1:5
    weights=squeeze(connMatrices.(cond).(measType)(iBand,:,:));
    for iVar=1:length(weights)
        weights(iVar,iVar)=0;
    end
%     figure(weightHist)
%     subplot(6,1,iBand)

%     histogram(weights(:))
    maxVal=max(weights(:));
%     myCircularGraph_withSelfConn(weights,...
%         'Colormap',colorVals,...
%         'Label',textToDisplay,...
%         'MaxVal',maxVal);
    if iBand==1
        disp('WARNING! Transposing connectivity matrix to reflext from-to cnnections in correct/intuitive manner')
    end
    weights=weights';
    

    figure(trueMoFig)
    subplot(nRows,nCols,startPlotInd)
    weights=myCircularGraph2(-inf,weights,textToDisplay,myColorMap,maxVal);
    startPlotInd=startPlotInd+1;
    
    gtMatrix=conns.(bandNames{iBand}).pairs;
    for iVar=1:length(gtMatrix)
        gtMatrix(iVar,iVar)=0;
    end
    gtConnsThisBand=find(gtMatrix==1);
    predConns=find(weights>.5);
    tp=length(intersect(gtConnsThisBand,predConns));
    fp=length(predConns)-tp;
    gtDisconnsThisBand=find(gtMatrix==0);
    predDisconns=find(weights<.5);
    tn=length(intersect(gtDisconnsThisBand,predDisconns));
    fn=length(predDisconns)-tn;
    tn=tn-M;% subtract sc since these were set to zero manually to showcase other connections
    
    figure(trueMoFig)
    subplot(nRows,nCols,startPlotInd)
    imagesc(weights)
    startPlotInd=startPlotInd+1;
    
    f1Scores(iBand,1)=tp;
    f1Scores(iBand,2)=fp;
    f1Scores(iBand,3)=tn;
    f1Scores(iBand,4)=fn;
    
    prec=tp/(tp+fp);
    rec=tp/(tp+fn);

    f1=2*((prec*rec)/(prec+rec));
    if isnan(f1)
        if prec==0 && rec==0
            f1=0;
        end
    end
    f1Scores(iBand,5)=f1;
end

% tp,fp,tn,fn
% keyboard


end