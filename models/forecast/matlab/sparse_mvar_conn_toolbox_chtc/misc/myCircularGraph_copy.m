function this = myCircularGraph_copy(adjacencyMatrix,varargin)
% Constructor
p = inputParser;

defaultColorMap = parula(length(adjacencyMatrix));
defaultLabel = cell(length(adjacencyMatrix));
for i = 1:length(defaultLabel)
    defaultLabel{i} = num2str(i);
end
defaultMaxVal = -1;

addRequired(p,'adjacencyMatrix',@(x)(isnumeric(x) || islogical(x)));
addParameter(p,'ColorMap',defaultColorMap,@(colormap)length(colormap) == length(adjacencyMatrix));
addParameter(p,'Label'   ,defaultLabel   ,@iscell);
addParameter(p,'MaxVal', defaultMaxVal, @isfloat);

parse(p,adjacencyMatrix,varargin{:});
this.ColorMap = p.Results.ColorMap;
this.Label    = p.Results.Label;
maxVal = p.Results.MaxVal;

%       this.ShowButton = uicontrol(...
%         'Style','pushbutton',...
%         'Position',[0 40 80 40],...
%         'String','Show All',...
%         'Callback',@circularGraph.showNodes,...
%         'UserData',this);
%
%       this.HideButton = uicontrol(...
%         'Style','pushbutton',...
%         'Position',[0 0 80 40],...
%         'String','Hide All',...
%         'Callback',@circularGraph.hideNodes,...
%         'UserData',this);

%       fig = gcf;
%       set(fig,...
%         'UserData',this,...
%         'CloseRequestFcn',@circularGraph.CloseRequestFcn);

% Draw the nodes
if isfield(this,'Node')
    this = rmfield(this,'Node');
end
t = linspace(-pi,pi,length(adjacencyMatrix) + 1).'; % theta for each node

extent = zeros(length(adjacencyMatrix),1);
for i = 1:length(adjacencyMatrix)
    this.Node(i) = node2(cos(t(i)),sin(t(i)));
    this.Node(i).Color = this.ColorMap(i,:);
    this.Node(i).Label = this.Label{i};
    this.Node(i).MarkerSize=10; % default is 8 (line 58 of node2.m in case you want to change default)
end

% Find non-zero values of s and their indices
[row,col,v] = find(adjacencyMatrix);

% Calculate line widths based on values of s (stored in v).
minLineWidth  = 0;
lineWidthCoef = 2.5;
if maxVal > 0
    lineWidth = v./maxVal;
else
    lineWidth = v./max(v);
end
if sum(lineWidth) == numel(lineWidth) % all lines are the same width.
    lineWidth = repmat(minLineWidth,numel(lineWidth),1);
else % lines of variable width.
    lineWidth = lineWidthCoef*lineWidth + minLineWidth;
end

% Draw connections on the Poincare hyperbolic disk.
%
% Equation of the circles on the disk:
% x^2 + y^2
% + 2*(u(2)-v(2))/(u(1)*v(2)-u(2)*v(1))*x
% - 2*(u(1)-v(1))/(u(1)*v(2)-u(2)*v(1))*y + 1 = 0,
% where u and v are points on the boundary.
%
% Standard form of equation of a circle
% (x - x0)^2 + (y - y0)^2 = r^2
%
% Therefore we can identify
% x0 = -(u(2)-v(2))/(u(1)*v(2)-u(2)*v(1));
% y0 = (u(1)-v(1))/(u(1)*v(2)-u(2)*v(1));
% r^2 = x0^2 + y0^2 - 1

for i = 1:length(v)
    if row(i) ~= col(i)
        if abs(row(i) - col(i)) - length(adjacencyMatrix)/2 == 0
            % points are diametric, so draw a straight line
            u = [cos(t(row(i)));sin(t(row(i)))];
            v = [cos(t(col(i)));sin(t(col(i)))];
            this.Node(row(i)).Connection(end+1) = line(...
                [u(1);v(1)],...
                [u(2);v(2)],...
                'LineWidth', lineWidth(i),...
                'Color', (this.ColorMap(row(i),:)),...
                'PickableParts','none');
        else % points are not diametric, so draw an arc
            u  = [cos(t(row(i)));sin(t(row(i)))];
            v  = [cos(t(col(i)));sin(t(col(i)))];
            x0 = -(u(2)-v(2))/(u(1)*v(2)-u(2)*v(1));
            y0 =  (u(1)-v(1))/(u(1)*v(2)-u(2)*v(1));
            r  = sqrt(x0^2 + y0^2 - 1);
            thetaLim(1) = atan2(u(2)-y0,u(1)-x0);
            thetaLim(2) = atan2(v(2)-y0,v(1)-x0);
            
            if u(1) >= 0 && v(1) >= 0
                % ensure the arc is within the unit disk
                theta = [linspace(max(thetaLim),pi,50),...
                    linspace(-pi,min(thetaLim),50)].';
            else
                theta = linspace(thetaLim(1),thetaLim(2)).';
            end
            
            this.Node(row(i)).Connection(end+1) = line(...
                r*cos(theta)+x0,...
                r*sin(theta)+y0,...
                'LineWidth', lineWidth(i),...
                'Color', (this.ColorMap(row(i),:)),...
                'PickableParts','none');
        end
    else
        %% opt1: Draw circle surrounding source node for self-connectivity (SC). looks a bit cluttered (uncomment line 52 before running)
% %         maxLoopSz=.2;
% %         nodeR=this.Node(row(i)).MarkerSize/2; 
% %         % note: remove comment below to place circle at end of node rather
% %         % than intersecting node
% %         scale=1+(maxLoopSz*(lineWidth(i)/max(lineWidth)));%+nodeR;
% %         viscircles(this.Node(row(i)).Position*scale,maxLoopSz*(lineWidth(i)/max(lineWidth)),'LineWidth', lineWidth(i),...
% %                 'Color', (this.ColorMap(row(i),:)+this.ColorMap(col(i),:))/2);
        %% opt2: scale both size of node and line thickness for nodes with more self-connectivity. Larger ROIs presumably have more of this, so I like this depiction (uncomment line 52 before running)
%         maxNodeSize=30;
%         this.Node(row(i)).MarkerSize=lineWidth(i)/max(lineWidth)*maxNodeSize;
%         this.Node(row(i)).MarkerThickness=lineWidth(i);
        %% opt3: Scale line thickness for nodes with more self-connectivity. Erase nodes when SC does not exist
        if lineWidth(i) > 0
            SC_radius=.04;% desired radius of circle surrounding node point
            
            %% Actual radius drawn is modified by line thickness of
            % circle. Adjust radius' value accordingly if you want circles
            % to be exactly the same size. Empirically, LineWidth of 103 corresponds to additional .4 extension of
            % graph object on either side (i.e. line drawn on x-axis will extend to .4 and
            % -.4... this is different depending on size of figure, so only
            % a temporary solution.
%             figure
%             plot(1:3,[0,0,0],'LineWidth',103)
            SC_radius=SC_radius-((lineWidth(i)/103)*.4);
            
            %% Add self-connection
            u = [cos(t(row(i)));sin(t(row(i)))];
            this.Node(row(i)).Connection(end+1) = rectangle('Position', [u(1)-(SC_radius/2) u(2)-(SC_radius/2) SC_radius SC_radius],...
                'EdgeColor', this.Node(row(i)).Color,...
                'Curvature', [1 1],...
                'LineWidth', lineWidth(i));
        end
    end
end

axis image;
ax = gca;
for i = 1:length(adjacencyMatrix)
    extent(i) = this.Node(i).Extent;
end
extent = max(extent(:));
ax.XLim = ax.XLim + extent*[-1 1];
fudgeFactor = 1.75; % Not sure why this is necessary. Eyeballed it.
ax.YLim = ax.YLim + fudgeFactor*extent*[-1 1];
ax.Visible = 'off';
ax.SortMethod = 'depth';
fig = gcf;
fig.Color = [1 1 1];

%% graph object method below for adding self-connections... doesn't allow curved edges tho
% % % % % %% read varargin
% % % % % p = inputParser;
% % % % % 
% % % % % defaultColorMap = parula(length(adjacencyMatrix));
% % % % % defaultLabel = cell(length(adjacencyMatrix));
% % % % % for i = 1:length(defaultLabel)
% % % % %     defaultLabel{i} = num2str(i);
% % % % % end
% % % % % defaultMaxVal = -1;
% % % % % 
% % % % % addRequired(p,'adjacencyMatrix',@(x)(isnumeric(x) || islogical(x)));
% % % % % addParameter(p,'ColorMap',defaultColorMap,@(colormap)length(colormap) == length(adjacencyMatrix));
% % % % % addParameter(p,'Label'   ,defaultLabel   ,@iscell);
% % % % % addParameter(p,'MaxVal', defaultMaxVal, @isfloat);
% % % % % 
% % % % % parse(p,adjacencyMatrix,varargin{:});
% % % % % myColorMap = p.Results.ColorMap;
% % % % % labels    = p.Results.Label;
% % % % % maxVal = p.Results.MaxVal;
% % % % % 
% % % % % %% Init graph
% % % % % adjacencyMatrix=adjacencyMatrix./maxVal;
% % % % % % weightsVec=adjacencyMatrix(:);
% % % % % % weightsVec=weightsVec(weightsVec~=0);
% % % % % G=graph(adjacencyMatrix,labels);
% % % % % % G.Edges.Weight=weightsVec;
% % % % % M=size(adjacencyMatrix,1);
% % % % % idx=eye(M,M);
% % % % % 
% % % % % %% begin plot
% % % % % p=plot(G);
% % % % % 
% % % % % %% reorder nodes in circle
% % % % % layout(p,'circle')
% % % % % 
% % % % % %% remove weak connections
% % % % % % THRESH=0.05;%;
% % % % % % [rows, cols] = ind2sub(size(adjacencyMatrix), find(adjacencyMatrix<THRESH & adjacencyMatrix > 0));
% % % % % % T=digraph(rows,cols);
% % % % % % T=T.addnode(M-size(T.Nodes,1));
% % % % % % highlight(p,T,'EdgeColor','w');
% % % % % 
% % % % % %% fix distorted edges from removing overlapping edges in previous step
% % % % % [rows, cols] = ind2sub(size(adjacencyMatrix), find(adjacencyMatrix));
% % % % % for iConn=1:length(rows)
% % % % %     T=graph(rows(iConn),cols(iConn));
% % % % %     T=T.addnode(M-size(T.Nodes,1));
% % % % %     lineWidths=adjacencyMatrix(rows(iConn),cols(iConn))*10;
% % % % %     edgeColor=myColorMap(rows(iConn),:);
% % % % %     highlight(p,T,'EdgeColor',edgeColor,'LineWidth',lineWidths);
% % % % % end
% % % % % 
% % % % % %% color each node with it's color code in the hierarchy
% % % % % nodes=unique(rows)';
% % % % % for iNode=nodes
% % % % %     highlight(p,iNode,'NodeColor',myColorMap(nodes(iNode),:))
% % % % % end


