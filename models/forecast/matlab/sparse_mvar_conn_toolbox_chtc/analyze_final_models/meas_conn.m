function connMatrix=meas_conn(sampleRate,varCats,Ahat_lagBlks,covMatrix,diag_bool)

%% extract info from input params
Q=size(Ahat_lagBlks,1); % nScalar processes (nVar)
if Q ~= length(varCats)
    keyboard
end
p=size(Ahat_lagBlks,2)/Q; % model-order

%% Map virtual variable index to its proper category 
roiIndsMap=containers.Map;
unique_labels={};
for roiInd=1:length(varCats)
    if ~isKey(roiIndsMap,varCats{roiInd})%(3:end))
        roiIndsMap(varCats{roiInd})=roiInd;%(3:end))=roiInd;
        unique_labels{length(unique_labels)+1}=varCats{roiInd};%(3:end);
    else
        curInds=roiIndsMap(varCats{roiInd});%(3:end));
        curInds=[curInds,roiInd];
        roiIndsMap(varCats{roiInd})=curInds;%(3:end))=curInds;
    end
end

%% construct chCtPerROI
varCtPerROI=nan(length(unique(unique_labels)),1);
for roiInd=1:length(unique_labels)
    varCtPerROI(roiInd)=length(roiIndsMap(unique_labels{roiInd}));
end

%% multi-scale GC
% tau=5; %scale (NOTE: for tau=1 computes time-domain state-space GC, (ncoeff is irrelevant))
% ncoeff=6; % number of coeff of FIR lowpass filter
% [GCdws,GCflt,b]=msgc(Ahat_lagBlks,covMatrix,tau,ncoeff);
% connMatrix.mscg.GCdws=GCdws;
% connMatrix.mscg.GCflt=GCflt;
% connMatrix.mscg.b=b;

%% Run 3rdPartyCode
nfft=500; % # numFreqBins
if diag_bool
    [bDC,bPDC,mF,mG,bS,bP,bH,bAf,f]=block_fdMVAR_diag(Ahat_lagBlks,covMatrix,varCtPerROI,nfft,sampleRate);%non-generalized
else
    [bDC,bPDC,mF,mG,bS,bP,bH,bAf,f]=block_fdMVAR(Ahat_lagBlks,covMatrix,varCtPerROI,nfft,sampleRate);%non-generalized
end
nROIs=length(varCtPerROI);

%% Plot spectra
% figure % mF
% k=0;
% M=length(chCtPerROI);
% for i=1:M        
%     for j=1:M
%         if i==j
%             continue
%         end
%         plotInd=(M*i)-(M-j);
%         subplot(M,M,plotInd); 
%         plot(f, squeeze(bPDC(j,i,:)),'b'); %hold on; plot(f, squeeze(bPDC2(i,j,:)),'c:');
%         axis([0 sampleRate/2 0 1.1*max(bPDC(:))]);
%         set(gca, 'XScale', 'log')
%         xlim([0,10^2])
%         ylim([0,.5])
%         title([int2str(i) '->' int2str(j)]);
% 
%         k=k+1;        
%     end
% end
% suptitle(cond)
%% store measures averaged across freq. bins
% % %     h1=figure('numbertitle','off','name','Fig. 2a,b - nROIsultivariate total and direct causality: f,g');
% % %     h2=figure('numbertitle','off','name','Fig. 2c,d - Block Directed Coherence, Block Partial Directed Coherence');
nRows=((nROIs*nROIs)-nROIs)/2;
k=0;
ynROIsaxScale=1;
for i=1:nROIs        
    for j=1:nROIs    
        for iBand=1:length(FreqBands.Names)
            begFreq=FreqBands.Limits.(FreqBands.Names{iBand})(1);
            begFreq=find(f>=begFreq,1,'first');
%                 if begFreq == 5
%                     begFreq=1;
%                 end
            if iBand~=length(FreqBands.Names)
                endFreq=FreqBands.Limits.(FreqBands.Names{iBand})(2);
                endFreq=find(f>=endFreq,1,'first');
            else
                endFreq=nfft;
            end
            %% store as connMatrix.mF(iBand,j,i) to undo bass ackwarks way Faes toolbox stores connectivity (so can read as row a causes col b after I store like this)
            connMatrix.mF(iBand,j,i)=sum(mF(i,j,begFreq:endFreq))./length(begFreq:endFreq);
            connMatrix.mG(iBand,j,i)=sum(mG(i,j,begFreq:endFreq))./length(begFreq:endFreq);
            connMatrix.bDC(iBand,j,i)=sum(bDC(i,j,begFreq:endFreq))./length(begFreq:endFreq);
            connMatrix.bPDC(iBand,j,i)=sum(bPDC(i,j,begFreq:endFreq))./length(begFreq:endFreq);
        end
% % %         figure(h1); % mF
% % %         subtightplot(nRows,4,4*k+1); plot(f, squeeze(mF(i,j,:)),'b'); %hold on; plot(f, squeeze(mF2(i,j,:)),'c:');
% % %         axis([0 fc/2 0 ynROIsaxScale*max(mF(:))]);% title(['f^(^m^)' int2str(j) '->' int2str(i)]);
% % %         set(gca,'XScale','log')
% % %         subtightplot(nRows,4,4*k+2); plot(f, squeeze(mF(j,i,:)),'b'); %hold on; plot(f, squeeze(mF2(j,i,:)),'c:');
% % %         axis([0 fc/2 0 ynROIsaxScale*max(mF(:))]); %title(['f^(^m^)' int2str(i) '->' int2str(j)]);
% % %         set(gca,'XScale','log')
% % % 
% % %         subtightplot(nRows,4,4*k+3); plot(f, squeeze(mG(i,j,:)),'r'); %hold on; plot(f, squeeze(mG2(i,j,:)),'m:');
% % %         axis([0 fc/2 0 ynROIsaxScale*max(mF(:))]);% title(['g^(^m^)' int2str(j) '->' int2str(i)]);
% % %         set(gca,'XScale','log')
% % % 
% % %         subtightplot(nRows,4,4*k+4); plot(f, squeeze(mG(j,i,:)),'r'); %hold on; plot(f, squeeze(mG2(j,i,:)),'m:');
% % %         axis([0 fc/2 0 ynROIsaxScale*max(mF(:))]);% title(['g^(^m^)' int2str(i) '->' int2str(j)]);
% % %         set(gca,'XScale','log')
% % % 
% % %         figure(h2); % mF
% % %         subtightplot(nRows,4,4*k+1); plot(f, squeeze(bDC(i,j,:)),'b'); %hold on; plot(f, squeeze(bDC2(i,j,:)),'c:');
% % %         axis([0 fc/2 -0.05 1]);% title(['bDC' int2str(i) int2str(j)]);
% % %         set(gca,'XScale','log')
% % % 
% % %         subtightplot(nRows,4,4*k+2); plot(f, squeeze(bDC(j,i,:)),'b'); %hold on; plot(f, squeeze(bDC2(j,i,:)),'c:');
% % %         axis([0 fc/2 -0.05 1]);% title(['bDC' int2str(j) int2str(i)]);
% % %         set(gca,'XScale','log')
% % % 
% % %         subtightplot(nRows,4,4*k+3); plot(f, squeeze(bPDC(i,j,:)),'r'); %hold on; plot(f, squeeze(bPDC2(i,j,:)),'m:');
% % %         axis([0 fc/2 -0.05 1]); %title(['bPDC' int2str(i) int2str(j)]);
% % %         set(gca,'XScale','log')
% % % 
% % %         subtightplot(nRows,4,4*k+4); plot(f, squeeze(bPDC(j,i,:)),'r'); %hold on; plot(f, squeeze(bPDC2(j,i,:)),'m:');
% % %         axis([0 fc/2 -0.05 1]);% title(['bPDC' int2str(j) int2str(i)]);
% % %         set(gca,'XScale','log')
% % % 
% % %         k=k+1;        

    end
end
