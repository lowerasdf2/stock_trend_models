function [err,varargout] = oneStepPredErr(X,p,l,A_hat,B_hat,stim_bool,u,plotBool,varargin)

%%%%%%%%%%% Vars you should set to run this
%%% X = cell array containing epochs of data. 
% If usign 3 epochs, 10 chs, and 100-sample epochs, should look like this:
% {[10x100],[10x100],[10x100]}
%% p = model order
%% A_hat = model parameters
% if using 10 chs and p=5, should be size: 10x50. Each 10x10 block (nBlocks=p)
% should contain coefficients that are meant to be applied to a specific lag

%%%%%%%%%%% Vars I'm guessing you won't use and can set to zero/false
%% l = stim effect (for using exogenous vars; you can set this to 0 when there are no exogenous vars or stimulus input)
%% stim_bool (says whether or not a stimulus effect is being modeled)
%% u = stimulus representated as a boolean vector
if ~isempty(varargin)
    N=varargin{1};
    q=varargin{2};
end
n_o = max(p, l - 1);
epochRes=[];
if iscell(X)
    M=size(X{1},1);
    epochErrs=nan(length(X),1);
    for j = 1:length(X)
        n_spl = size(X{j}, 2) - n_o;
        X_full=X{j};
        %% construct design matrix (Z) for epoch
        Z = zeros(M * p + l, n_spl);
        for i = 1:p
            Z((i - 1) * M + 1:i * M, :) = X{j}(:, n_o + 1 - i:end - i); 
        end
        if stim_bool
            D = toeplitz([u{j}(1); zeros(l - 1, 1)], u{j});
            Z(M * p + 1:end, :) = D(:, n_o + 1:end);
        end

        if stim_bool
            theta=horzcat(A_hat,B_hat);
        else
            theta=A_hat;
        end
        y_hat=theta*Z;
        %% NOTE (from book pg. 70, sec. 3.2.1): vec(y_hat)==y_hat(:)==kron(Z',eye(size(y_hat,1)))*theta(:)==kron(theta',eye(size(y_hat,1)))*theta(:)
        % vecYhat=y_hat(:);
        % vecYhat2=kron(Z',eye(size(y_hat,1)))*theta(:);
        % vecYhat3=kron(Z',eye(size(y_hat,1)))*theta(:);
        % figure
        % plot(vecYhat)
        % hold on
        % plot(vecYhat2)
        % hold on
        % plot(vecYhat3)
        y=X{j}(:,n_o+1:end);
        epochRes(:,:,j)=y_hat-y;
        err=sum(sum(((epochRes(:,:,j)).^2)))/(size(y,1)*size(y,2));
        epochErrs(j)=err;

        %% code to plot
        if plotBool && j==1 || j==2 || j==3
            figure
            nChsToPlot=min([10,size(y,1)]);
            plotDur=min([4000,size(y,2)]);
            shiftSz=10;
            wf_shift = (0:-shiftSz:(nChsToPlot-1)*(-shiftSz))';
            plot((y(1:nChsToPlot,1:plotDur) + wf_shift(:, ones(1, plotDur)))', 'Color', [31,120,180] / 255);
            hold on
            plot((y_hat([1:nChsToPlot],1:plotDur) + wf_shift(:, ones(1, plotDur)))', 'Color', [255 0 0] / 255);
            title('1-step error')
        end
    end
    %% avg error
    err=nanmean(epochErrs);
    varargout{1}=epochRes;
    varargout{2}=y_hat;
    varargout{3}=y;
else
    error('input is in wrong format')
end

