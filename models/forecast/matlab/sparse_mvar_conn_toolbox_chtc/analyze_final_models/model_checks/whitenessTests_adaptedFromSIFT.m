function stats=whitenessTests_adaptedFromSIFT(epochRes,morder,g)
%    epochRes:  [M x npoints-p x ntrials] matrix of residuals (1-step ahead
%               prediction error). Note that
nchs=size(epochRes,1);%
ntr=size(epochRes,3);%nEpochs
npntsTotal = ntr*size(epochRes,2);            
t=1;
% calculate residual autocovariance and autocorrelation matrices
% ---------------------------------------------------------------------
[racvf{t} racf{t}] = deal(zeros(ntr,nchs,nchs*(g.numAcfLags+1))); % residual autocov, autocorr
resvar{t}          = zeros(ntr,nchs);  % residual variance

% first calculate autocovariance/autocorrelation for each trial ...
for tr=1:ntr
    % R = [Rs1s1 Rs1s2 Rs1s3 Rs2s1 Rs2s2 Rs2s3 Rs3s1 Rs3s2 Rs3s3 ...]
    tmpacvf = xcov(epochRes(:,:,tr)',g.numAcfLags,'biased');

    % extract positive lags
    tmpacvf = tmpacvf(g.numAcfLags+1:end,:);         
    for lag = 1:g.numAcfLags+1
        racvf{t}(tr,:,(1:nchs)+nchs*(lag-1)) = reshape(tmpacvf(lag,:),[nchs nchs])';
    end

    % convert autocov to autocorrelation function
    rvar{t}(tr,:) = 1./sqrt(diag(squish(racvf{t}(tr,1:nchs,1:nchs))));
    D = diag(rvar{t}(tr,:));
    for lag = 1:g.numAcfLags+1
        racf{t}(tr,:,(1:nchs)+nchs*(lag-1)) = D*squish(racvf{t}(tr,:,(1:nchs)+nchs*(lag-1)))*D;
    end
end
% ... finally average autocovariance/correlation matrices across trials
racf{t}      = squish(mean(racf{t},1));
racvf{t}     = squish(mean(racvf{t},1));
rvar{t}      = squish(mean(rvar{t},1));
% figure
% nPlotChs=50;
%% Plot 
corLagsFig=figure;
for iVar=1:size(epochRes,1)
	xx=[];
    yy=[];
    for iTrial=1:size(epochRes,3)
        x=autocorr(epochRes(iVar,:,iTrial));
        figure
        autocorr(epochRes(iVar,:,iTrial),'NumLags',40);
        xx=[xx;x];
        y=parcorr(epochRes(iVar,:,iTrial));    
        yy=[yy;y];
    end
    x_mu=mean(xx,1);
    y_mu=mean(yy,1);
    figure(corLagsFig)
    subplot(2,1,1)
    hold on
    plot(0:length(x_mu)-1,x_mu)
    subplot(2,1,2)
    hold on
    plot(0:length(y)-1,y)
end
%% These two plots should be equivalent
% figure
% plot(0:length(x_mu)-1,x_mu)
% hold on
% plot(0:length(x_mu)-1,racf{t}(end,nchs:nchs:size(racvf{t},2)))

%% compute whiteness criteria
% ---------------------------------------------------------------------
% TODO: performance can be improved by avoiding recomputation of similar statistics 
g.whitenessCriteria{1}='acf';
g.whitenessCriteria{2}='ljungbox';
g.whitenessCriteria{3}='boxpierce';
g.whitenessCriteria{4}='limcleod';

for i=1:length(g.whitenessCriteria)
    switch lower(g.whitenessCriteria{i})
        case 'acf'
            % test residual autocorrelations
            sigthresh = 1.96/sqrt(npntsTotal);    % 0.95 confidence interval for acf
            numNonSigCoeffs = sum(sum(abs(racf{t}(:,nchs+1:end))<sigthresh));    % count how many coefficients are within the bounds for 'white' residuals
            numCoeffs = numel(racf{t}(:,nchs+1:end));                            % count the total number of coefficients
            stats.acf.pval(t) = numNonSigCoeffs / numCoeffs;                     % estimate the probability for a coefficient to be inside the bounds (probability of whiteness)
            stats.acf.w(t) = stats.acf.pval(t) > 1-g.alpha;                       
            stats.acf.acffun{t} = racf{t};
            stats.acf.fullname = 'ACF';
        case 'ljungbox' 
            % ljung-box (modified portmanteau) test for residual autocorrelation up to lag h
            Qh=0;
            C0inv = inverse(racvf{t}(1:nchs,1:nchs));
            for k=1:g.numAcfLags
                Qh = Qh + 1/(npntsTotal-k) * trace(racvf{t}(1:nchs,(1:nchs)+k*nchs)'*C0inv*racvf{t}(1:nchs,(1:nchs)+k*nchs)*C0inv);
            end
            stats.ljungbox.value(t) = Qh*npntsTotal*(npntsTotal+2);
            stats.ljungbox.pval(t) = 1-chi2cdf(stats.ljungbox.value(t),(nchs^2)*(g.numAcfLags-morder));
            stats.ljungbox.w(t)= stats.ljungbox.pval(t)>g.alpha;
            stats.ljungbox.fullname = 'Ljung-Box';
        case 'boxpierce' 
            % box-pierce portmanteau test for residual autocorrelation up to lag h
            Qh=0;
            C0inv = inverse(racvf{t}(1:nchs,1:nchs));
            for k=1:g.numAcfLags
                Qh = Qh + trace(racvf{t}(1:nchs,(1:nchs)+k*nchs)'*C0inv*racvf{t}(1:nchs,(1:nchs)+k*nchs)*C0inv);
            end
            stats.boxpierce.value(t) = Qh*npntsTotal;   % Qh*npntsTotal^2  is the modified portmanteau test (Lutkepohl, p.171)
            stats.boxpierce.pval(t) = 1-chi2cdf(stats.boxpierce.value(t),(nchs^2)*(g.numAcfLags-morder));
            stats.boxpierce.w(t)= stats.boxpierce.pval(t)>g.alpha;
            stats.boxpierce.fullname = 'Box-Pierce';
        case 'limcleod'
            % Li-McLeod portmanteau test for residual autocorrelation up to lag h
            Qh=0;
            C0inv = inverse(racvf{t}(1:nchs,1:nchs));
            for k=1:g.numAcfLags
                Qh = Qh + trace(racvf{t}(1:nchs,(1:nchs)+k*nchs)'*C0inv*racvf{t}(1:nchs,(1:nchs)+k*nchs)*C0inv);
            end
            stats.limcleod.value(t) = Qh*npntsTotal + nchs^2*g.numAcfLags*(g.numAcfLags+1)/(2*npntsTotal);
            stats.limcleod.pval(t) = 1-chi2cdf(stats.limcleod.value(t),(nchs^2)*(g.numAcfLags-morder));
            stats.limcleod.w(t) = stats.limcleod.pval(t) > g.alpha;    % using bonferroni correction for multiple tests (across channels)
            stats.limcleod.fullname = 'Li-McLeod';
        otherwise
            fprintf('Unknown whiteness test %s',g.whitenessCriteria{i}); 
    end
end

