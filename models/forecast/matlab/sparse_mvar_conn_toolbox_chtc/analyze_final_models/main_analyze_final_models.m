function main_analyze_final_models(pars,dataIDparams)
%% main script for running post-hoc analyses from fitted VAR models
% 1) model_checks: Runs test for serial correlation of residuals
% 2) meas_conn: Measure connectivity from model params using measure of
% choice - PDC/DC (implemented via 3rdParty\eMVAR\functions)
% 3) test_conn: Determine which connections/interactions are significant
% 4) plot_conn: Plot connectivity graphs

%% Add paths for post-hoc stuff
addpath([pars.dirs.postHocAnalyze filesep 'plot_connectivity\paul-kassebaum-mathworks-circularGraph-3a7926b'])
addpath([pars.dirs.baseCodeDir filesep  '3rdParty' filesep 'blockMVAR'])

%% Set constants
dagDir=createDagDirName(pars);

%% Init vars
connMatrices=struct; % store connectivity measurements across conds&freqs
close all
%% Loop through finalized models (one in each condition)
for cond=dataIDparams.conds
    pars.dataType.condition=cond{1};
    if pars.run.postHoc.conn.skipCond.bool
        if strcmp(pars.dataType.condition,pars.run.postHoc.conn.skipCond.name)
            continue
        end
    elseif pars.run.postHoc.conn.oneCond.bool
        if ~strcmp(pars.dataType.condition,pars.run.postHoc.conn.oneCond.name)
            continue
        end
    end
    chtc_out_dir=[pars.dirs.baseResultsDir filesep 'chtc_complete' filesep pars.dataType.globalDataCat filesep ...
        pars.dataType.varSelect filesep pars.dataType.catType filesep pars.dataType.dataID filesep ...
        filesep pars.dataType.condition filesep dagDir];
    if ~exist([chtc_out_dir],'dir') 
        keyboard
    end
    if pars.cv.nested.bool
        finalModelFile=dir([chtc_out_dir filesep 'finalSparseModel*.mat']);
    elseif pars.cv.p.run
        finalModelFile=dir([chtc_out_dir filesep 'finalFullyConnectedModel*.mat']);
    else
        keyboard
    end
    if isempty(finalModelFile)
        keyboard % output doesn't exist or dagDirName needs to be updated to reflect latest dag version
    end
    finalModelFile=finalModelFile(1).name;
    
    %% load final model
    load([chtc_out_dir filesep finalModelFile])

    %% Load dataInfo (get ROIs used for model)
    % set dir containing epochData
%     epochDir=[pars.dirs.dimRedData pars.dataType.dataID filesep pars.dataType.condition filesep];
    if isempty(dir([chtc_out_dir filesep 'dataEpochs*']))
        keyboard % dataEpochs should be copied to output dir when model is complete
    end
    epochFile=getEpochFilename(pars); 
    if ~exist([chtc_out_dir filesep epochFile],'file')
        baseOldFile1=['dataEpochs_trainTestIndices_epLen' num2str(pars.dataType.epochLen) '_' num2str(pars.cv.p.K) 'folds_' num2str(pars.cv.p.dimRed.ch.lasso.K) 'subfolds_' ...
            pars.dataType.globalDataCat '_' pars.dataType.catType '_'];
        baseOldFile2=['dataEpochs_trainTestIndices_' num2str(pars.cv.p.K) 'folds_' num2str(pars.cv.p.dimRed.ch.lasso.K) 'subfolds_' ...
            pars.dataType.globalDataCat '_' pars.dataType.catType '_'];        
        if strcmp(pars.dataType.varSelect,'ALLVARS')
            oldFileName1=[baseOldFile1 'ALLchs_'];
            oldFileName2=[baseOldFile2 'ALLchs_'];
        elseif strcmp(pars.dataType.varSelect,'AUDITORY')
            oldFileName1=[baseOldFile1 'AUDITORYchs_'];
            oldFileName2=[baseOldFile2 'AUDITORYchs_'];
        else
            keyboard
        end
        epochFile1=[oldFileName1 num2str(pars.dataType.sampleRate) 'hz_pcaPerc' num2str(pars.cv.genDimRed.ch.PCA.perc*100) '.mat'];
        epochFile2=[oldFileName2 num2str(pars.dataType.sampleRate) 'hz_pcaPerc' num2str(pars.cv.genDimRed.ch.PCA.perc*100) '.mat'];
        if exist([chtc_out_dir filesep epochFile1],'file')
            movefile([chtc_out_dir filesep epochFile1],[chtc_out_dir filesep  epochFile])
        elseif exist([chtc_out_dir filesep epochFile2],'file')
            movefile([chtc_out_dir filesep epochFile2],[chtc_out_dir filesep  epochFile])
        else
            keyboard
        end
    end
    epochFile=dir([chtc_out_dir filesep epochFile]);
    if length(epochFile) ~=1
        keyboard
    else
        load([chtc_out_dir filesep epochFile(1).name])
        if ~exist('dataInfo','var') || ~isfield(dataInfo,'varCat')
            disp('WARNING: Processing older dataset. Adding dataInfo to epochs file...')
            %% load condData and try to create correct dataEpochs file
            % load updated epochs file (run batch_main_fit_data_chtc.m with
            % pars.run.bools.addLabelToDataFile=true to get this file
            [origDataSaveFile,dataSaveDir]=getDimRedDataFileName(pars,cond{1});
            if ~exist([dataSaveDir filesep origDataSaveFile],'file')
                createInputData=true;
            else
                createInputData=false;
            end
            formatData(pars,dataIDparams,createInputData) % runs PCA, creates orig input data files, runs runNonCHTCcode if desired
            [dataSaveDir,dataSaveFile]=createDataEpochs(pars);
            fullFileDir=[dataSaveDir filesep dataSaveFile];
            x=load(fullFileDir);
            if x.n_epoch ~= n_epoch
                keyboard
            elseif (norm(dataEpochs{1}-x.dataEpochs{1},'fro')>1e-7) || (norm(dataEpochs{2}-x.dataEpochs{2},'fro')>1e-7) || (norm(dataEpochs{3}-x.dataEpochs{3},'fro')>1e-7)
                keyboard
            elseif ~isequal(indices_cv_p,x.indices_cv_p)
                keyboard
            elseif isfield(x,'indices_cv_p_lasso') && (~isequal(indices_cv_p_lasso,x.indices_cv_p_lasso))
                keyboard
            end
            if exist('dataInfo','var') && isfield(dataInfo,'label')
                if ~isequal(dataInfo.label,x.dataInfo.varCat)
                    keyboard % problematic
                end
            end
            dataInfo=x.dataInfo;
            if pars.cv.p.dimRed.ch.lasso.bool
                save([chtc_out_dir filesep dataSaveFile],'dataEpochs','n_epoch','indices_cv_p','indices_cv_p_lasso','dataInfo')
            else
                save([chtc_out_dir filesep dataSaveFile],'dataEpochs','n_epoch','indices_cv_p','dataInfo')
            end  
        end
    end
    
    %% model checks: Model stability, serial correlation
    addpath([pars.dirs.baseCodeDir filesep '..' filesep '..' filesep 'eeglab\plugins\tmullen-sift_public_beta-c5404fa778c6\utils\squish'])
    addpath([pars.dirs.baseCodeDir filesep '..' filesep '..' filesep 'eeglab\plugins\tmullen-sift_public_beta-c5404fa778c6\external\Factorize\Factorize'])
    [covMatrix,whiteness_stats]=check_model(pars,dataEpochs,Ahat_lagBlks);
    
    %% measure connectivity between ROIs
%     diag_bool=1;
%     connMatrices_gen.(pars.dataType.condition)=meas_conn(pars.dataType.sampleRate,dataInfo.label,Ahat_lagBlks,diag_bool);
    diag_bool=1;
    try
        connMatrices.(pars.dataType.condition)=meas_conn(pars.dataType.sampleRate,dataInfo.varCat,Ahat_lagBlks,covMatrix,diag_bool);
    catch why
        keyboard
    end
%     close all
%     figure
%     image
%     imagesc(squeeze(connMatrices.(pars.dataType.condition).bPDC(1,:,:)-connMatrices_gen.(pars.dataType.condition).bPDC(1,:,:)),[0,1])
    
end
%% plot connectivity
plot_conn_across_conds(pars,dataIDparams,dataInfo.varCat,connMatrices)


