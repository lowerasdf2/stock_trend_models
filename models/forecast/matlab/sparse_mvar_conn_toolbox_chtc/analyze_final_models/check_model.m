function [innovNoiseCovMat,whiteness_stats]=check_model(pars,dataEpochs,Ahat_lagBlks)
whiteness_stats=[];
%% extract info from input params
Q=size(Ahat_lagBlks,1); % nScalar processes (nChs)
p=size(Ahat_lagBlks,2)/Q; % model-order

%% Zero mean data because this was not done for newly processed smaller segments of data...
allData={zscore(horzcat(cell2mat(dataEpochs(:)'))')'};
%% place zero-mean (scored over ENTIRE trainset) data back into epochs
startInd=1;
endInd=size(dataEpochs{1},2);
epochLen=endInd;
for iEpoch=1:length(dataEpochs)
    dataEpochs{iEpoch}=allData{1}(:,startInd:endInd);
    startInd=startInd+epochLen;
    endInd=endInd+epochLen;
end

%% Method for determining optimal window length
% determineOptimalWindowLen_AMVAR()

%% Plot 1st lag of final coefs
figure
subplot(2,1,1)
imagesc(Ahat_lagBlks(:,1:size(Ahat_lagBlks,1)))
subplot(2,1,2)
imagesc(Ahat_lagBlks)

%% Run outlier detection?
% mahalDist_epochOutliers(dataEpochs);

% % moutlier1(allData{1},.05)
% TF=isoutlier(allData{1}(:,:));
% figure
% plot(1:length(TF),sum(TF))

%% Calculate moving average
% avgWindow=50;
% figure
% for iVar=1:size(allData{1},1)
%     avgs=movmean(allData{1}(iVar,:),avgWindow);
%     hold on
%     plot(1:length(avgs),avgs)
% end
% figure
% plot(allData{1}(1,:))

%% Report sparsity levels
possConn=size(Ahat_lagBlks,1)^2;
percConn=(possConn-(sum(sum(Ahat_lagBlks==0))/p))/possConn;
disp(['Model retained ' num2str(percConn*100) '% of connections.'])

%% check for model stability
% [stbl,lambda]=is_stbl(Ahat_lagBlks);
% if stbl
%     disp(['Model is stable, lambda=' num2str(lambda)])
% else
%     disp(['WARNING! Model is unstable, lambda=' num2str(lambda)])
% end

%% Calculate epsilons and innovation noise matrix 
% % % 
innovNoiseCovMat=calcCovMatrix(allData);

%% Check what portion of correlation structure in data is captured by fitted MVAR model (code here adapted from SIFT)
% Method described here: https://link.springer.com/content/pdf/10.1007%2Fs004229900137.pdf
% % % % % % % % % % % % % % % % % % % % % % doNorm=false;
% % % % % % % % % % % % % % % % % % % % % % Nr=length(dataEpochs);
% % % % % % % % % % % % % % % % % % % % % % % convert data to SIFT format
% % % % % % % % % % % % % % % % % % % % % % data=nan(size(dataEpochs{1},1),size(dataEpochs{1},2),length(dataEpochs));
% % % % % % % % % % % % % % % % % % % % % % for i=1:length(dataEpochs)
% % % % % % % % % % % % % % % % % % % % % %     data(:,:,i)=dataEpochs{i};
% % % % % % % % % % % % % % % % % % % % % % end
% % % % % % % % % % % % % % % % % % % % % % c=nan(length(dataEpochs),1);
% % % % % % % % % % % % % % % % % % % % % % for i=1:length(dataEpochs)
% % % % % % % % % % % % % % % % % % % % % %     c(i)=est_consistency_adaptedFromSIFT(data(:,:,i),Ahat_lagBlks,innovNoiseCovMat,doNorm,Nr);
% % % % % % % % % % % % % % % % % % % % % % end
% % % % % % % % % % % % % % % % % % % % % % consistencyFig=figure;
% % % % % % % % % % % % % % % % % % % % % % plot(1:length(c),c)
% % % figure
% % % % R = corrcov(C)
% % % imagesc(corrcov(covMatrix),[-1,1])
% % % title([cond ': noise correlation matrix'])
B_hat=[];
u=[];
%% Calculate model-error on entire dataset (would probs be better to use hold-out set here). Plot example of 1-step error to ensure there's not a larger problem to be addressed
plotBool=true;
[err,epochRes]=oneStepPredErr(dataEpochs,p,pars.modelParams.l,Ahat_lagBlks,B_hat,pars.modelParams.l,u,plotBool);
[err,fullRes]=oneStepPredErr(allData,p,pars.modelParams.l,Ahat_lagBlks,B_hat,pars.modelParams.l,u,plotBool);
disp(['Avg chErr=' num2str(err)])

%% check if individuals channels are stationary
% lags=0;%0:9;
% winSizes=[5,10:30:300];%100,150:50:500,600:100:2000];
% allPs=nan(Q,length(winSizes),floor(size(allData{1},2)/min(winSizes)),length(lags));
% allStats=nan(Q,length(winSizes),floor(size(allData{1},2)/min(winSizes)),length(lags));
% for iVar=1:1
%     windowLenInd=1;
%     for windowLen=winSizes
%         startInd=1;
%         endInd=windowLen;
%         segInd=1;
%         while endInd <= size(allData{1},2)
% %             [~,pValue,stats] = kpsstest(allData{1}(iVar,startInd:endInd),'Lags',lags,'Trend',false);
%             stats=kpsstest(allData{1}(iVar,startInd:endInd));%,'Lags',lags,'Trend',false);
%             startInd=startInd+round(windowLen/2);
%             endInd=endInd+round(windowLen/2);
% %             allPs(iVar,windowLenInd,segInd,1:length(pValue))=pValue;
%             allStats(iVar,windowLenInd,segInd,1:length(stats))=stats;
%             segInd=segInd+1;
%         end
%         windowLenInd=windowLenInd+1;
%     end
% end
% avgP=squeeze(nanmean(allStats,1));%avg over chs
% avgP=squeeze(nanmean(avgP,2));%avg over segments
% figure
% for iLag=1:length(lags)
%     hold on
%     plot(winSizes,avgP(:,iLag))
% end    

%% Check for cointegration
% [h,pValue,stat,cValue,mles] = jcitest(allData{1}(1:2,:)','Lags',1:5:30);% performs the Johansen cointegration test on a data matrix Y.

%% Plot residuals and/or cyclical errors to search for relevant lags
% % % figure
% % % plotDur=50000;
% % % errDiffs=[];
% % % for iVar=1:size(fullRes,1)
% % % %     hold on
% % % %     plot(1:plotDur,fullRes(iVar,1:plotDur))    
% % %     [pkVals,errEvents]=findpeaks(fullRes(iVar,1:end),'MinPeakHeight',mean(fullRes(iVar,1:end))+(2*std(fullRes(iVar,1:end))),'MinPeakDistance',5);%,
% % %     figure
% % %     hist(diff(errEvents),500)
% % %     set(gca,'XScale','log')
% % %     errDiffs=[errDiffs,diff(errEvents)];
% % % end
% % % figure
% % % hist(errDiffs,100)
%% whiteness test
% g.numAcfLags=30;
% g.alpha=.05;
% whiteness_stats=whitenessTests_adaptedFromSIFT(fullRes,p,g);
% moutlier1(
%% other whiteness test
% maxlag=10;
% alpha=.1;
% [pp,Qh,critlo,crithi,crit1tail,C0,stringout,stringflag]=test_whiteness(epochRes(:,:,1),p,maxlag,alpha);
% disp(stringout)
% keyboard
% figure
% for iVar=1:10
%     hold on
%     plot(1:size(epochRes,2),epochRes(iVar,:,1))
% end

function covMatrix=calcCovMatrix(allData)
    disp('calculating error covariance matrix based on training set.')
    eye(size(Ahat_lagBlks,1));
    % concat all dataEpochs
    % create design matrix
    l=pars.modelParams.l;
    n_o=max(p, l - 1);
    n_spl=size(allData{1}, 2)-n_o;
    Z_full = zeros(Q * p + l, n_spl);
    for i = 1:p
        shiftedSignal=allData{1}(:, n_o + 1 - i:end - i);
        Z_full((i - 1) * Q + 1:i * Q, :) = shiftedSignal;
    end
    if pars.modelParams.l~=0
        keyboard % add functionality (should look similar to below; see orig mvarx_fit.m)
        %         D = toeplitz([u{j}(1); zeros(l - 1, 1)], u{j});
        %         Z_full(Q * p + 1:end, :) = D(:, n_o + 1:end);
    end
    if pars.modelParams.l~=0
        keyboard % add functionality
        %             keyboard % make sure we're all set here for wavelets and norm
        %         Epsilons{j} = Data_cltd{j} - theta * Z_cltd{j};
    else
        % wavelet case (testing no stim first)
        oneStepSignal=Ahat_lagBlks*Z_full;
        epsilons=allData{1}(:,p+1:end)-oneStepSignal;
    end
    covMatrix=epsilons*epsilons';
end

function determineOptimalWindowLen_AMVAR()
    chA=1;
    chB=5;
    % [r,lags]=xcorr(allData{1}(chA,:),allData{1}(chB,:));
    % figure
    % plot(r)
    % CORRGRAM(A,B,MAXLAG,WINDOW,NOVERLAP) 
    N=size(allData{1},2);
    % [c_out, l_out, t_out]=corrgram(allData{1}(chA,:),allData{1}(chB,:),floor(0.1*N),floor(0.1*N))
    figure
    subplot(2,1,1)
    plotLen=min(size(allData{1},2),1000);
    plot(allData{1}(chA,1:plotLen))
    hold on
    plot(allData{1}(chB,1:plotLen))
    subplot(2,1,2)
    x=allData{1}(chA,1:plotLen); % 39.96 seconds of recording
    y=allData{1}(chB,1:plotLen);

    windowLens=1:10:51;
    windowLens_cell=mat2cell(windowLens,ones(size(windowLens,1),1),ones(size(windowLens,2),1));
    windowLens_str=cellfun(@num2str,windowLens_cell,'uni',false);
    clear windowLens_cell
    for windowLen=windowLens
        if windowLen==1
            crossC=x.*y;
        else
            % ASSUME windowLen is odd
            lNrN=floor(windowLen/2);
            crossC=[];
            for t=1:length(x)
                if t-lNrN <= 0
                    continue
                elseif t+lNrN > length(x)
                    continue
                else
                    xTemp=x(t-lNrN:t+lNrN);
                    yTemp=y(t-lNrN:t+lNrN);
                    crossC(t)=mean(xTemp.*yTemp);
                end
            end
        %     end
        end
        hold on
        plot(crossC)
        % window length = 30 is probably the highest I can go based on this methodology:https://link.springer.com/content/pdf/10.1007%2Fs004229900137.pdf 
    end
    legend(windowLens_str)
    % % % % % % % % % % % % % % % corrgram(allData{1}(chA,:),allData{1}(chB,:))
    % % % % % % % % % % % % % % % 
    % % % % % % % % % % % % % % % 
    % % % % % % % % % % % % % % % c_out=corrgram(allData{1}(chA,:),allData{1}(chB,:),maxLag,windowLen)
    % % % % % % % % % % % % % % % 
    % % % % % % % % % % % % % % % hold on
    % % % % % % % % % % % % % % % figure
    % % % % % % % % % % % % % % % plot(1:length(c_out),c_out)
    % % % % % % % % % % % % % % % [c_out,l_out,t_out]=corrgram(allData{1}(chA,:),allData{1}(chB,:),maxLag,windowLen)

end
    
end
