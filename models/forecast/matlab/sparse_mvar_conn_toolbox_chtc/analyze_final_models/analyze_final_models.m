function [dataInfo,orig_labels,Mv,roiIndsMap,connMatrices]=analyze_final_models(pars,batchParams_anes,varargin)



%% code to prep different analysis output dirs (will move this to its proper home)
% % 1) Create connMeas output dir
% patientResultsDir=[pars.dirs.baseResultsDir filesep 'chtc_complete' filesep pars.dataType.globalDataCat filesep pars.dataType.varSelect filesep pars.dataType.dataID filesep];
% connMeasDir=[patientResultsDir filesep 'connMeas'];
% if ~exist(connMeasDir,'dir')
%     mkdir(connMeasDir)
% end
% % 2) Move conditional data into its own fitted_models directory
% fittedModelsDir=[patientResultsDir filesep 'fitted_models'];
% if ~exist(fittedModelsDir,'dir')
%     mkdir(fittedModelsDir)
% end
% condFolders=dir([patientResultsDir filesep 'OAAS*']);
% for iFolder=1:length(condFolders)
%     movefile([patientResultsDir filesep condFolders(iFolder).name],[fittedModelsDir filesep condFolders(iFolder).name])
% end
% % check if conn has already been measured. if not, run analysis for all
% % conditions in batchParams
% outFile=[connMeasDir filesep dagDir filesep 'connData.mat'];
% if ~exist(outFile,'file')
%     [dataInfo,orig_labels,Mv,roiIndsMap,connMatrices]=analyzeFinalModels(pars,dagDir,batchParams_anes);
%     save(outFile,'dataInfo','orig_labels','connMatrices');
% else
%     load(outFile) 
%     if length(fields(connMatrices))~=length(batchParams_anes.cond)
%         [dataInfo,orig_labels,Mv,roiIndsMap,connMatrices]=analyze_final_models(pars,dagDir,batchParams_anes);
%         save(outFile,'dataInfo','orig_labels','connMatrices');
%     end
% end
connMatrices=struct;
dagDir=createDagDirName(pars);

for cond=batchParams_anes.cond
    cond=cond{1};
    if pars.run.postHoc.conn.skipCond.bool
        if strcmp(cond,pars.run.postHoc.conn.skipCond.name)
            continue
        end
    elseif pars.run.postHoc.conn.oneCond.bool
        if ~strcmp(cond,pars.run.postHoc.conn.oneCond.name)
            continue
        end
    end
    chtc_out_dir=[pars.dirs.baseResultsDir filesep 'chtc_complete' filesep pars.dataType.globalDataCat filesep ...
        pars.dataType.varSelect filesep pars.dataType.dataID filesep ...
        'fitted_models' filesep pars.dataType.condition filesep dagDir];
    finalModelFile=dir([chtc_out_dir filesep 'finalSparseModel*.mat']);
    if isempty(finalModelFile)
        keyboard % output doesn't exist or dagDirName needs to be updated to reflect latest dag version
    end
    finalModelFile=finalModelFile(1).name;
    
    %% load final model
    load([chtc_out_dir filesep finalModelFile])

    %% get ROIs used for model
    % set dir containing epochData
    epochDir=[pars.dirs.dimRedData pars.dataType.dataID filesep pars.dataType.condition filesep];
    epochFile=getEpochFilename(pars); 
    epochFile=dir([epochDir filesep epochFile]);
    if length(epochFile) ~=1
        keyboard
    else
        load([epochDir filesep epochFile(1).name])
    end
    
    %% measure connectivity between ROIs
    connMatrices.(cond)=meas_conn(pars,dataInfo,Ahat_lagBlks);

    
end