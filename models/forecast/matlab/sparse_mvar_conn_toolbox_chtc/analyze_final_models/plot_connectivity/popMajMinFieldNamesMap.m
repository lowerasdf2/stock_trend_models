function [LR_rois,majMinFieldNamesMap]=popMajMinFieldNamesMap(pars,varInfo)
totalChs=0;
majMinFieldNamesMap=containers.Map; % 9wayGlobalRoi-specRoi
LR_rois={};% store which ROI contain both L & R channels
disp('WARNING: No longer tracking LR_rois in popMajMinFieldNamesMap... consider removing this var?')
if isfield(varInfo, 'newROI') 

% % %     if pars.run.postHoc.conn.useAnatSubdivs
% % %     varCats={varInfo.varCat};
% % %     varCats(find(strcmp(varCats,'HG')))={'HGPM'};
% % %     else
% % %     varCats={varInfo.oldROI};
% % %     end
    varCats={varInfo.varCat};
    %% MFG has two global vars, but PCA did not notice this. collapse all MFG to PFC
    rmMFGfromOtherBecuzPCA=find(strcmp(varCats,'R_MFG') & strcmp({varInfo.newROI},'Other') );
    for i=rmMFGfromOtherBecuzPCA
        varInfo(i).newROI='PFC';
    end
    rmMFGfromOtherBecuzPCA=find(strcmp(varCats,'L_MFG') & strcmp({varInfo.newROI},'Other') );
    for i=rmMFGfromOtherBecuzPCA
        varInfo(i).newROI='PFC';
    end
    %% SFG has two global vars--other and PFC. Collapse all to PFC.
    rmMFGfromOtherBecuzPCA=find(strcmp(varCats,'R_SFG') & strcmp({varInfo.newROI},'Other') );
    for i=rmMFGfromOtherBecuzPCA
        varInfo(i).newROI='PFC';
    end
    rmMFGfromOtherBecuzPCA=find(strcmp(varCats,'L_SFG') & strcmp({varInfo.newROI},'Other') );
    for i=rmMFGfromOtherBecuzPCA
        varInfo(i).newROI='PFC';
    end
    %%
    majorRois={varInfo.newROI};
    uniqueMajors=unique(majorRois);

% % %     stgInds=find(strcmp(varCat,'L_STG'));
% % %     for i=stgInds
% % %         varCat{i}='L_STGP';
% % %     end
% % %     stgInds=find(strcmp(varCat,'R_STG'));
% % %     for i=stgInds
% % %         varCat{i}='R_STGP';
% % %     end
% % %     if sum(strcmp(oldRoiNames,'STGP')) == 0 && sum(strcmp(oldRoiNames,'STGM')) == 0 && sum(strcmp(oldRoiNames,'STGA')) == 0 
% % % %         keyboard % this might be problematic. I think 376's spreadsheet does not append STG rows in the oldROI column with -P/A/M s
% % %         stgInds=find(strcmp(oldRoiNames,'STG'));
% % %         for i=stgInds
% % %             oldRoiNames{i}='STGP';
% % %         end
% % %     end
    % Get channel numbers corresponding to roi_labels
    for iMaj=1:length(uniqueMajors)
        if sum(strcmp(pars.run.postHoc.conn.finalROIlist,uniqueMajors{iMaj}))==1
            majorRoi=uniqueMajors{iMaj};
            majorIndex=contains(majorRois,majorRoi);
            minorRois=unique(varCats(majorIndex));
% % %             minorRoi=unique(varCats(majorIndex));
% % %             if length(minorRoi) ~= length(minorRoi)
% % %                 % find out which ROI contains both L & R channels
% % %                 for iRoi=1:length(minorRoi)
% % %                     sumRoi=sum(contains(minorRoi,minorRoi{iRoi}));
% % %                     if sumRoi > 1
% % %                         if sumRoi ~= 2
% % %                             keyboard % wtf
% % %                         else
% % %                             LR_rois{length(LR_rois)+1}=minorRoi{iRoi};
% % %                         end
% % %                     end
% % %                 end
% % %             end
%             % Just focus on subdivided MTG:            
% % %             curSize=length(minorRoi);
%             addOldROI=addOldROI(~strcmp(addOldROI,'MTG'));
%             addOldROI_LR=addOldROI_LR(~strcmp(addOldROI_LR,'L_MTG'));
%             addOldROI_LR=addOldROI_LR(~strcmp(addOldROI_LR,'R_MTG'));
% % %             if curSize ~= length(minorRoi)
% % %                 keyboard
% % %             end
            for iMin=1:length(minorRois)
                if sum(contains(LR_rois,minorRois{iMin})) > 0
                    keyboard
                    for side={'L','R'}
                        oldRoiName=[side{1} '_' minorRois{iMin}];
                        minorRoiLog=contains(varCats,oldRoiName) ...
                            & contains(majorRois,majorRoi);
                        nVar=sum(minorRoiLog);
                        if nVar == 0
                            keyboard % wtf
                        end
                        oldRoiName=[side{1} minorRois{iMin}];
                        disp([pars.dataType.dataID ': ' majorRoi '_' oldRoiName ' has ' num2str(nVar) ' chs']);
                        valToStore=[side{1} '_' minorRois{iMin}];
                        majMinFieldNamesMap([majorRoi '_' oldRoiName])=valToStore;
                    end
                else
                    minorRoiLog=contains(varCats,minorRois{iMin}) ...
                        & contains(majorRois,majorRoi);
                    nVar=sum(minorRoiLog);
                    totalChs=totalChs+nVar;
                    if nVar == 0
                        keyboard % wtf
                    end
                    disp([pars.dataType.dataID ': ' majorRoi '_' minorRois{iMin} ' has ' num2str(nVar) ' chs']);
                    try
                        valToStore=minorRois{contains(minorRois,minorRois{iMin})};
                    catch why
                        keyboard
                    end
                    try
                        majMinFieldNamesMap([majorRoi '_' minorRois{iMin}])=valToStore;
                    catch why
                        keyboard
                    end
                end
            end
        end
    end
else
    disp('oh shit')
    keyboard 
end
if totalChs ~= length(varCats)
    keyboard
end