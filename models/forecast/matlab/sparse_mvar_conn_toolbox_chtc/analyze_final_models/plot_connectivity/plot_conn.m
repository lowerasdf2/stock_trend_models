function plot_conn()
%% Check optimized model meets VAR model assumptions, produces relatively low 1-step error, and is not fully connected
% keyboard % add test for serial correlation
% keyboard % check for coefficient matrix stability??? is_stbl()
% keyboard % calculate error of model
% keyboard % check how sparse model is
% close all

%% set color options
if strcmp(pars.dataType.varSelect,'AUDITORY')
    colorVals=[1,0,0; 1,0,.5; ...
        0,.5,0; 0,.5,.5; ...
        0,0,1];
else
    if length(pars.run.postHoc.conn.finalROIlist)==9
        colorVals=[1,0,0; 1,0,0.3; 1,0,0.6; 1,0,0.9; 0.6,0.6,0; 0,1,0; 0,0,1; 0,1,1; 0.7,0.7,0.7];
    else
        keyboard % add more colors
    end
end



%% Assign channels to ROIs
majMinFieldNamesMap=containers.Map;
ECoGchannels_anes=batchParams_anes.ECoGchannels;

%% Clean up mistake (used anatomical subdivisions on older models)
if pars.run.postHoc.conn.useAnatSubdivs
    %% If using older models (which grabbed anat regions during PCA process), need to make sure there's a consistent 9way-newROI that belongs to MFG; 
    % patient372L's MFG was split up based on newROIs into both MFGpf and PMC;
    % this means that its 9-way categorization belongs to both PFC (MFGpf) and Other
    % (PMC)
    oldRoiNames={ECoGchannels_anes.AnatReg};
    newRoiNames={ECoGchannels_anes.newROI};
    mfgInds=contains(oldRoiNames,'MFG');keyboard % replace contains with strcmp?
    newROIsForMFG=newRoiNames(mfgInds);
    % assign final new ROI based on whichever one is most common
    uniqueNewRoisForMFG=unique(newROIsForMFG);
    finalRoi='';
    highCt=0;
    for iMFGroi=1:length(uniqueNewRoisForMFG)
        roiCt=sum(contains(newROIsForMFG,uniqueNewRoisForMFG{iMFGroi}));keyboard % replace contains with strcmp?
        if roiCt > highCt
            highCt=roiCt;
            finalRoi=uniqueNewRoisForMFG{iMFGroi};
        end
    end
    mfgInds=find(mfgInds);
    for mfgInd=mfgInds
        ECoGchannels_anes(mfgInd).newROI=finalRoi;
    end
    %% Replace STS with non-subdivisions in the original ROI labels assigned to channel matrix modeled
    rois=dataInfo.label;
    ORIG_N_ROIS=length(unique(rois));
    stsInds=find(contains(rois,'STS'));keyboard % replace contains with strcmp?
    if ~isempty(find(abs(diff(stsInds))>1))
        keyboard % means that there are 2/more subdivisions of STS... which means we may want to consider leaving the subdivision
    end
    for iRoi=stsInds
        if contains(rois{iRoi},'STS')&& length(rois{iRoi}) > 5 ;keyboard % replace contains with strcmp?
            rois{iRoi}=rois{iRoi}(1:5);
        end 
    end
    dataInfo.label=rois;
    %% Also update ECoGchannels_anes anat column s.t. STS no longer has subdivisions listed
    stsInds=find(contains({ECoGchannels_anes.oldROI},'STS')); keyboard % replace contains with strcmp?
    for stsInd=stsInds
        ECoGchannels_anes(stsInd).AnatReg='STS';
    end
end
%% Replace STG with subdivisions
if pars.run.postHoc.conn.useAnatSubdivs
    oldRoiNames={ECoGchannels_anes.AnatReg};
else
    oldRoiNames={ECoGchannels_anes.oldROI};
end
if sum(strcmp(oldRoiNames,'STGP')) == 0 && sum(strcmp(oldRoiNames,'STGM')) == 0 && sum(strcmp(oldRoiNames,'STGA')) == 0 
    if pars.run.postHoc.conn.useAnatSubdivs
        keyboard
    end
    anatROINames={ECoGchannels_anes.AnatReg};
    if strcmp(pars.dataType.dataID,'patient376R') || strcmp(pars.dataType.dataID,'patient369LR') 
        stgInds=find(contains(oldRoiNames,'STG'));keyboard % replace contains with strcmp?
        if isempty(stgInds)
            keyboard
        else
            for stgInd=1:length(stgInds)
                ECoGchannels_anes(stgInds(stgInd)).oldROI=anatROINames{stgInds(stgInd)};
            end
        end
        oldRoiNames={ECoGchannels_anes.oldROI};
        if sum(strcmp(oldRoiNames,'STGP')) == 0 && sum(strcmp(oldRoiNames,'STGM')) == 0 && sum(strcmp(oldRoiNames,'STGA')) == 0 
            keyboard % wtf, fix didn't work?
        end
    else
        keyboard % I think this only occurs for 376R and 369LR
    end
end

%% reduce batchParams to PCA representation & create map of weight matrix denoting order of ROIs
roiOrderMap=containers.Map;
% first create field for side_roi
if pars.run.postHoc.conn.useAnatSubdivs
    oldRoiNames={ECoGchannels_anes.AnatReg};
else
    oldRoiNames={ECoGchannels_anes.oldROI};
end
lOrR={ECoGchannels_anes.side};
sidedOldROINames=cellfun(@(x,y) [x '_' y],lOrR,oldRoiNames,'un',0);
[ECoGchannels_anes(:).sidedOldROINames]=sidedOldROINames{:};
pcaRois=unique(dataInfo.label);
for pcaRoiInd=1:length(pcaRois)
    pcaRoi=pcaRois{pcaRoiInd};
    xlsxName=pcaRoi;% keep L or R attached since some patients have ROIs on L and R side (372L has L_Grep and R_Grep
    pcaRoiInds=contains(dataInfo.label,pcaRoi);keyboard % replace contains with strcmp?
    roiLogicalInds=zeros(length(pcaRois),1);
    roiLogicalInds(pcaRoiInd)=1;
    roiOrderMap(pcaRoi)=logical(roiLogicalInds);
    if ~isempty(find(abs(diff(pcaRoiInds)) ~= 1 & abs(diff(pcaRoiInds)) ~= 0,1))
        keyboard % checks that pca ROIs are listed in order
    end
    % remove extra entries in batchParams representing original number
    % channels
    sidedOldROINames={ECoGchannels_anes.sidedOldROINames};
    origRoiInds=find(contains(sidedOldROINames,xlsxName));keyboard % replace contains with strcmp?
    if isempty(origRoiInds)
        keyboard
    end
    delRoiInds=origRoiInds(sum(pcaRoiInds)+1:end);
    ECoGchannels_anes(delRoiInds)=[];
end   
%% arrange rows of ECoGchannels_anes to match organization of
% dataInfo.label s.t. channel indices are properly marked to match
% dataInfo.label later on (which controls initial organization of final
% connectivity weights between channels/Rois) before we sort it to
% match hierarchical representation
ECoGchannels_anes_ordered=ECoGchannels_anes;
ECoGchannels_anes_ordered(:)=[];
for pcaRoiInd=1:length(pcaRois)
    pcaRoi=pcaRois{pcaRoiInd};
    xlsxName=pcaRoi; %(3:end);% (using side info now since ROIs can repeat, but have different sides)
    pcaRoiInds=contains(dataInfo.label,pcaRoi);keyboard % replace contains with strcmp?
    curInds=contains({ECoGchannels_anes.sidedOldROINames},xlsxName);keyboard % replace contains with strcmp?
    if sum(curInds) == 0 || sum(pcaRoiInds) ~= sum(curInds)
        keyboard% wtf
    end
    ECoGchannels_anes_ordered(find(pcaRoiInds))=ECoGchannels_anes(find(curInds));
end
ECoGchannels_anes=ECoGchannels_anes_ordered;
clear ECoGchannels_anes_ordered


LR_rois={};% store which ROI contain both L & R channels
if isfield(ECoGchannels_anes, 'newROI') % should new ROI be changed to the field of ClusterGroup???????
    newROINames={ECoGchannels_anes.newROI};
    uniqueNewROIs=unique(newROINames);
    if pars.run.postHoc.conn.useAnatSubdivs
        oldRoiNames={ECoGchannels_anes.AnatReg};
    else
        oldRoiNames={ECoGchannels_anes.oldROI};
    end
    oldRoiNames_LR={ECoGchannels_anes.sidedOldROINames};
    if sum(strcmp(oldRoiNames,'STGP')) == 0 && sum(strcmp(oldRoiNames,'STGM')) == 0 && sum(strcmp(oldRoiNames,'STGA')) == 0 
        keyboard % this might be problematic. I think 376's spreadsheet does not append STG rows in the oldROI column with -P/A/M s
    end
    % Get channel numbers corresponding to ROIs
    for iNewROI=1:length(uniqueNewROIs)
        if sum(contains(pars.run.postHoc.conn.finalROIlist,uniqueNewROIs{iNewROI}))==1 ;keyboard % replace contains with strcmp?
            thisNewROI=uniqueNewROIs{iNewROI};
            newROIindex=contains(newROINames,thisNewROI);keyboard % replace contains with strcmp?
            addOldROI=unique(oldRoiNames(newROIindex));
            addOldROI_LR=unique(oldRoiNames_LR(newROIindex));
            if length(addOldROI) ~= length(addOldROI_LR)
                % find out which ROI contains both L & R channels
                for iRoi=1:length(addOldROI)
                    sumRoi=sum(contains(addOldROI_LR,addOldROI{iRoi}));keyboard % replace contains with strcmp?
                    if sumRoi > 1
                        if sumRoi ~= 2
                            keyboard % wtf
                        else
                            LR_rois{length(LR_rois)+1}=addOldROI{iRoi};
                        end
                    end
                end
            end
%             % Just focus on subdivided MTG:            
            curSize=length(addOldROI);
%             addOldROI=addOldROI(~strcmp(addOldROI,'MTG'));
%             addOldROI_LR=addOldROI_LR(~strcmp(addOldROI_LR,'L_MTG'));
%             addOldROI_LR=addOldROI_LR(~strcmp(addOldROI_LR,'R_MTG'));
            if curSize ~= length(addOldROI)
                keyboard
            end
            for iOldROI=1:length(addOldROI)
                if sum(contains(LR_rois,addOldROI{iOldROI})) > 0 ;keyboard % replace contains with strcmp?
                    for side={'L','R'}
                        oldRoiName=[side{1} '_' addOldROI{iOldROI}];
                        oldROILogical=contains(oldRoiNames_LR,oldRoiName) ...
                            & contains(newROINames,thisNewROI); keyboard % replace contains with strcmp?
                        nVar=sum(oldROILogical);
                        if nVar == 0
                            keyboard % wtf
                        end
                        oldRoiName=[side{1} addOldROI{iOldROI}];
                        disp([pars.dataType.dataID ': ' thisNewROI '_' oldRoiName ' has ' num2str(nVar) ' chs']);
                        valToStore=[side{1} '_' addOldROI{iOldROI}];
                        majMinFieldNamesMap([thisNewROI '_' oldRoiName])=valToStore;
                    end
                else
                    oldROILogical=contains(oldRoiNames,addOldROI{iOldROI}) ...
                        & contains(newROINames,thisNewROI); keyboard % replace contains with strcmp?
                    nVar=sum(oldROILogical);
                    if nVar == 0
                        keyboard % wtf
                    end
                    disp([pars.dataType.dataID ': ' thisNewROI '_' addOldROI{iOldROI} ' has ' num2str(nVar) ' chs']);
                    valToStore=addOldROI_LR{contains(addOldROI_LR,addOldROI{iOldROI})}; keyboard % replace contains with strcmp?
                    majMinFieldNamesMap([thisNewROI '_' addOldROI{iOldROI}])=valToStore;
                end

            end
        end
    end
else
    disp('oh shit')
    keyboard 
end

%% check that sleep and anesth. data match
% % % ROIs_sleep=fieldnames(chanLogical_sleep);
% % % ROIs_anes=fieldnames(chanLogical_anes);
% % % % Check for error
% % % if length(ROIs_sleep) ~= length(ROIs_anes) || ...
% % %         length(intersect(ROIs_sleep,ROIs_anes))~= length(ROIs_anes)
% % %     error('Mismatch in ROIs_sleep and ROIs_anes');
% % % end

%%
% Sort ROIs and check that ROIs are in both sleep and anesthesia data
% Compare ROIs_sleep with ROI list from ROI_tableRead. Note that to exclude
% ROIs from the analysis, just need to omit from the list in ROI_tableRead.
electrodeFilePath=[pars.dirs.electrodeSpreadsheet filesep 'Electrode MNI coordinates and ROI' filesep];
electrodeFile='LGD electrodes for analysis - sorted by ROI.xlsx';
electrodeSheet='ROI_table';
[ROI_table]=ROI_tableRead(electrodeFilePath,electrodeFile,electrodeSheet);
if pars.run.postHoc.conn.useAnatSubdivs
    % these regions were not subdivided based on anatomy
    ROI_table(strcmp(ROI_table.oldROI,'MTGP'),:)=[];
    ROI_table(strcmp(ROI_table.oldROI,'MTGM'),:)=[];
    ROI_table.oldROI(strcmp(ROI_table.oldROI,'MTGA'))={'MTG'};
    ROI_table.oldROI(strcmp(ROI_table.oldROI,'MFGpf'))={'MFG'};
    oldRoiNum=ROI_table.oldROINum;
    roiIndJump=find(diff(oldRoiNum)>1);
    if length(roiIndJump) > 1
        keyboard
    else
        ROI_table.oldROINum(roiIndJump+1:end)=ROI_table.oldROINum(roiIndJump+1:end)-2;
    end
end
ROIsInTable={};
ROIs_anes=keys(majMinFieldNamesMap);
if length(ROIs_anes) ~= ORIG_N_ROIS
    keyboard % wtf
end
roiCt=1;
sumLRreps=0;
for iROI=1:size(ROI_table,1)
    % check if ROI has L/R repeat
    lr_rep=false;
    for repRoi=1:length(LR_rois)
        if strcmp(ROI_table.oldROI{iROI},LR_rois{repRoi})
            lr_rep=true;
            sumLRreps=sumLRreps+1;
        end
    end
    if ~lr_rep
        newField=[ROI_table.newROI{iROI} '_' ROI_table.oldROI{iROI}];
        if sum(contains(ROIs_anes,newField)) > 0 keyboard % replace contains with strcmp?
            ROIsInTable{roiCt}=newField;
            roiCt=roiCt+1;
        end
    else
        for side={'L','R'}
            newField=[ROI_table.newROI{iROI} '_' side{1} ROI_table.oldROI{iROI}];
            if sum(contains(ROIs_anes,newField)) > 0 keyboard % replace contains with strcmp?
                ROIsInTable{roiCt}=newField;
                roiCt=roiCt+1;
            end
        end
    end
end
if sumLRreps ~= length(LR_rois)
    keyboard % wtf
end
%% check to see if ROIs_anes contains any fieldnames not present in ROI_table (a few areas purposely excluded: Other_SubInn, others)
allFound=true;
for roiInd=1:length(ROIs_anes)
    roiToFind=ROIs_anes{roiInd};
    foundRoi=false;
    for roiInd2=1:length(ROIsInTable)
        newField=ROIsInTable{roiInd2};
        if strcmp(roiToFind,newField)
            foundRoi=true;
        end
    end
    if ~foundRoi
        allFound=false;
        disp(['ROI excluded from final excel table, but included in model: ' roiToFind])
    end
end
if (length(ROIsInTable) > length(ROIs_anes)) && strcmp(pars.dataType.varSelect,'ALL')
    keyboard % ROIsInTable should never be longer than ROIs_anes unless processing auditory data
end
if ~allFound
    ROIsToProcess=intersect(ROIs_anes,ROIsInTable);
elseif length(ROIs_anes) ~= length(ROIsInTable)
    keyboard
else 
    ROIsToProcess=ROIs_anes;
end

%% order ROIs by hierarchy based on excel table
nROIs=length(ROIsToProcess);
sortIndex=NaN(1,nROIs);
for iROI=1:nROIs
    sortIndex(iROI)=ROI_table.oldROINum(contains(ROIsToProcess,ROIsInTable{iROI})); keyboard % replace contains with strcmp?
end
ROIsToProcess=ROIsToProcess(sortIndex);

%% Now do plotting
condsToPlot=batchParams_anes.cond; % condsToPlot replaced statesToPlot
maxVals=ones(length(FreqBands.Names),1)*-1;  

checkPcaStuffFig=figure;
pcaStuffBool=true;
for iBand=1:length(FreqBands.Names)
    thisBand=FreqBands.Names{iBand};
    if pars.analyzeConn.plotChordConn
        chordFigName=[pars.dataType.dataID ' - ConnChordPlot - ' thisBand];
        imageSz=[0.5000, 0.0370, 0.5000, 0.9630]; %[0 0 1 1] for full screen
        chordFig=figure('Name',chordFigName,'units','normalized','outerposition',imageSz);
        
        weightsFigName=[pars.dataType.dataID ' - weightsFig - ' thisBand];
        weightsFig=figure('Name',weightsFigName,'units','normalized','outerposition',imageSz);
    end
    %% find reasonable maxVal for this patient's power spectrum
    measTypes={'bPDC'};
    for measTypeInd=1:length(measTypes)
        measType=measTypes{measTypeInd};
        allWeights=[];
        maxVal=0;
        for iCond=1:length(condsToPlot)
            cond=batchParams_anes.cond{iCond};
            if pars.analyzeConn.skipCond.bool && strcmp(cond,pars.analyzeConn.skipCond.name)
                continue
            elseif pars.analyzeConn.oneCond.bool && ~strcmp(cond,pars.analyzeConn.oneCond.name)
                continue
            end
            weights=squeeze(connMatrices.(cond).(measType)(iBand,:,:));
            if sum(isnan(weights(:))) > 0
                keyboard
            end
            allWeights=vertcat(allWeights,weights(:));
            weights_noSC=weights;
            for iROI=1:length(weights)
                weights(iROI,iROI)=0;
            end
            maxVal=max(weights(:));
            if maxVals(iBand) < maxVal
                maxVals(iBand)=maxVal;
            end
        end
        for iCond=1:length(condsToPlot)
            cond=batchParams_anes.cond{iCond};
            %% estimate connectivity across ROIs
            
            %% add code here to save out results
%             finalModelDir=[pars.dirs.baseResultsDir filesep 'chtc_complete' filesep pars.dataType.globalDataCat filesep pars.dataType.varSelect filesep pars.dataType.dataID filesep dagDir];
%             if ~exist(finalModelDir,'dir')
%                 error('Completed model''s dir does not exist. Might need to update legacy name/dir or change pars to run data that actually has finalized models.')
%             end
%             connMeasDir=[finalModelDir filesep 'connMeas'];
%             if ~exist(ppDir,'dir')
%                 mkdir(ppDir)
%             end
%             outFile=[ppDir filesep 'connData.mat'];
%             if ~exist(outFile,'file')
%                 [dataInfo,orig_labels,Mv,roiIndsMap,connMatrices]=calcROIconnMat(pars,dagDir,batchParams_anes);
%                 save(outFile,'dataInfo','orig_labels','connMatrices');
%             else
%                 load(outFile) 
%                 if length(fields(connMatrices))~=length(batchParams_anes.cond)
%                     [dataInfo,orig_labels,Mv,roiIndsMap,connMatrices]=calcROIconnMat(pars,dagDir,batchParams_anes);
%                     save(outFile,'dataInfo','orig_labels','connMatrices');
%                 end
%             end
            
            if pars.analyzeConn.skipCond.bool && strcmp(cond,pars.analyzeConn.skipCond.name)
                continue
            elseif pars.analyzeConn.oneCond.bool && ~strcmp(cond,pars.analyzeConn.oneCond.name)
                continue
            end
            %% standardize format of weights matrix (order ROIs)
            newWeights=nan(nROIs,nROIs);
            for iROI=1:nROIs
                thisROI=ROIsToProcess{iROI};
                for jROI=1:nROIs
                    thatROI=ROIsToProcess{jROI};
                    thisLogical=roiOrderMap(majMinFieldNamesMap(thisROI));
                    thatLogical=roiOrderMap(majMinFieldNamesMap(thatROI));
                    if length(thisLogical) ~= size(connMatrices.(cond).(measType)(iBand,:,:),2)
                        keyboard
                    elseif length(thatLogical) ~= size(connMatrices.(cond).(measType)(iBand,:,:),2)
                        keyboard
                    end
                    try
                        newWeights(iROI,jROI)=connMatrices.(cond).(measType)(iBand,thisLogical,thatLogical);%tempMn=wPLIMn.(thisName).(thisBand).(thisState)(thisLogical,thatLogical);
                    catch why
                        keyboard
                    end
                end
            end
            newConnMatrices.(cond).(measType)(iBand,:,:)=newWeights;
            labels=[];
            myColorMap=containers.Map;
            numericColorMap=nan(nROIs,3);
            for iROI=1:length(ROIsToProcess)
                thisROI=ROIsToProcess{iROI};
                labels{iROI}=thisROI(strfind(thisROI,'_')+1:end);
                myColorMap(labels{iROI})=colorVals(contains(pars.run.postHoc.conn.finalROIlist,thisROI(1:strfind(thisROI,'_')-1)),:); keyboard % replace contains with strcmp?
                numericColorMap(iROI,:)=colorVals(contains(pars.run.postHoc.conn.finalROIlist,thisROI(1:strfind(thisROI,'_')-1)),:); keyboard % replace contains with strcmp?
            end
            weights=squeeze(newConnMatrices.(cond).(measType)(iBand,:,:));
            if size(weights,1) ~= length(labels)
                keyboard
            end
            figure(chordFig);
            nCols=length(measTypes);
            plotCol=measTypeInd;%iBand
            nRows=length(condsToPlot);
            plotRow=iCond;
            plotInd=(nCols*plotRow)-(nCols-plotCol);
            subplot(nRows,nCols,plotInd);
            
            %% 1) get rid of self-connections for now
            for iRoi=1:size(weights,1)
                weights(iRoi,iRoi)=0;
            end
            figure(weightsFig)
            subplot(nRows,nCols,plotInd);
            imagesc(weights)
            
            figure(checkPcaStuffFig)
            plotInd=(2*iBand)-(2-2);
            subtightplot(length(FreqBands.Names),2,plotInd)
            imagesc(weights,[0,1])

            %% 2) establish weight thresh
            weightsVec=weights(:);
            weightsVec=weightsVec(weightsVec~=0);
            THRESH=mean(weightsVec)+std(weightsVec);
            weights(weights<THRESH)=0;
            if iBand==1 && measTypeInd==1 && iCond==1
                disp('WARNING! Transposing connectivity matrix to reflext from-to cnnections in correct/intuitive manner')
            end
            weights=weights';
            %% myCircularGraph_copy
            figure(chordFig);
%             myCircularGraph_withSelfConn(weights,...
%                 'Colormap',numericColorMap,...
%                 'Label',labels,...
%                 'MaxVal',maxVals(iBand));
            %% myCircularGraph2
            figure(checkPcaStuffFig)
            plotInd=(2*iBand)-(2-1);
            subtightplot(length(FreqBands.Names),2,plotInd)
            weights=myCircularGraph2(THRESH,weights,labels,myColorMap,.5);%maxVals(iBand));
            if ~pcaStuffBool
                ax=gca;
                if iCond == 1
                    t=text(mean(ax.XLim),ax.YLim(2)*1.1,[measType '-' cond]);
                else
                    t=text(mean(ax.XLim),ax.YLim(2)*1.1,cond);
                end
                t.FontSize=16;
                t.FontWeight='bold';
                t.HorizontalAlignment='center';
            end
        end
        if ~pars.analyzeConn.skipCond.bool && ~pars.analyzeConn.oneCond.bool
            outPath=[pars.dirs.baseResultsDir filesep 'finalNetworks' filesep];
    %         set(gcf, 'Position',  [100, 50, 360, 950])
            saveas(chordFig,[outPath measType '_' chordFigName '.png']);
            savefig(chordFig,[outPath measType '_' chordFigName '.fig']);
            close(chordFig)
            saveas(weightsFig,[outPath measType '_' weightsFigName '.png']);
            savefig(weightsFig,[outPath measType '_' weightsFigName '.fig']);
            close(weightsFig)
        end
    end
end%Loop over FreqBands.Names
if pcaStuffBool
    keyboard % fix below hardcoded dirs
    figure(checkPcaStuffFig)
    if pars.cv.genDimRed.ch.PCA.run
        suptitle(['pca' num2str(pars.cv.genDimRed.ch.PCA.perc*100)])
        saveas(checkPcaStuffFig,['M:\PassiveEphys\Analysis Results\Chris\ecog_connectivity\PCA\backProjectionErrCheck\pca' num2str(pars.cv.genDimRed.ch.PCA.perc*100) '.png'])
        savefig(checkPcaStuffFig,['M:\PassiveEphys\Analysis Results\Chris\ecog_connectivity\PCA\backProjectionErrCheck\pca' num2str(pars.cv.genDimRed.ch.PCA.perc*100) '.fig'])
    else
        suptitle(['noPca'])
        saveas(checkPcaStuffFig,['M:\PassiveEphys\Analysis Results\Chris\ecog_connectivity\PCA\backProjectionErrCheck\noPca.png'])
        savefig(checkPcaStuffFig,['M:\PassiveEphys\Analysis Results\Chris\ecog_connectivity\PCA\backProjectionErrCheck\noPca.fig'])
    end
end
% end%Loop over measureTypes
