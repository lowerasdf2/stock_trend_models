function compareWPLI_AnesVsSleep_dbt()
%% General parameters
computer = 'Mac';
% computer = 'desktopPC';

anesSetName = 'splitCond';
sleepSetName = 'sleep_selected_hierarchySet';

if strcmp(computer,'Mac')
    computerSpecPrefix = '/Users/bankslaptop/Box Sync/PHI_Data-A5309-Banks/Banks-UIowa-LDS/My documents/';
else
    computerSpecPrefix = 'D:\Box Sync\PHI_Data-A5309-Banks\Banks-UIowa-LDS\My documents\';
end

if ~exist('defaultPath','var')
    dataPath = [computerSpecPrefix 'Data and analysis' filesep 'ECoG data' filesep];
end
if ~exist('electrodeFilePath','var')
    electrodeFilePath = [computerSpecPrefix 'Data and analysis' filesep ...
        'ECoG data' filesep 'Electrode MNI coordinates and ROI' filesep];
end
electrodeFile = 'LGD electrodes for analysis - sorted by ROI.xlsx';
electrodeSheet = 'ROI_table';
if ~exist('outPath','var')
    outPath = [computerSpecPrefix 'Data and analysis' filesep 'ECoG data' filesep];
end

% Ensure output directory exists
if ~exist(outPath, 'dir')
    mkdir(outPath);
end
disp(['Data will be saved to `' outPath '`']);

onlyAudHierarchy = 0;
pctToDiscard = 0; %Keep 100 - pctDiscard% of conns when thresholding
binarize = 0;
stateToCluster = 'W';

plotChanByChanAdjMat = 0;
plotClusteredAdjMat = 0;
plotNorms = 1;
plotNormMatrix = 0;
plotComparisons = 1;
plotChordConn = 1;
plotROIByROIAdjMat = 1;

Scale=[3 1 0.2];
nineROIs = {'HGPM','HGAL','PT','PP','STG','Aud-rel','PFC','SensMot','Other'};

sleepStages = {'W','N1','N2','N3','N4','R'};
anesStates = {'wake','sed','LOC'};
compWithin_anes = {'wake','sed'; 'wake','LOC'; 'sed','LOC'};
compWithin_sleep = {'W','N1'; 'W','N2'; 'N1','N2'};
compAcross = {'wake','W'; 'sed','N1'; 'LOC','N2'};
compShuff = {'wake','N1'; 'wake','N2';'sed','W'; 'sed','N2';'LOC','W'; 'LOC','N1';};

maxVals = [0.25,0.25,0.35,0.25,0.19,0.16]; %For chord conn plots - band-specific

%% Subject by subject ROI-based analysis 

newROIList = {'HGPM','HGAL','PT','PP','STG','AudRel','PFC','SensMot','Other'};
colorVals = [1,0,0; 1,0,0.3; 1,0,0.6; 1,0,0.9; 0.6,0.6,0; 0,1,0; 0,0,1; 0,1,1; 0.7,0.7,0.7];
patientsToUse = {'patient372L','patient376R','patient403L','patient409L','patient423L'};
stagesToPlot = {'W','N1','N2','N3','R'};
statesToPlot = {'wake','sed','LOC'};

%Load anesthesia data
load([outPath 'ECoG_conn_' anesSetName '.mat'],'ECoG_conn','batchParams');
ECoG_conn_anes = ECoG_conn;
batchParams_anes = batchParams;
clear ECoG_conn
clear batchParams
% Get list of suspect channels
if exist([outPath 'ECoG_pow_dbt_' anesSetName '.mat'],'file')
    load([outPath 'ECoG_pow_dbt_' anesSetName '.mat'],'ECoG_pow');
    zCriterion = 3.5;
    makePlots = 0;
    [~,indexInfo_anes] = channelCheck(ECoG_pow,batchParams_anes,statesToPlot,zCriterion,makePlots,outPath);
    clear ECoG_pow
end

%Load sleep data
load([outPath 'ECoG_conn_' sleepSetName '.mat'],'ECoG_conn','batchParams');
ECoG_conn_sleep = ECoG_conn;
batchParams_sleep = batchParams;
clear ECoG_conn
clear batchParams
% Get list of suspect channels
if exist([outPath 'ECoG_pow_dbt_' sleepSetName '.mat'],'file')
    load([outPath 'ECoG_pow_dbt_' sleepSetName '.mat'],'ECoG_pow');
    zCriterion = 3.5;
    makePlots = 0;
    [~,indexInfo_sleep] = channelCheck(ECoG_pow,batchParams_sleep,stagesToPlot,zCriterion,makePlots,outPath);
    clear ECoG_pow
end

patientIDs_sleep = fieldnames(ECoG_conn_sleep.wPLI_dbt);
patientIDs_anes = fieldnames(ECoG_conn_anes.wPLI_dbt);
patientIDs = intersect(patientIDs_sleep,patientIDs_anes);

% Assemble full wPLI matrices (will exclude channels below)
wPLIMn = struct();
wPLISEM = struct();
for iPatient = 1:length(patientIDs)
    thisName = patientIDs{iPatient};
    %Sleep data
    stages = fieldnames(ECoG_conn_sleep.wPLI_dbt.(thisName));
    stageCount = 0;
    for iStage = 1:length(sleepStages)
        % Use the following if statement to plot stages in standard order
        if sum(contains(stages,sleepStages{iStage}))>0
            stageCount = stageCount+1;
            thisStage = sleepStages{iStage};
            segs = fieldnames(ECoG_conn_sleep.wPLI_dbt.(thisName).(thisStage));
            nSegs = length(segs);
            bands = fieldnames(ECoG_conn_sleep.wPLI_dbt.(thisName).(thisStage).seg_1);
            for iBand = 1:length(bands)
                thisBand = bands{iBand};
                for iSeg = 1:nSegs
                    thisSeg = segs{iSeg};
                    tempMat = ...
                        ECoG_conn_sleep.wPLI_dbt.(thisName).(thisStage).(thisSeg).(thisBand).wpli_debiasedspctrm;
                    tempMat(isnan(tempMat)) = 0;
                    % compute wPLI adj matrix as average over all segments
                    if iSeg == 1
                        wPLIMn.(thisName).(thisBand).(thisStage) = ...
                            zeros(size(tempMat));
                    end
                    wPLIMn.(thisName).(thisBand).(thisStage) = ...
                        wPLIMn.(thisName).(thisBand).(thisStage) + tempMat;
                    tempMat = ...
                        ECoG_conn_sleep.wPLI_dbt.(thisName).(thisStage).(thisSeg).(thisBand).wpli_debiasedspctrmsem;
                    % compute wPLI adj matrix as average over all segments
                    if iSeg == 1
                        wPLISEM.(thisName).(thisBand).(thisStage) = ...
                            zeros(size(tempMat));
                    end
                    wPLISEM.(thisName).(thisBand).(thisStage) = ...
                        wPLISEM.(thisName).(thisBand).(thisStage) + tempMat;
                end
                wPLIMn.(thisName).(thisBand).(thisStage) = wPLIMn.(thisName).(thisBand).(thisStage)/nSegs;
                wPLIMn.(thisName).(thisBand).(thisStage)(logical(eye(size(wPLIMn.(thisName).(thisBand).(thisStage))))) = 0;
                wPLISEM.(thisName).(thisBand).(thisStage) = wPLISEM.(thisName).(thisBand).(thisStage)/nSegs;
                wPLISEM.(thisName).(thisBand).(thisStage)(logical(eye(size(wPLISEM.(thisName).(thisBand).(thisStage))))) = 0;
            end
        end
    end
    % Anesthesia data
    conditions = batchParams_anes.(thisName).newCond;
    OAASVals = zeros(1,length(conditions));
    for iCond = 1:length(conditions)
        OAASVals(iCond) = getOAASVal(conditions{iCond});
    end
    condSet.(anesStates{1}) = [1,2]; %First two are always pre-drug in split cond
    sedIndx = find(OAASVals>=3);
    condSet.(anesStates{2}) = sedIndx(sedIndx>2); %>2 because first two are pre-drug
    condSet.(anesStates{3}) = find(OAASVals<3);
    for iState = 1:length(anesStates)
        thisState = anesStates{iState};
        for iBand = 1:length(FreqBands.Names)
            thisBand = FreqBands.Names{iBand};
            firstCond = true;
            for iCond = condSet.(thisState)
                thisCond = conditions{iCond};
                tempMat = ECoG_conn_anes.wPLI_dbt.(thisName).(thisCond).(thisBand).wpli_debiasedspctrm;
                if firstCond
                    firstCond = false;
                    wPLIMn.(thisName).(thisBand).(thisState) = zeros(size(tempMat));
                    wPLISEM.(thisName).(thisBand).(thisState) = zeros(size(tempMat));
                end
                wPLIMn.(thisName).(thisBand).(thisState) = wPLIMn.(thisName).(thisBand).(thisState) + tempMat;
                tempMat = ECoG_conn_anes.wPLI_dbt.(thisName).(thisCond).(thisBand).wpli_debiasedspctrmsem;
                wPLISEM.(thisName).(thisBand).(thisState) = wPLISEM.(thisName).(thisBand).(thisState) + tempMat;
            end
            wPLIMn.(thisName).(thisBand).(thisState) = ...
                wPLIMn.(thisName).(thisBand).(thisState)/length(condSet.(anesStates{iState}));
            wPLIMn.(thisName).(thisBand).(thisState)(logical(eye(size(wPLIMn.(thisName).(thisBand).(thisState))))) = 0;            
            wPLISEM.(thisName).(thisBand).(thisState) = ...
                wPLISEM.(thisName).(thisBand).(thisState)/length(condSet.(anesStates{iState}));
            wPLISEM.(thisName).(thisBand).(thisState)(logical(eye(size(wPLISEM.(thisName).(thisBand).(thisState))))) = 0;            
        end
    end
end

%Assign channels to ROIs
chanLogical_anes = struct();
chanLogical_sleep = struct();

for iPatient = 1:length(patientIDs)
    thisName = patientIDs{iPatient};
    params_anes = batchParams_anes.(thisName);
    params_sleep = batchParams_sleep.(thisName);
    % Grab suspect channel numbers
    % chanIndicesToSkip_sleep, _anes contain the concatenated indices of the 
    % channel numbers contained in indexInfo for this subject
    chanIndicesToSkip_anes = indexInfo_anes.(thisName).badChans; 
    chanIndicesToSkip_sleep = indexInfo_sleep.(thisName).badChans;
    ECoGchannels_anes.(thisName) = params_anes.ECoGchannels;
    ECoGchannels_sleep.(thisName) = params_sleep.ECoGchannels;
    chanNum_anes = [ECoGchannels_anes.(thisName).chanNum];
    if isfield(ECoGchannels_sleep.(thisName),'contactNum')
        chanNum_sleep = [ECoGchannels_sleep.(thisName).contactNum];
    else
        chanNum_sleep = [ECoGchannels_sleep.(thisName).chanNum];
    end
% [C,ia,ib] = interset(A,B); % C=A(ia) and C=B(ib)
    [~,indexAnes,indexSleep] = intersect(chanNum_anes,chanNum_sleep);
    
    % Now exclde from indexAnes and indexSleep any channels on the suspect
    % channel list.
    indexAnes = indexAnes(~ismember(indexAnes,chanIndicesToSkip_anes));
    indexSleep = indexSleep(~ismember(indexSleep,chanIndicesToSkip_sleep));
    % Must also exclude the anesthesia channels from sleep list and vice
    % versa
    % First find sleep channels corresponding to suspect anesthesia chans
    tempChanNum = chanNum_anes(chanIndicesToSkip_anes); % Actual channel numbers to skip
    tempIndex = find(ismember(chanNum_sleep,tempChanNum));
    indexSleep = indexSleep(~ismember(indexSleep,tempIndex));
    % Now find anesthesia channels corresponding to suspect sleep chans
    tempChanNum = chanNum_sleep(chanIndicesToSkip_sleep); % Actual channel numbers to skip
    tempIndex = find(ismember(chanNum_anes,tempChanNum));
    indexAnes = indexAnes(~ismember(indexAnes,tempIndex));
    
    ECoGchannels_anes.(thisName) = ECoGchannels_anes.(thisName)(indexAnes);
    ECoGchannels_sleep.(thisName) = ECoGchannels_sleep.(thisName)(indexSleep);
    if isfield(ECoGchannels_sleep.(thisName), 'newROI')
        newROINames = {ECoGchannels_sleep.(thisName).newROI};
        uniqueNewROIs = unique(newROINames);
        oldROINames = {ECoGchannels_sleep.(thisName).oldROI};
        % Get channel numbers corresponding to ROIs
        for iNewROI = 1:length(uniqueNewROIs)
            if sum(contains(newROIList,uniqueNewROIs{iNewROI})) ==1
                thisNewROI = uniqueNewROIs{iNewROI};
                newROIindex = contains(newROINames,thisNewROI);
                addOldROI = unique(oldROINames(newROIindex));
                % Just focus on subdivided MTG:
                addOldROI = addOldROI(~strcmp(addOldROI,'MTG'));
                for iOldROI = 1:length(addOldROI)
                    oldROILogical = contains(oldROINames,addOldROI{iOldROI}) ...
                        & contains(newROINames,thisNewROI);
                    nChans = sum(oldROILogical);
                    display([thisName ': ' thisNewROI '_' addOldROI{iOldROI} ' has ' num2str(nChans)]);
                    %                         if ~isfield(chanLogical,[thisNewROI '_' addOldROI{iOldROI}])
                    %                             chanLogical.([thisNewROI '_' addOldROI{iOldROI}]) = [];
                    %                         end
                    chanLogical_sleep.([thisNewROI '_' addOldROI{iOldROI}]).(thisName) = oldROILogical;
                end
            end
        end
    end
    if isfield(ECoGchannels_anes.(thisName), 'newROI')
        newROINames = {ECoGchannels_anes.(thisName).newROI};
        uniqueNewROIs = unique(newROINames);
        oldROINames = {ECoGchannels_anes.(thisName).oldROI};
        % Get channel numbers corresponding to ROIs
        for iNewROI = 1:length(uniqueNewROIs)
            if sum(contains(newROIList,uniqueNewROIs{iNewROI})) ==1
                thisNewROI = uniqueNewROIs{iNewROI};
                newROIindex = contains(newROINames,thisNewROI);
                addOldROI = unique(oldROINames(newROIindex));
                % Just focus on subdivided MTG:
                addOldROI = addOldROI(~strcmp(addOldROI,'MTG'));
                for iOldROI = 1:length(addOldROI)
                    oldROILogical = contains(oldROINames,addOldROI{iOldROI}) ...
                        & contains(newROINames,thisNewROI);
                    nChans = sum(oldROILogical);
                    display([thisName ': ' thisNewROI '_' addOldROI{iOldROI} ' has ' num2str(nChans)]);
                    %                         if ~isfield(chanLogical,[thisNewROI '_' addOldROI{iOldROI}])
                    %                             chanLogical.([thisNewROI '_' addOldROI{iOldROI}]) = [];
                    %                         end
                    chanLogical_anes.([thisNewROI '_' addOldROI{iOldROI}]).(thisName) = oldROILogical;
                end
            end
        end
    end
end

ROIs_sleep = fieldnames(chanLogical_sleep);
ROIs_anes = fieldnames(chanLogical_anes);
% Check for error
if length(ROIs_sleep) ~= length(ROIs_anes) || ...
        length(intersect(ROIs_sleep,ROIs_anes))~= length(ROIs_anes)
    error('Mismatch in ROIs_sleep and ROIs_anes');
end

% Sort ROIs and check that ROIs are in both sleep and anesthesia data
% Compare ROIs_sleep with ROI list from ROI_tableRead. Note that to exclude
% ROIs from the analysis, just need to omit from the list in ROI_tableRead.
%
[ROI_table] = ROI_tableRead(electrodeFilePath,electrodeFile,electrodeSheet);
ROIsInTable = {};
for iROI = 1:size(ROI_table,1)
    ROIsInTable{iROI} = [ROI_table.newROI{iROI} '_' ROI_table.oldROI{iROI}];
end
ROIs_to_process = intersect(ROIs_sleep,ROIsInTable);
nROIs = length(ROIs_to_process);
sortIndex = NaN(1,nROIs);
for iROI = 1:nROIs
    sortIndex(iROI) = ROI_table.oldROINum(contains(ROIs_to_process,ROIsInTable{iROI}));
end
ROIs_to_process = ROIs_to_process(sortIndex);

% Now do plotting
wPLIMatrix_save = struct();
for iPatient = 1:length(patientsToUse)
    thisName = patientsToUse{iPatient};
    for iBand = 1:length(FreqBands.Names)
        thisBand = FreqBands.Names{iBand};
        theseStages = intersect(fieldnames(wPLIMn.(thisName).(thisBand)),stagesToPlot);
        if plotChordConn
            chordFigName = [thisName '- ConnChordPlot - AnesVsSleep - thresh@' num2str(pctToDiscard) ' - ' thisBand];
            chordFig = figure('Name',chordFigName);
        end
        if plotROIByROIAdjMat
            ROIMatFigName = [thisName '- ROIMatPlot - AnesVsSleep - thresh@' num2str(pctToDiscard) ' - ' thisBand];
            ROIMatFig = figure('Name',ROIMatFigName);
            maxVal = 0;
            minVal = 0;
        end
        for iState = 1:length(statesToPlot)
            thisState = statesToPlot{iState};            
            wPLIMnMatrix = NaN(nROIs,nROIs);
            wPLISEMMatrix = NaN(nROIs,nROIs);
            for iROI = 1:nROIs
                thisROI = ROIs_to_process{iROI};
                for jROI = 1:iROI
                    thatROI = ROIs_to_process{jROI};
                    if isfield(chanLogical_anes.(thisROI),thisName) && isfield(chanLogical_anes.(thatROI),thisName)
                        thisLogical = chanLogical_anes.(thisROI).(thisName);
                        thatLogical = chanLogical_anes.(thatROI).(thisName);
                        tempMn = wPLIMn.(thisName).(thisBand).(thisState)(thisLogical,thatLogical);
                        tempSEM = wPLISEM.(thisName).(thisBand).(thisState)(thisLogical,thatLogical);
                        %                         tempSEM = ECoG_conn.wPLI_dbt.(thisName).W.seg_1.alpha.wpli_debiasedspctrmsem(thisLogical,thatLogical);
                        %                         tempZ = tempMeas./tempSEM;
                        %                         testZ = tempZ>2;
                        %                         tempMeas(~testZ) = 0;
                        %                         tempMeas(~testZ) = NaN;
                        wPLIMnMatrix(iROI,jROI) = mean(tempMn(:),'omitnan');
                        wPLISEMMatrix(iROI,jROI) = mean(tempSEM(:),'omitnan');
                    end
                end
            end
            
            ROIsToPlot = zeros(1,nROIs);
            for iROI = 1:nROIs
                if sum(~isnan(wPLIMnMatrix(iROI,:)))>0 || sum(~isnan(wPLIMnMatrix(:,iROI))) >0
                    ROIsToPlot(iROI) = 1;
                end
            end
            ROIIndices = find(ROIsToPlot);

            wPLIMnMatrix = wPLIMnMatrix(logical(ROIsToPlot),logical(ROIsToPlot));
            tempVals = wPLIMnMatrix(:);
            tempVals = tempVals(tempVals>0);
            tempVals = tempVals(~isnan(tempVals));
            threshCrit = prctile(tempVals(:),pctToDiscard);
            wPLIMnMatrix(wPLIMnMatrix<threshCrit) = 0;
            wPLIMnMatrix(isnan(wPLIMnMatrix)) = 0;
            
            wPLIMatrix_save.(thisName).(thisBand).(thisState) = wPLIMnMatrix;
            wPLIMatrixToPlot = wPLIMatrix_save.(thisName).(thisBand).(thisState);
            
            textToDisplay = [];
            myColorMap = [];
            for iROI = 1:length(ROIIndices)
                thisIndex = ROIIndices(iROI);
                thisROI = ROIs_to_process{thisIndex};
                textToDisplay{iROI} = thisROI(strfind(thisROI,'_')+1:end);
                myColorMap(iROI,:) = colorVals(contains(newROIList,thisROI(1:strfind(thisROI,'_')-1)),:);
            end
            
            if plotROIByROIAdjMat
                tempROIMat = wPLIMatrixToPlot;
                for iRow = 1:size(tempROIMat,1)
                    for iCol = 1:iRow
                        tempROIMat(iCol,iRow) = tempROIMat(iRow,iCol);
                    end
                end
                maxVal = max([maxVal, prctile(tempROIMat(:),97.5)]);
                figure(ROIMatFig);
                subplot(2,length(theseStages),iState);
                imagesc(tempROIMat);
                axis square;
                ax = gca;
                ax.Title.String = thisState;
                if iState == 1
                    ax.XLabel.String = 'ROI';
                    ax.YLabel.String = 'ROI';
                    ax.XTick = 1:nROIs;
                    ax.YTick = 1:nROIs;
                    ax.XTickLabel = textToDisplay;
                    ax.YTickLabel = textToDisplay;
                    ax.XTickLabelRotation = 90;
                    ax.FontSize = 8;
                else
                    ax.XTickLabel = [];
                    ax.YTickLabel = [];
                end
            end
            if plotChordConn
                figure(chordFig);
                subplot(2,length(theseStages),iState);
                myCircularGraph(wPLIMatrixToPlot(end:-1:1,end:-1:1),...
                    'Colormap',myColorMap(end:-1:1,:),...
                    'Label',textToDisplay(end:-1:1),...
                    'MaxVal',maxVals(iBand));
                ax = gca;
                %         t = text(mean(ax.XLim),ax.YLim(2)*1.1,[thisBand ' - ' thisState]);
                t = text(mean(ax.XLim),ax.YLim(2)*1.1,thisState);
                t.FontSize = 18;
                t.FontWeight = 'bold';
                t.HorizontalAlignment = 'center';
            end
        end
        stageCount = 0;
        for iStage = 1:length(stagesToPlot)
            thisStage = stagesToPlot{iStage};
            if sum(contains(theseStages,thisStage))
                stageCount = stageCount+1;
                subplot(2,length(theseStages),stageCount+length(theseStages));
                wPLIMnMatrix = NaN(nROIs,nROIs);
                for iROI = 1:nROIs
                    thisROI = ROIs_to_process{iROI};
                    for jROI = 1:iROI
                        thatROI = ROIs_to_process{jROI};
                        if isfield(chanLogical_sleep.(thisROI),thisName) && isfield(chanLogical_sleep.(thatROI),thisName)
                            thisLogical = chanLogical_sleep.(thisROI).(thisName);
                            thatLogical = chanLogical_sleep.(thatROI).(thisName);
                            tempMn = wPLIMn.(thisName).(thisBand).(thisStage)(thisLogical,thatLogical);
                            %                         tempSEM = ECoG_conn.wPLI_dbt.(thisName).W.seg_1.alpha.wpli_debiasedspctrmsem(thisLogical,thatLogical);
                            %                         tempZ = tempMeas./tempSEM;
                            %                         testZ = tempZ>2;
                            %                         tempMeas(~testZ) = 0;
                            %                         tempMeas(~testZ) = NaN;
                            wPLIMnMatrix(iROI,jROI) = mean(tempMn(:),'omitnan');
                        end
                    end
                end
                
                ROIsToPlot = zeros(1,nROIs);
                for iROI = 1:nROIs
                    if sum(~isnan(wPLIMnMatrix(iROI,:)))>0 || sum(~isnan(wPLIMnMatrix(:,iROI))) >0
                        ROIsToPlot(iROI) = 1;
                    end
                end
                ROIIndices = find(ROIsToPlot);
                
                tempVals = wPLIMnMatrix(:);
                tempVals = tempVals(tempVals>0);
                tempVals = tempVals(~isnan(tempVals));
                threshCrit = prctile(tempVals(:),pctToDiscard);
                wPLIMnMatrix(wPLIMnMatrix<threshCrit) = 0;
                wPLIMnMatrix(isnan(wPLIMnMatrix)) = 0;
                
                wPLIMatrix_save.(thisName).(thisBand).(thisStage) = wPLIMnMatrix(logical(ROIsToPlot),logical(ROIsToPlot));
                wPLIMatrixToPlot = wPLIMatrix_save.(thisName).(thisBand).(thisStage);
                textToDisplay = [];
                myColorMap = [];
                for iROI = 1:length(ROIIndices)
                    thisIndex = ROIIndices(iROI);
                    thisROI = ROIs_to_process{thisIndex};
                    textToDisplay{iROI} = thisROI(strfind(thisROI,'_')+1:end);
                    myColorMap(iROI,:) = colorVals(contains(newROIList,thisROI(1:strfind(thisROI,'_')-1)),:);
                end
                
                if plotROIByROIAdjMat
                    tempROIMat = wPLIMatrixToPlot;
                    for iRow = 1:size(tempROIMat,1)
                        for iCol = 1:iRow
                            tempROIMat(iCol,iRow) = tempROIMat(iRow,iCol);
                        end
                    end
                    maxVal = max([maxVal, prctile(tempROIMat(:),97.5)]);
                    figure(ROIMatFig);
                    subplot(2,length(theseStages),stageCount+length(theseStages));
                    imagesc(tempROIMat);
                    axis square;
                    ax = gca;
                    ax.Title.String = thisStage;
                    if stageCount == 1
                        ax.XLabel.String = 'ROI';
                        ax.YLabel.String = 'ROI';
                        ax.XTick = 1:nROIs;
                        ax.YTick = 1:nROIs;
                        ax.XTickLabel = textToDisplay;
                        ax.YTickLabel = textToDisplay;
                        ax.XTickLabelRotation = 90;
                        ax.FontSize = 8;
                    else
                        ax.XTickLabel = [];
                        ax.YTickLabel = [];
                    end
                    if stageCount == length(theseStages)
                        colorbar('southoutside');
                    end
                end
                if plotChordConn
                    figure(chordFig);
                    subplot(2,length(theseStages),stageCount+length(theseStages));
                    myCircularGraph(wPLIMatrixToPlot(end:-1:1,end:-1:1),...
                        'Colormap',myColorMap(end:-1:1,:),...
                        'Label',textToDisplay(end:-1:1),...
                        'MaxVal',maxVals(iBand));
                    ax = gca;
                    %         t = text(mean(ax.XLim),ax.YLim(2)*1.1,[thisBand ' - ' thisStage]);
                    t = text(mean(ax.XLim),ax.YLim(2)*1.1,thisStage);
                    t.FontSize = 18;
                    t.FontWeight = 'bold';
                    t.HorizontalAlignment = 'center';
                end
            end 
        end %stages

        if plotChordConn
            saveas(chordFig,[outPath chordFigName]);
        end
        if plotROIByROIAdjMat
            figure(ROIMatFig);
            for iPlot = 1:length(statesToPlot)
                subplot(2,length(theseStages),iPlot);
                caxis([minVal,maxVal]);
            end
            for iPlot = 1:stageCount
                subplot(2,length(theseStages),iPlot+length(statesToPlot));
                caxis([minVal,maxVal]);
            end
            saveas(ROIMatFig,[outPath ROIMatFigName]);
        end
    end %Loop over FreqBands.Names
end %Loop over subjects

%% ROI by ROI norm analyses
compWithin_anes = {'wake','sed'; 'wake','LOC'; 'sed','LOC'};
compWithin_sleep = {'W','N1'; 'W','N2'; 'N1','N2'};
compAcross = {'wake','W'; 'sed','N1'; 'LOC','N2'};
compShuff = {'wake','N1'; 'wake','N2';'sed','W'; 'sed','N2';'LOC','W'; 'LOC','N1';};
diffNorms = struct();
for iPatient = 1:length(patientIDs)
    thisName = patientIDs{iPatient};
    %
    % Note: norms are unaffected by clustering
    %
    for iComp = 1:size(compWithin_sleep,1)
        for iBand = 1:length(FreqBands.Names)
            thisBand = FreqBands.Names{iBand};
            mat1 = wPLIMatrix_save.(thisName).(thisBand).(compWithin_sleep{iComp,1});
            mat1 = makeSymmetric(mat1);
            mat2 = wPLIMatrix_save.(thisName).(thisBand).(compWithin_sleep{iComp,2});
            mat2 = makeSymmetric(mat2);
            diffNorms.(thisBand).withinSleep(iPatient,iComp) = norm(mat1-mat2);
        end
    end
    for iComp = 1:size(compWithin_anes,1)
        for iBand = 1:length(FreqBands.Names)
            thisBand = FreqBands.Names{iBand};
            mat1 = wPLIMatrix_save.(thisName).(thisBand).(compWithin_anes{iComp,1});
            mat1 = makeSymmetric(mat1);
            mat2 = wPLIMatrix_save.(thisName).(thisBand).(compWithin_anes{iComp,2});
            mat2 = makeSymmetric(mat2);
            diffNorms.(thisBand).withinAnes(iPatient,iComp) = norm(mat1-mat2);
        end
    end
    for iComp = 1:size(compAcross,1)
        for iBand = 1:length(FreqBands.Names)
            thisBand = FreqBands.Names{iBand};
            mat1 = wPLIMatrix_save.(thisName).(thisBand).(compAcross{iComp,1});
            mat1 = makeSymmetric(mat1);
            mat2 = wPLIMatrix_save.(thisName).(thisBand).(compAcross{iComp,2});
            mat2 = makeSymmetric(mat2);
            diffNorms.(thisBand).across(iPatient,iComp) = norm(mat1-mat2);
        end
    end
    for iComp = 1:size(compShuff,1)
        for iBand = 1:length(FreqBands.Names)
            thisBand = FreqBands.Names{iBand};
            mat1 = wPLIMatrix_save.(thisName).(thisBand).(compShuff{iComp,1});
            mat1 = makeSymmetric(mat1);
            mat2 = wPLIMatrix_save.(thisName).(thisBand).(compShuff{iComp,2});
            mat2 = makeSymmetric(mat2);
            diffNorms.(thisBand).shuff(iPatient,iComp) = norm(mat1-mat2);
        end
    end
end %loop over patients
% Scatter plots of norms results
% compAcross = {'wake','W'; 'sed','N1'; 'LOC','N2'};
% compShuff = {'WA','N1'; 'WA','N2';'S','WS'; 'S','N2';'U','WS'; 'U','N1';};
% shuffList(1,:) = [1,2,3,5]; %Compare WA,WS to WA,N1; WA,N2;, S,WS; U,WS
% shuffList(2,:) = [3,4,1,6]; %Compare S,N1 to S,WS; S,N2;, WA,N1; U,N1
% shuffList(3,:) = [5,6,2,4]; %Compare U,N2 to U,WS; U,N1;, WA,N2; S,N2
shuffList = [];
shuffList(1,:) = [2,5]; %Compare WA,WS to WA,N2; U,WS
shuffList(2,:) = [4,6]; %Compare S,N1 to S,N2; U,N1
shuffList(3,:) = [6,4]; %Compare U,N2 to U,N1; S,N2
symb = {'ro','r^','rx'; 'bo','b^','bx'; 'go','g^','gx'; 'ko','k^','kx'; 'mo','m^','mx'};
xLim = [0,1.5; 0,2; 0,3; 0,2; 0,2; 0,2];
yLim = xLim;
xTxtPos = 0.65*(xLim(:,2)-xLim(:,1))+xLim(:,1);
yTxtPos = 0.1*(yLim(:,2)-yLim(:,1))+yLim(:,1);
if plotNorms
    for iBand = 1:length(FreqBands.Names)
        thisBand = FreqBands.Names{iBand};
        figName = ['AnesVsSleep-ROIByROI-diffNorms@' num2str(pctToDiscard) '-' thisBand];
        normsFig = figure('Name',figName);
        hold on
        for iPatient = 1:length(patientIDs)
            for iState = 1:size(shuffList,1)
                shuffToPlot = mean(diffNorms.(thisBand).shuff(iPatient,shuffList(iState,:)));
                plot(diffNorms.(thisBand).across(iPatient,iState),shuffToPlot,symb{iPatient,iState},'MarkerSize',10);
%                 equivNorms = ones(1,size(shuffList,2))*diffNorms.(thisBand).across(iPatient,iState);
%                 plot(equivNorms,diffNorms.(thisBand).shuff(iPatient,shuffList(iState,:)),symb{iPatient,iState});
            end
        end
        axis square
        ax = gca;
        ax.XLim = xLim(iBand,:);
        ax.YLim = yLim(iBand,:);
        ax.LineWidth = 1;
        plot(xLim(iBand,:),yLim(iBand,:),':k','LineWidth',1);
        ax.XLabel.String = 'd_E_q_u_i_v';
        ax.YLabel.String = 'd_N_o_n_-_e_q_u_i_v';
        ax.FontSize = 14;
        plot(xTxtPos(iBand),yTxtPos(iBand)+1.2*yTxtPos(iBand),'ko','MarkerSize',10)
        plot(xTxtPos(iBand),yTxtPos(iBand)+0.6*yTxtPos(iBand),'k^','MarkerSize',10)
        plot(xTxtPos(iBand),yTxtPos(iBand),'kx','MarkerSize',10)
        text(xTxtPos(iBand)+.05*xTxtPos(iBand),yTxtPos(iBand)+1.2*yTxtPos(iBand),'WA vs WS','FontSize',12);
        text(xTxtPos(iBand)+.05*xTxtPos(iBand),yTxtPos(iBand)+0.6*yTxtPos(iBand),'S vs N1','FontSize',12);
        text(xTxtPos(iBand)+.05*xTxtPos(iBand),yTxtPos(iBand),'U vs N2','FontSize',12);
        saveas(normsFig,[outPath figName]);
    end
end

nPatients = length(patientIDs);
xLim = [0.5 nPatients+0.5];
yLim = [0,3; 0,3; 0,3; 0,3; 0,3; 0,3];
%yLim = [0,25; 0,25; 0,25; 0,20; 0,15];

for iBand = 1:length(FreqBands.Names)
    thisBand = FreqBands.Names{iBand};

    % comparison table of norm results
    % plot as 6x6 matrix, i.e. {WA,S,U,WS,N1,N2} vs {WA,S,U,WS,N1,N2}
    % sleepStages = {'W','N1','N2','N3','N4','R'};
    % anesStates = {'wake','sed','LOC'};
    normMatrix = NaN(6,6,nPatients);
    nmlzVal = zeros(1,nPatients);
    nmlz_norms_th = diffNorms;
    for iPatient = 1:nPatients
        comps = fieldnames(diffNorms.(thisBand));
        allVals = [];
        for iComp = 1:length(comps)
            thisComp = comps{iComp};
            allVals = [allVals diffNorms.(thisBand).(thisComp)(iPatient,:)];
        end
        nmlzVal(iPatient) = mean(allVals);
        for iComp = 1:length(comps)
            thisComp = comps{iComp};
            nmlz_norms_th.(thisBand).(thisComp)(iPatient,:) = diffNorms.(thisBand).(thisComp)(iPatient,:)/nmlzVal(iPatient);
        end    
    end
    normMatrix(1,2,1:nPatients) = diffNorms.(thisBand).withinAnes(1:nPatients,1); %wake vs sed
    normMatrix(1,3,1:nPatients) = diffNorms.(thisBand).withinAnes(1:nPatients,2); %wake vs LOC
    normMatrix(2,3,1:nPatients) = diffNorms.(thisBand).withinAnes(1:nPatients,3); %sed vs LOC
    normMatrix(1,4,1:nPatients) = diffNorms.(thisBand).across(1:nPatients,1);     %wake vs W
    normMatrix(1,5,1:nPatients) = diffNorms.(thisBand).shuff(1:nPatients,1);      %wake vs N1
    normMatrix(1,6,1:nPatients) = diffNorms.(thisBand).shuff(1:nPatients,2);      %wake vs N2
    normMatrix(2,4,1:nPatients) = diffNorms.(thisBand).shuff(1:nPatients,3);      %sed vs W
    normMatrix(2,5,1:nPatients) = diffNorms.(thisBand).across(1:nPatients,2);     %sed vs N1
    normMatrix(2,6,1:nPatients) = diffNorms.(thisBand).shuff(1:nPatients,4);      %sed vs N2
    normMatrix(3,4,1:nPatients) = diffNorms.(thisBand).shuff(1:nPatients,5);      %LOC vs W
    normMatrix(3,5,1:nPatients) = diffNorms.(thisBand).shuff(1:nPatients,6);      %LOC vs N1
    normMatrix(3,6,1:nPatients) = diffNorms.(thisBand).across(1:nPatients,3);     %LOC vs N2
    normMatrix(4,5,1:nPatients) = diffNorms.(thisBand).withinSleep(1:nPatients,1);%W vs N1
    normMatrix(4,6,1:nPatients) = diffNorms.(thisBand).withinSleep(1:nPatients,2);%W vs N2
    normMatrix(5,6,1:nPatients) = diffNorms.(thisBand).withinSleep(1:nPatients,3);%N1 vs N2

    if plotComparisons
        figName = ['AnesVsSleep-ROIByROI-within comparisons-thresh@' num2str(pctToDiscard) '-' thisBand];
        withinCompFig = figure('Name',figName);
        subplot(1,2,1);
        plot(squeeze(normMatrix(1,2,:)),'o-b','MarkerSize',10,'LineWidth',2); %wake vs sed
        hold on
        plot(squeeze(normMatrix(1,3,:)),'o-r','MarkerSize',10,'LineWidth',2); %wake vs LOC
        plot(squeeze(normMatrix(2,3,:)),'o-m','MarkerSize',10,'LineWidth',2); %sed vs LOC
        legend({'wave vs. sed'; 'wake vs LOC'; 'sed vs. LOC'});
        ax = gca;
        ax.LineWidth = 1;
        ax.XLabel.String = 'Subject #';
        ax.YLabel.String = 'd';
        ax.Title.String = 'Anesthesia';
        ax.XTick = 1:nPatients;
        ax.XLim = xLim;
        ax.YLim = yLim(iBand,:);
        ax.FontSize = 14;

        subplot(1,2,2)
        plot(squeeze(normMatrix(4,5,:)),'o-b','MarkerSize',10,'LineWidth',2); %W vs N1
        hold on
        plot(squeeze(normMatrix(4,6,:)),'o-r','MarkerSize',10,'LineWidth',2); %W vs N2
        plot(squeeze(normMatrix(5,6,:)),'o-m','MarkerSize',10,'LineWidth',2); %N1 vs N2
        legend({'W vs N1'; 'W vs N2'; 'N1 vs N2'});
        ax = gca;
        ax.LineWidth = 1;
        ax.XLabel.String = 'Subject #';
        ax.YTickLabel = [];
%         ax.YLabel.String = 'd';
        ax.Title.String = 'Sleep';
        ax.XTick = 1:nPatients;
        ax.XLim = [0.5 nPatients+0.5];
        ax.YLim = yLim(iBand,:);
        ax.FontSize = 14;
        saveas(withinCompFig,[outPath figName]);
    end
end
%%
% %% Assemble data
% % Find channels common to anesthesia and sleep in each subject, 
% % sort by distance from HGPM and compute means across sleep segments and
% % across common anesthesia states.
% 
% %Load anesthesia data
% load([outPath 'ECoG_conn_' anesSetName '.mat'],'ECoG_conn','batchParams');
% ECoG_conn_anes = ECoG_conn;
% batchParams_anes = batchParams;
% clear ECoG_conn
% clear batchParams
% 
% %Load sleep data
% load([outPath 'ECoG_conn_' sleepSetName '.mat'],'ECoG_conn','batchParams');
% ECoG_conn_sleep = ECoG_conn;
% batchParams_sleep = batchParams;
% clear ECoG_conn
% clear batchParams
% 
% patientIDs_sleep = fieldnames(ECoG_conn_sleep.wPLI_dbt);
% patientIDs_anes = fieldnames(ECoG_conn_anes.wPLI_dbt);
% patientIDs = intersect(patientIDs_sleep,patientIDs_anes);
% wPLIMn = struct();
% 
% for iPatient = 1:length(patientIDs)
%     thisName = patientIDs{iPatient};
%     params_anes = batchParams_anes.(thisName);
%     params_sleep = batchParams_sleep.(thisName);
%     ECoGchannels_anes.(thisName) = params_anes.ECoGchannels;
%     ECoGchannels_sleep.(thisName) = params_sleep.ECoGchannels;
%     chanNum_anes = [ECoGchannels_anes.(thisName).chanNum];
%     if isfield(ECoGchannels_sleep.(thisName),'contactNum')
%         chanNum_sleep = [ECoGchannels_sleep.(thisName).contactNum];
%     else
%         chanNum_sleep = [ECoGchannels_sleep.(thisName).chanNum];
%     end
% % [C,ia,ib] = interset(A,B); % C=A(ia) and C=B(ib)
%     [chanIntersect,indexAnes,indexSleep] = intersect(chanNum_anes,chanNum_sleep);
%     ECoGchannels_anes.(thisName) = ECoGchannels_anes.(thisName)(indexAnes);
%     ECoGchannels_sleep.(thisName) = ECoGchannels_sleep.(thisName)(indexSleep);
%     sortIndex_anes = chanSort(ECoGchannels_anes.(thisName),onlyAudHierarchy);
%     sortIndex_sleep = chanSort(ECoGchannels_sleep.(thisName),onlyAudHierarchy);
%     if ~isfield(ECoGchannels_anes.(thisName),'newROINum')
%         ROINum_anes = zeros(1,length(indexAnes));
%         for iROI = 1:length(nineROIs)
%             ROINum_anes(contains({ECoGchannels_anes.(thisName).newROI},nineROIs{iROI})) = iROI;
%         end
%     else
%         ROINum_anes = [ECoGchannels_anes.(thisName).newROINum];
%     end
%     ROINum_anes = ROINum_anes(sortIndex_anes);
%     if ~isfield(ECoGchannels_sleep.(thisName),'newROINum')
%         ROINum_sleep = zeros(1,length(indexSleep));
%         for iROI = 1:length(nineROIs)
%             ROINum_sleep(contains({ECoGchannels_sleep.(thisName).newROI},nineROIs{iROI})) = iROI;
%         end
%     else
%         ROINum_sleep = [ECoGchannels_sleep.(thisName).newROINum];
%     end
%     ROINum_sleep = ROINum_sleep(sortIndex_sleep);
% %     tempAnes = ECoGchannels_anes.(thisName)(sortIndex);
% %     tempSleep = ECoGchannels_sleep.(thisName)(sortIndex);
%     
%     %Sleep data
%     stages = fieldnames(ECoG_conn_sleep.wPLI_dbt.(thisName));
%     stageCount = 0;
%     for iStage = 1:length(sleepStages)
%         % Use the following if statement to plot stages in standard order
%         if sum(contains(stages,sleepStages{iStage}))>0
%             stageCount = stageCount+1;
%             thisStage = sleepStages{iStage};
%             segs = fieldnames(ECoG_conn_sleep.wPLI_dbt.(thisName).(thisStage));
%             nSegs = length(segs);
%             FreqBands.Names = fieldnames(ECoG_conn_sleep.wPLI_dbt.(thisName).(thisStage).seg_1);
%             for iBand = 1:length(FreqBands.Names)
%                 thisBand = FreqBands.Names{iBand};
%                 for iSeg = 1:nSegs
%                     thisSeg = segs{iSeg};
%                     tempMat = ...
%                         ECoG_conn_sleep.wPLI_dbt.(thisName).(thisStage).(thisSeg).(thisBand).wpli_debiasedspctrm;
%                     tempMat = tempMat(indexSleep,indexSleep);
%                     tempMat = tempMat(sortIndex_sleep,sortIndex_sleep);
%                     tempMat(logical(eye(size(tempMat)))) = 0;
%                     nCheckNan = sum(isnan(tempMat(:)));
%                     if nCheckNan > 0
%                         display(['Warning: ' num2str(nCheckNan) ' wPLI vals are NaNs for ' thisName ' ' thisStage ' ' thisBand]);
%                     end
%                     tempMat(isnan(tempMat)) = 0;
%                     nCheckNan = sum(isnan(tempMat(:)));
%                     if nCheckNan > 0
%                         display(['Warning: ' num2str(nCheckNan) ' wPLI vals are NaNs for ' thisName ' ' thisStage ' ' thisBand]);
%                     end
%                     % compute wPLI adj matrix as average over all segments
%                     if iSeg == 1
%                         wPLIMn.(thisName).(thisBand).(thisStage) = ...
%                             zeros(size(tempMat));
%                     end
%                     wPLIMn.(thisName).(thisBand).(thisStage) = ...
%                         wPLIMn.(thisName).(thisBand).(thisStage) + tempMat;
%                 end
%                 wPLIMn.(thisName).(thisBand).(thisStage) = wPLIMn.(thisName).(thisBand).(thisStage)/nSegs;
%                 wPLIMn.(thisName).(thisBand).(thisStage)(logical(eye(size(wPLIMn.(thisName).(thisBand).(thisStage))))) = 0;
%             end
%         end
%     end
%     % Anesthesia data
%     conditions = params_anes.newCond;
%     OAASVals = zeros(1,length(conditions));
%     for iCond = 1:length(conditions)
%         OAASVals(iCond) = getOAASVal(conditions{iCond});
%     end
%     condSet.(anesStates{1}) = [1,2]; %First two are always pre-drug in split cond
%     sedIndx = find(OAASVals>=3);
%     condSet.(anesStates{2}) = sedIndx(sedIndx>2); %>2 because first two are pre-drug
%     condSet.(anesStates{3}) = find(OAASVals<3);
%     for iState = 1:length(anesStates)
%         thisState = anesStates{iState};
%         FreqBands.Names = fieldnames(ECoG_conn_anes.wPLI_dbt.(thisName).(conditions{1}));
%         for iBand = 1:length(FreqBands.Names)
%             thisBand = FreqBands.Names{iBand};
%             wPLIMn.(thisName).(thisBand).(thisState) = zeros(length(sortIndex_anes),length(sortIndex_anes));
%             for iCond = condSet.(thisState)
%                 thisCond = conditions{iCond};
%                 tempMat = ECoG_conn_anes.wPLI_dbt.(thisName).(thisCond).(thisBand).wpli_debiasedspctrm;
%                 tempMat = tempMat(indexAnes,indexAnes);
%                 tempMat = tempMat(sortIndex_anes,sortIndex_anes);
%                 tempMat(logical(eye(size(tempMat)))) = 0;
%                 nCheckNan = sum(isnan(tempMat(:)));
%                 if nCheckNan > 0
%                     display(['Warning: ' num2str(nCheckNan) ' wPLI vals are NaNs for ' thisName ' ' thisState ' ' thisBand]);
%                 end
%                 tempMat(isnan(tempMat)) = 0;
%                 wPLIMn.(thisName).(thisBand).(thisState) = wPLIMn.(thisName).(thisBand).(thisState) + tempMat;
%             end
%             wPLIMn.(thisName).(thisBand).(thisState) = wPLIMn.(thisName).(thisBand).(thisState)/length(condSet.(anesStates{iState}));
% %             wPLIMn.(thisName).(thisBand).(thisState)(logical(eye(size(wPLIMn.(thisName).(thisBand).(thisState))))) = 0;            
%         end
%     end
% end
% 
% %% Take assembled wPLI data and do subject-by-subject analyses - here using nChan x nChan adj matrices
% 
% % Compare adjacency matrices derived from wPLI connectivity matrices
% % -----Threshold (and maybe binarize)-----
% 
% % First, align electrode lists for sleep and anesthesia data - there may be
% % some small discrepancies.
% % Second, threshold matrices to yield band-specific adjacency matrices.
% % Third, compute similarity of adjacency matrices. Similarity is computed
% % as the operator norm of the difference matrix. The operator norm of a 
% % matrix A is the maximum, over all vectors v with ||v|| = 1, of || Av||. 
% % This is a measure of how much A expands vectors when you apply it to 
% % them. It turns out this is related to the spectrum of A^T*A, and in 
% % particular is equal to the square root of the max eigenvalue of A^T*A.
% % This is computed in Matlab as norm(A).
% % We have two manipulations - sleep and anesthesia. We will compare within
% % manipulation across brain state, i.e. is the adjacency matrix similar in
% % wake vs sedated, etc? And we will compare equivalent brain states across
% % manipulations, i.e. wake_anes versus wake_sleep, sed_anes versus
% % N1_sleep, LOC_anes versus N2/N3_sleep. The hypothesis is that the latter
% % comparisons will yield more similar matrices than the former.
% 
% wPLI_thresh = wPLIMn;
% for iPatient = 1:length(patientIDs)
%     thisName = patientIDs{iPatient};
%     plotMinVal = zeros(1,length(FreqBands.Names));
%     plotMaxVal = zeros(1,length(FreqBands.Names));
%     %Threshold & maybe binarize
%     for iBand = 1:length(FreqBands.Names)
%         thisBand = FreqBands.Names{iBand};
%         states = fieldnames(wPLI_thresh.(thisName).(thisBand));
%         threshCutoff = zeros(1,length(states));
%         for iState = 1:length(states)
%             thisState = states{iState};
%             tempMat = tril(wPLI_thresh.(thisName).(thisBand).(thisState));
%             wPLIVals = tempMat(:);
%             wPLIVals = wPLIVals(wPLIVals~=0);
%             threshCutoff(iState) = prctile(wPLIVals,pctToDiscard);
%             maxVal(iState) = prctile(wPLIVals,97.5);
%             %Threshold
%             wPLI_thresh.(thisName).(thisBand).(thisState)(wPLI_thresh.(thisName).(thisBand).(thisState)<threshCutoff(iState)) = 0;
%             %Binarize
%             if binarize
%                 wPLI_thresh.(thisName).(thisBand).(thisState)(wPLI_thresh.(thisName).(thisBand).(thisState)>0) = 1;
%             end
%         end
%         plotMinVal(iBand) = min(threshCutoff)*0.9;
%         plotMaxVal(iBand) = max(maxVal);
%     end
%     
%     %compAcross = {'wake','W'; 'sed','N1'; 'LOC','N2'};
%     if plotChanByChanAdjMat
%         stateLabels = compAcross';
%         [nRows, nCols] = size(stateLabels);
%         for iBand = 1:length(FreqBands.Names)
%             thisBand = FreqBands.Names{iBand};
%             figName = [thisName '-AnesVsSleep-wPLIAdjMat-thresholded@' num2str(pctToDiscard) '-' thisBand];
%             matFig = figure('Name',figName);
%             for iRow = 1:nRows
%                 for iCol = 1:nCols
%                     subplot(nRows,nCols,iCol+(iRow-1)*nCols);
%                     imagesc(wPLI_thresh.(thisName).(thisBand).(stateLabels{iRow,iCol})); 
%                     axis square;
%                     ax = gca;
%                     ax.LineWidth = 1;
%                     caxis(gca,[plotMinVal(iBand), plotMaxVal(iBand)]);
%                     ax.Title.String = [stateLabels{iRow,iCol} ' - ' thisBand];
%                     if iRow == nRows && iCol == 1
%                         ax.XLabel.String = 'Chan #';
%                         ax.YLabel.String = 'Chan #';
%                     else
%                         ax.XTickLabel = [];
%                         ax.YTickLabel = [];
%                     end
%                     ax.FontSize = 12;
%                     if iRow == nRows && iCol == nCols
%                         colorbar('southoutside');
%                     end
%                 end
%             end
%             saveas(matFig,[outPath figName]);
%         end
%     end
% 
%     if plotClusteredAdjMat
%         %Cluster
%         wPLI_thr_clst = struct();
%         for iBand = 1:length(FreqBands.Names)
%             thisBand = FreqBands.Names{iBand};
%             %Cluster based on one state
%             adjMat = wPLI_thresh.(thisName).(thisBand).(stateToCluster);        
%             adjMat(adjMat>0) = 1;
%             switch stateToCluster
%                 case {'W','N1','N2','N3','R'}
%                     ROINum = ROINum_sleep;
%                 case {'wake','sed','LOC'}
%                     ROINum = ROINum_anes;
%             end
% 
%             VV=GCAFG(adjMat,Scale);
%             Mbst=CNNodMemb(VV,adjMat,1);
%             V=VV(:,Mbst);
%             nClusters = max(V);
%             % To try and get consistency between different runs
%             % of clustering algorithm, relable cluster numbers
%             % according to their mean ROI value (ROI = 1:7)
%             mnROIVal = zeros(1,nClusters);
%             for iCluster = 1:nClusters
%                 mnROIVal(iCluster) = mean(ROINum(V==iCluster));
%             end
%             [~,mnIndex] = sort(mnROIVal);
%             V = -V; %So as not to get confused when replacing V values
%             for iCluster = 1:nClusters
%                 V(V==-iCluster) = mnIndex(iCluster);
%             end
%             [~,indx] = sort(V);
% 
%             states = fieldnames(wPLI_thresh.(thisName).(thisBand));
%             for iState = 1:length(states)
%                 thisState = states{iState};
%                 switch thisState
%                     case {'W','N1','N2','N3','R'}
%                         ROINum = ROINum_sleep;
%                     case {'wake','sed','LOC'}
%                         ROINum = ROINum_anes;
%                 end
%                 wPLI_thr_clst.(thisBand).(thisState) = wPLI_thresh.(thisName).(thisBand).(thisState)(indx,indx);
%             end
%         end
%         stateLabels = compAcross';
%         [nRows, nCols] = size(stateLabels);
%         for iBand = 1:length(FreqBands.Names)
%             thisBand = FreqBands.Names{iBand};
%             figName = [thisName '-AnesVsSleep-wPLIAdjMat-thresholded@' num2str(pctToDiscard) '-Clustered-' thisBand];
%             matFig = figure('Name',figName);
%             for iRow = nRows:-1:1
%                 if iRow == 1
%                     ROINum = ROINum_anes;
%                 else
%                     ROINum = ROINum_sleep;
%                 end
%                 for iCol = 1:nCols
%                     subplot(nRows,nCols,iCol+(iRow-1)*nCols);
%                     adjMat = wPLI_thr_clst.(thisBand).(stateLabels{iRow,iCol});
%                     adjMat_ROIScaled = adjMat.*(sqrt(ROINum'*ROINum));
%                     imagesc(adjMat_ROIScaled); 
%                     axis square;
%                     ax = gca;
% %                     caxis(gca,[plotMinVal, plotMaxVal]);
%                     caxis(gca,[0, 7]);
%                     ax.Title.String = [stateLabels{iRow,iCol} ' - ' thisBand];
%                     if iRow == nRows && iCol == 1
%                         ax.XLabel.String = 'Chan #';
%                         ax.YLabel.String = 'Chan #';
%                     else
%                         ax.XTickLabel = [];
%                         ax.YTickLabel = [];
%                     end
%                     if iRow == nRows && iCol == nCols
%                         colorbar('southoutside');
%                     end
%                 end
%             end
%             saveas(matFig,[outPath figName]);
%         end
%     end
% end
% 
% diffNorms = struct();
% for iPatient = 1:length(patientIDs)
%     thisName = patientIDs{iPatient};   
%     % compWithin_anes = {'wake','sed'; 'wake','LOC'; 'sed','LOC'};
%     % compWithin_sleep = {'W','N1'; 'W','N2'; 'N1','N2'};
%     % compAcross = {'wake','W'; 'sed','N1'; 'LOC','N2'};
%     % compShuff = {'wake','N1'; 'wake','N2';'sed','W'; 'sed','N2';'LOC','W'; 'LOC','N1';};
%     %
%     % Note: norms are unaffected by clustering
%     %
%     for iComp = 1:size(compWithin_sleep,1)
%         for iBand = 1:length(FreqBands.Names)
%             thisBand = FreqBands.Names{iBand};
%             diffNorms.(thisBand).withinSleep(iPatient,iComp) = ...
%                 norm(wPLI_thresh.(thisName).(thisBand).(compWithin_sleep{iComp,1})-wPLI_thresh.(thisName).(thisBand).(compWithin_sleep{iComp,2}));
%         end
%     end
%     for iComp = 1:size(compWithin_anes,1)
%         for iBand = 1:length(FreqBands.Names)
%             thisBand = FreqBands.Names{iBand};
%             diffNorms.(thisBand).withinAnes(iPatient,iComp) = ...
%                 norm(wPLI_thresh.(thisName).(thisBand).(compWithin_anes{iComp,1})-wPLI_thresh.(thisName).(thisBand).(compWithin_anes{iComp,2}));
%         end
%     end
%     for iComp = 1:size(compAcross,1)
%         for iBand = 1:length(FreqBands.Names)
%             thisBand = FreqBands.Names{iBand};
%             diffNorms.(thisBand).across(iPatient,iComp) = ...
%                 norm(wPLI_thresh.(thisName).(thisBand).(compAcross{iComp,1})-wPLI_thresh.(thisName).(thisBand).(compAcross{iComp,2}));
%         end
%     end
%     for iComp = 1:size(compShuff,1)
%         for iBand = 1:length(FreqBands.Names)
%             thisBand = FreqBands.Names{iBand};
%             diffNorms.(thisBand).shuff(iPatient,iComp) = ...
%                 norm(wPLI_thresh.(thisName).(thisBand).(compShuff{iComp,1})-wPLI_thresh.(thisName).(thisBand).(compShuff{iComp,2}));
%         end
%     end
% end %loop over patients
% % Scatter plots of norms results
% % compAcross = {'wake','W'; 'sed','N1'; 'LOC','N2'};
% % compShuff = {'WA','N1'; 'WA','N2';'S','WS'; 'S','N2';'U','WS'; 'U','N1';};
% % shuffList(1,:) = [1,2,3,5]; %Compare WA,WS to WA,N1; WA,N2;, S,WS; U,WS
% % shuffList(2,:) = [3,4,1,6]; %Compare S,N1 to S,WS; S,N2;, WA,N1; U,N1
% % shuffList(3,:) = [5,6,2,4]; %Compare U,N2 to U,WS; U,N1;, WA,N2; S,N2
% shuffList = [];
% shuffList(1,:) = [2,5]; %Compare WA,WS to WA,N2; U,WS
% shuffList(2,:) = [4,6]; %Compare S,N1 to S,N2; U,N1
% shuffList(3,:) = [6,4]; %Compare U,N2 to U,N1; S,N2
% symb = {'ro','r^','rx'; 'bo','b^','bx'; 'go','g^','gx'; 'ko','k^','kx'; 'mo','m^','mx'};
% xLim = [1,8; 2,9; 1,13; 1,8; 0,7; 0,7];
% yLim = xLim;
% xTxtPos = 0.65*(xLim(:,2)-xLim(:,1))+xLim(:,1);
% yTxtPos = 0.1*(yLim(:,2)-yLim(:,1))+yLim(:,1);
% if plotNorms
%     for iBand = 1:length(FreqBands.Names)-1
%         thisBand = FreqBands.Names{iBand};
%         figName = ['AnesVsSleep-diffNorms@' num2str(pctToDiscard) '-' thisBand];
%         normsFig = figure('Name',figName);
%         hold on
%         for iPatient = 1:length(patientIDs)
%             for iState = 1:size(shuffList,1)
%                 shuffToPlot = mean(diffNorms.(thisBand).shuff(iPatient,shuffList(iState,:)));
%                 plot(diffNorms.(thisBand).across(iPatient,iState),shuffToPlot,symb{iPatient,iState},'MarkerSize',10);
% %                 equivNorms = ones(1,size(shuffList,2))*diffNorms.(thisBand).across(iPatient,iState);
% %                 plot(equivNorms,diffNorms.(thisBand).shuff(iPatient,shuffList(iState,:)),symb{iPatient,iState});
%             end
%         end
%         axis square
%         ax = gca;
%         ax.XLim = xLim(iBand,:);
%         ax.YLim = yLim(iBand,:);
%         ax.LineWidth = 1;
%         plot(xLim(iBand,:),yLim(iBand,:),':k','LineWidth',1);
%         ax.XLabel.String = 'd_E_q_u_i_v';
%         ax.YLabel.String = 'd_N_o_n_-_e_q_u_i_v';
%         ax.FontSize = 14;
%         plot(xTxtPos(iBand),yTxtPos(iBand)+0.4*yTxtPos(iBand),'ko','MarkerSize',10)
%         plot(xTxtPos(iBand),yTxtPos(iBand)+0.2*yTxtPos(iBand),'k^','MarkerSize',10)
%         plot(xTxtPos(iBand),yTxtPos(iBand),'kx','MarkerSize',10)
%         text(xTxtPos(iBand)+.05*xTxtPos(iBand),yTxtPos(iBand)+0.4*yTxtPos(iBand),'WA vs WS','FontSize',12);
%         text(xTxtPos(iBand)+.05*xTxtPos(iBand),yTxtPos(iBand)+0.2*yTxtPos(iBand),'S vs N1','FontSize',12);
%         text(xTxtPos(iBand)+.05*xTxtPos(iBand),yTxtPos(iBand),'U vs N2','FontSize',12);
%         saveas(normsFig,[outPath figName]);
%     end
% end
% 
% nPatients = length(patientIDs);
% xLim = [0.5 nPatients+0.5];
% yLim = [0,10; 0,11; 0,15; 0,10; 0,8; 0,8];
% %yLim = [0,25; 0,25; 0,25; 0,20; 0,15];
% 
% for iBand = 1:length(FreqBands.Names)-1 %skip high gamma
%     thisBand = FreqBands.Names{iBand};
% 
%     % comparison table of norm results
%     % plot as 6x6 matrix, i.e. {WA,S,U,WS,N1,N2} vs {WA,S,U,WS,N1,N2}
%     % sleepStages = {'W','N1','N2','N3','N4','R'};
%     % anesStates = {'wake','sed','LOC'};
%     mnNormMatrix = NaN(6,6);
%     mnNormMatrix(logical(eye(6))) = diag(zeros(6));
%     normMatrix = NaN(6,6,nPatients);
%     nmlzVal = zeros(1,nPatients);
%     nmlz_norms_th = diffNorms;
%     for iPatient = 1:nPatients
%         comps = fieldnames(diffNorms.(thisBand));
%         allVals = [];
%         for iComp = 1:length(comps)
%             thisComp = comps{iComp};
%             allVals = [allVals diffNorms.(thisBand).(thisComp)(iPatient,:)];
%         end
%         nmlzVal(iPatient) = mean(allVals);
%         for iComp = 1:length(comps)
%             thisComp = comps{iComp};
%             nmlz_norms_th.(thisBand).(thisComp)(iPatient,:) = diffNorms.(thisBand).(thisComp)(iPatient,:)/nmlzVal(iPatient);
%         end    
%     end
%     normMatrix(1,2,1:nPatients) = diffNorms.(thisBand).withinAnes(1:nPatients,1); %wake vs sed
%     normMatrix(1,3,1:nPatients) = diffNorms.(thisBand).withinAnes(1:nPatients,2); %wake vs LOC
%     normMatrix(2,3,1:nPatients) = diffNorms.(thisBand).withinAnes(1:nPatients,3); %sed vs LOC
%     normMatrix(1,4,1:nPatients) = diffNorms.(thisBand).across(1:nPatients,1);     %wake vs W
%     normMatrix(1,5,1:nPatients) = diffNorms.(thisBand).shuff(1:nPatients,1);      %wake vs N1
%     normMatrix(1,6,1:nPatients) = diffNorms.(thisBand).shuff(1:nPatients,2);      %wake vs N2
%     normMatrix(2,4,1:nPatients) = diffNorms.(thisBand).shuff(1:nPatients,3);      %sed vs W
%     normMatrix(2,5,1:nPatients) = diffNorms.(thisBand).across(1:nPatients,2);     %sed vs N1
%     normMatrix(2,6,1:nPatients) = diffNorms.(thisBand).shuff(1:nPatients,4);      %sed vs N2
%     normMatrix(3,4,1:nPatients) = diffNorms.(thisBand).shuff(1:nPatients,5);      %LOC vs W
%     normMatrix(3,5,1:nPatients) = diffNorms.(thisBand).shuff(1:nPatients,6);      %LOC vs N1
%     normMatrix(3,6,1:nPatients) = diffNorms.(thisBand).across(1:nPatients,3);     %LOC vs N2
%     normMatrix(4,5,1:nPatients) = diffNorms.(thisBand).withinSleep(1:nPatients,1);%W vs N1
%     normMatrix(4,6,1:nPatients) = diffNorms.(thisBand).withinSleep(1:nPatients,2);%W vs N2
%     normMatrix(5,6,1:nPatients) = diffNorms.(thisBand).withinSleep(1:nPatients,3);%N1 vs N2
% 
%     mnNormMatrix(1,2) = mean(diffNorms.(thisBand).withinAnes(:,1)./nmlzVal'); %wake vs sed
%     mnNormMatrix(1,3) = mean(diffNorms.(thisBand).withinAnes(:,2)./nmlzVal'); %wake vs LOC
%     mnNormMatrix(2,3) = mean(diffNorms.(thisBand).withinAnes(:,3)./nmlzVal'); %sed vs LOC
%     mnNormMatrix(1,4) = mean(diffNorms.(thisBand).across(:,1)./nmlzVal');     %wake vs W
%     mnNormMatrix(1,5) = mean(diffNorms.(thisBand).shuff(:,1)./nmlzVal');      %wake vs N1
%     mnNormMatrix(1,6) = mean(diffNorms.(thisBand).shuff(:,2)./nmlzVal');      %wake vs N2
%     mnNormMatrix(2,4) = mean(diffNorms.(thisBand).shuff(:,3)./nmlzVal');      %sed vs W
%     mnNormMatrix(2,5) = mean(diffNorms.(thisBand).across(:,2)./nmlzVal');     %sed vs N1
%     mnNormMatrix(2,6) = mean(diffNorms.(thisBand).shuff(:,4)./nmlzVal');      %sed vs N2
%     mnNormMatrix(3,4) = mean(diffNorms.(thisBand).shuff(:,5)./nmlzVal');      %LOC vs W
%     mnNormMatrix(3,5) = mean(diffNorms.(thisBand).shuff(:,6)./nmlzVal');      %LOC vs N1
%     mnNormMatrix(3,6) = mean(diffNorms.(thisBand).across(:,3)./nmlzVal');     %LOC vs N2
%     mnNormMatrix(4,5) = mean(diffNorms.(thisBand).withinSleep(:,1)./nmlzVal');%W vs N1
%     mnNormMatrix(4,6) = mean(diffNorms.(thisBand).withinSleep(:,2)./nmlzVal');%W vs N2
%     mnNormMatrix(5,6) = mean(diffNorms.(thisBand).withinSleep(:,3)./nmlzVal');%N1 vs N2
%     if plotNormMatrix
%         figName = ['AnesVsSleep-normMatrix-thresh@' num2str(pctToDiscard) '-' thisBand];
%         normMatFig = figure('Name',figName);
%         imagesc(mnNormMatrix);
%         axis square;
%         ax = gca;
%         caxis(gca,[0.5, 1]);
%         %    ax.Title.String = [stateLabels{iRow,iCol} ' - ' thisBand];
%         %     if iRow == nRows && iCol == 1
%         %         ax.XLabel.String = 'Chan #';
%         %         ax.YLabel.String = 'Chan #';
%         %     else
%         %         ax.XTickLabel = [];
%         %         ax.YTickLabel = [];
%         %     end
%         colorbar('southoutside');
%         saveas(normMatFig,[outPath figName]);
%     end
%     if plotComparisons
%         figName = ['AnesVsSleep-within comparisons-thresh@' num2str(pctToDiscard) '-' thisBand];
%         withinCompFig = figure('Name',figName);
%         subplot(1,2,1);
%         plot(squeeze(normMatrix(1,2,:)),'o-b','MarkerSize',10,'LineWidth',2); %wake vs sed
%         hold on
%         plot(squeeze(normMatrix(1,3,:)),'o-r','MarkerSize',10,'LineWidth',2); %wake vs LOC
%         plot(squeeze(normMatrix(2,3,:)),'o-m','MarkerSize',10,'LineWidth',2); %sed vs LOC
%         legend({'wave vs. sed'; 'wake vs LOC'; 'sed vs. LOC'});
%         ax = gca;
%         ax.LineWidth = 1;
%         ax.XLabel.String = 'Subject #';
%         ax.YLabel.String = 'd';
%         ax.Title.String = 'Anesthesia';
%         ax.XTick = 1:nPatients;
%         ax.XLim = xLim;
%         ax.YLim = yLim(iBand,:);
%         ax.FontSize = 14;
% 
%         subplot(1,2,2)
%         plot(squeeze(normMatrix(4,5,:)),'o-b','MarkerSize',10,'LineWidth',2); %W vs N1
%         hold on
%         plot(squeeze(normMatrix(4,6,:)),'o-r','MarkerSize',10,'LineWidth',2); %W vs N2
%         plot(squeeze(normMatrix(5,6,:)),'o-m','MarkerSize',10,'LineWidth',2); %N1 vs N2
%         legend({'W vs N1'; 'W vs N2'; 'N1 vs N2'});
%         ax = gca;
%         ax.LineWidth = 1;
%         ax.XLabel.String = 'Subject #';
%         ax.YTickLabel = [];
% %         ax.YLabel.String = 'd';
%         ax.Title.String = 'Sleep';
%         ax.XTick = 1:nPatients;
%         ax.XLim = [0.5 nPatients+0.5];
%         ax.YLim = yLim(iBand,:);
%         ax.FontSize = 14;
%         saveas(withinCompFig,[outPath figName]);
%     end
% end
