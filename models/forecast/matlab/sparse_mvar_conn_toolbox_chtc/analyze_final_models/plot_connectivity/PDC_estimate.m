%% 
addpath('W:\code\Chris\gitUploads\ecog_connectivity\3rdParty\eMVAR\functions')

%% Load errorsByVars
Am=errorsByVars(1).lastFold_params;
Su=errorsByVars(1).trainSet_errCov{1};
nfft=20; % number of frequency bins
fc=250; % sample frequency

%% PDC
 [dc,dtf,pdc,gpdc,coh,pcoh,pcoh2,h,s,pp,f] = fdMVAR(Am,Su,nfft,fc);
 figure
 for subInd = 1:nfft
     subtightplot(1,nfft,subInd)
     imagesc(real(pdc(:,:,subInd).^2))
 end

%% MVAR absolute weights by ROI
nCh=size(Am,1);
modelOrder=size(Am,2)/nCh;
connMatrix=zeros(nCh,nCh);
for fromCh=1:nCh
    for toCh=1:nCh
        varWeights=sum(abs(Am(fromCh,toCh:nCh:end)));
        connMatrix(fromCh,toCh)=varWeights;
    end
end
 myLabel = cell(length(connMatrix));
for i = 1:length(connMatrix)
  myLabel{i} = num2str(round(1000000*rand(1,1)));
end

% Create custom colormap
figure;
myColorMap = lines(length(connMatrix));
circularGraph(connMatrix,'Colormap',myColorMap,'Label',myLabel);

