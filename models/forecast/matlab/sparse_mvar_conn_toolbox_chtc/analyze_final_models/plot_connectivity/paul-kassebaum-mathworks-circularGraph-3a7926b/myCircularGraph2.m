function weights=myCircularGraph2(THRESH,weights,labels,myColorMap,maxVal)

weights(weights<THRESH)=0;% plot only connections that will be retained
weightsVec=weights(:);
weightsVec=weightsVec(weightsVec~=0);
G=digraph(weights,labels);
G.Edges.Weight=weightsVec;
weights=weights./maxVal; % normalize to maxVal across conds within single band

%rep
% % % orig_weightsFig=figure;
% % % orig_weights=orig_weights./maxVal;
% % % orig_weights(orig_weights==0)=.000001;
% % % orig_weightsVec=orig_weights(:);
% % % orig_G=digraph(orig_weights,orig_labels); 
% % % orig_G.Edges.Weight=orig_weightsVec;

M=size(weights,1);
idx=eye(M,M);

%% begin plot
% figure(weightsFig);
p=plot(G,'w');

%% reorder nodes to match hierarchy
layout(p,'circle')

%% remove weak/non-existent connections
disp(['THRESH=' num2str(THRESH)])
[rows, cols] = ind2sub(size(weights), find(weights<THRESH & weights > 0));
T=digraph(rows,cols);
T=T.addnode(M-size(T.Nodes,1));
highlight(p,T,'EdgeColor','w');

%rep
% % % figure(orig_weightsFig);
% % % orig_p=plot(orig_G);
% % % THRESH=.01;
% % % [row, col] = ind2sub(size(orig_weights), find(orig_weights<THRESH));
% % % T=digraph(row,col);
% % % highlight(orig_p,T,'EdgeColor','w');

% % % % % figure
% % % % % for iColor=1:length(myColorMap)
% % % % %     c=myColorMap(iColor,:);
% % % % %     hold on
% % % % %     plot([1:10],[1:10].*iColor,'color',c)
% % % % % end
% % % % % legend({'HGPM','HGAL','PT','PP','STG','AudRel','PFC','SensMot','Other'})
%% fix distorted edges from removing overlapping edges in previous step
% figure(weightsFig);
[rows, cols] = ind2sub(size(weights), find(weights>THRESH));
% G.Edges.LWidths=7*G.Edges.Weight/max(G.Edges.Weight);
% p.LineWidth=G.Edges.LWidths+1;
maxArrowSize=20;
% % % orig_weights=orig_weights./maxVal;

for iConn=1:length(rows)
    T=digraph(rows(iConn),cols(iConn));
    T=T.addnode(M-size(T.Nodes,1));
    lineWidths=weights(rows(iConn),cols(iConn))*10;
    originNode=rows(iConn);
    edgeColor=myColorMap(labels{originNode});
%     if rows(iConn) ~= cols(iConn)
        try
            highlight(p,T,'EdgeColor',edgeColor,'LineWidth',lineWidths,'ArrowSize',maxArrowSize*(.3+(lineWidths/7)));
        catch why
            keyboard
        end
%     else
%         keyboard
%         highlight(p,T,'EdgeColor',edgeColor,'LineWidth',lineWidths,'ArrowSize',0);
%     end
end
%% color each node with it's color code in the hierarchy
nodes=1:length(weights);
for iNode=nodes
    highlight(p,iNode,'NodeColor',myColorMap(labels{iNode}))
end

