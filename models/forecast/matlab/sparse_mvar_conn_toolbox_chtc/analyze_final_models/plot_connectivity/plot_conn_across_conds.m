function plot_conn_across_conds(pars,batchParams,varCats,connMatrices)

% stgInds=find(strcmp(varCats,'L_STG'));
% for i=stgInds
%     varCats{i}='L_STGP';
% end
% stgInds=find(strcmp(varCats,'R_STG'));
% for i=stgInds
%     varCats{i}='R_STGP';
% end
% varCats(find(strcmp(varCats,'HG')))={'HGPM'};

%% temporarily ignore parcellation of STG since standROI is screwed up due to ECoGChannelMap
% roi_labels(strcmp(roi_labels,'R_STG'))={'R_STGA'};
% roi_labels(strcmp(roi_labels,'L_STG'))={'L_STGA'};

%% set color options
if strcmp(pars.dataType.varSelect,'AUDITORY')
    colorVals=[1,0,0; 1,0,.5; ...
        0,.5,0; 0,.5,.5; ...
        0,0,1];
elseif strcmp(pars.dataType.varSelect,'ALLVARS')
    if length(pars.run.postHoc.conn.finalROIlist)==9
        colorVals=[1,0,0; 1,0,0.3; 1,0,0.6; 1,0,0.9; 0.6,0.6,0; 0,1,0; 0,0,1; 0,1,1; 0.7,0.7,0.7];
    else
        keyboard % add more colors
    end
else 
    keyboard % add code?
end

%% Assign channels to ROIs
varInfo=batchParams.varInfo;

%% add quick fixes to varInfo and varCats s.t. their lables match ROI_table.oldROI pattern
varCats(find(strcmp(varCats,'L_HG')))={'L_HGPM'};
varCats(find(strcmp(varCats,'R_HG')))={'R_HGPM'};
[varInfo(strcmp({varInfo.varCat},'L_HG')).varCat]=deal('L_HGPM');
[varInfo(strcmp({varInfo.varCat},'R_HG')).varCat]=deal('R_HGPM');
varCats(find(strcmp(varCats,'L_STSL')))={'L_STS'};
varCats(find(strcmp(varCats,'R_STSL')))={'R_STS'};
varCats(find(strcmp(varCats,'L_STSU')))={'L_STS'};
varCats(find(strcmp(varCats,'R_STSU')))={'R_STS'};
[varInfo(strcmp({varInfo.varCat},'L_STSL')).varCat]=deal('L_STS');
[varInfo(strcmp({varInfo.varCat},'R_STSL')).varCat]=deal('R_STS');
[varInfo(strcmp({varInfo.varCat},'L_STSU')).varCat]=deal('L_STS');
[varInfo(strcmp({varInfo.varCat},'R_STSU')).varCat]=deal('R_STS');
%% Clean up mistake (used anatomical subdivisions on older models)
if strcmp(pars.dataType.catType,'anatROIs')
    %% If using older models (which grabbed anat regions during PCA process), need to make sure there's a consistent 9way-newROI that belongs to MFG; 
    % patient372L's MFG was split up based on newROIs into both MFGpf and PMC;
    % this means that its 9-way categorization belongs to both PFC (MFGpf) and Other
    % (PMC)
% % %     oldRoiNames={varInfo.varCat};
% % %     newRoiNames={varInfo.newROI};
% % %     mfgInds=contains(oldRoiNames,'MFG'); % replace contains with strcmp?
% % %     newROIsForMFG=newRoiNames(mfgInds);
% % %     % assign final new ROI based on whichever one is most common
% % %     uniqueNewRoisForMFG=unique(newROIsForMFG);
% % %     finalRoi='';
% % %     highCt=0;
% % %     for iMFGroi=1:length(uniqueNewRoisForMFG)
% % %         roiCt=sum(contains(newROIsForMFG,uniqueNewRoisForMFG{iMFGroi}));%keyboard % replace contains with strcmp?
% % %         if roiCt > highCt
% % %             highCt=roiCt;
% % %             finalRoi=uniqueNewRoisForMFG{iMFGroi};
% % %         end
% % %     end
% % %     mfgInds=find(mfgInds);
% % %     for mfgInd=mfgInds
% % %         varInfo(mfgInd).newROI=finalRoi;
% % %     end
    %% Replace STS with non-subdivisions in the original ROI labels assigned to channel matrix modeled
% % %     stsInds=find(contains(varCats,'STS'));%keyboard % replace contains with strcmp?
% % %     if ~isempty(find(abs(diff(stsInds))>1))
% % %         keyboard % means that there are 2/more subdivisions of STS... which means we may want to consider leaving the subdivision
% % %     end
% % %     for iRoi=stsInds
% % %         if contains(varCats{iRoi},'STS')&& length(varCats{iRoi}) > 5 %keyboard % replace contains with strcmp?
% % %             varCats{iRoi}=varCats{iRoi}(1:5);
% % %         end 
% % %     end
    %% Also update varInfo anat column s.t. STS no longer has subdivisions listed
% % %     stsInds=find(contains({varInfo.oldROI},'STS'));%keyboard % replace contains with strcmp?
% % %     for stsInd=stsInds
% % %         varInfo(stsInd).AnatReg='STS';
% % %     end
end
%% Replace STG with subdivisions
% % % if pars.run.postHoc.conn.useAnatSubdivs
% % %     oldRoiNames={varInfo.varCat};
% % % else
% % %     oldRoiNames={varInfo.oldROI};
% % % end

% % % if sum(strcmp(oldRoiNames,'STGP')) == 0 && sum(strcmp(oldRoiNames,'STGM')) == 0 && sum(strcmp(oldRoiNames,'STGA')) == 0 
% % % %     if pars.run.postHoc.conn.useAnatSubdivs
% % % %         keyboard
% % % %     end
% % %     anatROINames={varInfo.varCat};
% % %     if strcmp(pars.dataType.dataID,'patient376R') || strcmp(pars.dataType.dataID,'patient369LR') || strcmp(pars.dataType.dataID,'patient403L') || strcmp(pars.dataType.dataID,'patient423L')
% % %         stgInds=find(contains(oldRoiNames,'STG')); %keyboard % replace contains with strcmp?
% % %         if isempty(stgInds)
% % %             keyboard
% % %         else
% % %             for stgInd=1:length(stgInds)
% % %                 varInfo(stgInds(stgInd)).oldROI=anatROINames{stgInds(stgInd)};
% % %             end
% % %         end
% % %         oldRoiNames={varInfo.oldROI};
% % %         if sum(strcmp(oldRoiNames,'STGP')) == 0 && sum(strcmp(oldRoiNames,'STGM')) == 0 && sum(strcmp(oldRoiNames,'STGA')) == 0 
% % % %             keyboard % wtf, fix didn't work?
% % %             stgInds=find(strcmp(oldRoiNames,'STG'));
% % %             for i=stgInds
% % %                 varInfo(stgInds(stgInd)).oldROI='STGP';
% % %             end
% % %         end
% % %     else
% % %         keyboard % I think this only occurs for 376R and 369LR
% % %     end
% % % end

%% reduce batchParams to PCA representation & create map of weight matrix denoting order of roi_labels
[varInfo,varOrderMap]=toVirtChForm(pars,varCats,varInfo);

%% arrange rows of varInfo to match organization of
% roi_labels s.t. channel indices are properly marked to match
% roi_labels later on (which controls initial organization of final
% connectivity weights between channels/roi_labels) before we sort it to
% match hierarchical representation
varInfo=orderVars(varCats,varInfo);

%% populate majMinFieldNamesMap, track LR_rois
[LR_rois,majMinFieldNamesMap]=popMajMinFieldNamesMap(pars,varInfo);

%% Compare ROIs with ROI list from ROI_tableRead. Note that to exclude
% roi_labels from the analysis, just need to omit from the list in ROI_tableRead.
electrodeFilePath=[pars.dirs.electrodeSpreadsheet filesep];
electrodeFile='LGD electrodes for analysis - sorted by ROI.xlsx';
electrodeSheet='ROI_table';
[ROI_table]=ROI_tableRead(electrodeFilePath,electrodeFile,electrodeSheet);
if strcmp(pars.dataType.catType,'anatROIs')
    % these regions were not subdivided based on anatROIs. Change table to
    % reflect anat's version of minor ROIs, where relevant (where
    % discrepancies exist between catTypes)
    if sum(strcmp(varCats,'L_MTGP'))>0 || sum(strcmp(varCats,'L_MTGM'))>0 ...
        || sum(strcmp(varCats,'L_MTGA'))>0 
       sum(strcmp(varCats,'R_MTGP'))>0 || sum(strcmp(varCats,'R_MTGM'))>0 ...
        || sum(strcmp(varCats,'R_MTGA'))>0 
        keyboard
    end
    if sum(strcmp(varCats,'L_MTGpf'))>0 || sum(strcmp(varCats,'R_MTGpf'))>0
        keyboard
    end
    oldRoiNum=ROI_table.oldROINum;
    ROI_table.oldROI(strcmp(ROI_table.oldROI,'MTGP'),:)={'MTG'};
    ROI_table(strcmp(ROI_table.oldROI,'MTGM'),:)=[];
    ROI_table(strcmp(ROI_table.oldROI,'MTGA'),:)=[];

    ROI_table.oldROI(strcmp(ROI_table.oldROI,'MFGpf'),:)={'MFG'};

    if sum(diff(oldRoiNum))==length(oldRoiNum)-1
        ROI_table.oldROINum(:)=(1:size(ROI_table,1));
    else
        keyboard % if diffs arent' by 1, then need more sophisticated code to adjust oldROINum after removing some rows from table
    end

    roiIndJump=find(diff(oldRoiNum)>1);
    if length(roiIndJump) > 1
        keyboard
    else
        if ~isempty(roiIndJump)
            keyboard % remind myself why I have this below line... is roiIndjUmp ever not empty? becuz if it's always empty, there's no point in having the below line of code
        
            ROI_table.oldROINum(roiIndJump+1:end)=ROI_table.oldROINum(roiIndJump+1:end)-2;
        end
    end
end
%% Find which ROIs in dataset match list of ROIs we're especially interested in (documented in ROI_table)
tableROIsFound={};
minorROIs=keys(majMinFieldNamesMap)';
if length(minorROIs) ~= length(unique(varCats))
    % find setdiff
    minorROIs_minorOnly={};
    for i=1:length(minorROIs)
        roi=minorROIs{i};
        unders=strfind(roi,'_');
        minorROIs_minorOnly{i}=roi(unders(1)+1:end);
    end
    [uniqueList,~,uniqueNdx] = unique(minorROIs_minorOnly);
    N = histc(uniqueNdx,1:numel(uniqueList));
    dupNames = uniqueList(N>1);
    dupNdxs = arrayfun(@(x) find(uniqueNdx==x), find(N>1), ...
        'UniformOutput',false);
    dupNames
    keyboard % wtf
end
roiCt=1;
sumLRreps=0;
for iROI=1:size(ROI_table,1)
    % check if ROI has L/R repeat
% % %     lr_rep=false;
% % %     for repRoi=1:length(LR_rois)
% % %         if strcmp(ROI_table.oldROI{iROI},LR_rois{repRoi})
% % %             lr_rep=true;
% % %             sumLRreps=sumLRreps+1;
% % %         end
% % %     end
% % %     if ~lr_rep
%         if strcmp(pars.dataType.catType,'anatROIs')
%             if 
%         keyboard % replace contains with strcmp?
    for side={'L','R'}
        newField=[ROI_table.newROI{iROI} '_' side{1} '_' ROI_table.oldROI{iROI}];
        if sum(contains(minorROIs,newField)) > 0
            tableROIsFound{roiCt}=newField;
            roiCt=roiCt+1;
        end
    end
% % %     else
% % %         for side={'L','R'}
% % %             newField=[ROI_table.newROI{iROI} '_' side{1} ROI_table.oldROI{iROI}];
% % % %             keyboard % replace contains with strcmp?
% % %             if sum(contains(minorROIs,newField)) > 0
% % %                 ROIsInTable{roiCt}=newField;
% % %                 roiCt=roiCt+1;
% % %             end
% % %         end
% % %     end
end
% % % if sumLRreps ~= length(LR_rois)
% % %     keyboard % wtf
% % % end

%% check to see if minorROIs contains any fieldnames not present in ROI_table (a few areas purposely excluded: Other_SubInn, Other_Put?)
allFound=true;
for roiInd=1:length(minorROIs)
    roiToFind=minorROIs{roiInd};
    foundRoi=false;
    for roiInd2=1:length(tableROIsFound)
        newField=tableROIsFound{roiInd2};
        if strcmp(roiToFind,newField)
            foundRoi=true;
        end
    end
    if ~foundRoi
        allFound=false;
        disp(['ROI excluded from final excel table, but included in model: ' roiToFind])
    end
end
if (length(tableROIsFound) > length(minorROIs)) && strcmp(pars.dataType.varSelect,'ALLVARS')
    keyboard % ROIsInTable should never be longer than minorROIs unless processing auditory data
end
if ~allFound
    ROIsToProcess=intersect(minorROIs,tableROIsFound);
elseif length(minorROIs) ~= length(tableROIsFound)
    keyboard
else 
    ROIsToProcess=minorROIs;
end

%% order roi_labels by hierarchy based on excel table
nROIsToPlot=length(ROIsToProcess);
sortIndex=NaN(1,nROIsToPlot);
for iROI=1:nROIsToPlot
    try
        sortIndex(iROI)=ROI_table.oldROINum(contains(ROIsToProcess,tableROIsFound{iROI})); %keyboard % replace contains with strcmp?
    catch why
        keyboard
    end
end
ROIsToProcess=ROIsToProcess(sortIndex);

%% Now do plotting
condsToPlot=batchParams.conds; % condsToPlot replaced statesToPlot
maxVals=ones(length(FreqBands.Names),1)*-1; % find max value of connectivity across all conditions w/ given freq. band
checkPcaStuffFig=figure;
pcaStuffBool=true;
plotCol=0;
for iBand=1:length(FreqBands.Names)
    plotCol=1;%plotCol+1;
    thisBand=FreqBands.Names{iBand};
    %% find reasonable maxVal for this patient's power spectrum
    measTypes={'bPDC','bDC'};
    for measTypeInd=1:length(measTypes)
        measType=measTypes{measTypeInd};
        allWeights=[];
        maxVal=0;
        if pars.run.postHoc.conn.plotChordConn% && iBand == 1
            chordFigName=[pars.dataType.dataID ' - ConnChordPlot - ' thisBand];
            imageSz=[0.73000, 0.0370, .3000, 0.9630]; %[0 0 1 1] for full screen
            chordFig=figure('Name',chordFigName,'units','normalized','outerposition',imageSz);
            weightsFigName=[pars.dataType.dataID ' - weightsFig - ' thisBand];
            weightsFig=figure('Name',weightsFigName,'units','normalized','outerposition',imageSz);
        end
        for iCond=1:length(condsToPlot)
            cond=batchParams.conds{iCond};
            if pars.run.postHoc.conn.skipCond.bool && strcmp(cond,pars.run.postHoc.conn.skipCond.name)
                continue
            elseif pars.run.postHoc.conn.oneCond.bool && ~strcmp(cond,pars.run.postHoc.conn.oneCond.name)
                continue
            end
            weights=squeeze(connMatrices.(cond).(measType)(iBand,:,:));
            if sum(isnan(weights(:))) > 0
                keyboard
            end
            allWeights=vertcat(allWeights,weights(:));
            weights_noSC=weights;
            for iROI=1:length(weights)
                weights(iROI,iROI)=0;
            end
            maxVal=max(weights(:));
            if maxVals(iBand) < maxVal
                maxVals(iBand)=maxVal;
            end
        end
        for iCond=1:length(condsToPlot)
            cond=batchParams.conds{iCond};
            %% estimate connectivity across roi_labels
            if pars.run.postHoc.conn.skipCond.bool && strcmp(cond,pars.run.postHoc.conn.skipCond.name)
                continue
            elseif pars.run.postHoc.conn.oneCond.bool && ~strcmp(cond,pars.run.postHoc.conn.oneCond.name)
                continue
            end
            %% standardize format of weights matrix (order roi_labels)
            newWeights=nan(nROIsToPlot,nROIsToPlot);
            for iROI=1:nROIsToPlot
                thisROI=ROIsToProcess{iROI};
                for jROI=1:nROIsToPlot
                    thatROI=ROIsToProcess{jROI};
                    thisLogical=varOrderMap(majMinFieldNamesMap(thisROI));
                    thatLogical=varOrderMap(majMinFieldNamesMap(thatROI));
                    if length(thisLogical) ~= size(connMatrices.(cond).(measType)(iBand,:,:),2)
                        keyboard
                    elseif length(thatLogical) ~= size(connMatrices.(cond).(measType)(iBand,:,:),2)
                        keyboard
                    end
                    try
                        newWeights(iROI,jROI)=connMatrices.(cond).(measType)(iBand,thisLogical,thatLogical);%tempMn=wPLIMn.(thisName).(thisBand).(thisState)(thisLogical,thatLogical);
                    catch why
                        keyboard
                    end
                end
            end
            newConnMatrices.(cond).(measType)(iBand,:,:)=newWeights;
            labels=[];
            myColorMap=containers.Map;
            for iROI=1:length(ROIsToProcess)
                thisROI=ROIsToProcess{iROI};
                underscores=strfind(thisROI,'_');
                label=thisROI(underscores(1)+1:end);%(strfind(thisROI,'_')+1:end);
                label=label([1,3:end]);
                labels{iROI}=label;
                myColorMap(labels{iROI})=colorVals(strcmp(pars.run.postHoc.conn.finalROIlist,thisROI(1:strfind(thisROI,'_')-1)),:);%keyboard % replace strcmp with strcmp?
            end
            weights=squeeze(newConnMatrices.(cond).(measType)(iBand,:,:));
            if size(weights,1) ~= length(labels)
                keyboard
            end
            figure(chordFig);
            nCols=1;%length(measTypes);%1;%length(FreqBands.Names);%length(measTypes);
            plotCol=1;%measTypeInd;%plotCol;
            nRows=length(condsToPlot);
            plotRow=iCond;
            plotInd=(nCols*plotRow)-(nCols-plotCol);
            subplot(nRows,nCols,plotInd);
            
            %% 1) get rid of self-connections for now
            for iRoi=1:size(weights,1)
                weights(iRoi,iRoi)=0;
            end
            figure(weightsFig)
            subplot(nRows,nCols,plotInd);
            imagesc(weights)
            
            %% 2) establish weight thresh
            weightsVec=weights(:);
            weightsVec=weightsVec(weightsVec~=0);
            THRESH=mean(weightsVec)+(std(weightsVec)*0);
%             weights(weights<THRESH)=0;
            if iBand==1 && measTypeInd==1 && iCond==1
                disp('WARNING! Transposing connectivity matrix to reflext from-to cnnections in correct/intuitive manner')
            end
            weights=weights';
            %% myCircularGraph_copy
            figure(chordFig);
%             myCircularGraph_withSelfConn(weights,...
%                 'Colormap',numericColorMap,...
%                 'Label',labels,...
%                 'MaxVal',maxVals(iBand));
            %% myCircularGraph2
%             figure(checkPcaStuffFig)
%             plotInd=(2*iBand)-(2-1);
%             subtightplot(length(FreqBands.Names),2,plotInd)
%             THRESH=0;
%             figure
            weights=myCircularGraph2(THRESH,weights,labels,myColorMap,maxVals(iBand));
            singleFig=figure;
            weights=myCircularGraph2(THRESH,weights,labels,myColorMap,maxVals(iBand));
            outPath=[pars.dirs.baseResultsDir filesep 'finalNetworks' filesep];
            saveas(singleFig,[outPath measType '_' chordFigName '_' cond '.png']);
            savefig(singleFig,[outPath measType '_' chordFigName '_' cond '.fig']);
            close(singleFig)
%             if pcaStuffBool
%                 ax=gca;
%                 if iCond == 1
%                     t=title([measType]);
% %                 else
% %                     t=text(mean(ax.XLim),ax.YLim(2)*1.1,cond);
%                 end
%                 t.FontSize=16;
%                 t.FontWeight='bold';
%                 t.HorizontalAlignment='center';
%             end
        end
        if ~pars.run.postHoc.conn.skipCond.bool && ~pars.run.postHoc.conn.oneCond.bool
            outPath=[pars.dirs.baseResultsDir filesep 'finalNetworks' filesep];
    %         set(gcf, 'Position',  [100, 50, 360, 950])
            saveas(chordFig,[outPath measType '_' chordFigName '.png']);
            savefig(chordFig,[outPath measType '_' chordFigName '.fig']);
            close(chordFig)
            saveas(weightsFig,[outPath measType '_' weightsFigName '.png']);
            savefig(weightsFig,[outPath measType '_' weightsFigName '.fig']);
            close(weightsFig)
        end
    end
end%Loop over FreqBands.Names
% % % if pcaStuffBool
% % %     keyboard % fix below hardcoded dirs. also add commet to remind me why I'm doing this here?
% % %     figure(checkPcaStuffFig)
% % %     if pars.cv.genDimRed.ch.PCA.run
% % %         suptitle(['pca' num2str(pars.cv.genDimRed.ch.PCA.perc*100)])
% % %         saveas(checkPcaStuffFig,['M:\PassiveEphys\Analysis Results\Chris\ecog_connectivity\PCA\backProjectionErrCheck\pca' num2str(pars.cv.genDimRed.ch.PCA.perc*100) '.png'])
% % %         savefig(checkPcaStuffFig,['M:\PassiveEphys\Analysis Results\Chris\ecog_connectivity\PCA\backProjectionErrCheck\pca' num2str(pars.cv.genDimRed.ch.PCA.perc*100) '.fig'])
% % %     else
% % %         suptitle(['noPca'])
% % %         saveas(checkPcaStuffFig,['M:\PassiveEphys\Analysis Results\Chris\ecog_connectivity\PCA\backProjectionErrCheck\noPca.png'])
% % %         savefig(checkPcaStuffFig,['M:\PassiveEphys\Analysis Results\Chris\ecog_connectivity\PCA\backProjectionErrCheck\noPca.fig'])
% % %     end
% % % end
% end%Loop over measureTypes


