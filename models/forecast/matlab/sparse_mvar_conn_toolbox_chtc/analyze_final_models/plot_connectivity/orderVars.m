function varInfo=orderVars(varCats,varInfo)
varInfo_ordered=varInfo;
varInfo_ordered(:)=[];
unique_vars=unique(varCats);
for pcaVarInd=1:length(unique_vars)
    var=unique_vars{pcaVarInd};
    varInds=strcmp(varCats,var);
    curInds=contains({varInfo.varCat},var);
    if sum(curInds) == 0 || sum(varInds) ~= sum(curInds)
        keyboard% wtf
    end
    varInfo_ordered(find(varInds))=varInfo(find(curInds));
end
varInfo=varInfo_ordered;
