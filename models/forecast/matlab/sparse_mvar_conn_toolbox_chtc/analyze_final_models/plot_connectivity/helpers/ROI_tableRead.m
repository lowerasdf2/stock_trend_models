function [ROI_table] = ROI_tableRead(electrodeFilePath,electrodeFile,electrodeSheet)
computer = 'Mac';
if strcmp(computer,'Mac')
    computerSpecPrefix = '/Users/bankslaptop/Box Sync/PHI_Data-A5309-Banks/Banks-UIowa-LDS/My documents/';
else
    computerSpecPrefix = 'D:\Box Sync\PHI_Data-A5309-Banks\Banks-UIowa-LDS\My documents\';
end

if ~exist('electrodeFilePath','var')
    electrodeFilePath = [computerSpecPrefix 'Data and analysis' filesep ...
        'ECoG data' filesep 'Electrode MNI coordinates and ROI' filesep];
end

if nargin==0
    electrodeFile = 'LGD electrodes for analysis - sorted by ROI.xlsx';
    electrodeSheet = 'ROI_table';
end

chanFileName = [electrodeFilePath electrodeFile];
ROISheetName = electrodeSheet;

ROI_table = readtable(chanFileName,'Sheet',ROISheetName);
varNames_Read = ROI_table.Properties.VariableNames;