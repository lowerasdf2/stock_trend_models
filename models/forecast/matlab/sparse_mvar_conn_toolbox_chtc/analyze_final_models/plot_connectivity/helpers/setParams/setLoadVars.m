function setLoadVars(pars)
%% Set static cv params that stay the same for all runType cases
pars=setStaticVars(pars); % vars that are fairly static across model iterations

%% dataType vars (e.g. OR vs. CRU, ALL vs. RESP)
pars=setPars_dataTypeStrs(pars,varargin);
pars=setPars_varSelect(pars,varargin);

%% Set PCA params
pars=setPars_PCA(pars,varargin);

%% Set CV_p or CV_lambda params
if pars.cv.lambda.run
    %% Set method for predetermining best model order prior to CV_lambda; methods include: {'CV_P_LASSO','CV_P','Manual'};
    pars=setPars_lambda_modelOrderChoice(pars,varargin);
    %% Set 'load' params, transfer to necessary 'run' params
    pars=setPars_lambda_setLoadParams(pars,varargin);
    %% new lasso params to use during CV_lambda procedure
    pars=setPars_lambda_setNewParams(pars,varargin);
else
    %% set general CV_p params
    pars=setPars_modelOrder(pars,varargin);
    %% set CV_p w/ lasso params
    pars=setPars_modelOrder_lasso(pars,varargin);
end

%% Set ideal model orders if they are already known (skip CV)
if sum([pars.cv.lambda.run, pars.cv.p.run]) == 0
    pars.cv.run=false;
    crash; % no reason to reach this code as of yet
    pars.modelOrderMap=containers.Map;
    pars.lambdaMap=containers.Map;
    pars.modelOrderMap('patient372L')=30;
    pars.modelOrderMap('patient394R')=624;
    pars.modelOrderMap('patient369LR')=90;
    pars.modelOrderMap('patient376R')=36;
    pars.modelOrderMap('patient384B')=392;
    pars.modelOrderMap('patient399R')=512;
else
    pars.cv.run=true;
end
