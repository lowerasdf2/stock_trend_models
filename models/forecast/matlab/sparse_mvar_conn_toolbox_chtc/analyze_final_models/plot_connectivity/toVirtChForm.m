function [varInfo,varOrderMap]=toVirtChForm(pars,varCats,varInfo)

varOrderMap=containers.Map;
% first create field for side_roi
% % % if pars.run.postHoc.conn.useAnatSubdivs
% % % else
% % %     oldRoiNames={varInfo.oldROI};
% % % end
% % % stgInds=find(strcmp(varCats,'STG'));

% % % for i=stgInds
% % %     varCats{i}='STGP';
% % % end
% % % stgInds=find(strcmp(varCats,'STG'));
% % % for i=stgInds
% % %     varCats{i}='STGP';
% % % end
% % % lOrR={varInfo.side};
% % % varCats=varCats;%cellfun(@(x,y) [x '_' y],lOrR,varCats,'un',0);
% % % [varInfo(:).sidedOldROINames]=varCats{:};
unique_vars=unique(varCats);
for pcaVarInd=1:length(unique_vars)
    pcaVar=unique_vars{pcaVarInd};
    pcaVarInds=strcmp(varCats,pcaVar);%keyboard % replace contains with strcmp?
    varLogicalInds=zeros(length(unique_vars),1);
    varLogicalInds(pcaVarInd)=1;
    varOrderMap(pcaVar)=logical(varLogicalInds);
    if ~isempty(find(abs(diff(pcaVarInds)) ~= 1 & abs(diff(pcaVarInds)) ~= 0,1))
        keyboard % checks that pca ROIs are listed in order
    end
    % remove extra entries in batchParams representing original number
    % channels
% % %     varCats={varInfo.sidedOldROINames};
    origVarInds=find(strcmp({varInfo.varCat},pcaVar));%keyboard % replace contains with strcmp?
    if isempty(origVarInds)
        keyboard
    end
    delRoiInds=origVarInds(sum(pcaVarInds)+1:end);
    varInfo(delRoiInds)=[];
end   
