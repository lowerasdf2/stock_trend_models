function batch_sparse_mvar_conn_analysis()
clear all
close all

%% Restore path to remove effects from other computational wizards working from Odin
% restoredefaultpath
%% Add local path that leads to rest of the code
pars.dirs.localPath='C:\Users\qualiaMachine\Documents\code\stock_trend_models\models\forecasting_models\VAR\sparse_mvar_conn_toolbox_chtc\';
addpath(genpath([pars.dirs.localPath]));

%% set masterPars.config settings: holds options for running different analyses, list of dirs needed, and list of dataType options to iterate through
pars.run.bools.createDag=false; % create dag and everything necessary to run dag
pars.run.bools.localDag=true; % run local version of dag
pars.run.bools.postHoc=false; % measure connectivity from model params
pars.run.bools.createPCAcoefsAndScoresOnly=false; % cheap way to output coefs/scores after PCA is run so I can backproject from PCA space back to standard space
pars.run.bools.measPCAerr=false; % plot and inspect loss of info when using PCA to model connectivity (use backprojection and also compare datasets that do/don't use PCA); use patient369LR-AUDITORY for comparison
pars.run.bools.addLabelToDataFile=false; % flag for modifying dag input files previously created by adding one additional variable to data input files
pars.run.bools.nonCHTCcode=false; % runs old code to fit model that is not segmented to run in chtc fashion. currently only way to run w/out lasso on.
pars.run.bools.fixCorruptedDagFiles=false; % boolean to reruns job(s) of dag to replace corrupted dag file(s) that is/are preventing dag from continuing. hopefully I won't have to use this after updating dag scripts to check for corrupted files before job completion.

%% Run nestedCV or manually set MO and then CV_lambda (this param only interacts with setModelOrder function (one of the main compiled programs the dag runs)
pars.cv.nested.bool=true; % set to true to run both CV_mo and CV_lambda (2 layers of CV)

%% If not running the above "nestedCV" (this term is actually incorrectly used here and I will fix later), choose which hyperparam to optimize while ignoring the other
if ~pars.cv.nested.bool
    keyboard% set one of below 2 params to true
    pars.cv.lambda.run=false;
    pars.cv.p.run=true; 
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    pars.cv.p.dimRed.ch.lasso.bool=false; % TO-DO: add otpion to run CV_mo w/ manually set sparsity levels. Then could potentially set this to true (but make sure it doesn't fuck up downstream stuff).
    if pars.cv.lambda.run && pars.cv.p.run
        keyboard % only one can be true
    elseif pars.cv.lambda.run
        pars.cv.nested.manualMO=20; % reminder to check this setting
        pars.cv.lambda.modelOrderChoice='MANUAL'; %keywords={'MANUAL','CV_P_LASSO','CV_P'};
        disp(['USING MANUALLY SET MODEL-ORDER=' num2str(pars.cv.nested.manualMO) ' TO FIT FINAL SPARSE MODEL'])
    elseif pars.cv.p.run
        disp(['Optimizing model-order hyperparam using fully connected models only (TO-DO: add option to manually set sparsity).'])  
    end
else
    pars.cv.p.dimRed.ch.lasso.bool=true;
    pars.cv.lambda.run=true;
    pars.cv.p.run=true; 
    pars.cv.lambda.modelOrderChoice='CV_P_LASSO'; %keywords={'MANUAL','CV_P_LASSO','CV_P'};
end
if pars.cv.p.run || pars.cv.lambda.run
    pars.cv.run=true;
end
%% USE MIN PARAMS is set to true for debugging purposes. Uses all of the min params to run nested CV very quicikly on dataset that has only 2 channels
pars.dataType.USE_MIN_PARAMS=false; % automatically sets nestedCV process to run w/ fewer params (helpful for testing new Condor features, testing effectiveness of PCA, and determine if epochLength is a significant factor in connectivity results)

%% set dirs
pars=setDirs(pars);

%% set path
setpath_main_fit_data(pars);

%% set pars.modelParams pars, pars.shortenSeg pars, and pars.patients
% These are params that tend to vary across different simulations/model
% tests. USE_MIN_PARAMS will override some of these if it is set to true
% pars.modelPars.
% set custom params
%% Note: pars.dataLoops differs from pars.dataType fields in that pars.dataType does not receive list of params to iterate through
% holds lists to be iterated through
if pars.dataType.USE_MIN_PARAMS
    processNewData=true;
    pars.dataLoops.dataIDs={'patient394R'};%{'patient394R'};% use this guy for testing new versions of stuff (has the least amount of data)
    pars.dataLoops.varSelects={'AUDITORY'};
    pars.dataLoops.globalDataCats={'OR-RS'};
    pars.dataLoops.epochLens=TrialProperties.Length; % specified in seconds 
    if processNewData
        pars.dataLoops.catTypes={'standROIs'};
    else
        pars.dataLoops.catTypes={'anatROIs'};
    end
    pars.dataType.sampleRate=125; % use >= 101 to retain ability to measure gamma interactions. Use 125 so that downsample is an integer (sample rate is 1000 Hz)
    %
    pars.dataType.shortenSeg.bool=true; % rather than analyze connectivity w/in each recording block (which can have mutliple OAAS readings), split up recording blocks into minute-long segments to derive models from each minute of data.
    pars.dataType.shortenSeg.segLen=.1; % length (in mins) of recording period (in minutes) to use for model fitting (gets divided into train/test sets)
    pars.dataType.shortenSeg.startMin=0; % 0=start from beginning of recording period, 1=start from minute1, and so on
    %
    pars.dataLoops.pcaBools=1;
    pars.dataLoops.pcaPercs=.6;
else
    %% Feel free to customize any of these
%     processNewData=true;
%     %     pars.patients={'patient403L','patient409L','patient423L'};%{'patient376R'}; % pars.patients={'patient369LR'}; % use this guy for investigating loss of info when using PCA % patients={'patient372L','patient369LR','patient376R','patient399R','patient400L','patient394R'}; % full list
%     pars.dataLoops.dataIDs={'patient376R','patient369LR','patient372L','patient409L','patient403L','patient423L',};%{'patient376R'}; % pars.patients={'patient369LR'}; % use this guy for investigating loss of info when using PCA % patients={'patient372L','patient369LR','patient376R','patient399R','patient400L','patient394R'}; % full list
%     pars.dataLoops.varSelects={'AUDITORY'}; % 'ALLVARS','AUDITORY'
%     if processNewData
%         pars.dataLoops.catTypes={'standROIs'};
%     else
%         pars.dataLoops.catTypes={'anatROIs'};
%     end
%     %
%     pars.dataType.sampleRate=250;%250;%250; % use 101 to retain ability to measure gamma interactions
%     % epochLens given in seconds
%     pars.dataLoops.epochLens=1;%.33;%.33;%2;%1;%TrialProperties.Length;%[.25,.5,1,2,4,8,16];%TrialProperties.Length;
%     %
%     pars.dataLoops.globalDataCats={'OR-RS'};%{'OR-RS','ENDO'}
%     %
%     pars.dataType.shortenSeg.bool=true; % rather than analyze connectivity w/in each recording block (which can have mutliple OAAS readings), split up recording blocks into minute-long segments to derive models from each minute of data.
%     pars.dataType.shortenSeg.segLen=10/60;%2/60; % length (in mins) of recording period to use for model fitting (gets divided into train/test sets)
%     pars.dataType.shortenSeg.startMin=5.5; % 0=start from beginning of recording period, 1=start from minute1, and so on
%     %
%     pars.dataLoops.pcaBools=1;
%     pars.dataLoops.pcaPercs=[.7];%.8;
    
    %% ALL channels datasets
    processNewData=false;
    pars.dataLoops.dataIDs={'patient423L','patient376R'};%'patient369LR','patient409L',patient372L      ;%{'patient376R'}; % pars.patients={'patient369LR'}; % use this guy for investigating loss of info when using PCA % patients={'patient372L','patient369LR','patient376R','patient399R','patient400L','patient394R'}; % full list
    pars.dataLoops.varSelects={'ALLVARS'}; % 'ALLVARS','AUDITORY'
    if processNewData
        pars.dataLoops.catTypes={'standROIs'};
    else
        pars.dataLoops.catTypes={'anatROIs'};
    end
    %
    pars.dataType.sampleRate=250;%250;%250; % use 101 to retain ability to measure gamma interactions
    % epochLens given in seconds
    pars.dataLoops.epochLens=TrialProperties.Length;%.33;%.33;%2;%1;%TrialProperties.Length;%[.25,.5,1,2,4,8,16];%TrialProperties.Length;
    %
    pars.dataLoops.globalDataCats={'OR-RS'};%{'OR-RS','OR_EV'}
    %
    pars.dataType.shortenSeg.bool=false; % rather than analyze connectivity w/in each recording block (which can have mutliple OAAS readings), split up recording blocks into minute-long segments to derive models from each minute of data.
    pars.dataType.shortenSeg.segLen=2/60; % length (in mins) of recording period to use for model fitting (gets divided into train/test sets)
    pars.dataType.shortenSeg.startMin=0;%5.5; % 0=start from beginning of recording period, 1=start from minute1, and so on
    %
    pars.dataLoops.pcaBools=1;
    pars.dataLoops.pcaPercs=.8;%.8;
    
    %% test data
    processNewData=true;
    pars.dataLoops.dataIDs={'FOREX'}; % patients can be name of specific dataset you're trying to study. Patient for EEG stuff. broadDataCategory_sampleRate for other data types.
    pars.dataLoops.varSelects={'ALLVARS'}; % if using a certain category of all vars in dataset, define here. e.g. subchannel region, only evoked channels, etc.
    pars.dataLoops.catTypes={'catALL'};
     
    pars.dataType.sampleRate=1/60; %minutely. %250;%250; % use 101 to retain ability to measure gamma interactions
    % epochLens given in seconds
    pars.dataLoops.epochLens=5*60;%.33;%2;%1;%TrialProperties.Length;%[.25,.5,1,2,4,8,16];%TrialProperties.Length;
    %
    pars.dataLoops.globalDataCats={'minute'};%{'OR-RS'}
    %
    pars.dataType.shortenSeg.bool=false; % rather than analyze connectivity w/in each recording block (which can have mutliple OAAS readings), split up recording blocks into minute-long segments to derive models from each minute of data.
    pars.dataType.shortenSeg.segLen=30/60; % length (in mins) of recording period to use for model fitting (gets divided into train/test sets)
    pars.dataType.shortenSeg.startMin=0;%5.5; % 0=start from beginning of recording period, 1=start from minute1, and so on
    %
    pars.dataLoops.pcaBools=1;
    pars.dataLoops.pcaPercs=.7;%.8;
    
    %% test data
    processNewData=true;
    pars.dataLoops.dataIDs={'sample_nonStandFormat'}; % patients can be name of specific dataset you're trying to study. Patient for EEG stuff. broadDataCategory_sampleRate for other data types.
    pars.dataLoops.varSelects={'ALLVARS'}; % if using a certain category of all vars in dataset, define here. e.g. subchannel region, only evoked channels, etc.
    pars.dataLoops.catTypes={'catALL'};
     
    pars.dataType.sampleRate=1/(60*60*24); %daily. 
    % epochLens given in seconds
    pars.dataLoops.epochLens=pars.dataType.sampleRate*5;% 5 day epoch %5*60;%.33;%2;%1;%TrialProperties.Length;%[.25,.5,1,2,4,8,16];%TrialProperties.Length;
    %
    pars.dataLoops.globalDataCats={'daily'};%{'OR-RS'}
    %
    pars.dataType.shortenSeg.bool=false; % rather than analyze connectivity w/in each recording block (which can have mutliple OAAS readings), split up recording blocks into minute-long segments to derive models from each minute of data.
    pars.dataType.shortenSeg.segLen=30/60; % length (in mins) of recording period to use for model fitting (gets divided into train/test sets)
    pars.dataType.shortenSeg.startMin=0;%5.5; % 0=start from beginning of recording period, 1=start from minute1, and so on
    %
    pars.dataLoops.pcaBools=1;
    pars.dataLoops.pcaPercs=.7;%.8;
    
    %% PCA comparisons settings
%     pars.patients={'patient369LR'};
%     pars.varSelects={'AUDITORY'}; 
%     pars.epochLen=TrialProperties.Length;
%     pars.dataType.sampleRate=250; % use 101 to retain ability to measure gamma interactions
%     pars.shortenSeg.bool=false; % rather than analyze connectivity w/in each recording block (which can have mutliple OAAS readings), split up recording blocks into minute-long segments to derive models from each minute of data.
%     pars.shortenSeg.segLen=.5; % length of recording period to use for model fitting (gets divided into train/test sets)
%     pars.shortenSeg.startMin=0; % 0=start from beginning of recording period, 1=start from minute1, and so on
%     %
%     pars.pcaBools=1;
%     pars.pcaPercs=.7;
end

%% set pars.createDag pars
if pars.run.bools.createDag || pars.run.bools.localDag
    pars=setCreateDagPars(pars);
end
%% postHoc params
if pars.run.bools.postHoc
    pars=setPostHocPars(pars);
end
%% set pars.run.fixCorruptedDagFiles
if pars.run.bools.fixCorruptedDagFiles
    pars=setCorruptedDagPars(pars);
end

%% Set params for data-processing & modeling
for epochLen=pars.dataLoops.epochLens
    for catType=pars.dataLoops.catTypes
        for dataTypeStr=pars.dataLoops.globalDataCats
            for varSelect=pars.dataLoops.varSelects
                for pcaBool=pars.dataLoops.pcaBools
                    pcaInd=0;
                    for pcaPerc=pars.dataLoops.pcaPercs%[.95]%.9,.95]
                        pcaInd=pcaInd+1;
                        if pcaBool==0
                            pcaPerc=1; % doesn't actually affect anything since PCA isn't run, but makes conceptual sense
                        end
                        if pcaInd > 1 && pcaPerc==1
                            continue
                        end
                        disp(['pcaPerc=' num2str(pcaPerc)])
                        if pars.dataType.USE_MIN_PARAMS
                            p_Ks=2;
                        else
                            p_Ks=5;%5; % customize as you like (I typically use 5 as default for real data)
                        end
                        for p_K=p_Ks%[5]keyboard
                            disp(['p_K=' num2str(p_K)])
                            
                            %% set dataType params
                            pars=setDataTypePars(pars,epochLen,dataTypeStr,varSelect,catType);
                            %% set pca params
                            pars=setPCAparams(pars,pcaBool,pcaPerc);
                            %% Set static cv params that stay the same for typical workflow
                            pars=setStaticVars(pars); % vars that are fairly static across model iterations
                            %% setPars_lambda_setNewParams (not the same as setPars_modelOrder_lasso!)
                            pars=setPars_lambda_setNewParams(pars,p_K); % edit file params if you need to change lasso params
                            %% set general CV_p params
                            pars=setPars_modelOrder(pars,p_K,1);
                            
                            %% get batch params and list of conds (channel info e.g. associated ROIs) in order to extract which conds to run (and load/save data if necessary)
                            [batchParams]=getBatchParams_prepCondData(pars);
                            for dataID=pars.dataLoops.dataIDs%dataIDparams=batchParams
                                dataID=dataID{1};%dataIDparams.dataID;
%                                 if sum(strcmp(pars.dataLoops.dataIDs,dataID)) == 0
                                if sum(strcmp({batchParams.dataID},dataID)) == 0
                                    keyboard
                                else
                                    dataIDparams=batchParams(find(strcmp({batchParams.dataID},dataID)));
                                    if ~strcmp(dataIDparams.dataID,dataID)
                                        keyboard
                                    end
                                end
                                pars.dataType.dataID=dataID;
                                disp([pars.dataType.dataID '...'])
                                
                                %% check if formatted input data has been created (this doesn't refer to epochs file, it refers to 'data that isn't split up yet' file)
                                % For newer analyses, dataFileExists()
                                % function checks that all condData files
                                % exist
                                createInputData=false;
                                if ~dimRedDataFileExists(pars,dataIDparams.conds)
                                    createInputData=true;
                                    if pars.run.bools.postHoc
%                                         keyboard % data should exist for post-hoc analyses
                                        createInputData=false;
                                    end
                                end
                                %% load data, save data
                                if createInputData || pars.run.bools.createDag || pars.run.bools.localDag || pars.run.bools.fixCorruptedDagFiles || pars.run.bools.createPCAcoefsAndScoresOnly || pars.run.bools.nonCHTCcode
                                    %% Creates orig data file used for creation of dataEpochs: Runs PCA, downsample data, rm nans if relevant.
                                    formatData(pars,dataIDparams,createInputData) % runs PCA, creates orig input data files, runs runNonCHTCcode if desired
                                    if pars.run.bools.createPCAcoefsAndScoresOnly
                                        continue
                                    end
                                end
                                condInd=0;
                                for cond=dataIDparams.conds
                                    %% set condition param
                                    cond=cond{1};
                                    condInd=condInd+1;
                                    pars.dataType.condition=cond;
                                    if pars.run.bools.fixCorruptedDagFiles
                                        corruptConds=fields(pars.run.fixCorruptedDagFiles);
                                        if sum(contains(corruptConds,pars.dataType.condition))==0
                                            continue
                                        end
                                    end
                                    disp(['Processing ' pars.dataType.dataID ':' cond '''s data.'])
                                    if pars.run.bools.createDag || pars.run.bools.localDag || pars.run.bools.fixCorruptedDagFiles || pars.run.bools.addLabelToDataFile %|| pars.run.bools.postHoc
                                        try
                                            createDataEpochs(pars);
                                        catch why
                                            keyboard
                                        end
                                    end
                                    %% Calculate optimal model order range to test
                                    if processNewData && ~ pars.dataType.USE_MIN_PARAMS
                                        pars=setModelOrderRange(pars);
                                    elseif ~processNewData && ~pars.dataType.USE_MIN_PARAMS
                                        pars.cv.p.modelOrderRange=2:2:20;
                                    elseif pars.dataType.USE_MIN_PARAMS
                                        pars.cv.p.modelOrderRange=5;
                                    end
                                    %% check/display params
                                    pars=printCheckParsStruct(pars);
                                    %% post-hoc analyses
                                    if pars.run.bools.postHoc
                                        if condInd==1 % because this only needs to run once (script iterates through conditions. calling after conds for loop so that I can call setModelOrderRange, which depends on createDataEpochs)
                                            main_analyze_final_models(pars,dataIDparams)
                                        else
                                            continue
                                        end
                                    end
                                    if pars.run.bools.measPCAerr
                                        if condInd==1
                                            addpath(genpath([pars.dirs.baseCodeDir filesep '..' filesep 'pcaBackProjection']))
                                            close all
                                            measPCAerr(pars,dataIDparams)
                                        else
                                            continue
                                        end
                                    end
                                    % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                 
                                    %% create dag
                                    if pars.run.bools.createDag || pars.run.bools.localDag
                                        addpath(genpath([pars.dirs.baseCodeDir filesep 'chtc' filesep 'prepDags']))
                                        condorUser = 'endemann';
                                        createSubmitFiles=true;
                                        createDagAndSubmitFiles(pars,cond,condorUser,createSubmitFiles);
                                    end

                                    % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %   
                                    %% If replacing corrupted files, check that pars file in pwd matches all settings in corrupted dag folder
                                    if pars.run.bools.fixCorruptedDagFiles
                                        addpath(genpath([pars.dirs.baseCodeDir filesep 'replaceCorruptedFile']))
                                        specDagDir=createFileEnd_fitECoG(pars,1);
                                        if pars.cv.genDimRed.ch.PCA.run
                                            specDagDir=specDagDir(strfind(specDagDir,['PCAAC']):end);
                                        else
                                            keyboard % double check below line works
                                            specDagDir=specDagDir(strfind(specDagDir,['CV' num2str(pars.cv.p.percData*100)]):end);
                                        end
                                        corruptedDagFolder=[pars.dirs.baseCorruptDagDir filesep ...
                                            pars.dataType.varSelect filesep pars.dataType.dataID filesep pars.dataType.condition ...
                                            filesep specDagDir filesep];
                                        dagParsUsed=load([corruptedDagFolder 'pars.mat']);
                                        dagParsUsed=dagParsUsed.pars;
                                        checkFieldsMatch(dagParsUsed,pars) % this will keyboard w/in function if there's a mismatch (so user can check out discrepancy)
                                    end
                    % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %        
                                    %% run local version of pipeline
                                    if pars.run.bools.localDag || pars.run.bools.fixCorruptedDagFiles
                                        %% cd to dag spec folder s.t. multiple instances of this program does not result in overwriting data
                                        dagDir=getDagDir(pars,cond);
                                        cd(dagDir)
                                        %% delete all .mat files in current directory
%                                         matFiles=dir([pwd filesep '*.mat']);
%                                         for iFile=1:length(matFiles)
%                                             delete([pwd filesep matFiles(iFile).name])
%                                         end
                                        %% run
                                        tic
                                        addpath(genpath([pars.dirs.baseCodeDir filesep 'chtc' filesep 'localDag']))
                                        main_fit_data_chtc()
                                        toc
                                        %% delete leftover tar files that mimic transfer protocol on Condor
                                        tarFiles=dir([pwd filesep '*.gz']);
                                        for iFile=1:length(tarFiles)
                                            delete([pwd filesep tarFiles(iFile).name])
                                        end
                                    end
                                end
                            end
                        end
                    end
                end
            end
        end
    end
end







%% SET ALL pars
% % %                 [pars]=setGlobalPars(nan,...
% % %                     pars.dataType.globalDataCat,...
% % %                     varSelect,...
% % %                     pars.cv.genDimRed.ch.PCA.run,pcaPerc,...
% % %                     modelOrderChoice,...
% % %                     load_K_p,load_varSelect,load_dataTypeStr,load_pcaBool,load_pcaPerc,pars.dataType.sampleRate,p_mo_range,load_percData,...
% % %                     cond_model_order,lambda_K,lambda_lambdaWeightRange,lambda_inc,lambda_its,lambda_percData,lambda_lasso_plotParamsByLambda,lambda_SC,...    
% % %                     p_K,p_modelOrderRange,p_percData,...
% % %                     pars.cv.p.dimRed.ch.lasso.bool,p_lasso_lambdaWeightRange,p_lasso_inc,p_lasso_its,p_lasso_percData,p_lasso_plotParamsByLambda,p_lasso_K,p_lasso_SC_bool,...
% % %                     pars);
function pars=printCheckParsStruct(pars)

%% Display some shit re: params
if pars.run.bools.pcaOnly
    disp('RUNNING PCA ONLY')
else
    displayParams(pars)
end

%% check relevant params for conflicts
if pars.dataType.epochLen*pars.dataType.sampleRate <= max(pars.cv.p.modelOrderRange)
    error('Max model-order cannot equal/exceend pars.dataType.epochLen*pars.dataType.sampleRate')
end
end

%% HELPER FUNCTION TO SET ALL pars
function pars=setDataTypePars(pars,epochLen,dataTypeStr,varSelect,catType)
pars.dataType.globalDataCat=dataTypeStr{1};
pars.dataType.varSelect=varSelect{1};
pars.dataType.catType=catType{1}; % anatROIs
pars.dataType.epochLen=epochLen;
end
%% HELPER FUNCTION TO SET ALL pars
function pars=setPCAparams(pars,pcaBool,pcaPerc)
pars.cv.genDimRed.ch.PCA.run=pcaBool;
pars.cv.genDimRed.ch.PCA.perc=pcaPerc;
end

end

