function [dataIDparams,condData]=removeNans(dataIDparams,condData)

if length(condData.varID)~=size(condData.epochs.data,2)
    % remove data from condData.epochs var
    condData.epochs.data(:,length(condData.varID)+1:end,:)=[];
    % remove varID & varCat from condData.varID 
    condData.varID(length(condData.varID)+1:end)=[];
    condData.varCat(length(condData.varID)+1:end)=[];
    % remove data from dataIDparams.varInfo
    dataIDparams.varInfo(length(condData.varID)+1:end)=[];
end
%% determine avg epoch% filled by each var
percFilledAllVarsAllEpochs=nan(size(condData.epochs.data,2),size(condData.epochs.data,1));
for iVar=1:size(condData.epochs.data,2)
    for iPoch=1:size(condData.epochs.data,1)
        percFilled=(squeeze(isnan(condData.epochs.data(iPoch,iVar,:))));
        percFilled=sum(percFilled)/length(percFilled);
        percFilledAllVarsAllEpochs(iVar,iPoch)=percFilled;
    end
end
figure
imagesc(percFilledAllVarsAllEpochs)
keyboard % inspect missing data
%% 1) find epochs where more than 50% of vars lacked any observations for an epoch. Remove such epochs.
rmEpInds=[];
for iEpoch=1:size(condData.epochs.data,1)
    varCt=0;
    for iVar=1:size(condData.epochs.data,2)
        if sum(isnan(condData.epochs.data(iEpoch,iVar,:))) > 0%.002*size(condData.epochs.data(iEpoch,iVar,:),3)
            varCt=varCt+1;
        end
    end
    if varCt>size(condData.epochs.data,2)*.5
        rmEpInds=[rmEpInds,iEpoch];
    end
end
if ~isempty(rmEpInds)
    % remove data from condData.epochs var
    condData.epochs.data(rmEpInds,:,:)=[];
    disp(['Removed ' num2str(length(rmEpInds)) ' epochs'])
end
%% 2) Find  variables that lack full epochs for at least half of all epochs
varsToRem=[];
for iVar=1:size(condData.epochs.data,2)
    emptyEpochCt=0;
    for iEpoch=1:size(condData.epochs.data,1)
        if sum(isnan(condData.epochs.data(iEpoch,iVar,:))) > .5*size(condData.epochs.data(iEpoch,iVar,:),3)
            emptyEpochCt=emptyEpochCt+1;
        end
    end
    if emptyEpochCt>size(condData.epochs.data,1)*.5
        varsToRem=[varsToRem,iVar];
    end
end
if ~isempty(varsToRem)
    disp(['Removing ' num2str(length(varsToRem)) ' vars that lack enough data per epoch (>50% epoch is nan)'])
    % remove data from condData.epochs var
    condData.epochs.data(:,varsToRem,:)=[];
    % remove varID & varCat from condData.varID 
    condData.varID(varsToRem)=[];
    condData.varCat(varsToRem)=[];
    % remove data from dataIDparams.varInfo
    dataIDparams.varInfo(varsToRem)=[];
end

%% 3) remove common nan time points?
%         keyboard
%         allNanLocs=zeros(size(condData.epochs.data,3),1);
%         for iEpoch=1:size(condData.epochs.data,1)
%             for iVar=1:size(condData.epochs.data,2)
%                 nanInds=find(isnan(condData.epochs.data(iEpoch,iVar,:)));
% %                 if sum(nanInds==1)>0
% %                     keyboard
% %                 end
%                 allNanLocs(nanInds)=allNanLocs(nanInds)+1;
%             end
%         end
% %         find(allNanLocs
%         for iEpoch=1:size(condData.epochs.data,1)
%             if find(sum(isnan(condData.epochs.data(iEpoch,:,:)))==73)
%                 disp(num2str(iEpoch))
%             end
%         end
%% are there any variables with all data? will work with those to start with. need method to deal with nans still.
allDataVars=0;
missingDataVars=[];
for iVar=1:size(condData.epochs.data,2)
    sumData=sum(sum(~isnan(condData.epochs.data(:,iVar,:))));
    totalDataPossible=size(condData.epochs.data,1)*size(condData.epochs.data,3);
    if sumData==totalDataPossible
        allDataVars=allDataVars+1;
    else
        missingDataVars=[missingDataVars,iVar];
    end
end
if ~isempty(missingDataVars)
    disp(['Removing ' num2str(length(missingDataVars)) ' vars that have any nans present'])
    % remove data from condData.epochs var
    condData.epochs.data(:,missingDataVars,:)=[];
    % remove varID & varCat from condData.varID 
    condData.varID(missingDataVars)=[];
    condData.varCat(missingDataVars)=[];
    % remove data from dataIDparams.varInfo
    dataIDparams.varInfo(missingDataVars)=[];
end