function mahalDist_epochOutliers(dataEpochs)
disp('Detecting outlier epochs based on Mahal distance...')

removeAny=true;
while removeAny
    [removeAny,dataEpochs]=mahalDist_epochOutliers_helper(dataEpochs);
end
