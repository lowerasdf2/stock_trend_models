function [removedAny,dataEpochs]=mahalDist_epochOutliers_helper(dataEpochs)
% winLen=size(dataEpochs{1},2);
% stepSize=winLen;%100;
% maxBegT=size(dimRedCondData.data,2)-winLen+1;
nBins=length(dataEpochs);%1:stepSize:maxBegT);
% startInd=1;
% endInd=winLen;
mahalDists=nan(nBins,1);
keepInds=1:length(dataEpochs);
removedAny=false;
for i=1:length(dataEpochs)
    %% calc mean and covariance of majority of data - ROI
    estDataInds=[1:i-1,i+1:length(dataEpochs)];
    estData=dataEpochs(estDataInds);%dimRedCondData.data(:,[1:startInd,endInd+1:size(dimRedCondData.data,2)]);
    estData=horzcat(estData{:});
    %% concat all matrices in cell array
    mu_est=mean(estData,2);
    Sinv=inv(cov(estData'));
    %% compare ROI to sample mean/cov using mahal distance for each timePt
    % in ROI
    testDat=dataEpochs{i};%dimRedCondData.data(:,startInd:endInd);
%     for t=1:size(testDat,2)
    mu_test=mean(testDat,2);% use mean of time-window as observation to compare against distribution
    %% calculate mahal distance
    mahalDists(i)=sqrt(((mu_est-mu_test)'*Sinv)*(mu_est-mu_test)); 
    if mahalDists(i) > 4
        keepInds=keepInds(keepInds~=i);
        removedAny=true;
    end
    %% update inds
%     startInd=startInd+stepSize;
%     endInd=endInd+stepSize;
end
figure
plot((mahalDists))
dataEpochs=dataEpochs(keepInds);


