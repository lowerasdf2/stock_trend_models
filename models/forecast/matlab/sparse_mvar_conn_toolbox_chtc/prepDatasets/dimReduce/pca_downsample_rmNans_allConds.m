function pcaData=pca_downsample_rmNans_allConds(pars,dataIDparams)

%% run script
disp('PCA: Running PCA on channel data concatenated across all conditions.')
pcaDir=[pars.dirs.dimRedData filesep '..' filesep 'pca_data' filesep pars.dataType.dataID filesep];
if ~exist(pcaDir)
    mkdir(pcaDir)
end
fileName=['pcaData_' pars.dataType.globalDataCat '_' pars.dataType.catType '_' pars.dataType.varSelect '_' num2str(pars.dataType.sampleRate) 'hz_perc' num2str(pars.cv.genDimRed.ch.PCA.perc*100) '.mat'];
%% don't bother checking for file since I am making file adjustments on the fly
if exist([pcaDir fileName],'file') && ~pars.run.bools.pcaOnly
    disp('WARNING: currently recreating (updating) saved dimRedCondData file via pca_downsample_alConds.m')
end
% % %     load([pcaDir fileName],'ppData');
% % % elseif ~exist([pcaDir fileName],'file')
try
    [pcaData]=pca_downsample_rmNans_allConds_helper(pars,dataIDparams);
catch why
    keyboard
    [pcaData]=pca_downsample_rmNans_allConds_helper(pars,dataIDparams);
end
save([pcaDir fileName],'pcaData','pars') % saving out pars just in case for future reference
% % % else
% % %     keyboard
% % % end