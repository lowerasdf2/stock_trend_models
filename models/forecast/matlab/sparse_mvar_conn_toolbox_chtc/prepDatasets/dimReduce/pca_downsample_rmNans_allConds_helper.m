function [allPcaCondData]=pca_downsample_rmNans_allConds_helper(pars,dataIDparams)
%% PCA code
% Step1: Concat. all data across conditions
% Step2: Split channels into sets by ROI
% Step3: Run PCA and keep #vars that leads to at least pars.cv.genDimRed.ch.PCA.perc of
% ROI-spec variance (across channels) accounted for
allCondData=[];
trialLengthsMap=containers.Map;
timeVecMap=containers.Map;
trialTimesMap=containers.Map;
trialsByTypeMap=containers.Map;
if pars.dataType.downsampleBool
    disp('PCA: Downsampling data prior to PCA')
end
if pars.cv.genDimRed.ch.PCA.norm_cond
    disp('PCA: Zscoring data w/in each condition s.t. variance is measured relative to total power w/in a condition.')
end
cumVarAcrossROIs=struct;
%% Create one dataset that consists of all condition-specific data
condInd=0;
for cond=dataIDparams.conds 
    cond=cond{1};
    condInd=condInd+1;
    condDataDir=getCondDataDir(pars,dataIDparams.dataID,cond);
    load([condDataDir filesep 'condData.mat'],'condData')
    if isfield(condData,'varIDs')
        % meant to fix old format files
        condData=RenameField(condData,'varIDs','varID');
        save([condDataDir filesep 'condData.mat'],'condData');
    end
    if ~isfield(condData,'varCat')
        % meant to fix old format files
        condData.varCat={dataIDparams.varInfo.varCat};
        save([condDataDir filesep 'condData.mat'],'condData');
    end
    if length(condData.varCat) ~= length(condData.varID)
        keyboard % wtf
    end
    %% check length & contents match
    if ~isequal(condData.varCat,{dataIDparams.varInfo.varCat}) || ~isequal(condData.varID,{dataIDparams.varInfo.varID}) 
        keyboard
%         condData.varID={dataIDparams.varInfo.varID};
%         save([condDataDir filesep 'condData.mat'],'condData');
    end
    %% quick fix
    if isfield(condData,'epochs') && size(condData.epochs.data,1)==701
        keyboard % shouldn't need this anymore since it ran once already
        condData.epochs.data=reshape(condData.epochs.data,[99,701,1440]);
        save([condDataDir filesep 'condData.mat'],'condData');
    end
    %% store trial stuff
    if isfield(condData,'trial_times')
        trialTimesMap(cond)=condData.trial_times;
        trialsByTypeMap(cond)=condData.trials_by_type;
    end
    %% remove nan data
    pars.misc.rmNanTimePts=true;
    if pars.misc.rmNanTimePts         
        if strcmp(dataIDparams.dataID,'FOREX')% 3rdPartyDataset I used to revamp pipeline to generalize to different kinds of time-series datasets
            keyboard
            [dataIDparams,condData]=removeNans(dataIDparams,condData)
        elseif ~isfield(condData,'data')
            keyboard
        else
            if sum(sum(isnan(condData.data))) > 0
                keyboard
            end
        end
    end

    %% check length & contents match
    if ~isequal(condData.varCat,{dataIDparams.varInfo.varCat}') || ~isequal(condData.varID,{dataIDparams.varInfo.varID}') 
        keyboard % wtf
%         condData.varID={dataIDparams.varInfo.varID};
%         save([condDataDir filesep 'condData.mat'],'condData');
    end
    %% downsample
    condData=downsample_data(pars,condData);
    if ~sum(isnan(condData.data(:)))==0
        keyboard
    end
    
    %% find periods of no change in data
    if strcmp(pars.dataType.globalDataCat,'minute') && strcmp(dataIDparams.dataID,'FOREX')
        noChangeZones=zeros(size(condData.data,2),1);
        for iVar=1:size(condData.data,1)
            noChangeInds=find(diff(condData.data(iVar,:))<.000000000000001);
            noChangeZones(noChangeInds)=noChangeZones(noChangeInds)+1;
        end
        condData.data=condData.data(:,(noChangeZones<(size(condData.data,1)/2)));
    end
    %% normalize w/in condition before concatenating across conditions
    if pars.cv.genDimRed.ch.PCA.norm_cond    
        X=condData.data';
        if any(isnan(X(:)))
            xmu=nanmean(X);
            xsigma=nanstd(X);
            x=((X-repmat(xmu,length(X),1))./repmat(xsigma,length(X),1))';
%             figure
%             imagesc(x)
            condData.data=x;
        else
            condData.data=zscore(condData.data')';
        end
    end
    
    %% store length of condition data so we can extract cond-specific data after pca is applied
    trialLengthsMap(cond)=size(condData.data,2);
    timeVecMap(cond)=condData.time; 
%% CME used this section to determine how PCA will affect the phase of the virtual signal when combining 2 sinusoids
% % %     if size(condData.data,1)==10 && strcmp(pars.dataType.dataID,'patient394R')
% % %         keyboard        
% % %         %%Time specifications:
% % %         Fs=250;                   % samples per second
% % %         dt=1/Fs;                   % seconds per sample
% % %         StopTime=10;             % seconds
% % %         t=(0:dt:StopTime-dt)';     % seconds
% % %         %%Sine wave:
% % %         Fc=4;                     % hertz
% % %         %    x=cos(2*pi*Fc*t);
% % %         x=sin(pi/4*Fc*t);
% % %         condData.data=[];
% % %         condData.data(1,:)=x;
% % %         % Plot the signal versus time:
% % %         figure;
% % %         plot(t,x);
% % %         xlabel('time (in seconds)');
% % %         title('Signal versus Time');
% % %         zoom xon;
% % %         hold on
% % %         x=sin((pi/4*Fc*t)+(pi)) ;
% % %         condData.data(2,:)=x;
% % %         plot(t,x);
% % %     end
    %% concat across conds
    allCondData=horzcat(allCondData,condData.data);
    endCondInds(condInd)=size(allCondData,2);
end
roiList=unique({dataIDparams.varInfo.varCat});
coefMap=containers.Map; % maps for saving out
scoreMap=containers.Map;
for roiInd=1:length(roiList)
    roi=roiList{roiInd};
    chList=find(strcmp({dataIDparams.varInfo.varCat},roi)==1);
    A=allCondData(chList,:)';
    
    %% compare eigenvecs across conditions
% % %     eigVecDiffsAcrossCondsFig=figure;
% % %     B1=zscore(A(1:endCondInds(1),:)); % turns out that this step is already done by PCA, but I'll leave it just in case matlab version changes the function later
% % %     [COEFF SCORE]=pca(B1);
% % %     for iCond=2:length(patientParams.cond)
% % % %         if iCond == 2
% % % %             B1=zscore(A(1:endCondInds(1),:)); % turns out that this step is already done by PCA, but I'll leave it just in case matlab version changes the function later
% % % %         else
% % % %             keyboard
% % % %             B1=zscore(A(endCondInds(iCond-2):endCondInds(iCond-1),:));
% % % %         end
% % % %         [COEFF SCORE LATENT]=pca(B1);
% % %         
% % %         B2=zscore(A(endCondInds(iCond-1):endCondInds(iCond),:));
% % %         [COEFF2 SCORE2]=pca(B2);
% % %         
% % %         subplot(length(patientParams.cond)-1,1,iCond-1)
% % %         imagesc((COEFF-COEFF2).^2)
% % %         colorbar
% % %         title(['(' patientParams.cond{1} ' - ' patientParams.cond{iCond} ')^2'])
% % %         suptitle({[patientParams.patientID ': ' strrep(roi,'_','-')]})
% % %     end
% % %     clear COEFF2 SCORE2
% % %     figure(eigVecDiffsAcrossCondsFig)
% % %     figDir=[pars.dirs.baseResultsDir filesep 'PCA_eigVecDiffsAcrossConds' filesep pars.dataType.varSelect];
% % %     figName=[roi '_' patientParams.patientID createFileEnd_fitECoG(pars) '-eigVecDiffsAcrossConds'];
% % %     if ~exist(figDir,'dir')
% % %         mkdir(figDir)
% % %     end
% % %     savefig(gcf,[figDir filesep figName])
% % %     saveas(gcf,[figDir filesep figName],'png')
% % %     
    %% hyperparams
    use_zscore=false; % correlation amplifies noise
    useSVD=true;
    % (false,false), (true,false), (false,true), (true,true)
    %% Zscore or don't
    if use_zscore
        hyperParamsString='_zscore';
        B=zscore(A); % turns out that this step is already done by PCA, but I'll leave it just in case matlab version changes the function later
    else
        hyperParamsString='_noz';
        B=A;
    end
    [COEFF SCORE]=pca(B);
    coefMap(roi)=COEFF; 
    scoreMap(roi)=SCORE;
% % %     [COEFF2 SCORE LATENT]=myPCA(B);
% % %     figure
% % %     subplot(2,1,1)
% % %     imagesc(COEFF)
% % %     subplot(2,1,2)
% % %     imagesc(COEFF2)
    
    cumsumVar=cumsum(nanvar(SCORE)) / sum(nanvar(SCORE));
    var_B=(nanvar(SCORE)) / sum(nanvar(SCORE));
    cumVarAcrossROIs.(roi)=cumsumVar;
    cumsumVar=cumsum(nanvar(SCORE)) / sum(nanvar(SCORE));
    var_B=(nanvar(SCORE)) / nanvar(var(SCORE));
    cumVarAcrossROIs.(roi)=cumsumVar;
    
% % %     figure
% % %     subplot(1,1,1)
% % % % % %     [COEFF2 SCORE LATENT]=pca(A+10);
% % %     imagesc(COEFF-COEFF2)
% % %     
% % %     subplot(2,1,2)
% % %     imagesc(COEFF2)
    
    %% coeff is same as eigenvectors
    %% PC scores are orthogonal and only relate to themselves
%     corr(SCORE)

    %% See if I can reproduce B using eigenvectors (coeff) and transformed data
%     B2=(SCORE*COEFF'); % recreates data in 'B' variable
%     %% Compare B and B2
%     figure
%     subplot(2,1,1)
%     imagesc(zscore(B(1:10,:)))
%     subplot(2,1,2)
%     imagesc((zscore(B2(1:10,:))))

    %% See if I can produce the score variable myself: learned how from here: https://stats.stackexchange.com/questions/229092/how-to-reverse-pca-and-reconstruct-original-variables-from-several-principal-com
%     SCORE2=B*COEFF;
%     [vars,inds]=sort(var(SCORE2),'descend');
%     SCORE2=zscore(SCORE2(:,inds)); % sort score by which vars in PC space have most variance
%     figure
%     subplot(2,1,1)
%     imagesc(SCORE2(1:10,:))
%     subplot(2,1,2)
%     imagesc(SCORE(1:10,:))


    
% % %     figure
% % %     subplot(3,1,1)
% % %     imagesc(COEFF)
% % %     title('coef comparison')
% % %     subplot(3,1,2)
% % %     imagesc(COEFF_lopass)
% % %     subplot(3,1,3)
% % %     imagesc(COEFF_hipass)

% % %     figure
% % %     subplot(3,1,1)
% % %     imagesc(v)
% % %     subplot(3,1,2)
% % %     imagesc(COEFF_lopass)
% % %     subplot(3,1,3)
% % %     imagesc(COEFF_hipass)
    
% % %     u=COEFF_lopass;
% % %     CosTheta_uv=dot(u,v)/(norm(u)*norm(v));
% % %     ThetaInDegrees_uv=acosd(CosTheta_uv);
    
% % %     X=B_lopass';
% % %     [D, N]=size(X);
% % %     m=mean(X, 2);
% % %     X=X - repmat(m, 1, N);
% % %     [e, f, g]=svd(X,'econ');
% % %     figure
% % %     subplot(2,1,1)
% % %     imagesc(e)
% % %     subplot(2,1,2)
% % %     imagesc(COEFF_lopass)
    
% % %     permuts=10;
% % %     randChs=[randperm(size(SCORE_lopass,2));randperm(size(SCORE_lopass,2));randperm(size(SCORE_lopass,2))]';
% % %     for rInd=1:permuts
% % %         
% % %         figure
% % %         subplot(2,1,1)
% % %     %     scatter(SCORE_lopass(:,2),SCORE_lopass(:,3))
% % %         startInd=1;
% % %         endInd=1000;
% % %         scatter3(SCORE_lopass(startInd:endInd,randChs(rInd,1)),SCORE_lopass(startInd:endInd,randChs(rInd,2)),SCORE_lopass(startInd:endInd,randChs(rInd,3)))
% % %     %     set(gca,'Xscale','log')
% % %     %     set(gca,'Yscale','log')
% % %         subplot(2,1,2)
% % %         scatter3(SCORE_hipass(startInd:endInd,randChs(rInd,1)),SCORE_hipass(startInd:endInd,randChs(rInd,2)),SCORE_hipass(startInd:endInd,randChs(rInd,3)))
% % %     end
    
    %% Compare variance explained by regular eigenvectors, low-freq. eigenvectors and high-freq. eigenvectors
    if pars.cv.genDimRed.ch.PCA.plot_hi_vs_lo
        pca_hi_vs_lo_freq_tests();
    end
    if strcmp(pars.cv.genDimRed.ch.PCA.threshType,'PERC')
        numVarsToKeep=find(cumsumVar >= pars.cv.genDimRed.ch.PCA.perc,1,'first');
    else
        keyboard
    end
    newData=SCORE(:,1:numVarsToKeep);
    newLabel=cell(1,numVarsToKeep);
    newLabel(:)={roi};
    if roiInd == 1
        pcaData.varCat=newLabel;
        pcaData.data=newData;
    else
        pcaData.varCat=horzcat(pcaData.varCat,newLabel);
        pcaData.data=horzcat(pcaData.data,newData);
    end
    close all
end
if strcmp(pars.cv.genDimRed.ch.PCA.threshType,'PERC')
    disp(['PCA(' num2str(100*pars.cv.genDimRed.ch.PCA.perc) '% variance) removed ' num2str(length({dataIDparams.varInfo.varCat}) - length(pcaData.varCat)) ' vars (chs) or ' ...
        num2str(round((length({dataIDparams.varInfo.varCat}) - length(pcaData.varCat))/length({dataIDparams.varInfo.varCat}),4)*100) ...
        '% of vars from ' dataIDparams.dataID])
else
    keyboard
end

%% Separate data by conditions once again
allPcaCondData=containers.Map;
ppData_roiChMaps=containers.Map;
condInd=1;
for cond=dataIDparams.conds
    cond=cond{1};
    pcaCondData=struct;
    pcaCondData.varCat=pcaData.varCat;
    pcaCondData.fsample=condData.fsample;
    pcaCondData.time=timeVecMap(cond);
    if ~isempty(trialTimesMap)
        pcaCondData.trial_times=trialTimesMap(cond);
        pcaCondData.trials_by_type=trialsByTypeMap(cond);
    end
    trialLength=trialLengthsMap(cond);
    if condInd == 1
        pcaCondData.data=pcaData.data(1:trialLength,:)';
        startInd=trialLength+1;
    else
        pcaCondData.data=pcaData.data(startInd:startInd+trialLength-1,:)';
        startInd=startInd+trialLength;
    end
    allPcaCondData(cond)=pcaCondData;
    ppData_roiChMaps(cond)=pcaCondData.varCat;
    condInd=condInd+1;
end
saveDir=[pars.dirs.baseResultsDir filesep 'PCA' filesep 'coefByROImaps'];
fileName=[dataIDparams.dataID '_' num2str(pars.dataType.sampleRate) 'hz_' pars.dataType.globalDataCat '_' pars.dataType.varSelect '_PCAA' num2str(pars.cv.genDimRed.ch.PCA.perc*100) '.mat'];
if ~exist(saveDir,'dir')
    mkdir(saveDir)
end
save([saveDir filesep fileName],'coefMap')
saveDir=[pars.dirs.baseResultsDir filesep 'PCA' filesep 'fullScoresByROImaps'];
fileName=[dataIDparams.dataID '_' num2str(pars.dataType.sampleRate) 'hz_' pars.dataType.globalDataCat '_' pars.dataType.varSelect '_PCAA' num2str(pars.cv.genDimRed.ch.PCA.perc*100) '.mat'];
if ~exist(saveDir,'dir')
    mkdir(saveDir)
end
save([saveDir filesep fileName],'scoreMap')
saveDir=[pars.dirs.baseResultsDir filesep 'PCA' filesep 'cumvar_curves'];
if ~exist(saveDir,'dir')
    mkdir(saveDir)
end
save([saveDir filesep fileName],'cumVarAcrossROIs')


