function formatData(pars,batchParams,createInputData)
% call pca, downsample, and rmNan functions
% output=dimRedCondData variable
idInd=0;
for dataIDparams=batchParams
    idInd=idInd+1;
    pars.dataType.dataID=dataIDparams.dataID;
    disp(['Processing data for: ' dataIDparams.dataID '...'])  
    %% check if files have already been made
    if ~createInputData && ~pars.run.bools.nonCHTCcode
        continue
    end
    %% PCA dim. reduction
    if pars.cv.genDimRed.ch.PCA.run
        pcaData=pca_downsample_rmNans_allConds(pars,dataIDparams);
        if pars.run.bools.pcaOnly
            continue
        end
    end
    condInd=0;
    for cond=dataIDparams.conds 
        condInd=condInd+1;
        cond=cond{1};
        %% saveDir/saveFile for data saved out for each condition
        [data_saveFile,dataSaveDir]=getDimRedDataFileNameAndDir(pars,cond);
        if ~exist(dataSaveDir,'dir')
            mkdir(dataSaveDir)
        end
        if exist([dataSaveDir filesep data_saveFile],'file') && ~pars.run.bools.nonCHTCcode
%             return
        end
        %% extract conditional data, downsample if data did not go through pca function using same downsample function called in pca function
        if pars.cv.genDimRed.ch.PCA.run
            dimRedCondData=pcaData(cond);
        else
            keyboard % check this section of code saves dimRedCondData in proper format 
            %% Convert it to fieldtrip format, load selected data
            cfg = [];
            if strcmp(pars.dataType.globalDataCat,'OR-RS')
                cfg.rsOnly=true;
            elseif strcmp(pars.dataType.globalDataCat,'OR-EV')
                cfg.rsOnly=false;
            else
                error('Need to add another check to detect if data is resting state or not for loading purposes in below lines.')
            end
            cfg.condition      = condition{:};
            cfg.patientID      = dataIDparams.dataID;
            cfg.ECoGchannels   = dataIDparams.varInfo;
            cfg.signalName     = dataIDparams.signalName;
            cfg.dataPrefix     = dataIDparams.dataPrefix;
            cfg.data_dir=LocalPaths.Ecog_Data_Dir;
            cfg.downsample     = 0;
            cfg.dataType       = pars.dataType;
            [dimRedCondData, ~, ~] = load_condition_ecog(cfg); keyboard % make sure trial_times and trials_by_type are added to dimRedCondData as fields since no longer saving out as separate vars
            %% downsample w/ same function used in pca_allConds procedure
            dimRedCondData=downsample_data(pars,dimRedCondData);
        end
        %% continue if data already saved out
%         if ~exist([dataSaveDir filesep data_saveFile],'file')
        save([dataSaveDir filesep data_saveFile],'dimRedCondData')
%         end
    end
end