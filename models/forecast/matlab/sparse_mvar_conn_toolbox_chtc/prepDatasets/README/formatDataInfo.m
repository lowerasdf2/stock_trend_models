%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   prepData Pipeline    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Batch_sparse_mvar_conn_analysis: getBatchParams_prepCondData() -> batchParams+condData -> formatData() -> dimRedCondData -> createDataEpochs() -> dataEpochs
%% getBatchParams_prepCondData()
% 1) Creates batchParams.mat (described in detail in below section)
    % This file stores all metadata related to dataset for all dataIDs relevant
    % to particular experiment. Does not store actual data.
% 2) Creates condData.mat (described in section below). This is the file
% that contains the actual data that is being modeled.

%% formatData(condData)
% 1) Downsamples condData if requested, runs pca if requested, removes nans if
% requested

%% createDataEpochs(dimRedCondData)
% 1) Creates dataEpochs file used for CV_p_lambda

%% %%%%%%%%%%%%%%%%%%%%%%%%%%   Reference vars & storage schema   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% core reference vars
globalDataCat='minute';% Name of dataset category: highest level categorization of data being processed/modeled.
dataID={'FOREX'}; % Name of specific datasets to model w/in dataType.globalDataCat category. 
                                % Model does not incorporate data across items in this list. You could store
                                % mixed data in sinlge folder with dataID to mix variable info together. 
                                % But data needs to be aligned to start at same time point.
varSelect={'ALLVARS'}; % Acts as a filter for dataID's data. If there is a subset of vars you want to utilize, save corresponding batchParams file out for it via modifing getBatchParams_prepCondData pipeline.
catType={'catALL'}; % specifies what categorization scheme to use for vars in dataID/varSelect folder. These categories determine how to partition variables for PCA. catALL=run PCA on entire dataset.
cond={'seg0'}; % condition specific condition for dataID/varSelect/catType. Comes in handy for analyzing the same data under different conditions. Often can just use 'seg0' for singleCond/noCond

%% storage
% batchParams.mat
% ..\batchParams\$(globalDataCat)\$(varSelect)\$(catType)\batchParams.mat                
    % e.g. ..\batchParams\minute\ALLVARS\catALL\batchParams.mat;
% condData.mat 
% ..\condData\$(globalDataCat)\$(varSelect)\$(dataID)\$(cond)\condData.mat   
    % e.g. ..\condData\minute\ALLVARS\FOREX\seg0\condData.mat;
% ..\dimRedCondData\$(globalDataCat)\$(varSelect)\$(catType)\$(dataID)\$(cond)\dataEpochs_trainTestInds_*.mat 
% dimRedData_125hz_pca60.mat & dataEpochs_trainTestInds_*.mat 
    % e.g. ..\dimRedCondData\OR-RS\AUDITORY\standROIs\patient394R\OAAS5\dataEpochs_trainTestInds_epLen1.4_mins0-0.1_2folds_2subfolds_125hz_pca60

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% formatted data/dataInfo files %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% batchParams stores all info for all dataIDs/patients of experiment
% IDind refers to patient/dataID index
batchParams(IDind).varSelect='ALLVARS'; % {'ALLVARS','AUDITORY'}
                             % Name of manual variable selection method applied before PCA/LASSO (i.e. not PCA/LASSO). 
                             % Could be that you only care about channels recorded from e.g. auditory
                             % ctx. If simply using all variables, call that 'ALLVARS'

batchParams(IDind).dataID='patient394R'; % e.g. name of dataset being modeled/analyzed. Used as folder name reference for accessing dataID's data.
                         
batchParams(IDind).varInfo % 1 x nVar structure containing all info on vars
batchParams(IDind).varInfo(varInd).varID='1';  % unique ID of variable provided as a string (e.g. chID_ROIlabel1_ROIlabel2)
batchParams(IDind).varInfo(varInd).varCat='L_PFC'; %  category of varID e.g. ROI label. PCA is run on each 
                                                   % variable category (e.g. within ROIs). Note: for bilateral 
                                                   % recordings you should
                                                   % append L/R to ROI
                                                   % name. Set as ALL to
                                                   % run pca on entire
                                                   % dataset. If not
                                                   % splitting up data
                                                   % simply use 'ALLVARS'
                                       
batchParams(IDind).varInfo(varInd).catType='catALL'; % should specify method/label of categorization of variables into groups/ROIs (e.g. 'anatROI' vs. 'funcROI'). 
                                    % PCA is run on each group, so this
                                    % method is relevant to final outputs
                                    % of model.
                                    % --> set as 'catALL' to run PCA
                                    % on entire dataset rather than
                                    % w/in ROIs
                             
batchParams(IDind).signalName='LFPx'; % e.g. LFP, ECoG, EEG. I don't actually use this info anywhere, but good to store it for analysis reference later.

batchParams(IDind).conds={'seg0'}; % cell array of "conditions" e.g. {'OAAS1','OAAS5'} or {'seg1','seg2'}. Conditions are anything that split the 
% data in a way that requires a new model. For fitting models to entire RS blocks, can simply use name of RS block, which is synonymous 
% with OAAS range during block. For fitting to different segments, can use
% segmentInteger. Whatever the method, cond-specific data should be stored
% in separate folder with same name as cond. IF there is only one condition
% of interest, simply name condition folder 'seg0'

%% batchParams storage instructions
% You should only need one batchParams file for every dataType_varType
% combination. Store for multiple patients/IDs using IDind as storage index.
% Keep files in: ['whereverYouWant\batchParams\' dataType '_' varType '_batchParams.mat'] 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% condData stores data corresponding to all varIDs. file does not care about variable category/type.
% FILEDIRNAME='G:\data\condData\$(dataType.str)\$(varSelect)\$(dataID)\$(cond)\condData.mat'
% e.g. 'G:\data\condData\OR-RS\ALLVARS\patient376R\OAAS5\condData.mat'
condData.varID=batchParams.varInfo(varInd).varID; %
condData.fsample % sampleRate in Hz
condData.fsampleName % optional name of sample rate in case Hz is too small to interpret easily (e.g. with daily/weekly samples)
condData.time % 1xT vector of timestamps from recording, where T is total number of recorded samples 
condData.trial_times % 1 x nTrials vector w/ time stamps matching time vector of trial start times
condData.trials_by_type % if trialType matters, then store labels for each trial (same length as trial_times)
%% For storing actual data, choose from two formatting options...
% OPTION1
condData.data % [nVar x T] matrix containing all data sampled in continous/consistent manner
% OPTION2
condData.epochs.data % [nEpochs x nVar x T] 3D matrix stores data by epoch
condData.epochs.type % string that denotes what marks a single epoch. Should usually just be 'trial'.
condData.epochs.convertible % Boolean that describes whether or not epoch representation can be concatenated to yield continuous form identical to conData.data. If some trials are removed before storing, then this would be false. 
                            % --> It's probably best to ensure convertible can
                                % be set to true just in case pre/post
                                % trial data is relevant
                                
%% dimRedCondData = formmated cond data (pca + downsampling applied). 
% FILEDIRNAME=data_$(pars.dataType.globalDataCat)_$(pars.dataType.catType)_$(pars.dataType.varSelect)_$(pars.dataType.sampleRate)hz_pca$(pars.cv.genDimRed.ch.perc).mat'
dimRedCondData.varCat % variable category/ROI (after PCA, this is the only reliable distinguisher)
dimRedCondData.fsample % sample-rate after downsampling has occurred
dimRedCondData.trial_times % optional if data is evoked or exogenous inputs present
dimRedCondData.trials_by_type  % optional if data is evoked or exogenous inputs present
%% condData storage instructions
% Store condData in
% ..\whateverYouCallYourDataFolder\batchParams(IDind).dataType\batchParams(IDind).varType\batchParams(IDind).dataID\batchParams(IDind).cond{condInd} folder
% e.g. if...
%     batchParams(IDind).dataType='OR-RS';
%     batchParams(IDind).varType='AUDITORY';
%     batchParams(IDind).dataID='patient376R';
%     batchParams(IDind).cond{1}='OAAS1';
%     ... path would be: data\OR-RS\AUDITORY\patient376R\OAAS1

%% dataEpochs file
dataEpochs % epoched data
n_epoch % 
indices_cv_p % 
indices_cv_p_lasso %
dataInfo.varCat=dimRedCondData.varCat;
dataInfo.trial_times=dimRedCondData.trial_times; % optional if data is evoked or exogenous inputs present
dataInfo.trials_by_type=dimRedCondData.trials_by_type; % optional if data is evoked or exogenous inputs present


