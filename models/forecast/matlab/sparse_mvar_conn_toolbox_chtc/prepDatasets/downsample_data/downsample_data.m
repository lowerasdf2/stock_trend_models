function condData=downsample_data(pars,condData)
if pars.dataType.downsampleBool
    downSampleFactor=round(condData.fsample/pars.dataType.sampleRate);
    if isfield(condData,'epochs')
        if condData.epochs.convertible
            newdata=nan(size(condData.epochs.data,2),size(condData.epochs.data,1)*size(condData.epochs.data,3));
            for iVar=1:size(condData.epochs.data,2)
                startInd=1;
                endInd=size(condData.epochs.data,3);
                for iEpoch=1:size(condData.epochs.data,1)
                    newdata(iVar,startInd:endInd)=condData.epochs.data(iEpoch,iVar,:);
                    startInd=startInd+size(condData.epochs.data,3);
                    endInd=endInd+size(condData.epochs.data,3);
                end
            end
            condData.data=newdata;%reshape(condData.epochs.data,[size(condData.epochs.data,2),size(condData.epochs.data,1)*size(condData.epochs.data,3)]);
        else
            crash % need to decide how to run pca on epochs...
        end
        figure
        plot(squeeze(condData.epochs.data(1,1,1:1000)))
%         newSize=length(1:downSampleFactor:size(condData.epochs.data,3));
%         condData.epochs.newdata=nan(size(condData.epochs.data,1),size(condData.epochs.data,2),newSize);
%         for i=1:size(condData.epochs.data,1)
% %             condData.epochs.newdata(i,:,:)=downsample(squeeze(condData.epochs.data(i,:,:))',downSampleFactor)';
%             condData.data(i,:,:)=downsample(squeeze(condData.epochs.data(i,:,:))',downSampleFactor)';
%         end
%         condData.epochs.data=condData.epochs.newdata;
%         condData.epochs=rmfield(condData.epochs,'newdata');
    end
    condData.data=downsample(condData.data',downSampleFactor)';
%     end
    condData.fsample=condData.fsample/downSampleFactor;
end
if pars.dataType.sampleRate ~= condData.fsample
    crash
end