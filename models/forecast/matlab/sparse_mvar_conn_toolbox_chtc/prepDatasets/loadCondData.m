function [conditionECoG, trial_times, trials_by_type] = loadCondData(args)
% Loads the raw data for a given patient and condition, along with some basic metadata. 
% Arguments:
% args.downsample   - (int) If 0, data will not be downsampled. Otherwise, the 
%                     data will be downsampled to this frequency (in Hz), if needed. 
% args.patientID    - (str) The ID of the patient to load, e.g. 'patient369LR'. 
% args.condition    - (str) The condition to load, e.g. 'OAAS5_1'.
% args.data_dir     - (str) The directory which contains the patientID/condition/
%                     subdirectory, which in turn contains the raw data. 
% args.ECoGchannels - [struct] As produced from ECoGChannelMap or load_batch_params.
% args.signalName   - (str) 
% args.dataPrefix   - (str) 
%
% Outputs:
% conditionECoG     - A FieldTrip-formatted structure containing the continuous data for this patient and condition. 
% trial_times       - [nTrials] The start time of each trial, in seconds. 
% trials            - trails.loclStd is an [n x 1] list of all the trail numbers 
%                     corresponding to local standard trails. Similarly for loclDev, globStd, globDev.

%% Load all the raw data for all of the trials in this condition as
% one contiguous, undifferentiated block. To be segmented later.
[data, fsample, trial_times, trials_by_type] = load_condition_ecog_raw(args); 
[~,nPts] = size(data);
conditionECoG=struct;
conditionECoG.data=data;
conditionECoG.fsample=fsample; % sampling frequency in Hz
conditionECoG.time=(0:nPts-1) ./ conditionECoG.fsample; % One time point per sample, in seconds. 

