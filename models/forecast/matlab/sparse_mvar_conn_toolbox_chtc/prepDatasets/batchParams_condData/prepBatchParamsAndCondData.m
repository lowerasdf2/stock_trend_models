function batchParams=prepBatchParamsAndCondData(pars,batchParamsSaveDir)
%% create and save out both batchParams and condData for each data type. cond
if strcmp(pars.dataType.globalDataCat,'minute')
    if ~exist([batchParamsSaveDir filesep 'batchParams.mat'],'file')
        batchParams_condData_FOREX(pars,batchParamsSaveDir);
    end
elseif strcmp(pars.dataType.globalDataCat,'CRU-RS-ZZZ') || strcmp(pars.dataType.globalDataCat,'CRU-EV-ZZZ')
    keyboard % haven't rly tested below function at all. not sure if it's the right one to use.
    batchParams=getBatchParams_sleep(electrodeFilePath);
elseif strcmp(pars.dataType.globalDataCat,'OR-RS')
    batchParams_condData_banksECoG(pars,batchParamsSaveDir);
else
    keyboard % unexpected dataset. add code to handle new case.
end
load([batchParamsSaveDir filesep 'batchParams.mat'])
% for i=1:length(batchParams.varInfo)
%     batchParams.varInfo(i).varID=[num2str(batchParams.varInfo(i).varID) '-' batchParams.varInfo(i).varCat];
%     batchParams.varInfo(i).varCat='ALLVARS';
% end
% save([batchParamsSaveDir filesep 'batchParams.mat'])