function batchParams_banksECoG(pars,batchParamsSaveDir)

% Load patient-specific processing parameters and channel info. 
%
% Arguments:
% args.patients_to_load - A cell array containing the patientIDs of patients to 
%                         load parameters for. Can be set to 'all'.
% args.electrode_file   - The path to the Excel spreadhseet containing channel
%                         info for all patients. 
%
% Outputs:
% A struct array, where each element contains parameters for one patient. Fields 
% are: 'patientID', 'recDate', 'ECoGchannels', 'dataPrefix', 'signalNamme', 
% 'exempChans', and 'cond'. See code for details.
batchParams=[];
if contains(pars.dataType.globalDataCat,'OR-')
    %% 369LR
    dataID='patient369LR';
    ECoGchanID='R369_OR';
    if pars.dataType.rsData
        cond{1}='OAAS5';
        cond{2}='OAAS4t3';
    else
        cond{1}='OAAS5_1';
        cond{2}='OAAS5_2';
        cond{3}='OAAS5t4';
        cond{4}='OAAS3t1';
    end
    dataIDparams=load_patient_params(pars,dataID,ECoGchanID,cond);
    batchParams=[batchParams, dataIDparams];
    clear cond;

    %% 372L
    dataID='patient372L';
    ECoGchanID='L372_OR';
    if pars.dataType.rsData
        cond{1}='OAAS5';
        cond{2}='OAAS3t2';
        cond{3}='OAAS1';
    else
        cond{1}='OAAS5t4';
        cond{2}='OAAS4t3';
        cond{3}='OAAS2t1';
        cond{4}='OAAS1';
    end

    dataIDparams=load_patient_params(pars,dataID, ECoGchanID, cond);
    batchParams=[batchParams, dataIDparams];
    clear cond;

    %% 376R
    dataID='patient376R';
    ECoGchanID='R376_OR';
    if pars.dataType.rsData
        cond{1}='OAAS5';
        cond{2}='OAAS4';
        cond{3}='OAAS1';
    else
        cond{1}='OAAS5';
        cond{2}='OAAS5t4';
        cond{3}='OAAS4t1';
        cond{4}='OAAS1';
    end

    dataIDparams=load_patient_params(pars,dataID, ECoGchanID, cond);
    batchParams=[batchParams, dataIDparams];
    clear cond;

    %% 384B --> not using this guy right now... behaves oddly in model-selection procedure (weird model-order perf. curves)
    % % % patientID='patient384B';
    % % % ECoGchanID='B384_OR';
    % % % if args.dataType.rsData
    % % %     cond{1}='OAAS5';
    % % %     cond{2}='OAAS1_1';
    % % %     cond{3}='OAAS1_2';
    % % % else
    % % %     cond{1}='OAAS5';
    % % %     cond{2}='OAAS1'; 
    % % % end
    % % % 
    % % % patientParams=load_patient_params(patientID, ECoGchanID, cond);
    % % % batchParams=[batchParams, patientParams];
    % % % clear cond;

    %% 394R
    dataID='patient394R';
    ECoGchanID='R394_OR';
    if pars.dataType.rsData
        cond{1}='OAAS5';
        cond{2}='OAAS4t3';
        cond{3}='OAAS1';
    else
        cond{1}='OAAS5';
        cond{2}='OAAS5t4';
        cond{3}='OAAS3t1';
        cond{4}='OAAS1';
    end

    dataIDparams=load_patient_params(pars,dataID, ECoGchanID, cond);
    batchParams=[batchParams, dataIDparams];
    clear cond;

    %% 399R
    dataID='patient399R';
    ECoGchanID='R399_OR';
    if pars.dataType.rsData
        cond{1}='OAAS5';
        cond{2}='OAAS3t2';
        cond{3}='OAAS1';
    else
        cond{1}='OAAS5';
        cond{2}='OAAS5t3';
        cond{3}='OAAS2t1';
        cond{4}='OAAS1';
    end
    dataIDparams=load_patient_params(pars,dataID, ECoGchanID, cond);
    batchParams=[batchParams, dataIDparams];
    clear cond;

    %% 400L
    dataID='patient400L';
    ECoGchanID='L400_OR';
    if pars.dataType.rsData
        cond{1}='OAAS4';
        cond{2}='OAAS3';
        cond{3}='OAAS3t1';
    else
        cond{1}='OAAS4';
        cond{2}='OAAS4t3';
        cond{3}='OAAS3';
        cond{4}='OAAS1';
    end
    dataIDparams=load_patient_params(pars,dataID, ECoGchanID, cond);
    batchParams=[batchParams, dataIDparams];
    clear cond;

    %% Commented out 403 and 405 initially because these two Pps lacked latency data in the spreadsheet. No longer commenting out 403L because it is a priority participant
    %% 403L
    dataID='patient403L';
    ECoGchanID='L403_OR';
    if pars.dataType.rsData
        cond{1}='OAAS5';
        cond{2}='OAAS3t1';
        cond{3}='OAAS1';
    else
        cond{1}='OAAS5t4';
        cond{2}='OAAS4t3';
        cond{3}='OAAS1_1';
        cond{4}='OAAS1_2';
    end
    dataIDparams=load_patient_params(pars,dataID, ECoGchanID, cond);
    batchParams=[batchParams, dataIDparams];
    clear cond;
    
    %% leaving 405 commented out since its not a priority participant
    % % % % 
    % % % % patientParams=load_patient_params(patientID, ECoGchanID, exempChans, cond);
    % % % % batchParams=[batchParams, patientParams];
    % % % % clear cond;
    % % % % 
    % % % % %% 405L
    % % % % patientID='patient405L';
    % % % % % TODO: Why does this patient have a non-standard Excel sheet name (_OR suffix)? 
    % % % % ECoGchanID='L405_OR';
    % % % % exempChans.nums=[99,69,71,104,240,251,235,250,256]; % pmHG,alHG,PT,STG,MTG,SMG,IFG,MFG
    % % % % exempChans.ROIs={'L_HGPM','L_HGAL','L_PT','L_PP','L_STGM','L_STGP','L_MTG','L_SMG','L_PFC'};
    % % % % 
    % % % % if args.dataType.rsData
    % % % %     cond{1}='OAAS4t5';
    % % % %     cond{2}='OAAS3t2';
    % % % %     cond{3}='OAAS1';
    % % % % else
    % % % %     cond{1}='OAAS5';
    % % % %     cond{2}='OAAS5t3';
    % % % %     cond{3}='OAAS2t1';
    % % % %     cond{4}='OAAS1';
    % % % % end
    % % % % 
    % % % % patientParams=load_patient_params(patientID, ECoGchanID, exempChans, cond);
    % % % % batchParams=[batchParams, patientParams];
    % % % % clear cond;
    %% added 409 12/27/18
    dataID='patient409L';
    ECoGchanID='L409_OR';
    if pars.dataType.rsData
        cond{1}='OAAS5';
        cond{2}='OAAS4t3';
        cond{3}='OAAS3t1';
    else
        keyboard
    end
    dataIDparams=load_patient_params(pars,dataID, ECoGchanID, cond);
    batchParams=[batchParams, dataIDparams];
    clear cond;
    
    %% added 423 12/27/18
    dataID='patient423L';
    ECoGchanID='L423_OR';
    if pars.dataType.rsData
        cond{1}='OAAS5';
        cond{2}='OAAS5t4';
        cond{3}='OAAS3t1';
        cond{4}='OAAS1';
    else
        keyboard
    end
    dataIDparams=load_patient_params(pars,dataID, ECoGchanID, cond);
    batchParams=[batchParams, dataIDparams];
    clear cond;
    
else
    keyboard
    %% CRU data
end
%% Save out data
save([batchParamsSaveDir filesep 'batchParams.mat'],'batchParams')

% This is a helper functon to encapsulate some of the repetitive work involed 
% in loading each patient's data. 
function dataIDparams=load_patient_params(pars,dataID, ECoGchanID, cond)
    % exempChans.nums defines a list of channel numbers, with one
    % channel number per ROI found in exempChans.ROIs. This channel is
    % considered representative of that ROI. 
    dataIDparams=struct( ...
        'dataID', dataID, ...
        'recDate', 'yymdd', ...
        'varInfo', ECoGChannelMap(pars.electrode_file, ECoGchanID,pars.dataType.catType), ...
        'dataPrefix', 'LFPx_RZ2_chn', ...
        'signalName', 'LFPx', ...
        'conds', {cond});

    %% select channel set
    if strcmp(pars.dataType.varSelect,'ALLVARS')
        % ALL = do nothing
    elseif strcmp(pars.dataType.varSelect,'AUDITORY')
        entryCt=0;
        audRois={'PT','HGPM','HGAL','PP','STG'};
        for nVar=1:length(dataIDparams.varInfo)
            try
                for roiInd=1:length(audRois)
                    if strcmp(dataIDparams.varInfo(nVar).varCat(3:end),audRois{roiInd})
                        entryCt=entryCt+1;
                        dataIDparams.newECoGchannels(entryCt)=dataIDparams.varInfo(nVar);
                    end
                end
            catch why
                keyboard
            end
        end
        dataIDparams.varInfo=dataIDparams.newECoGchannels;
        dataIDparams=rmfield(dataIDparams,'newECoGchannels');
    elseif strcmp(pars.dataType.varSelect,'RESP')
        %% No longer using. Would probs need to be updated before using again
        keyboard
% % %         entryCt=0;
% % %         for nVar=1:length(dataIDparams.varInfo)
% % %             try
% % %                 if dataIDparams.varInfo(nVar).AEPLatLoc ~= -1 || dataIDparams.varInfo(nVar).HiGLatLoc ~= -1 ...
% % %                         || dataIDparams.varInfo(nVar).AEPLatGlob ~= -1 || dataIDparams.varInfo(nVar).HiGLatGlob ~= -1
% % %                     entryCt=entryCt+1;
% % %                     dataIDparams.newECoGchannels(entryCt)=dataIDparams.varInfo(nVar);
% % %                 end
% % %             catch why
% % %                 keyboard
% % %             end
% % %         end
% % %         dataIDparams.varInfo=dataIDparams.newECoGchannels;
% % %         dataIDparams=rmfield(dataIDparams,'newECoGchannels');
    elseif strcmp(pars.dataType.varSelect,'EXEMP')
        %% No longer using. Would probs need to be updated before using again
        keyboard
% % %         % keep just exempChans?
% % %         for nVar=1:length(exempChans.nums)
% % %             for crossChInd=1:length(dataIDparams.varInfo)
% % %                 if dataIDparams.varInfo(crossChInd).chanNum == exempChans.nums(nVar)
% % %                     dataIDparams.newECoGchannels(nVar)=dataIDparams.varInfo(crossChInd);
% % %                 end
% % %             end
% % %         end     
% % %         dataIDparams.varInfo=dataIDparams.newECoGchannels;
% % %         dataIDparams=rmfield(dataIDparams,'newECoGchannels');
    else
        keyboard % unexpected case. add code?
    end

end
end
