function batchParams=batchParams_condData_banksECoG(pars,batchParamsSaveDir)
if ~exist([batchParamsSaveDir filesep 'batchParams.mat'],'file')
    batchParams_banksECoG(pars,batchParamsSaveDir);
    load([batchParamsSaveDir filesep 'batchParams.mat'])
else
    load([batchParamsSaveDir filesep 'batchParams.mat'])
end

%% loop through patients and save out data
for dataIDparams=batchParams
    for cond=dataIDparams.conds
        cond=cond{1};
        condDataDir=getCondDataDir(pars,dataIDparams.dataID,cond);
        if exist([condDataDir filesep 'condData.mat'],'file')
            continue
%             disp('currently recreating (updating) saved (baseline) condData file via batchParams_condData_banksECoG.m')
        end
        cfg = [];
        if strcmp(pars.dataType.globalDataCat,'OR-RS')
            cfg.rsOnly=true;
        elseif strcmp(pars.dataType.globalDataCat,'OR-EV')
            cfg.rsOnly=false;
        else
            error('Need to add another check to detect if data is resting state or not for loading purposes in below lines.')
        end
        cfg.condition      = cond;
        cfg.patientID      = dataIDparams.dataID;
        cfg.ECoGchannels   = dataIDparams.varInfo;
        cfg.signalName     = dataIDparams.signalName;
        cfg.dataPrefix     = dataIDparams.dataPrefix;
        cfg.data_dir=LocalPaths.Ecog_Data_Dir;
        cfg.downsample     = 0;%300; % Not downsampling w/ field trip anymore, doing it on my own afterwards
        cfg.dataType.catType=pars.dataType.catType;
        [condData, trial_times, trials_by_type] = loadCondData(cfg);
        condData.varID={dataIDparams.varInfo.varID}';
        condData.varCat={dataIDparams.varInfo.varCat}';
        condData.catType={dataIDparams.varInfo.catType}';
        if ~cfg.rsOnly
            condData.trial_times=trial_times;
            condData.trials_by_type=trials_by_type;
        end
        if length(condData.varID) ~= size(condData.data,1)
            keyboard %wtf
        end   
        condDataDir=getCondDataDir(pars,dataIDparams.dataID,cond);
        if ~exist(condDataDir,'dir')
            mkdir(condDataDir)
        end
        save([condDataDir filesep 'condData.mat'],'condData')
    end 
end


end

