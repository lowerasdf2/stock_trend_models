function condDataDir=getCondDataDir(pars,dataID,cond)
fileEnd=[pars.dataType.globalDataCat filesep pars.dataType.varSelect filesep dataID filesep cond];
condDataDir=[pars.dirs.baseCondDataDir filesep 'condData' filesep fileEnd];
