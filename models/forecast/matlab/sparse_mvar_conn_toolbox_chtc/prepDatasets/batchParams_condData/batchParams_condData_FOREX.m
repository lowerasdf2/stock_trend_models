function [batchParams]=batchParams_condData_FOREX(pars,batchParamsSaveDir)
batchParams=struct;
batchParams.conds={'seg0'}; % default for no/single cond
batchParams.signalName='minutePrices';
batchParams.catType=pars.dataType.catType;
batchParams.dataID='FOREX';%pars.dataType.dataID;% this dataset has only one dataID. others may have more.
unparsedDataDir=[pars.dirs.baseLocalDagDir filesep '..' filesep '3rdPartyData\testData\minuteForexData\price'];
varFiles=dir([unparsedDataDir filesep '*.csv']);
%% set static condData pars
condData.fsample=1/60;
condData.fsampleName='minute'; % new field included just to help interpet data after reading in condData (not required)
condData.epochs.type='daily'; % define epoch type: e.g. trials or daily. what marks a single epoch basically.
condData.epochs.convertible=true; % convertible means that continuous data could be reconstructed from concatenating combining epochs

%% loop through files
iFiles=1:length(varFiles);
storeFileInd=1;
maxDayInd=0;
minDateNum=inf;
maxDateNum=-inf;
firstDate='2018-11-13';
for iFile=iFiles
    filename=varFiles(iFile).name;
    [data,timeStamps]=xlsread([unparsedDataDir filesep filename]);
    if iFile==1
        % init vars
        maxNumDays=round(365*.5);% 1/3 year
        maxMins=60*24;
        allData=nan(maxNumDays,length(iFiles),maxMins);
    end
    batchParams.varInfo(storeFileInd).varID=[num2str(iFile) '-' filename(1:end-4)];
    condData.varID(storeFileInd)={batchParams.varInfo(storeFileInd).varID};
    batchParams.varInfo(storeFileInd).varCat='ALLVARS';
    condData.varCat(storeFileInd)={batchParams.varInfo(storeFileInd).varCat};
    %% loop through time-stamps and store data
    formatIn='yyyy-mm-ddTHH:MM:SS';
    for i=1:length(timeStamps)
        stamp=timeStamps{i};
        yyyy_mo_da_CURR=stamp(1:10);
        dayInd=daysact(firstDate,yyyy_mo_da_CURR)+1;
        if dayInd <= 0
            keyboard % need to adjust firstDate variable
        end
        % update max/min dates
        dateNum=datenum(stamp,formatIn);
        if dateNum < minDateNum
            minDateNum=dateNum;
            minDate=stamp;
        end
        if dateNum > maxDateNum
            maxDateNum=dateNum;
            maxDate=stamp;
        end
        hh_mm_CURR=stamp(12:end-4);
        % convert hour-minute to single number
        min_CURR=(str2double(hh_mm_CURR(1:2))*60)+str2double(hh_mm_CURR(4:5))+1;
        if min_CURR > size(allData,3)
            keyboard % need to init larger matrix
        end
        try
            if dayInd > size(allData,1)
                keyboard % need to init larger matrix
            end
            allData(dayInd,storeFileInd,min_CURR)=data(i);
        catch why
            keyboard
        end
    end
    storeFileInd=storeFileInd+1;
    if dayInd > maxDayInd
        maxDayInd=dayInd;
    end
    %% save progress files since this takes FOREVERRRR
    %  condData
    condData.time={minDate,maxDate}; % if storing min/max times, use cell array. use mat for full time vec
    condData.epochs.data=allData;
    condDataDir=getCondDataDir(pars,batchParams.dataID,batchParams.conds{1});
    if ~exist(condDataDir,'dir')
        mkdir(condDataDir)
    end
    save([condDataDir filesep 'condData.mat'],'condData')
    % batchParams
    save([batchParamsSaveDir filesep 'batchParams.mat'],'batchParams')
end
%% store allData
% 1) reduce to number of dims truly needed
allData=allData(1:maxDayInd,:,:);

%% create condData
condData.time={minDate,maxDate}; % if storing min/max times, use cell array. use mat for full time vec
condData.epochs.data=allData;

%% save out final condData
condDataDir=getCondDataDir(pars,batchParams.dataID,batchParams.conds{1});
if ~exist(condDataDir,'dir')
    mkdir(condDataDir)
end
save([condDataDir filesep 'condData.mat'],'condData')

%% save out final batchParams
save([batchParamsSaveDir filesep 'batchParams.mat'],'batchParams')
