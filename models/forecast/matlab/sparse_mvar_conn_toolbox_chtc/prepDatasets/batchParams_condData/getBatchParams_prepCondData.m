function batchParams=getBatchParams_prepCondData(pars)
%% function checks if batchParams file exists, and creates it (along w/ condData) if it does not exist
batchParamsSaveDir=[pars.dirs.baseCondDataDir filesep 'batchParams' filesep pars.dataType.globalDataCat filesep pars.dataType.varSelect filesep pars.dataType.catType];
if ~exist(batchParamsSaveDir,'dir')
    mkdir(batchParamsSaveDir)
end

%% create and save out both batchParams and condData for each data type. cond
if strcmp(pars.dataType.globalDataCat,'minute')
    keyboard % might need another condition check here to make sure prepping correct data
    if ~exist([batchParamsSaveDir filesep 'batchParams.mat'],'file')
        batchParams_condData_FOREX(pars,batchParamsSaveDir);
    end
elseif strcmp(pars.dataType.globalDataCat,'daily')
    keyboard
%     if ~exist([batchParamsSaveDir filesep 'batchParams.mat'],'file')
%         batchParams_condData_FOREX(pars,batchParamsSaveDir);
%     end
elseif strcmp(pars.dataType.globalDataCat,'CRU-RS-ZZZ') || strcmp(pars.dataType.globalDataCat,'CRU-EV-ZZZ')
    keyboard % haven't rly tested below function at all. not sure if it's the right one to use.
    batchParams=getBatchParams_sleep(electrodeFilePath);
elseif strcmp(pars.dataType.globalDataCat,'OR-RS')
    batchParams_condData_banksECoG(pars,batchParamsSaveDir);
else
    keyboard % unexpected dataset. add code to handle new case.
end
load([batchParamsSaveDir filesep 'batchParams.mat'])


